<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<?php

// PHP Examples
// Example of merchants post application, to which the datatrans service posts results
// The Datatrans service calls this application directly (server to server).
// This application receives the same parameters as the responsePage and should check and store the trx data.
// The usage of this application is mandatory, if payment method POSTFINANCE is used.
//
// Attention: this application MUST be reachable thru http protocol (https is not supported)
//
// Date   : 27.05.2004
// Author : Lubor Kult
//
// Copyright 2004, Webtra GmbH

include "signUtils.inc";

// compute the MD5 signature, if the security level 3 is used (service sends data signed)
// hashed values are : MerchantID + Amount + Currency + Datatrans TrxId
// for demo purposes, the key is hardcoded in module signUtil.inc
$responseSign=sign($key, $HTTP_POST_VARS['merchantId'],$HTTP_POST_VARS['amount'], $HTTP_POST_VARS['currency'], $HTTP_POST_VARS['uppTransactionId']);

reset ($HTTP_POST_VARS);
while (list ($key, $val) = each ($HTTP_POST_VARS)) {
    $key = strtoupper($key);
    $$key = $val;

    echo $key
	echo $val
}

if ("$status"=="success") {

	// the trx could be e.g. stored in the database
	// here is also a good place to send notification (order confirmation) to the user

	//
	// if security level 3 is used, the response signature can be checked
	//
	if ($responseSign== $HTTP_POST_VARS['sign2']){
		// handle this case
	} else {
		// handle this case (response may be hacked)
	}
}
?>
