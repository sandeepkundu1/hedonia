<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<?php

// PHP Examples
// Example of merchants receipt page, to which the user is sent back from datatrans service
//
// Date   : 27.05.2004
// Author : Lubor Kult
//
// Copyright 2004, Webtra GmbH

include "signUtils.inc";

// compute the MD5 signature, if the security level 3 is used (service sends data signed)
// hashed values are : MerchantID + Amount + Currency + Datatrans TrxId
// for demo purposes, the key is hardcoded in module signUtil.inc
$responseSign=sign($key, $HTTP_POST_VARS['merchantId'],$HTTP_POST_VARS['amount'], $HTTP_POST_VARS['currency'], $HTTP_POST_VARS['uppTransactionId']);
//echo '<pre>';print_r($responseSign);die;
?>

<HTML>

	<HEAD>
		<TITLE>PHP SAMPLES - RESPONSE PAGE</TITLE>
		<META http-equiv=Content-Type CONTENT="text/html; charset=UTF-8">
  		<META http-equiv=Content-Language CONTENT="en-us">
  		<SCRIPT language="JavaScript"></SCRIPT>
 	</HEAD>

   	<STYLE>

		body, table {
			FONT-SIZE: 12px;
			COLOR: #888888;
			FONT-FAMILY: Verdana,Arial,Helvetica;
		}
		.title {
			FONT-SIZE: 18px;
			COLOR: #666666;
			FONT-WEIGHT: bold;
		}

   	</STYLE>

 	<BODY LEFTMARGIN="20" TOPMARGIN="20">

 	<TABLE BORDER=0 CELLSPACING="0" CELLPADDING="0">

	<TR><TD COLSPAN="3" CLASS="title"><B>Sample for receipt page redirected from UPP service</B></TD></TR>
 	<TR><TD COLSPAN="3">&nbsp;</TD></TR>
	<TR><TD COLSPAN="3">(this page simulates merchant's next page after payment)</TD></TR>
 	<TR><TD COLSPAN="3">&nbsp;</TD></TR>

 	<TR><TD COLSPAN="3">List of responded Parameters:</TD></TR>
 	<TR><TD COLSPAN="3">&nbsp;</TD></TR>

 	<TR>
 		<TD><B>Parameter</B></TD>
 		<TD></TD>
 		<TD><B>Value</B></TD>
 	</TR>
 	<TR><TD COLSPAN="3">&nbsp;</TD></TR>
<?
reset ($HTTP_POST_VARS);
while (list ($key, $val) = each ($HTTP_POST_VARS)) {
    $key = strtoupper($key);
    $$key = $val;
?>
 	<TR>
  		<TD ALIGN="LEFT" nowrap><? echo $key?> : </TD>
  		<TD WIDTH="10">&nbsp;</TD>
  		<TD ALIGN="LEFT" nowrap><? echo $val ?></TD>
 	</TR>

<?
}
?>



<? if ("$status"=="error") {?>

<!-- Here can be handled the case, when if the transaction failed -->
 	<TR>
  		<TD ALIGN="LEFT" COLSPAN="3">The transaction has failed</TD>
 	</TR>
 	<TR>
  		<TD ALIGN="LEFT" nowrap>error msg: </TD>
  		<TD WIDTH="10">&nbsp;</TD>
  		<TD ALIGN="LEFT" nowrap><? echo $HTTP_POST_VARS['errorMessage'] ?></TD>
 	</TR>
 	<TR>
  		<TD ALIGN="LEFT" nowrap>error detail: </TD>
  		<TD WIDTH="10">&nbsp;</TD>
  		<TD ALIGN="LEFT" nowrap><? echo $HTTP_POST_VARS['errorDetail'] ?></TD>
 	</TR>


<?
} else {
	if ("$status"=="cancel") {
?>


<!-- Here can be handled the case, when if the transaction has been canceled -->
 	<TR>
  		<TD ALIGN="LEFT" COLSPAN="3">The transaction has been canceled</TD>
 	</TR>


<?
	} else {
?>


<!-- Here can be handled the case, when the transaction has been completed successfully -->
 	<TR>
  		<TD ALIGN="LEFT" COLSPAN="3">The transaction has been completed successfully</TD>
 	</TR>

	<!-- if security level 3 is used, the signature should be checked (option) -->
<?		if ($responseSign== $HTTP_POST_VARS['sign2']){?>
        	<TR><TD COLSPAN="3">(Signature check has been completed successfully)</TD></TR>
<?		}else{?>
        	<TR><TD COLSPAN="3">Signature check has failed (response maybe hacked?)</TD></TR>
<?
		}
	}
}
?>


	</TABLE>
	</BODY>
</HTML>
