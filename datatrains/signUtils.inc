<?php

function hexstr($hex)
{
   // translate byte array to hex string
   
   $string="";
   for ($i=0;$i<strlen($hex)-1;$i+=2)
       $string.=chr(hexdec($hex[$i].$hex[$i+1]));
   return $string;
}

function hmac ($key, $data)
{
   // RFC 2104 HMAC implementation for php.
   // Creates an md5 HMAC.
   // Eliminates the need to install mhash to compute a HMAC

   $b = 64; // byte length for md5
   if (strlen($key) > $b) {
       $key = pack("H*",md5($key));
   }
   $key  = str_pad($key, $b, chr(0x00));
   $ipad = str_pad('', $b, chr(0x36));
   $opad = str_pad('', $b, chr(0x5c));
   $k_ipad = $key ^ $ipad ;
   $k_opad = $key ^ $opad;

   return md5($k_opad  . pack("H*",md5($k_ipad . $data)));
}

function sign($key, $merchId, $amount, $ccy, $idno){
	$str=$merchId.$amount.$ccy.$idno;
	$key2=hexstr($key);
	return hmac($key2, $str);
}

//
// the key must be replaced by your own key
//
$key = "2ef48967dd92a6a483d9d20a68c6a167f44e2833a41fba94b833b6d5415a20e2184f30c72c6c5daa2c0c6df305e8b0f23bc6bec77c5a3a48555017ade261973f";

?>
