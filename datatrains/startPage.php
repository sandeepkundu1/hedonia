<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<?php

// PHP Examples
// Example of merchants page calling the datatrans service
//
// Date   : 26.05.2004
// Author : Lubor Kult
//
// Copyright 2004, Webtra GmbH

include "signUtils.inc";

$mid='1100004665'; // replace with your own id
$amt='100';
$ccy='CHF';
$ref='123456';

// compute the MD5 signature, if the security level 3 is used
// hashed values are : MerchantID + Amount + Currency + Merchants RefNo
// for demo purposes, the key is hardcoded in module signUtil.inc
$requestSign=sign($key,$mid,$amt,$ccy,$ref);

?>

<HTML>

	<HEAD>
		<TITLE>PHP SAMPLES - START PAGE</TITLE>
		<META http-equiv=Content-Type CONTENT="text/html; charset=UTF-8">
  		<META http-equiv=Content-Language CONTENT="en-us">
  		<SCRIPT language="JavaScript"></SCRIPT>
 	</HEAD>

   	<STYLE>

		body, table {
			FONT-SIZE: 12px;
			COLOR: #888888;
			FONT-FAMILY: Verdana,Arial,Helvetica;
		}
		.title {
			FONT-SIZE: 18px;
			COLOR: #666666;
			FONT-WEIGHT: bold;
		}

   	</STYLE>

 	<BODY LEFTMARGIN="20" TOPMARGIN="20">

	<FORM NAME="uppform" ACTION="https://pilot.datatrans.biz/upp/jsp/upStart.jsp" METHOD="post">

	<!-- testOnly="no" for productive environment, testOnly="yes" for test environment -->
	<INPUT TYPE=HIDDEN NAME="testOnly" 		VALUE="yes">
	<INPUT TYPE=HIDDEN NAME="language" 		VALUE="de">

	<!-- parameters, which define the transaction -->
	<INPUT TYPE=HIDDEN NAME="merchantId" 	VALUE="<?php echo $mid?>">
	<INPUT TYPE=HIDDEN NAME="amount" 		VALUE="<?php echo $amt?>">
	<INPUT TYPE=HIDDEN NAME="currency" 		VALUE="<?php echo $ccy?>">
	<INPUT TYPE=HIDDEN NAME="refno" 		VALUE="<?php echo $ref?>">
	<INPUT TYPE=HIDDEN NAME="sign"			VALUE="<?php echo $requestSign?>">

	<!-- merchant can send also his own parameters thru the upp service, if he likes -->
 	<INPUT TYPE=HIDDEN NAME="MERCHANT_PAR_1" VALUE="This is my private parameter">
 	<INPUT TYPE=HIDDEN NAME="MERCHANT_PAR_2" VALUE="This is my private parameter">


 	<TABLE BORDER=0 CELLSPACING="0" CELLPADDING="0">

	<TR><TD COLSPAN="3" CLASS="title"><B>Sample for starting UPP service</B></TD></TR>
 	<TR><TD COLSPAN="3">&nbsp;</TD></TR>
	<TR><TD COLSPAN="3">(this page simulates merchant's last checkout page before payment)</TD></TR>
 	<TR><TD COLSPAN="3">&nbsp;</TD></TR>

 	<TR><TD>merchantId :</TD><TD WIDTH="10">&nbsp;</TD><TD><?php echo $mid?></TD></TR>
 	<TR><TD>amount : 	</TD><TD WIDTH="10">&nbsp;</TD><TD><?php echo $amt?></TD></TR>
 	<TR><TD>currency : 	</TD><TD WIDTH="10">&nbsp;</TD><TD><?php echo $ccy?></TD></TR>
 	<TR><TD>refno : 	</TD><TD WIDTH="10">&nbsp;</TD><TD><?php echo $ref?></TD></TR>

	<TR><TD COLSPAN="3">&nbsp;</TD></TR>

 	<TR><TD>dig.sign : 	</TD><TD WIDTH="10">&nbsp;</TD><TD><?php echo $requestSign?></TD></TR>
 	<TR><TD>			</TD><TD WIDTH="10">&nbsp;</TD><TD>(must be sent as parameter SIGN, if security level 3 is used)</TD></TR>

	<TR><TD COLSPAN="3">&nbsp;</TD></TR>

 	<TR>
  		<TD COLSPAN="2">&nbsp;</TD>
  		<TD ALIGN=LEFT><INPUT TYPE=SUBMIT VALUE="Start Payment"></TD>
 	</TR>

 	</TABLE>

 	</FORM>
	</BODY>
</HTML>
