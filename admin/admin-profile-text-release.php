<?php 
$path=$_SERVER['DOCUMENT_ROOT'].'/hedon/';
include_once($path.'wp-load.php');
include_once($path.'wp-includes/wp-db.php');
$home_path=home_url();
$user_login_check=$_SESSION["user"];
?>
<link rel="shortcut icon" href="<?php echo $home_path; ?>/wp-content/themes/twentyfourteen/images/favicon.ico" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $home_path; ?>/wp-content/themes/twentyfourteen/css/bootstrap.css">
<link href="<?php echo $home_path; ?>/wp-content/themes/twentyfourteen/css/font-awesome.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo $home_path; ?>/wp-content/themes/twentyfourteen/css/fonts.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo $home_path; ?>/wp-content/themes/twentyfourteen/css/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $home_path; ?>/wp-content/themes/twentyfourteen/css/style.css">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo $home_path; ?>/wp-content/themes/twentyfourteen/js/bootstrap.js"></script>
<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/hedon/wp-config.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/hedon/wp-includes/general-template.php');
global $wpdb;
?>
<header class="header">
    <nav class="navbar" role="navigation">                        
        <div class="navbar-header" style="width: 100%;">            
            <a class="navbar-brand" href="<?php echo $home_path; ?>">
                <img class="img-responsive" alt="logo" src="<?php echo $home_path; ?>/wp-content/themes/twentyfourteen/images/logo.jpg">
            </a>
            <p class="profileTextInfo"><em>All profiles and members pictures must be checked BEFORE be placed in portal *)</em></p>
        </div>        
    </nav>
</header>
<div class="container">
<?php if($user_login_check!='')
{ ?>
    <div class="col-sm-12">
    <ul class="admin-Nav set">
        <?php if ($_GET['user'] == 'admin') {
            ?>
            
                <li> <a href="<?php echo $home_path; ?>/admin/admin-portal-setting.php/?user=admin">Master</a></li>
                <li> <a href="<?php echo $home_path; ?>/admin/admin-panel_member_status.php/?user=admin">Operation</a></li>
            
        <?php } ?>
        <li class="logout_div"<a id="admin_log_out" href="javascript:void(0)">Logout</a></li>
        </ul>
    </div>
    
    <div class="row">
        <div class="col-sm-6">

            <div class="panel-admin">
                <div class="panel-admin-up"><h3 class="col-sm-12">ADMIN Profile Text Release</h3></div>
                <div class="panel-admin-down textrwrapper">
                    <!--<div class="mast-pass row form-group">
                        <div class="col-sm-7">                            
                            <label>Master password</label>
                            <input type="text" class="form-control">                            
                        </div>
                        <div class="col-sm-4"><a class="arrow-right-btn" href="#"><img src="http://projects.udaantechnologies.com/hedonia_dev/wp-content/themes/twentyfourteen/images/icon-right.png"></a></div>
                    </div>-->
                    <p class="edit-text-line">New or edited <strong>texts must be checked according <a href="#" >Swiss law</a></strong> before being public</p>
                    <div class="mast-pass row form-group">
                        <div class="col-sm-8">                            
                            <label>Text of member</label>
                            <select name="text_of_member" id="text_of_member" class="text_of_member form-control">
                                <?php
                                $list_users = get_users(array(
                                    "meta_key" => "approved_as_basic",
                                    "meta_value" => "yes"
                                ));
                                foreach ($list_users as $value) {
                                    $describe_status = get_user_meta($value->ID, 'describe_status', true);
                                    if ($describe_status == '') {
                                        ?>
                                        <option value="<?php echo $value->ID; ?>"><?php echo get_user_meta($value->ID, 'nickname', true); ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>    
                                      
                        </div>
                        <div class="col-sm-2">
                            <span class="bar_img" style="display: none;float:right">
                                <img width="60px" height="12px" src="<?php echo $home_path; ?>/wp-content/themes/twentyfourteen/images/ajax-status.gif">
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-offset-2 col-sm-10"><textarea id="member_introduction" class="form-control" rows="4" cols="50" style="min-height: 350px;"></textarea></div>                    
                    <div class="buttonBar col-sm-offset-2 col-sm-10">
                        <a href="javascript:void(0)" id="accept_content" class="accept_content btn pull-left acceptBtn">accepted</a>
                        <a href="javascript:void(0)" id="refused_content" class="refused_content btn pull-right refuseBtn">refused</a> 
                    </div>
                    <div id="msg" class="successMSg"></div>
                    <span class="bar_img21" style="display: none;float:right">
                        <img width="60px" height="12px" src="<?php echo $home_path; ?>/wp-content/themes/twentyfourteen/images/ajax-status.gif">
                    </span>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="panel-admin">
                <div class="panel-admin-up"><h3 class="col-sm-12">ADMIN Photo Release</h3></div>            

                <div class="panel-admin-down textrwrapper">
                    <!--<div class="mast-pass row form-group">
                        <div class="col-sm-7">                            
                            <label>Master password</label>
                            <input type="text" class="form-control">                            
                        </div>
                        <div class="col-sm-4"><a class="arrow-right-btn" href="#"><img src="http://projects.udaantechnologies.com/hedonia_dev/wp-content/themes/twentyfourteen/images/icon-right.png"></a></div>
                    </div>-->
                    <p class="edit-text-line">Each new members picture has to be checked according <strong><a href="#" >Swiss law</a></strong></p>
                    <div class="mast-pass row form-group">
                        <div class="col-sm-8">                            
                            <label>Picture of member</label>
                            <select name="album_of_member" id="album_of_member" class="album_of_member form-control">
                                <?php
                                foreach ($list_users as $value) {
                                    $albumb_id = $wpdb->get_results("SELECT id FROM user_create_album WHERE user_id=" . $value->ID . "");
                                    if (!empty($albumb_id)) {
                                        ?>
                                        <option value="<?php echo $value->ID; ?>"><?php echo get_user_meta($value->ID, 'nickname', true); ?>
                                        </option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>                                    
                        </div>     
                         <div class="col-sm-2">
                           <span class="bar_img_load" style="display: none;float:right">
                                <img width="60px" height="12px" src="<?php echo $home_path; ?>/wp-content/themes/twentyfourteen/images/ajax-status.gif">
                            </span> 
                        </div>
                    </div>
                    <div class="col-sm-offset-2 col-sm-10">
                        <div class="slider-main">
                            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner" role="listbox">
                                </div>
                                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                    <span class="left-arrow" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                    <span class="right-arrow" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div> 
                    </div>
                    <div class="buttonBar pickside" id="image_status">
                        <a href="javascript:void(0)" id="accept_image" class="accept_image btn pull-left acceptBtn">accepted</a>
                        <a href="javascript:void(0)" id="refused_image" class="refused_image pull-right refuseBtn">refused</a>
                    </div>
                    <span class="bar_img1" style="display: none;float:right">
                                <img width="60px" height="12px" src="<?php echo $home_path; ?>/wp-content/themes/twentyfourteen/images/ajax-status.gif">
                            </span>
                    <div id="accept_ref_msg" class="successMsg"></div>
                </div>

            </div>
        </div>
    </div>
    <p class="termsText"><em>*) This job can be done by third parties. For these moderator we need a special password for access only to this area (not the Master Password !)</em></p>
    <?php } else { ?>
    <h2 style="text-align: center;">Unauthorised access</h2>
    <p style="text-align: center; color:#fff;">Please click <a href="<?php echo $home_path; ?>/admin">here</a> to login</p>
    <?php } ?>
</div>



<script>
    $(document).ready(function () {
        var home_path='<?php echo $home_path; ?>';
        var id = $('select#text_of_member option:selected').val();
        $.ajax({
            type: "POST",
            url: home_path+"/wp-admin/admin-ajax.php",
            data: ({action: "get_text_of_member_profile", id: id}),
            success: function (data)
            {
                var intro = data.trim();
                if (intro == '')
                {
                    $('#member_introduction').val("No Introduction");
                }
                else
                {
                    $('#member_introduction').val(intro);
                }
            }
        });
    })
</script>
<script>
    $(document).ready(function () {
        var home_path='<?php echo $home_path; ?>';
        $('select#text_of_member').on('change', function () {
            var id = this.value;
            $('.bar_img').show();
            $.ajax({
                type: "POST",
                url: home_path+"/wp-admin/admin-ajax.php",
                data: ({action: "get_text_of_member_profile", id: id}),
                success: function (data)
                {
                    var intro = data.trim()
                    $('.bar_img').hide();
                    if (intro == '')
                    {
                        $('#member_introduction').val("No Introduction");
                    }
                    else
                    {
                        $('#member_introduction').val(intro);
                    }
                }
            });
        });
        $('#accept_content').click(function () {
            var id = $('select#text_of_member option:selected').val();
            $('.bar_img21').show();
            $.ajax({
                type: "POST",
                url: home_path+"/wp-admin/admin-ajax.php",
                data: ({action: "get_text_of_member_accept_or_refused", id: id, status: 'accept'}),
                success: function (data)
                {
                    $('.bar_img21').hide();
                    var result = jQuery.parseJSON(data);
                    jQuery('#msg').html(result.msg);
                    setTimeout(function(){jQuery('#msg').fadeOut(3000);location.reload();}, 3000);   
                    
                }
            });
        });
        $('#refused_content').click(function () {
            var id = $('select#text_of_member option:selected').val();
            $('.bar_img21').show();
            $.ajax({
                type: "POST",
                url: home_path+"/wp-admin/admin-ajax.php",
                data: ({action: "get_text_of_member_accept_or_refused", id: id, status: 'refused'}),
                success: function (data)
                {
                    $('.bar_img21').hide();
                    var result = jQuery.parseJSON(data);
                    jQuery('#msg').html(result.msg);
                    setTimeout(function(){jQuery('#msg').fadeOut(3000);location.reload();}, 3000);
                }
            });
        });
    });
</script>
<script>
    $(document).ready(function () {
        var home_path='<?php echo $home_path; ?>';
        var status = '';
        var img_url = '<?php echo get_template_directory_uri(); ?>/images/';
        var id = $('select#album_of_member option:selected').val();
        $.ajax({
            type: "POST",
            url: home_path+"/wp-admin/admin-ajax.php",
            data: ({action: "get_album_of_member", id: id}),
            success: function (data)
            {
                $('.carousel-inner').html(data);
                status = $('.active').attr('status');
                if (status == 1)
                {
                    $('#image_status').html('<span><img src="' + img_url + 'right_button.jpeg" class="pull-left" height="35" width="35"></span><a href="javascript:void(0)" id="refused_image" class="refused_image pull-right refuseBtn">refused</a>')
                }
                else if (status == 2)
                {
                    $('#image_status').html('<a href="javascript:void(0)" id="accept_image" class="accept_image btn pull-left acceptBtn">accept</a><span><img src="' + img_url + 'cross_button.png" height="35" width="35" class="pull-right"></span>')
                }
            }
        });
        $('.right').click(function () {
            status = $('.active').next('.item').attr('status');
            if (status == 1)
            {
                $('#image_status').html('<span><img src="' + img_url + 'right_button.jpeg" class="pull-left" height="35" width="35"></span><a href="javascript:void(0)" id="refused_image" class="refused_image pull-right refuseBtn">refused</a>')
            }
            else if (status == 2)
            {
                $('#image_status').html('<a href="javascript:void(0)" id="accept_image" class="accept_image btn pull-left acceptBtn">accept</a><span><img src="' + img_url + 'cross_button.png" height="35" width="35" class="pull-right"></span>')
            }
            else
            {
                $('#image_status').html('<a href="javascript:void(0)" id="accept_image" class="accept_image btn pull-left acceptBtn">accept</a><a href="javascript:void(0)" id="refused_image" class="refused_image pull-right refuseBtn">refused</a>')
            }
        });
        $('.left').click(function () {
            status = $('.active').prev('.item').attr('status');
            if (status == 1)
            {
                $('#image_status').html('<span><img src="' + img_url + 'right_button.jpeg" class="pull-left" height="35" width="35"></span><a href="javascript:void(0)" id="refused_image" class="refused_image pull-right refuseBtn">refused</a>')
            }
            else if (status == 2)
            {
                $('#image_status').html('<a href="javascript:void(0)" id="accept_image" class="accept_image btn pull-left acceptBtn">accept</a><span><img src="' + img_url + 'cross_button.png" height="35" width="35" class="pull-right"></span>')
            }
            else
            {
                $('#image_status').html('<a href="javascript:void(0)" id="accept_image" class="accept_image btn pull-left acceptBtn">accept</a><a href="javascript:void(0)" id="refused_image" class="refused_image pull-right refuseBtn">refused</a>')
            }
        });
    });
</script>
<script>
$(document).ready(function(){
    var home_path='<?php echo $home_path; ?>';
    $('select#album_of_member').on('change', function () {
        var img_url = '<?php echo get_template_directory_uri(); ?>/images/';
        var id = this.value;
        var status = '';
        $('.bar_img_load').show();
        $.ajax({
            type: "POST",
            url: home_path+"/wp-admin/admin-ajax.php",
            data: ({action: "get_album_of_member", id: id}),
            success: function (data)
            {
                $('.bar_img_load').hide();
                $('.carousel-inner').html(data);
                status = $('.active').attr('status');
                if (status == 1)
                {
                    $('#image_status').html('<span><img src="' + img_url + 'right_button.jpeg" class="pull-left" height="35" width="35"></span><a href="javascript:void(0)" id="refused_image" class="refused_image pull-right refuseBtn">refused</a>')
                }
                else if (status == 2)
                {
                    $('#image_status').html('<a href="javascript:void(0)" id="accept_image" class="accept_image btn pull-left acceptBtn">accept</a><span><img src="' + img_url + 'cross_button.png" height="35" width="35" class="pull-right"></span>')
                }
                else
                {
                    $('#image_status').html('<a href="javascript:void(0)" id="accept_image" class="accept_image btn pull-left acceptBtn">accept</a><a href="javascript:void(0)" id="refused_image" class="refused_image pull-right refuseBtn">refused</a>')
                }
            }
        });
    });
});
</script>
<script>
    $(document).ready(function () {
        var home_path='<?php echo $home_path; ?>';
        $(document).on('click', 'a#accept_image', function () {
            var img_id = $('.active').attr('id');
            var status = 1;
            $('.bar_img1').show();
            $.ajax({
                type: "POST",
                url: home_path+"/wp-admin/admin-ajax.php",
                data: ({action: "get_album_accept_refused", img_id: img_id, status: status}),
                success: function (data)
                {
                    $('.bar_img1').hide();
                    $('#accept_ref_msg').html("Image has been accepted");
                }
            });
        });
        $(document).on('click', 'a#refused_image', function () {
            var img_id = $('.active').attr('id');
            var userid = jQuery('select#album_of_member option').val();
            var status = 2;
            $('.bar_img1').show();
            $.ajax({
                type: "POST",
                url: home_path+"/wp-admin/admin-ajax.php",
                data: ({action: "get_album_accept_refused", img_id: img_id, status: status, userid: userid}),
                success: function (data)
                {
                    $('.bar_img1').hide();
                    $('#accept_ref_msg').html("Image has been refused");
                }
            });
        });
        $('#admin_log_out').click(function(){
            $.ajax({
                type:'POST',
                data:({action:"user_logout_admin_ajax"}),
                url: home_path+"/wp-admin/admin-ajax.php",
                success: function (data)
                {
                    window.location = home_path+"/admin";
                }
            });
        });
    });
</script>
