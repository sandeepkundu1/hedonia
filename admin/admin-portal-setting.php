<?php 
$path=$_SERVER['DOCUMENT_ROOT'].'/hedon/';
include_once($path.'wp-load.php');
include_once($path.'wp-includes/wp-db.php');
$home_path=home_url();
$user_login_check=$_SESSION["user"];
?>
<link rel="shortcut icon" href="<?php echo $home_path; ?>/wp-content/themes/twentyfourteen/images/favicon.ico" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $home_path; ?>/wp-content/themes/twentyfourteen/css/bootstrap.css">
<link href="<?php echo $home_path; ?>/wp-content/themes/twentyfourteen/css/font-awesome.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo $home_path; ?>/wp-content/themes/twentyfourteen/css/fonts.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo $home_path; ?>/wp-content/themes/twentyfourteen/css/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $home_path; ?>/wp-content/themes/twentyfourteen/css/style.css">
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<?php 
require_once($path.'/wp-config.php');
require_once($path.'/wp-includes/general-template.php');
global $wpdb;
$result = $wpdb->get_results("select * from wp_admin_portal_setting where portal_setting_type = 'portal setting' ");
?>
<header class="header">
    <nav role="navigation" class="navbar">                        
        <div style="width: 100%;" class="navbar-header">            
            <a href="http://projects.udaantechnologies.com/hedon" class="navbar-brand">
                <img src="<?php echo $home_path; ?>/wp-content/themes/twentyfourteen/images/logo.jpg" alt="logo" class="img-responsive">
            </a>            
        </div>        
    </nav>
</header>
<div class="admin-panal">
<?php if($user_login_check!='')
{ ?>
    <div class="col-sm-2 col-md-1 user-img no-padding">
		<img class="user-icon" src="<?php echo $home_path; ?>/wp-content/themes/twentyfourteen/images/user-img.png" alt="user">
    </div>
    <div class="col-sm-10 col-md-11 admin-panel-sec">
	<!-- Panel start -->
        <div class="col-sm-12">
        <ul class="admin-Nav">
                <?php if ($_GET['user'] == 'admin') {
                    ?>
                    
                        <li><a href="<?php echo $home_path; ?>/admin/admin-profile-text-release.php/?user=admin">Moderator</a></li>
                        <li> <a href="<?php echo $home_path; ?>/admin/admin-panel_member_status.php/?user=admin">Operation</a></li>
                    
                <?php } ?>
                <li class="logout_div"><a id="admin_log_out" href="javascript:void(0)">Logout</a></li>
                </ul>
            </div>
            
		<div class="panel-admin">
	    	<div class="panel-admin-up">
				<div class="col-sm-3">
		    		<h3>ADMIN portal settings </h3>
				</div>
				<!--<div class="col-sm-9 mast-pass">
		    		<div class="col-sm-6">
						<div class="form-group">
			    			<label>Master password</label>
			    			<input type="text" class="form-control">
						</div>
		    		</div>
		    		<div class="col-sm-4">
		    			<a class="arrow-right-btn" href="#"><img src="http://projects.udaantechnologies.com/hedonia_dev/wp-content/themes/twentyfourteen/images/icon-right.png">
		    			</a>
		    		</div>
				</div>-->
	    	</div>
	    	<div class="panel-admin-down">
				<div class="col-sm-12">
		    		<div class="col-sm-5 col-md-5 left-sec">
						<div class="row">
			    			<h3>PROFILE SETTINGS
							<br/>
							<small style="visibility: hidden">&nbsp;</small>
			    			</h3>
			    			<ul class="list-all all1 profile-setting selectBox-options">
								<!--<li id="li1" for="profile_settings" get="first">Min. character</li>-->
								<li id="li1" for="profile_settings" get="second"><input type="text" value="<?php echo $result[0]->portal_setting_text ; ?>"></li>
								<!--<li id="li3" for="profile_settings" get="third">Second residence</li>-->
								<li id="li2" for="profile_settings" get="fifth"><input type="text" value="<?php echo $result[1]->portal_setting_text ; ?>"></li>
								<li id="li3" for="profile_settings" get="sixth"><input type="text" value="<?php echo $result[2]->portal_setting_text ; ?>"></li>
			    			 </ul>
						</div>			
		    		</div>
		    		<div class="col-sm-7 col-md-7 right-sec">
						<div class="row">
			    			<div class="col-sm-4">
								<h3>BASIC MEMBERS
								    <br/>
								    <small>not validated</small>
								</h3>
								
								<ul class="list-all all1 basic-mamber">
								    <!--<li for="basic_member" id="basic1" get="first"><input type="text" value="<?php // echo $result[0]->basic; ?>"></li>-->
								    <li for="basic_member" id="basic1" get="second"><input type="text" value="<?php echo $result[0]->basic; ?>"></li>
								   <?php /* ?> <li for="basic_member" id="basic3" get="third">
								    <input id="second_residence23" <?php echo ($result[2]->basic == '1') ? "checked" : ""; ?> type="checkbox" for="search" value="couples" name="couples" class="checkbox_cus checkInput">
									<label for="second_residence23" class="checkbox_label"></label></li><?php */ ?>
								    <li for="basic_member" id="basic2" get="fifth">
								    <input id="second_residence23" <?php echo ($result[1]->basic == '1') ? "checked" : ""; ?> type="checkbox" for="search" value="couples" name="couples" class="checkbox_cus checkInput">
									<label for="second_residence23" class="checkbox_label"></label></li>
								    <li for="basic_member" id="basic3" get="sixth">
								    <input id="second_residence32" <?php echo ($result[2]->basic == '1') ? "checked" : ""; ?> type="checkbox" for="search" value="couples" name="couples" class="checkbox_cus checkInput">
									<label for="second_residence32" class="checkbox_label"></label></li>
								</ul>
			    			</div>
			    			<div class="col-sm-4">
								<h3>BASIC MEMBERS
								    <br/>
								    <small>validated</small>
								</h3>
								<ul class="list-all all1 basic-mamb-valid">
								    <?php /* ?><li for="basic_member_valid" id="validate1" get="first"><input type="text" value="<?php echo $result[0]->validated; ?>"></li><?php */ ?>
								    <li for="basic_member_valid" id="validate1" get="second"><input type="text" value="<?php echo $result[0]->validated; ?>"></li>
								    <?php /* ?><li for="basic_member_valid" id="validate3" get="third"><input id="second_residence44" <?php echo ($result[2]->validated == '1') ? "checked" : ""; ?> type="checkbox" for="search" value="couples" name="couples" class="checkbox_cus checkInput">
									<label for="second_residence44" class="checkbox_label"></label></li>
								    <?php */ ?><li for="basic_member_valid" id="validate2" get="fifth"><input id="second_residence45" <?php echo ($result[1]->validated == '1') ? "checked" : ""; ?> type="checkbox" for="search" value="couples" name="couples" class="checkbox_cus checkInput">
									<label for="second_residence45" class="checkbox_label"></label></li>
								    <li for="basic_member_valid" id="validate3" get="sixth" class="for-checkbox">
									    <input type="checkbox" <?php echo ($result[2]->validated == '1') ? "checked" : ""; ?> for="search" value="couples" name="couples" class="checkbox_cus checkInput" id="couples">
										<label for="couples" class="checkbox_label"></label>
									</li>
								</ul>
			    			</div>
			    			<div class="col-sm-4">
								<h3 class="primeum-mamb">PREMIUM MEMBERS
				    			<br/>
				    			<small>validated + paying</small>
								</h3>
								<ul class="list-all all1 primium-mamber">
								    <?php /*?><li for="premium_member" id="premium1" get="first"><input type="text" value="<?php echo $result[0]->premium; ?>"></li><?php */ ?>
								    <li for="premium_member" id="premium1" get="second"><input type="text" value="<?php echo $result[0]->premium; ?>"></li>
								   <?php /* ?> <li for="premium_member" id="premium3" get="third" class="for-checkbox">
									<input id="second_residence" <?php echo ($result[2]->premium == '1') ? "checked" : ""; ?>  type="checkbox" for="search" value="couples" name="couples" class="checkbox_cus checkInput">
									<label for="second_residence" class="checkbox_label"></label>
								    </li><?php */ ?>
								    <li for="premium_member" id="premium2" get="fifth" class="for-checkbox">
									<input id="erotic_profile" type="checkbox" <?php echo ($result[1]->premium == '1') ? "checked" : ""; ?> for="search" value="couples" name="couples" class="checkbox_cus checkInput">
									<label for="erotic_profile" class="checkbox_label"></label>
								    </li>
								    <li for="premium_member" id="premium3" get="sixth" class="for-checkbox">
									<input id="perfect_match" type="checkbox" <?php echo ($result[2]->premium == '1') ? "checked" : ""; ?> for="search" value="couples" name="couples" class="checkbox_cus checkInput">
									<label for="perfect_match" class="checkbox_label"></label>
								    </li>
								</ul>
			    			</div>
			    			<a href="javascript:void(0)" id="profile_settings" class="btn-save pull-right">save</a>
			    			<span class="bar_img" style="display: none;float:right">
                                <img width="60px" height="12px" src="<?php echo $home_path; ?>/wp-content/themes/twentyfourteen/images/ajax-status.gif">
                            </span>
                            <div id="setting_msg" class="successMsg"></div>    
						</div>
		    		</div>
				</div>
	    	</div>
		</div>
<script>
$('.checkInput').click(function(){ 
    if (this.checked == true)
        {
            $(this).each(function () {
                $(this).attr('checked', 'checked');
            });
        } else {
            $(this).each(function () {
                $(this).attr('checked', false);
            });
        } 
});
</script>  
   

<script type="text/javascript">
	$(document).ready(function()
	{
		var home_path='<?php echo $home_path; ?>';
		$('a#profile_settings').click(function()
		{
			$('.bar_img').show();			
			var count = 0 ;
			$('ul.all1').each(function(){
				count = $(this).children('li').length;				
			});
			var json = '[';
			for(var i=1;i<=count;i++)
			{	
				if($('ul.all1').children('#li'+i).find('input').attr('type') == 'text'){
				
				json = json + '{"portal_setting_text":"'+$('ul.all1').children('#li'+i).children('input').val()+'",';
			    }
				if($('ul.all1').children('#basic'+i).find('input').attr('type') == 'text'){
					json = json + '"basic":"'+$('ul.all1').children('#basic'+i).children('input').val()+'",';
				} 
				else if($('ul.all1').children('#basic'+i).find('input').attr('type') == 'checkbox'){
					if($('ul.all1').children('#basic'+i).children('input').attr('checked') == 'checked')
					{
						json = json + '"basic":"1",';	
					}else
					{
						json = json + '"basic":"0",';
					}
					
				}
				else{
					json = json + '"basic":"'+$('ul.all1').children('#basic'+i).text()+'",';
				}
				
				if($('ul.all1').children('#validate'+i).find('input').attr('type') == 'checkbox'){
					if($('ul.all1').children('#validate'+i).children('input').attr('checked') == 'checked')
					{
						json = json + '"validate":"1",';	
					}else
					{
						json = json + '"validate":"0",';
					}					
				}else if($('ul.all1').children('#validate'+i).find('input').attr('type') == 'text'){
						json = json + '"validate":"'+$('ul.all1').children('#validate'+i).children('input').val()+'",';
					}
					else{
						json = json + '"validate":"'+$('ul.all1').children('#validate'+i).text()+'",';
					}
				if($('ul.all1').children('#premium'+i).find('input').attr('type') == 'checkbox'){
					if($('ul.all1').children('#premium'+i).children('input').attr('checked') == 'checked'){
						json = json + '"premium":"1"';	
					}else
					{
						json = json + '"premium":"0"';
					}				
				}
				else if($('ul.all1').children('#premium'+i).find('input').attr('type') == 'text')
					{
						json = json + '"premium":"'+$('ul.all1').children('#premium'+i).children('input').val()+'"';
					}
				else
					{
						json = json + '"premium":"'+$('ul.all1').children('#premium'+i).text()+'"';	
					}		
				json = json + '},';			
			}
			json = json.replace(/,\s*$/,"");
			json = json + ']';	
			var formData = 'action=save_profile_setting&data='+json+'&type=character_seeting';
			$.ajax({
					type: "POST",
					url: home_path+"/wp-admin/admin-ajax.php",
					data: formData,
					success:function(data)
					{
							$('.bar_img').hide();
							$('#setting_msg').html('Portal setting has saved!');
							setTimeout(function(){jQuery('#setting_msg').fadeOut(7000);}, 7000);	
					}
				});
		});
	});
</script>		
	<!-- Panel end -->
	<?php 
	$album = $wpdb->get_results("select * from wp_admin_portal_setting where portal_setting_type = 'album setting'");
	?>
	<!-- panel start -->
		<div class="panel-admin">
	    	<div class="panel-admin-up">
				<div class="col-sm-3">
		    		<h3>ADMIN portal settings </h3>
				</div>
				<!--<div class="col-sm-9 mast-pass">
		    		<div class="col-sm-6">
						<div class="form-group">
			    			<label>Master password</label>
			    			<input type="text" class="form-control">
						</div>
		    		</div>
		    		<div class="col-sm-4">
		    			<a class="arrow-right-btn" href="#"><img src="http://projects.udaantechnologies.com/hedonia_dev/wp-content/themes/twentyfourteen/images/icon-right.png">
		    			</a>
		    		</div>
				</div>-->
	    	</div>
	    	<div class="panel-admin-down">
				<div class="col-sm-12">
		    		<div class="col-sm-5 col-md-5 left-sec">
						<div class="row">
						    <h3>ALBUM SETTINGS
							<br/>
							<small style="visibility: hidden">&nbsp;</small>
						    </h3>
						    <ul class="list-all all2 profile-setting">
								<li for="profile_settings" id="albText1"><input type="text" value="<?php echo $album[0]->portal_setting_text ; ?>"></li>
								<li for="profile_settings" id="albText2"> <input type="text" value="<?php echo $album[1]->portal_setting_text ; ?>"></li>
								<li for="profile_settings" id="albText3"> <input type="text" value="<?php echo $album[2]->portal_setting_text ; ?>"></li>
								<li for="profile_settings" id="albText4"><input type="text" value="<?php echo $album[3]->portal_setting_text ; ?>"></li>				
						    </ul>
						</div>			
		    		</div>
		    		<div class="col-sm-7 col-md-7 right-sec">
						<div class="row">
			    			<div class="col-sm-4">
								<h3>BASIC MEMBERS
								    <br/>
								    <small>not validated</small>
								</h3>
								<ul class="list-all all2 basic-mamber">
								    <li for="basic_member" id="basic_alb1" ><input type="text" value="<?php echo $album[0]->basic; ?>" id="number_of_album_basic" class="number_of_album"></li>
								    <li for="basic_member" id="basic_alb2"><input type="text" value="<?php echo $album[1]->basic; ?>" class="number_of_images"></li>
								    <li for="basic_member" id="basic_alb3"><input type="text" value="<?php echo $album[2]->basic; ?>" class="number_of_lockable"></li>
								    <li for="basic_member" id="basic_alb4"><input type="text" value="<?php echo $album[3]->basic; ?>" class="number_of_high_pic"></li>				    
								</ul>
			    			</div>
			    			<div class="col-sm-4">
								<h3>BASIC MEMBERS
								    <br/>
								    <small>validated</small>
								</h3>
								<ul class="list-all all2 basic-mamb-valid">
								    <li for="basic_member_valid" id="validate_alb1"><input type="text" value="<?php echo $album[0]->validated; ?>" id="number_of_album_validate" class="number_of_album"></li>
								    <li for="basic_member_valid" id="validate_alb2"><input type="text" value="<?php echo $album[1]->validated; ?>" class="number_of_images"></li>
								    <li for="basic_member_valid" id="validate_alb3"><input type="text" value="<?php echo $album[2]->validated; ?>" class="number_of_lockable"></li>
								    <li for="basic_member_valid" id="validate_alb4"><input type="text" value="<?php echo $album[3]->validated; ?>" class="number_of_high_pic"></li>				    
								</ul>
			    			</div>
			    			<div class="col-sm-4">
								<h3>PREMIUM MEMBERS
								    <br/>
								    <small>validated + paying</small>
								</h3>
								<ul class="list-all all2 primium-mamber">
								    <li for="premium_member" id="primium_alb1"><input type="text" value="<?php echo $album[0]->premium; ?>" id="number_of_album_premium" class="number_of_album"></li>
								    <li for="premium_member" id="primium_alb2"><input type="text" value="<?php echo $album[1]->premium; ?>" class="number_of_images"></li>
								    <li for="premium_member" id="primium_alb3"><input type="text" value="<?php echo $album[2]->premium; ?>" class="number_of_lockable"></li>
								    <li for="premium_member" id="primium_alb4"><input type="text" value="<?php echo $album[3]->premium; ?>" class="number_of_high_pic"></li>				    
								</ul>
			    			</div>			    
						</div>
		    		</div>
		    		<div class="total-size">
						<a href="javascript:void(0)" id="album_settings" class="btn-save pull-right">save</a>
						<span class="bar_img1" style="display: none;float:right">
                            <img width="60px" height="12px" src="<?php echo $home_path; ?>/wp-content/themes/twentyfourteen/images/ajax-status.gif">
                        </span>
		    		</div>
		    		<div id="album_setting_msg" class="successMsg"></div>
				</div>
	    	</div>
		</div>
<script type="text/javascript">
	$(document).ready(function()
	{
		$('.number_of_album').keyup(function() 
		{
               var num_alb=$(this).val();
               if(num_alb>5)
               {
               	$(this).val(0);
               	$('#album_setting_msg').html("You can enter maximum 5")
               }
        });
        $('.number_of_images').keyup(function() 
		{
               var num_alb=$(this).val();
               if(num_alb>10)
               {
               	$(this).val(0);
               	$('#album_setting_msg').html("You can enter maximum 10")
               }
        });
        $('.number_of_lockable').keyup(function() 
		{
               var num_alb=$(this).val();
               if(num_alb>4)
               {
               	$(this).val(0);
               	$('#album_setting_msg').html("You can enter maximum 4")
               }
        });
        $('.number_of_high_pic').keyup(function() 
		{
               var num_alb=$(this).val();
               if(num_alb>3)
               {
               	$(this).val(0);
               	$('#album_setting_msg').html("You can enter maximum 3")
               }
        });
		var home_path='<?php echo $home_path; ?>';
		$('a#album_settings').click(function()
		{
			$('.bar_img1').show();
			var i = 1 ;
			var json = '[';
			$('ul.all2').each(function()
			{
				if($('ul.all2').children('#basic_alb'+i).find('input').attr('type') == 'text')
				{
				json = json + '{"portal_setting_text":"'+$('ul.all2').find('#albText'+i).children('input').val()+'",';		
			    }		
				if($('ul.all2').children('#basic_alb'+i).find('input').attr('type') == 'text')
				{
					json = json + '"basic":"'+$('ul.all2').children('#basic_alb'+i).children('input').val()+'",';
				}
				else
				{
					json = json + '"basic":"'+$('ul.all2').children('#basic_alb'+i).text()+'",';
				}
				json = json + '"validate":"'+$('ul.all2').children('#validate_alb'+i).children('input').val()+'",';
				json = json + '"premium":"'+$('ul.all2').children('#primium_alb'+i).children('input').val()+'"';
				json = json + '},';
				i++;
			});
			json = json + '{"portal_setting_text":"'+$('ul.automatic li').text()+'",';
			json = json + '"basic":"'+$('input#auto_unlock').val()+'",';
			json = json + '"validate":"'+$('input#auto_unlock').val()+'",';
			json = json + '"premium":"'+$('input#auto_unlock').val()+'"},';
			json = json.replace(/,\s*$/,"");
			json = json + ']';	
			var formData = 'action=save_profile_setting&data='+json+'&type=album_setting';
			$.ajax({
					type: "POST",
					url: home_path+"/wp-admin/admin-ajax.php",
					data: formData,
					success:function(data)
					{
							$('.bar_img1').hide();
							$('#album_setting_msg').html('Album setting has saved!');
							setTimeout(function(){jQuery('#album_setting_msg').fadeOut(7000);}, 7000);	
					}
				});
		});
	});
</script>
	<!-- Panel end -->
	<!-- Panel start -->
	<?php 
	$visibilty = $wpdb->get_results("select * from wp_admin_portal_setting where portal_setting_type = 'visibilty setting' ");
	?>
		<div class="panel-admin">
	    	<div class="panel-admin-up">
				<div class="col-sm-3">
		    		<h3>ADMIN portal settings </h3>
				</div>
				<!--<div class="col-sm-9 mast-pass">
		    		<div class="col-sm-6">
						<div class="form-group">
			    			<label>Master password</label>
			    			<input type="text" class="form-control">
						</div>
		    		</div>
		    		<div class="col-sm-4">
		    			<a class="arrow-right-btn" href="#"><img src="http://projects.udaantechnologies.com/hedonia_dev/wp-content/themes/twentyfourteen/images/icon-right.png">
		    			</a>
		    		</div>
				</div>-->
	    	</div>
	    	<div class="panel-admin-down">
				<div class="col-sm-12">
		    		<div class="col-sm-5 col-md-5 left-sec">
						<div class="row">
			    			<h3>VISIBILITY
							<br/>
							<small style="visibility: hidden">&nbsp;</small>
			    			</h3>
						    <ul class="list-all all3 profile-setting">
								<li for="profile_settings" id="text1"><input type="text" value="<?php echo $visibilty[0]->portal_setting_text ; ?>"></li>
								<li for="profile_settings" id="text2"><input type="text" value="<?php echo $visibilty[1]->portal_setting_text ; ?>"></li> 
								<li for="profile_settings" id="text3"><input type="text" value="<?php echo $visibilty[2]->portal_setting_text ; ?>"></li>
								<li for="profile_settings" id="text4"><input type="text" value="<?php echo $visibilty[3]->portal_setting_text ; ?>"></li>
						    </ul>
						</div>			
		    		</div>
		    		<div class="col-sm-7 col-md-7 right-sec">
						<div class="row">
			    			<div class="col-sm-4">
								<h3>BASIC MEMBERS
								    <br/>
								    <small>not validated</small>
								</h3>
								<ul class="list-all all3 basic-mamber">
								    <li id="visibilty_basic1"><input type="text" value="<?php echo $visibilty[0]->basic; ?>"></li>
								    <li id="visibilty_basic2"><input type="text" value="<?php echo $visibilty[1]->basic; ?>"></li>
								    <li id="visibilty_basic3">
								    	<input id="basicResidence" <?php echo ($visibilty[2]->basic == '1') ? "checked" : ""; ?> type="checkbox" name="couples" class="checkbox_cus checkFields">
										<label for="basicResidence" class="checkbox_label"></label>
								    </li>
								    <li id="visibilty_basic4">
								    	<input id="basicResidence1" <?php echo ($visibilty[3]->basic == '1') ? "checked" : ""; ?> type="checkbox" name="couples" class="checkbox_cus checkFields">
										<label for="basicResidence1" class="checkbox_label"></label>
								    </li>
								</ul>
			    			</div>
			    			<div class="col-sm-4">
								<h3>BASIC MEMBERS
								    <br/>
								    <small>validated</small>
								</h3>
								<ul class="list-all all3 basic-mamb-valid">
								    <li for="basic_member_valid" id="visibilty_valid1"><input type="text" value="<?php echo $visibilty[0]->validated; ?>"></li>
								    <li for="basic_member_valid" id="visibilty_valid2"><input type="text" value="<?php echo $visibilty[1]->validated; ?>"></li>								    
								    <li id="visibilty_valid3">
								    	<input id="sresidence" for="search" <?php echo ($visibilty[2]->validated == '1') ? "checked" : ""; ?> value="couples" type="checkbox" name="couples" class="checkbox_cus checkFields">
										<label for="sresidence" class="checkbox_label"></label>
								    </li>								    
								    <li id="visibilty_valid4">
								    	<input id="hresidence" type="checkbox" <?php echo ($visibilty[3]->validated == '1') ? "checked" : ""; ?> name="couples" class="checkbox_cus checkFields">
										<label for="hresidence" class="checkbox_label"></label>
								    </li>
								</ul>
			    			</div>
						    <div class="col-sm-4">
								<h3 class="primeum-mamb">PREMIUM MEMBERS
								    <br/>
								    <small>validated + paying</small>
								</h3>
								<ul class="list-all all3 primium-mamber">
								    <li for="premium_member" id="visibilty_pre1"><input type="text" value="<?php echo $visibilty[0]->premium; ?>"></li>
								    <li for="premium_member" id="visibilty_pre2"><input type="text" value="<?php echo $visibilty[1]->premium; ?>"></li>
								    <li for="premium_member" id="visibilty_pre3">
									<input id="description" type="checkbox" for="search" value="couples" <?php echo ($visibilty[2]->premium == '1') ? "checked" : ""; ?> name="couples" class="checkbox_cus checkFields">
									<label for="description" class="checkbox_label"></label>
								    </li>
								    <li for="premium_member" id="visibilty_pre4">
									<input id="register_second" type="checkbox" for="search" value="couples" <?php echo ($visibilty[3]->premium == '1') ? "checked" : ""; ?> name="couples" class="checkbox_cus checkFields">
									<label for="register_second" class="checkbox_label"></label>
								    </li>
								</ul>
						    </div>
			    			<a href="javascript:void(0)" id="visibility" class="btn-save pull-right">save</a>
			    			<span class="bar_img_vis" style="display: none;float:right">
                                <img width="60px" height="12px" src="<?php echo $home_path; ?>/wp-content/themes/twentyfourteen/images/ajax-status.gif">
                            </span>
                            <div id="visibility_msg" class="successMsg"></div> 
						</div>
		    		</div>
				</div>
	    	</div>
		</div>
<script type="text/javascript">
$('.checkFields').click(function(){ 
    if (this.checked == true)
        {
            $(this).each(function () {
                $(this).attr('checked', 'checked');
            });
        } else {
            $(this).each(function () {
                $(this).attr('checked', false);
            });
        } 
});
</script>
<script>
	$(document).ready(function()
	{
		var home_path='<?php echo $home_path; ?>';
		var count = 0 ;
		$('ul.all3').each(function(){
			count = $(this).children('li').length;				
		});
		
		$('a#visibility').click(function()
		{
			var json = '[';
			for(var i=1 ; i<= count; i++)				
			{
			    if($('ul.all3').children('#text'+i).find('input').attr('type') == 'text')
				{
				json = json + '{"portal_setting_text":"'+$('ul.all3 #text'+i).children('input').val()+'",';
			    }
			    
				if($('ul.all3').children('#visibilty_basic'+i).find('input').attr('type') == 'text')
				{
					json = json + '"basic":"'+$('ul.all3 #visibilty_basic'+i).children('input').val()+'",';
				}
				else if($('ul.all3 #visibilty_basic'+i).children('input').attr('type') == 'checkbox')
				{
					if($('ul.all3 #visibilty_basic'+i).children('input').attr('checked') == 'checked'){
						json = json + '"basic":"1",';	
					}else
					{
						json = json + '"basic":"0",';
					}
				}
				if($('ul.all3').children('#visibilty_valid'+i).find('input').attr('type') == 'text')
				{
					json = json + '"validate":"'+$('ul.all3 #visibilty_valid'+i).children('input').val()+'",';
				}
				else if($('ul.all3 #visibilty_valid'+i).children('input').attr('type') == 'checkbox')
				{
					if($('ul.all3 #visibilty_valid'+i).children('input').attr('checked') == 'checked'){
						json = json + '"validate":"1",';	
					}else
					{
						json = json + '"validate":"0",';
					}
				}
				if($('ul.all3').children('#visibilty_pre'+i).find('input').attr('type') == 'text')
				{
					json = json + '"premium":"'+$('ul.all3 #visibilty_pre'+i).children('input').val()+'"';
				}
				else if($('ul.all3 #visibilty_pre'+i).children('input').attr('type') == 'checkbox')
				{
					if($('ul.all3 #visibilty_pre'+i).children('input').attr('checked') == 'checked'){
						json = json + '"premium":"1"';	
					}else
					{
						json = json + '"premium":"0"';
					}
				}				
				json = json + '},';				
			}
			json = json.replace(/,\s*$/,"");
			json = json + ']';
			$('.bar_img_vis').show();
			var formData = 'action=save_profile_setting&data='+json+'&type=visibilty_setting';
			$.ajax({
					type: "POST",
					url: home_path+"/wp-admin/admin-ajax.php",
					data: formData,
					success:function(data)
					{
							$('.bar_img_vis').hide();
							$('#visibility_msg').html('Visibility setting has saved!');
							setTimeout(function(){jQuery('#visibility_msg').fadeOut(7000);}, 7000);	
					}
				});
			
		});
	});
</script>
	<!-- Panel end -->
	<?php 
	$search = $wpdb->get_results("select * from wp_admin_portal_setting where portal_setting_type = 'search setting'");
	?>
	<!-- Panel start -->
		<div class="panel-admin">
	    	<div class="panel-admin-up">
				<div class="col-sm-3">
		    		<h3>ADMIN portal settings </h3>
				</div>
				<!--<div class="col-sm-9 mast-pass">
		    		<div class="col-sm-6">
						<div class="form-group">
			    			<label>Master password</label>
			    			<input type="text" class="form-control">
						</div>
		    		</div>
		    		<div class="col-sm-4">
		    			<a class="arrow-right-btn" href="#"><img src="http://projects.udaantechnologies.com/hedonia_dev/wp-content/themes/twentyfourteen/images/icon-right.png">
		    			</a>
		    		</div>
				</div>-->
	    	</div>
	    	<div class="panel-admin-down">
				<div class="col-sm-12">
		    		<div class="col-sm-5 col-md-5 left-sec">
						<div class="row">
			    			<h3>SEARCH OPTIONS
							<br/>
							<small style="visibility: hidden">&nbsp;</small>
						    </h3>
						    <ul class="list-all all4 profile-setting">
								<li for="profile_settings" id="textserch1"><input type="text" value="<?php echo $search[0]->portal_setting_text ; ?>"></li>
								<li for="profile_settings" id="textserch2"><input type="text" value="<?php echo $search[1]->portal_setting_text ; ?>"></li>
								<li for="profile_settings" id="textserch3"><input type="text" value="<?php echo $search[2]->portal_setting_text ; ?>"></li>
								<li for="profile_settings" id="textserch4"><input type="text" value="<?php echo $search[3]->portal_setting_text ; ?>"></li>
								<li for="profile_settings" id="textserch5"><input type="text" value="<?php echo $search[4]->portal_setting_text ; ?>"></li>
								<li for="profile_settings" id="textserch6"><input type="text" value="<?php echo $search[5]->portal_setting_text ; ?>"></li>
								<li for="profile_settings" id="textserch7"><input type="text" value="<?php echo $search[6]->portal_setting_text ; ?>"></li>
								<li for="profile_settings" id="textserch8"><input type="text" value="<?php echo $search[7]->portal_setting_text ; ?>"></li>
								<li for="profile_settings" id="textserch9"><input type="text" value="<?php echo $search[8]->portal_setting_text ; ?>"></li>
								<li for="profile_settings" id="textserch10"><input type="text" value="<?php echo $search[9]->portal_setting_text ; ?>"></li>
								<li for="profile_settings" id="textserch11"><input type="text" value="<?php echo $search[10]->portal_setting_text ; ?>"></li>
								<li for="profile_settings" id="textserch12"><input type="text" value="<?php echo $search[11]->portal_setting_text ; ?>"></li>
						    </ul>
						</div>			
		    		</div>
		    		<div class="col-sm-7 col-md-7 right-sec">
						<div class="row">
			    			<div class="col-sm-4">
								<h3>BASIC MEMBERS
								    <br/>
								    <small>not validated</small>
								</h3>
								<ul class="list-all all4 basic-mamber">
								    <li for="basic_member" id="basicserch1">
								    	<input id="bsc_sw" type="checkbox" <?php echo ($search[0]->basic == '1') ? "checked" : ""; ?> for="search" value="couples" name="couples" class="checkbox_cus checkSerch">
										<label for="bsc_sw" class="checkbox_label"></label>
								    </li>
								    <li for="basic_member" id="basicserch2">
								    	<input id="bsc_pw" type="checkbox" <?php echo ($search[1]->basic == '1') ? "checked" : ""; ?> for="search" value="couples" name="couples" class="checkbox_cus checkSerch">
										<label for="bsc_pw" class="checkbox_label"></label>
								    </li>
								    <li for="basic_member" id="basicserch3">
								    	<input id="bsc" type="checkbox" <?php echo ($search[2]->basic == '1') ? "checked" : ""; ?> for="search" value="couples" name="couples" class="checkbox_cus checkSerch">
										<label for="bsc" class="checkbox_label"></label>
								    </li>
								    <li for="basic_member" id="basicserch4">
								    	<input id="bsc1" type="checkbox" <?php echo ($search[3]->basic == '1') ? "checked" : ""; ?> for="search" value="couples" name="couples" class="checkbox_cus checkSerch">
										<label for="bsc1" class="checkbox_label"></label>
								    </li>
								    <li for="basic_member" id="basicserch5">
								    	<input id="bsc2" type="checkbox" <?php echo ($search[4]->basic == '1') ? "checked" : ""; ?> for="search" value="couples" name="couples" class="checkbox_cus checkSerch">
										<label for="bsc2" class="checkbox_label"></label>
								    </li>
								    <li for="basic_member" id="basicserch6">
								    	<input id="bsc3" type="checkbox" <?php echo ($search[5]->basic == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkSerch">
										<label for="bsc3" class="checkbox_label"></label>
								    </li>
								    <li for="basic_member" id="basicserch7">
								    	<input id="bsc4" type="checkbox" <?php echo ($search[6]->basic == '1') ? "checked" : ""; ?> for="search" value="couples" name="couples" class="checkbox_cus checkSerch">
										<label for="bsc4" class="checkbox_label"></label>
								    </li>
								    <li for="basic_member" id="basicserch8">
								    	<input id="bsc8" type="checkbox" <?php echo ($search[7]->basic == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkSerch">
										<label for="bsc8" class="checkbox_label"></label>
								    </li>
								    <li for="basic_member" id="basicserch9">
								    	<input id="bsc9" type="checkbox" <?php echo ($search[8]->basic == '1') ? "checked" : ""; ?> for="search" value="couples" name="couples" class="checkbox_cus checkSerch">
										<label for="bsc9" class="checkbox_label"></label>
								    </li>
								    <li for="basic_member" id="basicserch10">
								    	<input id="bsc10" type="checkbox" <?php echo ($search[9]->basic == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkSerch">
										<label for="bsc10" class="checkbox_label"></label>
								    </li>
								    <li for="basic_member" id="basicserch11">
								    	<input id="bsc11" type="checkbox" <?php echo ($search[10]->basic == '1') ? "checked" : ""; ?> for="search" value="couples" name="couples" class="checkbox_cus checkSerch">
										<label for="bsc11" class="checkbox_label"></label>
								    </li>
								      <li for="basic_member" id="basicserch12">
								    	<input id="bsc12" type="checkbox" <?php echo ($search[11]->basic == '1') ? "checked" : ""; ?> for="search" value="couples" name="couples" class="checkbox_cus checkSerch">
										<label for="bsc12" class="checkbox_label"></label>
								    </li>
								</ul>
			    			</div>
						    <div class="col-sm-4">
								<h3>BASIC MEMBERS
								    <br/>
								    <small>validated</small>
								</h3>
								<ul class="list-all all4 basic-mamb-valid">
								    <li for="basic_member_valid" id="validateserch1">
									<input id="smokings" type="checkbox" <?php echo ($search[0]->validated == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkSerch">
									<label for="smokings" class="checkbox_label"></label>
								    </li>
								    <li for="basic_member_valid" id="validateserch2">
									<input id="smoking" type="checkbox" <?php echo ($search[1]->validated == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkSerch">
									<label for="smoking" class="checkbox_label"></label>
								    </li>
								    <li for="basic_member_valid" id="validateserch3">
									<input id="criteria" type="checkbox" <?php echo ($search[2]->validated == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkSerch">
									<label for="criteria" class="checkbox_label"></label>
								    </li>
								    <li for="basic_member_valid" id="validateserch4">
									<input id="age" type="checkbox" <?php echo ($search[3]->validated == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkSerch">
									<label for="age" class="checkbox_label"></label>
								    </li>
								    <li for="basic_member_valid" id="validateserch5">
									<input id="members" type="checkbox" <?php echo ($search[4]->validated == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkSerch">
									<label for="members" class="checkbox_label"></label>
								    </li>
								    <li for="basic_member_valid" id="validateserch6">
								    	<input id="members12" type="checkbox" <?php echo ($search[5]->validated == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkSerch">
										<label for="members12" class="checkbox_label"></label>
								    </li>
								    <li for="basic_member_valid" id="validateserch7">
								    	<input id="members23" type="checkbox" <?php echo ($search[6]->validated == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkSerch">
										<label for="members23" class="checkbox_label"></label>
								    </li>
								    <li for="basic_member_valid" id="validateserch8">
								    	<input id="members_p" type="checkbox" <?php echo ($search[7]->validated == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkSerch">
										<label for="members_p" class="checkbox_label"></label>
								    </li>
								    <li for="basic_member_valid" id="validateserch9">
								    	<input id="members_val" type="checkbox" <?php echo ($search[8]->validated == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkSerch">
										<label for="members_val" class="checkbox_label"></label>
								    </li>
								    <li for="basic_member_valid" id="validateserch10">
								    	<input id="members_0" type="checkbox" <?php echo ($search[9]->validated == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkSerch">
										<label for="members_0" class="checkbox_label"></label>
								    </li>
								    <li for="basic_member_valid" id="validateserch11">
								    	<input id="member_p3" type="checkbox" <?php echo ($search[10]->validated == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkSerch">
										<label for="member_p3" class="checkbox_label"></label>
								    </li>
								    <li for="basic_member_valid" id="validateserch12">
								    	<input id="member_pi3" type="checkbox" <?php echo ($search[11]->validated == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkSerch">
										<label for="member_pi3" class="checkbox_label"></label>
								    </li>
								</ul>
						    </div>
			    			<div class="col-sm-4">
								<h3 class="primeum-mamb">PREMIUM MEMBERS
								    <br/>
								    <small>validated + paying</small>
								</h3>
								<ul class="list-all all4 primium-mamber">
								 	<li for="premium_member" id="preserch1">
									<input id="married12" type="checkbox" <?php echo ($search[0]->premium == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkSerch">
									<label for="married12" class="checkbox_label"></label>
								    </li>
								    <li for="premium_member" id="preserch2">
									<input id="married" type="checkbox" <?php echo ($search[1]->premium == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkSerch">
									<label for="married" class="checkbox_label"></label>
								    </li>
								    <li for="premium_member" id="preserch3">
									<input id="single" type="checkbox" <?php echo ($search[2]->premium == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkSerch">
									<label for="single" class="checkbox_label"></label>
								    </li>
								    <li for="premium_member" id="preserch4">
									<input id="status" type="checkbox" <?php echo ($search[3]->premium == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkSerch">
									<label for="status" class="checkbox_label"></label>
								    </li>
								    <li for="premium_member" id="preserch5">
									<input id="intension" type="checkbox" <?php echo ($search[4]->premium == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkSerch">
									<label for="intension" class="checkbox_label"></label>
								    </li>
								    <li for="premium_member" id="preserch6">
									<input id="playroom" type="checkbox" <?php echo ($search[5]->premium == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkSerch">
									<label for="playroom" class="checkbox_label"></label>
								    </li>
								    <li for="premium_member" id="preserch7">
									<input id="possibilities" type="checkbox" <?php echo ($search[6]->premium == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkSerch">
									<label for="possibilities" class="checkbox_label"></label>
								    </li>
								    <li for="premium_member" id="preserch8">
									<input id="possibilities8" type="checkbox" <?php echo ($search[7]->premium == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkSerch">
									<label for="possibilities8" class="checkbox_label"></label>
								    </li>
								    <li for="premium_member" id="preserch9">
									<input id="possibilities9" type="checkbox" <?php echo ($search[8]->premium == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkSerch">
									<label for="possibilities9" class="checkbox_label"></label>
								    </li>
								    <li for="premium_member" id="preserch10">
									<input id="possibilities10" type="checkbox" <?php echo ($search[9]->premium == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkSerch">
									<label for="possibilities10" class="checkbox_label"></label>
								    </li>
								    <li for="premium_member" id="preserch11">
									<input id="possibilities11" type="checkbox" <?php echo ($search[10]->premium == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkSerch">
									<label for="possibilities11" class="checkbox_label"></label>
								    </li>
								    <li for="premium_member" id="preserch12">
									<input id="possibilities12" type="checkbox" <?php echo ($search[11]->premium == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkSerch">
									<label for="possibilities12" class="checkbox_label"></label>
								    </li>
								</ul>
			    			</div>
			    			<a href="javascript:void(0)" id="search_options" class="btn-save pull-right">save</a>
			    			<span class="bar_img_search" style="display: none;float:right">
                                <img width="60px" height="12px" src="<?php echo $home_path; ?>/wp-content/themes/twentyfourteen/images/ajax-status.gif">
                            </span>
                            <div id="search_msg" class="successMsg"></div>
						</div>
		    		</div>
				</div>
	    	</div>
		</div>
	<!-- Panel end -->

<script type="text/javascript">
$('.checkSerch').click(function(){ 
    if (this.checked == true)
        {
            $(this).each(function () {
                $(this).attr('checked', 'checked');
            });
        } else {
            $(this).each(function () {
                $(this).attr('checked', false);
            });
        } 
});
</script>
<script>
	$(document).ready(function()
	{
		var home_path='<?php echo $home_path; ?>';
		var count = 0 ;
		$('ul.all4').each(function(){
			count = $(this).children('li').length;				
		});
		$('a#search_options').click(function()
		{
			var json = '[';
			for(var i=1 ; i<= count; i++)				
			{
				if($('ul.all4').children('#textserch'+i).find('input').attr('type') == 'text')
				{
				json = json + '{"portal_setting_text":"'+$('ul.all4 li#textserch'+i).children('input').val()+'",';
			    }
				if($('ul.all4').children('#basicserch'+i).find('input').attr('type') == 'text')
				{
					json = json + '"basic":"'+$('ul.all4 #basicserch'+i).children('input').val()+'",';
				}
				else if($('ul.all4 #basicserch'+i).children('input').attr('type') == 'checkbox')
				{
					if($('ul.all4 #basicserch'+i).children('input').attr('checked') == 'checked'){
						json = json + '"basic":"1",';	
					}else
					{
						json = json + '"basic":"0",';
					}
				}
				if($('ul.all4').children('#validateserch'+i).find('input').attr('type') == 'text')
				{
					json = json + '"validate":"'+$('ul.all4 #validateserch'+i).children('input').val()+'",';
				}
				else if($('ul.all4 #validateserch'+i).children('input').attr('type') == 'checkbox')
				{
					if($('ul.all4 #validateserch'+i).children('input').attr('checked') == 'checked'){
						json = json + '"validate":"1",';	
					}else
					{
						json = json + '"validate":"0",';
					}
				}
				if($('ul.all4').children('#preserch'+i).find('input').attr('type') == 'text')
				{
					json = json + '"premium":"'+$('ul.all4 #preserch'+i).children('input').val()+'"';
				}
				else if($('ul.all4 #preserch'+i).children('input').attr('type') == 'checkbox')
				{
					if($('ul.all4 #preserch'+i).children('input').attr('checked') == 'checked'){
						json = json + '"premium":"1"';	
					}else
					{
						json = json + '"premium":"0"';
					}
				}				
				json = json + '},';				
			}
			json = json.replace(/,\s*$/,"");
			json = json + ']';	
			$('.bar_img_search').show();		
			var formData = 'action=save_profile_setting&data='+json+'&type=search_setting';
			$.ajax({
					type: "POST",
					url: home_path+"/wp-admin/admin-ajax.php",
					data: formData,
					success:function(data)
					{
							$('.bar_img_search').hide();
							$('#search_msg').html('Search setting has saved!');
							setTimeout(function(){jQuery('#search_msg').fadeOut(7000);}, 7000);	
					}
				});
			
		});
	});
</script>
		<!-- Panel start -->
		<?php 
			$operational = $wpdb->get_results("select * from wp_admin_portal_setting where portal_setting_type = 'operational setting'");
		?>
		<div class="panel-admin">
	    	<div class="panel-admin-up">
				<div class="col-sm-3">
		    		<h3>ADMIN portal settings </h3>
				</div>
				<!--<div class="col-sm-9 mast-pass">
		    		<div class="col-sm-6">
						<div class="form-group">
			    			<label>Master password</label>
			    			<input type="text" class="form-control">
						</div>
		    		</div>
		    		<div class="col-sm-4">
		    			<a class="arrow-right-btn" href="#"><img src="http://projects.udaantechnologies.com/hedonia_dev/wp-content/themes/twentyfourteen/images/icon-right.png">
		    			</a>
		    		</div>
				</div>-->
	    	</div>
	    	<div class="panel-admin-down">
				<div class="col-sm-12">
		    		<div class="col-sm-5 col-md-5 left-sec">
						<div class="row">
			    			<h3>OPERATIONAL FEATURES
							<br/>
							<small style="visibility: hidden">&nbsp;</small>
						    </h3>
						    <ul class="list-all all5 profile-setting">
								<li for="profile_settings" id="textserch1"><input type="text" value="<?php echo $operational[0]->portal_setting_text ; ?>"></li>
								<li for="profile_settings" id="textserch2"><input type="text" value="<?php echo $operational[1]->portal_setting_text ; ?>"></li>
								<li for="profile_settings" id="textserch3"><input type="text" value="<?php echo $operational[2]->portal_setting_text ; ?>"></li>
								<li for="profile_settings" id="textserch4"><input type="text" value="<?php echo $operational[3]->portal_setting_text ; ?>"></li>
								<li for="profile_settings" id="textserch5"><input type="text" value="<?php echo $operational[4]->portal_setting_text ; ?>"></li>
								<li for="profile_settings" id="textserch6"><input type="text" value="<?php echo $operational[5]->portal_setting_text ; ?>"></li>
								<li for="profile_settings" id="textserch7"><input type="text" value="<?php echo $operational[6]->portal_setting_text ; ?>"></li>
								<li for="profile_settings" id="textserch8"><input type="text" value="<?php echo $operational[7]->portal_setting_text ; ?>"></li>
								<li style="display:none" for="profile_settings" id="textserch9"><input type="text" value="<?php echo $operational[8]->portal_setting_text ; ?>"></li>
								<li for="profile_settings" id="textserch10"><input type="text" value="<?php echo $operational[9]->portal_setting_text ; ?>"></li>
								<li for="profile_settings" id="textserch11"><input type="text" value="<?php echo $operational[10]->portal_setting_text ; ?>"></li>
								<li for="profile_settings" id="textserch12"><input type="text" value="<?php echo $operational[11]->portal_setting_text ; ?>"></li>
								<li for="profile_settings" id="textserch13"><input type="text" value="<?php echo $operational[12]->portal_setting_text ; ?>"></li>
						    </ul>
						</div>			
		    		</div>
		    		<div class="col-sm-7 col-md-7 right-sec">
						<div class="row">
			    			<div class="col-sm-4">
								<h3>BASIC MEMBERS
								    <br/>
								    <small>not validated</small>
								</h3>
								<ul class="list-all all5 basic-mamber">
								    <li for="basic_member" id="opfeature1">
								    	<input type="text" value="<?php echo $operational[0]->basic; ?>">
								    </li>
								    <li for="basic_member" id="opfeature2">
								    	<input id="bsc_pw1" type="checkbox" <?php echo ($operational[1]->basic == '1') ? "checked" : ""; ?> for="search" value="couples" name="couples" class="checkbox_cus checkOpr">
										<label for="bsc_pw1" class="checkbox_label"></label>
								    </li>
								    <li for="basic_member" id="opfeature3">
								    	<input type="text" value="<?php echo $operational[2]->basic; ?>">
								    </li>
								    <li for="basic_member" id="opfeature4">
								    	<input id="bsc01" type="checkbox" <?php echo ($operational[3]->basic == '1') ? "checked" : ""; ?> for="searc" value="couples" name="couples" class="checkbox_cus checkOpr">
										<label for="bsc01" class="checkbox_label"></label>
								    </li>
								    <li for="basic_member" id="opfeature5">
								    	<input id="bsc21" type="checkbox" <?php echo ($operational[4]->basic == '1') ? "checked" : ""; ?> for="searc" value="couples" name="couples" class="checkbox_cus checkOpr">
										<label for="bsc21" class="checkbox_label"></label>
								    </li>
								    <li for="basic_member" id="opfeature6">
								    	<input id="bsc31" type="checkbox" <?php echo ($operational[5]->basic == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkOpr">
										<label for="bsc31" class="checkbox_label"></label>
								    </li>
								    <li for="basic_member" id="opfeature7">
								    	<input id="bsc41" type="checkbox" <?php echo ($operational[6]->basic == '1') ? "checked" : ""; ?> for="searc" value="couples" name="couples" class="checkbox_cus checkOpr">
										<label for="bsc41" class="checkbox_label"></label>
								    </li>
								    <li for="basic_member" id="opfeature8">
								    	<input id="bsc81" type="checkbox" <?php echo ($operational[7]->basic == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkOpr">
										<label for="bsc81" class="checkbox_label"></label>
								    </li>
								    <li style="display:none" for="basic_member" id="opfeature9">
								    	<input id="bsc91" type="checkbox" <?php echo ($operational[8]->basic == '1') ? "checked" : ""; ?> for="searc" value="couples" name="couples" class="checkbox_cus checkOpr">
										<label for="bsc91" class="checkbox_label"></label>
								    </li>
								    <li for="basic_member" id="opfeature10">
								    	<input id="bsc101" type="checkbox" <?php echo ($operational[9]->basic == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkOpr">
										<label for="bsc101" class="checkbox_label"></label>
								    </li>
								    <li for="basic_member" id="opfeature11">
								    	<input id="bsc111" type="checkbox" <?php echo ($operational[10]->basic == '1') ? "checked" : ""; ?> for="searc" value="couples" name="couples" class="checkbox_cus checkOpr">
										<label for="bsc111" class="checkbox_label"></label>
								    </li>
								      <li for="basic_member" id="opfeature12">
								    	<input id="bsc121" type="checkbox" <?php echo ($operational[11]->basic == '1') ? "checked" : ""; ?> for="searc" value="couples" name="couples" class="checkbox_cus checkOpr">
										<label for="bsc121" class="checkbox_label"></label>
								    </li>
								    <li for="basic_member" id="opfeature13">
								    	<input type="text" value="<?php echo $operational[12]->basic; ?>">
								    </li>
								</ul>
			    			</div>
						    <div class="col-sm-4">
								<h3>BASIC MEMBERS
								    <br/>
								    <small>validated</small>
								</h3>
								<ul class="list-all all5 basic-mamb-valid">
								    <li for="basic_member_valid" id="validateopftr1">
									<input type="text" value="<?php echo $operational[0]->validated; ?>">
								    </li>
								    <li for="basic_member_valid" id="validateopftr2">
									<input id="smoking1" type="checkbox" <?php echo ($operational[1]->validated == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkOpr">
									<label for="smoking1" class="checkbox_label"></label>
								    </li>
								    <li for="basic_member_valid" id="validateopftr3">
									<input type="text" value="<?php echo $operational[2]->validated; ?>">
								    </li>
								    <li for="basic_member_valid" id="validateopftr4">
									<input id="age1" type="checkbox" <?php echo ($operational[3]->validated == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkOpr">
									<label for="age1" class="checkbox_label"></label>
								    </li>
								    <li for="basic_member_valid" id="validateopftr5">
									<input id="members1" type="checkbox" <?php echo ($operational[4]->validated == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkOpr">
									<label for="members1" class="checkbox_label"></label>
								    </li>
								    <li for="basic_member_valid" id="validateopftr6">
								    	<input id="members121" type="checkbox" <?php echo ($operational[5]->validated == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkOpr">
										<label for="members121" class="checkbox_label"></label>
								    </li>
								    <li for="basic_member_valid" id="validateopftr7">
								    	<input id="members231" type="checkbox" <?php echo ($operational[6]->validated == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkOpr">
										<label for="members231" class="checkbox_label"></label>
								    </li>
								    <li for="basic_member_valid" id="validateopftr8">
								    	<input id="members_p1" type="checkbox" <?php echo ($operational[7]->validated == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkOpr">
										<label for="members_p1" class="checkbox_label"></label>
								    </li>
								    <li style="display:none" for="basic_member_valid" id="validateopftr9">
								    	<input id="members_val1" type="checkbox" <?php echo ($operational[8]->validated == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkOpr">
										<label for="members_val1" class="checkbox_label"></label>
								    </li>
								    <li for="basic_member_valid" id="validateopftr10">
								    	<input id="members_01" type="checkbox" <?php echo ($operational[9]->validated == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkOpr">
										<label for="members_01" class="checkbox_label"></label>
								    </li>
								    <li for="basic_member_valid" id="validateopftr11">
								    	<input id="member_p31" type="checkbox" <?php echo ($operational[10]->validated == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkOpr">
										<label for="member_p31" class="checkbox_label"></label>
								    </li>
								    <li for="basic_member_valid" id="validateopftr12">
								    	<input id="member_pi31" type="checkbox" <?php echo ($operational[11]->validated == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkOpr">
										<label for="member_pi31" class="checkbox_label"></label>
								    </li>
								    <li for="basic_member_valid" id="validateopftr13">
								    	<input type="text" value="<?php echo $operational[12]->validated; ?>">
								    </li>
								</ul>
						    </div>
			    			<div class="col-sm-4">
								<h3 class="primeum-mamb">PREMIUM MEMBERS
								    <br/>
								    <small>validated + paying</small>
								</h3>
								<ul class="list-all all5 primium-mamber">
								 	<li for="premium_member" id="preoprserch1">
									<input type="text" value="<?php echo $operational[0]->premium; ?>">
								    </li>
								    <li for="premium_member" id="preoprserch2">
									<input id="married121" type="checkbox" <?php echo ($operational[1]->premium == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkOpr">
									<label for="married121" class="checkbox_label"></label>
								    </li>
								    <li for="premium_member" id="preoprserch3">
									<input type="text" value="<?php echo $operational[2]->premium; ?>">
								    </li>
								    <li for="premium_member" id="preoprserch4">
									<input id="status12" type="checkbox" <?php echo ($operational[3]->premium == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkOpr">
									<label for="status12" class="checkbox_label"></label>
								    </li>
								    <li for="premium_member" id="preoprserch5">
									<input id="intension12" type="checkbox" <?php echo ($operational[4]->premium == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkOpr">
									<label for="intension12" class="checkbox_label"></label>
								    </li>
								    <li for="premium_member" id="preoprserch6">
									<input id="playroom12" type="checkbox" <?php echo ($operational[5]->premium == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkOpr">
									<label for="playroom12" class="checkbox_label"></label>
								    </li>
								    <li for="premium_member" id="preoprserch7">
									<input id="possibilities121" type="checkbox" <?php echo ($operational[6]->premium == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkOpr">
									<label for="possibilities121" class="checkbox_label"></label>
								    </li>
								    <li for="premium_member" id="preoprserch8">
									<input id="possibilities81" type="checkbox" <?php echo ($operational[7]->premium == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkOpr">
									<label for="possibilities81" class="checkbox_label"></label>
								    </li>
								    <li style="display:none" for="premium_member" id="preoprserch9">
									<input id="possibilities91" type="checkbox" <?php echo ($operational[8]->premium == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkOpr">
									<label for="possibilities91" class="checkbox_label"></label>
								    </li>
								    <li for="premium_member" id="preoprserch10">
									<input id="possibilities101" type="checkbox" <?php echo ($operational[9]->premium == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkOpr">
									<label for="possibilities101" class="checkbox_label"></label>
								    </li>
								    <li for="premium_member" id="preoprserch11">
									<input id="possibilities111" type="checkbox" <?php echo ($operational[10]->premium == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkOpr">
									<label for="possibilities111" class="checkbox_label"></label>
								    </li>
								    <li for="premium_member" id="preoprserch12">
									<input id="possibilities112" type="checkbox" <?php echo ($operational[11]->premium == '1') ? "checked" : ""; ?> value="couples" name="couples" class="checkbox_cus checkOpr">
									<label for="possibilities112" class="checkbox_label"></label>
								    </li>
								    <li for="premium_member" id="preoprserch13">
									<input type="text" value="<?php echo $operational[12]->premium; ?>">
								    </li>
								</ul>
			    			</div>
			    			<a href="javascript:void(0)" id="operational_setting" class="btn-save pull-right">save</a>
			    			<span class="bar_img_operational" style="display: none;float:right">
                                <img width="60px" height="12px" src="<?php echo $home_path; ?>/wp-content/themes/twentyfourteen/images/ajax-status.gif">
                            </span>
                            <div id="operational_msg" class="successMsg"></div>
						</div>
		    		</div>
				</div>
	    	</div>
		</div>
	<!-- Panel end -->
		
	</div>
	<?php } else { ?>
	<h2 style="text-align: center;">Unauthorised access</h2>
    <p style="text-align: center; color:#fff;">Please click <a href="<?php echo $home_path; ?>/admin">here</a> to login</p>
	<?php } ?>
</div>

<script type="text/javascript">
$('.checkOpr').click(function(){ 
    if (this.checked == true)
        {
            $(this).each(function () {
                $(this).attr('checked', 'checked');
            });
        } else {
            $(this).each(function () {
                $(this).attr('checked', false);
            });
        } 
});
</script>
<script>
	$(document).ready(function()
	{
		var home_path='<?php echo $home_path; ?>';
		var count = 0 ;
		$('ul.all5').each(function(){
			count = $(this).children('li').length;				
		});
		$('a#operational_setting').click(function()
		{
			var json = '[';
			for(var i=1 ; i<= count; i++)				
			{
				if($('ul.all5').children('#textserch'+i).find('input').attr('type') == 'text')
				{
				json = json + '{"portal_setting_text":"'+$('ul.all5 li#textserch'+i).children('input').val()+'",';
			    }
				if($('ul.all5').children('#opfeature'+i).find('input').attr('type') == 'text')
				{
					json = json + '"basic":"'+$('ul.all5 #opfeature'+i).children('input').val()+'",';
				}
				else if($('ul.all5 #opfeature'+i).children('input').attr('type') == 'checkbox')
				{
					if($('ul.all5 #opfeature'+i).children('input').attr('checked') == 'checked'){
						json = json + '"basic":"1",';	
					}else
					{
						json = json + '"basic":"0",';
					}
				}
				if($('ul.all5').children('#validateopftr'+i).find('input').attr('type') == 'text')
				{
					json = json + '"validate":"'+$('ul.all5 #validateopftr'+i).children('input').val()+'",';
				}
				else if($('ul.all5 #validateopftr'+i).children('input').attr('type') == 'checkbox')
				{
					if($('ul.all5 #validateopftr'+i).children('input').attr('checked') == 'checked'){
						json = json + '"validate":"1",';	
					}else
					{
						json = json + '"validate":"0",';
					}
				}
				if($('ul.all5').children('#preoprserch'+i).find('input').attr('type') == 'text')
				{
					json = json + '"premium":"'+$('ul.all5 #preoprserch'+i).children('input').val()+'"';
				}
				else if($('ul.all5 #preoprserch'+i).children('input').attr('type') == 'checkbox')
				{
					if($('ul.all5 #preoprserch'+i).children('input').attr('checked') == 'checked'){
						json = json + '"premium":"1"';	
					}else
					{
						json = json + '"premium":"0"';
					}
				}				
				json = json + '},';				
			}
			json = json.replace(/,\s*$/,"");
			json = json + ']';	
			$('.bar_img_operational').show();		
			var formData = 'action=save_profile_setting&data='+json+'&type=operational_setting';
			$.ajax({
					type: "POST",
					url: home_path+"/wp-admin/admin-ajax.php",
					data: formData,
					success:function(data)
					{
							$('.bar_img_operational').hide();
							$('#operational_msg').html('Operational setting has saved!');
							setTimeout(function(){jQuery('#operational_msg').fadeOut(7000);}, 7000);	
					}
				});
			
		});
	$('#admin_log_out').click(function(){
            $.ajax({
                type:'POST',
                data:({action:"user_logout_admin_ajax"}),
                url: home_path+"/wp-admin/admin-ajax.php",
                success: function (data)
                {
                    window.location = home_path+"/admin";
                }
            });
        });
	});
</script>






