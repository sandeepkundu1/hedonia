<?php 
$path=$_SERVER['DOCUMENT_ROOT'].'/hedon/';
include_once($path.'wp-config.php');
include_once($path.'wp-load.php');
include_once($path.'wp-includes/wp-db.php');
$home_path=home_url();
?>
<link rel="stylesheet" type="text/css" href="<?php echo $home_path; ?>/wp-content/themes/twentyfourteen/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="<?php echo $home_path; ?>/wp-content/themes/twentyfourteen/css/style.css">
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo $home_path; ?>/wp-content/themes/twentyfourteen/js/bootstrap.js"></script>
<div class="lostPwd">
    <form>
        <div class="loginHead">
            <a class="navbar-brand" href="<?php echo $home_path; ?>">
                <img class="img-responsive" alt="logo" src="<?php echo $home_path; ?>/wp-content/themes/twentyfourteen/images/logo.jpg">
            </a>
        </div>
        <div class="error_msg_box error-msg text-center" id="error_msg_box"></div>
        <div class="form-group">
            <label>User Type</label>
            <select class="form-control select_user_type">
                <option>Select</option>
                <option value="master">Master</option>
                <option value="operator">Operation</option>
                <option value="moderator">Moderator</option>
            </select>
        </div>
        <div class="form-group">
            <label>User Name</label>
            <input type="text" for="check" name="user_name" id="user_name" class="user_name form-control">
        </div>
        <div class="form-group">
            <label>Password</label>
            <input type="password" for="check" name="user_password" id="user_password" class="user_password form-control">
        </div>        
        <div class="form-group">
            <a href="javascript:void(0)" class="btn btn-success btn-sm signupBtn" id="submit_login">Submit</a>
            <span class="bar_img4" style="display: none;float:right">
                <img width="60px" height="12px" src="<?php echo $home_path; ?>/wp-content/themes/twentyfourteen/images/ajax-status.gif">
            </span>
        </div>
    </form>
</div>
<script>
    jQuery(document).ready(function () {
        jQuery('#submit_login').click(function () {
            var hint = 0;
            var home_url='<?php echo $home_path; ?>';
            $('input[for="check"]').each(function ()
            {
                if (jQuery(this).val() == "")
                {
                    jQuery(this).css("border-color", "red");
                    hint = 1;
                }
            });
            if (hint == 0)
            {
                jQuery('.bar_img4').show();
                var name = jQuery('#user_name').val();
                var pass = jQuery('#user_password').val();
                var user_type=jQuery('.select_user_type').val();
                jQuery.ajax({
                    type: "POST",
                    url: home_url+"/admin/index_service.php",
                    data: "name=" + name + "&pass=" + pass,
                    success: function (data)
                    {
                        var tm_data = data.trim();
                        jQuery('.bar_img4').hide();
                        if (tm_data == 'operator')
                        {
                            window.location = home_url+"/admin/admin-panel_member_status.php";
                        }
                        else if (tm_data == 'master')
                        {
                            window.location = home_url+"/admin/admin-portal-setting.php";
                        }
                        else if (tm_data == 'moderator')
                        {
                            window.location = home_url+"/admin/admin-profile-text-release.php";
                        }
                        else if(tm_data!='administrator')
                        {
                            jQuery('#error_msg_box').html(tm_data);
                        }
                        if(tm_data == 'administrator' && user_type=='operator')
                        {
                            window.location = home_url+"/admin/admin-panel_member_status.php/?user=admin";
                        }
                        if(tm_data == 'administrator' && user_type=='moderator')
                        {
                            window.location = home_url+"/admin/admin-profile-text-release.php/?user=admin";
                        }
                        if(tm_data == 'administrator' && user_type=='master')
                        {
                            window.location = home_url+"/admin/admin-portal-setting.php/?user=admin";
                        }
                    }
                });
            }
        });
    });
</script>
