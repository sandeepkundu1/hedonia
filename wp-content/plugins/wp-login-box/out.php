﻿<?php
if (!function_exists('curPageURL')) {
function curPageURL() {
    $pageURL = 'http';
    if ($_SERVER["HTTPS"] == "on") {
	    $pageURL .= "s";
    }
    $pageURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
    } else {
        $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
    }
    return $pageURL;
}
}

$data = get_option('wplb_options');
$home = site_url();
$redirect = $home."/index.php";
if ($data['inred'] != "") {
    $redirect = $data['inred'];
}

$rme = "0";
if ($data['rme'] != "") {
	$rme = $data['rme'];
}

$reglink = $home.'/wp-signup.php';
if ($data['reglink'] != "") {
	$reglink = $data['reglink'];
}

$passlink = $home.'/wp-login.php?action=lostpassword';
if ($data['passlink'] != "") {
	$passlink = $data['passlink'];
}

$remembermetext = "Remember<br /> me?";
if ($data['uremembermetext'] != "") {
    $remembermetext = $data['uremembermetext'];
}

$logintext = "Login";
if ($data['ulogintext'] != "") {
    $logintext = $data['ulogintext'];
}

$registertext = "Register";
if ($data['uregistertext'] != "") {
    $registertext = $data['uregistertext'];
}

$forgottext = "Forgot password?";
if ($data['uforgottext'] != "") {
    $forgottext = $data['uforgottext'];
}

$extra = "";
if (isset($_GET['failed_login'])) {
	$extra = "style = \"color: red;\"";
}

$blog_url = site_url();
?>
<div id='wplb_wrap' style='float: <?php echo $data['float']; ?>;'>
<div id='wplb_main' >
<form name='loginform' id='loginform' action='<?php echo $home; ?>/wp-login.php' method='post' onsubmit="return validateForm();">
    
        <div class="form-group">
                <label for="inputEmail3" class="col-sm-4 control-label text-left">Profile name</label>
            <div class="col-sm-5">
                <input type='text' class="form-control " name='log' id='user_login' <?php echo $extra; ?> value=''  />
                <div class="error1" style="display:none">Please fill user name</div>
            </div>
        </div>
    
        <div class="form-group">
                <label for="inputEmail3" class="col-sm-4 control-label text-left">Password</label>
            <div class="col-sm-5">
                <input type='password' class='form-control ' <?php echo $extra; ?> name='pwd' id='user_pass' value=''  />
                <div class="error2" style="display:none">Please fill password </div>
            </div>
        </div>
        <div class="form-group">
                <div class="col-sm-4 col-sm-offset-2">
					<?php 
                        if ($data['register'] == "1") {
                        	echo "<div class='checkbox'><a class='forgotPwd' href='".$reglink."'>".$registertext."</a></div>\n";
                        }
                        if ($data['forgot'] == "1") {
                        	echo "<div class='checkbox'><a class='forgotPwd' href='http://hedon.trendhosting.ch/?page_id=650'>".$forgottext."</a></div>\n";
                        }
                        ?>					
					
					<?php if($rme != "1") {?>
                    <input name='rememberme' class='wplb_check' type='checkbox' id='rememberme' value='forever' /> 
                        <span class='wplb_text'><?php echo $remembermetext; ?></span>
                    <?php } ?></div>
                    <div class="col-sm-3">
                        <input type='submit' name='wp-submit' class='btn btn-success btn-sm signinBtn' id='wp-submit'  value='<?php echo $logintext; ?>'  />
                    </div>
                </div>   
   

        <input type="hidden" name="curl" value="<?php echo curPageURL(); ?>" />
		<input type='hidden' name='redirect_to' value='<?php echo $redirect; ?>' />
		<input type='hidden' name='testcookie' value='1' />
		
		
</form>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  $("#wp-submit").click(function(){
	  var user_name = $("#user_login").val();
	  var email     = $("#user_pass").val();
	  if(user_name==""||user_name==null) 
	  {
		  $(".error1").show();
		  return false;
		  }
		  else if(email==""||email==null)
		  {
			  $(".error2").show();
			   return false;
			  }
			  else
			  {
				   return true;
				  }
    
  });
  
  $("#user_login").keypress(function(){
    $(".error1").hide()
    
  });
  
  $("#user_pass").keypress(function(){
    $(".error2").hide()
    
  });  
});

</script>

</div>

</div>
