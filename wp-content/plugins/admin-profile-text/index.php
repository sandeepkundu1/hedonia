<?php
/*
  Plugin Name: Member Profile  Text Release
  Plugin URI: http://www.udaantechnologies.com
  Description: Plugin to order portfolio
  Author: Gireesh Kumar
  Version: 1.0
  Author URI: http://www.udaantechnologies.com
 */


#.....code Add Admin Menu
require_once($_SERVER['DOCUMENT_ROOT'].'/hedon' . '/smtp/newsmtp/classes/class.phpmailer.php');

add_action('admin_menu', 'wp_admin_post_order');

function wp_admin_post_order() {
    add_menu_page('Manage Profile', 'Member Profile Text Release', 'edit_posts', 'member-list', 'wp_order_list');
    // Add to submenu
    add_submenu_page('member-list', __('Members Validate With Images'), __('Members Validate With Images'), 'edit_themes', 'validate_member', 'my_submenu_render');
     // Add to submenu
    add_submenu_page('member-list', __('Members Validate With Code'), __('Members Validate With Code'), 'edit_themes', 'validate_code', 'my_validated_code_member');
}

#===== Include CSS and rs
add_action('init', 'wp_portfolio_addcss');

function wp_portfolio_addcss() {
    wp_enqueue_style('style.css', '/' . PLUGINDIR . '/admin-profile-text/css/style-member.css');
    wp_enqueue_style('lightbox.css', '/' . PLUGINDIR . '/wp-jquery-lightbox/styles/lightbox.css');
    //wp_enqueue_script('mootool.js', '/' . PLUGINDIR . '/portfolio-order/js/mootool.js');
    wp_enqueue_script('jquery.lightbox.min.js', '/' . PLUGINDIR . '/wp-jquery-lightbox/jquery.lightbox.min.js');
    wp_enqueue_script('jquery.touchwipe.min.js', '/' . PLUGINDIR . '/wp-jquery-lightbox/jquery.touchwipe.min.js');
}

#========== Function to Set Portfolio Order

function wp_order_list() {
    global $wpdb;
?>

<div class="member-admin-page">
    <div class="col-md-12 heading-bar">
        <h2 class="heading-title">ADMIN PANEL for members status</h2>
        <div class="left-pass-input">
        <!--<input  type="text" class="pass-input"/>
        <input type="submit" class="submit-btn"/>-->
        </div>
    </div>

    <!-- admin Profile validation -->
    <div class="clearfix"></div>
        <hr/>
        <div class="panel-title">ADMIN Profile Text Release</div>
            <div class="panel">
                <table width="100%">
                    <th align="left" width="50%" style="font-size: 18px; padding-bottom: 10px;">Profile Text</th>
                    <th align="left" width="50%" style="font-size: 18px; padding-bottom: 10px;">Profile Images</th>
                    <tr>
                        <td valign="top">
                            <?php
                            $args = array(
                             'orderby' => 'ID',
                             'order'        => 'DESC',
    'meta_query' => array(
    
        array(
            'key' => 'approved_as_basic',
            'value' => 'no',
            'compare' => '='
        ),
        array(
            'key' => 'user_type',
            'value' => 'basic',
            'compare' => '='
        )
    )
);
                                $blogusers = get_users($args);
                                function getInitials($name)
                                {
                                    //split name using spaces
                                    $words=explode(" ",$name);
                                    $inits='';
                                    //loop through array extracting initial letters
                                    foreach($words as $word){
                                        $inits.=strtoupper(substr($word,0,1));
                                        }
                                    return $inits; 
                                }
                                $currval = "";

                            ?>  
                            <select class="form-control member-select" name="username_text" id="username_text" style="margin-bottom:10px;">
                                <option>Select Member</option>
                                <?php
                                    // Array of WP_User objects.
                                    foreach ($blogusers as $user) 
                                    {
                                        $initial = getInitials($user->user_login);
                                        /*if($initial!='' && $currval != $initial) {
                                            $currval = $initial;
                                            echo '<optgroup label="'.$initial.'"></option>';
                                        }*/
                                ?>
                                    <option value="<?php echo $user->ID; ?>">
                                        <?php echo '<span>' . esc_html(trim($user->user_login)) . '</span>'.'<span class="regdate" style="padding-left:10px;padding-right:5px;">&nbsp&nbspRegister Date : </span>'.'<span class="date_u" style="padding-left:5px;">' . esc_html($user->user_registered) . '</span>'; ?>
                                    </option>
                                <?php } ?>
                            </select>
                            <textarea name="usertext" id="usertext" style="width:90%;height:200px; border: 0 none;"></textarea> 
                            <table width="">
                            <tr>
                                <td>We refuse your application because of the following  reasons(s):</td>
                                <td width="50"></td>
                            </tr>
                            <tr>
                                <td>Your introduction is too short</td>
                                <td width="50"><input type="checkbox" value="Your introduction is too short" class="check_resaon" /></td>
                            </tr>
                            <tr>
                                <td>Your introduction is too vulgar</td>
                                <td width="50"><input type="checkbox" value="Your introduction is too vulgar" class="check_resaon" /></td>
                            </tr>
                            <tr>
                                <td>Your introduction is too meaningfull</td>
                                <td width="50"><input type="checkbox" value="Your introduction is too meaningfull" class="check_resaon" /></td>
                            </tr>
                            <tr>
                                <td style="color:#d30000;">Your introduction is against low</td>
                                <td width="50"><input type="checkbox" value="Your introduction is against low" class="check_resaon" /></td>
                            </tr>
                            <tr>
                                <td>Your introduction is against our philosophy and code of behaviour</td>
                                <td width="50"><input type="checkbox" value="Your introduction is against our philosophy and code of behaviour" value="" class="check_resaon" /></td>
                            </tr>
                            <tr>
                                <td>Your profile contains email/ phone number/ URL</td>
                                <td width="50"><input type="checkbox" value="Your profile contains email/ phone number/ URL" class="check_resaon" /></td>
                            </tr>
                            <tr>
                                <td>Your profile picture(s) are pornographic or inappropriate </td>
                                <td width="50"><input type="checkbox" value="Your profile picture(s) are pornographic or inappropriate" class="check_resaon" /></td>
                            </tr>
                            <tr>
                                <td style="color:#d30000;">Your are obviously not owner of your picture(s)</td>
                                <td width="50"><input type="checkbox" value="Your are obviously not owner of your picture(s)" class="check_resaon" /></td>
                            </tr>
                            <tr>
                                <td style="color:#d30000;">Your profile picture(s) are against low</td>
                                <td width="50"><input type="checkbox" value="Your profile picture(s) are against low" class="check_resaon" /></td>
                            </tr>
                        </table>
                        </td>
                    <td valign="top">
                        <span id="userpic">
                            <a href="#" rel="lightbox" id="her_path">
                                <img id="her_name" src="<?php echo get_template_directory_uri(); ?>/images/images.jpeg" height='150' width='150'>
                            </a>&nbsp;&nbsp;
                            <a href="#" rel="lightbox" id="his_path">
                                <img id="his_name" src="<?php echo get_template_directory_uri(); ?>/images/images.jpeg" height='150' width='150'>
                            </a>
                        </span>
                        <br>
                        <br>
                        <button type="button" class="validate button action"  value="accept" id="accept" style="margin-right: 10px"/> Profile Accepted</button>
                        <button type="button" class="reject button action"  value="reject" id="rejects"/> Rejected</button>
                        <div style="margin-top:15px;text-align:center;">
                            <span class="member_img" style="display:none;">
                                <img height="12px" width="60px" src="<?php echo plugins_url(); ?>/admin-profile-text/ajax-status.gif">
                            </span>
                            <div id="msg" class="successMsg" style="color:yellow!important;text-align: left;"></div>
                        </div>
                    </td>                    
                </tr>
            </table>
        </div>
    </div>
<!-- Profile Validation-->
<script>
    jQuery("#username_text").change(function () {
        var username_text = jQuery("#username_text").val();
        if(username_text =='Select Member')
        {
            jQuery("#msg").html('Please select atleast one user from drop down..!');
            setTimeout(function(){jQuery('#msg').fadeOut(2000);}, 3000);
        }else         
        {
            jQuery.ajax({
                type: 'POST', // Adding Post method
                url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php', // Including ajax file
                data: {"action": "member_panel_check_text", "userid": username_text}, 
                dataType:'json',
                beforeSend : function(){
                    jQuery('.member_img').css('display','block');
                },
                success: function (response) {    
                    jQuery("#usertext").val(response.txt);
                    if(response.pic1 !="")
                    {
                        jQuery("#her_name").attr('src',response.pic1);
                        jQuery("#her_path").attr('href',response.pic1);
                    }
                    if(response.pic2 !="")
                    {
                        jQuery("#his_name").attr('src',response.pic2);
                        jQuery("#his_path").attr('href',response.pic2);
                    }
                },
                error: function() {
                    alert('failed');
                },
                complete: function() {
                    jQuery('.member_img').css('display','none');
                }
            });
        }
    });
</script>
<!-- Profile Accept-->
<script>
    jQuery("#accept").click(function () {
        var accept = jQuery("#accept").val();    
        var userid = jQuery("#username_text").val();   
        if(userid =='Select Member')
        {
            jQuery("#msg").html('Please select atleast one user from drop down..!');
            setTimeout(function(){jQuery('#msg').fadeOut(2000);}, 3000);
        }else         
        { 
            jQuery.ajax({
                type: 'POST', // Adding Post method
                url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php', 
                data: {"action": "member_panel_check_accept", "userid": userid}, 
                beforeSend : function(){
                    jQuery('.member_img').css('display','block');
                },
                success: function (data) { 
                    jQuery("#msg").show().html(data.slice(0, -1));
                },
                error: function() {
                    alert('failed');
                },
                complete: function() {
                    jQuery('.member_img').css('display','none');
                    setTimeout(function(){jQuery('#msg').fadeOut(2000);}, 3000);
                }
            });
        }
    });
</script>

<!-- Profile Reject-->
<script>
jQuery(document).ready(function(){
    jQuery("#rejects").click(function () {
        var hint = 0;
        var accept = jQuery("#rejects").val();    
        var userid = jQuery("#username_text").val();
        var count =0 ;
        var arr =[]; 
        jQuery('.check_resaon:checked').each(function(){
            count++;      
        });
        if(count == 0){
            hint = 1;
        }
        var json = '{';
        jQuery('.check_resaon:checked').each(function(){
            arr.push(jQuery(this).val()) ;  
        }); 
        json += '}';
        
        if(userid =='Select Member')
        {
            jQuery("#msg").html('Please select atleast one user from drop down..!');
            setTimeout(function(){jQuery('#msg').fadeOut(2000);}, 3000);
        }
        else
        {
            if(hint == 0)
            {
                jQuery.ajax({
                    type: 'POST', 
                    url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php', // Including ajax file
                    data: "action=member_panel_check_reject&userid="+ userid + "&data="+arr, 
                    beforeSend : function(){
                        jQuery('.member_img').css('display','block');
                    },
                    success: function (data) { 
                        jQuery("#msg").show().html(data.slice(0, -1));
                    },
                    error: function() {
                        alert('failed');
                    },
                    complete: function() {
                        jQuery('.member_img').css('display','none');
                        setTimeout(function(){jQuery('#msg').fadeOut(2000);}, 3000);
                        location.reload();
                    }
                });
            }else{
                jQuery("#msg").show().html('Please select atleast one reason..!');
                setTimeout(function(){jQuery('#msg').fadeOut(2000);}, 3000);
            }
        }
    });
});
</script>

<?php
}
#============== display member profile =============================#
add_action('wp_ajax_member_panel_check_text', 'member_panel_check_text'); 
add_action('wp_ajax_nopriv_member_panel_check_text', 'member_panel_check_text'); 

function member_panel_check_text() 
{
    global $wpdb;
    $user_id    = $_POST['userid'];
    $url        = site_url();
    $user_info  = get_userdata($user_id);
   // echo '<pre>';print_r($user_info);die;
    $describe   = trim(strip_tags(get_user_meta($user_id, 'describe', true)));
    $her_pick   = get_user_meta($user_id, 'her_pick', true);
    $his_pick   = get_user_meta($user_id, 'his_pick', true);
    $url        = $url . "/wp-content/uploads/profilepick_" . $user_id . "";

    if ($her_pick == "no_img.jpg" && $his_pick != "no_img.jpg"){
        $hisname = $url . '/' . $his_pick;
    }elseif ($her_pick != "no_img.jpg" && $his_pick == "no_img.jpg"){
        $hername = $url . '/' . $her_pick;
    }elseif ($her_pick != "no_img.jpg" && $his_pick != "no_img.jpg"){
        $hername = $url . '/' . $her_pick;
        $hisname = $url . '/' . $his_pick;
    }elseif ($her_pick){
        $hername = $url . '/' . $her_pick;
    }elseif ($his_pick){
        $hisname = $url . '/' . $his_pick;
    }
    echo json_encode(array('txt'=>$describe ,'pic1'=>$hername,'pic2'=>$hisname));
    die;
}

#==================== accept the user profile =====================#
add_action('wp_ajax_member_panel_check_accept', 'member_panel_check_accept'); 
add_action('wp_ajax_nopriv_member_panel_check_accept', 'member_panel_check_accept'); 

function member_panel_check_accept() 
{
    global $wpdb;
    $user_id = $_POST['userid'];
    $user_by = get_user_by('id', $user_id);
    $user_name = $user_by->data->user_login;
    $user_email = $user_by->data->user_email;
    $user_update = update_user_meta($user_id, 'approved_as_basic', 'yes');

    $email = $user_email;
    $mail = new PHPMailer; // call the class
    $mail->IsSMTP();
    $mail->SMTPSecure = "";
    $mail->SMTPDebug  = 0;  
    $mail->Host = 'ssl://smtp.gmail.com';
    $mail->Port = '465'; //Port of the SMTP like to be 25, 80, 465 or 587
    $mail->SMTPAuth = true; //Whether to use SMTP authentication
    $mail->Username = "theclub.hedon@gmail.com";
    $mail->Password = "testing@theclub";
    $mail->SetFrom("theclub.hedon@gmail.com"); //From address of the mail
    //$mail->SMTPSecure = "tls";
    $mail->Subject = "Confirmation"; //Subject od your mail
    $mail->AddAddress($email, $user_name); //To address who will receive this email
    $patient_message = '<!DOCTYPE html>
                <html>
                    <head>
                        <title>Hendonia</title>
                    </head>
                    <body style="margin: 0;padding: 0;" bgcolor="#000000">
                        <table width="100%" border="0" cellpadding="0" cellsacing="0" align="center" bgcolor="#000000" style="padding-left:20px;">
                            <tr>
                                <td align="left">
                                    <table width="600" border="0" cellpadding="0" cellsacing="0" align="center" style="padding-bottom: 30px;">
                                        <tr>
                                            <td align="left" height="70" valign="middle">
                                                <a href="'.home_url().'" > <img src="'.get_template_directory_uri().'/images/logo.jpg" alt=""/></a>                   
                                            </td>
                                        </tr>
                                        <tr align="left">
                                            <td style="padding-left: 15px; padding-top: 15px; padding-right:15px; padding-bottom: 10px; background-color: #333;">
                                                <table  border="0" cellpadding="0" cellsacing="0" width="100%">
                                                    <tr>
                                                        <td  style="font-family: arial;font-size: 22px; line-height: 20px; color: #fff;">Subscriber !</td>                                        
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr align="left">
                                            <td  style="font-family: arial;font-size: 24px; color: #FE0000; padding-bottom: 10px; padding-top: 30px;">Welcome to Hedonia</td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: arial;font-size: 14px;line-height:20px; color: #fff; ">Hi  '.$user_name.'</td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: arial;font-size: 14px;line-height:20px; color: #fff; ">You have subscribed as a basic member at <a href="'.home_url().'" style="color: #FE0000">Hedonia.ch</a></td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: arial;font-size: 14px;line-height:20px; color: #fff; padding-top: 40px; ">
                                                Regards
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: arial;font-size: 16px;line-height:20px; color: #FE0000; padding-top: 0px; ">
                                                Hedonia
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </body>
                </html>';   
    $mail->MsgHTML($patient_message); //Put your body of the message you can place html code here
    $send = $mail->Send(); //Send the mails
    echo 'You have subscribed as a basic member';
}

#======================= Rject a user =====================================#
add_action('wp_ajax_member_panel_check_reject', 'member_panel_check_reject'); 
add_action('wp_ajax_nopriv_member_panel_check_reject', 'member_panel_check_reject'); 

function member_panel_check_reject() 
{
    global $wpdb;
    $user_id        = $_POST['userid'];    
    $explodeData    = explode(",",$_POST['data']);         
    $user_by        = get_user_by('id', $user_id);
    $user_update    = update_user_meta($user_id, 'approved_as_basic', 'no');
    $user_name      = $user_by->data->user_login;
    $user_email     = $user_by->data->user_email;
    $email          = $user_email;
    $mail = new PHPMailer; // call the class
    $mail->IsSMTP();
    $mail->SMTPSecure = "";
    $mail->SMTPDebug  = 0;  
    $mail->Host = 'ssl://smtp.gmail.com';
    $mail->Port = '465'; //Port of the SMTP like to be 25, 80, 465 or 587
    $mail->SMTPAuth = true; //Whether to use SMTP authentication
    $mail->Username = "theclub.hedon@gmail.com";
    $mail->Password = "testing@theclub";
    //$mail->AddReplyTo("admin@quiconnaitunbon.com.com", "Admin"); //reply-to address
    $mail->SetFrom("theclub.hedon@gmail.com"); //From address of the mail
    //$mail->SMTPSecure = "tls";
    $mail->Subject = "Rejection"; //Subject od your mail
    $mail->AddAddress($email, $user_name); //To address who will receive this email
    $patient_message = '<!DOCTYPE html>
                <html>
                    <head>
                        <title>Hendonia</title>
                    </head>
                    <body style="margin: 0;padding: 0;" bgcolor="#000000">
                        <table width="100%" border="0" cellpadding="0" cellsacing="0" align="center" bgcolor="#000000" style="padding-left:20px;">
                            <tr>
                                <td align="left">
                                    <table width="600" border="0" cellpadding="0" cellsacing="0" align="center" style="padding-bottom: 30px;">
                                        <tr>
                                            <td align="left" height="70" valign="middle">
                                                <a href="'.home_url().'" > <img src="'.get_template_directory_uri().'/images/logo.jpg" alt=""/></a>                   
                                            </td>
                                        </tr>
                                        <tr align="left">
                                            <td style="padding-left: 15px; padding-top: 15px; padding-right:15px; padding-bottom: 10px; background-color: #333;">
                                                <table  border="0" cellpadding="0" cellsacing="0" width="100%">
                                                    <tr>
                                                        <td  style="font-family: arial;font-size: 22px; line-height: 20px; color: #fff;">Reject Profile!</td>                                        
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr align="left">
                                            <td  style="font-family: arial;font-size: 24px; color: #FE0000; padding-bottom: 10px; padding-top: 30px;">Welcome to Hedonia</td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: arial;font-size: 14px;line-height:20px; color: #fff; ">Hi  '.$user_name.'</td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: arial;font-size: 14px;line-height:20px; color: #fff; ">You are not authorized yet as basic member  at <a href="'.home_url().'" style="color: #FE0000">Hedonia.ch</a></td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: arial;font-size: 14px;line-height:20px; color: #fff; ">Reasons</td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                <table width="100%" border="0" cellpadding="0" cellsacing="0" >';
                                            for($i=0;$i<count($explodeData);$i++){
                                            $patient_message .= ' <tr><td style="font-family: arial;font-size: 14px;line-height:20px; color: #fff; ">'.$explodeData[$i].'</td>
                                                                                        </tr>';
                                             }                                           
                                             $patient_message .= '</table></td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: arial;font-size: 14px;line-height:20px; color: #fff; padding-top: 40px; ">
                                                Regards
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: arial;font-size: 16px;line-height:20px; color: #FE0000; padding-top: 0px; ">
                                                Hedonia
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </body>
                </html>';

    $mail->MsgHTML($patient_message); //Put your body of the message you can place html code here
    $send = $mail->Send(); //Send the mails

    if(in_array('Your introduction is against low', $explodeData) || in_array('Your are obviously not owner of your picture(s)', $explodeData) || in_array('Your profile picture(s) are against low', $explodeData))
    {
        $done = $wpdb->delete( 'wp_users', array( 'ID' =>$user_id ) );
        if($done == true) 
        {
            echo 'user has been blocked...!';
            $wpdb->delete( 'wp_usermeta', array( 'user_id' =>$user_id ) );
        }
        else{
            echo 'something worng...!';   
        }
    }else
    {
        echo 'You have Rejected as a basic member...!';
    }

   die;
}

#================Validation process with images =================#
##################################################################
function my_submenu_render()
{
    global $wpdb,$title; 
    $args = array(
        'meta_query'   =>
            array(
                'relation' => 'AND',
            array(
                'key' => 'user_type',
                'value' => 'basic',
                'compare' => "=",
                'type' => ''
            ),
            array(
                'key' => 'validation_type',
                'value' => 'validation by self-timer',
                'compare' => "=",
                'type' => ''
            )

         )

    );
    $blogusers = get_users($args);              
?>
<div class="member-admin-page">
    <div class="col-md-12 heading-bar">
        <h2 class="heading-title"><?php echo $title; ?></h2>
    </div>
    <div class="clear"></div>
    <div class="" style="margin-top:10px;">
        <div class="vaslidation_process">
            <table width="60%">
                <tr>
                    <td width="20%"><label>Select validation type:</label></td>
                    <td width="40%">
                        <select class="form-control member-select" name="user-name" id="select_validation_type" style="margin-bottom:10px;">
                            <option>Select Type</option>                                
                            <option value = "validation by self-timer">Validation by self-timer</option> 
                            <option value = "mail to admin">Mail to admin</option>
                            <option value = "invitation code">Invitation code </option>   
                        </select>
                    </td>
                </tr>
            </table>
            <div id="members_types"></div>
            <div>
                <span class="member_img" style="display:none;text-align:center;">
                    <img height="12px" width="60px" src="<?php echo plugins_url(); ?>/admin-profile-text/ajax-status.gif">
                </span>
            </div>
        </div>
        <table width="100%">
            <tr>
                <td id="pics" style="display:none;">
                    <a href="#" rel="lightbox" id="her_validate_path">
                        <img id="her_validate_name" src="<?php echo get_template_directory_uri(); ?>/images/images.jpeg" height='150' width='150'>
                    </a>
                </td>
                <td id="pics1" style="display:none;">
                    <a href="#" rel="lightbox" id="her_regis_path">
                        <img id="her_registration_name" src="<?php echo get_template_directory_uri(); ?>/images/images.jpeg" height='150' width='150'>
                    </a>
                </td>
                <td id="pics2" style="display:none;">
                    <a href="#" rel="lightbox" id="his_regis_path">
                        <img id="his_registration_name" src="<?php echo get_template_directory_uri(); ?>/images/images.jpeg" height='150' width='150'>
                    </a>
                </td>
            </tr>
        </table>
        <div id="user_validate" style="display:none;">
            <span>&nbsp;&nbsp;</span>
            <div style="margin-top:15px;text-align:center;">
                <span class="user_img" style="display:none;">
                    <img height="12px" width="60px" src="<?php echo plugins_url(); ?>/admin-profile-text/ajax-status.gif">
                </span>
                <div id="error-msg" class="successMsg" style="color:#000!important;text-align: left;"></div>
            </div>
            <div style="margin-top:15px;text-align:center;">
                <span class="member_img1" style="display:none;color:red;">
                    <img height="12px" width="60px" src="<?php echo plugins_url(); ?>/admin-profile-text/ajax-status.gif">
                </span>
                <div id="image_msg" class="successMsg" style="color:yellow!important;text-align: left;"></div>
            </div>
            <br><br>
            <button type="button" class="validate button action"  value="accept" id="validate_images" style="margin-right: 10px;display:none;"/> Validate Profile</button>
            <button type="button" class="reject button action"  value="reject" style="display:none;" id="reject_profile"/> Reject</button>
        </div>
    </div>
</div>

<script>
jQuery('#select_validation_type').change(function(){
    var validationType = jQuery(this).val();
    jQuery('.member_img').css('display','block');
    jQuery.ajax({
        type: 'POST', // Adding Post method
        url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php', // Including ajax file
        data: {"action": "user_validation_type", "type": validationType}, 
        dataType:'json',
        success: function (response) 
        {
            jQuery('#members_types').html(response.userlist);
        },
        complete: function(){
            jQuery('.member_img').css('display','none'); 
        }
    });
});
</script>

<script>
jQuery(document).ready(function(){
    jQuery(document).on('change','#username_valid',function()
    {
        var username = jQuery(this).val();
        jQuery('.member_img').css('display','block');
        if(username =='')
        {
            jQuery("#msg").html('Please select atleast one user from drop down..!');
            setTimeout(function(){jQuery('#error-msg').fadeOut(2000);}, 3000);
        }else         
        {
            jQuery.ajax({
                type: 'POST', // Adding Post method
                url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php', // Including ajax file
                data: {"action": "member_panel_check_validimage", "userid": username}, 
                dataType:'json',
                success: function (response) 
                { 
                    jQuery('#validate_images').show();
                    jQuery('#reject_profile').show();
                    jQuery('#user_validate').show();
                    if(response.pic1 !="")
                    {
                        jQuery("#pics").show(); 
                        jQuery("#her_validate_name").attr('src',response.pic1);   
                        jQuery("#her_validate_path").attr('href',response.pic1); 
                                             
                    }
                    if(response.img != "")
                    {
                        jQuery("#pics1").show();
                        jQuery("#his_registration_name").attr('src',response.img);  
                        jQuery("#his_regis_path").attr('href',response.pic1);                       
                    }
                    if(response.img1 != "")
                    {
                        jQuery("#pics2").show();
                        jQuery("#her_registration_name").attr('src',response.img1);  
                        jQuery("#her_regis_path").attr('href',response.pic2);                       
                    }
                },
                complete: function(){
                    jQuery('.member_img').css('display','none');
                }
            });
        }
    });

    jQuery('#validate_images').click(function(){
        jQuery('.member_img').css('display','block');
        var userid = jQuery('#username_valid option:selected').val();
        jQuery.ajax({
            type: 'POST', // Adding Post method
            url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php', // Including ajax file
            data: {"action": "approval_for_profile_validation", "userid": userid}, 
            dataType:'json',
            success : function(response){
                jQuery('.member_img').css('display','none');
                jQuery('#image_msg').text(response.txt);
                setTimeout(function(){
                jQuery('#image_msg').fadeOut(2000);}, 3000);
                location.reload();
            }
        });
    });

    jQuery('#reject_profile').click(function(){
        jQuery('.member_img1').css('display','block');
        var userid = jQuery('#username_valid').val();
        jQuery.ajax({
            type: 'POST', // Adding Post method
            url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php', // Including ajax file
            data: {"action": "reject_image", "userid": userid}, 
            dataType:'json',
            success : function(response){ 
                jQuery('.member_img1').css('display','none');                   
                jQuery('#image_msg').text(response.txt);
                setTimeout(function(){jQuery('#image_msg').fadeOut(2000);}, 3000);
                location.reload();
            }
        });
    });
});
    
</script>
<?php 
} //end my_submenu_render function

#================ validate with code ================#
#############################################################
function my_validated_code_member()
{
    global $title;
?>
<div class="member-admin-page">
    <div class="col-md-12 heading-bar">
        <h2 class="heading-title"><?php echo $title; ?></h2>
    </div>
    <div class="left-pass-input" style="margin-top:10px;">
        <?php
            $args = array(
                'meta_query'   =>
                    array(
                        'relation' => 'AND',
                    array(
                        'key' => 'user_type',
                        'value' => 'basic',
                        'compare' => "=",
                        'type' => ''
                    ),
                    array(
                        'key' => 'approved_as_basic',
                        'value' => 'yes',
                        'compare' => "=",
                        'type' => ''
                    ),
                    array(
                        'key' => 'invitation_code',
                        'compare' => "NOT EXISTS"
                    )
                )
            );
            $blogusers = get_users($args);
            function getInitials($name)
            {
                //split name using spaces
                $words=explode(" ",$name);
                $inits='';
                //loop through array extracting initial letters
                foreach($words as $word){
                    $inits.=strtoupper(substr($word,0,1));
                    }
                return $inits; 
            }
            $currval = "";
        ?>
        <table width="100%">
            <tr>
                <td width="20%">Select A Member</td>
                <td width="60%">
                    <select class="form-control member-select" name="username_text" id="username_text" style="margin-bottom:10px;">
                        <option value="">Select Member</option>
                        <?php
                            // Array of WP_User objects.
                            foreach ($blogusers as $user) 
                            {
                                $initial = getInitials($user->user_login);
                                if($initial!='' && $currval != $initial) {
                                    $currval = $initial;
                                    echo '<optgroup label="'.$initial.'"></option>';
                                }
                        ?>
                            <option value="<?php echo $user->ID; ?>">
                                <?php echo '<span>' . esc_html($user->user_login) . '</span>'; ?>
                            </option>
                        <?php } ?>
                    </select>
                </td>
                <td width="20%"></td>
            </tr>
            <tr>
                <td width="20%">invitation code</td>
                <td width="60%"><input  type="text" class="pass-input" id="code" name="code"/></td>
                <td width="20%">
                    <button id="gn_code" onclick='document.getElementById("code").value = Password.generate(12)'>Genrate Code</button>
                </td>
            </tr>
            <tr>
                <td><button id="save_code">Save Code</button></td>
            </tr>
        </table>
        <span class="member_img" style="display:none;">
            <img height="12px" width="60px" src="<?php echo plugins_url(); ?>/admin-profile-text/ajax-status.gif">
        </span>
        <div id="msg"></div>
    </div>
</div>
<script type="text/javascript">
var Password = {
 
  _pattern : /[a-zA-Z0-9]/,
  _getRandomByte : function()
  {
    if(window.crypto && window.crypto.getRandomValues) 
    {
      var result = new Uint8Array(1);
      window.crypto.getRandomValues(result);
      return result[0];
    }
    else if(window.msCrypto && window.msCrypto.getRandomValues) 
    {
      var result = new Uint8Array(1);
      window.msCrypto.getRandomValues(result);
      return result[0];
    }
    else
    {
      return Math.floor(Math.random() * 256);
    }
  },
  
  generate : function(length)
  {
    return Array.apply(null, {'length': length})
      .map(function()
      {
        var result;
        while(true) 
        {
          result = String.fromCharCode(this._getRandomByte());
          if(this._pattern.test(result))
          {
            return result;
          }
        }        
      }, this)
      .join('');  
  }    
    
};
</script>
<script>
jQuery("#save_code").click(function () {    
    var userid = jQuery("#username_text").val();  
     var code = jQuery("#code").val();  
    if(userid =='')
    {
        jQuery("#msg").html('Please select atleast one user from drop down..!');
        jQuery("#msg").show();
        setTimeout(function(){jQuery('#msg').fadeOut(2000);}, 5000);
    }else if(code =='')         
    { 
        jQuery("#msg").html('Please enter the code..!');
        jQuery("#msg").show();
        setTimeout(function(){jQuery('#msg').fadeOut(2000);}, 5000);
    }else{
        jQuery.ajax({
            type: 'POST', // Adding Post method
            url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php', 
            dataType:'json',
            data: {"action": "member_panel_save_code", "userid": userid,"code":code}, 
            beforeSend : function(){
                jQuery('.member_img').css('display','block');
            },
            success: function (response) { 
                jQuery("#msg").show().html(response.msg);
                setTimeout(function(){location.reload();}, 3000);
            },
            error: function() {
                alert('failed');
            },
            complete: function() {
                jQuery('.member_img').css('display','none');
                setTimeout(function(){jQuery('#msg').fadeOut(2000);}, 5000);
            }
        });
    }
});
</script>
<?php
} // end function

add_action('wp_ajax_member_panel_save_code', 'member_panel_check_code'); 
add_action('wp_ajax_nopriv_member_panel_save_code', 'member_panel_check_code'); 

function member_panel_check_code()
{
    global $wpdb;
    $user_id = $_POST['userid'];
    $code    = $_POST['code'];
    $intcode = get_user_meta($user_id,'invitation_code',true);
    if($intcode)
    {
        $jsr = array('msg'=>'you have already send invitation code.!');
    }else
    {
        $up = update_usermeta($user_id,'invitation_code',$code);
        if($up==true)
        {
            $jsr = array('msg'=>'invitation code has saved..!');
        }else
        {
            $jsr = array('msg'=>'invitation code has not saved..!');
        }
    }
    echo json_encode($jsr);
    die;
}
