<?php  
    /* 
    Plugin Name: User Members Panel
    Plugin URI: http://www.orangecreative.net 
    Description: Plugin for displaying products from an OSCommerce shopping cart database 
    Author: Gireesh Kumar
    Version: 1.0 
    Author URI: http://www.orangecreative.net 
    */  
//Include Javascript library

// including ajax script in the plugin Myajax.ajaxurl
wp_localize_script( 'inkthemes', 'MyAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php')));
//wp_enqueue_script('inkthemes', plugins_url( './js/demo.js' , __FILE__ ) , array( 'jquery' ));
    function members_panel_admin() {  
     include('members-panel-admin.php');  
}  

    function members_panel() {  
      add_options_page("Members Panel","Members Panel ",1,"MembersPanel","members_panel_admin");  
    }  

    add_action('admin_menu', 'members_panel');  
?>
<?php
function member_panel_check(){
$user_id = $_POST['userid'];
global $wpdb;
      $user_info = get_userdata($user_id);
      $profilename= $user_info->user_login;
      $userid=$user_info->ID;
      $reg3_vrfctncode= get_user_meta($user_id,'invitation_code',true);
      $user_profile_type= get_user_meta($user_id,'member_type',true);
      $user_type_data= get_user_meta($user_id,'user_type',true);
      $user_validate_pick = get_user_meta($user_id,'validate_through_image',true);
      $img_src = '<img src="'.site_url().'/'.$user_validate_pick.'" style="width:100px;height:100px">';
      echo $profilename.'||'.$reg3_vrfctncode.'||'.$user_profile_type.'||'.$user_type_data.'||'.$user_id.'||'.$img_src;
      
die();
return true;
}
//
add_action('wp_ajax_member_panel_check', 'member_panel_check'); // Call when user logged in
add_action('wp_ajax_nopriv_member_panel_check', 'member_panel_check'); // Call when user in not logged in?>


<?php
function member_panel_activate(){
echo $user_id = $_POST['activate'];
global $wpdb;
add_user_meta($user_id , 'activate',1 );
$msg=get_user_meta($user_id , 'activate');
if($msg==1)
{
echo "Success !";
}
else
{
	echo "Failed !";
}
die();
return true;
}
//
add_action('wp_ajax_member_panel_activate', 'member_panel_activate'); // Call when user logged in
add_action('wp_ajax_nopriv_member_panel_activate', 'member_panel_activate'); // Call when user in not logged in?>


<?php
function member_panel_activate_basic(){
echo $user_id = $_POST['activate_basic'];
global $wpdb;
add_user_meta($user_id , 'activate_basic',1 );
$msg=get_user_meta($user_id , 'activate_basic',true);
if($msg==1)
{
echo "Success !";
}
else
{
	echo "Failed !";
}
die();
return true;
}
//
add_action('wp_ajax_member_panel_activate_basic', 'member_panel_activate_basic'); // Call when user logged in
add_action('wp_ajax_nopriv_member_panel_activate_basic', 'member_panel_activate_basic'); // Call when user in not logged in?>

<?php
function member_panel_activate_verified(){
$user_id = $_POST['userid'];
 $veryfiedid = $_POST['verified'];
global $wpdb;
echo $msg=get_user_meta($user_id , 'invitation_code',true);
if($veryfiedid==$msg)
{
 update_user_meta($user_id , 'is_verified',1 );	
 update_user_meta($user_id,'user_type','validated');
 $msg_veryfied=get_user_meta($user_id , 'is_verified',true);
 if($msg_veryfied==1)
 {
	 echo "Now You Verified !";
	 $adminemail = get_option('admin_email');
	 $get_user_info = get_user_by('id',$user_id);
	 $user_email = $get_user_info->data->user_email;
	 $user_name  = $get_user_info->data->user_login;
	 
	 
     require_once($_SERVER['DOCUMENT_ROOT'].'/smtp/newsmtp/classes/class.phpmailer.php');
     $email = $user_email;
								  $mail  = new PHPMailer; // call the class
								  $mail->IsSMTP();
								  $mail->SMTPSecure = "";
								  //$mail->SMTPDebug  = 2;  
								  $mail->Host = 'ssl://smtp.gmail.com'; // platinum.waxspace.com Hostname of the mail server
								  $mail->Port = '465'; //Port of the SMTP like to be 25, 80, 465 or 587
								  $mail->SMTPAuth = true; //Whether to use SMTP authentication
								  $mail->Username = 'theclub.hedon@gmail.com'; //Username for SMTP authentication any valid email created in your domain
								  $mail->Password = 'testing@theclub'; //Password for SMTP authentication
								  //$mail->AddReplyTo("admin@quiconnaitunbon.com.com", "Admin"); //reply-to address
								  $mail->SetFrom("theclub.hedon@gmail.com"); //From address of the mail
								  //$mail->SMTPSecure = "tls";
								  // put your while loop here like below,
								  $mail->Subject = "User Registeration"; //Subject od your mail
								  $mail->AddAddress($email,$user_name); //To address who will receive this email
								  
								  $patient_message= "Hi &nbsp;".$user_name."<br><br>
													 You have become verified member at hedonia.ch
													 <br><br>
													 Thanks & Regards <br>
													 Hedonia.ch
								  ";
                  
								  $mail->MsgHTML($patient_message); //Put your body of the message you can place html code here
								  $send = $mail->Send(); //Send the mails
	  
 }
 else 
 { 
   echo "Not Verified";
 }
 
}else
{
	echo "Incorrect value , user still not verified !";
}


die();
return true;
}
//
add_action('wp_ajax_member_panel_activate_verified', 'member_panel_activate_verified'); // Call when user logged in
add_action('wp_ajax_nopriv_member_panel_activate_verified', 'member_panel_activate_verified'); // Call when user in not logged in?>
<?php
function member_panel_check_profile(){
$user_id = $_POST['userid'];
global $wpdb;
      $user_info = get_userdata($user_id);
      $describe=trim(strip_tags(get_user_meta($user_id , 'describe' )));
      $her_pick=get_user_meta($user_id , 'her_pick',true);
      $his_pick=get_user_meta($user_id , 'his_pick',true);
      $url=site_url()."/wp-content/uploads/profilepick_".$user_id."";
      if($her_pick && $his_pick)
        $img="<img src=".$url.$her_pick." height='150' width='150'>"."<img src=".$url.$his_pick." height='150' width='150'>";
      elseif($her_pick)
        $img="<img src=".$url.$her_pick." height='150' width='150'>";
      elseif($his_pick)
        $img="<img src=".$url.$his_pick." height='150' width='150'>";
    
      echo $describe."||".$img;

}
//
add_action('wp_ajax_member_panel_check_profile', 'member_panel_check_profile'); // Call when user logged in
add_action('wp_ajax_nopriv_member_panel_check_profile', 'member_panel_check_profile'); // Call when user in not logged in?>


<?php
function member_panel_check_profile_text(){
$user_id = $_POST['userid'];
$verified = $_POST['verified'];
global $wpdb;

}
//
add_action('wp_ajax_member_panel_check_profile_text', 'member_panel_check_profile_text'); // Call when user logged in
add_action('wp_ajax_nopriv_member_panel_check_profile_text', 'member_panel_check_profile_text'); // Call when user in not logged in?>

<?php
function member_panel_check_profile_pic(){
$user_id = $_POST['userid'];
$verified = $_POST['verified'];
global $wpdb;

}
//
add_action('wp_ajax_member_panel_check_profile_pic', 'member_panel_check_profile_pic'); // Call when user logged in
add_action('wp_ajax_nopriv_member_panel_check_profile_pic', 'member_panel_check_profile_pic'); // Call when user in not logged in?>
