<?php
$wp_ffpc_config = array (
  'hedon.trendhosting.ch' => 
  array (
    'hosts' => '127.0.0.1:11211',
    'memcached_binary' => 0,
    'authpass' => '',
    'authuser' => '',
    'expire' => '300',
    'expire_home' => '300',
    'expire_taxonomy' => '300',
    'invalidation_method' => 0,
    'prefix_meta' => 'meta-',
    'prefix_data' => 'data-',
    'charset' => 'utf-8',
    'log' => '1',
    'cache_type' => 'memcached',
    'cache_loggedin' => 0,
    'nocache_home' => 0,
    'nocache_feed' => 0,
    'nocache_archive' => 0,
    'nocache_single' => 0,
    'nocache_page' => 0,
    'nocache_cookies' => 0,
    'nocache_dyn' => '1',
    'nocache_url' => '^/wp-',
    'response_header' => 0,
    'generate_time' => 0,
    'precache_schedule' => 'null',
    'key' => '$scheme://$host$request_uri',
    'comments_invalidate' => '1',
    'pingback_header' => 0,
    'version' => '1.7.4',
  ),
);
include_once ('/home/www/hedon/htdocs/wp-content/plugins/wp-ffpc/wp-ffpc-acache.php');
?>