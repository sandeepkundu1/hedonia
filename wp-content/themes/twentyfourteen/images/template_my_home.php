<?php
/*
 *Template Name:My Home 
 * 
 * 
 * 
*/
get_header();
global $current_user;

?>
<link rel="stylesheet" type="text/css" href="<?php echo bloginfo('template_url'); ?>/template_css/bootstrap.css">
		<link href="<?php echo bloginfo('template_url'); ?>/template_css/font-awesome.css" rel="stylesheet" type="text/css"/>
		<link href="<?php echo bloginfo('template_url'); ?>/template_css/fonts.css" rel="stylesheet" type="text/css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo bloginfo('template_url'); ?>/template_css/style.css">	
		<script src="<?php echo bloginfo('template_url'); ?>/template_js/jquery-1.11.1.min.js" type="text/javascript"></script>			
		<script src="<?php echo bloginfo('template_url'); ?>/template_js/bootstrap.js" type="text/javascript"></script>	
<?php require_once('user_profile_header.php');  ?>
  <div class="container-fluid">
	<a href="<?php echo get_permalink(305).'&user_id=106'; ?>">clickhere</a>
<div class="container">
	<div class="parent_div">
		<form class="form-horizontal" role="form" name="form" method="post">
			<!---------------------------send mail section-------------------------------------->
			<div class="col-sm-3 no-padding-left">
				<h4 class="my-home-h4">Next events in your regio</h4>
				<img class="my-home-img" src="images/myhome_left.jpg">
			</div>
			<div class="col-sm-9 no-padding myhome-right-content">
				<div class="col-sm-4 no-padding-left">
					<div class="myhome-blue-part">
						<div class="msg-header">
							<img src="images/message.jpg">
							<p>you have 3 new mails</p>
						</div>
						<p class="mark-text">mark</p>
						<?php 
	
	    $mylink = $wpdb->get_results("SELECT * FROM email_template WHERE reciepient_uid = '".$current_user->ID."'");
	    if(!empty($mylink))
	    {
		foreach($mylink as $val)
		{
			$sender_uid = $val->sender_uid;
			$delete_box_id[] = $sender_uid;
			$user_data = get_user_meta($val->sender_uid);
			$pick_name =$user_data["her_pick"]["0"];
			$upload_dir = wp_upload_dir();
			$pick_source = $upload_dir['baseurl'].'/'.$pick_name;
			echo '<div class="msg-content">';
			echo '<div class="gray-div">
								<div class="msg-user-img">
									<img src="'.$pick_source.'">
								</div>';
			echo '<div class="msg-user-text">';					
			//echo '<pre>';print_r($val);
			
			//print_r($user_data);
			$source = $val->date;
			$date = new DateTime($source);
			 $date->format('j F ');
			echo '<a href="#">'.$user_data["nickname"]["0"].'</a>';
			echo '<p class="msg-date">'.$date->format('j F ').'</p>';
			echo '<p class="msg-description">'.$val->content.'</p>';
			echo '<a href="#" class="col-sm-2 no-padding my-home-arrow"><img src="images/arrowright.jpg"></a>';
			echo '</div></div>';
			echo '<div class="msg-chkbox-div">
				  <input type="checkbox"  name="delete_user" value="'.$sender_uid.'" id="read_and_accept'.$sender_uid.'" class="form-control">
				  
				  </div>
				  </div>';

			}
		
	
	?>

						<div class="row">
								<div class="col-sm-5"></div>
								<a href="3" class="col-sm-2 small-text1"><img src="images/arrowdown.png"><br/><span>older</span></a>
								<div class="col-sm-5"></div>
							</div>
							<div class="btn-swiss-right">
								<a href="#" class="light-blue-btn1">old mails</a>
								<a href="#" class="light-blue-btn1" style="">mails sent</a> 
								<button type="button" id="delete" class="dark-blue-btn1">delete</button> 
							</div>
							<?php } else {echo 'No mail Exist';}?>
					</div>
					
				</div>
				
				
				<input type="hidden" name="current_user_id" value="<?php echo $current_user->ID;  ?>" id ="current_user_id">
					<script language="javascript" type="text/javascript">
						$(document).ready(function () {
						$("#delete").click(function(){
						var deleted_id = new Array();
						$('input[name="delete_user"]:checked').each(function() {
						deleted_id.push(this.value);
						});
						alert(deleted_id);
						var current_user_id = $("#current_user_id").val();
						alert(current_user_id);
						formData = '&deleted_id='+deleted_id+'&current_user_id='+current_user_id;
						  $.ajax({
							  type: 'POST', 
							  data:formData,
							  url:"<?php echo bloginfo('template_url');  ?>/delete_email.php",dataType: "json",
							  success:function(result){
								  var Success = result
								 
								alert(Success.message);
								}
						  });
						
						});
						});
                        </script>
				
	<!---------------------------End send mail section-------------------------------------->
	
	<!---------------------------Add favourite section-------------------------------------->

				
				<div class="col-sm-4 no-padding-left">
				
					<div class="myhome-blue-part">
						<div class="msg-header">
							<img src="images/like.png">
							<p>3 members marked you as favourite</p>
						</div>
						<p class="mark-text">mark</p>
			<?php 
			$fav_data = $wpdb->get_results("SELECT * FROM add_favourite WHERE liked_to = '".$current_user->ID."'");  
            if(!empty($fav_data))
            {
				foreach($fav_data as $fav_value)
				{
					$liked_by =  $fav_value->liked_by;
					$add_fav = get_user_meta($liked_by);
				    $likername = $add_fav['nickname']['0'];
					$source_date = $fav_value->send_date;
			        $likeddate = new DateTime($source_date);
			        $apply_date =  $likeddate->format('j F ');
			        
					echo '<div class="msg-content">
					      <div class="gray-div">
					      <div class="msg-user-img">
					      <img src="images/Marina.jpg">
					      </div>
					      <div class="msg-user-text">
					      <a href="#">'.$likername.'</a>
					      <p class="msg-date">'.$apply_date.'</p>
					      </div>
					      </div>';
					      echo '<div class="msg-chkbox-div">
								<input type="checkbox" class="checkbox_cus" name="married" value="married" id="read_and_accept" class="form-control" required="">
									<label class="checkbox_label7" for="read_and_accept">
							</div>
						</div>';
					
					
					
					}
				
				
				
             ?>


							
						<div class="row">
								<div class="col-sm-5"></div>
								<a href="3" class="col-sm-2 small-text1"><img src="images/arrowdown.png"><br/><span>older</span></a>
								<div class="col-sm-5"></div>
							</div>
							<div class="btn-swiss-right">
								<a href="#" class="light-blue-btn1">old likes</a>
								<a href="#" class="light-blue-btn1" style="">likes sent</a> 
								<a href="#" class="dark-blue-btn1" style="">delete</a> 
							</div>
							<?php } else {echo 'No likes found for this user';} ?>
					</div>
				
				</div>
				
		<!------------------------------End  favourite section-------------------------------------->		
		
		
		<!------------------------------send kiss section-------------------------------------->	
				
				
				<div class="col-sm-4 no-padding-left">
					<div class="myhome-blue-part1">
						<div class="msg-header">
							<img src="images/kissbig.png">
							<p>you have 4 new kisses</p>
						</div>
						<div><span class="mark-text">mark</span></div>
						<?php
						$kiss_data = $wpdb->get_results("SELECT * FROM send_a_kiss WHERE recipient_user_id = '".$current_user->ID."'");  
						foreach($kiss_data as $kiss_user)
						{
							//print_r($kiss_user);
							$kissed_by = $kiss_user->send_user_id;
							$attached_url = $kiss_user->attachment_url;
							$fromuser = get_user_meta($kissed_by);
							$username = $fromuser['nickname']['0'];
							$kiss_date = $fav_value->send_date;
			                $send_kiss_date = new DateTime($source_date);
			                $sent_date =  $likeddate->format('j F ');
							
							
							echo '<div class="msg-content">
							
							<div class="gray-div">
								<div class="msg-user-img">
									<img src="images/Marina.jpg">
								</div>
								<div class="msg-user-text">
									<a href="#">'.$username.'</a>
									<p class="msg-date">'.$sent_date.'</p>
									<p class="kiss-description">kiss back &nbsp;<img class="kiss-small" src="'.$attached_url.'" /></p>
								</div>
							</div>
							<div class="msg-chkbox-div">
								<input type="checkbox"  name="married" value="'.$kissed_by.'" id="delete_kiss_'.$kissed_by.'" class="form-control" required="">
									
							</div>
						</div>';
							
							
							}
						?>
						
						
						<div class="row">
								<div class="col-sm-5"></div>
								<a href="3" class="col-sm-2 small-text1"><img src="images/arrowdown.png"><br/><span>older</span></a>
								<div class="col-sm-5"></div>
							</div>
							<div class="btn-swiss-right">
								<a href="#" class="light-blue-btn2">Older kisses</a>
								<a href="#" class="light-blue-btn2" style="">kisses sent</a> 
								<a href="#" class="dark-blue-btn1" style="">delete</a> 
							</div>
					</div>
					
					
					
					
					
					
					
					
					
					<div class="myhome-blue-star-part  myhome-box-margin-top">
						<div class="msg-header">
							<img src="images/Star.png">
							<p>you have 1 new matches</p>
						</div>
						
						<div class="msg-content">
							
							<div class="gray-div">
								<div class="msg-user-img">
									<img src="images/Marina.jpg">
								</div>
								<div class="msg-user-text">
									<a href="#">Marina&Stephan</a>
									<p class="msg-date">19 August</p>
									<!-- <p class="msg-description">We will be next week in Zurich</p>
									<a href="#" class="col-sm-2 no-padding my-home-arrow"><img src="images/arrowright.jpg"></a> -->
								</div>
							</div>
							<div class="msg-chkbox-div">
								<input type="checkbox" class="checkbox_cus" name="married" value="married" id="read_and_accept" class="form-control" required="">
									<label class="checkbox_label7" for="read_and_accept">
							</div>
						</div>
						<div class="row">
								<div class="col-sm-5"></div>
								<a href="3" class="col-sm-2 small-text1"><img src="images/arrowdown.png"><br/><span>older</span></a>
								<div class="col-sm-5"></div>
							</div>
							<div class="btn-swiss-right">
								<a href="#" class="light-blue-btn2">Older matches</a>
								<a href="#" class="dark-blue-btn1" style="float:right; margin-right: 6px;">delete</a> 
							</div>
					</div>
						
						
						
					</div>
					
				</div>
			</div>
		</form>
	</div>
</div>
		
						
					
                        
                      

                        
                        
						
<?php get_footer(); ?>
