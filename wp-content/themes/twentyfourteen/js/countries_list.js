/*
	*	Original script by: Shafiul Azam
	*	Version 4.0
	*	Modified by: Luigi Balzano

	*	Description:
	*	Inserts Countries and/or States as Dropdown List
	*	How to Use:

		In Head section:
		----------------
		<script type= "text/javascript" src = "countries.js"></script>
		
		In Body Section:
		----------------
		Select Country (with states):   <select id="country" name ="country"></select>
			
		Select State: <select name ="state" id ="state"></select>

        Select Country (without states):   <select id="country2" name ="country2"></select>
			
		<script language="javascript">
			populateCountries("country", "state");
			populateCountries("country2");
		</script>

	*
	*	License: Free to copy, distribute, modify, whatever you want to.
	*	Aurthor's Website: http://bdhacker.wordpress.com
	*
*/

// Countries

// Countries
var country_arr = new Array("Albania","Austria","Belarus", "Belgium","Bosnia and Herzegovina","Bulgaria", "Croatia","Czech Republic", "Denmark", "Estonia","Finland", "France","Germany","Greece","Hungary", "Island","Ireland","Italy","Latvia",  "Liechtenstein", "Lithuania", "Luxembourg","Macedonia", "Malta","Netherlands","Norway", "Poland", "Portugal","Romania","Russia", "Serbia","Slovakia", "Slovenia",  "Spain","Sweden", "Switzerland","Turkey","Ukraine","United Kingdom");
// States
var s_a = new Array();
s_a[0]="";
s_a[1]="";
s_a[2]="Burgenland|Kärnten|Nieder‐Österreich|Ober‐Österreich|Salzburg|Steiermark|Tirol|Vorarlberg";
s_a[3]="";
s_a[4]="Bruxelles|Vlaanderen|Wallonie";
s_a[5]="";
s_a[6]="Black Sea Coast|Central Northern|Northern Thrace|Northwest|Southwest";
s_a[7]="Dubrovnik region|Istra region|Kvaner Highlands region|Sibenik region|Slavonia region|Split region|Zadar region|Zagrep region";
s_a[8]="Central Bohemia (Praha)|Eastern Bohemia  (Zámrsk)|Northern Bohemia (Litoměřice)|Northern Moravia (Opava )|Prague|Southern Bohemia (Třeboň)|Southern Moravia (Brno)|Western Bohemia ( Plzeň)";
s_a[9]="Hovenstaden|Midtjlland|Nordjylland|Sjælland|Syddanmarl";
s_a[10]="";
s_a[11]="Eastern Finland|Lapland|Oulu|Southern Finland|Western Finland";
s_a[12]="Alsace|Aquitaine|Auvergne|Basse-Normandie|Bourgogne|Bretagne|Centre|Champagne‐Ardenne entre|Corsica|Franche-Comte|Haute-Normandie|Ile-de-France|Languedoc-Roussillon|Limousin|Lorraine|Midi-Pyrenees|Nord-Pas-de-Calais|Pays de la Loire|Picardie|Poitou-Charentes|Provence-Alpes-Cote d'Azur|Rhone-Alpes";
s_a[13]="Baden-Wuerttemberg|Bayern|Brandenburg|Berlin|Hamburg|Hessen|Mecklenburg-Vorpommern|Niedersachsen|Nordrhein-Westfalen|Rheinland-Pfalz|Saarland|Sachsen|Sachsen-Anhalt|Schleswig-Holstein|Thueringen";
s_a[14]="Attica|Central Greece|Central Macedonia|Crete|East Macedonia and Thrace|Epirus|Evia|Macedonia|Ionian Islands|Mount Athos|North Aegean|Peloponnese|South Aegean|Thessaly|West Greece|West ";
s_a[15]="Central Hungary|Central Transdanubia|Northern Great Plain|Northern Hungary|Southern Great Plain|Southern Transdanubia|Western Transdanubia";
s_a[16]="";
s_a[17]="Connaught|Leinster|Munster|Ulster";
s_a[18]="Abruzzo|Basilicata|Calabria|Campania|Emilia-Romagna|Friuli|Lazio|Liguria|Lombardia|Marche|Molise|Piemonte|Puglia|Sardegna|Sicilia|Toscana|Trentino|Umbria|Valle d'Aosta|Veneto";
s_a[19]="";
s_a[20]="";
s_a[21]="";
s_a[22]="";
s_a[23]="";
s_a[24]="";
s_a[25]="Amsterdam|Drenthe|Flevoland|Friesland|Gelderland|Groningen|Limburg|Noord-Brabant|Noord-Holland|Overijssel|Utrecht|Zeeland|Zuid-Holland";
s_a[26]="Nord‐Norge|Østlandet|Sørlandet|Trøndeleg|Vestlandet";
s_a[27]="Dolnoslaskie|Kujawsko-Pomorskie| Łódzkie|Lubelskie|Lubuskie|Malopolskie|Mazowieckie|Opolskie|Podkarpackie|Podlaskie|Pomorskie|Śląskie|Świętokrzyskie|Warmińsko‐Mazurskie|Wielkopolskie|Zachodniopomorskie";
s_a[28]="Algarve|Alentejo|Centro|Lisboa|Norte";
s_a[29]="Banat|Bucovina|Crişana|Dobrogea|Maramureş|Moldova|Transylvania|Walachia";
s_a[30]="";
s_a[31]="Bačka|Banat|Beograd|Centralna Srbija – Zapad|Centralna Srbija – Istok|Južna Srbija|Srem, Mačva, Kolubara";
s_a[32]="Bánska Bystrica|Bratislava|Košice|Nitra|Prešov|Trenčin|Trnava|Žilina";
s_a[33]="";
s_a[34]="Andalucia|Aragon|Asturias|Balears|Canarias|Cantabria|Castilla – La Mancha|Castilla y Leon|Catalunya|Euskadi |Extremadura|Galicia|Madrid|Murcia|Navarra|Rioja|Valencia";
s_a[35]="Götaland|Norrland|Svealand";
s_a[36]="Espace Mittelland|Geneve + Vaud|Graubünden|Nordwestschweiz|Ostschweiz|Ticino|Valais|Zentralschweiz|Zürich";
s_a[37]="";
s_a[38]="";
s_a[39]="East Midlands|Eastern Scotland|East of England|London|North East England|North West England|Northern Ireland|Northern Scotland|South East England|South West England|Wales|West Midlands|Western Scotland|Yorkshire & Humberside";

/*** Array for country flag ****/
var flag = new Array();
flag[1] = "Albania-flag.jpg";
flag[6] = "Bulgaria-flag.png";
flag[13] = "germany-flag.jpg";
flag[9] = "Denmark-flag.jpg";
flag[35] = "Sweden-flag.jpg";
flag[12] = "French-flag.png";
flag[25] = "netherlands-flag.jpg";
flag[11] = "Finland-flag.GIF";
flag[17] = "Ireland-Flag.jpg";
flag[39] = "United-Kingdom-flag.jpg";
flag[8] = "Czech-Republic-flag.png";
flag[27] = "Poland-flag.jpg";
flag[28] = "portugal-flag.png";
flag[2] = "Austria-flag.png";
flag[34] = "Spain-flag.jpg";
flag[6] = "Bulgaria-flag.png";
flag[32] = "Slovakia-flag.jpg";
flag[15] = "Hungary-Flag.png";
flag[14] = "Greece-flag.jpg";
flag[7] = "Croatia-flag.png";
flag[26] = "Norway-Flag.jpg";
flag[18] = "italy-flag.png";
flag[29] = "Romania-flag.jpg";
flag[31] = "Serbia-flag.jpg";
flag[32] = "Slovakia-flag.jpg";
flag[36] = "Switzerland-flag.jpg";
/*** Array for country map ****/

var map = Array();
map[4] = "Belgium.jpg";
map[13] = "Germany.jpg";
map[9] = "Denmark.jpg";
map[35] = "Sweden.jpg";
map[12] = "France.jpg";
map[25] = "Netherlands.jpg";
map[11] = "Finland.jpg";
map[17] = "Ireland.jpg";
map[39] = "UK.jpg";
map[8] = "Czech.jpg";
map[27] = "Poland.jpg";
map[28] = "Portugal.jpg";
map[2] = "Austria.jpg";
map[34] = "Spain Territories main.jpg";
map[6] = "Bulgaria.jpg";
map[32] = "Slovakia.jpg";
map[15] = "Hungary.jpg";
map[29] = "Romania.jpg";
map[14] = "13.jpg";
map[18] = "Italy Main.jpg";
map[7] = "Croatia.jpg";
map[26] = "Norway.jpg";
map[31] = "Serbia.jpg";
map[36] = "Switzerland.jpg";

function populateStates( countryElementId, stateElementId, stateArray ){	
	var selectedCountryIndex = document.getElementById( countryElementId ).selectedIndex;
	var stateElement = document.getElementById( stateElementId );	
	stateElement.length=0;	// Fixed by Julian Woods
	stateElement.options[0] = new Option('Select State','');
	stateElement.selectedIndex = 0;	
	var state_arr = s_a[selectedCountryIndex].split("|");	
	var check="";
	var count = 1;		
	var newArray = new Array();
	jQuery.each(stateArray.split(","), function(index, item) {
    	 newArray.push(item);
	});

	if(newArray.length > 0){
		for (var i=0; i<state_arr.length; i++) {		
		if(jQuery.inArray(state_arr[i], newArray) != -1)  
		{
			var checked = "checked";
		}
		else
		{
			var checked = "";
		}
		stateElement.options[stateElement.length] = new Option(state_arr[i],state_arr[i]);
		if(stateElement.length != 2)
		{
			check += '<div class="col-sm-9 col-md-9">'+state_arr[i]+'</div>';
		check +='<div class="col-sm-3 col-md-3"><input '+checked+'  type="checkbox" class="checkbox_cus asas" id="'+state_arr[i]+'" value="'+state_arr[i]+'" name="statelist[]"><label class="checkbox_label2" for="'+state_arr[i]+'"></div>';

		}
		
		count++;
	}
	}else {
		for (var i=0; i<state_arr.length; i++) {
		stateElement.options[stateElement.length] = new Option(state_arr[i],state_arr[i]);
		check += '<div class="col-sm-9 col-md-9">'+state_arr[i]+'</div>';
		check +='<div class="col-sm-3 col-md-3"><input count="'+stateArray+'" type="checkbox" class="checkbox_cus" id="'+state_arr[i]+'" value="'+state_arr[i]+'" name="statelist[]"><label class="checkbox_label2" for="'+state_arr[i]+'"></div>';
		count++;
	}

	}
	
	var stateId=$('#'+countryElementId).closest("div").find('.statecheck-box').attr('id');	
	$("#"+stateId).html(check);
}

function populateCountries(countryElementId, stateElementId, countryName, stateName){
	// given the id of the <select> tag as function argument, it inserts <option> tags
	var countryElement = document.getElementById(countryElementId);	
	countryElement.length=0;
	countryElement.options[0] = new Option('Select Country','');
	countryElement.selectedIndex = 0;
	for (var i=0; i<country_arr.length; i++) {
		countryElement.options[countryElement.length] = new Option(country_arr[i],country_arr[i]);
	}

	if(countryName)
	{

		$('select[name^="'+countryElementId+'"] option[value="'+countryName+'"]').attr("selected","selected");
		populateStates( countryElementId, stateElementId, stateName );
		countryFlag(countryElementId);
	}
	// Assigned all countries. Now assign event listener for the states.
	if( stateElementId ){
		countryElement.onchange = function(){
			populateStates( countryElementId, stateElementId, stateName);
			countryFlag(countryElementId);
		};
	}
}


function countryFlag(countryElementId)
{
	var selectedCountryIndex = document.getElementById( countryElementId ).selectedIndex;
	var country_name = country_arr[selectedCountryIndex - 1];	
	$('#'+countryElementId).closest("div").find('.showMap').attr('countryid', selectedCountryIndex);
	$('#'+countryElementId).closest("div").find('.map-part').show();
	$('#'+countryElementId).closest("div").find('.map-part-h3').html(country_name);
	$('#'+countryElementId).closest("div").find('.selectImage').attr('src','/hedon/wp-content/themes/twentyfourteen/page-templates/countryflag/' + flag[selectedCountryIndex]);
	$('#'+countryElementId).closest("div").find('.zoom').attr('imagepath','/hedon/wp-content/themes/twentyfourteen/page-templates/countrymap/' + map[selectedCountryIndex]);
	$('#'+countryElementId).closest("div").find('.showMap').attr('src','/hedon/wp-content/themes/twentyfourteen/page-templates/countrymap/' + map[selectedCountryIndex]);
	$('#'+countryElementId).closest("div").find('.zoomMap').attr('href','/hedon/wp-content/themes/twentyfourteen/page-templates/countrymap/' + map[selectedCountryIndex]);
}



/**************** for single country region function *******************/

function singleStates( countryElementId, stateElementId, stateArray ){	
	var selectedCountryIndex = document.getElementById( countryElementId ).selectedIndex;
	var stateElement = document.getElementById( stateElementId );	
	stateElement.length=0;	// Fixed by Julian Woods
	stateElement.options[0] = new Option('Select State','');
	stateElement.selectedIndex = 0;	
	var state_arr = s_a[selectedCountryIndex].split("|");	
	var check="";	
	/*if(stateArray != 0){
		for (var i=1; i<state_arr.length; i++) {
		$('select[name^="'+stateElementId+'"] option[value="'+stateArray+'"]').attr("selected","selected");		
		var selectedState = (stateArray == state_arr[i]) ? "checked" : "";
		stateElement.options[stateElement.length] = new Option(state_arr[i],state_arr[i]);		
	}
	}else{
		for (var i=1; i<state_arr.length; i++) {
		stateElement.options[stateElement.length] = new Option(state_arr[i],state_arr[i]);		
	}
	}*/	
	if(stateArray != 0){
		for (var i=0; i<state_arr.length; i++) {
		$('select[name^="'+stateElementId+'"] option[value="'+stateArray+'"]').attr("selected","selected");		
		var selectedState = (stateArray == state_arr[i]) ? "selected" : "";
		stateElement.options[stateElement.length] = new Option(state_arr[i],state_arr[i]);		
	}
	}else{
		for (var i=0; i<state_arr.length; i++) {
		stateElement.options[stateElement.length] = new Option(state_arr[i],state_arr[i]);		
	}
	}
	
}

function singleCountries(countryElementId, stateElementId, countryName, stateName){
	// given the id of the <select> tag as function argument, it inserts <option> tags
	var countryElement = document.getElementById(countryElementId);	
	countryElement.length=0;
	countryElement.options[0] = new Option('Select Country','');
	countryElement.selectedIndex = 0;
	for (var i=0; i<country_arr.length; i++) {
		countryElement.options[countryElement.length] = new Option(country_arr[i],country_arr[i]);
	}
	if(countryName)
	{
		$('select[name^="'+countryElementId+'"] option[value="'+countryName+'"]').attr("selected","selected");
		singleStates( countryElementId, stateElementId, stateName );
		countryFlag(countryElementId);
	}
	// Assigned all countries. Now assign event listener for the states.
	if( stateElementId ){
		countryElement.onchange = function(){
			singleStates( countryElementId, stateElementId, stateName);
			countryFlag(countryElementId);
		};
	}
}
