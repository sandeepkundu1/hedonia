<?php
/*
 * Template Name: Validation Process
 * Description: A Page Template with show the validation process.
 * Author Name : Sanjay Shinghania
 */

get_header();
?> 

<script language="javascript" type="text/javascript">
jQuery(function () {
    jQuery("#photoimg").change(function () {
        jQuery("#dvPreview").html("");
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg)$/;
        // check the image size
        var iSize = ($("#photoimg")[0].files[0].size / 1024); 
        if (iSize / 1024 > 1) 
        {  
            iSize = (Math.round((iSize / 1024) * 100) / 100)
            jQuery("#dvPreview").html("Image size is greater than 1 Mb");   
        } 
        else 
        {
            // check the image format
            if (regex.test($(this).val().toLowerCase())) 
            {
                if (jQuery.browser.msie && parseFloat(jQuery.browser.version) <= 9.0) {
                    jQuery("#dvPreview").show();
                   jQuery("#dvPreview")[0].filters.item("DXImageTransform.Microsoft.AlphaImageLoader").src = $(this).val();
                }
                else {
                    if (typeof (FileReader) != "undefined") {
                        jQuery("#dvPreview").show();
                        jQuery("#dvPreview").append("<img style='max-height: 400px;max-width: 100%;' id='validateImg' class='img-responsive'/>");
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            jQuery("#dvPreview img").attr("src", e.target.result);
                        }
                        reader.readAsDataURL(jQuery(this)[0].files[0]);
                    } else {
                        jQuery("#dvPreview").html("This browser does not support FileReader.");  
                    }
                }
            } else {
                jQuery("#dvPreview").html("Please upload a valid image file.");  
            }
        } 
    });
});
</script>
<div class="container-fluid">
    <div class="happyWrap">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="lightGry validatedH3"><?php echo  $current_user->user_login; ?></h4>
                    <div class="col-sm-offset-1">	
                        <div class="col-sm-6">				
                            <div class="premium-part-main validation_process">				
                                <div class="premium-part"><h5 class="red"> Validation process</h5></div>
                                <div class="body">
                                    <p>Validation is confirmation of your  status as couple / lady and that you are a real couple / lady / gentleman. <i style="color: #fff!important">Only validated members can become a premium member.</i></p>
                                    <p>We offer three different ways to be validated.</p>
                                    <p>
                                        <span class="red">1.  Validation by self-timer (selfie) picture:</span><br>
                                        Please hold (if couple both) a sheet of paper containing following words:<br>
                                        Hedonia.ch / Member name / Date. </p>

                                    <ul class="vali-process">
                                        <li>- All three fields are handwritten</li>
                                        <li>- Eyes can be covered by sun glass / mask / bar</li>
                                        <li>- Picture in original size and not edited by Photoshop or similar programs.</li>
                                        <li>
                                            <div class="file-upload row">
                                                <div class="col-sm-3 text-right">
                                                    <!--<input type="file" class="custom-file-input" style="display: none;" id="attechPic">-->
                                                    <form id="imageform" method="post" enctype="multipart/form-data" action="" onsubmit="return false;">
                                                        <input type="file" name="photoimg" id="photoimg" style="display: none" />
                                                        <label for="photoimg" class="attechPic">
                                                            <img src="<?php echo bloginfo('template_url'); ?>/images/file-icon.jpg" alt=""/>
                                                        </label>
                                                    </form>                                                    
                                                </div>
                                                <div class="col-sm-6"><p class="attechText">Attaché your picture here</p></div>
                                                <div class="">
                                                    <button class="image-send-btn send_attach_btn" id="imageSend">send</button>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </li>
                                        <li>We will approve your picture. This picture will be exclusively <br/>used ONLY for the validation process.</li>
                                    </ul>
                                    <div class="clearfix"></div>
                                    <div class="step-2">
                                        <p>
                                            <span class="red">2. You already have an <strong>invitation code</strong></span><br/>
                                            If you already have an invitation code ( e.g. by visiting one of the our events ) please fill your personal invitation code below. If the code is correct, you will be immediately confirmed as validated couple and can directly go to the last step for premium membership. 
                                        </p>
                                        <div class="invation-code">
                                            <input type="text" class="" id="code" name="code" value=""/>
                                            <label class="">Personal invitation code</label>
                                            <button id="invite_code" class="image-send-btn-new">send</button>
                                            <div id="msg2" style="display: none" class="successMsg"></div>
                                        </div>
                                    </div>
                                    <div class="step-3">
                                        <p><span class="red">3. Transfer of your existing validation in another erotic portal</span> <br/>
                                            In some cases we accept your existing validation in another well known erotic portal. In this case please send us your validated profile name and the name of the portal. Please consider that validation based on validation in other portals is a case to case decision of our admin.</p>
                                    </div>
                                    <p class="text-right"><button id="mail_to_admin" class="mailtoadmin">Mail to admin</button></p>
                                    <div class="clearfix"></div>
                                    <p>After successful validation we will send you information about the last stage for premium membership</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-5 text-left validation_process_right_box">
                            <div id='dvPreview'>
                                <img class="img-responsive" src="<?php echo bloginfo('template_url'); ?>/images/validate-process-right-pic.jpg">
                            </div>      
                            <span class="process_img" style="display:none;">
                               <img width="60px" height="12px" src="<?php echo get_template_directory_uri(); ?>/images/ajax-status.gif">
                            </span>  
                             <div id="msg" class="successMsg"  style="margin-top:20px;"></div>            
                        </div>		
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" name="user_id" id="user_id" value="<?php echo $current_user->ID; ?>">

<script>
jQuery(document).ready(function(){
    // for image process
    jQuery("#imageSend").click(function(){
        var img = jQuery("#validateImg").attr('src');
        var userid= jQuery("#user_id").val();
        var formdata = 'action=my_action_validations_process&user_id='+userid+'&imageName='+img+'&type=picture';

        if(img ===undefined)
        {
            jQuery('#msg').html('Please Attaché your picture..!');
            setTimeout(function(){jQuery('#msg').fadeOut(10000);}, 10000);
        }else{
            jQuery.ajax({
                url:'<?php echo site_url(); ?>/wp-admin/admin-ajax.php', 
                data:formdata,
                type:'POST',
                dataType:'json',
                beforeSend: function(){
                    jQuery(".process_img").css("display", "block");
                },
                success: function (response) {
                    jQuery('#msg').html(response.err);
                    jQuery('#msg').show();
                    setTimeout(function(){jQuery('#msg').fadeOut(10000);}, 10000);
                },
                complete: function(){
                    jQuery(".process_img").css("display", "none");
                }
            });
        }
        
    });

    // for the code process
    jQuery("#invite_code").click(function(){
        var code = jQuery("#code").val();
        if(!code)
        {
            jQuery('#msg').html('please enter the invitation code..!');
            setTimeout(function(){jQuery('#msg').fadeOut(10000);}, 10000);
        }else{
            var userid= jQuery("#user_id").val();
            var formdata = 'action=my_action_validations_process&user_id='+userid+'&code='+code+'&type=code';   
            jQuery.ajax({
                url:'<?php echo site_url(); ?>/wp-admin/admin-ajax.php', 
                data:formdata,
                type:'POST',
                dataType:'json',
                beforeSend: function(){
                    jQuery(".process_img").css("display", "block");
                },
                success: function (response) {
                    jQuery('#msg').html(response.err);
                    jQuery('#msg').show();
                    setTimeout(function(){jQuery('#msg').fadeOut(10000);}, 10000);
                },
                complete: function(){
                    jQuery(".process_img").css("display", "none");
                }
            });
        }
    });

    // mail to admin
    jQuery("#mail_to_admin").click(function(){
        var mail = 1;
        var userid= jQuery("#user_id").val();
        var formdata = 'action=my_action_validations_process&user_id='+userid+'&mail='+mail+'&type=mail';   
        jQuery.ajax({
            url:'<?php echo site_url(); ?>/wp-admin/admin-ajax.php', 
            data:formdata,
            type:'POST',
            dataType:'json',
            beforeSend: function(){
                jQuery(".process_img").css("display", "block");
            },
            success: function (response) {
                jQuery('#msg').html(response.err);
                jQuery('#msg').show();
                setTimeout(function(){jQuery('#msg').fadeOut(10000);}, 10000);
            },
            complete: function(){
                jQuery(".process_img").css("display", "none");
            }
        });
    });
});

</script>

<?php get_footer(); ?>