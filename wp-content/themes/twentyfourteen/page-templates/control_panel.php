<?php
global $wpdb;

/*Template Name:My Control Panel
 * 
 * 
 * 
*/
get_header();   
global $current_user;
$user_id = $current_user->ID;
$get_user_data = get_user_meta($user_id);
//print_r($get_user_data);

?> 


            <div class="container-fluid">
                <div class="parent_div">
                    <form class="form-horizontal" role="form" name="form" method="post">
                        <div class="row">

                            <div class="col-md-4 col-sm-4 col-xs-12 margin-bottom-30">
                                <div class="control_panel">
                                    <h3 class="heading_control">Control Panel of<span><?php echo $get_user_data['nickname']['0'];  ?></span></h3>
                                </div>
                                <div class="margin_top margin-top_align">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12 col-xs-12 ">
                                            <div class="Album-Part1">
                                                <div class="row"> 
                                                    <h3 class="alb-h3-2  col-sm-12 col-md-12"><span>MY ALBUMS</span></h3>
                                                </div>
                                                <ul class="album_section">
                                                    <li>
                                                        <img src="images/alb_1.jpg">
                                                        <div class="">
                                                            <p>1</p>
                                                            <a href="#">edit</a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <img src="images/alb_2.jpg">
                                                        <div class="">
                                                            <p>2</p>
                                                            <a href="#">edit</a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <img src="images/alb_3.jpg">
                                                        <div class="">
                                                            <p>3</p>
                                                            <a href="#">edit</a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <img src="images/alb_4.jpg">
                                                        <div class="">
                                                            <p>4</p>
                                                            <a href="#">edit</a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <img src="images/alb_5.jpg">
                                                        <div class="">
                                                            <p>5</p>
                                                            <a href="#">edit</a>
                                                        </div>
                                                    </li>
                                                </ul>							
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="margin_top ">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12 col-xs-12 ">
                                            <div class="Album-Part2 album_align">
                                                <div class="row"> 
                                                    <h3 class="alb-h3-2  col-sm-4 col-md-4 bold"><span>ALBUM1</span></h3>
                                                    <div class="col-sm-8 col-md-8">
                                                        <div class="row">
                                                            <div class="col-md-2 col-sm-2"><p>Name</p></div>
                                                            <div class="col-sm-8 col-md-8  no-padding-right"><input class="form-control"/></div>
                                                            <div class="col-sm-2 col-md-2 album_section"><a class="">edit</a></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <ul class="album_section">
                                                    <li>
                                                        <img src="images/alb_1.jpg">
                                                        <div class="">										
                                                            <a href="#">1 edit</a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <img src="images/alb_2.jpg">
                                                        <div class="">										
                                                            <a href="#">2 edit</a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <img src="images/alb_3.jpg">
                                                        <div class="">
                                                            <a href="#">3 edit</a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <img src="images/alb_4.jpg">
                                                        <div class="">										
                                                            <a href="#">4 edit</a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <img src="images/alb_5.jpg">
                                                        <div class="">										
                                                            <a href="#">5 insert</a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <img src="images/alb_5.jpg">
                                                        <div class="">

                                                            <a href="#">6 insert</a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <img src="images/alb_5.jpg">
                                                        <div class="">

                                                            <a href="#">7 insert</a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <img src="images/alb_5.jpg">
                                                        <div class="">
                                                            <a href="#">8 insert</a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <img src="images/alb_5.jpg">
                                                        <div class="">										
                                                            <a href="#">9 insert</a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <img src="images/alb_5.jpg">
                                                        <div class="">
                                                            <a href="#">10 insert</a>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <div class="row">
                                                    <div class="col-sm-8 col-xs-12 col-md-8">
                                                        <div class="btn_gray margin-left-10">
                                                            <a href="#">rearrange  pictures</a>
                                                            <span>(just move to new place)</span>
                                                        </div>						
                                                    </div>	
                                                    <div class="col-sm-4 col-xs-12 col-md-3">
                                                        <a class="btn_gray_add pull-right" href="">add picture</a>						
                                                    </div>				
                                                </div>
                                                <!-- <div class="row">
                                                        <div class="col-sm-12 col-xs-12 col-md-12">
                                                                <div class="margin-left-10 check_required">
                                                                        <div class="col-sm-1 no-padding">
                                                                          <input type="checkbox" required="" id="not_married" value="married" name="married" class="checkbox_cus">
                                                                          <label for="not_married" class="checkbox_label"></label>
                                                                        </div>
                                                                        <div class="col-sm-11">
                                                                                <span>album private</span>
                                                                                <img src="images/lock.png">
                                                                                <span>without mini pre‐view of a picture</span>
                                                                        </div>
                                                                </div>
                                                                <div class="margin-left-10 check_required">
                                                                        <div class="col-sm-1 no-padding">
                                                                          <input type="checkbox" required="" id="not_married" value="married" name="married" class="checkbox_cus">
                                                                          <label for="not_married" class="checkbox_label"></label>
                                                                        </div>
                                                                        <div class="col-sm-11">
                                                                                <span>album private</span>
                                                                                <img src="images/lock.png">
                                                                                <span>with mini pre‐view of a picture</span>
                                                                        </div>
                                                                </div>
                                                        </div>											
                                                </div> -->
                                                <div class="edit-picture-border-box">	
                                                    <div class="row">
                                                        <div class="col-sm-6 col-xs-12 col-md-6">
                                                            <div class=" check_required">										
                                                                <h3>EDIT PICTURE 3</h3>
                                                                <img src="images/alb_1.jpg">
                                                            </div>									
                                                        </div>
                                                        <div class="col-sm-6 col-xs-12 col-md-6">
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-left" for="inputEmail3">rename</label>
                                                                <div class=" col-sm-9">
                                                                    <input class="form-control" type="text"/>		
                                                                </div>										
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-sm-3 yellow-light control-label no-padding-left" for="inputEmail3">delete</label>
                                                                <div class="col-sm-9">
                                                                    <input type="checkbox" required="" id="married" value="married" name="married" class="checkbox_cus">
                                                                    <label for="married" class="checkbox_label"></label>
                                                                </div>
                                                            </div>

                                                        </div>											
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-5"></div>
                                                        <div class="col-sm-7 no-padding-left">
                                                            <div class="form-group">
                                                                <label class="col-sm-5 control-label no-padding" for="inputEmail3">shift to album no</label>
                                                                <div class="col-sm-3 padding-left10">
                                                                    <input type="checkbox" required="" id="married" value="married" name="married" class="checkbox_cus">
                                                                    <label for="married" class="checkbox_label"></label>
                                                                </div>
                                                                <div class="col-md-4 col-sm-4 no-padding-left">
                                                                    <button class="orange-dark_btn margin-top4">ok</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="edit-picture-border-box">
                                                    <div class="row">
                                                        <div class="col-sm-9 col-xs-12 col-md-9">
                                                            <div class="form-group">
                                                                <label class="col-sm-10 control-label yellow-light" for="inputEmail3">DELETE ALBUM and all pictures</label>
                                                                <div class="col-sm-2 pull-right no-padding-right ">
                                                                    <input type="checkbox" required="" id="married" value="married" name="married" class="checkbox_cus">
                                                                    <label for="married" class="checkbox_label"></label>
                                                                </div>
                                                            </div>									
                                                        </div>	
                                                        <div class="col-sm-3 col-xs-12 col-md-3">
                                                            <button class="red_btn margin-top4">ok</button>							
                                                        </div>	
                                                    </div>	
                                                </div>		
                                            </div>					
                                        </div>
                                    </div>				
                                </div>
                            </div>

                            <div class="col-sm-4 col-md-4 col-xs-12 no-padding-left  margin-bottom-30">
                                <div class="margin_top">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12 col-xs-12 ">
                                            <div class="Album-Part3 green_part">
                                                <div class="row"> 
                                                    <h3 class=" alb-h3_sec col-sm-6 col-md-6"><span>SHOW / HIDE YOUR PROFILE</span></h3>
                                                    <p class="bold mini_head col-md-12 col-sm-12 no-padding-left">make your profile visible  for a specific member outside of your <a href="#">circle of selected members</a></p>
                                                </div>
                                                <div class="row">
													<div id="hide_show_msg"></div>
                                                    <div class="col-md-9 col-sm-9">
                                                        <div class="login_search">
                                                            <div class="row">
                                                                <div class="col-md-3 col-sm-3">
                                                                    <p>Members name</p>
                                                                </div>
                                                                <div class="col-md-6 col-sm-6 no-padding-right">
                                                                    <input type="text" class="form-control search" id="searchid" value=""/>
                                                                    
                                                                </div>                                                               
                                                                <div class="col-md-3 col-sm-3">
                                                                    <button class="green_btn" id ="show_hide">ok</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div id="result"></div>
                                                                 
                                                        
                                                        <div class="row">
                                                            <div class="col-md-11 col-sm-11 no-padding-right">
                                                                <p class="make_visible">make your profile <span>visible for all members of Hedonia</span></p>
                                                            </div>
                                                            <div class="col-sm-1 search-padding allow_all no-padding_how">
                                                                <input type="checkbox" class="checkbox_cus" name="married" value="married" id="all_region" required="">
                                                                <label class="checkbox_label" for="all_region"></label>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="Album-Part8 orange_part">
                                                <div class="row">
                                                    <div class="col-sm-12 col-xs-12 col-md-12">
                                                        <p class="black">Block visibility of your profile for a specific member </p>
                                                        <a href="#" class="pull-right">List of blocked members</a>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-9 col-sm-9">
                                                        <div class="login_search orange_search">
                                                            <div class="row">
																<div id="block_msg"></div>
                                                                <div class="col-md-3 col-sm-3">
                                                                    <p>Members name</p>
                                                                </div>
                                                                <div class="col-md-6 col-sm-6 no-padding-right">
                                                               
                                                                     <input type="text" class="form-control search_block" id="search_block_user" value=""/>
                                                                </div>
                                                                <div class="col-md-3 col-sm-3">
                                                                    <button class="orange_btn" id ="block_visible">ok</button>
                                                                </div>
                                                                 <div id="result_block"></div>
                                                            </div>
                                                        </div>									
                                                    </div>
                                                </div>
                                            </div>					
                                        </div>
                                    </div>
                                </div>			
                                <div class="margin_top">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12 col-xs-12 ">
                                            <div class="Album-Part4 green_part border-bottom">
                                                <div class="row"> 
                                                    <h3 class=" alb-h3_sec col-sm-6 col-md-6"><span>OPEN / CLOSE YOUR ALBUMS</span></h3>
                                                    <p class="bold mini_head col-md-12 col-sm-12 no-padding-left">									
                                                        Open specific albums for  all members of your <a href="#">circle of selected members</a>
                                                    </p>
                                                </div>
                                                <div class="row ">
                                                    <div class="col-md-11 col-sm-11">
                                                        <div class="login_search login_search_light">
                                                            <div class="row">
                                                                <div class="col-md-12 col-sm-12">
                                                                    <ul class="album_selection">
                                                                        <li>
                                                                            <div class="no-padding">
                                                                                <input type="checkbox" required="" id="1" value="married" name="married" class="checkbox_cus">
                                                                                <label for="1" class="checkbox_label"></label>
                                                                            </div>
                                                                            <p>Album1</p>
                                                                        </li>
                                                                        <li>
                                                                            <div class="no-padding">
                                                                                <input type="checkbox" required="" id="1" value="married" name="married" class="checkbox_cus">
                                                                                <label for="1" class="checkbox_label"></label>
                                                                            </div>
                                                                            <p>Album2</p>
                                                                        </li>	
                                                                        <li>
                                                                            <div class="no-padding">
                                                                                <input type="checkbox" required="" id="1" value="married" name="married" class="checkbox_cus">
                                                                                <label for="1" class="checkbox_label"></label>
                                                                            </div>
                                                                            <p>Album3</p>
                                                                        </li>	
                                                                        <li>
                                                                            <div class="no-padding">
                                                                                <input type="checkbox" required="" id="1" value="married" name="married" class="checkbox_cus">
                                                                                <label for="1" class="checkbox_label"></label>
                                                                            </div>
                                                                            <p>Album4</p>
                                                                        </li>	
                                                                        <li>
                                                                            <div class="no-padding">
                                                                                <input type="checkbox" required="" id="1" value="married" name="married" class="checkbox_cus">
                                                                                <label for="1" class="checkbox_label"></label>
                                                                            </div>
                                                                            <p>Album5</p>
                                                                        </li>	
                                                                        <li>
                                                                            <button class="green_btn">ok</button>
                                                                        </li>	
                                                                    </ul>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-9 col-sm-9">
                                                        <div class="row">
                                                            <div class="col-md-11 col-sm-11 no-padding-right">
                                                                <p class="make_visible">make marked albums visible for all members of Hedonia</p>
                                                            </div>
                                                            <div class="col-sm-1 search-padding allow_all no-padding_how">
                                                                <input type="checkbox" class="checkbox_cus" name="married" value="married" id="all_region" required="">
                                                                <label class="checkbox_label" for="all_region"></label>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>							
                                            </div>
                                            <div class="Album-Part4 green_part">
                                                <div class="row"> 
                                                    <p class="bold mini_head col-md-12 col-sm-12 no-padding-left">									
                                                        Open specific albums for specific members
                                                    </p>
                                                </div>
                                                <div class="row ">
                                                    <div class="col-md-11 col-sm-11">
                                                        <div class="login_search login_search_light">
                                                            <div class="row">
                                                                <div class="col-md-12 col-sm-12">
                                                                    <ul class="album_selection">
                                                                        <li>
                                                                            <div class="no-padding">
                                                                                <input type="checkbox" required="" id="1" value="married" name="married" class="checkbox_cus">
                                                                                <label for="1" class="checkbox_label"></label>
                                                                            </div>
                                                                            <p>Album1</p>
                                                                        </li>
                                                                        <li>
                                                                            <div class="no-padding">
                                                                                <input type="checkbox" required="" id="1" value="married" name="married" class="checkbox_cus">
                                                                                <label for="1" class="checkbox_label"></label>
                                                                            </div>
                                                                            <p>Album2</p>
                                                                        </li>	
                                                                        <li>
                                                                            <div class="no-padding">
                                                                                <input type="checkbox" required="" id="1" value="married" name="married" class="checkbox_cus">
                                                                                <label for="1" class="checkbox_label"></label>
                                                                            </div>
                                                                            <p>Album3</p>
                                                                        </li>	
                                                                        <li>
                                                                            <div class="no-padding">
                                                                                <input type="checkbox" required="" id="1" value="married" name="married" class="checkbox_cus">
                                                                                <label for="1" class="checkbox_label"></label>
                                                                            </div>
                                                                            <p>Album4</p>
                                                                        </li>	
                                                                        <li>
                                                                            <div class="no-padding">
                                                                                <input type="checkbox" required="" id="1" value="married" name="married" class="checkbox_cus">
                                                                                <label for="1" class="checkbox_label"></label>
                                                                            </div>
                                                                            <p>Album5</p>
                                                                        </li>														
                                                                    </ul>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>	
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-9 col-sm-9">
                                                        <div class="login_search margin-top-10">
                                                            <div class="row">
                                                                <div class="col-md-3 col-sm-3">
                                                                    <p>Members name</p>
                                                                </div>
                                                                <div class="col-md-6 col-sm-6 no-padding-right">
                                                                    <input class="form-control">
                                                                </div>
                                                                <div class="col-md-3 col-sm-3">
                                                                    <button class="green_btn">ok</button>
                                                                </div>
                                                            </div>
                                                        </div>									
                                                    </div>
                                                </div>							
                                            </div>						
                                            <div class="Album-Part5 orange_part border-bottom">
                                                <div class="row"> 
                                                    <p class="bold mini_head col-md-12 col-sm-12 no-padding-left">									
                                                        Close specific albums for all members of your <a href="#">circle of selected members</a>
                                                    </p>
                                                </div>
                                                <div class="row ">
                                                    <div class="col-md-11 col-sm-11">
                                                        <div class="login_search  orange_search">
                                                            <div class="row">
                                                                <div class="col-md-12 col-sm-12">
                                                                    <ul class="album_selection">
                                                                        <li>
                                                                            <div class="no-padding">
                                                                                <input type="checkbox" required="" id="1" value="married" name="married" class="checkbox_cus">
                                                                                <label for="1" class="checkbox_label"></label>
                                                                            </div>
                                                                            <p>Album1</p>
                                                                        </li>
                                                                        <li>
                                                                            <div class="no-padding">
                                                                                <input type="checkbox" required="" id="1" value="married" name="married" class="checkbox_cus">
                                                                                <label for="1" class="checkbox_label"></label>
                                                                            </div>
                                                                            <p>Album2</p>
                                                                        </li>	
                                                                        <li>
                                                                            <div class="no-padding">
                                                                                <input type="checkbox" required="" id="1" value="married" name="married" class="checkbox_cus">
                                                                                <label for="1" class="checkbox_label"></label>
                                                                            </div>
                                                                            <p>Album3</p>
                                                                        </li>	
                                                                        <li>
                                                                            <div class="no-padding">
                                                                                <input type="checkbox" required="" id="1" value="married" name="married" class="checkbox_cus">
                                                                                <label for="1" class="checkbox_label"></label>
                                                                            </div>
                                                                            <p>Album4</p>
                                                                        </li>	
                                                                        <li>
                                                                            <div class="no-padding">
                                                                                <input type="checkbox" required="" id="1" value="married" name="married" class="checkbox_cus">
                                                                                <label for="1" class="checkbox_label"></label>
                                                                            </div>
                                                                            <p>Album5</p>
                                                                        </li>	
                                                                        <li>
                                                                            <button class="orange_btn">ok</button>
                                                                        </li>	
                                                                    </ul>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-9 col-sm-9">
                                                        <div class="row">
                                                            <div class="col-md-11 col-sm-11 no-padding-right">
                                                                <p class="make_visible">close marked albums for all members of Hedonia</p>
                                                            </div>
                                                            <div class="col-sm-1 search-padding allow_all no-padding_how">
                                                                <input type="checkbox" class="checkbox_cus" name="married" value="married" id="all_region" required="">
                                                                <label class="checkbox_label" for="all_region"></label>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>							
                                            </div>				
                                            <div class=" Album-Part6 orange_part">
                                                <div class="row">
                                                    <div class="col-sm-12 col-xs-12 col-md-12">
                                                        <p class="black">Close specific albums for specific members</p>									
                                                    </div>
                                                </div>
                                                <div class="row ">
                                                    <div class="col-md-11 col-sm-11">
                                                        <div class="login_search  orange_search">
                                                            <div class="row">
                                                                <div class="col-md-12 col-sm-12">
                                                                    <ul class="album_selection">
                                                                        <li>
                                                                            <div class="no-padding">
                                                                                <input type="checkbox" required="" id="1" value="married" name="married" class="checkbox_cus">
                                                                                <label for="1" class="checkbox_label"></label>
                                                                            </div>
                                                                            <p>Album1</p>
                                                                        </li>
                                                                        <li>
                                                                            <div class="no-padding">
                                                                                <input type="checkbox" required="" id="1" value="married" name="married" class="checkbox_cus">
                                                                                <label for="1" class="checkbox_label"></label>
                                                                            </div>
                                                                            <p>Album2</p>
                                                                        </li>	
                                                                        <li>
                                                                            <div class="no-padding">
                                                                                <input type="checkbox" required="" id="1" value="married" name="married" class="checkbox_cus">
                                                                                <label for="1" class="checkbox_label"></label>
                                                                            </div>
                                                                            <p>Album3</p>
                                                                        </li>	
                                                                        <li>
                                                                            <div class="no-padding">
                                                                                <input type="checkbox" required="" id="1" value="married" name="married" class="checkbox_cus">
                                                                                <label for="1" class="checkbox_label"></label>
                                                                            </div>
                                                                            <p>Album4</p>
                                                                        </li>	
                                                                        <li>
                                                                            <div class="no-padding">
                                                                                <input type="checkbox" required="" id="1" value="married" name="married" class="checkbox_cus">
                                                                                <label for="1" class="checkbox_label"></label>
                                                                            </div>
                                                                            <p>Album5</p>
                                                                        </li>													
                                                                    </ul>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>								
                                                </div>						
                                                <div class="row">
                                                    <div class="col-md-9 col-sm-9">
                                                        <div class="login_search orange_search  margin-top-10">
                                                            <div class="row">
                                                                <div class="col-md-3 col-sm-3">
                                                                    <p>Members name</p>
                                                                </div>
                                                                <div class="col-md-6 col-sm-6 no-padding-right">
                                                                    <input class="form-control"/>
                                                                </div>
                                                                <div class="col-md-3 col-sm-3">
                                                                    <button class="orange_btn">ok</button>
                                                                </div>
                                                            </div>
                                                        </div>									
                                                    </div>
                                                </div>
                                            </div>					
                                        </div>
                                    </div>
                                </div>
                            </div>		
                            <div class="col-sm-4 col-md-4 col-xs-12 no-padding-left  margin-bottom-30">
                                <div class="margin_top">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12 col-xs-12 ">
                                            <div class="Album-Part3 green_part">
                                                <div class="row"> 
                                                    <h3 class=" alb-h3_sec2 col-sm-9 col-md-9"><span>SHOW / HIDE YOUR EROTIC PREFERENCES</span></h3>
                                                    <p class="bold mini_head col-md-12 col-sm-12 no-padding-left">Show your hidden erotic preferences for a specific member</p>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-9 col-sm-9">
                                                        <div class="login_search">
                                                            <div class="row">
                                                                <div class="col-md-3 col-sm-3">
                                                                    <p>Members name</p>
                                                                </div>
                                                                <div class="col-md-6 col-sm-6 no-padding-right">
                                                                    <input class="form-control"/>
                                                                </div>
                                                                <div class="col-md-3 col-sm-3">
                                                                    <button class="green_btn">ok</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-11 col-sm-11 no-padding-right">
                                                                <p class="make_visible">Make your erotic profile<span>visible for all members of Hedonia</span></p>
                                                            </div>
                                                            <div class="col-sm-1 search-padding allow_all no-padding_how">
                                                                <input type="checkbox" class="checkbox_cus" name="married" value="married" id="all_section_region" required="">
                                                                <label class="checkbox_label" for="all_section_region"></label>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class=" Album-Part8 orange_part">
                                                <div class="row">
                                                    <div class="col-sm-12 col-xs-12 col-md-12">
                                                        <p class="black">Close your hidden erotic preferences for a specific member </p>
                                                        <a class="pull-right">List of blocked members</a>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-9 col-sm-9">
                                                        <div class="login_search orange_search">
                                                            <div class="row">
                                                                <div class="col-md-3 col-sm-3">
                                                                    <p>Members name</p>
                                                                </div>
                                                                <div class="col-md-6 col-sm-6 no-padding-right">
                                                                    <input class="form-control"/>
                                                                </div>
                                                                <div class="col-md-3 col-sm-3">
                                                                    <button class="orange_btn">ok</button>
                                                                </div>
                                                            </div>
                                                        </div>									
                                                    </div>
                                                </div>
                                            </div>					
                                        </div>
                                    </div>
                                </div>		
                                <div class="margin_top">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12 col-xs-12 ">
                                            <div class="Album-Part7 red_part border-bottom">
                                                <div class="row"> 
                                                    <h3 class=" alb-h3_sec3 col-sm-10 col-md-10"><span>SETTINGS FOR YOUR EXTERNAL „<span class="italic">HOMEPAGE</span>“ <span class="top-text1">*)</span> </span></h3>
                                                    <p class="bold mini_head col-md-12 col-sm-12 no-padding-left white">									
                                                        You can define here visibility of your album  and  your erotic preferences by external link
                                                    </p>
                                                </div>							
                                                <div class="row">
                                                    <div class="col-md-11 col-sm-11">
                                                        <div class="login_search red_search">
                                                            <div class="row">
                                                                <div class="col-md-5 col-sm-5">
                                                                    <p>Your password for external link </p>
                                                                </div>
                                                                <div class="col-md-6 col-sm-6">
                                                                    <input class="form-control" type="text"/>
                                                                </div>											
                                                            </div>
                                                        </div>
                                                    </div>								
                                                </div>							
                                                <div class="row margin-top-10">
                                                    <div class="col-md-11 col-sm-11">
                                                        <div class="login_search red_search">
                                                            <div class="row">
                                                                <div class="col-md-12 col-sm-12">
                                                                    <ul class="album_selection">
                                                                        <li>
                                                                            <div class="no-padding">
                                                                                <input type="checkbox" required="" id="1" value="married" name="married" class="checkbox_cus">
                                                                                <label for="1" class="checkbox_label"></label>
                                                                            </div>
                                                                            <p>Album1</p>
                                                                        </li>
                                                                        <li>
                                                                            <div class="no-padding">
                                                                                <input type="checkbox" required="" id="1" value="married" name="married" class="checkbox_cus">
                                                                                <label for="1" class="checkbox_label"></label>
                                                                            </div>
                                                                            <p>Album2</p>
                                                                        </li>	
                                                                        <li>
                                                                            <div class="no-padding">
                                                                                <input type="checkbox" required="" id="1" value="married" name="married" class="checkbox_cus">
                                                                                <label for="1" class="checkbox_label"></label>
                                                                            </div>
                                                                            <p>Album3</p>
                                                                        </li>	
                                                                        <li>
                                                                            <div class="no-padding">
                                                                                <input type="checkbox" required="" id="1" value="married" name="married" class="checkbox_cus">
                                                                                <label for="1" class="checkbox_label"></label>
                                                                            </div>
                                                                            <p>Album4</p>
                                                                        </li>	
                                                                        <li>
                                                                            <div class="no-padding">
                                                                                <input type="checkbox" required="" id="1" value="married" name="married" class="checkbox_cus">
                                                                                <label for="1" class="checkbox_label"></label>
                                                                            </div>
                                                                            <p>Album5</p>
                                                                        </li>	

                                                                    </ul>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- <div class="col-md-9 col-sm-9">
                                                            <div class="row">
                                                                    <div class="col-md-11 col-sm-11 no-padding-right">
                                                                            <p class="make_visible">make marked albums visible for all members of Hedonia</p>
                                                                    </div>
                                                                    <div class="col-sm-1 search-padding allow_all no-padding_how">
                                                                      <input type="checkbox" class="checkbox_cus" name="married" value="married" id="all_region" required="">
                                                                      <label class="checkbox_label" for="all_region"></label>
                                                                    </div>
                                                                    
                                                            </div>
                                                    </div> -->
                                                </div>
                                                <div class="row margin-top-10">
                                                    <div class="col-md-7 col-sm-7">
                                                        <div class="login_search red_search">
                                                            <div class="row">
                                                                <div class="col-md-10 col-sm-10">
                                                                    <p>Show your erotic preferences externally</p>
                                                                </div>
                                                                <div class="col-md-2 col-sm-2 no-padding-left">
                                                                    <div class=" search-padding allow_all no-padding_how">
                                                                        <input type="checkbox" required="" id="all_region" value="married" name="married" class="checkbox_cus">
                                                                        <label for="all_region" class="checkbox_label"></label>
                                                                    </div>
                                                                </div>											
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5 col-sm-5 no-padding-left">
                                                        <div class="red_active">									
                                                            <p>Activate external link</p>
                                                            <button class="red_btn">ok</button>			
                                                        </div>
                                                    </div>	
                                                </div>							
                                            </div>														
                                        </div>
                                    </div>
                                </div>
                                <div class="margin_top">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12 col-xs-12 ">
                                            <div class="Album-Part10 profile_status control-last-box">
                                                <div class="row"> 
                                                    <div class="  col-sm-8 col-md-8 no-padding-left">
                                                        <h3 class="alb-h3_sec"><span>PROFILE STATUS</span></h3>
                                                    </div>
                                                    <div class="col-sm-12 col-md-12">
                                                        <div class="row margin-top-10">
                                                            <div class="col-sm-8 col-md-8">
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <p>Type of your membership</p>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <input class="form-control"  value="<?php echo $get_user_data['user_type_data']['0']; ?>"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-8 col-md-8">
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <p>Membership fee paid until</p>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <input class="form-control" value="--/--/201-"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-12 col-md-12">
                                                                <div class="row">
                                                                    <div class="col-md-3 no-padding-right">
                                                                        <p>Your external link</p>
                                                                    </div>
                                                                    <div class="no-padding-left col-md-9 back_red">
                                                                        <input class="form-control" type="text" value="http://www.hedonia.ch/members/hotswisscouple/zklm214"/>
                                                                        <a href="#">copy link</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="edit-profile-border-box">
															<div id="dec_msg"></div>
                                                            <div class="row">
                                                                <div class="col-sm-12 col-md-12">
                                                                    <div class="row">
                                                                        <div class="col-md-4">
                                                                            <p class="deactive">Deactivate your profile</p>
                                                                        </div>
                                                                        <div class="no-padding-left col-md-8 back_red">
                                                                            <div class="col-sm-4">
                                                                                <input type="checkbox" class="checkbox_cus" name="deactivate_profile" value="1" id="deactivate_profile" <?php if($get_user_data['ja_disable_user']['0']==1){echo 'checked';}  ?>>
                                                                                <label class="checkbox_label" for="deactivate_profile"></label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="no-padding-left col-md-9 back_red">
                                                                            <span>*)&nbsp;without losing your pictures, validation, erotic profile, contacts, favorites and more.  In this time profile is inactive = invisible  and can be reactivated anytime </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row margin-top-10">
                                                                <div class="col-sm-12 col-md-12">
                                                                    <div class="row">
                                                                        <div class="col-md-4">
                                                                            <p class="reactivate">Reactivate profile</p>
                                                                        </div>
                                                                        <div class="no-padding-left col-md-5 back_red">
                                                                            <div class="col-sm-4">
                                                                                <input type="checkbox" class="checkbox_cus" name="deactivate_profile" value="0" id="reactivate"<?php if($get_user_data['ja_disable_user']['0']==0){echo 'checked';}  ?>>
                                                                                <label class="checkbox_label" for="reactivate"></label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <button class="gray_btn margin-top4" id="deactivate">ok</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="edit-profile-border-red-box">
															<div id ="delete_profile_msg"></div>
                                                            <div class="row margin-top-10">
                                                                <div class="col-sm-12 col-md-12">
                                                                    <div class="row">
                                                                        <div class="col-sm-4">
                                                                            <p class="delete_acc yellow-light ">DELETE PROFILE</p>
                                                                        </div>
                                                                        <div class="no-padding-left col-sm-2 back_red">
                                                                            <div class="col-sm-12">
                                                                                <input type="checkbox" class="checkbox_cus" name="delete_profile" value="1" id="delete_profile" >
                                                                                <label class="checkbox_label" for="delete_profile"></label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-3 no-padding-left">
                                                                            <p class="delete_acc yellow-light Font-size8">Are you sure ?</p>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <button class="red_btn margin-top4" id ="remove_profile">ok</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>														
                                            </div>						
                                        </div>
                                    </div>
                                </div>		
                            </div>

                        </div>
                        <input type="hidden" name ="current_user_id" value="<?php echo $user_id; ?>" id ="current_user_id">
                    </form>
                </div>
            </div>
        </div>
  
  
  
<!--------------------------------------Section for SHOW / HIDE YOUR PROFILE---------------------------------------->  
        
<script type="text/javascript">
$(function(){
$(".search").keyup(function() 
{ 
var searchid = $(this).val();
var dataString = 'search='+ searchid;
if(searchid!='')
{
    $.ajax({
    type: "POST",
    url: "<?php echo bloginfo('template_url'); ?>/page-templates/search.php",
    data: dataString,
    cache: false,
    success: function(html)
    {
    $("#result").html(html).show();
    }
    });
}return false;    
});

jQuery("#result").live("click",function(e){ 
    var $clicked = $(e.target);    
    var $name = $clicked.find('.name').html();    
    var decoded = $("<div/>").html($name).text();
    $('#searchid').val(decoded);
});
jQuery(document).live("click", function(e) { 
    var $clicked = $(e.target);
    if (! $clicked.hasClass("search")){
    jQuery("#result").fadeOut(); 
    }
});
$('#searchid').click(function(){
    jQuery("#result").fadeIn();
});
});
</script>

<script language="javascript" type="text/javascript">
						$(document).ready(function () {
						$("#show_hide").click(function(){
						//var checkedValue = $('.checkbox_cus:checked').val();
						var hide_user_id   = $("#search_user_id").val();
						var hidden_by = $("#current_user_id").val();
						var searchid  = $("#searchid").val(); 
						//alert(hidden_by);
						//alert(hide_user_id);
						//alert(searchid);
						
						formData = '&hide_user_id='+hide_user_id+'&hidden_by='+hidden_by+'&searchid='+searchid;
						  $.ajax({
							  type: 'POST', 
							  data:formData,
							  url:"<?php echo bloginfo('template_url');  ?>/hide_show_profile.php",
							  success:function(result){
								  jQuery("#hide_show_msg").html(result);
								}
						  });
						
						});
						});
                        </script>




 <!--------------------------------------Blog visibility section-------------------------------------------------------->
  <script type="text/javascript">
$(function(){
$(".search_block").keyup(function() 
{ 
var searchid = $(this).val();
var dataString = 'search_block='+ searchid;
if(searchid!='')
{
    $.ajax({
    type: "POST",
    url: "<?php echo bloginfo('template_url'); ?>/page-templates/search_user.php",
    data: dataString,
    cache: false,
    success: function(html)
    {
    $("#result_block").html(html).show();
    }
    });
}return false;    
});

jQuery("#result_block").live("click",function(e){ 
    var $clicked = $(e.target);    
    var $name = $clicked.find('.name').html();    
    var decoded = $("<div/>").html($name).text();
    $('#search_block_user').val(decoded);
});
jQuery(document).live("click", function(e) { 
    var $clicked = $(e.target);
    if (! $clicked.hasClass("search_block")){
    jQuery("#result_block").fadeOut(); 
    }
});
$('#search_block_user').click(function(){
    jQuery("#result_block").fadeIn();
});
});
</script>

<script language="javascript" type="text/javascript">
						$(document).ready(function () {
						$("#block_visible").click(function(){
						//var checkedValue = $('.checkbox_cus:checked').val();
						var hidden_by = $("#current_user_id").val();
						var searchid  = $("#search_block_user").val(); 
						//alert(hidden_by);
						//alert(hide_user_id);
						alert(searchid);
						formData = '&searchid='+searchid+'&hidden_by='+hidden_by;
						  $.ajax({
							  type: 'POST', 
							  data:formData,
							  url:"<?php echo bloginfo('template_url');  ?>/blog_visible.php",
							  success:function(result){
								  jQuery("#block_msg").html(result);
								}
						  });
						
						});
						});
                        </script>

  
<!--------------------------------------Blog visibility section-------------------------------------------------------->
  




  <!--------------------------------------EndSection for SHOW / HIDE YOUR PROFILE---------------------------------------->
  
 
  <!--------------------------------------Profile status section-------------------------------------------------------->
  
  <script language="javascript" type="text/javascript">
						$(document).ready(function () {
						$("#deactivate").click(function(){
						//var checkedValue = $('.checkbox_cus:checked').val();
						var deleted_id = new Array();
						$('input[name="deactivate_profile"]:checked').each(function() {
						deleted_id.push(this.value);
						});
						alert(deleted_id);
						var reactivate   = $("#reactivate").val();
						var current_user_id = $("#current_user_id").val();
						alert(current_user_id);
						formData = '&deleted_id='+deleted_id+'&current_user_id='+current_user_id;
						  $.ajax({
							  type: 'POST', 
							  data:formData,
							  url:"<?php echo bloginfo('template_url');  ?>/deactivate_user_profile.php",
							  success:function(result){
								  jQuery("#dec_msg").html(result);
								}
						  });
						
						});
						});
                        </script>
				
  
  
  
  
  
  
  <!--------------------------------------End -Profile status section-------------------------------------------------------->
  
  
  
  
  
  
  
  <!------------------------------------Delete Profile------------------------------------------------------------------------>
  
  
  
  <script language="javascript" type="text/javascript">
						$(document).ready(function () {
						$("#remove_profile").click(function(){
						//var checkedValue = $('.checkbox_cus:checked').val();
						var deleted_profile_id = new Array();
						$('input[name="delete_profile"]:checked').each(function() {
						deleted_profile_id.push(this.value);
						});
						alert(deleted_profile_id);
						var current_user_id = $("#current_user_id").val();
						alert(current_user_id);
						formData = '&deleted_profile_id='+deleted_profile_id+'&current_user_id='+current_user_id;
						  $.ajax({
							  type: 'POST', 
							  data:formData,
							  url:"<?php echo bloginfo('template_url');  ?>/delete_user_profile.php",
							  success:function(result){
								  jQuery("#delete_profile_msg").html(result);
								}
						  });
						
						});
						});
                        </script>
  
                                           
<?php get_footer(); ?>
