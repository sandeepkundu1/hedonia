<?php
/*
 * Template Name: Activate Account
 * Description: A Page Template with activate account.
 * Author Name : Sanjay Shinghania
 */

get_header();

$user_id = filter_input( INPUT_GET, 'user', FILTER_VALIDATE_INT, array( 'options' => array( 'min_range' => 1 ) ) );
$key = filter_input( INPUT_GET, 'key' );
?>


<div class="text-center succesPoint">
	<h4>
	<?php
		if ( $user_id ) {
		    $code = get_user_meta($user_id, 'has_to_be_activated', true );
		    if ($code == $key) {
		        delete_user_meta( $user_id, 'has_to_be_activated' );
		        echo 'Please login and fill your profile. Than please wait for activation of your basic profile and profile Photo(s) by our admin.';
		    }
		    else
		    {
		    	echo 'Your account already has been activated..';
		    }
		}
	?>
	</h4>
</div>


<?php get_footer(); ?>