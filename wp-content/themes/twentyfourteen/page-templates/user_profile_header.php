<link href="<?php echo get_template_directory_uri(); ?>/page-templates/css/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css"/>
<script src="<?php echo get_template_directory_uri(); ?>/page-templates/js/jquery.mCustomScrollbar.min.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/page-templates/js/jquery.mousewheel.min.js" type="text/javascript"></script>

<?php
global $current_user;
get_currentuserinfo();
// print_r($current_user); 
$current_user->ID;

$get_user_type = get_user_meta($current_user->ID);

//echo'<pre>'; print_r($get_user_type);
?>

<nav role="navigation" class="navbar">  
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="<?php echo home_url(); ?>" class="navbar-brand"><?php echo do_shortcode('[site_logo]'); ?></a>
        </div>
        <div class="navbar-collapse collapse" id="navbar">
            <?php
            $user_roles = $current_user->roles['0'];
            if($get_user_type['member_type']['0']== "couples" && is_user_logged_in() && $get_user_type['approved_as_basic']['0']=="no"){
				
			$defaults = array(
                    'theme_location' => '',
                    'menu' => 'user_admin_menu',
                    'container' => 'div',
                    'container_class' => '',
                    'container_id' => '',
                    'menu_class' => 'menu',
                    'menu_id' => '',
                    'echo' => true,
                    'fallback_cb' => 'wp_page_menu',
                    'before' => '',
                    'after' => '',
                    'link_before' => '',
                    'link_after' => '',
                    'items_wrap' => '<ul id="%1$s" class="nav navbar-nav profile-nav disable-btn">%3$s</ul>',
                    'depth' => 0,
                    'walker' => ''
                );
                echo wp_nav_menu($defaults);	
				
			}
			elseif($get_user_type['member_type']['0']== "gentleman" && is_user_logged_in() && $get_user_type['approved_as_basic']['0']=="no")
			{
				
				$defaults = array(
                    'theme_location' => '',
                    'menu' => 'gentlemen_menu',
                    'container' => 'div',
                    'container_class' => '',
                    'container_id' => '',
                    'menu_class' => 'menu',
                    'menu_id' => '',
                    'echo' => true,
                    'fallback_cb' => 'wp_page_menu',
                    'before' => '',
                    'after' => '',
                    'link_before' => '',
                    'link_after' => '',
                    'items_wrap' => '<ul id="%1$s" class="nav navbar-nav profile-nav disable-btn">%3$s</ul>',
                    'depth' => 0,
                    'walker' => ''
                );
                echo wp_nav_menu($defaults);
				
				}
			elseif($get_user_type['member_type']['0']== "ladies" && is_user_logged_in() && $get_user_type['approved_as_basic']['0']=="no")
			{
				
				$defaults = array(
                    'theme_location' => '',
                    'menu' => 'Ladies_menu',
                    'container' => 'div',
                    'container_class' => '',
                    'container_id' => '',
                    'menu_class' => 'menu',
                    'menu_id' => '',
                    'echo' => true,
                    'fallback_cb' => 'wp_page_menu',
                    'before' => '',
                    'after' => '',
                    'link_before' => '',
                    'link_after' => '',
                    'items_wrap' => '<ul id="%1$s" class="nav navbar-nav profile-nav disable-btn">%3$s</ul>',
                    'depth' => 0,
                    'walker' => ''
                );
                echo wp_nav_menu($defaults);
				
				}
				
					
            
            elseif ($get_user_type['member_type']['0']== "couples" && is_user_logged_in()) {

                $defaults = array(
                    'theme_location' => '',
                    'menu' => 'user_admin_menu',
                    'container' => 'div',
                    'container_class' => '',
                    'container_id' => '',
                    'menu_class' => 'menu',
                    'menu_id' => '',
                    'echo' => true,
                    'fallback_cb' => 'wp_page_menu',
                    'before' => '',
                    'after' => '',
                    'link_before' => '',
                    'link_after' => '',
                    'items_wrap' => '<ul id="%1$s" class="nav navbar-nav profile-nav">%3$s</ul>',
                    'depth' => 0,
                    'walker' => ''
                );
                echo wp_nav_menu($defaults);
            } elseif ($get_user_type['member_type']['0']== "ladies" && is_user_logged_in()) {
                $defaults = array(
                    'theme_location' => '',
                    'menu' => 'Ladies_menu',
                    'container' => 'div',
                    'container_class' => '',
                    'container_id' => '',
                    'menu_class' => 'menu',
                    'menu_id' => '',
                    'echo' => true,
                    'fallback_cb' => 'wp_page_menu',
                    'before' => '',
                    'after' => '',
                    'link_before' => '',
                    'link_after' => '',
                    'items_wrap' => '<ul id="%1$s" class="nav navbar-nav profile-nav">%3$s</ul>',
                    'depth' => 0,
                    'walker' => ''
                );

                echo wp_nav_menu($defaults);
            }
            
            elseif($get_user_type['member_type']['0']== "gentleman" && is_user_logged_in())
            {
				$defaults = array(
                    'theme_location' => '',
                    'menu' => 'gentlemen_menu',
                    'container' => 'div',
                    'container_class' => '',
                    'container_id' => '',
                    'menu_class' => 'menu',
                    'menu_id' => '',
                    'echo' => true,
                    'fallback_cb' => 'wp_page_menu',
                    'before' => '',
                    'after' => '',
                    'link_before' => '',
                    'link_after' => '',
                    'items_wrap' => '<ul id="%1$s" class="nav navbar-nav profile-nav">%3$s</ul>',
                    'depth' => 0,
                    'walker' => ''
                );

                echo wp_nav_menu($defaults);
				
				
				}
             else {
                wp_redirect(home_url());
            }
            ?>

                                

        </div>
    </div><!--/.nav-collapse -->
</div><!--/.container-fluid -->
</nav>
<div class="container-fluid">
<div class="selectLanguages">
    <span>Change languages</span>
    <div  class="languageList">
        <?php echo do_shortcode('[tp widget="flags/tpw_flags_css.php"]'); ?>
    </div>
</div>
</div>
