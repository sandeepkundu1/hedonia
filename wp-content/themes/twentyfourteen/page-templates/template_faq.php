<?php
/*
 *Template Name: FAQ
 * 
 * 
 * 
*/
get_header();
?>
<?php require_once('user_profile_header.php');  ?>

<div class="container">
<div class="faqWrap">
  <?php echo do_shortcode('[faq]'); ?>
</div>
</div>

<?php get_footer(); ?>
