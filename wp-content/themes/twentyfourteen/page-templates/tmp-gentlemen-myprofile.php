<?php
/*
 * Template Name: Gentlemen My Profile
 * Description: A Page Template with a Change Profile Staus.
 * Author Name : Sanjay Shinghania
 */
global $wpdb;
get_header();

  $textColor = array(
            "182" => "#395723", 
            "183" => "#476D2D", 
            "184" => "#61953D", 
            "185" => "#A9D12A",
            "186" => "#E2F0D9", 
            "187" => "#ECF5E7", 
            "706" => "#F8FBF7", 
            "707" => "#F2F2F2", 
            "708" => "#F2F2F2", 
            "709" => "#F2F2F2", 
            "710" => "#F2F2F2", 
            "711" => "#F2F2F2", 
            "712" => "#F2F2F2", 
            "713" => "#F2F2F2", 
            "714" => "#F2F2F2", 
            "715" => "#FF3F3F",
            "716" => "#C00000"             
        );

    $forColor = array(
            "182" => "#fff", 
            "183" => "#fff", 
            "184" => "#fff", 
            "185" => "#fff",
            "186" => "#000", 
            "187" => "#000", 
            "706" => "#000", 
            "707" => "#000", 
            "708" => "#000", 
            "709" => "#000", 
            "710" => "#000", 
            "711" => "#000", 
            "712" => "#000", 
            "713" => "#000", 
            "714" => "#000", 
            "715" => "#000", 
            "716" => "#000"            
        );
    
    $get_user_info = get_user_meta($current_user->ID);

    $user_type_name =  get_user_meta($current_user->ID ,'user_type', true );
    $user_alb_count=$album_setting[0]->$user_type_name;
    # ===== Profilepick for couples
    $upload_dir = wp_upload_dir();
    $image_url_male = $upload_dir['baseurl'] . '/profilepick_' . $current_user->ID . '/' . $get_user_info['his_pick']['0'];


    $age2 = $get_user_info['he_dob']['0'];
    $current_date = date();

    $datetime3 = new DateTime($current_date);
    $datetime4 = new DateTime($age2);
    $his_age = $datetime3->diff($datetime4);
    $male_age = $his_age->y;

    $his_hight = $get_user_info['his_hight']['0'];
    $his_figure = $get_user_info['his_figure']['0'];
    $explodeHisFigure = explode('(',$his_figure);

    $his_smoke = $get_user_info['his_smoke']['0'];
    $his_language = $get_user_info['his_language']['0'];
    $secondLang =  get_user_meta($current_user->ID ,'language_two', true );
    $thrd_lang = get_user_meta($current_user->ID ,'language_thre', true );
    $forLang =  get_user_meta($current_user->ID ,'language_for', true );
    $selectedLanguages = "";
    if($his_language != ""){
    $selectedLanguages .= $his_language . ",";
    }
    if($secondLang != ""){
    $selectedLanguages .= $secondLang . ",";
    }
    if($thrd_lang != ""){
    $selectedLanguages .= $thrd_lang . ",";
    }
    if($forLang != ""){
    $selectedLanguages .= $forLang . ",";
    }

    $his_weight = $get_user_info['his_weight']['0'];
    $his_profession = $get_user_info['his_profession']['0'];
    $his_hobby = $get_user_info['his_hobby']['0'];
    $describe = $get_user_info['describe']['0'];
    $des_status = $get_user_info['describe_status']['0'];

    $fav_quote_content = $get_user_info['faq_quote']['0'];

    $nickname = $get_user_info['nickname']['0'];
    $first_name = $get_user_info['first_name']['0'];
    $last_name = $get_user_info['last_name']['0'];

    $country = $get_user_info['country']['0'];
    $second_country = $get_user_info['country_second']['0'];

    $district1 = $get_user_info['district1']['0'];
    $second_district = $get_user_info['district_second']['0'];

    $city1          = $get_user_info['city1']['0'];
    $second_city    = $get_user_info['city_second']['0'];
    $reg3_txtshe    = $get_user_info['reg3_txtshe']['0'];
    $reg3_txthe     = $get_user_info['reg3_txthe']['0'];
    $reg3_txtboth   = $get_user_info['reg3_both']['0'];

    #====== End Age for couples

    #change user type
    $user_type_name =  get_user_meta($current_user->ID ,'user_type', true );
    $profile_name = get_option($user_type_name);

    $appproved = get_user_meta($current_user->ID,'approved_as_basic',true);
//perfect match
    $setting = $wpdb->get_results("select * from wp_admin_portal_setting where portal_setting_type = 'portal setting' ");
    
    $id = get_the_ID();
    $page_title = get_page($id);
    $page_title->post_title;

if (is_user_logged_in()) 
{
?>
<div class="userProfile">
    <div class="container-single-profile">
        <form class="form-horizontal" role="form" name="form" method="post">
            <div class="row">
                <div class="col-sm-3">   
                    <?php if($appproved == 'no'){ ?>                             
                    <div class="col-sm-10 no-padding profilePicWrap">                           
                        
                        <h3 class="search_premium_heading blue_heading">
                            <?php echo $get_user_info['nickname']['0']; ?>
                        </h3>                         
                        <img class="prof-cartoon-img" src="<?php echo $image_url_male; ?>">
                    </div>
                    <?php } else { ?>
                    <div class="col-sm-10 no-padding profilePicWrap">                           
                        <h3 class="search_premium_heading blue_heading">
                            <?php echo $get_user_info['nickname']['0']; ?>
                        </h3>                         
                        <img class="prof-cartoon-img" src="<?php echo $image_url_male; ?>">
                    </div>
                    <?php } ?>  

                    <div class="clear"></div>
                    <div class="list_div">
                        <ul class="side-bar-profile no-padding">
                            <?php if($appproved == 'no'){ ?>
                                <li class="invalid"><span class="grayTrans"></span>
                                    <a href="javascript:void(0)"><i>
                                        <img src="<?php echo bloginfo("template_url"); ?>/images/hand.jpg">
                                        </i><span>i like add to favourites</span>
                                    </a>
                                </li>
                                <li class="invalid">
                                    <span class="grayTrans"></span>
                                    <a href="javascript:void(0)"><i><img src="<?php echo bloginfo("template_url"); ?>/images/lip.jpg"></i>
                                        <span>send a kiss</span>
                                    </a>
                                </li>
                                <li class="invalid">
                                    <span class="grayTrans"></span>
                                    <a href="javascript:void(0)">
                                        <i><img src="<?php echo bloginfo("template_url"); ?>/images/mes1.jpg"></i>
                                        <span>send mail</span>
                                    </a>
                                </li>
                                <li class="invalid">
                                    <span class="grayTrans"></span>
                                    <a href ="#">
                                        <i><img src="<?php echo bloginfo("template_url"); ?>/images/mes3.jpg"></i>
                                        <span>mail history</span>
                                    </a>
                                </li>
                                <li class="invalid">
                                    <span class="grayTrans"></span>
                                    <a href="javascript:void(0)">
                                        <i><img src="<?php echo bloginfo("template_url"); ?>/images/lockband.jpg"></i>
                                        <span>hide/show profile<br/> open/close albums<br/> erotic preferences</span>
                                    </a>
                                </li>
                                <li class="invalid">
                                    <span class="grayTrans"></span>
                                    <a href="javascript:void(0)">
                                        <i><img src="<?php echo bloginfo("template_url"); ?>/images/note.jpg"></i>
                                        <span>make a note<br/> for this profile</span>
                                    </a>
                                </li>
                                <li class="invalid">
                                    <span class="grayTrans"></span>
                                    <a href="javascript:void(0)">
                                        <i><img src="<?php echo bloginfo("template_url"); ?>/images/block.jpg"></i>
                                        <span>block this profile</span>
                                    </a>
                                </li>
                                <li class="invalid">
                                    <span class="grayTrans"></span>
                                    <a href="javascript:void(0)">
                                        <i><img src="<?php echo bloginfo("template_url"); ?>/images/submit.jpg"></i>
                                        <span>submit complaint about this profile</span>
                                    </a>
                                </li>                                           
                            <?php  } else //elseif ($user_type_name == 'premium')
                             { ?>
                               <?php  if($operational_setting[3]->$user_type_name != 1){ ?>
                                <li style="position:relative;"> 
                                    <span class="grayTrans"></span>
                                <?php } else { ?>
                                    <li>
                                <?php } ?>  
                                    <a href = "#"><i>
                                        <img src="<?php echo bloginfo("template_url"); ?>/images/hand.jpg">
                                        </i><span>i like<br/>   add to favourites</span>
                                    </a>
                                </li>
                                 
                               <?php  if($operational_setting[5]->$user_type_name != 1){ ?>
                                <li style="position:relative;"> 
                                    <span class="grayTrans"></span>
                                <?php } else { ?>
                                    <li>
                                <?php } ?>  
                                    <a href = "#">
                                        <i><img src="<?php echo bloginfo("template_url"); ?>/images/lip.jpg"></i>
                                        <span>send a kiss</span>
                                    </a>
                                </li>
                  
                                    <li>
                                
                                    <a href ="#" >
                                        <i><img src="<?php echo bloginfo("template_url"); ?>/images/mes1.jpg"></i>
                                        <span>send mail</span>
                                    </a>
                                </li>
                               <?php  if($operational_setting[2]->$user_type_name < 0 ){ ?>
                                <li style="position:relative;"> 
                                    <span class="grayTrans"></span>
                                <?php } else { ?>
                                    <li>
                                <?php } ?>  
                                    <i><img src="<?php echo bloginfo("template_url"); ?>/images/mes3.jpg"></i>
                                    <span>mail history</span>
                                </li>       
                                <li>
                                    <?php echo ($user_type_name == 'premium') ? "" : "<span style='height:50px;' class='grayTrans'></span>"; ?>
                                    <i><img src="<?php echo bloginfo("template_url"); ?>/images/lockband.jpg"></i>
                                    <span>hide/show profile<br/> open/close albums<br/> erotic preferences</span>
                                </li>
                                 <?php  if($operational_setting[6]->$user_type_name != 1){ ?>
                                <li style="position:relative;"> 
                                    <span class="grayTrans"></span>
                                <?php } else { ?>
                                    <li>
                                <?php } ?>  
                                    <i><img src="<?php echo bloginfo("template_url"); ?>/images/note.jpg"></i>
                                    <span>make a note <br/>for this profile</span>
                                </li>
      
							<?php if ($get_user_info['user_type']['0'] == 'basic') {?>     
                                <li style="position:relative;"> 
									<span class="grayTrans"></span>	
							
                                    <i><img src="<?php echo bloginfo("template_url"); ?>/images/block.jpg"></i>
                                    <span>block this profile</span>
                                </li>
                     
                                <li style="position:relative;"> 
									<span class="grayTrans"></span>	
							
                                    <i><img src="<?php echo bloginfo("template_url"); ?>/images/submit.jpg"></i>
                                    <span>submit complaint<br/> about this profile</span>
                                </li>
                                <?php } else { ?>
                                
                                <li> 
                                    <i><img src="<?php echo bloginfo("template_url"); ?>/images/block.jpg"></i>
                                    <span>block this profile</span>
                                </li>
                                <li> 
							     <i><img src="<?php echo bloginfo("template_url"); ?>/images/submit.jpg"></i>
                                    <span>submit complaint<br/> about this profile</span>
                                </li>
                                <?php } }/* elseif ($get_user_info['user_type']['0'] == 'validated') { ?>
                                <li>
                                    <a href = "#">
                                        <i><img src="<?php echo bloginfo("template_url"); ?>/images/hand.jpg"></i>
                                        <span>i like<br/>   add to favourites</span>
                                    </a>
                                </li>
                                <li class="invalid">
                                    <span class="grayTrans"></span>
                                    <a href = "#">
                                        <i><img src="<?php echo bloginfo("template_url"); ?>/images/lip.jpg"></i>
                                        <span>send a kiss</span>
                                    </a>
                                </li>
                                <li>
                                    <a href ="#" >
                                        <i><img src="<?php echo bloginfo("template_url"); ?>/images/mes1.jpg"></i>
                                        <span>send mail</span>
                                    </a>
                                </li>
                                <li>
                                    <a href ="#" >
                                        <i><img src="<?php echo bloginfo("template_url"); ?>/images/mes3.jpg"></i>
                                        <span>mail history</span>
                                    </a>
                                </li>
                                <li class="invalid">
                                    <div class="clear"></div>
                                    <span class="grayTrans"></span>
                                    <a href ="#" >
                                        <i><img src="<?php echo bloginfo("template_url"); ?>/images/lockband.jpg"></i>
                                        <span>hide/show profile<br/> open/close albums<br/> erotic preferences</span>
                                    </a>
                                </li>
                                <li class="invalid">
                                    <div class="clear"></div>
                                    <span class="grayTrans"></span>
                                    <a href ="#" >
                                        <i><img src="<?php echo bloginfo("template_url"); ?>/images/note.jpg"></i>
                                        <span>make a note <br/>for this profile</span>
                                    </a>
                                </li>
                                <li>
                                    <a href ="#" >
                                        <i><img src="<?php echo bloginfo("template_url"); ?>/images/block.jpg"></i>
                                        <span>block this profile</span>
                                    </a>
                                </li>
                                <li>
                                    <a href ="#" ><i>
                                        <img src="<?php echo bloginfo("template_url"); ?>/images/submit.jpg"></i>
                                        <span>submit complaint<br/> about this profile</span>
                                    </a>
                                </li>
                            <?php } elseif ($user_type_name == 'basic') { ?>
                                <li class="invalid">
                                    <span class="grayTrans"></span>
                                    <a href = "#">
                                        <i><img src="<?php echo bloginfo("template_url"); ?>/images/hand.jpg"></i>
                                        <span>i like add to favourites</span>
                                    </a>
                                </li>
                                <li class="invalid">
                                    <span class="grayTrans"></span>
                                    <a href = "#"><i><img src="<?php echo bloginfo("template_url"); ?>/images/lip.jpg"></i>
                                        <span>send a kiss</span>
                                    </a>
                                </li>
                                <li>
                                    <a href ="#" >
                                        <i><img src="<?php echo bloginfo("template_url"); ?>/images/mes1.jpg"></i>
                                        <span>send mail</span>
                                    </a>
                                </li>
                                <li>
                                    <a href ="#" >
                                        <i><img src="<?php echo bloginfo("template_url"); ?>/images/mes3.jpg"></i>
                                        <span>mail history</span>
                                    </a>
                                </li>
                                <li class="invalid">
                                    <span class="grayTrans"></span>
                                    <a href ="#" ><i><img src="<?php echo bloginfo("template_url"); ?>/images/lockband.jpg"></i>
                                        <span>hide/show profile<br/> open/close albums<br/> erotic preferences</span>
                                    </a>
                                </li>
                                <li class="invalid">
                                    <span class="grayTrans"></span>
                                    <a href ="#" >
                                        <i><img src="<?php echo bloginfo("template_url"); ?>/images/note.jpg"></i>
                                        <span>make a note<br/> for this profile</span>
                                    </a>
                                </li>
                                <li class="invalid">
                                    <span class="grayTrans"></span>
                                    <a href ="#" >
                                        <i><img src="<?php echo bloginfo("template_url"); ?>/images/block.jpg"></i>
                                        <span>block this profile</span>
                                    </a></li>
                                <li class="invalid">
                                    <span class="grayTrans"></span>
                                    <a href ="#" >
                                        <i><img src="<?php echo bloginfo("template_url"); ?>/images/submit.jpg"></i>
                                        <span>submit complaint about this profile</span>
                                    </a>
                                </li>
                            <?php } */ ?>
                                    </ul>
                    </div> <!--list div -->
                    <div class="clear"></div>
                    <hr/>
                    <span class="border-bottom"></span>
                        <?php if($operational_setting[9]->$user_type_name  == 1 && $get_user_info['approved_as_basic']['0'] != 'no'){
                                    $user_type_name=$get_user_info['user_type']['0'];
                                 ?>
                                    <div id="addText" class="text-box-round">                                        
                                        <?php 
                                        $dating_ads=$wpdb->get_results("SELECT * FROM wp_dating_ads WHERE user_id=".$current_user->ID." ORDER BY ID DESC LIMIT 0,2");
                                        $upload_dir = wp_upload_dir();
                                        $img_srcpath = $upload_dir['baseurl'] . '/dating_ads/'; ?>
                                        <h5>MY SPEED DATING</h5>
                                    <?php foreach ($dating_ads as $value) { 
                                          $from=strtotime($value->from);
                                          $to=strtotime($value->to); 
                                          $from_m=date('m', $from);
                                          $monthName_from = date('F', mktime(0, 0, 0, $from_m, 10));
                                          $to_m=date('m', $to);
                                          $monthName_to = date('F', mktime(0, 0, 0, $to_m, 10));
                                          $from_y=date('Y', $from);
                                          $to_y=date('Y', $to);
                                          $from_d=date('d', $from);
                                          $to_d=date('d',$to);
                                          $country=$value->country;
                                          if($value->region != "")
                                            {
                                                $city= ' - ' . $value->region;
                                            }else
                                            {
                                               $city= ""; 
                                            }
                                     ?>
                                    <div class="form-group"> 
                                       <div class="adds_content no-padding-left col-sm-7">
                                       <p>
                                       <?php echo $country; ?>
                                       <span class="city_white"><?php echo $city; ?></span><br/>
                                       <?php echo $from_d."-".$to_d; ?>
                                       <?php if($from_m==$to_m){ echo $monthName_from; }
                                       else{ echo $monthName_from."-".$monthName_to; }
                                       echo $to_y; ?>
                                       </p>
                                       <a href="<?php echo home_url(); ?>/?page_id=735&user_id=<?php echo $current_user->ID; ?>" class="goAdd pull-right">go to ads</a>
                                       </div>
                                       <div class="col-sm-5">
                                          <img src="<?php echo $img_srcpath.$value->ads_pick; ?>" alt="adds image" class="img-responsive">
                                       </div>
                                     </div>
                                   <?php } ?>
                                    </div>
                                    <?php } else { ?>
                                    <div class="text-box-round invalid">
                                        <span class="grayTrans"></span>
                                        <?php $speed_dating = get_post(362); ?>
                                        <h5>MY SPEED DATING</h5>
                                        <p><?php // echo $get_user_info['speed_data']['0']; ?></p>
                                    </div>
                                    <?php } 
                                    if($operational_setting[10]->$user_type_name  == 1 && $get_user_info['approved_as_basic']['0'] != 'no') { ?>
                                    <div class="text-box-round">
                                        <h5>MY TRAVEL AGENDA</h5>
                                        <?php $travels_ads=$wpdb->get_results("SELECT * FROM wp_travel_dating WHERE tarvel_logged_user=".$current_user->ID." ORDER BY ID DESC"); ?>
                                        <?php 
                                        foreach ($travels_ads as $value) 
                                        { 
                                            $from=strtotime($value->travel_from);
                                            $to=strtotime($value->travel_to); 
                                            $from_m=date('m', $from);
                                            $monthName_from = date('F', mktime(0, 0, 0, $from_m, 10));
                                            $to_m=date('m', $to);
                                            $monthName_to = date('F', mktime(0, 0, 0, $to_m, 10));
                                            $from_y=date('Y', $from);
                                            $to_y=date('Y', $to);
                                            $from_d=date('d', $from);
                                            $to_d=date('d',$to);
                                            $country=$value->travel_country;
                                            if($value->travel_regions != "")
                                            {
                                                $city= ' - ' . $value->travel_region;
                                            }else
                                            {
                                               $city= ""; 
                                            }
                                        ?>
                                           <div class="form-group"> 
                                                <div class="adds_content no-padding-left col-sm-12">
                                                   <p>
                                                   <?php echo $country; ?>
                                                   <span class="city_white"><?php echo $city; ?></span><br/>
                                                   <?php echo $from_d."-".$to_d; ?>
                                                   <?php if($from_m==$to_m)
                                                   {  echo $monthName_from; }
                                                   else { echo $monthName_from."-".$monthName_to; }
                                                   echo $to_y; ?>
                                                   </p>
                                                </div>
                                           </div>
                                      <?php } ?>                                            
                                    </div>
                                    <?php } else { ?>
                                        <div class="text-box-round invalid">
                                            <span class="grayTrans"></span>
                                            <?php $trave_agenda = get_post(364); ?>
                                            <h5>MY TRAVEL AGENDA</h5>
                                            <p><?php // echo $get_user_info['travel_agent']['0']; ?></p>
                                        </div>
                                    <?php } ?>
                </div> <!--col-sm-3 -->

                <div class="col-sm-6 no-padding">
                    <?php if ($get_user_info['user_type']['0'] == 'premium') { ?>
                        <h5 class="red profileType">
                           <?php if($profile_name){echo $profile_name;}else{echo "Premium Member";}?>
                       </h5>
                    <?php } elseif ($get_user_info['user_type']['0'] == 'validated') { ?>   
                        <h5 class="greenText profileType">
                            <?php if($profile_name){echo $profile_name;}else{echo "Validated Member";}?>
                            <img width="12" src="<?php echo bloginfo('template_url'); ?>/images/tick-green.png" alt="">
                        </h5>

                    <?php } elseif ($get_user_info['user_type']['0'] == 'basic' && $get_user_info['approved_as_basic']['0'] == 'yes' ) { ?>
                        <h5 class="greenText profileType" style="color:#989898;">
                           <?php if($profile_name){echo $profile_name;}else{echo "Basic Member";}?>
                        </h5>
                    <?php } elseif ($get_user_info['approved_as_basic']['0'] == 'no'){ ?>
                        <h5 class="greenText profileType" style="color:#989898;"><?php echo "Pre-membership stage"; ?></h5>
                    <?php } ?>

                    <div class="row">
                        <div class="col-sm-6 no-padding-right">
                            <div class="Profile-Part-BI cus_scroll">                                    
                                <h3 class="Proh-h3"><span>BASIC INFO</span></h3>
                                <div class="padding-basic-info mt30">
                                    <div class="form-group border-bottom">
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Age</label>
                                        </label>
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-center bold"><?php echo $male_age; ?></label>
                                    </div>
                                    <div class="form-group border-bottom">
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Height (cm)</label>
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-center bold"><?php echo $his_hight; ?></label>
                                    </div>
                                    <div class="form-group border-bottom">
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Figure</label>
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-center">
                                            <span class="normalText"><?php echo $explodeHisFigure[0]; ?></span>
                                            <span class="normalText"><?php echo ($explodeHisFigure[1] != '') ? str_replace(')', '', $explodeHisFigure[1]) :''; ?></span>
                                        </label>
                                    </div>
                                    <div class="form-group border-bottom">
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Relationship</label>
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-center"><?php echo $get_user_info['relation_ship']['0']; ?></label>
                                    </div>
                                    <div class="form-group border-bottom">
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Smoking</label>
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-center"><?php echo $his_smoke; ?></label>
                                    </div>

                                    <div class="form-group border-bottom">
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Hair color</label>
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-center"><?php echo $get_user_info['his_haircolor']['0']; ?></label>

                                    </div>                                        
                                    <div class="form-group border-bottom">
                                            <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Languages</label>
                                            <label for="inputEmail3" class="col-sm-6 BasicInfo-label"><?php echo str_replace(',', ', ', rtrim($selectedLanguages,",")); ?></label>
                                        </div>
                                        
                                    <div class="form-group border-bottom">
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Education</label>
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-center"><?php echo $get_user_info['his_graduation']['0'];  ?></label>

                                    </div>                                        
                                    <div class="form-group border-bottom">
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Hobbies Interests</label>
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label"><?php echo $his_hobby; ?></label>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Additional info</label>                                        
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label"><?php echo $get_user_info['about_his']['0']; ?></label>
                                    </div>
                                </div>
                            </div> <!-- profile part -->
                                
                                  <?php if($appproved=='no' ) {  ?>     
                            <div class="Album-Part">
                                <span class="grayTrans"></span>
                                <div class=""> 
                                    <h3 class="Pro-h3"><span>ALBUMS</span></h3>
                                </div>
                                <div class="WidthFull">
                                    <?php
                                        global $current_user;
                                        get_currentuserinfo();
                                        $current_user->user_type;
                                        $results = $wpdb->get_results("SELECT * FROM user_create_album WHERE user_id = '$current_user->ID'");
                                        $upload_dir = wp_upload_dir();
                                        $imgPath = $upload_dir['baseurl'] . '/users_album/album_' . $current_user->ID . '/';
                                        $i = 1;
                                        foreach ($results as $value) {
                                            $select_img = $wpdb->get_results("SELECT * FROM user_album_images WHERE album_id = '$value->id' AND status != 2 order by img_order ASC limit 1");
                                            if (count($select_img) == 0) {
                                                ?>
                                                <figure class="album-img-div text-center">
                                                    <img src="<?php bloginfo('template_url'); ?>/images/alb_2.jpg" alt="">
                                                    <figcaption>2</figcaption>
                                                </figure>
                                                <?php
                                            } else {
                                                foreach ($select_img as $select) {
                                                    $number = '"' . $value->id . '"';
                                                    ?>                                                            
                                                    <figure class="album-img-div text-center"><a href="<?php echo get_permalink(424); ?>&imgid=<?php echo base64_encode($number); ?>" name='imgid' target='_blank' value="<?php echo $value->id; ?>" ><img src="<?php echo $imgPath . $select->img_url; ?>"></a><figcaption><?php echo $i++; ?></figcaption></figure>                                      
                                                    <?php
                                                }
                                            }
                                        }
                                        if (count($results) != 0) {
                                            $sum = 1;
                                            for ($i = 0; $i < (5 - count($results)); $i++) {
                                                ?>
                                                <figure class="album-img-div text-center">
                                                    <img src="<?php bloginfo('template_url'); ?>/images/alb_2.jpg" alt="">
                                                    <figcaption><?php echo count($results) + $sum; ?></figcaption>
                                                </figure>
                                                <?php
                                                $sum++;
                                            }
                                        } else {
                                            echo "No Album";
                                        }
                                    ?>                                                 
                                </div> 
                                <div class="clear"></div>        
                            </div> <!--album part -->

                            <?php } else { ?>

                            <div class="Album-Part">
                                <div class=""> 
                                    <h3 class="Pro-h3"><span>ALBUMS</span></h3>
                                </div>
                                <div class="WidthFull">
                                    <?php
                                        global $current_user;
                                        get_currentuserinfo();
                                        $current_user->user_type;
                                        $results = $wpdb->get_results("SELECT * FROM user_create_album WHERE user_id = '$current_user->ID'");
                                        $upload_dir = wp_upload_dir();
                                        $imgPath = $upload_dir['baseurl'] . '/users_album/album_' . $current_user->ID . '/';
                                        $i = 1;
                                        foreach ($results as $value) {
                                            $select_img = $wpdb->get_results("SELECT * FROM user_album_images WHERE album_id = '$value->id' AND status != 2 order by img_order ASC limit 1");
                                            if (count($select_img) == 0) {
                                                ?>
                                                <figure class="album-img-div text-center">
                                                    <img src="<?php bloginfo('template_url'); ?>/images/no_image_avl.jpeg" alt="">
                                                    <figcaption><?php echo $value->album_name; ?></figcaption>
                                                </figure>
                                                <?php
                                            } else {
                                                foreach ($select_img as $select) {
                                                    $number = '"' . $value->id . '"';
                                                    ?>                                                            
                                                    <figure class="album-img-div text-center"><a href="<?php echo home_url(); ?>/?page_id=761&album=<?php echo $value->id; ?>&user_id=<?php echo $value->user_id; ?>" ><img src="<?php echo $imgPath . $select->img_url; ?>"></a><figcaption><?php echo $value->album_name; ?></figcaption></figure>                                      
                                                    <?php
                                                }
                                            }
                                        }
                                        if (count($results) != 0) {
                                            $sum = 1;
                                            for ($i = 0; $i < (5 - count($results)); $i++) {
                                                if((count($results)+$i)<$user_alb_count){?>
                                                <figure class="album-img-div text-center">
                                                    <img src="<?php bloginfo('template_url'); ?>/images/album_dummy.gif" alt="">
                                                    <figcaption>no album</figcaption>
                                                </figure>
                                                <?php } else { ?>
                                                    
                                                    <figure class="album-img-div text-center">
                                                        <span class="grayTrans"></span>
                                                    <img src="<?php bloginfo('template_url'); ?>/images/album_dummy.gif" alt="">
                                                    <figcaption></figcaption>
                                                </figure>
                                                <?php }
                                                $sum++;
                                            }
                                        } else {
                                            for ($i = 0; $i < (5 - count($results)); $i++) {
                                                if($i<$user_alb_count){?>
                                                <figure class="album-img-div text-center">
                                                    <img src="<?php bloginfo('template_url'); ?>/images/album_dummy.gif" alt="">
                                                    <figcaption>no album</figcaption>
                                                </figure>
                                                <?php } else { ?>
                                                    <figure class="album-img-div text-center">
                                                     <span class="grayTrans"></span>
                                                    <img src="<?php bloginfo('template_url'); ?>/images/album_dummy.gif" alt="">
                                                    <figcaption></figcaption>
                                                </figure>
                                                <?php }
                $sum++;
            }
                                        }
                                        ?>                                                 
                                    </div> 
                                    <div class="clear"></div>
                                </div>
                            <?php } ?>
                        </div>

                        <div class="col-sm-6">
                            <div class="Favourite-Part2">                                    
                                    <h3 class="Pro-h3 col-sm-12"><span>MAIN RESIDENCE</span></h3>                                   
                                <div class="padding-top-bottom">
                                    <div class="form-group"> 
                                        <label for="inputEmail3" class="col-sm-5 BasicInfo-label">COUNTRY :</label>
                                        <label for="inputEmail3" class="col-sm-7 BasicInfo-label para-left"><?php echo $country; ?></label>
                                    </div>
                                    <div class="form-group"> 
                                        <label for="inputEmail3" class="col-sm-5 BasicInfo-label">REGIO :</label>
                                        <label for="inputEmail3" class="col-sm-7 BasicInfo-label para-left"><?php echo $district1; ?></label>
                                    </div>
                                    <div class="form-group"> 
                                        <label for="inputEmail3" class="col-sm-5 BasicInfo-label">NEXT CITY :</label>
                                        <label for="inputEmail3" class="col-sm-7 BasicInfo-label para-left"><?php echo $city1; ?></label>
                                    </div>
                                    <div class="form-group"> 
                                        <label for="inputEmail3" class="col-sm-5 BasicInfo-label">Frequently in :</label>
                                        <label for="inputEmail3" class="col-sm-7 BasicInfo-label para-left"><?php echo $get_user_info['frequently_in']['0']; ?></label>
                                    </div>
                                     
                                </div>
                            </div>

                            <div class="Favourite-Part3 <?php if($portalsetting[2]->$user_type_name == '0'){echo 'invalid' ;}?>">
                                <?php if($portalsetting[2]->$user_type_name == '0'){echo '<span class="grayTrans-more"></span>';}?>
                                <div class=""> 
                                    <h3 class="Pro-h3 col-sm-12 col-md-12"><span>MATCHES</span></h3>
                                </div>
                                <div class="padding-top-bottom">
                                    <div class="row"> 
                                    <?php
                                            $get_sender_data = $wpdb->get_results("SELECT reciver_id FROM wp_user_profile WHERE sender_id ='".$current_user->ID."' AND add_fav='1' ORDER BY id DESC");
                                            $get_reciver_data = $wpdb->get_results("SELECT sender_id FROM  wp_user_profile WHERE reciver_id ='".$current_user->ID."' AND add_fav='1' ");
                                            $senderdataid = array();
                                            $count =1;                                
                                            foreach($get_sender_data as $value)
                                            {
                                                $senderdataid[] =  $value->reciver_id;  
                                            }
                                            $alldata = array();
                                            foreach($get_reciver_data as $data){
                                                if(in_array($data->sender_id, $senderdataid)){
                                                    $alldata[] = $data->sender_id ;                                                    
                                                }                                                
                                            }
                                        if (!empty($alldata)) 
                                        {
                                            foreach ($alldata as $mat_value) 
                                            {
                                                if($count == 4){
                                                    break;
                                                }
                                                $senderID       = $mat_value;
                                                $user_name      = get_user_meta($senderID,'nickname',true);
                                                
                                                $userUrl = get_permalink(305) . '&user_id=' . $senderID;
                                                #.. sender user pic data
                                                $his_pick          = get_user_meta($senderID,'his_pick', true);
                                                $her_pick          = get_user_meta($senderID,'her_pick' , true);
                                                if(!empty($her_pick))
                                                {
                                                    $pick_name = $her_pick;
                                                }else{
                                                    $pick_name = $his_pick;
                                                }
                                                $pick_source        = $upload_dir['baseurl'] . '/profilepick_' .$senderID.'/'.$pick_name;
                                    ?>                                    
                                        <div class="col-xs-4">
                                            <img src="<?php echo $pick_source; ?>" height="45" width="52">
                                            <label class="matchName"><?php echo $user_name; ?></label>
                                        </div>                                    
                                    <?php $count++;  }
                                        }else
                                        {
                                            echo '<div class="col-xs-8">No Matches</div>';
                                        }
                                     ?>  
                                    </div> 
                                </div>
                            </div>
                            <!-- end the residence -->
                            <div class="Favourite-Part">
                                <?php if($appproved=='no' ) { echo '<span class="grayTrans"></span>';} ?> 
                                <div class=""> 
                                    <h3 class="Pro-h4"><span>FAVOURITE QUOTE</span></h3>
                                </div>
                                <div class="col-sm-12 col-md-12 no-padding"> 
                                    <?php echo $fav_quote_content; ?>
                                </div>
                            </div>
                        </div>

                        </div>
                        <?php
                            $results = $wpdb->get_var("SELECT img_url FROM bg_images");
                            //$upload_dir = wp_upload_dir();                            
                            $imgPath = $results;
                            ?>
                        <div class="row">
                            <div class="col-sm-12 prelative">
                                <h3 class="Pro-h3 newIntro"><span>INTRODUCTION</span></h3>   
                                <div  id="content-1"> 
                                    <div class="Introduction-Part" style="background-size: 100% 100%; background-image: url('<?php echo $imgPath; ?>');" id ="introduction_part">
                                        <div class="padding-para-div" id="intro_data"> 
                                        <?php 
                                                if($des_status == '' && $describe != '')
                                                {
                                                    $message = "Your introduction not approved by admin yet!";
                                                }else if($des_status == 'refused'){
                                                    $message = 'Your introduction has refused by admin!';
                                                }else
                                                {
                                                   $message = $describe;
                                                }                                        
                                                echo $message; 
                                            ?>
                                        </div>                                        
                                    </div>
                                </div>
                            </div>
                        </div><!-- row -->
                    </div> <!--row -->

                    <div class="col-sm-3">                                
                    <?php
                        $userid = get_current_user_id();
                        $arr = array();
                        $subId = array();
                        $status = array();
                        $myrows = $wpdb->get_results("SELECT *  FROM `wp_erotics_preference` where `userid` = $userid AND `type` = 1 order by like_dislike_attr_id");
                        foreach ($myrows as $result) {
                            $arr[] = $result->like_dislike_attr_id;
                            $subId[] = $result->subject_id;
                            $status[] = unserialize($result->status);
                        }

                        $myrows1 = $wpdb->get_results("SELECT *  FROM `wp_erotics_preference` where `userid` = $userid AND `type` = 2 order by like_dislike_attr_id");
                        $arr1 = array();
                        $subId1 = array();
                        $status1 = array();
                        foreach ($myrows1 as $result) {
                            $arr1[] = $result->like_dislike_attr_id;
                            $subId1[] = $result->subject_id;
                            $status1[] = unserialize($result->status);
                        }

                        $arg = array('posts_per_page' => -1, 'offset' => 1, 'category' => 14, 'post_type' => 'eroticoption', 'order' => 'asc');
                        $myposts = get_posts($arg);
                        $myAllposts = array();
                        $ctagoryid = array("3", "4", "5");

                        foreach ($ctagoryid as $values) {
                            $args = array('posts_per_page' => -1, 'offset' => 1, 'category' => $values->ID, 'post_type' => 'erotics', 'order' => 'asc', 'show_count' => 0);
                            $myAllposts = get_posts($args);
                        }
                        $content = array();

                        foreach ($myAllposts as $value) {
                            if (in_array($value->ID, $subId)) {
                                $content['subject'][] = $value->post_title;
                                $content1['subjectID'][] = $value;
                            }
                        }

                        foreach ($arr as $key => $value1) {
                            foreach ($myposts as $value) {
                                if ($value1 == $value->ID) {
                                    $content['desc'][] = $value->post_title;
                                    if(!(in_array($value->ID, $content['id'])))
                                    {
                                        $content['id'][] = $value->ID;
                                    }
                                }
                            }
                        }

                        $content1 = array();
                        foreach ($subId1 as $key => $value) 
                        {                          
                         $content1['subject'][] = get_the_title($value); 
                         $content1['subjectID'][] =  $value;                     
                        }                     

                        foreach ($arr1 as $key => $value1) {
                            foreach ($myposts as $value) {
                                if ($value1 == $value->ID) {
                                    $content1['desc'][] = $value->post_title;
                                    if(!(in_array($value->ID, $content1['id'])))
                                    {
                                        $content1['id'][] = $value->ID;
                                    }
                                }
                            }
                        }

                        $unique_arr_desc=array_unique($content1['desc']);
                        foreach ($unique_arr_desc as $value) 
                        {
                           $unique_arr_dist[]=$value;
                        }
                    ?>
                
                   <?php if ($setting[2]->$user_type_name == '1' && $get_user_info['approved_as_basic']['0'] != 'no') { ?>
                           <div class="perfect-match-Part cus_scrolln">
                               <div class=""> 
                                <h3 class="opm-h3 col-sm-12 col-md-12"><span>PERFECT MATCH</span></h3>
                            </div>
                            <div style="<?php echo ($reg3_txtshe == "") ? 'display:none' : 'display:block';  ?>" class="opm-text-box-roundsec">
                                <p><strong>SHE :</strong><?php echo $reg3_txtshe; ?></p>
                            </div>
                            <div style="<?php echo ($reg3_txthe == "") ? 'display:none' : 'display:block';  ?>" class="opm-text-box-roundsec">
                                <p><strong>HE :</strong><?php echo $reg3_txthe; ?></p> 
                            </div>
                            <div style="<?php echo ($reg3_txtboth == "") ? 'display:none' : 'display:block';  ?>" class="opm-text-box-roundsec">
                                <p><strong>BOTH :</strong><?php echo $reg3_txtboth; ?></p> 
                            </div>
                        </div> 
                        <?php }else{?>
                    <div class="perfect-match-Part cus_scrolln invalid">
                     <span class="greenTrans"></span>
                               <div class=""> 
                                <h3 class="opm-h3 col-sm-12 col-md-12"><span>PERFECT MATCH</span></h3>
                            </div>
                            <div style="<?php echo ($reg3_txtshe == "") ? 'display:none' : 'display:block';  ?>" class="opm-text-box-roundsec">
                                <p><strong>SHE :</strong><?php echo $reg3_txtshe; ?></p>
                            </div>
                            <div style="<?php echo ($reg3_txthe == "") ? 'display:none' : 'display:block';  ?>" class="opm-text-box-roundsec">
                                <p><strong>HE :</strong><?php echo $reg3_txthe; ?></p> 
                            </div>
                            <div style="<?php echo ($reg3_txtboth == "") ? 'display:none' : 'display:block';  ?>" class="opm-text-box-roundsec">
                                <p><strong>BOTH :</strong><?php echo $reg3_txtboth; ?></p> 
                            </div>
                        </div>     
                            <?php } ?>                    

                   <?php if ($get_user_info['approved_as_basic']['0'] != 'no' &&  $setting[1]->$user_type_name == '1') { ?>
                        <div class="eropic-profile-Part">
                            <h3 class="eropic-h3 no-margin no-padding col-sm-12 col-md-12">
                                <span class="red">His erotic profile</span>
                                <?php if ($get_user_info['user_type']['0'] == 'validated') { ?> <a class="profile-her-erotic-right-btn invalid" href="#">E </a> <?php } else { ?>
                                    <a class="profile-her-erotic-right-btn" href="#">E </a> 
                                <?php } ?>
                            </h3>
                            <?php
                                $count1 = 0;
                                for ($i = 0; $i < count($unique_arr_dist); $i++) {
                                    $color1 = $content1['id'][$count1];
                                    $textColor[$color1];
                                    $getTileShow = getActiveOrPassviveTitle($wpdb,$userid,$color1, '2');                                    
                                    ?>              
                                    <div class="eropic-box-round" style="background-color:<?php echo ($bgColor1 != "") ? $bgColor1 : $textColor[$color1]; ?>;color:<?php echo ($bgColor1 != "") ? $bgColor1 : $forColor[$color1]; ?>;">
                                        <p><strong class="up-case"><?php echo strtoupper(qtrans_use($q_config['language'],$unique_arr_dist[$i], true)); ?> :</strong></p>
                                        <?php for($j = 0; $j < count($content1['subject']); $j++)
                                        {
                                        if($content1['desc'][$j]==$unique_arr_dist[$i])
                                        { 
                                            $like_dislike_ID = $content1['subjectID'][$j];
                                            $text1 = getActiveOrPassvive($wpdb,$userid,$color1,$like_dislike_ID, '2');
                                        ?>
                                        <p style=""><?php echo qtrans_use($q_config['language'],$content1['subject'][$j], true) .' '. $text1['text'] ; ?></p>
                                         <?php }
                                    } ?>
                                    </div>
                                    <?php
                                    $count1++;
                                }
                            ?>
                        </div>
                    <?php } else { ?>
                        <div class="eropic-profile-Part invalid">
                            <span class="grayTrans-more"></span>
                            <h3 class="eropic-h3 no-margin no-padding col-sm-12 col-md-12">
                                <span class="red">His erotic profile</span>
                                <a class="profile-her-erotic-right-btn" href="#">E </a>
                            </h3>
                            <?php
                                $count1 = 0;
                                for ($i = 0; $i < count($content1['subject']); $i++) {
                                    $color1 = $content1['id'][$count1];
                                    $textColor[$color1];                                    

                                    ?>              
                                    <div class="eropic-box-round" style="background-color:<?php echo ($bgColor1 != "") ? $bgColor1 : $textColor[$color1]; ?>;color:<?php echo ($bgColor1 != "") ? $bgColor1 : $forColor[$color1]; ?>;">
                                        <p><strong class="up-case"><?php echo strtoupper(qtrans_use($q_config['language'],$content1['desc'][$i], true)); ?> :</strong><?php echo qtrans_use($q_config['language'],$content1['subject'][$i], true) .' '.$text1 ; ?></p>
                                    </div>
                                    <?php
                                    $count1++;
                                }
                            ?>
                        </div>
                    <?php } ?> 
                </div>                 
            </div>
        </form>
    </div>
    <div class="active_passive" style="float:right;margin-right:19%">(a) active (p) passive </div>
</div>

   
<script>
(function ($) {
    $(window).load(function () {
        $(".cus_scroll").mCustomScrollbar({
            theme: "light-2"
        });
        $(".cus_scrolln").mCustomScrollbar({
            theme: "light-2"
        });

        $("#content-1").mCustomScrollbar({
            theme: "light-2"
        });
    });
});
</script>

<?php
}
else {
    ?>
    <div class="text-center succesPoint">
        <h4>You must be loggedin to access this page</h4>
    </div>
    <?php
}
?>

<?php get_footer(); ?>
