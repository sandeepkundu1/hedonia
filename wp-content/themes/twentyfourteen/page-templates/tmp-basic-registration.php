<?php
/*
 * Template Name: Basic Registration types
 * Description: A Page Template with diffrent Basic Registrations types.
 * Author Name : Sanjay Shinghania
 */
get_header();
$type = $_GET['type'];
?>
    
<script src="<?php echo get_template_directory_uri(); ?>/js/modernizr.custom.16089.js"></script>
<script>
jQuery(document).ready(function(){
    jQuery('.married').change(function(){
    if( jQuery(this).prop('checked') ){
        jQuery(this).parent().parent().find('input').prop("checked", false);
       jQuery(this).prop("checked", true);
    }
    else
       {
           jQuery(this).prop("checked", false);
       }
    }); 
});
</script>
<div class="signUpWarp">
    <div class="container">             
        <div class="parent" style="display:none;"><span class="loader"></span></div>
            <div class="row">
                <?php $image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'single-post-thumbnail' ); ?>
                <div class="col-sm-4 col-sm-offset-1">
                    <a href="#" class="imgwrap"><img class="img-responsive" src="<?php echo $image[0]; ?>" alt="" /></a>
                </div>
                <div class="col-sm-5">
                    <div class="registrationPart">
                        <form class="form-horizontal" role="form" name="form" id="register-form" enctype="multipart/form-data">
                            <div class="firstInfo blue_gred">
                                <h2>Registration</h2>
                                    <div class="persionalInfo">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-6 control-label">Profile name</label>
                                            <div class="col-sm-6">
                                                <input type ="text" for="validation" name="profile_name" id="profile_name" class="form-control" required="" >
                                                <span style="display:none;" class="error-msg">Please fill required field</span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-6 control-label">email</label>
                                            <div class="col-sm-6">
                                                <input type="email" for="validation" name="email" id="email" class="form-control" required="" >
                                                <span style="display:none;" class="error-msg">Please fill required field</span>
                                                <span style="display:none;" class="error-email">Please fill a valid email!</span> 
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-6 control-label">password <em><br/>(min. 8 characters)</em></label>
                                            <div class="col-sm-6">
                                                <input type="password" for="validation"  name="password" id="password" class="form-control" required="" >
                                                <span style="display:none;" class="error-msg">Please fill required field</span>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!--blue greed -->

                                <div class="ageInfo red_gred">                                
                                    <h5>Entries in this section can be changed later only 1x !</h5>   
                                    <!-- Date of birth -->
                                    <?php if($type=='couples')  { ?>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-4 control-label">Date of birth (she)</label>
                                            <div class="col-sm-1 paddingsm">
                                                <input type="text" name="shedd" for="validation1" id="shedd" enterText="numeric" class="form-control" required="" maxlength="2">
                                            </div>

                                            <div class="col-sm-1 paddingsm">
                                                <input type="text" name="shemm" for="validation1" id="shemm" enterText="numeric" class="form-control" required="" maxlength="2">
                                            </div>

                                            <div class="col-sm-2 paddingsm">
                                                <input type="text" name="sheyy" for="validation1" id="sheyy" enterText="numeric" class="form-control" maxlength="4" >
                                            </div>

                                            <label for="inputEmail3" class="col-sm-4 control-label">day / month / year</label>
                                            <span id="error" style="color: yellow; display: none; font-size:10px;"></span>
                                            <span id="checkmsg" class="error-msg" style="color: yellow; display: none;">Please fill required field</span>
                                        </div>
                            
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-4 control-label">Date of birth (he)</label>
                                            <div class="col-sm-1 paddingsm">
                                                <input type="text" name="hedd" for="validation2" id="hedd" enterText="numeric1" class="form-control" required="" maxlength="2">
                                            </div>

                                            <div class="col-sm-1 paddingsm">
                                                <input type="text" name="hemm" for="validation2" id="hemm" enterText="numeric1" class="form-control" required="" maxlength="2">
                                            </div>

                                            <div class="col-sm-2 paddingsm">
                                                <input type="sheyy" name="heyy" for="validation2" id="heyy" enterText="numeric1" class="form-control" required="" maxlength="4">
                                            </div>
                                            
                                            <span id="error1" style="color: yellow; display: none; font-size:10px;"></span>
                                            <span id="checkmsg1" style="color: yellow; display: none; ">Please fill required field</span>
                                        </div>
                                    <?php }elseif ($type=='ladies') { ?>

                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-4 control-label">Date of birth (she)</label>
                                            <div class="col-sm-1 paddingsm">
                                                <input type="text" name="shedd" for="validation1" id="shedd" enterText="numeric" class="form-control" required="" maxlength="2">
                                            </div>

                                            <div class="col-sm-1 paddingsm">
                                                <input type="text" name="shemm" for="validation1" id="shemm" enterText="numeric" class="form-control" required="" maxlength="2">
                                            </div>

                                            <div class="col-sm-2 paddingsm">
                                                <input type="text" name="sheyy" for="validation1" id="sheyy" enterText="numeric" class="form-control" maxlength="4" >
                                            </div>

                                            <label for="inputEmail3" class="col-sm-4 control-label">day / month / year</label>
                                             <span id="error" style="color: yellow; display: none; font-size:10px;"></span>
                                             <span id="checkmsg" class="error-msg" style="color: yellow; display: none;">Please fill required field</span>
                                        </div>

                                    <?php }else{ ?>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-4 control-label">Date of birth (he)</label>
                                            <div class="col-sm-1 paddingsm">
                                                <input type="text" name="hedd" for="validation2" id="hedd" enterText="numeric1" class="form-control" required="" maxlength="2">
                                            </div>
                                            <div class="col-sm-1 paddingsm">
                                                <input type="text" name="hemm" for="validation2" id="hemm" enterText="numeric1" class="form-control" required="" maxlength="2">
                                            </div>
                                            <div class="col-sm-2 paddingsm">
                                                <input type="sheyy" name="heyy" for="validation2" id="heyy" enterText="numeric1" class="form-control" required="" maxlength="4">
                                            </div>
                                            <label for="inputEmail3" class="col-sm-4 control-label">day / month / year</label>
                                            <span id="error1" style="color: yellow; display: none; font-size:10px;"></span>
                                            <span id="checkmsg1" style="color: yellow; display: none; ">Please fill required field</span>
                                        </div>
                                    <?php } ?>

                                    <!-- end the date of birth -->

                                    <div class="form-group margin-top-chk">
                                        <label for="inputEmail3" class="col-sm-3 control-label">married</label>
                                        <div class="col-sm-1 no-padding">
                                            <input type="checkbox" class="checkbox_cus married" for="isChecked" name="relation_ship" value="married" id="married"  required="">
                                            <label class="checkbox_label" for="married"></label>
                                        </div>
                                        <label for="inputEmail3" class="col-sm-3 control-label">in relation</label>
                                        <div class="col-sm-1 no-padding">
                                            <input type="checkbox" class="checkbox_cus married" for="isChecked" name="relation_ship" value="in relation" id="not_married"  required="">
                                            <label class="checkbox_label" for="not_married"></label>
                                        </div>
                                        <label for="inputEmail3" class="col-sm-3 control-label">single</label>
                                        <div class="col-sm-1 no-padding">
                                            <input type="checkbox" class="checkbox_cus married" for="isChecked" name="relation_ship" value="single" id="Separeted"  required="">
                                            <label class="checkbox_label" for="Separeted"></label>
                                        </div>
                                        <span id="checkedBox" class="error-msg" style="padding-left: 15px; display:none;">Please select a status</span>
                                    </div><!--relation ship -->
                                </div>

                                <div class="mainResid blue_gred">                                
                                    <h3 class="reg-main-residence">Main residence:</h3>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-6 control-label">Country</label>
                                        <div class="col-sm-6">
                                           <select id="country" name ="country" class="required form-control"></select>
                                            <span id="country-msg" class="error-msg" style="display:none;color:red;">Please select a country</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-6 control-label"> Administrative district <p class="ParaS">(country, Bundesland, department, regioni, comunidades, canton.....)</p></label>
                                        <div class="col-sm-6">
                                            <div class="margin-admin">
                                                <select name ="state" id ="state" class="required form-control"></select>
                                                <span id="selectstate" style="display:none;font-size:10px;color:red;">Plese select state</span>
                                                <span id="selectstate" style="display:none;font-size:10px;color:red;">Plese select state</span>
                                                 <script language="javascript">
                                                        populateCountries("country", "state", "", "");
                                                 </script>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-6 control-label"><i>Next city</i> <i class="ParaS">(not mandatory)</i></label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" value="" name="city"/>                                         
                                        </div>
                                    </div>                                
                                </div>  
                                <!--
                                <div class="yourLook">
                                    <h4>You are looking for:</h4>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <span class="label">Couples</span>
                                            <input type="checkbox" class="checkbox_cus checkOne" name="looking_for" value="Couples" id="CouplesCheck"  required="">
                                            <label class="checkbox_label" for="CouplesCheck"></label>
                                        </div>
                                        <div class="col-sm-4">
                                            <span class="label">Ladies</span>
                                            <input type="checkbox" class="checkbox_cus checkOne" name="looking_for1" value="Ladies" id="LadiesCheck"  required="">
                                            <label class="checkbox_label" for="LadiesCheck"></label>
                                        </div>
                                        <div class="col-sm-4">
                                            <span class="label">Men</span>
                                            <input type="checkbox" class="checkbox_cus checkOne" name="looking_for2" value="Men" id="MenCheck"  required="">
                                            <label class="checkbox_label" for="MenCheck"></label>
                                        </div>
                                        <span class="error-msg" id="checkedBox1" style="padding-left: 15px; display:none">Please select atleast one status</span>
                                    </div>
                                </div> -->
                                <div class="termsCond">
                                    <p><a href="#" >We do not accept membership applications without profile photo<?php echo ($_GET['type'] == 'couples') ? "s" :""; ?></a> Please download here your profile picture <?php echo ($_GET['type'] == 'couples') ? "(s)" :""; ?></p>
                                    <small>You can change your photo <?php echo ($_GET['type'] == 'couples') ? "(s)" :""; ?> later anytime in your "edit my profile" section</small> 
                                    <!-- profile image upload -->

                                    <?php if($type=='couples')  { ?>
                                    <div class="form-group">
                                        <div class="col-sm-5">
                                            <input type="file" id="addPick1" style="visibility: hidden; position: absolute;"/>
                                            <label for="addPick1" class="btn">+ add picture 1</label>
                                        </div>
                                        <div class="col-sm-7 info">
                                            Her profile picture *)
                                            <a id="shw-img" class="pull-right" style="display:none;"> 
                                                <img  id="showimage" style="display:none;" src="">show image 
                                            </a>                                            
                                        </div>                                
                                        <span id="pic1" class="error-msg" style="display:none">Please upload picture</span>
                                    </div>                            
                                    <img id="imgprvw" height="150" width="90" style="display:none">
                                    <img id="imgprvw1" height="150" width="90" style="display:none">
                                    <div class="form-group">
                                        <div class="col-sm-5">
                                            <input type="file" id="addPick2" style="visibility: hidden; position: absolute;" />
                                            <label for="addPick2" class="btn">+ add picture 2</label>
                                        </div>
                                        <div class="col-sm-7 info">
                                            His profile picture *)
                                            <a id="shw-img1" style="display:none;" class="pull-right"> 
                                                <img  id="showimage1" style="display:none;" src="">show image 
                                            </a>
                                        </div>
                                        <div class="col-sm-7 info " ></div>
                                    </div>

                                    <?php }elseif ($type=='ladies') { ?>
                                    <div class="form-group">
                                        <div class="col-sm-5">
                                            <input type="file" id="addPick1" style="visibility: hidden; position: absolute;"/>
                                            <label for="addPick1" class="btn">+ add picture</label>
                                        </div>
                                        <div class="col-sm-7 info">
                                            your profile picture *)
                                            <a id="shw-img" class="pull-right" style="display:none;"> 
                                                <img  id="showimage" style="display:none;" src="">show image 
                                            </a>
                                        </div>
                                        <span id="pic1" class="error-msg" style="padding-left:35px; display:none" >Please upload picture</span>
                                    </div>
                                    <img id="imgprvw" height="150" width="90" style="display:none">                            
                                    <?php } else{ ?>

                                    <div class="form-group">
                                        <div class="col-sm-5">
                                            <input type="file" id="addPick2" style="visibility: hidden; position: absolute;"/>
                                            <label for="addPick2" class="btn">+ add picture</label>
                                        </div>
                                        <div class="col-sm-7 info">
                                            your profile picture *)
                                            <a id="shw-img1" class="pull-right" style="display:none;"> 
                                                <img style="display:none;" id="showimage1" src="">show image 
                                            </a>
                                        </div>
                                        
                                       <span id="pic1" class="error-msg" style="padding-left:35px; display:none">Please upload picture</span>
                                    </div>
                                    <img id="imgprvw1" height="150" width="90" style="display:none">
                                    <?php } ?>
                                    <!-- end the profile image upload -->

                                    <a href="#" class="text-right gotoTerm" >copyright conditions</a>
                                    <p><small>*) non - pornographic picture<?php echo ($_GET['type'] == 'couples') ? "s" :""; ?> in jpg </small> </p>
                                    <div class="text-center">
                                        <button type="button" class="btn_look btn"  id ="regnext1">Send </button>
                                    </div>
                                    <div id="dob_result" class=""></div>
                                </div>
                                <input type="hidden" name="user_type" value="basic">
                                <input type="hidden" name="member_type" value="<?php echo $type; ?>" id="usertype">
                            </form>
                        </div>
                    </div>
            </div>
        </div>
</div>
<script type="text/javascript">
// show image preview female
jQuery(function () {
    jQuery("#addPick1").change(function () {
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg)$/;
        // check the image size
        var iSize = ($("#addPick1")[0].files[0].size / 1024); 
        if (iSize / 1024 > 1) 
        {  
            iSize = (Math.round((iSize / 1024) * 100) / 100)
            jQuery("#dob_result").addClass('reg_msg alert alert-danger').html("Image size is greater than 1 Mb");   
            jQuery('#imgprvw').removeAttr('src');
            setTimeout(function(){jQuery('#dob_result').fadeToggle(10000);}, 10000);
        } 
        else 
        {
            // check the image format
            if (regex.test($(this).val().toLowerCase())) 
            {
                if (jQuery.browser.msie && parseFloat(jQuery.browser.version) <= 9.0) {
                   jQuery("#dob_result")[0].filters.item("DXImageTransform.Microsoft.AlphaImageLoader").src = $(this).val();
                   setTimeout(function(){jQuery('#dob_result').fadeToggle(10000);}, 10000);
                }
                else {
                    if (typeof (FileReader) != "undefined") {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            jQuery("#femalepick").remove();  
                            jQuery('#imgprvw').attr('src', e.target.result);
                            jQuery('#showimage').attr('src', e.target.result);
                            jQuery('#shw-img').show();
                        }
                        reader.readAsDataURL(jQuery(this)[0].files[0]);
                    } else {
                        jQuery("#dob_result").addClass('reg_msg alert alert-danger').html("This browser does not support FileReader.");  
                        jQuery('#imgprvw').removeAttr('src');
                        setTimeout(function(){jQuery('#dob_result').fadeToggle(10000);}, 10000);
                    }
                }
            } else {
                jQuery("#dob_result").addClass('reg_msg alert alert-danger').html("Please upload a valid image file."); 
                jQuery('#imgprvw').removeAttr('src'); 
                setTimeout(function(){jQuery('#dob_result').fadeToggle(10000);}, 10000);
            }
        } 
    });
});
// show image preview male
jQuery(function () {
    jQuery("#addPick2").change(function () {
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg)$/;
        // check the image size
        var iSize = ($("#addPick2")[0].files[0].size / 1024); 
        if (iSize / 1024 > 1) 
        {  
            iSize = (Math.round((iSize / 1024) * 100) / 100)
            jQuery("#dob_result").addClass('reg_msg alert alert-danger').html("Image size is greater than 1 Mb");   
            jQuery('#imgprvw1').removeAttr('src');
            setTimeout(function(){jQuery('#dob_result').fadeToggle(10000);}, 10000);
        } 
        else 
        {
            // check the image format
            if (regex.test($(this).val().toLowerCase())) 
            {
                if (jQuery.browser.msie && parseFloat(jQuery.browser.version) <= 9.0) {
                   jQuery("#dob_result")[0].filters.item("DXImageTransform.Microsoft.AlphaImageLoader").src = $(this).val();
                   setTimeout(function(){jQuery('#dob_result').fadeToggle(10000);}, 10000);
                }
                else {
                    if (typeof (FileReader) != "undefined") {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            jQuery("#malepick").remove();  
                            jQuery('#imgprvw1').attr('src', e.target.result);
                            jQuery('#showimage1').attr('src', e.target.result);
                            jQuery('#shw-img1').show();
                        }
                        reader.readAsDataURL(jQuery(this)[0].files[0]);
                    } else {
                        jQuery("#dob_result").addClass('reg_msg alert alert-danger').html("This browser does not support FileReader.");  
                        jQuery('#imgprvw1').removeAttr('src');
                        setTimeout(function(){jQuery('#dob_result').fadeToggle(10000);}, 10000);
                    }
                }
            } else {
                jQuery("#dob_result").addClass('reg_msg alert alert-danger').html("Please upload a valid image file.");  
                jQuery('#imgprvw1').removeAttr('src');
                setTimeout(function(){jQuery('#dob_result').fadeToggle(10000);}, 10000);
            }
        } 
    });
});

// mail error
$('input,select').focus(function(){
    $(this).next('span').hide();
    $('.error-email').hide();
});
// date checkbox
$('input[id="shedd"],input[id="shemm"],input[id="sheyy"]').focus(function(){
    $('#checkmsg').hide();
});
// date checkbox
$('input[id="hedd"],input[id="hemm"],input[id="heyy"]').focus(function(){
    $('#checkmsg1').hide();
});
// password length
$('#password').focusout(function(){
    if($(this).val().length > 0 && $(this).val().length < 8){
    $(this).after('<span class="error-msg">Please fill atleast 8 characters</span>');      
      hint=1;
    }
});
// year length checkbox
$('#sheyy').focusout(function(){
    if($(this).val().length < 4 && $(this).val().length != "" ){            
    $('#error').show();
    $('#error').text('value should be not less than four');
      hint=1;
    }
});
// year length checkbox   
$('#heyy').focusout(function(){
    if($(this).val().length < 4 && $(this).val().length != "" ){            
    $('#error1').show();
    $('#error1').text('value should not be less than four !');
      hint=1;
    }
});

//=============================#### display error======================
var specialKeys = new Array();
specialKeys.push(8); //Backspace

jQuery(function(){       
    jQuery('input[enterText="numeric"]').bind("keypress", function (e) {
        var keyCode = e.which ? e.which : e.keyCode 
        var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
        if (e.keyCode === 9) { 
            return true;
        }
        jQuery("#error").css("display", ret ? "none" : "inline");            
        jQuery("#error").html('Please fill only Numeric value');  
        return ret;
    });

    jQuery(function () {
        jQuery('input[enterText="numeric1"]').bind("keypress", function (e) {
            var keyCode = e.which ? e.which : e.keyCode 
            var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
            if (e.keyCode === 9) { 
                return true;
            }
            jQuery("#error1").css("display", ret ? "none" : "inline");            
            jQuery("#error1").html('Please fill only Numeric value');  
            return ret;
        });
    });
});
</script>

<script type="text/javascript">
$('#shedd').focusout(function(){ 
    if($(this).val() != ""){
        if($(this).val() >31 || $(this).val() < 1){
            $("#error").attr("style","display : block; font-size:10px;color:yellow");
            $("#error").html('Please enter a valid Number');
            $(this).val('');
        }
    }else{
        $(".error").attr("style","display : none; font-size:10px;color:yellow");
    }
});
//          
$('#hedd').focusout(function(){ 
    if($(this).val() != ""){
        if($(this).val() >31 || $(this).val() < 1){
            $("#error1").attr("style","display : block; font-size:10px;color:yellow");
            $("#error1").html('Please enter a valid number');
            $(this).val('');
        }
    }else{
        $(".error1").attr("style","display : none; font-size:10px;color:yellow");
    }
});
//
$('#shemm').focusout(function(){
    if($(this).val() != ""){
        if($(this).val() >12 || $(this).val() < 1){
           $("#error").attr("style","display : block; font-size:10px;color:yellow");
            $("#error").html('Please enter a valid number');
            $(this).val('');
        }
    }else{
        $(".error").attr("style","display : none; font-size:10px;color:yellow");
    }
});
//
$('#hemm').focusout(function(){
    if($(this).val() != ""){
        if($(this).val() >12 || $(this).val() < 1){
            $("#error1").attr("style","display : block; font-size:10px;color:yellow");
            $("#error1").html('Please enter a valid number');
            $(this).val('');
        }
    }else{
        $(".error1").attr("style","display : none; font-size:10px;color:yellow");
    }
});
//            
$('.shemm').focusin(function(){
    if($(this).val() >31 || $(this).val() < 1){
       $("#error").attr("style","display : none;font-size:10px;color:yellow ");                 
    }
});
</script>

<script type="text/javascript">
jQuery("#regnext1").click(function () {
    var hint = 0;
    var count = 0;
    var count1 = 0;
    var type = '<?php echo $_GET["type"]; ?>';
    var str = $( "#register-form" ).serialize();
    var her_pick = $("#imgprvw").attr('src');
    var his_pick = $("#imgprvw1").attr('src');
    var profile_name = $("#profile_name").val();

    if(type=='couples')
    {
        if(her_pick ==undefined && his_pick==undefined)
        {
            $('#pic1').show();
            hint =1;
        }
        else
        {
            $('#pic1').hide();
        }
    }
    else if(type=='ladies'){
        if(her_pick ==undefined)
        {
            $('#pic1').show();
            hint =1;
        }
        else
        {
            $('#pic1').hide();
        }
    }else{
        if(his_pick==undefined)
        {
            $('#pic1').show();
            hint =1;
        }
        else
        {
            $('#pic1').hide();
        }
    }  

    if($('#email').val() != ""){
        if(IsEmail($('#email').val()) == false){
            hint = 1;
            $('.error-email').show();
        }          
    }

    $('.married').each(function(){        
        if($(this).is(':checked')){
           count++; 
        }
      
    });

    if(count == 0){
       hint = 1;
       $('#checkedBox').show();
    }else{
        $('#checkedBox').hide();
    }

    if( $('#password').val() != ""){
        if($('#password').val().length < 8){
        hint =  1;            
        }
    }

    $('input[for="validation1"]').each(function(){
       if($(this).val() == ''){
        hint =1;
        if($(this).next('span').length == 0){
            $('#checkmsg').show();
            $('#checkmsg').show();
            window.scrollTo(500, 0);
            return false;
        }
        
       } 
    });

    $('input[for="validation2"]').each(function(){
       if($(this).val() == ''){
        hint =1;
        if($(this).next('span').length == 0){
            $('#checkmsg1').show();
            $('#checkmsg1').show();
            return false;
        }
        
       } 
    });

    $('input[for="validation"]').each(function(){
       if($(this).val() == ''){
            hint = 1;
            $(this).next('span').show();
            //$('.error-msg').show();            
       } 
    });

    $('select option:selected').each(function(){
         if($(this).val() == -1 ){
            $('#country-msg').show();
            hint =1;
         }        
     });             
    // send to data for registration on ajax call
    if(hint == 0)
    {
        formData = 'action=my_action_basic_registration&str=' + str +'&her_pick='+ her_pick +'&his_pick='+his_pick;
        jQuery.ajax({
            type: 'POST',
            data: formData,
            dataType: 'json',
            url: "<?php echo site_url(); ?>/wp-admin/admin-ajax.php",
            beforeSend : function(){
                jQuery('.parent').css("display" ,'block');
            },
            success: function (data) {
                if(data.err =='failed')
                {
                    jQuery("#dob_result").addClass('reg_msg alert alert-danger').html(data.msg);
                    setTimeout(function(){jQuery('#dob_result').fadeToggle(10000);}, 10000);
                }else{
                    window.location = "<?php echo get_permalink(521).'&lang='.$_GET['lang']; ?>";  
                }
            },
            complete: function() {
                jQuery('.parent').css("display" ,'none');
            }
        });
    }
}); 
</script>

<script type="text/javascript">
$('a#shw-img').mouseover(function(e){                        
    $('#showimage').show();
});
$('a#shw-img').mouseout(function(e){
    $('#showimage').hide();
});

$('a#shw-img1').mouseover(function(e){                        
    $('#showimage1').show();

});
$('a#shw-img1').mouseout(function(e){
    $('#showimage1').hide();
});

</script>

<script type="text/javascript">
function IsEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}
</script>

<?php get_footer(); ?>          
