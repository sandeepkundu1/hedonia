<?php
/**
 * Template Name: Couple Edit My Profile
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Twenty Twelve consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * Author Name : Sanjay Shinghania
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

    get_header();

    $textColor = array(
            "182" => "#395723", 
            "183" => "#476D2D", 
            "184" => "#61953D", 
            "185" => "#A9D12A",
            "186" => "#E2F0D9", 
            "187" => "#ECF5E7", 
            "706" => "#F8FBF7", 
            "707" => "#F2F2F2", 
            "708" => "#F2F2F2", 
            "709" => "#F2F2F2", 
            "710" => "#F2F2F2", 
            "711" => "#F2F2F2", 
            "712" => "#F2F2F2", 
            "713" => "#F2F2F2", 
            "714" => "#F2F2F2", 
            "715" => "#FF3F3F",
            "716" => "#C00000"             
        );

    $forColor = array(
            "182" => "#fff", 
            "183" => "#fff", 
            "184" => "#fff", 
            "185" => "#fff",
            "186" => "#000", 
            "187" => "#000", 
            "706" => "#000", 
            "707" => "#000", 
            "708" => "#000", 
            "709" => "#000", 
            "710" => "#000", 
            "711" => "#000", 
            "712" => "#000", 
            "713" => "#000", 
            "714" => "#000", 
            "715" => "#000", 
            "716" => "#000"            
        );

    $get_user_info = get_user_meta($current_user->ID);
    $user_type_name =  get_user_meta($current_user->ID ,'user_type', true );
    $user_alb_count=$album_setting[0]->$user_type_name;
    //echo '<pre>';print_r($get_user_info);

    # ============= Profilepick for couples
    $upload_dir = wp_upload_dir();
    $image_url_female = $upload_dir['baseurl'] . '/profilepick_' . $current_user->ID . '/' . $get_user_info['her_pick']['0'];
    $image_url_male = $upload_dir['baseurl'] . '/profilepick_' . $current_user->ID . '/' . $get_user_info['his_pick']['0'];

    #=============== Age  for couples
    $age1 = $get_user_info['she_dob']['0'];
    $age2 = $get_user_info['he_dob']['0'];
    $current_date = date();
    //echo "<pre>";
    //print_r($get_user_info);
    //die;  

    $datetime1 = new DateTime($current_date);
    $datetime2 = new DateTime($age1);
    $her_age = $datetime1->diff($datetime2);
    $female_age = $her_age->y;

    $datetime3 = new DateTime($current_date);
    $datetime4 = new DateTime($age2);
    $his_age = $datetime3->diff($datetime4);
    $male_age = $his_age->y;

    $her_hight = $get_user_info['her_hight']['0'];
    $his_hight = $get_user_info['his_hight']['0'];

    $her_figure = $get_user_info['her_figure']['0'];
    $explodeHerfigure = explode('(',$her_figure);      

    $his_figure = $get_user_info['his_figure']['0'];
    $explodeHisFigure = explode('(',$his_figure);

    $her_shave = $get_user_info['her_shave']['0'];
    $his_shave = $get_user_info['his_shave']['0'];

    $her_tattoos = $get_user_info['her_tattoos']['0'];
    $his_tattoos = $get_user_info['his_tattoos']['0'];

    $her_smoke = $get_user_info['her_smoke']['0'];
    $his_smoke = $get_user_info['his_smoke']['0'];

    $her_language = $get_user_info['her_language']['0'];
    $her_language1 = $get_user_info['her_language1']['0'];
    $her_language2 = $get_user_info['her_language2']['0'];
    $her_language3 = $get_user_info['her_language3']['0'];
    $selectedLanguages = "";
    if($her_language != ""){
    $selectedLanguages .= $her_language . ",";
    }
    if($her_language1 != ""){
    $selectedLanguages .= $her_language1 . ",";
    }
    if($her_language2 != ""){
    $selectedLanguages .= $her_language2 . ",";
    }
    if($her_language3 != ""){
    $selectedLanguages .= $her_language3 . ",";
    }
    $language_array = array($her_language,$her_language1,$her_language2,$her_language3);

    $his_language   = $get_user_info['his_language']['0'];
    $secondLang = $get_user_info['his_language1']['0'];
    $thrd_lang = $get_user_info['his_language2']['0'];
    $forLang =  $get_user_info['his_language3']['0'];   

    $selectedLanguages1 = "";
    if($his_language != ""){
    $selectedLanguages1 .= $his_language . ",";
    }
    if($secondLang != ""){
    $selectedLanguages1 .= $secondLang . ",";
    }
    if($thrd_lang != ""){
    $selectedLanguages1 .= $thrd_lang . ",";
    }
    if($forLang != ""){
    $selectedLanguages1 .= $forLang . ",";
    }   

    $language_array1 = array($his_language,$secondLang,$thrd_lang,$forLang);
        
    $her_weight = $get_user_info['her_weight']['0'];
    $his_weight = $get_user_info['his_weight']['0'];

    $her_profession = $get_user_info['her_profession']['0'];
    $his_profession = $get_user_info['his_profession']['0'];

    $her_hobby = $get_user_info['her_hobby']['0'];
    $his_hobby = $get_user_info['his_hobby']['0'];

    $describe = $get_user_info['describe']['0'];
    $des_status = $get_user_info['describe_status']['0'];
    $fav_quote_content = $get_user_info['faq_quote']['0'];

    $nickname = $get_user_info['nickname']['0'];
    $first_name = $get_user_info['first_name']['0'];
    $last_name = $get_user_info['last_name']['0'];

    $country = $get_user_info['country']['0'];
    $second_country = $get_user_info['country_second']['0'];

    $district1 = $get_user_info['district1']['0'];
    $second_district = $get_user_info['district_second']['0'];

    $city1 = $get_user_info['city1']['0'];
    $second_city = $get_user_info['city_second']['0'];

    $reg3_txtshe = $get_user_info['reg3_txtshe']['0'];
    $reg3_txthe = $get_user_info['reg3_txthe']['0'];
    $reg3_txtboth = $get_user_info['reg3_both']['0'];
    $relation_ship_status       = $get_user_info['relation_ship_status']['0'];
    $realtionship = get_user_meta($current_user->ID,'relation_ship',true);

    #========== End Age for couples
    #change user type
    $user_type_name =  get_user_meta($current_user->ID ,'user_type', true );
    $profile_name = get_option($user_type_name);
    // for perfect match
    $setting = $wpdb->get_results("select * from wp_admin_portal_setting where portal_setting_type = 'portal setting' "); 
    $id = get_the_ID();
    $page_title = get_page($id);
    $page_title->post_title;
?>
<?php $posts_array = get_posts('cat=15&posts_per_page=-1&orderby=post_date&order=ASC&post_type=intro-background'); 
?>
<style type="text/css">
    .marker {
    background-color: #ffff00;
}
</style>

<?php
if (is_user_logged_in()) 
{
?>

<div class="userProfile">
    <div class="container">        
        <div class="row">
            <div class="col-sm-12">
                <form class="form-horizontal" role="form" name="form" method="post">
                    <div class="col-sm-7 no-padding-right">
                        <div class="col-sm-6 no-padding profilePicWrap">
				            <?php if($get_user_info['approved_as_basic']['0'] == 'no'){ ?>
                            <div class="">								
                                <div class="col-sm-6 no-padding-left">
								    <div class="">								        
                                        <button type="button" class="editLink" data-toggle="modal" data-target="#edit_female_pick" id="edit">edit</button>                                
                                        <h3 class="search_premium_heading red_heading">
                                            <?php echo $get_user_info['nickname']['0']; ?>
                                        </h3>
                                        <?php if($get_user_info['her_pick']['0']!='no_img.jpg') { ?>
                                            <img class="prof-cartoon-img" id="female_show_img" src="<?php echo $image_url_female; ?>">  
                                        <?php } else { ?>
										      <img class="prof-cartoon-img" id="female_show_img" src="<?php echo bloginfo('template_url'); ?>/images/images.jpeg">
										<?php } ?>  
                                    </div>
								</div>
                                <div class="col-sm-6 no-padding-left">
								    <div class="posRel">								        
                                        <button type="button" class="editLink" data-toggle="modal" data-target="#edit_male_pick" id="edit">edit</button>  
                                        <?php if($get_user_info['his_pick']['0']!='no_img.jpg') { ?>                              
                                            <img class="prof-cartoon-img" id="male_show_img" src="<?php echo $image_url_male; ?>">
                                        <?php } else { ?>
										    <img class="prof-cartoon-img" id="male_show_img" src="<?php echo bloginfo('template_url'); ?>/images/images.jpeg">
										<?php } ?>                                 
                                    </div>
								</div>
                            </div> 
                            <?php } else { ?>
    						    <div class="">
                                    <div class="col-sm-6 no-padding-left">
                                        <button type="button" class="editLink" data-toggle="modal" data-target="#edit_female_pick" id="edit">edit</button>                                
                                        <h3 class="search_premium_heading red_heading"><?php echo $get_user_info['nickname']['0']; ?></h3>
                                        <img class="prof-cartoon-img" id="female_show_img" src="<?php echo $image_url_female; ?>">      
                                    </div>
                                    <div class="col-sm-6 no-padding-left">
                                        <button type="button" class="editLink" data-toggle="modal" data-target="#edit_male_pick" id="edit">edit</button>                                
                                        <img class="prof-cartoon-img" id="male_show_img" src="<?php echo $image_url_male; ?>">
                                    </div>
                                </div> 
						   <?php  } ?>
                        </div>
                        <div class="col-sm-6 no-padding">
                           <?php if ($user_type_name == 'premium') { ?>
                                <h5 class="red profileType">
                                    <?php if($profile_name){echo $profile_name;}else{echo "Premium Member";}?>
                                </h5>
                            <?php } elseif ($user_type_name == 'validated') { ?>   
                                <h5 class="greenText profileType">
                                    <?php echo "Validated Member";?>
                                    <img width="12" src="<?php echo bloginfo('template_url'); ?>/images/tick-green.png" alt="">
                                </h5>
                            <?php } elseif ($user_type_name == 'basic' && $get_user_info['approved_as_basic']['0'] == 'yes') { ?>
                                <h5 class="greenText profileType" style="color:#989898;">
                                    <?php if($profile_name){echo $profile_name;}else{echo "Basic Member";}?>
                                </h5>
                            <?php }elseif ($get_user_info['approved_as_basic']['0'] == 'no'){ ?>
                            <h5 class="greenText profileType" style="color:#989898;"><?php echo "Pre-membership stage"; ?></h5>
                            <?php } ?>

                            <div class="Profile-Part-BI  cus_scroll" id="hideuserInfo">
                                <button type="button" class="editLink" data-toggle="modal" data-target="#edit_basic_info" id="edit">edit</button>
                                <h3 class="Proh-h3"><span>BASIC INFO</span></h3>
                                <div class="row">
                                    <p class="Pro-p col-sm-4 col-xs-offset-4">she</p>
                                    <p class="Pro-p col-sm-4">he</p> 
                                </div>
                                <div class="padding-basic-info" id="show_basic_info">
                                    <div class="form-group border-bottom">
                                        <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Age</label>
                                        <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center bold">
                                            <?php echo $female_age; ?>
                                        </label>
                                        <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center bold">
                                            <?php echo $male_age; ?>
                                        </label>
                                    </div>
                                    <div class="form-group border-bottom">
                                        <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Height (cm)</label>
                                        <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center bold">
                                            <?php echo $her_hight; ?>
                                        </label>
                                        <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center bold">
                                            <?php echo $his_hight; ?>
                                        </label>
                                    </div>
                                    <div class="form-group border-bottom">
                                        <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Figure</label>
                                        <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center">
                                            <span class="normalText"><?php echo $explodeHerfigure[0]; ?></span>
                                            <span class="normalText"><?php echo ($explodeHerfigure[1] != '') ? str_replace(')', '', $explodeHerfigure[1]) :''; ?></span>
                                        </label>
                                        <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center">
                                            <span class="normalText"><?php echo $explodeHisFigure[0]; ?></span>
                                            <span class="normalText"><?php echo ($explodeHisFigure[1] != '') ? str_replace(')', '', $explodeHisFigure[1]) :''; ?></span>
                                        </label>
                                    </div>
                                    <div class="form-group border-bottom">
                                        <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Relationship</label>
                                        <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center">
                                            <?php echo get_user_meta($current_user->ID,'relation_ship',true);  ?>
                                        </label>
                                    </div>
                                    <div class="form-group border-bottom">
                                        <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Smoking</label>
                                        <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center">
                                            <?php echo $her_smoke; ?>
                                        </label>
                                        <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center">
                                            <?php echo $his_smoke; ?>
                                        </label>
                                    </div>

                                    <div class="form-group border-bottom row">
                                        <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Hair color</label>
                                        <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center">
                                            <?php echo $get_user_info['her_haircolor']['0']; ?>
                                        </label>
                                        <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center">
                                            <?php echo $get_user_info['his_haircolor']['0']; ?>
                                        </label>
                                    </div>
                                    

                                    <div class="form-group border-bottom">
                                        <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Languages</label>
                                        <label for="inputEmail3" class="col-sm-4 BasicInfo-label ">
                                            <?php echo str_replace(',', ', ', rtrim($selectedLanguages,",")); ?>
                                        </label>
                                        <label for="inputEmail3" class="col-sm-4 BasicInfo-label">
                                            <?php echo str_replace(',', ', ', rtrim($selectedLanguages1,",")); ?>
                                        </label>
                                    </div>

                                    <div class="form-group border-bottom">
                                        <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Education</label>
                                        <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center">
                                            <?php echo $get_user_info['her_graduation']['0']; ?>
                                        </label>
                                        <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center">
                                            <?php echo $get_user_info['his_graduation']['0']; ?>
                                        </label>
                                    </div>                                        
                                    <div class="form-group border-bottom">
                                        <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Hobbies Interests</label>
                                        <label for="inputEmail3" class="col-sm-4 BasicInfo-label">
                                            <?php echo $her_hobby; ?>
                                        </label>
                                        <label for="inputEmail3" class="col-sm-4 BasicInfo-label">
                                            <?php echo $his_hobby; ?>
                                        </label>
                                    </div>
                                    <?php 
                                    if(isset($_GET['user_id'])){
                                        if($current_user->ID == $_GET['user_id']){
                                    ?>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Additional info</label>
                                        <label for="inputEmail3" class="col-sm-4 BasicInfo-label">
                                            <?php echo $get_user_info['about_her']['0']; ?>
                                        </label>
                                        <label for="inputEmail3" class="col-sm-4 BasicInfo-label">
                                            <?php echo $get_user_info['about_his']['0']; ?>
                                        </label>
                                    </div>  
                                    <?php }
                                    }else{ ?>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Additional info</label>
                                        <label for="inputEmail3" class="col-sm-4 BasicInfo-label">
                                            <?php echo $get_user_info['about_her']['0']; ?>
                                        </label>
                                        <label for="inputEmail3" class="col-sm-4 BasicInfo-label">
                                            <?php echo $get_user_info['about_his']['0']; ?>
                                        </label>
                                    </div> 
                                     <?php } ?>                                 
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div> <!--col-sm-6 -->
                        <div class="clear">
                            <div class="col-sm-4 col-md-4 col-xs-12 no-padding">
                                <div class="list_div">
                                    <ul class="side-bar-profile no-padding">
										<?php if($get_user_info['approved_as_basic']['0'] == 'no'){ ?>
										    <li class="invalid">
                                                <span class="grayTrans"></span>
                                                <a href="javascript:void(0)">
                                                    <i><img src="<?php echo bloginfo("template_url"); ?>/images/hand.jpg"></i>
                                                    <span>i like add to favourites</span>
                                                </a>
                                            </li>
                                            <li class="invalid">
                                                <span class="grayTrans"></span>
                                                <a href="javascript:void(0)"><i><img src="<?php echo bloginfo("template_url"); ?>/images/lip.jpg"></i>
                                                    <span>send a kiss</span>
                                                </a>
                                            </li>
                                            <li class="invalid">
                                                <span class="grayTrans"></span>
                                                <a href="javascript:void(0)">
                                                    <i><img src="<?php echo bloginfo("template_url"); ?>/images/mes1.jpg"></i>
                                                    <span>send mail</span>
                                                </a>
                                            </li>
                                            <li class="invalid">
                                                <span class="grayTrans"></span>
                                                <a href="javascript:void(0)">
                                                    <i><img src="<?php echo bloginfo("template_url"); ?>/images/mes3.jpg"></i>
                                                    <span>mail history</span>
                                                </a>
                                            </li>
                                            <li class="invalid">
                                                <span class="grayTrans"></span>
                                                <a href="javascript:void(0)">
                                                    <i><img src="<?php echo bloginfo("template_url"); ?>/images/lockband.jpg"></i>
                                                    <span>hide/show profile<br/> open/close albums<br/> erotic preferences</span>
                                                </a>
                                            </li>
                                            <li class="invalid">
                                                <span class="grayTrans"></span>
                                                <a href="javascript:void(0)">
                                                    <i><img src="<?php echo bloginfo("template_url"); ?>/images/note.jpg"></i>
                                                    <span>make a note<br/> for this profile</span>
                                                </a>
                                            </li>
                                            <li class="invalid">
                                                <span class="grayTrans"></span>
                                                <a href="javascript:void(0)">
                                                    <i><img src="<?php echo bloginfo("template_url"); ?>/images/block.jpg"></i>
                                                    <span>block this profile</span>
                                                </a>
                                            </li>
                                            <li class="invalid">
                                                <span class="grayTrans"></span>
                                                <a href="javascript:void(0)">
                                                    <i><img src="<?php echo bloginfo("template_url"); ?>/images/submit.jpg"></i>
                                                    <span>submit complaint about this profile</span>
                                                </a>
                                            </li>										
 <?php  } //elseif ($get_user_info['user_type']['0'] == 'premium') { 
                                           else { $get_user_info['user_type']['0']=$get_user_info['user_type']['0'];
                                            ?>
                                                                         
                                                                                                       
                                       <?php  if($operational_setting[3]->$user_type_name != 1){ ?>
                                <li style="position:relative;"> 
									<span class="grayTrans"></span>
								<?php } else { ?>
									<li>
								<?php } ?>	
                                    <a href = "#"><i>
                                        <img src="<?php echo bloginfo("template_url"); ?>/images/hand.jpg">
                                        </i><span>i like<br/>   add to favourites</span>
                                    </a>
                                </li>
                                 
                               <?php  if($operational_setting[5]->$user_type_name != 1){ ?>
                                <li style="position:relative;"> 
									<span class="grayTrans"></span>
								<?php } else { ?>
									<li>
								<?php } ?>	
                                    <a href = "#">
                                        <i><img src="<?php echo bloginfo("template_url"); ?>/images/lip.jpg"></i>
                                        <span>send a kiss</span>
                                    </a>
                                </li>
                  
									<li>
								
                                    <a href ="#" >
                                        <i><img src="<?php echo bloginfo("template_url"); ?>/images/mes1.jpg"></i>
                                        <span>send mail</span>
                                    </a>
                                </li>
                               <?php  if($operational_setting[2]->$user_type_name < 0 ){ ?>
                                <li style="position:relative;"> 
									<span class="grayTrans"></span>
								<?php } else { ?>
									<li>
								<?php } ?>	
                                    <i><img src="<?php echo bloginfo("template_url"); ?>/images/mes3.jpg"></i>
                                    <span>mail history</span>
                                </li>
       
								<li>
                                <?php echo ($user_type_name == 'premium') ? "" : "<span class='grayTrans'></span>"; ?>							
                                    <i><img src="<?php echo bloginfo("template_url"); ?>/images/lockband.jpg"></i>
                                    <span>hide/show profile<br/> open/close albums<br/> erotic preferences</span>
                                </li>
                                <li style="position:relative;">
                                 <?php  if($operational_setting[6]->$user_type_name != 1){ ?>                                 
									<span class="grayTrans"></span>
								<?php } ?>	
                                    <i><img src="<?php echo bloginfo("template_url"); ?>/images/note.jpg"></i>
                                    <span>make a note <br/>for this profile</span>
                                </li> 
							<?php if ($get_user_info['user_type']['0'] == 'basic') {?>     
                                <li style="position:relative;"> 
									<span class="grayTrans"></span>	
							
                                    <i><img src="<?php echo bloginfo("template_url"); ?>/images/block.jpg"></i>
                                    <span>block this profile</span>
                                </li>
                     
                                <li style="position:relative;"> 
									<span class="grayTrans"></span>	
							
                                    <i><img src="<?php echo bloginfo("template_url"); ?>/images/submit.jpg"></i>
                                    <span>submit complaint<br/> about this profile</span>
                                </li>
                                <?php } else { ?>
                                
                                <li> 
                                    <i><img src="<?php echo bloginfo("template_url"); ?>/images/block.jpg"></i>
                                    <span>block this profile</span>
                                </li>
                                <li> 
							     <i><img src="<?php echo bloginfo("template_url"); ?>/images/submit.jpg"></i>
                                    <span>submit complaint<br/> about this profile</span>
                                </li>
                                <?php } } /* elseif ($get_user_info['user_type']['0'] == 'validated') { ?>


                                                <li>
                                                    <a href = "#"><i><img src="<?php echo bloginfo("template_url"); ?>/images/hand.jpg"></i><span>i like<br/>   add to favourites</span></a></li>

                                                <li class="invalid"><span class="grayTrans"></span><a href = "#"><i><img src="<?php echo bloginfo("template_url"); ?>/images/lip.jpg"></i><span>send a kiss</span></a></li>

                                                <li><a href ="#" ><i><img src="<?php echo bloginfo("template_url"); ?>/images/mes1.jpg"></i><span>send mail</span></a></li>

                                                <li><a href ="#" ><i><img src="<?php echo bloginfo("template_url"); ?>/images/mes3.jpg"></i><span>mail history</span></a></li>

                                                <li class="invalid"><div class="clear"></div><span class="grayTrans"></span><a href ="#" ><i><img src="<?php echo bloginfo("template_url"); ?>/images/lockband.jpg"></i><span>hide/show profile<br/> open/close albums<br/> erotic preferences</span></a></li>

                                                <li class="invalid"><div class="clear"></div><span class="grayTrans"></span><a href ="#" ><i><img src="<?php echo bloginfo("template_url"); ?>/images/note.jpg"></i><span>make a note <br/>for this profile</span></a></li>

                                                <li><a href ="#" ><i><img src="<?php echo bloginfo("template_url"); ?>/images/block.jpg"></i><span>block this profile</span></a></li>

                                                <li><a href ="#" ><i><img src="<?php echo bloginfo("template_url"); ?>/images/submit.jpg"></i><span>submit complaint<br/> about this profile</span></a></li>


                                            <?php } elseif ($get_user_info['user_type']['0'] == 'basic') { ?>



                                                <li class="invalid"><span class="grayTrans"></span><a href = "#"><i>
                                                            <img src="<?php echo bloginfo("template_url"); ?>/images/hand.jpg">
                                                        </i><span>i like add to favourites</span></a></li>

                                                <li class="invalid"><span class="grayTrans"></span><a href = "#"><i><img src="<?php echo bloginfo("template_url"); ?>/images/lip.jpg"></i><span>send a kiss</span></a></li>

                                                <li><a href ="#" ><i><img src="<?php echo bloginfo("template_url"); ?>/images/mes1.jpg"></i><span>send mail</span></a></li>

                                                <li><a href ="#" ><i><img src="<?php echo bloginfo("template_url"); ?>/images/mes3.jpg"></i><span>mail history</span></a></li>

                                                <li class="invalid"><span class="grayTrans"></span><a href ="#" ><i><img src="<?php echo bloginfo("template_url"); ?>/images/lockband.jpg"></i><span>hide/show profile<br/> open/close albums<br/> erotic preferences</span></a></li>

                                                <li class="invalid"><span class="grayTrans"><a href ="#" ></span><i><img src="<?php echo bloginfo("template_url"); ?>/images/note.jpg"></i><span>make a note<br/> for this profile</span></a></li>

                                                <li class="invalid"><span class="grayTrans"></span><a href ="#" ><i><img src="<?php echo bloginfo("template_url"); ?>/images/block.jpg"></i><span>block this profile</span></a></li>

                                                <li class="invalid"><span class="grayTrans"></span><a href ="#" ><i><img src="<?php echo bloginfo("template_url"); ?>/images/submit.jpg"></i><span>submit complaint about this profile</span></a></li>

                                            <?php }*/ ?>
                                    </ul>
                                </div>
                                <div class="clear"></div>
                                <hr/>
                                <span class="border-bottom"></span>
                                <?php if($operational_setting[9]->$user_type_name  == 1 && $get_user_info['approved_as_basic']['0'] != 'no'){
                                    $user_type_name=$get_user_info['user_type']['0'];
                                 ?>
                                    <div id="addText" class="text-box-round">                                        
                                        <?php 
                                        $dating_ads=$wpdb->get_results("SELECT * FROM wp_dating_ads WHERE user_id=".$current_user->ID." ORDER BY ID DESC LIMIT 0,2");
                                        $upload_dir = wp_upload_dir();
                                        $img_srcpath = $upload_dir['baseurl'] . '/dating_ads/'; ?>
                                        <h5>OUR SPEED DATING</h5>
                                    <?php foreach ($dating_ads as $value) { 
                                          $from=strtotime($value->from);
                                          $to=strtotime($value->to); 
                                          $from_m=date('m', $from);
                                          $monthName_from = date('F', mktime(0, 0, 0, $from_m, 10));
                                          $to_m=date('m', $to);
                                          $monthName_to = date('F', mktime(0, 0, 0, $to_m, 10));
                                          $from_y=date('Y', $from);
                                          $to_y=date('Y', $to);
                                          $from_d=date('d', $from);
                                          $to_d=date('d',$to);
                                          $country=$value->country;
                                          if($value->region != "")
                                          {
                                            $city= ' - ' . $value->region;
                                          }else
                                          {
                                            $city= "";
                                          }
                                     ?>
                                    <div class="form-group"> 
                                       <div class="adds_content no-padding-left col-sm-7">
                                       <p>
                                       <?php echo $country; ?>
                                       <span class="city_white"><?php echo $city; ?></span><br/>
                                       <?php echo $from_d."-".$to_d; ?>
                                       <?php if($from_m==$to_m){ echo $monthName_from; }
                                       else{ echo $monthName_from."-".$monthName_to; }
                                       echo $to_y; ?>
                                       </p>
                                       <a href="<?php echo home_url(); ?>/?page_id=735&user_id=<?php echo $current_user->ID; ?>" class="goAdd pull-right">go to ads</a>
                                       </div>
                                       <div class="col-sm-5">
                                          <img src="<?php echo $img_srcpath.$value->ads_pick; ?>" alt="adds image" class="img-responsive">
                                       </div>
                                     </div>
                                   <?php } ?>
                                    </div>
                                    <?php } else { ?>
                                    <div class="text-box-round invalid">
                                        <span class="grayTrans"></span>
                                        <?php $speed_dating = get_post(362); ?>
                                        <h5><?php echo $speed_dating->post_title; ?></h5>
                                        <p><?php // echo $get_user_info['speed_data']['0']; ?></p>
                                    </div>
                                    <?php } 
                                    if($operational_setting[10]->$user_type_name  == 1 && $get_user_info['approved_as_basic']['0'] != 'no') { ?>
                                    <div class="text-box-round">
                                        <h5>OUR TRAVEL AGENDA</h5>
                                        <?php $travels_ads=$wpdb->get_results("SELECT * FROM wp_travel_dating WHERE tarvel_logged_user=".$current_user->ID." ORDER BY ID DESC"); ?>
                                        <?php 
                                        foreach ($travels_ads as $value) 
                                        { 
                                            $from=strtotime($value->travel_from);
                                            $to=strtotime($value->travel_to); 
                                            $from_m=date('m', $from);
                                            $monthName_from = date('F', mktime(0, 0, 0, $from_m, 10));
                                            $to_m=date('m', $to);
                                            $monthName_to = date('F', mktime(0, 0, 0, $to_m, 10));
                                            $from_y=date('Y', $from);
                                            $to_y=date('Y', $to);
                                            $from_d=date('d', $from);
                                            $to_d=date('d',$to);
                                            $country=$value->travel_country;
                                            if($value->travel_regions != "")
                                            {
                                                $city= ' - ' . $value->travel_region;
                                            }else
                                            {
                                              $city="";  
                                            }
                                            
                                        ?>
                                           <div class="form-group"> 
                                                <div class="adds_content no-padding-left col-sm-12">
                                                   <p>
                                                   <?php echo $country; ?>
                                                   <span class="city_white"><?php echo $city; ?></span><br/>
                                                   <?php echo $from_d."-".$to_d; ?>
                                                   <?php if($from_m==$to_m)
                                                   {  echo $monthName_from; }
                                                   else { echo $monthName_from."-".$monthName_to; }
                                                   echo $to_y; ?>
                                                   </p>
                                                </div>
                                           </div>
                                      <?php } ?>                                            
                                    </div>
                                    <?php } else { ?>
                                        <div class="text-box-round invalid">
                                            <span class="grayTrans"></span>
                                            <?php $trave_agenda = get_post(364); ?>
                                            <h5><?php echo $trave_agenda->post_title; ?></h5>
                                            <p><?php // echo $get_user_info['travel_agent']['0']; ?></p>
                                        </div>
                                    <?php } ?>
                            </div>

                            <div class="col-sm-8 col-md-8 col-xs-12 no-padding">
                                <div class="col-sm-7 col-md-7 col-xs-12 ">
							        <?php if($get_user_info['approved_as_basic']['0']=='no') {  ?> 		
                                    <div class="Album-Part">
										<span class="grayTrans"></span>
                                        <div class="row"> 
                                            <h3 class="Pro-h3-2  col-sm-12 col-md-12"><span>OUR ALBUMS</span></h2><span class="edit-profile">edit</span>
                                        </div>
                                        <div class="WidthFull">
                                            <?php
                                            global $current_user;
                                            get_currentuserinfo();
                                            $current_user->user_type;
                                            $results = $wpdb->get_results("SELECT * FROM user_create_album WHERE user_id = '$current_user->ID'");
                                            $upload_dir = wp_upload_dir();
                                            $imgPath = $upload_dir['baseurl'] . '/users_album/album_' . $current_user->ID . '/';
                                            $i = 1;
                                            $count = 1;
                                            foreach ($results as $value) {
                                                $select_img = $wpdb->get_results("SELECT * FROM user_album_images WHERE album_id = '$value->id' order by img_order ASC limit 1");
                                                if (count($select_img) == 0) {
                                                    ?>
                                                    <figure class="album-img-div text-center">
                                                        <img src="<?php bloginfo('template_url'); ?>/images/NoAlbum.jpeg" alt="">
                                                        <figcaption><?php echo $count++; ?></figcaption>
                                                    </figure>
                                                    <?php
                                                } else {
                                                    foreach ($select_img as $select) {
                                                        $number = '"' . $value->id . '"';
                                                        ?>                                                            
                                                        <figure class="album-img-div text-center"><a href="<?php echo get_permalink(424); ?>&imgid=<?php echo base64_encode($number); ?>" name='imgid' target='_blank' value="<?php echo $value->id; ?>" >
                                                                <img src="<?php echo $imgPath . $select->img_url; ?>"></a><figcaption><?php echo $count++; ?></figcaption></figure>                                      
                                                        <?php
                                                    }
                                                }
                                            }
                                            if (count($results) != 0) {
                                                $sum = 1;
                                                for ($i = 0; $i < (5 - count($results)); $i++) {
                                                    ?>
                                                    <figure class="album-img-div text-center">
                                                        <img src="<?php bloginfo('template_url'); ?>/images/alb_2.jpg" alt="">
                                                        <figcaption><?php echo count($results) + $sum; ?></figcaption>
                                                    </figure>
                                                    <?php
                                                    $sum++;
                                                }
                                            } else {
                                                echo "No Album";
                                            }
                                            ?>
                                        </div>                                        
                                    </div>
                                <?php } else { ?>
				                    <div class="Album-Part">
                                        <div class="row"> 
                                            <h3 class="Pro-h3-2  col-sm-12 col-md-12"><span>OUR ALBUMS</span></h2><span class="edit-profile">edit</span>
                                        </div>
                                        <div class="WidthFull">
                                            <?php
                                            global $current_user;
                                            get_currentuserinfo();
                                            $current_user->user_type;
                                            $results = $wpdb->get_results("SELECT * FROM user_create_album WHERE user_id = '$current_user->ID'");
                                            $upload_dir = wp_upload_dir();
                                            $imgPath = $upload_dir['baseurl'] . '/users_album/album_' . $current_user->ID . '/';
                                            $i = 1;
                                            $count = 1;
                                            foreach ($results as $value) {
                                                $select_img = $wpdb->get_results("SELECT * FROM user_album_images WHERE album_id = '$value->id' order by img_order ASC limit 1");
                                                if (count($select_img) == 0) {
                                                    ?>
                                                    <figure class="album-img-div text-center">
                                                        <img src="<?php bloginfo('template_url'); ?>/images/no_image_avl.jpeg" alt="">
                                                        <figcaption><?php echo $value->album_name; ?></figcaption>
                                                    </figure>
                                                    <?php
                                                } else {
                                                    foreach ($select_img as $select) {
                                                        $number = '"' . $value->id . '"';
                                                        ?>                                                            
                                                        <figure class="album-img-div text-center"><a href="<?php echo home_url(); ?>/?page_id=761&album=<?php echo $value->id; ?>&user_id=<?php echo $value->user_id; ?>" name='imgid' target='_blank' value="<?php echo $value->id; ?>" >
                                                                <img src="<?php echo $imgPath . $select->img_url; ?>"></a><figcaption><?php echo $value->album_name; ?></figcaption></figure>                                      
                                                        <?php
                                                    }
                                                }
                                            }
                                            if (count($results) != 0) {
                                                $sum = 1;
                                                for ($i = 0; $i < (5 - count($results)); $i++) {
                                                    if((count($results)+$i)<$user_alb_count){?>
                                                <figure class="album-img-div text-center">
                                                    <img src="<?php bloginfo('template_url'); ?>/images/album_dummy.gif" alt="">
                                                    <figcaption>no album</figcaption>
                                                </figure>
                                                <?php } else { ?>
                                                    <figure class="album-img-div text-center">
                                                     <span class="grayTrans"></span>
                                                    <img src="<?php bloginfo('template_url'); ?>/images/album_dummy.gif" alt="">
                                                    <figcaption></figcaption>
                                                </figure>
                                                <?php }
                                                    $sum++;
                                                }
                                            } else {
                                                for ($i = 0; $i < (5 - count($results)); $i++) {
                                                if($i<$user_alb_count){?>
                                                <figure class="album-img-div text-center">
                                                    <img src="<?php bloginfo('template_url'); ?>/images/album_dummy.gif" alt="">
                                                    <figcaption>no album</figcaption>
                                                </figure>
                                                <?php } else { ?>
                                                    <figure class="album-img-div text-center">
                                                     <span class="grayTrans"></span>
                                                    <img src="<?php bloginfo('template_url'); ?>/images/album_dummy.gif" alt="">
                                                    <figcaption></figcaption>
                                                </figure>
                                                <?php }
                $sum++;
            }
                                            }
                                            ?>
                                        </div>
                                       
                                    </div>
				                <?php } ?>
                            </div>
                                <div class="col-sm-5 col-md-5 col-xs-12 no-padding">
									<?php if($get_user_info['approved_as_basic']['0']=='no' ) {  ?> 
                                    <div class="Favourite-Part">
										<span class="grayTrans"></span>
                                        <div class=""> 
                                            <h3 class="Pro-h4 col-sm-12 col-md-12"><span>FAVOURITE QUOTE</span></h3><span>
                                                    <button type="button" class="editLink" data-toggle="modal" data-target="#fav_quote" id="edit">edit</button></span>
                                        </div>
                                        <div class="col-sm-12 col-md-12 no-padding" id="fav-para"> 
                                            <?php echo $fav_quote_content; ?>
                                        </div>
                                    </div>
                                <?php } else { ?>
									
								<div class="Favourite-Part">
                                        <div class=""> 
                                            <h3 class="Pro-h4 col-sm-12 col-md-12"><span>FAVOURITE QUOTE</span></h3>
                                            <span>
                                                <button type="button" class="editLink" data-toggle="modal" data-target="#fav_quote" id="edit">edit</button>
                                            </span>
                                        </div>
                                        <div class="col-sm-12 col-md-12 no-padding" id="fav-para"> 
                                            <?php echo $fav_quote_content; ?>
                                        </div>
                                    </div>
                                <?php } ?>	
                            </div>
                                <?php
                                $results = $wpdb->get_results("SELECT * FROM bg_images");
                                //$upload_dir = wp_upload_dir();
                                //$img = "";
                                foreach ($results as $value) {
                                    $imgPath = $value->img_url;
                                }
                                //$imgPath = $upload_dir['baseurl'] . '/background_images' . '/' . $img;
                                ?>
                                <div class="col-sm-12 col-md-12 col-xs-12 no-padding-right prelative">
                                    <h3 class="Pro-h3 newIntro"><span>INTRODUCTION</span></h3><span></span>
                                    <div class="change_bg">
                                        <span>
                                            <button type="button" class="changeLanguage" data-toggle="modal" data-target="#change_background" id="edit">Change Background</button>
                                        </span>
                                    </div>
                                    <button type="button" class="editLink" data-toggle="modal" data-target="#edit_intro" id="edit">edit</button>
                                   <div  id="content-1"> 
                                        <div class="Introduction-Part" style="background-size: 100% 100%; background-image: url('<?php echo $imgPath; ?>');" id ="introduction_part">
                                            <div class="padding-para-div" id="intro_data"> 
                                            <?php 
                                                if($des_status == '' && $describe != '')
                                                {
                                                    $message = "Your introduction not approved by admin yet!";
                                                }else if($des_status == 'refused'){
                                                    $message = 'Your introduction has refused by admin!';
                                                }else
                                                {
                                                   $message = $describe;
                                                }                                        
                                                echo $message; 
                                            ?>                                                 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5 col-md-5 col-xs-12">
                        <div class="col-sm-6 col-md-6 col-xs-12 no-padding">
                            <div class="Favourite-Part2">
                                <div class=""> 
                                    <h3 class="Pro-h3 col-sm-12 col-md-12"><span>MAIN RESIDENCE</span></h2> <span><button type="button" class="editLink" data-toggle="modal" data-target="#edit_main_residence" id="edit">edit</button></span>
                                </div>
                                <div class="padding-top-bottom" id="show_main_residence">
                                    <input type="hidden" name="first_add" value="first">
                                    <div class="form-group"> 
                                        <label for="inputEmail3" class="col-sm-5 BasicInfo-label">COUNTRY :</label>
                                        <label for="inputEmail3" class="col-sm-7 BasicInfo-label para-left"><?php echo $country; ?></label>
                                    </div>
                                    <div class="form-group"> 
                                        <label for="inputEmail3" class="col-sm-5 BasicInfo-label">REGIO :</label>
                                        <label for="inputEmail3" class="col-sm-7 BasicInfo-label para-left"><?php echo $district1; ?></label>
                                    </div>
                                    <div class="form-group"> 
                                        <label for="inputEmail3" class="col-sm-5 BasicInfo-label">NEXT CITY :</label>
                                        <label for="inputEmail3" class="col-sm-7 BasicInfo-label para-left"><?php echo $city1; ?></label>
                                    </div>
                                    <div class="form-group"> 
                                        <label for="inputEmail3" class="col-sm-5 BasicInfo-label">Frequently in :</label>
                                        <label for="inputEmail3" class="col-sm-7 BasicInfo-label para-left"><?php echo $get_user_info['frequently_in']['0']; ?></label>
                                    </div>
                                </div>
                            </div>
                            
                                <div class="Favourite-Part3 <?php if($portalsetting[2]->$user_type_name == '0'){echo 'invalid' ;}?>">
                                    <?php if($portalsetting[2]->$user_type_name == '0'){echo '<span class="grayTrans-more"></span>';}?>
                                    <div class=""> 
                                        <h3 class="Pro-h3 col-sm-12 col-md-12">
                                            <span>MATCHES</span>
                                        </h3> 
                                        <!-- <span>
                                            <button type="button" class="editLink" data-toggle="modal" data-target="#edit_second_residence" id="edit">view</button>
                                        </span> -->
                                    </div>
                                    <div class="padding-top-bottom">
                                        <div class="row"> 
                                        <?php
                                        $get_sender_data = $wpdb->get_results("SELECT reciver_id FROM wp_user_profile WHERE sender_id ='" . $current_user->ID . "' AND add_fav='1' ORDER BY id DESC");
                                        $get_reciver_data = $wpdb->get_results("SELECT sender_id FROM  wp_user_profile WHERE reciver_id ='" . $current_user->ID . "' AND add_fav='1' ");
                                        $senderdataid = array();
                                        $count = 1;
                                        foreach ($get_sender_data as $value) {
                                            $senderdataid[] = $value->reciver_id;
                                        }
                                        $alldata = array();
                                        foreach ($get_reciver_data as $data) {
                                            if (in_array($data->sender_id, $senderdataid)) {
                                                $alldata[] = $data->sender_id;
                                            }
                                        }
                                        if (!empty($alldata)) {
                                            foreach ($alldata as $mat_value) {
                                                if ($count == 4) {
                                                    break;
                                                }
                                                $senderID = $mat_value;
                                                $user_name = get_user_meta($senderID, 'nickname', true);
                                                $userUrl = get_permalink(305) . '&user_id=' . $senderID;
                                                #.. sender user pic data
                                                $his_pick = get_user_meta($senderID, 'his_pick', true);
                                                $her_pick = get_user_meta($senderID, 'her_pick', true);
                                                if (!empty($her_pick)) {
                                                    $pick_name = $her_pick;
                                                } else {
                                                    $pick_name = $his_pick;
                                                }
                                                $pick_source = $upload_dir['baseurl'] . '/profilepick_' . $senderID . '/' . $pick_name;
                                                ?>
                                                <?php if($profile_status!='Deactive')
                                        { ?>
                                        <div class="col-xs-4">
                                            <img src="<?php echo $pick_source; ?>" height="45" width="52">
                                            <label class="matchName"><?php echo $user_name; ?></label>
                                        </div>
                                        <?php } $count++; } } else
                                        {
                                            echo 'No Matches';
                                        }
                                    ?>  
                                        </div>           
                                    </div>
                                </div>
                            
                            <?php  
                            $userid = get_current_user_id();
                            $arr = array();
                            $subId = array();
                            $status = array();
                            $myrows = $wpdb->get_results("SELECT *  FROM `wp_erotics_preference` where `userid` = $userid AND `type` = 1 order by like_dislike_attr_id" );
                            foreach ($myrows as $result) {
                                $arr[] = $result->like_dislike_attr_id;
                                $subId[] = $result->subject_id;
                                $status[] = unserialize($result->status);
                            }                                                       

                            $myrows1 = $wpdb->get_results("SELECT *  FROM `wp_erotics_preference` where `userid` = $userid AND `type` = 2 order by like_dislike_attr_id" );
                            $arr1 = array();
                            $subId1 = array();
                            $status1 = array();

                            foreach ($myrows1 as $result) {
                                $arr1[] = $result->like_dislike_attr_id;
                                $subId1[] = $result->subject_id;
                                $status1[] = unserialize($result->status);
                            }

                            $arg = array('posts_per_page' => -1, 'offset' => 1, 'category' => 14, 'post_type' => 'eroticoption', 'order' => 'asc');
                            $myposts = get_posts($arg);

                            $myAllposts = array();
                            $ctagoryid = array("3", "4", "5");

                            foreach ($ctagoryid as $values) {
                                $args = array('posts_per_page' => -1, 'offset' => 1, 'category' => $values->ID, 'post_type' => 'erotics', 'order' => 'asc', 'show_count' => 0);
                                $myAllposts = get_posts($args);
                            }

                            $content = array();
                            foreach ($subId as $key => $value) 
                            {                          
                                $content['subject'][] = get_the_title($value); 
                                $content['subjectID'][] =  $value;                   
                            }


                            foreach ($arr as $key => $value1) {
                                foreach ($myposts as $value) {
                                    if ($value1 == $value->ID) {
                                        $content['desc'][] = $value->post_title;
                                        if(!(in_array($value->ID, $content['id'])))
                                        {
                                            $content['id'][] = $value->ID;
                                        } 
                                    }
                                }
                            }

                            $content1 = array();
                            foreach ($subId1 as $key => $value) 
                            {                          
                                $content1['subject'][] = get_the_title($value);
                                $content1['subjectID'][] =  $value;                    
                            }

                            foreach ($arr1 as $key => $value1) {
                                foreach ($myposts as $value) {
                                    if ($value1 == $value->ID) {
                                        $content1['desc'][] = $value->post_title;
                                        if(!(in_array($value->ID, $content1['id'])))
                                        {
                                            $content1['id'][] = $value->ID;
                                        } 
                                    }
                                }
                            }
                            $unique_arr_desc=array_unique($content['desc']);
                            foreach ($unique_arr_desc as $value) 
                            {
                               $unique_arr_dist[]=$value;
                            }
                            $unique_arr_desc1=array_unique($content1['desc']);
                            foreach ($unique_arr_desc1 as $value) 
                            {
                               $unique_arr_dist1[]=$value;
                            }
                            ?>
                            <div class="eropic-profile-Part">
								 <?php echo ($get_user_info['approved_as_basic']['0'] == 'no' || $setting[1]->$user_type_name != '1') ? "<span class='grayTrans'></span>"  : "" ?>
                                    <div class=""> 
                                        <h3 class="eropic-h3 no-margin no-padding col-sm-12 col-md-12"><span>Her erotic profile</span> 
                                        <a href ="<?php echo get_permalink(245); ?>" class="editLink" id="edit">edit</a>
                                        <a class="profile-her-erotic-right-btn" href="#">E</a> </h3> 
                                    </div>
                                    <?php
                                    $count = 0;
                                    for ($i = 0; $i < count($unique_arr_dist); $i++) {
                                        $color = $content['id'][$count];
                                        $getTileShow = getActiveOrPassviveTitle($wpdb,$userid,$color, '1');
                                        ?>				
                                        <div class="eropic-box-round " style="background-color:<?php echo ($bgColor != "") ? $bgColor : $textColor[$color]; ?>;color:<?php echo ($bgColor != "") ? $bgColor : $forColor[$color]; ?>;">
                                            <p><strong class="up-case"><?php echo strtoupper(qtrans_use($q_config['language'],$unique_arr_dist[$i], true)); ?> :</strong></p>
                                            <?php for($j = 0; $j < count($content['subject']); $j++)
                                            {
                                                if($content['desc'][$j]==$unique_arr_dist[$i])
                                                { 
                                                    $like_dislike_ID = $content['subjectID'][$j];
                                                    $text = getActiveOrPassvive($wpdb,$userid,$color,$like_dislike_ID, '1');
                                                ?>
                                            <p style=""><?php echo qtrans_use($q_config['language'],$content['subject'][$j], true) .' '. $text['text']; ?></p>
                                            <?php }
                                            } ?>
                                        </div>
                                        <?php $count++; }?>
                                </div>                    	
                        </div>
                        <div class="col-sm-6 col-md-6 col-xs-12 no-padding-right">
							  <?php if ($setting[2]->$user_type_name == '1' && $get_user_info['approved_as_basic']['0'] != 'no') { ?>
                            <div class="perfect-match-Part cus_scrolln">
                                <div class=""> 
                                    <h3 class="opm-h3 col-sm-12 col-md-12"><span>PERFECT MATCH</span></h2>
                                </div>
                                <div  id="content-2">                                        
                                <div style="<?php echo ($reg3_txtshe != "") ? 'display:block' : 'display:none';  ?>" class="opm-text-box-roundsec" id="show_her">
                                    <span><a class="" data-toggle="modal" data-target="#edit_she_perfect_match" id="edit">edit</a></span>
                                    <strong>SHE :</strong><?php echo $reg3_txtshe; ?>  
                                    <p hideID='show_her' type='reg3_txtshe' class="deletematch" style="float:right"><a href="javascript:void(0)">delete this field</a></p>                                          
                                </div> 
                                </div>                                       
                                <p style="<?php echo ($reg3_txtshe != "") ? 'display:none' : 'display:block';  ?>" data-toggle="modal" data-target="#edit_she_perfect_match" class="addladies" style="float:right">
                                    <a href="javascript:void(0)">+add ladies</a>
                                </p>                                        
                                <div style="<?php echo ($reg3_txthe != "") ? 'display:block' : 'display:none';  ?>" class="opm-text-box-roundsec" id="show_his">
                                    <span><a class="" data-toggle="modal" data-target="#edit_he_perfect_match" id="edit">edit</a></span>
                                    <strong>HE :</strong><?php echo $reg3_txthe; ?>
                                    <p hideID = 'show_his' type='reg3_txthe' class="deletematch" style="float:right"><a href="javascript:void(0)">delete this field</a></p>
                                </div>                                        
                                <p style="<?php echo ($reg3_txthe != "") ? 'display:none' : 'display:block';  ?>" data-toggle="modal" data-target="#edit_he_perfect_match" class="addmen" style="float:right">
                                    <a href="javascript:void(0)">+add men</a>
                                </p>
                                <div style="<?php echo ($reg3_txtboth != "") ? 'display:block' : 'display:none';  ?>" class="opm-text-box-roundsec" id="show_both">
                                    <span><a class="" data-toggle="modal" data-target="#edit_couple_perfect_match" id="edit">edit</a></span>
                                    <strong>BOTH :</strong><?php echo $reg3_txtboth; ?>
                                    <p type='reg3_both' hideID = 'show_both' class="deletematch" style="float:right">
                                        <a href="javascript:void(0)">delete this field</a>
                                    </p>
                                </div>                                        
                                <p style="<?php echo ($reg3_txtboth != "") ? 'display:none' : 'display:block';  ?>" data-toggle="modal" data-target="#edit_couple_perfect_match" class="addcouple" style="float:right"><a href="javascript:void(0)">+add couple</a></p>                                        
                        </div> 
                        <?php }else {?>
							
						
                             <div class="perfect-match-Part cus_scrolln invalid">
                                        <span class="greenTrans"></span>
                                <div class=""> 
                                    <h3 class="opm-h3 col-sm-12 col-md-12"><span>PERFECT MATCH</span></h2>
                                </div>                                        
                                <div style="<?php echo ($reg3_txtshe != "") ? 'display:block' : 'display:none';  ?>" class="opm-text-box-roundsec" id="show_her">
                                    <span><a class="" data-toggle="modal" data-target="#edit_she_perfect_match" id="edit">edit</a></span>
                                    <strong>SHE :</strong><?php echo $reg3_txtshe; ?>  
                                    <p hideID='show_her' type='reg3_txtshe' class="deletematch" style="float:right"><a href="javascript:void(0)">delete this field</a></p>                                          
                                </div>                                        
                                <p style="<?php echo ($reg3_txtshe != "") ? 'display:none' : 'display:block';  ?>" data-toggle="modal" data-target="#edit_she_perfect_match" class="addladies" style="float:right">
                                    <a href="javascript:void(0)">+add ladies</a>
                                </p>                                        
                                <div style="<?php echo ($reg3_txthe != "") ? 'display:block' : 'display:none';  ?>" class="opm-text-box-roundsec" id="show_his">
                                    <span><a class="" data-toggle="modal" data-target="#edit_he_perfect_match" id="edit">edit</a></span>
                                    <strong>HE :</strong><?php echo $reg3_txthe; ?>
                                    <p hideID = 'show_his' type='reg3_txthe' class="deletematch" style="float:right"><a href="javascript:void(0)">delete this field</a></p>
                                </div>                                        
                                <p style="<?php echo ($reg3_txthe != "") ? 'display:none' : 'display:block';  ?>" data-toggle="modal" data-target="#edit_he_perfect_match" class="addmen" style="float:right">
                                    <a href="javascript:void(0)">+add men</a>
                                </p>
                                <div style="<?php echo ($reg3_txtboth != "") ? 'display:block' : 'display:none';  ?>" class="opm-text-box-roundsec" id="show_both">
                                    <span><a class="" data-toggle="modal" data-target="#edit_couple_perfect_match" id="edit">edit</a></span>
                                    <strong>BOTH :</strong><?php echo $reg3_txtboth; ?>
                                    <p type='reg3_both' hideID = 'show_both' class="deletematch" style="float:right">
                                        <a href="javascript:void(0)">delete this field</a>
                                    </p>
                                </div>                                        
                                <p style="<?php echo ($reg3_txtboth != "") ? 'display:none' : 'display:block';  ?>" data-toggle="modal" data-target="#edit_couple_perfect_match" class="addcouple" style="float:right"><a href="javascript:void(0)">+add couple</a></p>                                        
                        </div> 
							<?php } ?>

<!--=========================* Start Prefect match section for ladies *===================-->
<div class="modal fade" id="edit_she_perfect_match" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="parent" style="display:none;">
        <span class="loader"></span>
    </div>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="show_she_successMsg1" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Prefect Match</h4>
                <div id="match_show" class="successMsg"></div>
            </div>
            <div class="modal-body" >             
                <div class="editShe">
                    <h2>She</h2>
                    <textarea id='editor5'><?php echo $reg3_txtshe; ?></textarea>               
                    <script>CKEDITOR.replace('editor5',{enterMode: CKEDITOR.ENTER_BR,language: 'en',entities: false});</script>
                    <input type="hidden" name="her_match_content" id="her_match_content" value="to_her">
                    <div class="modal-footer">
                        <button type="button" id="show_she_successMsg" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary update_her_perfect" id ="update_her_perfect">Save changes</button>
                    </div>
                </div> 
            </div>            
        </div>
    </div>
</div>
<!--=========================* Start Prefect match section for gentleman *===================-->
<div class="modal fade" id="edit_he_perfect_match" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="parent" style="display:none;">
        <span class="loader"></span>
    </div>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="show_she_successMsg1" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Prefect Match</h4>
                <div id="match_show" class="successMsg"></div>
            </div>
            <div class="modal-body" >             
                <div class="editShe">
                    <h2>He</h2>
                    <textarea id='editor6'><?php echo $reg3_txthe; ?></textarea>               
                    <script>CKEDITOR.replace('editor6',{enterMode: CKEDITOR.ENTER_BR,language: 'en',entities: false});</script>
                    <input type="hidden" name="his_match_content" id="his_match_content" value="to_his">
                    <div class="modal-footer">
                        <button type="button" id="show_she_successMsg" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id ="update_his_perfect">Save changes</button>
                    </div>
                </div> 
            </div>            
        </div>
    </div>
</div>

<!--=========================* Start Prefect match section *===================-->
<div class="modal fade" id="edit_couple_perfect_match" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="parent" style="display:none;">
        <span class="loader"></span>
    </div>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="show_she_successMsg1" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Prefect Match</h4>
                <div id="match_show" class="successMsg"></div>
            </div>
            <div class="modal-body" > 
                <div class="editBoth">                  
                    <h2>Both</h2>                   
                    <textarea id='editor7'><?php echo $reg3_txtboth; ?></textarea>              
                    <script>CKEDITOR.replace('editor7',{language: 'en',entities: false});</script>              
                    <input type="hidden" name="both_match_content" id="both_match_content" value="to_both">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="hide_his_details" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id ="update_both_section">Save changes</button>
            </div>
        </div>
    </div>
</div>
<!-- End  -->	

                <div class="eropic-profile-Part">
					 <?php  echo ($get_user_info['approved_as_basic']['0'] == 'no' || $setting[1]->$user_type_name != '1') ? "<span class='grayTrans'></span>"  : "" ?>
                        <div class=""> 
                            <h3 class="eropic-h3 no-margin no-padding col-sm-12 col-md-12"><span>His erotic profile</span><a href ="<?php echo get_permalink(245); ?>" class="editLink"   id="edit">edit</a><a class="profile-her-erotic-right-btn" href="#">E </a></h3>
                        </div>
                        <?php
                        $count1 = 0;
                        for ($i = 0; $i < count($unique_arr_dist1); $i++) 
                        {
                            $color1 = $content1['id'][$count1];
                            $textColor[$color1]; 
                            $getTileShow = getActiveOrPassviveTitle($wpdb,$userid,$color1, '2');                                                                                                                                
                        ?>				
                        <div class="eropic-box-round" style="background-color:<?php echo ($bgColor1 != "") ? $bgColor1 : $textColor[$color1]; ?>;color:<?php echo ($bgColor1 != "") ? $bgColor1 : $forColor[$color1]; ?>;">
                            <p class="child"><strong class="up-case"><?php echo strtoupper(qtrans_use($q_config['language'],$unique_arr_dist1[$i], true)); ?> :</strong></p>
                            <?php for($j = 0; $j < count($content1['subject']); $j++)
                             {
                            if($content1['desc'][$j]==$unique_arr_dist1[$i])
                            { 
                                $like_dislike_ID = $content1['subjectID'][$j];
                                $text1 = getActiveOrPassvive($wpdb,$userid,$color1,$like_dislike_ID, '2');
                            ?>
                            <p style=""><?php echo qtrans_use($q_config['language'],$content1['subject'][$j], true) .' '. $text1['text']; ?></p>
                            <?php }
                        } ?>
                        </div>
                        <?php
                        $count1++;
                        }
                        ?>
                    </div>                
            </div>
        </div>
                <input type="hidden" name="logged_in_user" value="<?php echo $current_user->ID; ?>" id ="logged_in_user">
        </form>
    </div>
        <div class="active_passive" style="float:right;margin-right:30%">(a) active (p) passive </div>
    </div>
</div>
</div>

<!--=========================* start Female And male edit profile pick *===================-->
<script type="text/javascript">
function showimagepreview1(input) {
    if (input.files && input.files[0]) {
        var filerdr = new FileReader();
        filerdr.onload = function (e) {
            $("#malepick").remove();
            $('#previewing_male').attr('src', e.target.result);
        }
        filerdr.readAsDataURL(input.files[0]);
    }
}

function showimagepreview2(input) {
    if (input.files && input.files[0]) {
        var filerdr = new FileReader();
        filerdr.onload = function (e) {
            $("#malepick").remove();
            $('#previewing').attr('src', e.target.result);
        }
        filerdr.readAsDataURL(input.files[0]);
    }
}
</script>

<div class="modal fade" id="edit_female_pick" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="parent" style="display:none;"><span class="loader"></span></div>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Female Profile Pic</h4>
                <div id="msg_female" class="successMsg"></div>
            </div>
            <div class="modal-body">
                <form id="uploadimage" action="" method="post" enctype="multipart/form-data">
                    <div id="image_preview" class="imgFrame"><img id="previewing" src="<?php echo bloginfo('template_url'); ?>/images/images.jpeg" /></div>    
                    <div id="selectImage">
                        <label>Select Your Image</label><br/>
                        <input type="file" name="file" id="file" alt="file" onchange ="showimagepreview2(this)" />                            
                    </div>                   
                </form>     
            </div> 
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id ="update_female_pick">Save changes</button>
            </div>
        </div>
    </div>
</div> <!--end female part -->

<div class="modal fade" id="edit_male_pick" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="parent" style="display:none;"><span class="loader"></span></div>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Male Profile Pic</h4>
                <div id="profile_img" class="successMsg"> </div>
            </div>
            <div class="modal-body">            
                <form id="uploadimage_male" action="" method="post" enctype="multipart/form-data">
                    <div id="image_preview2"><img id="previewing_male" src="noimage.png" /></div>   
                    <div id="selectImage">
                        <label>Select Your Image</label><br/>
                        <input type="file" name="file" id="file_male" alt="file" onchange="showimagepreview1(this)"/>
                    </div>                   
                </form>     
            </div> 
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id ="update_male_pick">Save changes</button>
            </div>
        </div>
    </div>
</div> <!--male part end-->

<script>
jQuery("#update_female_pick").click(function () {
    var logged_in_user = jQuery("#logged_in_user").val();
    var srcimg = jQuery('#previewing').attr('src');
    intro_data = 'action=my_action_profile_img_female&srcimg=' + srcimg + '&logged_in_user=' + logged_in_user;

    jQuery(".parent").css("display", "block");
    jQuery.ajax({
        url:'<?php echo site_url(); ?>/wp-admin/admin-ajax.php', 
        data:intro_data,
        type:'POST',
        dataType:'json',
        success: function(response) {
            if(response.msg =='true') {
                jQuery("#msg_female").html(response.err);
                jQuery("#msg_female").show();
                setTimeout(function(){jQuery('#msg_female').fadeOut(2000);}, 3000);
                jQuery("#female_show_img").attr("src",response.cont);
            }else{
                jQuery("#msg_female").html(response.err);
                jQuery("#msg_female").show();
                setTimeout(function(){jQuery('#msg_female').fadeOut(2000);}, 3000);   
            }
            jQuery(".parent").css("display", "none");
        }
    });
});
// male pick change
jQuery("#update_male_pick").click(function () {
    var logged_in_user = jQuery("#logged_in_user").val();
    var srcimg = jQuery('#previewing_male').attr('src');
    intro_data = 'action=my_action_profile_img&srcimg=' + srcimg + '&logged_in_user=' + logged_in_user;

    jQuery(".parent").css("display", "block");
    jQuery.ajax({
    url:'<?php echo site_url(); ?>/wp-admin/admin-ajax.php', 
    data:intro_data,
    type:'POST',
    dataType:'json',
    success: function(response) {
            if(response.msg =='true') {
                jQuery("#profile_img").html(response.err);
                jQuery("#profile_img").show();
                setTimeout(function(){jQuery('#profile_img').fadeOut(2000);}, 3000);
                jQuery("#male_show_img").attr("src",response.cont);
            }else{
                jQuery("#profile_img").html(response.err);
                jQuery("#profile_img").show();
                setTimeout(function(){jQuery('#profile_img').fadeOut(2000);}, 3000);   
            }
            jQuery(".parent").css("display", "none");
        }
    });
});

</script>

<!--=========================* start Female And male edit profile pick *===================-->

<!--=========================* start edit basic section *===================-->
<div class="modal fade" id="edit_basic_info" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="parent" style="display:none;">
        <span class="loader"></span>
    </div>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="close_basic_info" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Basic Info</h4>
                <div id ="basicmsg" class="successMsg"></div>
            </div>
            <div class="modal-body">
                <div class="Profile-Part-BI cus_scroll3" id="hidePart1">
                    <h3 class="Proh-h3">BASIC INFO</h3>
                    <div class="row">
                        <p class="Pro-p col-sm-4 col-xs-offset-4">she</p>
                        <p class="Pro-p col-sm-4">he</p> 
                    </div>
                    <div class="padding-basic-info"> 
                        <div class="form-group border-bottom row">
                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Age</label>
                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center bold">
                                <input type="text" value="<?php echo $age1; ?>" name ="age_female" id ="her_dob" style="z-index:9999" disabled>
                            </label>
                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center bold">
                                <input type="text" value="<?php echo $age2; ?>" name ="age_male" id ="his_dob" disabled>
                            </label>
                        </div>
                        <div class="form-group border-bottom row">
                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Height (cm)</label>
                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center bold">     
                                <select name="shehight" class="form-control width60" id="height1">
                                    <option value ="">Select</option>
                                    <?php
                                    for ($h = 140; $h <= 210; $h++) {
                                        ?>
                                        <option value =<?php echo $h; ?> <?php if ($her_hight == $h) echo 'selected="selected"'; ?>><?php echo $h; ?> </option>
                                    <?php }
                                    ?>
                                </select>
                            </label>
                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center bold">
                                <select name="hehight" class="form-control width60" id="heighthe1">
                                    <option value ="">Select</option>
                                    <?php
                                    for ($a = 140; $a <= 210; $a++) {
                                        ?>
                                        <option value =<?php echo $a; ?> <?php if ($his_hight == $a) echo 'selected="selected"'; ?>><?php echo $a; ?> </option>
                                    <?php } ?>
                                </select>
                            </label>
                        </div>
                        <div class="form-group border-bottom row">
                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Figure</label>
                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center">
                                <select name="her_figure" class="form-control" id="her_figure">
                                    <option value ="">Select</option>
                                    <option value = "very slim" <?php if ($her_figure == "very slim") echo 'selected="selected"'; ?>>very slim</option>
                                    <option value = "slim" <?php if ($her_figure == "slim") echo 'selected="selected"'; ?>>slim</option>
                                    <option value = "normal" <?php if ($her_figure == "normal") echo 'selected="selected"'; ?>>normal</option>
                                    <option value = "slight overweight" <?php if ($her_figure == "slight overweight") echo 'selected="selected"'; ?>>slight overweight</option>
                                    <option value = "voluptuous" <?php if ($her_figure == "voluptuous") echo 'selected="selected"'; ?>>voluptuous</option>
                                    <option value = "plump" <?php if ($her_figure == "plump") echo 'selected="selected"'; ?>>plump</option>
                                    <option value = "fat" <?php if ($her_figure == "fat") echo 'selected="selected"'; ?>>fat</option>                                        
                                </select>
                            </label>
                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center">
                                <select name="his_figure" class="form-control" id="his_figure">
                                    <option value ="">Select</option>
                                    <option value = "very slim" <?php if ($his_figure == "very slim") echo 'selected="selected"'; ?>>very slim</option>
                                    <option value = "slim" <?php if ($his_figure == "slim") echo 'selected="selected"'; ?>>slim</option>
                                    <option value = "normal" <?php if ($his_figure == "normal") echo 'selected="selected"'; ?>>normal</option>
                                    <option value = "slight overweight" <?php if ($his_figure == "slight overweight") echo 'selected="selected"'; ?>>slight overweight</option>
                                    <option value = "voluptuous" <?php if ($his_figure == "voluptuous") echo 'selected="selected"'; ?>>voluptuous</option>
                                    <option value = "plump" <?php if ($his_figure == "plump") echo 'selected="selected"'; ?>>plump</option>
                                    <option value = "fat" <?php if ($his_figure == "fat") echo 'selected="selected"'; ?>>fat</option>
                                </select>
                            </label>
                        </div>
                        <div class="form-group border-bottom row">
                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Relation</label>
                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center">
                                <select <?php echo ($relation_ship_status == 1) ? "disabled" : ""; ?> name="relation_ship" class="form-control" id="relation_ship">
                                    <option  value ="">Select</option>
                                    <option <?php if ($realtionship == "married") echo 'selected="selected"'; ?> value="married">married</option>
                                    <option <?php if ($realtionship == "in relation") echo 'selected="selected"'; ?> value="in relation">in relation</option>
                                    <option <?php if ($realtionship == "single") echo 'selected="selected"'; ?> value="single">single</option>                                    
                                </select>
                            </label>                            
                        </div>
                        <div class="form-group border-bottom row">
                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Smoking</label>
                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center">
                                <select name="her_smoke" class="form-control" id="non_smokershe">
                                    <option value ="">Select</option>       
                                    <option value ="non smoker" <?php if ($her_smoke == "non smoker") echo 'selected="selected"'; ?>>non smoker</option>
                                    <option value="occasional smoker" <?php if ($her_smoke == "occasional smoker") echo 'selected="selected"'; ?>>occasional smoker</option>
                                    <option value="smoker" <?php if ($her_smoke == "smoker") echo 'selected="selected"'; ?>>smoker</option>
                                    <option value="heavy smoker" <?php if ($her_smoke == "heavy smoker") echo 'selected="selected"'; ?>>heavy smoker</option>
                                </select>
                            </label>
                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center">
                                <select name="her_smoke" class="form-control" id="non_smokerhe">
                                    <option value ="">Select</option>       
                                    <option value ="non smoker" <?php if ($his_smoke == "non smoker") echo 'selected="selected"'; ?>>non smoker</option>
                                    <option value="occasional smoker" <?php if ($his_smoke == "occasional smoker") echo 'selected="selected"'; ?>>occasional smoker</option>
                                    <option value="smoker" <?php if ($his_smoke == "smoker") echo 'selected="selected"'; ?>>smoker</option>
                                    <option value="heavy smoker" <?php if ($his_smoke == "heavy smoker") echo 'selected="selected"'; ?>>heavy smoker</option>
                                </select>
                            </label>
                        </div>
                        <div class="form-group border-bottom row">
                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Hair color</label>
                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center">
                                <select name="her_haircolor" class="form-control" id="her_haircolor">
                                        <option value ="">Select</option>
                                        <option <?php echo ($get_user_info['her_haircolor']['0'] == "blond") ? "selected" : ""; ?> value="blond">blond</option>
                                        <option <?php echo ($get_user_info['her_haircolor']['0'] == "red") ? "selected" : ""; ?> value="red">red</option>
                                        <option <?php echo ($get_user_info['her_haircolor']['0'] == "brown") ? "selected" : ""; ?> value="brown">brown</option>
                                        <option <?php echo ($get_user_info['her_haircolor']['0'] == "black") ? "selected" : ""; ?> value="black">black</option>
                                        <option <?php echo ($get_user_info['her_haircolor']['0'] == "grey") ? "selected" : ""; ?> value="grey">grey</option>
                                        <option <?php echo ($get_user_info['her_haircolor']['0'] == "bald") ? "selected" : ""; ?> value="bald">bald</option>
                                    </select>
                            </label>
                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center">
                                <select name="his_haircolor" class="form-control" id="his_haircolor">
                                        <option value ="">Select</option>
                                        <option <?php echo ($get_user_info['his_haircolor']['0'] == "blond") ? "selected" : ""; ?> value="blond">blond</option>
                                        <option <?php echo ($get_user_info['his_haircolor']['0'] == "red") ? "selected" : ""; ?> value="red">red</option>
                                        <option <?php echo ($get_user_info['his_haircolor']['0'] == "brown") ? "selected" : ""; ?> value="brown">brown</option>
                                        <option <?php echo ($get_user_info['his_haircolor']['0'] == "black") ? "selected" : ""; ?> value="black">black</option>
                                        <option <?php echo ($get_user_info['his_haircolor']['0'] == "grey") ? "selected" : ""; ?> value="grey">grey</option>
                                        <option <?php echo ($get_user_info['his_haircolor']['0'] == "bald") ? "selected" : ""; ?> value="bald">bald</option>
                                    </select>
                            </label>
                        </div>
                        <div class="form-group border-bottom row">
                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Shaved</label>
                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center">
                                <select name="her_shave" class="form-control" id="her_shave">
                                    <option value ="">Select</option>
                                    <option value ="Yes" <?php if ($her_shave == "yes") echo 'selected="selected"'; ?>>yes</option>
                                    <option value="No" <?php if ($her_shave == "no") echo 'selected="selected"'; ?>>no</option>
                                </select>
                            </label>
                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center">
                                <select name="his_shave" class="form-control" id="her_shave">
                                    <option value ="">Select</option>
                                    <option value ="Yes" <?php if ($his_shave == "yes") echo 'selected="selected"'; ?>>yes</option>
                                    <option value="No" <?php if ($his_shave == "no") echo 'selected="selected"'; ?>>no</option>
                                </select>
                            </label>
                        </div>
                        <div class="form-group border-bottom row">
                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Piercings / tattoos</label>
                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center">
                                <select name="her_tattoos" class="form-control" id="her_tattoos">
                                    <option value ="">Select</option>
                                    <option value ="Yes" <?php if ($her_tattoos == "yes") echo 'selected="selected"'; ?>>yes</option>
                                    <option value="No" <?php if ($her_tattoos == "no") echo 'selected="selected"'; ?>>no</option>
                                </select>
                            </label>
                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center">
                                <select name="his_tattoos" class="form-control" id="his_tattoos">
                                    <option value ="">Select</option>
                                    <option value ="Yes" <?php if ($his_tattoos == "yes") echo 'selected="selected"'; ?>>yes</option>
                                    <option value="No" <?php if ($his_tattoos == "no") echo 'selected="selected"'; ?>>no</option>
                                </select>
                            </label>
                        </div>
                        <?php $count = 1; $sum =1; for($i=0;$i<4;$i++){ ?>
                            <div class="form-group border-bottom row">
                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label"><?php echo $i + 1; ?>. Languages</label>
                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center">
                                    <select name="her_language[]" class="form-control" id="langshe<?php echo $count++ ; ?>">
                                        <option value ="">Select</option>       
                                        <option value ="English" <?php if ($language_array[$i] == "English") echo 'selected="selected"'; ?>>English</option>
                                        <option value ="German" <?php if ($language_array[$i] == "German") echo 'selected="selected"'; ?>>German</option> 
                                        <option value ="French" <?php if ($language_array[$i] == "French") echo 'selected="selected"'; ?>>French</option> 
                                        <option value ="Italian" <?php if ($language_array[$i] == "Italian") echo 'selected="selected"'; ?>>Italian</option> 
                                        <option value ="Spanish" <?php if ($language_array[$i] == "Spanish") echo 'selected="selected"'; ?>>Spanish</option> 
                                        <option value ="Dutch" <?php if ($language_array[$i] == "Dutch") echo 'selected="selected"'; ?>>Dutch</option>  
                                        <option value ="Albanian" <?php if ($language_array[$i] == "Albanian") echo 'selected="selected"'; ?>>Albanian</option>
                                        <option value ="Bosnian" <?php if ($language_array[$i] == "Bosnian") echo 'selected="selected"'; ?>>Bosnian</option>
                                        <option value ="Bulgarian" <?php if ($language_array[$i] == "Bulgarian") echo 'selected="selected"'; ?>>Bulgarian</option>
                                        <option value ="Croatian" <?php if ($language_array[$i] == "Croatian") echo 'selected="selected"'; ?>>Croatian</option>
                                        <option value ="Czech" <?php if ($language_array[$i] == "Czech") echo 'selected="selected"'; ?>>Czech</option>
                                        <option value ="Danish" <?php if ($language_array[$i] == "Danish") echo 'selected="selected"'; ?>>Danish</option>
                                        <option value ="Estonian" <?php if ($language_array[$i] == "Estonian") echo 'selected="selected"'; ?>>Estonian</option>
                                        <option value ="Finnish" <?php if ($language_array[$i] == "Finnish") echo 'selected="selected"'; ?>>Finnish</option>
                                        <option value ="Greek" <?php if ($language_array[$i] == "Greek") echo 'selected="selected"'; ?>>Greek</option>
                                        <option value ="Hungarian" <?php if ($language_array[$i] == "Hungarian") echo 'selected="selected"'; ?>>Hungarian</option>
                                        <option value ="Irish" <?php if ($language_array[$i] == "Irish") echo 'selected="selected"'; ?>>Irish</option>
                                        <option value ="Latvian" <?php if ($language_array[$i] == "Latvian") echo 'selected="selected"'; ?>>Latvian</option>
                                        <option value ="Lithuanian" <?php if ($language_array[$i] == "Lithuanian") echo 'selected="selected"'; ?>>Lithuanian</option>
                                        <option value ="Macedonian" <?php if ($language_array[$i] == "Macedonian") echo 'selected="selected"'; ?>>Macedonian</option>
                                        <option value ="Norwegian" <?php if ($language_array[$i] == "Norwegian") echo 'selected="selected"'; ?>>Norwegian</option>
                                        <option value ="Polish" <?php if ($language_array[$i] == "Polish") echo 'selected="selected"'; ?>>Polish</option>
                                        <option value ="Portuguese" <?php if ($language_array[$i] == "Portuguese") echo 'selected="selected"'; ?>>Portuguese</option>
                                        <option value ="Romanian" <?php if ($language_array[$i] == "Romanian") echo 'selected="selected"'; ?>>Romanian</option>
                                        <option value ="Russian" <?php if ($language_array[$i] == "Russian") echo 'selected="selected"'; ?>>Russian</option>
                                        <option value ="Serbian" <?php if ($language_array[$i] == "Serbian") echo 'selected="selected"'; ?>>Serbian</option>
                                        <option value ="Slovak" <?php if ($language_array[$i] == "Slovak") echo 'selected="selected"'; ?>>Slovak</option>
                                        <option value ="Slovene" <?php if ($language_array[$i] == "Slovene") echo 'selected="selected"'; ?>>Slovene</option>
                                        <option value ="Swedish" <?php if ($language_array[$i] == "Swedish") echo 'selected="selected"'; ?>>Swedish</option>
                                        <option value ="Turkish" <?php if ($language_array[$i] == "Turkish") echo 'selected="selected"'; ?>>Turkish</option>
                                        <option value ="Ukrainian" <?php if ($language_array[$i] == "Ukrainian") echo 'selected="selected"'; ?>>Ukrainian</option>
                                    </select>
                                </label>
                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center">
                                    <select name="his_language[]" class="form-control" id="langhe<?php echo $sum++ ; ?>">
                                        <option value ="">Select</option>       
                                        <option value ="English" <?php if ($language_array1[$i] == "English") echo 'selected="selected"'; ?>>English</option>
                                        <option value ="German" <?php if ($language_array1[$i] == "German") echo 'selected="selected"'; ?>>German</option> 
                                        <option value ="French" <?php if ($language_array1[$i] == "French") echo 'selected="selected"'; ?>>French</option> 
                                        <option value ="Italian" <?php if ($language_array1[$i] == "Italian") echo 'selected="selected"'; ?>>Italian</option> 
                                        <option value ="Spanish" <?php if ($language_array1[$i] == "Spanish") echo 'selected="selected"'; ?>>Spanish</option> 
                                        <option value ="Dutch" <?php if ($language_array1[$i] == "Dutch") echo 'selected="selected"'; ?>>Dutch</option>  
                                        <option value ="Albanian" <?php if ($language_array1[$i] == "Albanian") echo 'selected="selected"'; ?>>Albanian</option>
                                        <option value ="Bosnian" <?php if ($language_array1[$i] == "Bosnian") echo 'selected="selected"'; ?>>Bosnian</option>
                                        <option value ="Bulgarian" <?php if ($language_array1[$i] == "Bulgarian") echo 'selected="selected"'; ?>>Bulgarian</option>
                                        <option value ="Croatian" <?php if ($language_array1[$i] == "Croatian") echo 'selected="selected"'; ?>>Croatian</option>
                                        <option value ="Czech" <?php if ($language_array1[$i] == "Czech") echo 'selected="selected"'; ?>>Czech</option>
                                        <option value ="Danish" <?php if ($language_array1[$i] == "Danish") echo 'selected="selected"'; ?>>Danish</option>
                                        <option value ="Estonian" <?php if ($language_array1[$i] == "Estonian") echo 'selected="selected"'; ?>>Estonian</option>
                                        <option value ="Finnish" <?php if ($language_array1[$i] == "Finnish") echo 'selected="selected"'; ?>>Finnish</option>
                                        <option value ="Greek" <?php if ($language_array1[$i] == "Greek") echo 'selected="selected"'; ?>>Greek</option>
                                        <option value ="Hungarian" <?php if ($language_array1[$i] == "Hungarian") echo 'selected="selected"'; ?>>Hungarian</option>
                                        <option value ="Irish" <?php if ($language_array1[$i] == "Irish") echo 'selected="selected"'; ?>>Irish</option>
                                        <option value ="Latvian" <?php if ($language_array1[$i] == "Latvian") echo 'selected="selected"'; ?>>Latvian</option>
                                        <option value ="Lithuanian" <?php if ($language_array1[$i] == "Lithuanian") echo 'selected="selected"'; ?>>Lithuanian</option>
                                        <option value ="Macedonian" <?php if ($language_array1[$i] == "Macedonian") echo 'selected="selected"'; ?>>Macedonian</option>
                                        <option value ="Norwegian" <?php if ($language_array1[$i] == "Norwegian") echo 'selected="selected"'; ?>>Norwegian</option>
                                        <option value ="Polish" <?php if ($language_array1[$i] == "Polish") echo 'selected="selected"'; ?>>Polish</option>
                                        <option value ="Portuguese" <?php if ($language_array1[$i] == "Portuguese") echo 'selected="selected"'; ?>>Portuguese</option>
                                        <option value ="Romanian" <?php if ($language_array1[$i] == "Romanian") echo 'selected="selected"'; ?>>Romanian</option>
                                        <option value ="Russian" <?php if ($language_array1[$i] == "Russian") echo 'selected="selected"'; ?>>Russian</option>
                                        <option value ="Serbian" <?php if ($language_array1[$i] == "Serbian") echo 'selected="selected"'; ?>>Serbian</option>
                                        <option value ="Slovak" <?php if ($language_array1[$i] == "Slovak") echo 'selected="selected"'; ?>>Slovak</option>
                                        <option value ="Slovene" <?php if ($language_array1[$i] == "Slovene") echo 'selected="selected"'; ?>>Slovene</option>
                                        <option value ="Swedish" <?php if ($language_array1[$i] == "Swedish") echo 'selected="selected"'; ?>>Swedish</option>
                                        <option value ="Turkish" <?php if ($language_array1[$i] == "Turkish") echo 'selected="selected"'; ?>>Turkish</option>
                                        <option value ="Ukrainian" <?php if ($language_array1[$i] == "Ukrainian") echo 'selected="selected"'; ?>>Ukrainian</option>
                                    </select>
                                </label>
                            </div>
                        <?php } ?>
                        <div class="form-group border-bottom row">
                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Education</label>
                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center">  
                                <select name="her_graduation" class="form-control" id="graduationshe1">
                                        <option value ="">Select</option>       
                                        <option <?php echo ($get_user_info['her_graduation']['0'] == "primary school") ? "selected" : ""; ?> value ="primary school">primary school</option>
                                        <option <?php echo ($get_user_info['her_graduation']['0'] == "middle school") ? "selected" : ""; ?> value="middle school">middle school</option>
                                        <option <?php echo ($get_user_info['her_graduation']['0'] == "high school") ? "selected" : ""; ?> value="high school">high school</option>
                                        <option <?php echo ($get_user_info['her_graduation']['0'] == "college") ? "selected" : ""; ?> value="college">college</option>
                                        <option <?php echo ($get_user_info['her_graduation']['0'] == "university") ? "selected" : ""; ?> value="university">university</option>
                                        <option <?php echo ($get_user_info['her_graduation']['0'] == "masters") ? "selected" : ""; ?> value="masters">masters</option>
                                        <option <?php echo ($get_user_info['her_graduation']['0'] == "doctorate") ? "selected" : ""; ?> value="doctorate">doctorate</option>                                        
                                </select>
                            </label>
                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center">  
                                <select name="his_graduation" class="form-control" id="graduationhe1">
                                    <option value ="">Select</option>       
                                    <option <?php echo ($get_user_info['his_graduation']['0'] == "primary school") ? "selected" : ""; ?> value="primary school">primary school</option>
                                    <option <?php echo ($get_user_info['his_graduation']['0'] == "middle school") ? "selected" : ""; ?> value="middle school">middle school</option>
                                    <option <?php echo ($get_user_info['his_graduation']['0'] == "high school") ? "selected" : ""; ?> value="high school">high school</option>
                                    <option <?php echo ($get_user_info['his_graduation']['0'] == "college") ? "selected" : ""; ?> value="college">college</option>
                                    <option <?php echo ($get_user_info['his_graduation']['0'] == "university") ? "selected" : ""; ?> value="university">university</option>
                                    <option <?php echo ($get_user_info['his_graduation']['0'] == "masters") ? "selected" : ""; ?> value="masters">masters</option>
                                    <option <?php echo ($get_user_info['his_graduation']['0'] == "doctorate") ? "selected" : ""; ?> value="doctorate">doctorate</option>
                                </select>
                            </label>
                        </div>                      
                        
                        
                        <div class="form-group border-bottom border0 row">
                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Hobbies & Interests</label>
                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center">
                                <textarea maxlength="150" name="her_hobby" id ="her_hobby"><?php echo $her_hobby; ?></textarea>
                            </label>
                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center">
                                <textarea maxlength="150" name="his_hobby" id ="his_hobby"><?php echo $his_hobby; ?></textarea>
                            </label>
                        </div>
                        <div class="form-group border-bottom border0 row">
                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Choose your basic info about you</label>
                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center">
                                <textarea maxlength="150" name="about_her" id ="about_her"><?php echo $get_user_info['about_her']['0']; ?></textarea>
                            </label>
                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center">
                                <textarea maxlength="150" name="about_his" id ="about_his"><?php echo $get_user_info['about_his']['0']; ?></textarea>
                            </label>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="hide_msgbody" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id ="update_basic_info">Save changes</button>
            </div>
        </div>
    </div>
</div>
<script>
jQuery("#update_basic_info").click(function () {
    var logged_in_user = $("#logged_in_user").val();
    var she_dob     = $("#her_dob").val();
    var he_dob      = $("#his_dob").val();

    var her_hight   = $("#height1").val();
    var his_hight   = $("#heighthe1").val();

    var her_figure   = $("#her_figure").val();
    var his_figure   = $("#his_figure").val();

    var her_smoke   = $("#non_smokershe").val();
    var his_smoke   = $("#non_smokerhe").val();
    var her_shave   = $("#her_shave").val();
    var his_shave   = $("#his_shave").val();
    var her_tattoos = $("#her_tattoos").val();
    var his_tattoos = $("#his_tattoos").val();

    var her_language    = $("#langshe1").val();
    var her_language1   = $("#langshe2").val();
    var her_language2   = $("#langshe3").val();
    var her_language3   = $("#langshe4").val();

    var his_language    = $("#langhe1").val();
    var his_language1   = $("#langhe2").val();
    var his_language2   = $("#langhe3").val();
    var his_language3   = $("#langhe4").val();

    var her_graduation  = $("#graduationshe1").val();
    var his_graduation  = $("#graduationhe1").val();

    var her_profession  = $("#prffessionshe1").val();
    var his_profession  = $("#prffessionhe1").val();
    var her_haircolor   = $("#her_haircolor").val();
    var his_haircolor   = $("#his_haircolor").val();

    var about_her = $("#about_her").val();
    var about_his = $("#about_his").val();
    var relation_ship = $('select#relation_ship option:selected').val();        
    if(relation_ship == "" || relation_ship == "<?php echo $realtionship; ?>"){
        var relation_ship_status = 0;
    } else{
        var relation_ship_status = 1;
    }   

    var her_hobby = $("#her_hobby").val();
    var his_hobby = $("#his_hobby").val();

    intro_data = 'action=my_action_update_basic_info_both&she_dob=' + she_dob + '&logged_in_user=' +logged_in_user + '&he_dob=' + he_dob + '&her_hight=' + her_hight +'&his_hight=' + his_hight + '&her_figure=' + her_figure + '&his_figure=' +his_figure + '&her_smoke=' + her_smoke + '&his_smoke=' + his_smoke +'&her_shave=' + her_shave + '&his_shave=' + his_shave + '&her_tattoos=' +her_tattoos + '&his_tattoos=' + his_tattoos + '&her_language=' + her_language+ '&her_language1=' + her_language1 + '&her_language2=' + her_language2 +'&her_language3=' + her_language3 + '&his_language=' + his_language +'&his_language1=' + his_language1 + '&his_language2=' + his_language2 +'&his_language3=' + his_language3 + '&her_graduation=' + her_graduation +'&his_graduation=' + his_graduation + '&her_profession=' + her_profession +'&his_profession=' + his_profession + '&relation_ship=' + relation_ship + '&relation_ship_status='+relation_ship_status+'&her_hobby=' + her_hobby + '&his_hobby=' + his_hobby + '&her_haircolor=' +her_haircolor + '&his_haircolor=' + his_haircolor + '&about_her=' + about_her+ '&about_his=' + about_his + '&type=couple';    

    jQuery(".parent").css("display", "block");
    jQuery.ajax({
        url:'<?php echo site_url(); ?>/wp-admin/admin-ajax.php', 
        data:intro_data,
        type:'POST',
        dataType:'json',
        success: function(response) {
            if(response.msg =='true') {
                jQuery("#basicmsg").html('Your basic information data has been saved.');
                jQuery("#basicmsg").show();
                setTimeout(function(){jQuery('#basicmsg').fadeOut(2000);}, 3000);                
                jQuery("#show_basic_info").html(response.cont);
                
            }else{
                jQuery("#basicmsg").html('Your basic information data not saved.'); 
                jQuery("#basicmsg").show();
                setTimeout(function(){jQuery('#basicmsg').fadeOut(2000);}, 3000);  
            }
            window.location = self.location;
            jQuery(".parent").css("display", "none");
        }
    });
});
</script>

<!--================ +End Edit basic info +=====================-->

<!--=========== * Start Main Residence Model *=======================-->
<div class="modal fade" id="edit_main_residence" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="parent" style="display:none;">
        <span class="loader"></span>
    </div>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="close_residence_msg" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Main Residence</h4>
                <div id ="country_suc" class="successMsg"></div>
            </div>
            <div class="modal-body">                
                <div class="Profile-Part-BI cus_scroll3">
                    <div class="padding-basic-info">                        
                        <div class="form-group border-bottom row">
                            <label class="col-sm-4 BasicInfo-label" >COUNTRY:</label>
                            <label class="col-sm-4 BasicInfo-label para-center bold"><select id="country" name ="country[]" class="required form-control"></select></label>
                        </div>
                        <div class="form-group border-bottom row">
                            <label class="col-sm-4 BasicInfo-label" for="inputEmail3">REGIO:</label>
                            <label class="col-sm-4 BasicInfo-label para-center bold">
                            <select name ="state[]" id ="state" class="required form-control"></select>
                            </label>
                        </div>
                        <div class="form-group border-bottom row">
                            <script language="javascript">
                                populateCountries("country", "state", "", "");
                            </script>                                                                                                                           <label class="col-sm-4 BasicInfo-label"> NEXT CITY:</label>
                            <label class="col-sm-4 BasicInfo-label para-center bold">
                                <input type ="text" name="city_name" id="city_name" class="form-control"  required="" value="<?php echo $city1;  ?>" >
                            </label>
                        </div>
                        <div class="form-group border-bottom row" style="border-bottom:0 none!important;">
                            <label class="col-sm-4 BasicInfo-label">Frequently in:</label>
                            <label class="col-sm-4 BasicInfo-label para-center bold">                           
                                <textarea name="frequently_in" id="frequently_in"><?php echo $get_user_info['frequently_in']['0']; ?></textarea>
                                <input type="hidden" name="selected_country" id="selected_country" value="<?php echo $main_country; ?>">
                                <input type="hidden" name="selected_region" id="selected_region" value="<?php echo $main_district; ?>">
                                <input type="hidden" name="selected_city" id="selected_city" value="<?php echo $main_city; ?>">
                            </label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="hide_content" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id ="update_main_address">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
jQuery("#update_main_address").click(function () {
    var logged_in_user  = $("#logged_in_user").val();
    var country         = $("#country").val();
    var state           = $("#state").val();
    var city            = $("#city_name").val();
    var selected_country = $("#selected_country").val();
    var selected_region  = $("#selected_region").val();
    var selected_city    = $("#selected_city").val();
    var first_resi       = $("#first_add").val();
    var frequently_in    = $("#frequently_in").val();
   
    intro_data = 'action=my_action_update_main_residence&country=' + country + '&logged_in_user=' + logged_in_user + '&state=' + state + '&city=' + city + '&selected_country=' + selected_country + '&selected_region=' + selected_region + '&selected_city=' + selected_city + '&first_resi=' + first_resi + '&frequently_in=' + frequently_in;

    jQuery(".parent").css("display", "block");
    jQuery.ajax({
        url:'<?php echo site_url(); ?>/wp-admin/admin-ajax.php', 
        data:intro_data,
        type:'POST',
        dataType:'json',
        success: function(response) {
            if(response.msg =='true') {
                jQuery("#country_suc").html('Your residence data has been saved.');
                jQuery("#country_suc").show();
                setTimeout(function(){jQuery('#country_suc').fadeOut(2000);}, 3000);
                jQuery("#show_main_residence").html(response.cont);
            }else{
                jQuery("#country_suc").html(response.err);
                jQuery("#country_suc").show();
                setTimeout(function(){jQuery('#country_suc').fadeOut(2000);}, 3000);   
            }
            jQuery(".parent").css("display", "none");
        }
    });
});

</script>

<!--=========== * End Main Residence Model *=======================-->

<!--=========== * Start Second Residence Model *=======================-->
<div class="modal fade" id="edit_second_residence" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="parent" style="display:none;">
        <span class="loader"></span>
    </div>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="close_secondResi_msg" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">SECOND RESIDENCE</h4>
                <div id="second_residence" class="successMsg" ></div>
            </div>
            <div class="modal-body">
                <div class="Profile-Part-BI cus_scroll3">
                    <div class="padding-basic-info">
                        
                    </div>
                </div>
                <div class="clear"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id ="update_second_address">Save changes</button>
            </div>
        </div>
    </div>
</div>
<script>
jQuery("#update_second_address").click(function () {
    var logged_in_user      = $("#logged_in_user").val();
    var second_city_name    = $("#second_city_name").val();
    var second_country      = $("#country_second").val();
    var state_second        = $("#state_second").val();

    intro_data = 'action=my_action_second_res&second_city_name=' + second_city_name + '&logged_in_user=' + logged_in_user + '&second_country=' + second_country + '&state_second=' + state_second;

    jQuery(".parent").css("display", "block");
    jQuery.ajax({
        url:'<?php echo site_url(); ?>/wp-admin/admin-ajax.php', 
        data:intro_data,
        type:'POST',
        dataType:'json',
        success: function(response) {
            if(response.msg =='true') {
                jQuery("#second_res").html(response.err);
                jQuery("#second_res").show();
                setTimeout(function(){jQuery('#second_res').fadeOut(2000);}, 3000);
                jQuery("#show_second_residence").html(response.cont);
            }else{
                jQuery("#second_res").html(response.err);
                jQuery("#second_res").show();
                setTimeout(function(){jQuery('#second_res').fadeOut(2000);}, 3000);   
            }
            jQuery(".parent").css("display", "none");
        }
    });
});
</script>

<!--=========== * End Second Residence Model *=======================-->

<!--=========================* start fav queto section *===================-->
<div class="modal fade" id="fav_quote" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="parent" style="display:none;">
        <span class="loader"></span>
    </div>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="close_quoto_msg" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">OUR FAVOURITE QUOTE</h4>
                <div id ="fav_quote_msg" class="successMsg"></div>
            </div>
            <div class="modal-body">
                <div class="editBoth">
                    <textarea id='editor4'><?php echo $fav_quote_content; ?></textarea>				
                    <script>CKEDITOR.replace('editor4',{enterMode: CKEDITOR.ENTER_BR,language: 'en',entities: false});</script>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="close_quoto" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id ="update_fav_section">Save changes</button>
            </div>
        </div>
    </div>
</div>
<script>
jQuery("#update_fav_section").click(function () {
    var quote_content   = CKEDITOR.instances['editor4'].getData();
    var logged_in_user  = $("#logged_in_user").val();

    intro_data = 'action=my_action_favourite&quote_content=' + quote_content + '&logged_in_user=' + logged_in_user;
    jQuery(".parent").css("display", "block");
    jQuery.ajax({
        url:'<?php echo site_url(); ?>/wp-admin/admin-ajax.php', 
        data:intro_data,
        type:'POST',
        dataType:'json',
        success: function(response) {
            if(response.msg =='true') {
                jQuery("#fav_quote_msg").html(response.err);
                jQuery("#fav_quote_msg").show();
                setTimeout(function(){jQuery('#fav_quote_msg').fadeOut(2000);}, 3000);
                jQuery("#fav-para").html(response.cont);
            }else{
                jQuery("#fav_quote_msg").html(response.err);
                jQuery("#fav_quote_msg").show();
                setTimeout(function(){jQuery('#fav_quote_msg').fadeOut(2000);}, 3000);   
            }
            jQuery(".parent").css("display", "none");
        }
    });
});

</script>

<!--=========================* End fav queto section *===================-->

<!--=========================* Start Prefect match section *===================-->
<div class="modal fade" id="edit_perfect_match" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="parent" style="display:none;">
        <span class="loader"></span>
    </div>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="show_she_successMsg1" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Prefect Match</h4>
                <div id="match_show" class="successMsg"></div>
            </div>
            <div class="modal-body" >             
                <div class="editShe">
                    <h2>She</h2>
                    <textarea id='editor5'><?php echo $reg3_txtshe; ?></textarea>				
                    <script>CKEDITOR.replace('editor5',{enterMode: CKEDITOR.ENTER_BR,language: 'en',entities: false});</script>
                    <input type="hidden" name="her_match_content" id="her_match_content" value="to_her">
                    <div class="modal-footer">
                        <button type="button" id="show_she_successMsg" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id ="update_her_perfect">Save changes</button>
                    </div>
                </div>
                <div class="editHe">
                    <h2>He</h2>
                    <textarea id='editor6'><?php echo $reg3_txthe; ?></textarea>				
                    <script>CKEDITOR.replace('editor6',{language: 'en',entities: false});</script>
                    <input type="hidden" name="his_match_content" id="his_match_content" value="to_his">	
                    <div class="modal-footer">
                        <button type="button" id="hide_hisdetails" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id ="update_his_perfect">Save changes</button>
                    </div>
                </div>
                <div class="editBoth">                  
                    <h2>Both</h2>					
                    <textarea id='editor7'><?php echo $reg3_txtboth; ?></textarea>				
                    <script>CKEDITOR.replace('editor7',{language: 'en',entities: false});</script>				
                    <input type="hidden" name="both_match_content" id="both_match_content" value="to_both">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="hide_his_details" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id ="update_both_section">Save changes</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
jQuery('.deletematch').click(function()
{
var fieldname = jQuery(this).attr('type');
var hidDiv = jQuery(this).attr('hideID');
var intro_data = 'action=delete_perfect_matches&type='+ fieldname;
    jQuery(".parent").css("display", "block");
    jQuery.ajax
    ({
        url:'<?php echo site_url(); ?>/wp-admin/admin-ajax.php', 
        data:intro_data,
        type:'POST',
        success: function(response) 
        {
            jQuery('div#'+hidDiv).hide(); 
            window.location = self.location;          
        }
    });
});    
</script>
<script>
// for the her case
jQuery("#update_her_perfect").click(function () {
    var logged_in_user = $("#logged_in_user").val();
    var editor5 = CKEDITOR.instances['editor5'].getData();
    var to_whome = $("#her_match_content").val();
    intro_data = 'action=my_action_prefect_match&editor5='+ editor5 + '&logged_in_user=' + logged_in_user + '&to_whome=' + to_whome;

    jQuery(".parent").css("display", "block");
    jQuery.ajax({
        url:'<?php echo site_url(); ?>/wp-admin/admin-ajax.php', 
        data:intro_data,
        type:'POST',
        dataType:'json',
        success: function(response) {
            if(response.msg =='true') {
                jQuery("#match_show").html(response.err);
                jQuery("#match_show").show();
                setTimeout(function(){jQuery('#match_show').fadeOut(2000);}, 3000);
                jQuery("#show_her").show();
                jQuery(".addladies").hide();
                jQuery("#show_her").html(response.cont);
            }else{
                jQuery("#match_show").html(response.err);
                jQuery("#match_show").show();
                setTimeout(function(){jQuery('#match_show').fadeOut(2000);}, 3000);   
            }
            jQuery(".parent").css("display", "none");
            window.location = self.location;
        }
    });
});

// for the his case
jQuery("#update_his_perfect").click(function () {
    var logged_in_user = $("#logged_in_user").val();
    var editor6 = CKEDITOR.instances['editor6'].getData();
    var to_whome = $("#his_match_content").val();
    intro_data = 'action=my_action_prefect_match&editor6='+ editor6 + '&logged_in_user=' + logged_in_user + '&to_whome=' + to_whome;

    jQuery(".parent").css("display", "block");
    jQuery.ajax({
        url:'<?php echo site_url(); ?>/wp-admin/admin-ajax.php', 
        data:intro_data,
        type:'POST',
        dataType:'json',
        success: function(response) {
            if(response.msg =='true') {
                jQuery("#match_show").html(response.err);
                jQuery("#match_show").show();
                setTimeout(function(){jQuery('#match_show').fadeOut(2000);}, 3000);
                jQuery("#show_his").show();
                jQuery("#show_his").html(response.cont);
                jQuery(".addmen").hide();
            }else{
                jQuery("#match_show").html(response.err);
                jQuery("#match_show").show();
                setTimeout(function(){jQuery('#match_show').fadeOut(2000);}, 3000);   
            }
            jQuery(".parent").css("display", "none");
            window.location = self.location;
        }
    });
});

// for both case
jQuery("#update_both_section").click(function () {
    var logged_in_user = $("#logged_in_user").val();
    var editor7 = CKEDITOR.instances['editor7'].getData();
    var to_whome = $("#both_match_content").val();

    intro_data = 'action=my_action_prefect_match&editor7='+ editor7 + '&logged_in_user=' + logged_in_user + '&to_whome=' + to_whome;

    jQuery(".parent").css("display", "block");
    jQuery.ajax({
        url:'<?php echo site_url(); ?>/wp-admin/admin-ajax.php', 
        data:intro_data,
        type:'POST',
        dataType:'json',
        success: function(response) {
            if(response.msg =='true') {
                jQuery("#match_show").html(response.err);
                jQuery("#match_show").show();
                setTimeout(function(){jQuery('#match_show').fadeOut(2000);}, 3000);
                jQuery("#show_both").show();
                jQuery("#show_both").html(response.cont);
                jQuery(".addcouple").hide();
            }else{
                jQuery("#match_show").html(response.err);
                jQuery("#match_show").show();
                setTimeout(function(){jQuery('#match_show').fadeOut(2000);}, 3000);   
            }
            jQuery(".parent").css("display", "none");
            window.location = self.location;
        }
    });
});

</script>

<!--=========================* Start Prefect match section *===================-->

<!--=========================* Start speed dating section *===================-->
<div class="modal fade" id="speed_dating" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
     <div class="parent" style="display:none;">
        <span class="loader"></span>
    </div>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="close_showMsg" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Speed Dating</h4>
                <div id ="speed_msg" class="successMsg"></div>
            </div>
            <div class="modal-body">                
                <div class="editBoth">
                    <textarea id='editor8'><?php echo $get_user_info['speed_data']['0']; ?></textarea>				
                    <script>CKEDITOR.replace('editor8',{language: 'en',entities: false});</script>
                    <input type="hidden" name="speed_dating" id="speed_date" value ="speed_date">
                    <div class="modal-footer">
                        <button type="button" id="close_update_speed" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id ="update_speed_dating">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
jQuery("#update_speed_dating").click(function () {
    var editor8 = CKEDITOR.instances['editor8'].getData();
    var logged_in_user = $("#logged_in_user").val();
    var text_box_round = $("#speed_date").val();

    intro_data = 'action=my_action_speed_travel&editor8=' + editor8 + '&logged_in_user=' + logged_in_user + '&text_box_round=' + text_box_round;
    jQuery(".parent").css("display", "block");
    jQuery.ajax({
        url:'<?php echo site_url(); ?>/wp-admin/admin-ajax.php', 
        data:intro_data,
        type:'POST',
        dataType:'json',
        success: function(response) {
            if(response.msg =='true') {
                jQuery("#speed_msg").html(response.err);
                jQuery("#speed_msg").show();
                setTimeout(function(){jQuery('#speed_msg').fadeOut(2000);}, 3000);
                jQuery("#show_speed_date").next('p').remove();
                jQuery("#show_speed_date").html(response.cont);
            }else{
                jQuery("#speed_msg").html(response.err);
                jQuery("#speed_msg").show();
                setTimeout(function(){jQuery('#speed_msg').fadeOut(2000);}, 3000);   
            }
            jQuery(".parent").css("display", "none");
        }
    });
});
</script>
<!--===============+ end speed dating +===============-->

<!--===============+ Start travel agenda +===============-->
<div class="modal fade" id="travel_agenda" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="parent" style="display:none;">
        <span class="loader"></span>
    </div>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="close_travelMsg" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Travel agenda</h4>
                <div id ="travel_msg" class="successMsg"></div>
            </div>
            <div class="modal-body">                
                <div class="editBoth">
                    <textarea id='editor9'><?php echo $get_user_info['travel_agent']['0']; ?></textarea>				
                    <script>CKEDITOR.replace('editor9',{language: 'en',entities: false});</script>
                    <input type="hidden" name="travel_agend" id="travel_agend12" value ="travel_agend">
                </div>
                <div class="modal-footer">
                    <button type="button" id="hideMsg" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id ="update_travel_agenda">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
jQuery("#update_travel_agenda").click(function () {
    var editor9 = CKEDITOR.instances['editor9'].getData();
    var logged_in_user = $("#logged_in_user").val();
    var text_box_round = $("#travel_agend12").val();
    intro_data = 'action=my_action_speed_travel&editor9=' + editor9 + '&logged_in_user=' + logged_in_user + '&text_box_round=' + text_box_round;
    jQuery(".parent").css("display", "block");
    jQuery.ajax({
        url:'<?php echo site_url(); ?>/wp-admin/admin-ajax.php', 
        data:intro_data,
        type:'POST',
        dataType:'json',
        success: function(response) {
            if(response.msg =='true') {
                jQuery("#travel_msg").html(response.err);
                jQuery("#travel_msg").show();
                setTimeout(function(){jQuery('#travel_msg').fadeOut(2000);}, 3000);
                jQuery("#show_travel_date").next('p').remove();
                jQuery("#show_travel_date").html(response.cont);
            }else{
                jQuery("#travel_msg").html(response.err);
                jQuery("#travel_msg").show();
                setTimeout(function(){jQuery('#travel_msg').fadeOut(2000);}, 3000);   
            }
            jQuery(".parent").css("display", "none");
        }
    });
});
</script>

<!--====================+ end travel agenda +===========================-->

<!--=========================* start Basic introduction *===================-->
<div class="modal fade" id="edit_intro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="parent" style="display:none;">
        <span class="loader"></span>
    </div>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Form Preview</h4>
                <div id="intro_msg" class="successMsg"></div>
            </div>
            <div class="modal-body">                
                <div class="editBoth">
                    <textarea id='editor3' ><?php echo $describe; ?></textarea>             
                    <script>CKEDITOR.replace('editor3',{enterMode: CKEDITOR.ENTER_BR,language: 'en',entities: false});</script>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="hide_msg" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id ="update_profile_intro">Save changes</button>
            </div>
        </div>
    </div>
</div>
<script>
jQuery('p').keypress(function(){
alert('sdfmlk');
});
    
</script>
<script>
jQuery("#update_profile_intro").click(function () {
    var profile_intro = CKEDITOR.instances['editor3'].getData();
    var logged_in_user = $("#logged_in_user").val();
    intro_data = 'action=my_action_intro&profile_intro=' + profile_intro + '&logged_in_user=' + logged_in_user;
    jQuery(".parent").css("display", "block");
    jQuery.ajax({
        url:'<?php echo site_url(); ?>/wp-admin/admin-ajax.php', 
        data:intro_data,
        type:'POST',
        dataType:'json',
        success: function(response) {
            if(response.msg =='true') {
                jQuery("#intro_msg").html('Your Introduction part has been successfully updated');
                jQuery("#intro_msg").show();
                setTimeout(function(){jQuery('#intro_msg').fadeOut(2000);}, 3000);
                jQuery("#intro_data").html(response.cont);
            }else{
                jQuery("#intro_msg").html(response.err);
                jQuery("#intro_msg").show();
                setTimeout(function(){jQuery('#intro_msg').fadeOut(2000);}, 3000);   
            }
            jQuery(".parent").css("display", "none");
        }
    });
});
</script>

<!--=========================* End Basic introduction *===================-->

<!--===============+ Change image background +===============-->
<?php
    $results = $wpdb->get_results("SELECT * FROM bg_images");
    $upload_dir = wp_upload_dir();
    $imgPath = $upload_dir['baseurl'] . '/background_images' . '/';
?>
<div class="modal fade" id="change_background" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="parent" style="display:none;">
        <span class="loader"></span>
    </div>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Background Image</h4>
                <div id ="image_msg" class="successMsg"></div> 
            </div>
            <div class="modal-body">
                <div class="col-sm-12">
                    <div id ="country_suc" class="msg"></div>                           
                    <div class="form-group">
                        <?php foreach ($posts_array as $value) { ?> 
                            <a href="javascript:void(0)" class="bg_image" img_id = "<?php echo $value->ID; ?>">
                               <?php echo get_the_post_thumbnail($value->ID); ?></a>
                        <?php } ?> 
                    </div>                        
                </div>
                <div class="clear"></div>
            </div>
            <div class="modal-footer">
                <button type="button" id="hide_image_msg" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
jQuery(document).ready(function () {
    jQuery('a.bg_image').click(function () {
        var img = $(this).attr('img_id');
        jQuery(".parent").css("display", "block");
        jQuery.ajax({
            url:'<?php echo site_url(); ?>/wp-admin/admin-ajax.php', 
            data:'action=my_action_change_bg&img=' + img,
            type:'POST',
            dataType:'json',
            success: function(response) {
                if(response.msg =='true') {
                    jQuery("#image_msg").html('Your Background image are saved.');
                    jQuery("#image_msg").show();
                    setTimeout(function(){jQuery('#image_msg').fadeOut(2000);}, 3000);
                    var img = response.cont;
                    jQuery('#introduction_part').css('background-image', 'url("' + img + '")');
                }else{
                    jQuery("#image_msg").html(response.err);
                    jQuery("#image_msg").show();
                    setTimeout(function(){jQuery('#image_msg').fadeOut(2000);}, 3000);   
                }
                jQuery(".parent").css("display", "none");
            }
        });
    });
});
</script>
<!--===============+ Change image background +===============-->
<?php
    } else 
    {
		
        ?>
    <div class="text-center succesPoint">
        <h4>You must be loggedin to access this page</h4>
    </div>
    <?php
}
?>

<script>
(function ($) {
    $.noConflict();
        $(window).load(function () {
			
			
            $(".cus_scroll").mCustomScrollbar({
                theme: "light-2"
            });
            $(".cus_scrolln").mCustomScrollbar({
                theme: "light-2"
            });

            $("#content-1").mCustomScrollbar({   
                theme: "light-2"
            });
            $("#content-2").mCustomScrollbar({   
                theme: "light-2"
            });
        });
    })(jQuery);
</script>    
<?php get_footer(); ?>
