<?php
/*Template Name:Swiss Portal for Premimum Member
 * 
 * 
 * 
*/
get_header();

?> 

            <section class="container">				
                <div class="ourhighLight">
                <div class="row">
                    <div class="col-sm-8">
                        <h3 class="swiss-portal-h3">Swiss portal for highest level of safety, confidentiality and privacy in the Swinger World </h3>
                        <ul class="swiss-list">
                            <li>Your date and pictures are stored on a <span class="unique-text">Swiss server</span>. We offer unique <span class="unique-text">Swiss confidentiality and highest possible security</span></li>
                            <li>Members on our portal are "hand-picked". We do not accept all applications, keeping a select circle of high-class hedonistic members.We know less is often more</li>
                            <li>For your security and full privacy, we don't offer chats, forums and videos. We don't show your exact location and don't list your "friends".</li>
                            <li><span>You alone define on your own the audience and location for the visibility of your profile.</span><br/>
                                For example, you could restrict the visibility of your profile only for validated couples and / or for members in a specific geographical area of your choice. For all other members your membership in Hedonia is invisible for your privacy.</li>
                            <li>All or some of your photo albums and all or some of your erotic preferences in detail can be individually accessed by all members,by only certain types of members, or by individually selected members. You maintain <span class="unique-text">full control over your privacy</span>.</li>

                            <li>You have the ability to send your profile page to people outside of Hedonia.ch using a special, password-protected link.</li>
                            <li>The use of complicated and costly <em>adult verifications systems</em> before becoming a member of our portal is not required by Swiss law.  </li>
                            <li>We will never sell your email, your data, or your pictures to third parties. Your profile will be never visible in other publications.We will never send you a newsletter or sales offers. Your privacy and satisfaction is the base of our business.</li>
                            <li>If you cancel your membership in Hedonia, all your data and pictures will be completely deleted, leaving no traces.
                                </li>
                        </ul>                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="portal-sign-btn">
                            <a class="btn btn-success btn-sm signupBtn" href="<?php echo get_permalink( 45 ); ?>">Sign Up</a>
                            <!-- <div class="portal-sign-back-btn"><a href="#"><img src="images/back-button.jpg" alt="logo" class="back-btn" />&nbsp; Back</a></div> -->
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <a href="<?php echo get_permalink( 332 ); ?>" class="btn btn-success btn-sm signupBtn detailCom" >Detail comparison of different memberships in Hedonia.ch <i class="fa fa-chevron-right"></i>
                        </a>
                    </div>
                </div>
                </div>
            </section>
        </div>
        
        <?php get_footer(); ?>
