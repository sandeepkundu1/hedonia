<?php
/**
 * Template Name: Edit for couple
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Twenty Twelve consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
get_header();
$textColor = array("182" => "green", "183" => "green", "184" => "green", "185" => "green", "186" => "yellow", "187" => "red", "188" => "red", "189" => "", "190" => "", "191" => "", "192" => "", "193" => "green", "194" => "green", "195" => "green", "196" => "green", "197" => "", "198" => "");
?>
<?php require_once('user_profile_header.php'); ?>
<script type="text/javascript" src="<?php echo bloginfo('template_url'); ?>/page-templates/js/edit_datepicker.js"></script>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed|Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="<?php echo bloginfo('template_url'); ?>/page-templates/js/script.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<script src="<?php echo bloginfo('template_url'); ?>/page-templates/jquery-1.10.2.js"></script>
<script src="<?php echo bloginfo('template_url'); ?>/page-templates/1.11.2/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo bloginfo('template_url'); ?>/page-templates/js/jquery.js"></script>
<script type="text/javascript" src="<?php echo bloginfo('template_url'); ?>/page-templates/js/datepicker.js"></script>
<script type="text/javascript" src="<?php echo bloginfo('template_url'); ?>/page-templates/js/eye.js"></script>
<script type="text/javascript" src="<?php echo bloginfo('template_url'); ?>/page-templates/js/utils.js"></script>
<script type="text/javascript" src="js/layout.js?ver=1.0.2"></script>

<link rel="stylesheet" media="screen" type="text/css" href="<?php echo bloginfo('template_url'); ?>/page-templates/css/datepicker.css" />
<link rel="stylesheet" type="text/css" href="<?php echo bloginfo('template_url'); ?>/page-templates/css/bootstrap.css">
<link href="<?php echo bloginfo('template_url'); ?>/page-templates/css/font-awesome.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo bloginfo('template_url'); ?>/page-templates/css/fonts.css" rel="stylesheet" type="text/css"/>

<link rel="stylesheet" type="text/css" href="<?php echo bloginfo('template_url'); ?>/page-templates/css/style.css">
<link rel="stylesheet" href="<?php echo bloginfo('template_url'); ?>/page-templates/css/default.css" type="text/css">
<script src="<?php echo bloginfo('template_url'); ?>/page-templates/js/jquery-1.11.1.min.js" type="text/javascript"></script>			
<script src="<?php echo bloginfo('template_url'); ?>/page-templates/js/bootstrap.js" type="text/javascript"></script>	
<script src="<?php echo bloginfo('template_url'); ?>/page-templates/js/core.js" type="text/javascript"></script>
<script src="<?php echo bloginfo('template_url'); ?>/page-templates/js/core.js" type="text/javascript"></script>
<script src="<?php echo bloginfo('template_url'); ?>/page-templates/js/core.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo bloginfo('template_url'); ?>/page-templates/js/zebra_datepicker.js"></script>

<?php
$get_user_info = get_user_meta($current_user->ID);

//echo '<pre>';print_r($get_user_info); echo "</pre>";

/* * ******************************************************Profilepick for couples**************************************** */
$upload_dir = wp_upload_dir();
$image_url_female = $upload_dir['baseurl'] . '/profilepick_' . $current_user->ID . '/' . $get_user_info['her_pick']['0'];
$image_url_male = $upload_dir['baseurl'] . '/profilepick_' . $current_user->ID . '/' . $get_user_info['his_pick']['0'];
/* * ******************************************************End Profilepick for couples**************************************** */

/* * ******************************************************Age  for couples**************************************** */
$age1 = $get_user_info['she_dob']['0'];
$age2 = $get_user_info['he_dob']['0'];
$current_date = date();

$datetime1 = new DateTime($current_date);
$datetime2 = new DateTime($age1);
$her_age = $datetime1->diff($datetime2);
$female_age = $her_age->y;

$datetime3 = new DateTime($current_date);
$datetime4 = new DateTime($age2);
$his_age = $datetime3->diff($datetime4);
$male_age = $his_age->y;

$her_hight = $get_user_info['her_hight']['0'];
$his_hight = $get_user_info['his_hight']['0'];


$her_figure = $get_user_info['her_figure']['0'];
$his_figure = $get_user_info['his_figure']['0'];

$her_shave = $get_user_info['her_shave']['0'];
$his_shave = $get_user_info['his_shave']['0'];

$her_tattoos = $get_user_info['her_tattoos']['0'];
$his_tattoos = $get_user_info['his_tattoos']['0'];

$her_smoke = $get_user_info['her_smoke']['0'];
$his_smoke = $get_user_info['his_smoke']['0'];


$her_language = $get_user_info['her_language']['0'];
$her_language1 = $get_user_info['her_language1']['0'];
$her_language2 = $get_user_info['her_language2']['0'];
$her_language3 = $get_user_info['her_language3']['0'];
$language_array = array($her_language,$her_language1,$her_language2,$her_language3);



$his_language = $get_user_info['his_language']['0'];
$his_language1 = $get_user_info['his_language1']['0'];
$his_language2 = $get_user_info['his_language2']['0'];
$his_language3 = $get_user_info['his_language3']['0'];

$language_array1 = array($his_language,$his_language1,$his_language2,$his_language3);

$her_weight = $get_user_info['her_weight']['0'];
$his_weight = $get_user_info['his_weight']['0'];

$her_profession = $get_user_info['her_profession']['0'];
$his_profession = $get_user_info['his_profession']['0'];

$her_hobby = $get_user_info['her_hobby']['0'];
$his_hobby = $get_user_info['his_hobby']['0'];

$describe = $get_user_info['describe']['0'];
$fav_quote_content = $get_user_info['faq_quote']['0'];






$married = $get_user_info['married']['0'];
$notmarried = $get_user_info['notmarried']['0'];
$seprated = $get_user_info['seprated']['0'];


$nickname = $get_user_info['nickname']['0'];
$first_name = $get_user_info['first_name']['0'];
$last_name = $get_user_info['last_name']['0'];

$country = $get_user_info['country']['0'];
$second_country = $get_user_info['country_second']['0'];

$district1 = $get_user_info['district1']['0'];
$second_district = $get_user_info['district_second']['0'];

$city1 = $get_user_info['city1']['0'];
$second_city = $get_user_info['city_second']['0'];



$reg3_visible_forallmember = $get_user_info['reg3_visible_forallmember']['0'];
$reg3_couple_married = $get_user_info['reg3_couple_married']['0'];

$reg3_ladies = $get_user_info['reg3_ladies']['0'];
$reg3_men = $get_user_info['reg3_men']['0'];

$reg3_fullswap = $get_user_info['reg3_fullswap']['0'];
$reg3_couplentmred = $get_user_info['reg3_couplentmred']['0'];

$reg3_singleladies = $get_user_info['reg3_singleladies']['0'];
$reg3_singlmen = $get_user_info['reg3_singlmen']['0'];

$reg3_sheberequired = $get_user_info['reg3_sheberequired']['0'];
$reg3_onlyverifiedcouple = $get_user_info['reg3_onlyverifiedcouple']['0'];

$reg3_onlyverifiedladies = $get_user_info['reg3_onlyverifiedladies']['0'];
$reg3onlyverifiedmen = $get_user_info['reg3onlyverifiedmen']['0'];
$reg3_hebrequired = $get_user_info['reg3_hebrequired']['0'];

$reg3_wedntknwwttodo = $get_user_info['reg3_wedntknwwttodo']['0'];
$reg3_softswaporal = $get_user_info['reg3_softswaporal']['0'];
$reg3_shebinterested = $get_user_info['reg3_shebinterested']['0'];
$reg3_frstfrnwthtsex = $get_user_info['reg3_frstfrnwthtsex']['0'];
$reg3_swpngntexclud = $get_user_info['reg3_swpngntexclud']['0'];
$reg3_hebintrested = $get_user_info['reg3_hebintrested']['0'];
$reg3_wtchnshwngskincntct = $get_user_info['reg3_wtchnshwngskincntct']['0'];
$reg3_fullswpintrcrsrqr = $get_user_info['reg3_fullswpintrcrsrqr']['0'];
$reg3_cmpltlyopn = $get_user_info['reg3_cmpltlyopn']['0'];
$reg3_inorhouse = $get_user_info['reg3_inorhouse']['0'];

$reg3_inclub = $get_user_info['reg3_inclub']['0'];
$reg3_urplace = $get_user_info['reg3_urplace']['0'];
$reg3_outdoor = $get_user_info['reg3_outdoor']['0'];
$reg3_inhotel = $get_user_info['reg3_inhotel']['0'];
$reg3_othrplace = $get_user_info['reg3_othrplace']['0'];
$reg3_txtshe = $get_user_info['reg3_txtshe']['0'];
$reg3_txthe = $get_user_info['reg3_txthe']['0'];
$reg3_txtboth = $get_user_info['reg3_both']['0'];







/* * ******************************************************End Age for couples**************************************** */

$id = get_the_ID();
$page_title = get_page($id);
$page_title->post_title;
?>

<?php
if (is_user_logged_in()) {
    ?>
    <div class="userProfile">
        <div class="container">        
            <div class="row">
                <div class="col-sm-12">
                    <form class="form-horizontal" role="form" name="form" method="post">
                        <div class="col-sm-7 no-padding-right">
                            <div class="col-sm-6 no-padding profilePicWrap">
						<?php if($get_user_info['approved_as_basic']['0'] == 'no'){ ?>
                                <div class="">
									
                                    <div class="col-sm-6 no-padding-left">
									<div class="posRel">
									<span class="grayTrans"></span>
                                        <button type="button" class="editLink" data-toggle="modal" data-target="#edit_female_pick" id="edit">edit</button>                                
                                        <h3 class="search_premium_heading red_heading"><?php echo $get_user_info['nickname']['0']; ?></h3>
                                        <?php if($get_user_info['her_pick']['0']!='no_img.jpg') { ?>
                                              <img class="prof-cartoon-img" src="<?php echo $image_url_female; ?>">  
                                        <?php } else { ?>
											  <img class="prof-cartoon-img" src="<?php echo bloginfo('template_url'); ?>/page-templates/images/images.jpeg">
											<?php } ?>  
                                                                         
                                    </div>
									</div>
                                    <div class="col-sm-6 no-padding-left">
									<div class="posRel">
									<span class="grayTrans"></span>
                                        <button type="button" class="editLink" data-toggle="modal" data-target="#edit_male_pick" id="edit">edit</button>  
                                        <?php if($get_user_info['his_pick']['0']!='no_img.jpg') { ?>                              
                                        <img class="prof-cartoon-img" src="<?php echo $image_url_male; ?>">
                                         <?php } else { ?>
											  <img class="prof-cartoon-img" src="<?php echo bloginfo('template_url'); ?>/page-templates/images/images.jpeg">
											<?php } ?>                                 
                                    </div>
									</div>
                                </div> 
                           <?php } else { ?>
							   <div class="">
                                    <div class="col-sm-6 no-padding-left">
                                        <button type="button" class="editLink" data-toggle="modal" data-target="#edit_female_pick" id="edit">edit</button>                                
                                        <h3 class="search_premium_heading red_heading"><?php echo $get_user_info['nickname']['0']; ?></h3>
                                        <img class="prof-cartoon-img" src="<?php echo $image_url_female; ?>">                                
                                    </div>
                                    <div class="col-sm-6 no-padding-left">
                                        <button type="button" class="editLink" data-toggle="modal" data-target="#edit_male_pick" id="edit">edit</button>                                
                                        <img class="prof-cartoon-img" src="<?php echo $image_url_male; ?>">                                
                                    </div>
                                </div> 
							   <?php  } ?>
							        
                            </div>
                            <div class="col-sm-6 no-padding">
                                <?php if ($get_user_info['user_type']['0'] == 'premium') { ?>

                                    <h5 class="red profileType"><?php
                                        $explode = explode('_', $get_user_type['user_type']['0']);
                                        //print_r($explode);
                                        echo $explode['0'];
                                        ?> member</h5>
                                <?php } elseif ($get_user_info['user_type']['0'] == 'validated') { ?>   
                                    <h5 class="greenText profileType"><?php
                                        $explode = explode('_', $get_user_type['user_type']['0']);
                                        //print_r($explode);
                                        echo $explode['0'];
                                        ?> member
                                        <img width="12" src="<?php echo bloginfo('template_url'); ?>/images/tick-green.png" alt="">
                                    </h5>

                                <?php } elseif ($get_user_info['user_type']['0'] == 'basic_member') { ?>
                                    <h5 class="greenText profileType" style="color:#989898;"><?php
                                        $explode = explode('_', $get_user_type['user_type']['0']);
                                        //print_r($explode);
                                        echo $explode['0'];
                                        ?> member</h5>
                                <?php } ?>
                                <div class="Profile-Part-BI  cus_scroll" id="hideuserInfo">
                                    <button type="button" class="editLink" data-toggle="modal" data-target="#edit_basic_info" id="edit">edit</button>

                                    <h3 class="Proh-h3"><span>BASIC INFO</span></h3>
                                    <div class="row">
                                        <p class="Pro-p col-sm-4 col-xs-offset-4">she</p>
                                        <p class="Pro-p col-sm-4">he</p> 
                                    </div>
                                    <div class="padding-basic-info" id="show_basic_info">
                                        <div class="form-group border-bottom">
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Age</label>
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center bold"><?php echo $female_age; ?></label>
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center bold"><?php echo $male_age; ?></label>
                                        </div>
                                        <div class="form-group border-bottom">
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Height (cm)</label>
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center bold"><?php echo $her_hight; ?></label>
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center bold"><?php echo $his_hight; ?></label>
                                        </div>
                                        <div class="form-group border-bottom">
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Figure</label>
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center"><?php echo $her_figure; ?></label>
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center"><?php echo $his_figure; ?></label>
                                        </div>
                                        <div class="form-group border-bottom">
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Relationship</label>
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center"><?php echo $get_user_info['relationship']['0']; ?></label>
                                        </div>
                                        <div class="form-group border-bottom">
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Smoking</label>
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center"><?php echo $her_smoke; ?></label>
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center"><?php echo $his_smoke; ?></label>
                                        </div>

                                        <div class="form-group border-bottom row">
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Hair color</label>
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center"><?php echo $get_user_info['her_haircolor']['0']; ?></label>
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center"><?php echo $get_user_info['his_haircolor']['0']; ?></label>
                                        </div>

                                        <div class="form-group border-bottom">
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Languages</label>
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center"><?php echo $her_language; ?></label>
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center"><?php echo $his_language; ?></label>
                                        </div>
                                        <div class="form-group border-bottom">
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Graduation</label>
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center"><?php echo $get_user_info['her_graduation']['0']; ?></label>
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center"><?php echo $get_user_info['his_graduation']['0']; ?></label>
                                        </div>                                        
                                        <div class="form-group border-bottom">
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Hobbies Interests</label>
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center"><?php echo $her_hobby; ?></label>
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center"><?php echo $his_hobby; ?></label>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Choose your basic info about you</label>
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center"><?php echo $get_user_info['about_her']['0']; ?></label>
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center"><?php echo $get_user_info['about_his']['0']; ?></label>
                                        </div>
                                        <!--                                        <div class="form-group border-bottom">
                                                                                    <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Shaved</label>
                                                                                    <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center"><?php echo $her_shave; ?></label>
                                                                                    <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center"><?php echo $his_shave; ?></label>
                                                                                </div>
                                                                                <div class="form-group border-bottom">
                                                                                    <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Piercings / tattoos</label>
                                                                                    <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center"><?php echo $her_tattoos; ?></label>
                                                                                    <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center"><?php echo $his_tattoos; ?></label>
                                                                                </div>-->
                                    </div>

                                    <div class="clear"></div>
                                </div>

                            </div>
                            <div class="clear">
                                <div class="col-sm-4 col-md-4 col-xs-12 no-padding">
                                    <div class="list_div">
                                        <ul class="side-bar-profile no-padding">
											<?php if($get_user_info['approved_as_basic']['0'] == 'no'){ ?>
											  <li class="invalid"><span class="grayTrans"></span><a href = "#"><i>
                                                            <img src="<?php echo bloginfo("template_url"); ?>/images/hand.jpg">
                                                        </i><span>i like add to favourites</span></a></li>

                                                <li class="invalid"><span class="grayTrans"></span><a href = "#"><i><img src="<?php echo bloginfo("template_url"); ?>/images/lip.jpg"></i><span>send a kiss</span></a></li>

                                                <li class="invalid"><span class="grayTrans"></span><a href ="#" ><i><img src="<?php echo bloginfo("template_url"); ?>/images/mes1.jpg"></i><span>send mail</span></a></li>

                                                <li class="invalid"><span class="grayTrans"></span><a href ="#" ><i><img src="<?php echo bloginfo("template_url"); ?>/images/mes3.jpg"></i><span>mail history</span></a></li>

                                                <li class="invalid"><span class="grayTrans"></span><a href ="#" ><i><img src="<?php echo bloginfo("template_url"); ?>/images/lockband.jpg"></i><span>hide/show profile<br/> open/close albums<br/> erotic preferences</span></a></li>

                                                <li class="invalid"><span class="grayTrans"><a href ="#" ></span><i><img src="<?php echo bloginfo("template_url"); ?>/images/note.jpg"></i><span>make a note<br/> for this profile</span></a></li>

                                                <li class="invalid"><span class="grayTrans"></span><a href ="#" ><i><img src="<?php echo bloginfo("template_url"); ?>/images/block.jpg"></i><span>block this profile</span></a></li>

                                                <li class="invalid"><span class="grayTrans"></span><a href ="#" ><i><img src="<?php echo bloginfo("template_url"); ?>/images/submit.jpg"></i><span>submit complaint about this profile</span></a></li>
											
                                            <?php  } elseif ($get_user_info['user_type']['0'] == 'premium') { ?>
                                                <li><a href = "#"><i>
                                                            <img src="<?php echo bloginfo("template_url"); ?>/images/hand.jpg">
                                                        </i><span>i like<br/>   add to favourites</span></a></li>

                                                <li><a href = "#"><i><img src="<?php echo bloginfo("template_url"); ?>/images/lip.jpg"></i><span>send a kiss</span></a></li>

                                                <li><a href ="#" ><i><img src="<?php echo bloginfo("template_url"); ?>/images/mes1.jpg"></i><span>send mail</span></a></li>

                                                <li><i><img src="<?php echo bloginfo("template_url"); ?>/images/mes3.jpg"></i><span>mail history</span></li>

                                                <li><i><img src="<?php echo bloginfo("template_url"); ?>/images/lockband.jpg"></i><span>hide/show profile<br/> open/close albums<br/> erotic preferences</span></li>

                                                <li><i><img src="<?php echo bloginfo("template_url"); ?>/images/note.jpg"></i><span>make a note <br/>for this profile</span></li>

                                                <li><i><img src="<?php echo bloginfo("template_url"); ?>/images/block.jpg"></i><span>block this profile</span></li>

                                                <li><i><img src="<?php echo bloginfo("template_url"); ?>/images/submit.jpg"></i><span>submit complaint<br/> about this profile</span></li>

                                            <?php } elseif ($get_user_info['user_type']['0'] == 'validated') { ?>


                                                <li>
                                                    <a href = "#"><i><img src="<?php echo bloginfo("template_url"); ?>/images/hand.jpg"></i><span>i like<br/>   add to favourites</span></a></li>

                                                <li class="invalid"><span class="grayTrans"></span><a href = "#"><i><img src="<?php echo bloginfo("template_url"); ?>/images/lip.jpg"></i><span>send a kiss</span></a></li>

                                                <li><a href ="#" ><i><img src="<?php echo bloginfo("template_url"); ?>/images/mes1.jpg"></i><span>send mail</span></a></li>

                                                <li><a href ="#" ><i><img src="<?php echo bloginfo("template_url"); ?>/images/mes3.jpg"></i><span>mail history</span></a></li>

                                                <li class="invalid"><div class="clear"></div><span class="grayTrans"></span><a href ="#" ><i><img src="<?php echo bloginfo("template_url"); ?>/images/lockband.jpg"></i><span>hide/show profile<br/> open/close albums<br/> erotic preferences</span></a></li>

                                                <li class="invalid"><div class="clear"></div><span class="grayTrans"></span><a href ="#" ><i><img src="<?php echo bloginfo("template_url"); ?>/images/note.jpg"></i><span>make a note <br/>for this profile</span></a></li>

                                                <li><a href ="#" ><i><img src="<?php echo bloginfo("template_url"); ?>/images/block.jpg"></i><span>block this profile</span></a></li>

                                                <li><a href ="#" ><i><img src="<?php echo bloginfo("template_url"); ?>/images/submit.jpg"></i><span>submit complaint<br/> about this profile</span></a></li>


                                            <?php } elseif ($get_user_info['user_type']['0'] == 'basic_member') { ?>



                                                <li class="invalid"><span class="grayTrans"></span><a href = "#"><i>
                                                            <img src="<?php echo bloginfo("template_url"); ?>/images/hand.jpg">
                                                        </i><span>i like add to favourites</span></a></li>

                                                <li class="invalid"><span class="grayTrans"></span><a href = "#"><i><img src="<?php echo bloginfo("template_url"); ?>/images/lip.jpg"></i><span>send a kiss</span></a></li>

                                                <li><a href ="#" ><i><img src="<?php echo bloginfo("template_url"); ?>/images/mes1.jpg"></i><span>send mail</span></a></li>

                                                <li><a href ="#" ><i><img src="<?php echo bloginfo("template_url"); ?>/images/mes3.jpg"></i><span>mail history</span></a></li>

                                                <li class="invalid"><span class="grayTrans"></span><a href ="#" ><i><img src="<?php echo bloginfo("template_url"); ?>/images/lockband.jpg"></i><span>hide/show profile<br/> open/close albums<br/> erotic preferences</span></a></li>

                                                <li class="invalid"><span class="grayTrans"><a href ="#" ></span><i><img src="<?php echo bloginfo("template_url"); ?>/images/note.jpg"></i><span>make a note<br/> for this profile</span></a></li>

                                                <li class="invalid"><span class="grayTrans"></span><a href ="#" ><i><img src="<?php echo bloginfo("template_url"); ?>/images/block.jpg"></i><span>block this profile</span></a></li>

                                                <li class="invalid"><span class="grayTrans"></span><a href ="#" ><i><img src="<?php echo bloginfo("template_url"); ?>/images/submit.jpg"></i><span>submit complaint about this profile</span></a></li>

                                            <?php } ?>

                                        </ul>
                                    </div>
                                    <div class="clear"></div>
                                    <hr/>
                                    <span class="border-bottom"></span>
                                    <?php if ($get_user_info['user_type']['0'] == 'premium') { ?>
                                        <div id="addText" class="text-box-round">                                        
                                            <?php $speed_dating = get_post(362); ?>
                                            <h5><?php echo $speed_dating->post_title; ?></h5><span>
                                                <button type="button" class="editLink" data-toggle="modal" data-target="#speed_dating" id="edit">edit</button></span>
                                            <?php echo $get_user_info['speed_data']['0']; ?>
                                        </div>
                                        <div class="text-box-round">
                                            <?php $trave_agenda = get_post(364); ?>
                                            <h5><?php echo $trave_agenda->post_title; ?></h5><span>
                                                <button type="button" class="editLink" data-toggle="modal" data-target="#travel_agenda" id="edit">edit</button></span>
                                            <p><?php echo $get_user_info['travel_agent']['0']; ?></p>
                                        </div>
                                    <?php } else { ?>


                                        <div class="text-box-round invalid">
                                            <span class="grayTrans"></span>
                                            <?php $speed_dating = get_post(362); ?>
                                            <h5><?php echo $speed_dating->post_title; ?></h5>
                                            <p><?php echo $speed_dating->post_content; ?></p>
                                        </div>
                                        <div class="text-box-round invalid">
                                            <span class="grayTrans"></span>
                                            <?php $trave_agenda = get_post(364); ?>
                                            <h5><?php echo $trave_agenda->post_title; ?></h5>
                                            <p><?php echo $trave_agenda->post_content; ?></p>
                                        </div>


                                    <?php } ?>

                                </div>	
                                <div class="col-sm-8 col-md-8 col-xs-12 no-padding">
                                    <div class="col-sm-7 col-md-7 col-xs-12 ">
								<?php if($get_user_info['approved_as_basic']['0']=='no') {  ?> 		
                                        <div class="Album-Part">
											<span class="grayTrans"></span>
                                            <div class="row"> 
                                                <h3 class="Pro-h3-2  col-sm-12 col-md-12"><span>OUR ALBUMS</span></h2><span class="edit-profile">edit</span>
                                            </div>

                                            <div class="WidthFull">
                                                <?php
                                                global $current_user;
                                                get_currentuserinfo();
                                                $current_user->user_type;
                                                $results = $wpdb->get_results("SELECT * FROM user_create_album WHERE user_id = '$current_user->ID'");
                                                $upload_dir = wp_upload_dir();
                                                $imgPath = $upload_dir['baseurl'] . '/users_album/album_' . $current_user->ID . '/';
                                                $i = 1;
                                                $count = 1;
                                                foreach ($results as $value) {
                                                    $select_img = $wpdb->get_results("SELECT * FROM user_album_images WHERE album_id = '$value->id' order by img_order ASC limit 1");
                                                    if (count($select_img) == 0) {
                                                        ?>
                                                        <figure class="album-img-div text-center">
                                                            <img src="<?php bloginfo('template_url'); ?>/images/NoAlbum.jpeg" alt="">
                                                            <figcaption><?php echo $count++; ?></figcaption>
                                                        </figure>
                                                        <?php
                                                    } else {
                                                        foreach ($select_img as $select) {
                                                            $number = '"' . $value->id . '"';
                                                            ?>                                                            
                                                            <figure class="album-img-div text-center"><a href="<?php echo get_permalink(424); ?>&imgid=<?php echo base64_encode($number); ?>" name='imgid' target='_blank' value="<?php echo $value->id; ?>" >
                                                                    <img src="<?php echo $imgPath . $select->img_url; ?>"></a><figcaption><?php echo $count++; ?></figcaption></figure>                                      
                                                            <?php
                                                        }
                                                    }
                                                }
                                                if (count($results) != 0) {
                                                    $sum = 1;
                                                    for ($i = 0; $i < (5 - count($results)); $i++) {
                                                        ?>
                                                        <figure class="album-img-div text-center">
                                                            <img src="<?php bloginfo('template_url'); ?>/images/alb_2.jpg" alt="">
                                                            <figcaption><?php echo count($results) + $sum; ?></figcaption>
                                                        </figure>
                                                        <?php
                                                        $sum++;
                                                    }
                                                } else {
                                                    echo "No Album";
                                                }
                                                ?>
                                            </div>
                                            <!--
                                            <div class="WidthFull">
                                                <div class="album-lock-div "><label for="inputEmail3" class="col-sm-2 BasicInfo-label para-center bold">1</label></div>
                                                <div class="album-lock-div"><label for="inputEmail3" class="col-sm-2 BasicInfo-label para-center bold">2<img src="<?php echo bloginfo("template_url"); ?>/images/lock.png"></label></div>
                                                <div class="album-lock-div"><label for="inputEmail3" class="col-sm-2 BasicInfo-label para-center bold">3<img src="<?php echo bloginfo("template_url"); ?>/images/lock.png"></label></div>
                                                <div class="album-lock-div"><label for="inputEmail3" class="col-sm-2 BasicInfo-label para-center bold">4<img src="<?php echo bloginfo("template_url"); ?>/images/lock.png"></label></div>
                                                <div class="album-lock-div"><label for="inputEmail3" class="col-sm-2 BasicInfo-label para-center bold">5<img src="<?php echo bloginfo("template_url"); ?>/images/lock.png"></label></div>
                                            </div>
                                            -->
                                        </div>
                   <?php } else { ?>
					   
					   
					          <div class="Album-Part">
                                            <div class="row"> 
                                                <h3 class="Pro-h3-2  col-sm-12 col-md-12"><span>OUR ALBUMS</span></h2><span class="edit-profile">edit</span>
                                            </div>

                                            <div class="WidthFull">
                                                <?php
                                                global $current_user;
                                                get_currentuserinfo();
                                                $current_user->user_type;
                                                $results = $wpdb->get_results("SELECT * FROM user_create_album WHERE user_id = '$current_user->ID'");
                                                $upload_dir = wp_upload_dir();
                                                $imgPath = $upload_dir['baseurl'] . '/users_album/album_' . $current_user->ID . '/';
                                                $i = 1;
                                                $count = 1;
                                                foreach ($results as $value) {
                                                    $select_img = $wpdb->get_results("SELECT * FROM user_album_images WHERE album_id = '$value->id' order by img_order ASC limit 1");
                                                    if (count($select_img) == 0) {
                                                        ?>
                                                        <figure class="album-img-div text-center">
                                                            <img src="<?php bloginfo('template_url'); ?>/images/NoAlbum.jpeg" alt="">
                                                            <figcaption><?php echo $count++; ?></figcaption>
                                                        </figure>
                                                        <?php
                                                    } else {
                                                        foreach ($select_img as $select) {
                                                            $number = '"' . $value->id . '"';
                                                            ?>                                                            
                                                            <figure class="album-img-div text-center"><a href="<?php echo get_permalink(424); ?>&imgid=<?php echo base64_encode($number); ?>" name='imgid' target='_blank' value="<?php echo $value->id; ?>" >
                                                                    <img src="<?php echo $imgPath . $select->img_url; ?>"></a><figcaption><?php echo $count++; ?></figcaption></figure>                                      
                                                            <?php
                                                        }
                                                    }
                                                }
                                                if (count($results) != 0) {
                                                    $sum = 1;
                                                    for ($i = 0; $i < (5 - count($results)); $i++) {
                                                        ?>
                                                        <figure class="album-img-div text-center">
                                                            <img src="<?php bloginfo('template_url'); ?>/images/alb_2.jpg" alt="">
                                                            <figcaption><?php echo count($results) + $sum; ?></figcaption>
                                                        </figure>
                                                        <?php
                                                        $sum++;
                                                    }
                                                } else {
                                                    echo "No Album";
                                                }
                                                ?>
                                            </div>
                                            <!--
                                            <div class="WidthFull">
                                                <div class="album-lock-div "><label for="inputEmail3" class="col-sm-2 BasicInfo-label para-center bold">1</label></div>
                                                <div class="album-lock-div"><label for="inputEmail3" class="col-sm-2 BasicInfo-label para-center bold">2<img src="<?php echo bloginfo("template_url"); ?>/images/lock.png"></label></div>
                                                <div class="album-lock-div"><label for="inputEmail3" class="col-sm-2 BasicInfo-label para-center bold">3<img src="<?php echo bloginfo("template_url"); ?>/images/lock.png"></label></div>
                                                <div class="album-lock-div"><label for="inputEmail3" class="col-sm-2 BasicInfo-label para-center bold">4<img src="<?php echo bloginfo("template_url"); ?>/images/lock.png"></label></div>
                                                <div class="album-lock-div"><label for="inputEmail3" class="col-sm-2 BasicInfo-label para-center bold">5<img src="<?php echo bloginfo("template_url"); ?>/images/lock.png"></label></div>
                                            </div>
                                            -->
                                        </div>
					   <?php } ?>
                                    </div>
                                    <div class="col-sm-5 col-md-5 col-xs-12 no-padding">
										<?php if($get_user_info['approved_as_basic']['0']=='no') {  ?> 
                                        <div class="Favourite-Part">
											<span class="grayTrans"></span>
                                            <div class=""> 
                                                <h3 class="Pro-h4 col-sm-12 col-md-12"><span>FAVOURITE QUOTE</span></h2><span>
                                                        <button type="button" class="editLink" data-toggle="modal" data-target="#fav_quote" id="edit">edit</button></span>
                                            </div>
                                            <div class="col-sm-12 col-md-12 no-padding"> 
                                                <p class="fav-para"><?php echo $fav_quote_content; ?></p>
                                            </div>
                                        </div>
                                    
                                    <?php } else { ?>
										
									<div class="Favourite-Part">
                                            <div class=""> 
                                                <h3 class="Pro-h4 col-sm-12 col-md-12"><span>FAVOURITE QUOTE</span></h2><span>
                                                        <button type="button" class="editLink" data-toggle="modal" data-target="#fav_quote" id="edit">edit</button></span>
                                            </div>
                                            <div class="col-sm-12 col-md-12 no-padding"> 
                                                <p class="fav-para"><?php echo $fav_quote_content; ?></p>
                                            </div>
                                        </div>
                                   
                                    
                                    <?php } ?>	
 </div>
                                    <?php
                                    $results = $wpdb->get_results("SELECT * FROM bg_images where active = 1");
                                    $upload_dir = wp_upload_dir();
                                    $img = "";
                                    foreach ($results as $value) {
                                        $img = $value->img_url;
                                    }
                                    $imgPath = $upload_dir['baseurl'] . '/background_images' . '/' . $img;
                                    ?>


                                    <div class="col-sm-12 col-md-12 col-xs-12 no-padding-right prelative">
                                        
                                        <h3 class="Pro-h3 newIntro"><span>INTRODUCTION</span></h3><span></span>
                                        <div class="change_bg">
                                                    <span>
                                                        <button type="button" class="changeLanguage" data-toggle="modal" data-target="#change_background" id="edit">Change Background</button>
                                                    </span>
                                                </div>
                                                <button type="button" class="editLink" data-toggle="modal" data-target="#edit_intro" id="edit">edit</button>
                                       <div  id="content-1"> 
                                            <div class="Introduction-Part" style="background-size: 100% 100%; background-image: url('<?php echo $imgPath; ?>');" id ="introduction_part">

                                                <div class="padding-para-div"> 
                                                    <p class="Intro-para" id="intro_data"><?php echo $describe; ?> </p>
                                                    
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-5 col-md-5 col-xs-12">
                            <div class="col-sm-6 col-md-6 col-xs-12 no-padding">
                                <div class="Favourite-Part2">
                                    <div class=""> 
                                        <h3 class="Pro-h3 col-sm-12 col-md-12"><span>MAIN RESIDENCE</span></h2> <span><button type="button" class="editLink" data-toggle="modal" data-target="#edit_main_residence" id="edit">edit</button></span>
                                    </div>
                                    <div class="padding-top-bottom">
                                        <input type="hidden" name="first_add" value="first">
                                        <div class="form-group"> 
                                            <label for="inputEmail3" class="col-sm-5 BasicInfo-label">COUNTRY :</label>
                                            <label for="inputEmail3" class="col-sm-7 BasicInfo-label para-left"><?php echo $country; ?></label>
                                        </div>
                                        <div class="form-group"> 
                                            <label for="inputEmail3" class="col-sm-5 BasicInfo-label">REGIO :</label>
                                            <label for="inputEmail3" class="col-sm-7 BasicInfo-label para-left"><?php echo $district1; ?></label>
                                        </div>


                                        <div class="form-group"> 
                                            <label for="inputEmail3" class="col-sm-5 BasicInfo-label">NEXT CITY :</label>
                                            <label for="inputEmail3" class="col-sm-7 BasicInfo-label para-left"><?php echo $city1; ?></label>
                                        </div>
                                        <div class="form-group"> 
                                            <label for="inputEmail3" class="col-sm-5 BasicInfo-label">Frequently in :</label>
                                            <label for="inputEmail3" class="col-sm-7 BasicInfo-label para-left"><?php echo $get_user_info['frequently_in']['0']; ?></label>
                                        </div>
                                    </div>
                                </div>
                                <?php if ($get_user_info['user_type']['0'] == 'premium') { ?>
                                    <div class="Favourite-Part3">
                                        <div class=""> 
                                            <h3 class="Pro-h3 col-sm-12 col-md-12"><span>SECOND RESIDENCE</span></h2> <span><button type="button" class="editLink" data-toggle="modal" data-target="#edit_second_residence" id="edit">edit</button></span>
                                        </div>
                                        <div class="padding-top-bottom">
                                            <input type="hidden" name="second_add" value="second">
                                            <div class="form-group"> 
                                                <label for="inputEmail3" class="col-sm-5 BasicInfo-label">COUNTRY :</label>
                                                <label for="inputEmail3" class="col-sm-7 BasicInfo-label para-left"><?php echo $get_user_info['country_second']['0']; ?></label>
                                            </div>
                                            <div class="form-group"> 
                                                <label for="inputEmail3" class="col-sm-5 BasicInfo-label">REGIO :</label>
                                                <label for="inputEmail3" class="col-sm-7 BasicInfo-label para-left"><?php echo $get_user_info['district_second']['0']; ?></label>
                                            </div>
                                            <div class="form-group"> 
                                                <label for="inputEmail3" class="col-sm-5 BasicInfo-label">NEXT CITY :</label>
                                                <label for="inputEmail3" class="col-sm-7 BasicInfo-label para-left"><?php echo $get_user_info['city_second']['0']; ?></label>
                                            </div>
                                            <!--                                            <div class="form-group"> 
                                                                                            <label for="inputEmail3" class="col-sm-5 BasicInfo-label">STATE :</label>
                                                                                            <label for="inputEmail3" class="col-sm-7 BasicInfo-label para-left"><?php echo $second_district; ?></label>
                                                                                        </div>
                                                                                        <div class="form-group"> 
                                                                                            <label for="inputEmail3" class="col-sm-5 BasicInfo-label">MOBILITY :</label>
                                                                                            <label for="inputEmail3" class="col-sm-7 BasicInfo-label para-left"><?php echo $second_city; ?></label>
                                                                                        </div>-->
                                        </div>
                                    </div>
                                <?php } else { ?>


                                    <div class="Favourite-Part3 invalid">
                                        <span class="grayTrans-more"></span>
                                        <div class=""> 
                                            <h3 class="Pro-h3 col-sm-12 col-md-12"><span>SECOND RESIDENCE</span></h2>
                                        </div>
                                        <div class="padding-top-bottom">
                                            <div class="form-group"> 
                                                <label for="inputEmail3" class="col-sm-5 BasicInfo-label">COUNTRY :</label>
                                                <label for="inputEmail3" class="col-sm-7 BasicInfo-label para-left"><?php echo $second_country; ?></label>
                                            </div>
                                            <div class="form-group"> 
                                                <label for="inputEmail3" class="col-sm-5 BasicInfo-label">STATE :</label>
                                                <label for="inputEmail3" class="col-sm-7 BasicInfo-label para-left"><?php echo $second_district; ?></label>
                                            </div>
                                            <div class="form-group"> 
                                                <label for="inputEmail3" class="col-sm-5 BasicInfo-label">MOBILITY :</label>
                                                <label for="inputEmail3" class="col-sm-7 BasicInfo-label para-left"><?php echo $second_city; ?></label>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>		


                                <?php
                                $userid = get_current_user_id();
                                $arr = array();
                                $subId = array();
                                $status = array();
                                $myrows = $wpdb->get_results("SELECT *  FROM `wp_erotics_preference` where `userid` = $userid AND `type` = 1");
                                foreach ($myrows as $result) {
                                    $arr[] = $result->like_dislike_attr_id;
                                    $subId[] = $result->subject_id;
                                    $status[] = $result->status;
                                }

                                $myrows1 = $wpdb->get_results("SELECT *  FROM `wp_erotics_preference` where `userid` = $userid AND `type` = 2");
                                $arr1 = array();
                                $subId1 = array();
                                $status1 = array();

                                foreach ($myrows1 as $result) {
                                    $arr1[] = $result->like_dislike_attr_id;
                                    $subId1[] = $result->subject_id;
                                    $status1[] = $result->status;
                                }


                                $arg = array('posts_per_page' => -1, 'offset' => 1, 'category' => 6, 'post_type' => 'erotics', 'order' => 'asc');
                                $myposts = get_posts($arg);


                                $myAllposts = array();
                                $ctagoryid = array("3", "4", "5");

                                foreach ($ctagoryid as $values) {
                                    $args = array('posts_per_page' => -1, 'offset' => 1, 'category' => $values->ID, 'post_type' => 'erotics', 'order' => 'asc', 'show_count' => 0);
                                    $myAllposts = get_posts($args);
                                }




                                $content = array();
                                foreach ($myAllposts as $value) {
                                    if (in_array($value->ID, $subId)) {
                                        $content['subject'][] = $value->post_title;
                                    }
                                }




                                foreach ($arr as $key => $value1) {
                                    foreach ($myposts as $value) {
                                        if ($value1 == $value->ID) {
                                            $content['desc'][] = $value->post_title;
                                            $cotent['id'][] = $value->ID;
                                        }
                                    }
                                }





                                $content1 = array();
                                foreach ($myAllposts as $value) {
                                    if (in_array($value->ID, $subId1)) {
                                        $content1['subject'][] = $value->post_title;
                                    }
                                }





                                foreach ($arr1 as $key => $value1) {
                                    foreach ($myposts as $value) {
                                        if ($value1 == $value->ID) {
                                            $content1['desc'][] = $value->post_title;
                                            $content1['id'][] = $value->ID;
                                        }
                                    }
                                }

                                //print_r($cotent['id']);
                                ?>

                                <?php if ($get_user_info['user_type']['0'] == 'premium' || $get_user_info['user_type']['0'] == 'validated') { ?>
                                    <div class="eropic-profile-Part">
                                        <div class=""> 
                                            <h3 class="eropic-h3 no-margin no-padding col-sm-12 col-md-12"><span>Her erotic profile</span> <a href ="<?php echo get_permalink(245); ?>" class="editLink"   id="edit">edit</a><a class="profile-her-erotic-right-btn" href="#">E </a> </h3> 
                                            
                                        </div>

                                        <?php
                                        $count = 0;
                                        //print_r($textColor);

                                        for ($i = 0; $i < count($content['subject']); $i++) {
                                            $color = $cotent['id'][$count];
                                            if ($status[$count] == 0) {
                                                $text = '(A)';
                                            } else if ($status[$count] == 1) {
                                                $text = '(P)';
                                            } elseif ($status[$count] == 2) {
                                                $text = "";
                                            }
                                            if ($status[$count] == 0) {
                                                $bgColor = "";
                                            } else if ($status[$count] == 1) {
                                                $bgColor = "";
                                            } elseif ($status[$count] == 2) {
                                                $bgColor = "red";
                                            }
                                            ?>				
                                            <div class="eropic-box-round " style="background-color:<?php echo ($bgColor != "") ? $bgColor : $textColor[$color]; ?>">
                                                <p><strong class="up-case"><?php echo $content['desc'][$i]; ?> :</strong><?php echo $content['subject'][$i] . "&nbsp&nbsp&nbsp" . $text; ?></p>
                                            </div>
                                            <?php
                                            $count++;
                                        }
                                        ?>

                                    </div>

                                <?php } else { ?>

                                    <div class="eropic-profile-Part invalid">
                                        <span class="grayTrans-more"></span>
                                        <h3 class="eropic-h3 no-margin no-padding col-sm-12 col-md-12"><span class="red">Her erotic profile</span> <a class="profile-her-erotic-right-btn" href="#">E </a></h3>

                                        <?php
                                        $count = 0;

                                        for ($i = 0; $i < count($content['subject']); $i++) {
                                            $color = $cotent['id'][$count];
                                            if ($status[$count] == 0) {
                                                $text = '(A)';
                                            } else if ($status[$count] == 1) {
                                                $text = '(P)';
                                            } elseif ($status[$count] == 2) {
                                                $text = "";
                                            }
                                            if ($status[$count] == 0) {
                                                $bgColor = "";
                                            } else if ($status[$count] == 1) {
                                                $bgColor = "";
                                            } elseif ($status[$count] == 2) {
                                                $bgColor = "red";
                                            }
                                            ?>
                                            <div class="eropic-box-round" style="background-color:<?php echo ($bgColor != "") ? $bgColor : $textColor[$color]; ?>">
                                                <p><strong class="up-case"><?php echo $content['desc'][$i]; ?> :</strong><?php echo $content['subject'][$i] . "&nbsp&nbsp&nbsp" . $text; ?></p>
                                            </div>
                                            <?php
                                            $count++;
                                        }
                                        ?>

                                    </div>


                                <?php } ?>	

                            </div>


                            <div class="col-sm-6 col-md-6 col-xs-12 no-padding-right">
                                <?php if ($get_user_info['user_type']['0'] == 'premium') { ?>
                                    <div class="perfect-match-Part cus_scrolln">
                                        
                                            <div class=""> 
                                                <h3 class="opm-h3 col-sm-12 col-md-12"><span>PERFECT MATCH</span></h2>
                                                    <span><button type="button" class="editLink" data-toggle="modal" data-target="#edit_perfect_match" id="edit">edit</button></span>
                                            </div>

                                            <div class="opm-text-box-round">
                                                <strong>SIE :</strong><?php echo $reg3_txtshe; ?>
                                            </div>
                                            <div class="opm-text-box-roundsec">
                                                <strong>ER :</strong><?php echo $reg3_txthe; ?>
                                            </div>
                                            <div class="opm-text-box-roundsec">
                                                <strong>BEIDE :</strong><?php echo $reg3_txtboth; ?>
                                            </div>
                                    </div>
                                    <?php } else { ?>

                                        <div class="perfect-match-Part cus_scrolln invalid">
                                            <span class="greenTrans"></span>

                                            <div class=""> 
                                                <h3 class="opm-h3 col-sm-12 col-md-12"><span>PERFECT MATCH</span></h2>
                                            </div>

                                            <div class="opm-text-box-round">
                                                <p><strong>SIE :</strong><?php echo $reg3_txtshe; ?></p>
                                            </div>
                                            <div class="opm-text-box-roundsec">
                                                <p><strong>ER :</strong><?php echo $reg3_txthe; ?></p> 
                                            </div>
                                            <div class="opm-text-box-roundsec">
                                                <p><strong>BEIDE :</strong><?php echo $reg3_txtboth; ?></p> 
                                            </div>
                                        </div>

                                    <?php } ?>	



                                    <?php if ($get_user_info['user_type']['0'] == 'premium' || $get_user_info['user_type']['0'] == 'validated') { ?>
                                        <div class="eropic-profile-Part">
                                            <div class=""> 
                                                <h3 class="eropic-h3 no-margin no-padding col-sm-12 col-md-12"><span>His erotic profile</span><a href ="<?php echo get_permalink(245); ?>" class="editLink"   id="edit">edit</a><a class="profile-her-erotic-right-btn" href="#">E </a></h3>
                                                
                                            </div>

                                            <?php
                                            $count1 = 0;

                                            for ($i = 0; $i < count($content1['subject']); $i++) {
                                                $color1 = $content1['id'][$count1];
                                                $textColor[$color1];
                                                if ($status1[$count1] == 0) {
                                                    $text1 = '(A)';
                                                } else if ($status1[$count1] == 1) {
                                                    $text1 = '(P)';
                                                } elseif ($status1[$count1] == 2) {
                                                    $text1 = "";
                                                }
                                                if ($status1[$count1] == 0) {
                                                    $bgColor1 = "";
                                                } else if ($status1[$count1] == 1) {
                                                    $bgColor1 = "";
                                                } elseif ($status1[$count1] == 2) {
                                                    $bgColor1 = "red";
                                                }
                                                ?>				
                                                <div class="eropic-box-round" style="background-color:<?php echo ($bgColor1 != "") ? $bgColor1 : $textColor[$color1]; ?>">
                                                    <p><strong class="up-case"><?php echo $content1['desc'][$i]; ?> :</strong><?php echo $content1['subject'][$i] . "&nbsp&nbsp&nbsp" . $text1; ?></p>
                                                </div>
                                                <?php
                                                $count1++;
                                            }
                                            ?>


                                        </div>
                                    <?php } else { ?>
                                        <div class="eropic-profile-Part invalid">
                                            <span class="grayTrans-more"></span>
                                            <h3 class="eropic-h3 no-margin no-padding col-sm-12 col-md-12">
                                                <span class="red">His erotic profile</span>
                                                <a class="profile-her-erotic-right-btn" href="#">E </a>
                                            </h3>

                                            <?php
                                            $count1 = 0;

                                            for ($i = 0; $i < count($content1['subject']); $i++) {
                                                $color1 = $cotent1['id'][$count1];
                                                $textColor[$color1];
                                                if ($status1[$count1] == 0) {
                                                    $text1 = '(A)';
                                                } else if ($status1[$count1] == 1) {
                                                    $text1 = '(P)';
                                                } elseif ($status1[$count1] == 2) {
                                                    $text1 = "";
                                                }
                                                if ($status1[$count1] == 0) {
                                                    $bgColor1 = "";
                                                } else if ($status1[$count1] == 1) {
                                                    $bgColor1 = "";
                                                } elseif ($status1[$count1] == 2) {
                                                    $bgColor1 = "red";
                                                }
                                                ?>				
                                                <div class="eropic-box-round" style="background-color:<?php echo ($bgColor1 != "") ? $bgColor1 : $textColor[$color1]; ?>">
                                                    <p><strong class="up-case"><?php echo $content1['desc'][$i]; ?> :</strong><?php echo $content1['subject'][$i] . "&nbsp&nbsp&nbsp" . $text1; ?></p>
                                                </div>
                                                <?php
                                                $count1++;
                                            }
                                            ?>


                                        </div>

                                    <?php } ?>

                                </div>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <?php /*     * *******************************************Send mail functionality*************************************************** */ ?>

    <style type="text/css">
        .white_content {
            display: none;
            position: absolute;
            top: 25%;
            left: 25%;
            width: 50%;
            height: 50%;
            padding: 13px 15px;
            border: 16px solid orange;
            background-color: white;
            z-index:1008;
            overflow: auto;
            max-height: 500px;
        }
    </style>

    <div id="light" class="white_content">
        <div class="col-full"><a href="javascript:void(0)" onclick="document.getElementById('light').style.display = 'none';
                    document.getElementById('fade').style.display = 'none'" class="close-btn">×</a></div>

        From:<input type ="text" name="from_email" value="<?php echo $current_user->user_email; ?>" id ="from_email"><br>
        <input type="hidden" id ="sender_uid" value="<?php echo $current_user->ID; ?>" name="sender_id"> 
        To:  <?php
        $order = 'user_nicename';
        $users = $wpdb->get_results("SELECT * FROM $wpdb->users ORDER BY $order");
        ?> 
        <select name="to_email" id = "to_email">
            <?php
            foreach ($users as $user) : // start users' profile "loop"

                echo '<option value ="' . $user->user_email . '_' . $user->ID . '">' . $user->user_email . '</option>';
                ?>

                <?php
            endforeach; // end of the users' profile 'loop'
            ?>
        </select>
        Subject:<input type="text" name="email_subject" id ="email_subject"><br>
        Cc     :<input type ="text" name="cc_email" id ="cc_email"><br>
        Content:<textarea name="email_content" id ="email_content">Hi </textarea> <br>

        <button name="send" id ="send">SEND</button>

    </div>
    <script>
        jQuery("#send").click(function () {
            var from_email = $('#from_email').val();
            var to_email = $('#to_email').val();
            var email_subject = $("#email_subject").val();
            var cc_email = $("#cc_email").val();
            var email_content = $("#email_content").val();
            var sender_uid = $("#sender_uid").val();
            //alert(from_email);
            //alert(to_email);
            formData = '&from_email=' + from_email + '&to_email=' + to_email + '&email_subject=' + email_subject + '&cc_email=' + cc_email + '&email_content=' + email_content + '&sender_uid=' + sender_uid;
            $.ajax({
                type: 'POST',
                data: formData,
                url: "<?php echo bloginfo('template_url'); ?>/email_send.php", dataType: "json",
                success: function (result) {
                    // alert(result);
                }
            });
        });

    </script>

    <?php /*     * ****************************************EndSend mail functionality*************************************************** */ ?>




    <?php /*     * *******************************************like to add favorate a kiss mail functionality*************************************************** */ ?>



    <div id="send_a_kiss" class="white_content">
        <div class="col-full"><a href="javascript:void(0)" onclick="document.getElementById('send_a_kiss').style.display = 'none';
                    document.getElementById('fade').style.display = 'none'" class="close-btn">×</a></div>

        From:<input type ="text" name="send_kiss_email" value="<?php echo $current_user->user_email; ?>" id ="send_kiss_email"><br>
        <input type="hidden" id ="send_kiss_uid" value="<?php echo $current_user->ID; ?>" name="send_kiss_uid"> 
        To:  <?php
        $order = 'user_nicename';
        $users = $wpdb->get_results("SELECT * FROM $wpdb->users ORDER BY $order");
        ?> 
        <select name="to_email" id = "to_kissemail">
            <?php
            foreach ($users as $user) : // start users' profile "loop"

                echo '<option value ="' . $user->user_email . '_' . $user->ID . '">' . $user->user_email . '</option>';
                ?>

                <?php
            endforeach; // end of the users' profile 'loop'
            ?>
        </select>
        <?php echo $kiss_image = $upload_dir['baseurl'] . '/lip.jpg'; ?>
        <img class="prof-cartoon-img" src="<?php echo $kiss_image = $upload_dir['baseurl'] . '/lip.jpg'; ?>" style="display:none" id ="kiss_img">
        <input type="hidden" name="send_kiss_uid" value="<?php echo $current_user->ID; ?>">
        <button name="send_kiss" id ="send_kiss">SEND</button>
    </div>
    <script>
        jQuery("#send_kiss").click(function () {
            var send_kiss_email = $('#send_kiss_email').val();
            var to_kissemail = $('#to_kissemail').val();
            var send_kiss_uid = $("#send_kiss_uid").val();
            var srcimg = $("#kiss_img").attr('src');

            //alert(srcimg);
            //alert(to_kissemail);
            formData = '&send_kiss_email=' + send_kiss_email + '&to_kissemail=' + to_kissemail + '&send_kiss_uid=' + send_kiss_uid + '&srcimg=' + srcimg;
            $.ajax({
                type: 'POST',
                data: formData,
                url: "<?php echo bloginfo('template_url'); ?>/send_a_kiss.php",
                success: function (result) {
                    //alert(result);
                }
            });
        });

    </script>

    <?php /*     * ****************************************End Send a kiss mail functionality*************************************************** */ ?>






    <?php /*     * *******************************************like to add favorate*************************************************** */ ?>

    <div id="add_fav" class="white_content">
        <div class="col-full"><a href="javascript:void(0)" onclick="document.getElementById('add_fav').style.display = 'none';
                    document.getElementById('fade').style.display = 'none'" class="close-btn">×</a></div>

        From:<input type ="text" name="send_fav_email" value="<?php echo $current_user->user_email; ?>" id ="send_fav_email"><br>
        <input type="hidden" id ="send_fav_uid" value="<?php echo $current_user->ID; ?>" name="send_fav_uid"> 
        To:  <?php
        $order = 'user_nicename';
        $users = $wpdb->get_results("SELECT * FROM $wpdb->users ORDER BY $order");
        ?> 
        <select name="to_favemail" id = "to_favemail">
            <?php
            foreach ($users as $user) : // start users' profile "loop"

                echo '<option value ="' . $user->user_email . '_' . $user->ID . '">' . $user->user_email . '</option>';
                ?>

                <?php
            endforeach; // end of the users' profile 'loop'
            ?>
        </select>

        <button name="add_fav_button" id ="add_fav_button">SEND</button>
    </div>
    <script>
        jQuery("#add_fav_button").click(function () {
            var send_fav_email = $('#send_fav_email').val();
            var send_fav_uid = $('#send_fav_uid').val();
            var to_favemail = $("#to_favemail").val();
            //alert(send_fav_email);
            //alert(to_favemail);
            formData = '&send_fav_email=' + send_fav_email + '&send_fav_uid=' + send_fav_uid + '&to_favemail=' + to_favemail;
            $.ajax({
                type: 'POST',
                data: formData,
                url: "<?php echo bloginfo('template_url'); ?>/add_favorite.php", dataType: "json",
                success: function (result) {
                    //alert(result);
                }
            });
        });

    </script>

    <?php /*     * ****************************************End like to add favorate*************************************************** */ ?>






    <?php /*     * *******************************************make a note to this profile*************************************************** */ ?>

    <?php /*     * *******************************************make a note to this profile*************************************************** */ ?>

    <div id="make_note" class="white_content">
        <div class="col-full"><a href="javascript:void(0)" onclick="document.getElementById('make_note').style.display = 'none';
                    document.getElementById('fade').style.display = 'none'" class="close-btn">×</a></div>
                                 <?php $myrows = $wpdb->get_results("SELECT note_content FROM wp_make_a_note where user_id = '" . $current_user->ID . "'");
                                 ?>

        <textarea id='editor2'><?php echo $myrows['0']->note_content; ?></textarea>				
        <script>CKEDITOR.replace('editor2');</script>

        <script>
            nanospell.ckeditor('editor2', {
                dictionary: "en", // 24 free international dictionaries  
                server: "php"      // can be php, asp, asp.net or java
            });
        </script>
        <input type="hidden" name="logged_in_user" value="<?php echo $current_user->ID; ?>" id ="logged_in_user">
        <button name="add_make_button" id ="add_make_button">SEND</button>
    </div>
    <script>
        jQuery("#add_make_button").click(function () {
            var editor2 = CKEDITOR.instances['editor2'].getData();
            var logged_in_user = $("#logged_in_user").val();
            //alert(logged_in_user);
            formData = '&editor2=' + editor2 + '&logged_in_user=' + logged_in_user;
            $.ajax({
                type: 'POST',
                data: formData,
                url: "<?php echo bloginfo('template_url'); ?>/make_a_note.php",
                success: function (result) {
                    // alert(result);
                }
            });
        });

    </script>


    <?php /*     * ****************************************End make a note*************************************************** */ ?>




    <?php /*     * ****************************************End make a note*************************************************** */ ?>


    <?php /*     * ****************************************Edit Intro duction*************************************************** */ ?>



    <div class="modal fade" id="edit_intro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="parent" style="display:none;">
            <span class="loader"></span>
        </div>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Form Preview</h4>
                </div>
                <div class="modal-body">
                    <div id="intro_msg" class="successMsg"></div>
                    <div class="editBoth">
                        <textarea id='editor3' ><?php echo $describe; ?></textarea>				
                        <script>CKEDITOR.replace('editor3');</script>

                        <script>
                            nanospell.ckeditor('editor3', {
                                dictionary: "en", // 24 free international dictionaries  
                                server: "php"      // can be php, asp, asp.net or java
                            });
                        </script>


                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" id="hide_msg" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id ="update_profile_intro">Save changes</button>
                </div>
            </div>

        </div>
    </div>
    </div>


    <script>
        jQuery("#update_profile_intro").click(function () {
            jQuery('.parent').show();
            var profile_intro = CKEDITOR.instances['editor3'].getData();
            var logged_in_user = $("#logged_in_user").val();
            intro_data = '&profile_intro=' + profile_intro + '&logged_in_user=' + logged_in_user;
            $.ajax({
                type: 'POST',
                data: intro_data,
                url: "<?php echo bloginfo('template_url'); ?>/update_profile_intro.php",
                success: function (result) {
                    jQuery('.parent').hide();
                    var res = result;
                    var resc = res.split('||');
                    if(resc[1] != undefined || resc[1] != null || resc[1] != ""){
                        jQuery("#intro_msg").html(resc[0]);
                        jQuery("#intro_data").html(resc[1]);
                    }

                    if(resc[1] == undefined) {
                        jQuery("#intro_msg").html('Update successfully');
                    }
                    setTimeout(function(){jQuery('#intro_msg').fadeOut(2000);}, 3000);
                    
                }
            });
        });

        jQuery('#hide_msg').click(function(){
                $('#intro_msg').hide();
        });

    </script>

    <?php /*     * ****************************************End edit Introduction*************************************************** */ ?>




    <?php /*     * ****************************************Fav quote section*************************************************** */ ?>



    <div class="modal fade" id="fav_quote" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="parent" style="display:none;">
            <span class="loader"></span>
        </div>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" id="close_quoto_msg" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">OUR FAVOURITE QUOTE</h4>
                </div>
                <div class="modal-body">
                    <div id ="fav_quote_msg" class="successMsg"></div>	
                    <div class="editBoth">
                        <textarea id='editor4'><?php echo $fav_quote_content; ?></textarea>				
                        <script>CKEDITOR.replace('editor4');</script>

                        <script>
                            nanospell.ckeditor('editor3', {
                                dictionary: "en", // 24 free international dictionaries  
                                server: "php"      // can be php, asp, asp.net or java
                            });
                        </script>


                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" id="close_quoto" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id ="update_fav_section">Save changes</button>
                </div>
            </div>

        </div>
    </div>
    </div>

    <script>
        jQuery("#update_fav_section").click(function () {
        	jQuery('.parent').show();
            var quote_content = CKEDITOR.instances['editor4'].getData();
            var logged_in_user = $("#logged_in_user").val();
            intro_data = '&quote_content=' + quote_content + '&logged_in_user=' + logged_in_user;
            $.ajax({
                type: 'POST',
                data: intro_data,
                dataType:'html',
                url: "<?php echo bloginfo('template_url'); ?>/update_fav_quote.php",
                success: function (result) {
                	jQuery('.parent').hide();
                    if(result == ""){                        
                        $('p.fav-para').next().html(result);
                        jQuery("#fav_quote_msg").html("Your favourite content has been successfully updated.");

                    }else{
                        jQuery('#fav_quote_msg').show();
                         $('p.fav-para').next().html(result);
                        jQuery("#fav_quote_msg").html("Your favourite content has been successfully updated.");
                    }
                    setTimeout(function(){jQuery('#fav_quote_msg').fadeOut(2000);}, 3000);
                    
                }
            });
        });

jQuery('#close_quoto').click(function(){
    jQuery('#fav_quote_msg').hide();

});

jQuery('#close_quoto_msg').click(function(){
    jQuery('#fav_quote_msg').hide();

});


    </script>

    <?php /*     * ****************************************End Fav quote section*************************************************** */ ?>




    <?php /*     * ****************************************Edit Perfect match section*************************************************** */ ?>



    <div class="modal fade" id="edit_perfect_match" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
       <div class="parent" style="display:none;">
            <span class="loader"></span>
        </div>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" id="show_she_successMsg1" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Form Preview</h4>
                </div>
                <div class="modal-body" >
                 <div id="show_successMsg" class="successMsg"></div>
                    <div class="editShe">
                        <h2>She</h2>

                        <textarea id='editor5'><?php echo $reg3_txtshe; ?></textarea>				
                        <script>CKEDITOR.replace('editor5');</script>

                        <script>
                            nanospell.ckeditor('editor5', {
                                dictionary: "en", // 24 free international dictionaries  
                                server: "php"      // can be php, asp, asp.net or java
                            });
                        </script>

                        <input type="hidden" name="her_match_content" id="her_match_content" value="to_her">
                        <div class="modal-footer">
                            <button type="button" id="show_she_successMsg" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary" id ="update_her_perfect">Save changes</button>
                        </div>
                    </div>
                    <div class="editHe">
                    <div id="showsuccessMsg" class="successMsg"></div>
                        <h2>He</h2>
                        <textarea id='editor6'><?php echo $reg3_txthe; ?></textarea>				
                        <script>CKEDITOR.replace('editor6');</script>

                        <script>
                            nanospell.ckeditor('editor6', {
                                dictionary: "en", // 24 free international dictionaries  
                                server: "php"      // can be php, asp, asp.net or java
                            });
                        </script>

                        <input type="hidden" name="his_match_content" id="his_match_content" value="to_his">	
                        <div class="modal-footer">
                            <button type="button" id="hide_hisdetails" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary" id ="update_his_perfect">Save changes</button>
                        </div>
                    </div>
                    <div class="editBoth">
                    <div id="msg_for_both" class="successMsg"></div>                    
                        <h2>Both</h2>					
                        <textarea id='editor7'><?php echo $reg3_txtboth; ?></textarea>				
                        <script>CKEDITOR.replace('editor7');</script>

                        <script>
                            nanospell.ckeditor('editor7', {
                                dictionary: "en", // 24 free international dictionaries  
                                server: "php"      // can be php, asp, asp.net or java
                            });
                        </script>					
                        <input type="hidden" name="both_match_content" id="both_match_content" value="to_both">
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" id="hide_his_details" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id ="update_both_section">Save changes</button>
                </div>
            </div>

        </div>
    </div>
    </div>

    <script>
        jQuery("#update_her_perfect").click(function () {
            jQuery('.parent').show();
            var editor5 = CKEDITOR.instances['editor5'].getData();
            var logged_in_user = $("#logged_in_user").val();
            var to_whome = $("#her_match_content").val();
            intro_data = '&editor5=' + editor5 + '&logged_in_user=' + logged_in_user + '&to_whome=' + to_whome;
            $.ajax({
                type: 'POST',
                data: intro_data,
                url: "<?php echo bloginfo('template_url'); ?>/update_perfect_match.php",
                success: function (result) {
                    jQuery('.parent').hide();
                    jQuery('#show_successMsg').html("Update Successfully");
                    setTimeout(function(){jQuery('#show_successMsg').fadeOut(2000);}, 3000);
                }
            });
        });

        
        jQuery('#show_she_successMsg1').click(function(){
            jQuery('#show_successMsg').hide();
            jQuery('#showsuccessMsg').hide();
            jQuery('#msg_for_both').hide();

        });
    </script>


    <script>
        jQuery("#update_his_perfect").click(function () {
            jQuery('.parent').show();
            var editor6 = CKEDITOR.instances['editor6'].getData();
            var logged_in_user = $("#logged_in_user").val();
            var to_whome = $("#his_match_content").val();
            intro_data = '&editor6=' + editor6 + '&logged_in_user=' + logged_in_user + '&to_whome=' + to_whome;
            $.ajax({
                type: 'POST',
                data: intro_data,
                url: "<?php echo bloginfo('template_url'); ?>/update_perfect_match.php",
                success : function(result){
                    jQuery('#showsuccessMsg').show();
                    jQuery('.parent').hide();
                    jQuery('#showsuccessMsg').html(result);
                    setTimeout(function(){jQuery('#showsuccessMsg').fadeOut(2000);}, 3000);
                }
            });
        });

        </script>



    <script>
        jQuery("#update_both_section").click(function () {
            jQuery('.parent').show();
            var editor7 = CKEDITOR.instances['editor7'].getData();
            var logged_in_user = $("#logged_in_user").val();
            var to_whome = $("#both_match_content").val();
            intro_data = '&editor7=' + editor7 + '&logged_in_user=' + logged_in_user + '&to_whome=' + to_whome;
            $.ajax({
                type: 'POST',
                data: intro_data,
                url: "<?php echo bloginfo('template_url'); ?>/update_perfect_match.php",
                success: function (result) {
                    jQuery('div.parent').hide();
                    jQuery('#msg_for_both').html(result);
                    setTimeout(function(){jQuery('#msg_for_both').fadeOut(2000);}, 3000);
                }
            });
        });

    </script>



    <?php /*     * ****************************************End Perfect match section*************************************************** */ ?>

    <?php /*     * ****************************************Speed dating*************************************************** */ ?>



    <div class="modal fade" id="speed_dating" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
         <div class="parent" style="display:none;">
            <span class="loader"></span>
        </div>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" id="close_showMsg" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Form Preview</h4>
                </div>
                <div class="modal-body">
                    <div id ="speed_msg" class="successMsg"></div>
                    <div class="editBoth">
                        <textarea id='editor8'><?php echo $get_user_info['speed_data']['0']; ?></textarea>				
                        <script>CKEDITOR.replace('editor8');</script>

                        <script>
                            nanospell.ckeditor('editor8', {
                                dictionary: "en", // 24 free international dictionaries  
                                server: "php"      // can be php, asp, asp.net or java
                            });
                        </script>
                        <input type="hidden" name="speed_dating" id="speed_date" value ="speed_date">
                        <div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" id="close_update_speed" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary" id ="update_speed_dating">Save changes</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <script>
        $('#her_dob').DatePickerGetDate(formated);
    </script>
    <script>
        jQuery("#update_speed_dating").click(function () {
            jQuery('.parent').show();
            var editor8 = CKEDITOR.instances['editor8'].getData();
            var logged_in_user = $("#logged_in_user").val();
            var text_box_round = $("#speed_date").val();
            intro_data = '&editor8=' + editor8 + '&logged_in_user=' + logged_in_user + '&text_box_round=' + text_box_round;
            $.ajax({
                type: 'POST',
                data: intro_data,
                url: "<?php echo bloginfo('template_url'); ?>/update_speed_travel.php",
                success: function (result) {
                    jQuery('.parent').hide();
                    if(result == ""){
                        jQuery("#speed_msg").html(result);
                    }else{
                    jQuery("#speed_msg").html("Update Successfully");
                    }
                    setTimeout(function(){jQuery('#speed_msg').fadeOut(2000);}, 3000);
                    
                }
            });
        });

jQuery('#close_update_speed').click(function(){
    jQuery('#speed_msg').hide();

});
jQuery('#close_showMsg').click(function(){
    jQuery('#speed_msg').hide();

});

    </script>





    <?php /*     * ****************************************End Speed dating*************************************************** */ ?>





    <?php /*     * ****************************************Travel Agenda*************************************************** */ ?>



    <div class="modal fade" id="travel_agenda" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="parent" style="display:none;">
            <span class="loader"></span>
        </div>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" id="close_travelMsg" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Travel agenda</h4>
                </div>
                <div class="modal-body">
                    <div id ="travel_msg" class="successMsg"></div>
                    <div class="editBoth">
                        <textarea id='editor9'><?php echo $get_user_info['travel_agent']['0']; ?></textarea>				
                        <script>CKEDITOR.replace('editor9');</script>

                        <script>
                            nanospell.ckeditor('editor9', {
                                dictionary: "en", // 24 free international dictionaries  
                                server: "php"      // can be php, asp, asp.net or java
                            });
                        </script>
                        <input type="hidden" name="travel_agend" id="travel_agend12" value ="travel_agend">

                    </div>
                    <div class="modal-footer">
                        <button type="button" id="hideMsg" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id ="update_travel_agenda">Save changes</button>
                    </div>
                </div>


            </div>

        </div>
    </div>
    </div>

    <script>
        jQuery("#update_travel_agenda").click(function () {
            jQuery('.parent').show();
            var editor9 = CKEDITOR.instances['editor9'].getData();
            var logged_in_user = $("#logged_in_user").val();
            var text_box_round = $("#travel_agend12").val();
            intro_data = '&editor9=' + editor9 + '&logged_in_user=' + logged_in_user + '&text_box_round=' + text_box_round;
            $.ajax({
                type: 'POST',
                data: intro_data,
                url: "<?php echo bloginfo('template_url'); ?>/update_speed_travel.php",
                success: function (result) {
                    jQuery('.parent').hide();
                    if(result == ""){

                    }else{
                        jQuery("#travel_msg").html("Update Successfully");
                    }
                    setTimeout(function(){jQuery('#travel_msg').fadeOut(2000);}, 3000);
                    
                    
                }
            });
        });

        jQuery('#close_travelMsg').click(function(){
            jQuery("#travel_msg").hide();

        });
        jQuery('#hideMsg').click(function(){
            jQuery("#travel_msg").hide();

        });

    </script>





    <?php /*     * ****************************************End Travel Agenda*************************************************** */ ?>





    <?php /*     * ***************************************************Edit basic info*************************************************** */ ?>




    <div class="modal fade" id="edit_basic_info" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="parent" style="display:none;">
            <span class="loader"></span>
        </div>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                <button type="button" id="close_basic_info" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Basic Info</h4>
                </div>
                <div class="modal-body">
                    <div id ="basicmsg" class="successMsg"></div>

                    <!--------------------------------------------------------------------Basic info---------------------------------------------------->

                    <div class="Profile-Part-BI cus_scroll3" id="hidePart1">
                        <h3 class="Proh-h3">BASIC INFO</h3>
                        <div class="row">
                            <p class="Pro-p col-sm-4 col-xs-offset-4">she</p>
                            <p class="Pro-p col-sm-4">he</p> 
                        </div>
                        <div class="padding-basic-info"> 

                            <div class="form-group border-bottom row">
                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Age</label>
                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center bold"><input type="text" value="<?php echo $age1; ?>" name ="age_female" id ="her_dob" style="z-index:9999"></label>
                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center bold"><input type="text" value="<?php echo $age2; ?>" name ="age_male" id ="his_dob"></label>
                            </div>
                            <div class="form-group border-bottom row">
                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Height (cm)</label>

                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center bold">     
                                    <select name="shehight" class="form-control width60" id="height1">
                                        <option value ="select">Select</option>
                                        <?php
                                        for ($h = 1; $h < 181; $h++) {
                                            ?>
                                            <option value =<?php echo $h; ?> <?php if ($her_hight == $h) echo 'selected="selected"'; ?>><?php echo $h; ?> </option>
                                        <?php }
                                        ?>
                                    </select>
                                </label>
                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center bold">
                                    <select name="hehight" class="form-control width60" id="heighthe1">
                                        <option value ="select">Select</option>
                                        <?php
                                        for ($a = 1; $a < 181; $a++) {
                                            ?>
                                            <option value =<?php echo $a; ?> <?php if ($his_hight == $a) echo 'selected="selected"'; ?>><?php echo $a; ?> </option>
                                        <?php } ?>
                                    </select>
                                </label>
                            </div>
                            <div class="form-group border-bottom row">
                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Figure</label>
                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center">

                                    <select name="her_figure" class="form-control" id="her_figure">
                                        <option value ="select">Select</option>
                                        <option value = "slim" <?php if ($her_figure == "slim") echo 'selected="selected"'; ?>>slim</option>
                                        <option value = "normal (BMI 18,5 – 24,5)" <?php if ($her_figure == "normal (BMI 18,5 – 24,5)") echo 'selected="selected"'; ?>>normal (BMI 18,5 – 24,5)</option>
                                        <option value = "slight overweight" <?php if ($her_figure == "slight overweight") echo 'selected="selected"'; ?>>slight overweight</option>
                                        <option value = "voluptuous" <?php if ($her_figure == "voluptuous") echo 'selected="selected"'; ?>>voluptuous</option>
                                        <option value = "plump" <?php if ($her_figure == "plump") echo 'selected="selected"'; ?>>plump</option>
                                        <option value = "fat" <?php if ($her_figure == "fat") echo 'selected="selected"'; ?>>fat</option>                                        
                                    </select>

                                </label>
                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center">

                                    <select name="his_figure" class="form-control" id="his_figure">
                                        <option value ="select">Select</option>
                                        <option value = "very slim" <?php if ($his_figure == "very slim") echo 'selected="selected"'; ?>>very slim</option>
                                        <option value = "slim" <?php if ($his_figure == "slim") echo 'selected="selected"'; ?>>slim</option>
                                        <option value = "normal (BMI 18,5 – 24,5)" <?php if ($his_figure == "normal (BMI 18,5 – 24,5)") echo 'selected="selected"'; ?>>normal (BMI 18,5 – 24,5)</option>
                                        <option value = "slight overweight" <?php if ($his_figure == "slight overweight") echo 'selected="selected"'; ?>>slight overweight</option>
                                        <option value = "voluptuous" <?php if ($his_figure == "voluptuous") echo 'selected="selected"'; ?>>voluptuous</option>
                                        <option value = "plump" <?php if ($his_figure == "plump") echo 'selected="selected"'; ?>>plump</option>
                                        <option value = "fat" <?php if ($his_figure == "fat") echo 'selected="selected"'; ?>>fat</option>
                                    </select>
                                </label>
                            </div>
                            <div class="form-group border-bottom row">
                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Smoking</label>
                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center">
                                    <select name="her_smoke" class="form-control" id="non_smokershe">
                                        <option value ="select">Select</option>		  
                                        <option value ="non smoker" <?php if ($her_smoke == "non smoker") echo 'selected="selected"'; ?>>non smoker</option>
                                        <option value="ocassional smoker" <?php if ($her_smoke == "ocassional smoker") echo 'selected="selected"'; ?>>ocassional smoker</option>
                                        <option value="smoker" <?php if ($her_smoke == "smoker") echo 'selected="selected"'; ?>>smoker</option>
                                        <option value="heavy smoker" <?php if ($her_smoke == "heavy smoker") echo 'selected="selected"'; ?>>heavy smoker</option>
                                    </select>
                                </label>

                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center">

                                    <select name="her_smoke" class="form-control" id="non_smokerhe">
                                        <option value ="select">Select</option>		  
                                        <option value ="non smoker" <?php if ($his_smoke == "non smoker") echo 'selected="selected"'; ?>>non smoker</option>
                                        <option value="ocassional smoker" <?php if ($his_smoke == "ocassional smoker") echo 'selected="selected"'; ?>>ocassional smoker</option>
                                        <option value="smoker" <?php if ($his_smoke == "smoker") echo 'selected="selected"'; ?>>smoker</option>
                                        <option value="heavy smoker" <?php if ($his_smoke == "heavy smoker") echo 'selected="selected"'; ?>>heavy smoker</option>
                                    </select>


                                </label>
                            </div>

                            <div class="form-group border-bottom row">
                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Hair color</label>
                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center">

                                    <select name="her_haircolor" class="form-control" id="her_haircolor">
                                        <option>Select</option>
                                        <option value="blond">blond</option>
                                        <option value="black">black</option>
                                        <option value="brown">brown</option>
                                    </select>

                                </label>
                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center">

                                    <select name="his_haircolor" class="form-control" id="his_haircolor">
                                        <option>Select</option>
                                        <option value="blond">blond</option>
                                        <option value="black">black</option>
                                        <option value="brown">brown</option>
                                    </select>
                                </label>
                            </div>


                            <div class="form-group border-bottom row">
                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Shaved</label>
                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center">
                                    <select name="her_shave" class="form-control" id="her_shave">
                                        <option value ="select">Select</option>
                                        <option value ="Yes" <?php if ($her_shave == "Yes") echo 'selected="selected"'; ?>>Yes</option>
                                        <option value="No" <?php if ($her_shave == "No") echo 'selected="selected"'; ?>>No</option>
                                    </select>

                                </label>
                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center">
                                    <select name="his_shave" class="form-control" id="her_shave">
                                        <option value ="select">Select</option>
                                        <option value ="Yes" <?php if ($his_shave == "Yes") echo 'selected="selected"'; ?>>Yes</option>
                                        <option value="No" <?php if ($his_shave == "No") echo 'selected="selected"'; ?>>No</option>
                                    </select>
                                </label>
                            </div>
                            <div class="form-group border-bottom row">
                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Piercings / tattoos</label>
                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center">
                                    <select name="her_tattoos" class="form-control" id="her_tattoos">
                                        <option value ="select">Select</option>
                                        <option value ="Yes" <?php if ($her_tattoos == "Yes") echo 'selected="selected"'; ?>>Yes</option>
                                        <option value="No" <?php if ($her_tattoos == "No") echo 'selected="selected"'; ?>>No</option>
                                    </select>
                                </label>
                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center">

                                    <select name="his_tattoos" class="form-control" id="his_tattoos">
                                        <option value ="select">Select</option>
                                        <option value ="Yes" <?php if ($his_tattoos == "Yes") echo 'selected="selected"'; ?>>Yes</option>
                                        <option value="No" <?php if ($his_tattoos == "No") echo 'selected="selected"'; ?>>No</option>
                                    </select>
                                </label>
                            </div>
                            <?php $count = 1; $sum =1; for($i=0;$i<4;$i++){ ?>
                            <div class="form-group border-bottom row">
                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label"><?php echo $i + 1; ?>. Languages</label>
                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center">
                                    <select name="her_language[]" class="form-control" id="langshe<?php echo $count++ ; ?>">
                                        <option value ="select">Select</option>		  
                                        <option value ="English" <?php if ($language_array[$i] == "English") echo 'selected="selected"'; ?>>English</option>
                                        <option value ="German" <?php if ($language_array[$i] == "German") echo 'selected="selected"'; ?>>German</option> 
                                        <option value ="French" <?php if ($language_array[$i] == "French") echo 'selected="selected"'; ?>>French</option> 
                                        <option value ="Italian" <?php if ($language_array[$i] == "Italian") echo 'selected="selected"'; ?>>Italian</option> 
                                        <option value ="Spanish" <?php if ($language_array[$i] == "Spanish") echo 'selected="selected"'; ?>>Spanish</option> 
                                        <option value ="Dutch" <?php if ($language_array[$i] == "Dutch") echo 'selected="selected"'; ?>>Dutch</option>  
                                    </select>
                                </label>
                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center">
                                    <select name="his_language[]" class="form-control" id="langhe<?php echo $sum++ ; ?>">
                                        <option value ="select">Select</option>		  
                                        <option value ="English" <?php if ($language_array1[$i] == "English") echo 'selected="selected"'; ?>>English</option>
                                        <option value ="German" <?php if ($language_array1[$i] == "German") echo 'selected="selected"'; ?>>German</option> 
                                        <option value ="French" <?php if ($language_array1[$i] == "French") echo 'selected="selected"'; ?>>French</option> 
                                        <option value ="Italian" <?php if ($language_array1[$i] == "Italian") echo 'selected="selected"'; ?>>Italian</option> 
                                        <option value ="Spanish" <?php if ($language_array1[$i] == "Spanish") echo 'selected="selected"'; ?>>Spanish</option> 
                                        <option value ="Dutch" <?php if ($language_array1[$i] == "Dutch") echo 'selected="selected"'; ?>>Dutch</option>  
                                    </select>
                                </label>
                            </div>
                            <?php } ?>

                            

                            <div class="form-group border-bottom row">
                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Graduation</label>
                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center">  
                                    <select name="her_graduation" class="form-control" id="graduationshe1">
                                        <option value ="select">Select</option>		  
                                        <option value ="primary school">primary school</option>
                                        <option value="middle school">middle school</option>
                                        <option value="high school">high school</option>
                                        <option value="college">college</option>
                                        <option value="university">university</option>
                                        <option value="masters">masters</option>
                                        <option value="doctorate">doctorate</option>
                                    </select></label>
                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center">  
                                    <select name="his_graduation" class="form-control" id="graduationhe1">
                                        <option value ="select">Select</option>		  
                                        <option value value ="primary school">primary school</option>
                                        <option value="middle school">middle school</option>
                                        <option value="high school">high school</option>
                                        <option value="college">college</option>
                                        <option value="university">university</option>
                                        <option value="masters">masters</option>
                                        <option value="doctorate">doctorate</option>
                                    </select></label>
                            </div>

                            <div class="form-group border-bottom row">
                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Professional fields</label>
                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center">
                                    <select name="her_profession" class="form-control" id="prffessionshe1">
                                        <option value ="select">Select</option>		  
                                        <option value ="Accounting" <?php if ($her_profession == "Accounting") echo 'selected="selected"'; ?>>Accounting</option>
                                        <option value ="Administration" <?php if ($her_profession == "Administration") echo 'selected="selected"'; ?>>Administration</option>
                                        <option value ="Architecture" <?php if ($her_profession == "Architecture") echo 'selected="selected"'; ?>>Architecture</option>
                                        <option value ="Arts" <?php if ($her_profession == "Arts") echo 'selected="selected"'; ?>>Arts </option>
                                        <option value ="Consulting" <?php if ($her_profession == "Consulting") echo 'selected="selected"'; ?>>Consulting </option>
                                        <option value ="Entertainment" <?php if ($her_profession == "Entertainment") echo 'selected="selected"'; ?>>Entertainment</option>
                                        <option value ="Financial services / Banking" <?php if ($her_profession == "Financial services / Banking") echo 'selected="selected"'; ?>>Financial services / Banking</option>
                                        <option value ="Health / Medicine / Pharmacy" <?php if ($her_profession == "Health / Medicine / Pharmacy") echo 'selected="selected"'; ?>>Health / Medicine / Pharmacy</option>
                                        <option value ="Hospitality / Tourism" <?php if ($her_profession == "Hospitality / Tourism") echo 'selected="selected"'; ?>>Hospitality / Tourism</option>
                                        <option value ="Industry / Technique" <?php if ($her_profession == "Industry / Technique") echo 'selected="selected"'; ?>>Industry / Technique</option>
                                        <option value ="Insurance" <?php if ($her_profession == "Insurance") echo 'selected="selected"'; ?>>Insurance</option>
                                        <option value ="Judiciary" <?php if ($her_profession == "Judiciary") echo 'selected="selected"'; ?>>Judiciary</option>
                                        <option value ="Media & PR" <?php if ($her_profession == "Media & PR") echo 'selected="selected"'; ?>>Media & PR</option>
                                        <option value ="Military" <?php if ($her_profession == "Military") echo 'selected="selected"'; ?>>Military</option>
                                        <option value ="Real estate" <?php if ($her_profession == "Real estate") echo 'selected="selected"'; ?>>Real estate</option>
                                        <option value ="Retail / Consumer goods" <?php if ($her_profession == "Retail / Consumer goods") echo 'selected="selected"'; ?>>Retail / Consumer goods</option>
                                        <option value ="Sales & Marketing" <?php if ($her_profession == "Sales & Marketing") echo 'selected="selected"'; ?>>Sales & Marketing</option>
                                        <option value ="Sport / Fitness" <?php if ($her_profession == "Sport / Fitness") echo 'selected="selected"'; ?>>Sport / Fitness</option>
                                        <option value ="Trade" <?php if ($her_profession == "Trade") echo 'selected="selected"'; ?>>Trade</option>
                                        <option value ="Transportation" <?php if ($her_profession == "Transportation") echo 'selected="selected"'; ?>>Transportation</option>
                                        <option value ="others" <?php if ($her_profession == "others") echo 'selected="selected"'; ?>>others</option>
                                    </select>
                                </label>
                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center">

                                    <select name="his_profession" class="form-control" id="prffessionhe1">
                                        <option value ="select">Select</option>		  
                                        <option value ="Accounting" <?php if ($his_profession == "Accounting") echo 'selected="selected"'; ?>>Accounting</option>
                                        <option value ="Administration" <?php if ($his_profession == "Administration") echo 'selected="selected"'; ?>>Administration</option>
                                        <option value ="Architecture" <?php if ($his_profession == "Architecture") echo 'selected="selected"'; ?>>Architecture</option>
                                        <option value ="Arts" <?php if ($his_profession == "Arts") echo 'selected="selected"'; ?>>Arts </option>
                                        <option value ="Consulting" <?php if ($his_profession == "Consulting") echo 'selected="selected"'; ?>>Consulting </option>
                                        <option value ="Entertainment" <?php if ($his_profession == "Entertainment") echo 'selected="selected"'; ?>>Entertainment</option>
                                        <option value ="Financial services / Banking" <?php if ($his_profession == "Financial services / Banking") echo 'selected="selected"'; ?>>Financial services / Banking</option>
                                        <option value ="Health / Medicine / Pharmacy" <?php if ($his_profession == "Health / Medicine / Pharmacy") echo 'selected="selected"'; ?>>Health / Medicine / Pharmacy</option>
                                        <option value ="Hospitality / Tourism" <?php if ($his_profession == "Hospitality / Tourism") echo 'selected="selected"'; ?>>Hospitality / Tourism</option>
                                        <option value ="Industry / Technique" <?php if ($his_profession == "Industry / Technique") echo 'selected="selected"'; ?>>Industry / Technique</option>
                                        <option value ="Insurance" <?php if ($his_profession == "Insurance") echo 'selected="selected"'; ?>>Insurance</option>
                                        <option value ="Judiciary" <?php if ($his_profession == "Judiciary") echo 'selected="selected"'; ?>>Judiciary</option>
                                        <option value ="Media & PR" <?php if ($his_profession == "Media & PR") echo 'selected="selected"'; ?>>Media & PR</option>
                                        <option value ="Military" <?php if ($his_profession == "Military") echo 'selected="selected"'; ?>>Military</option>
                                        <option value ="Real estate" <?php if ($his_profession == "Real estate") echo 'selected="selected"'; ?>>Real estate</option>
                                        <option value ="Retail / Consumer goods" <?php if ($his_profession == "Retail / Consumer goods") echo 'selected="selected"'; ?>>Retail / Consumer goods</option>
                                        <option value ="Sales & Marketing" <?php if ($his_profession == "Sales & Marketing") echo 'selected="selected"'; ?>>Sales & Marketing</option>
                                        <option value ="Sport / Fitness" <?php if ($his_profession == "Sport / Fitness") echo 'selected="selected"'; ?>>Sport / Fitness</option>
                                        <option value ="Trade" <?php if ($his_profession == "Trade") echo 'selected="selected"'; ?>>Trade</option>
                                        <option value ="Transportation" <?php if ($his_profession == "Transportation") echo 'selected="selected"'; ?>>Transportation</option>
                                        <option value ="others" <?php if ($his_profession == "others") echo 'selected="selected"'; ?>>others</option>
                                    </select>
                                </label>
                            </div>
                            

                            <script type="text/javascript" src="<?php echo bloginfo('template_url'); ?>/page-templates/js/jquery.limit-1.2.source.js"></script>
                            <script>
        jQuery(document).ready(function () {
            jQuery('.married').change(function () {
                //alert("dsfds");
                if (jQuery(this).prop('checked')) {
                    jQuery(this).parent().parent().find('input').prop("checked", false);
                    jQuery(this).prop("checked", true);
                }
                else
                {
                    jQuery(this).prop("checked", false);
                }
            });

            
        });
                            </script>

                            
                            <div class="form-group border-bottom row">
                                <label for="inputEmail3" class="col-sm-3 control-label">married <p class="ParaS">(to one another)</p></label>
                                <div class="col-sm-1 no-padding">
                                    <input type="checkbox" class="checkbox_cus married" name="married" value="married" id="married" required="" <?php if ($married == "married") echo 'checked'; ?>>
                                    <label class="checkbox_label" for="married"></label>
                                </div>
                                <label for="inputEmail3" class="col-sm-3 control-label">not married <p class="ParaS">(living together)</p></label>
                                <div class="col-sm-1 no-padding">
                                    <input type="checkbox" class="checkbox_cus married" name="married" value="notmarried" id="notmarried" required="" <?php if ($notmarried == "notmarried") echo 'checked'; ?>>
                                    <label class="checkbox_label" for="notmarried"></label>
                                </div>
                                <label for="inputEmail3" class="col-sm-3 control-label">Separeted</label>
                                <div class="col-sm-1 no-padding">
                                    <input type="checkbox" class="checkbox_cus married" name="married" value="seprated" id="seprated" required="" <?php if ($seprated == "seprated") echo 'checked'; ?>>
                                    <label class="checkbox_label" for="seprated"></label>
                                </div>
                            </div>




                            <div class="form-group border-bottom border0 row">
                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Hobbies & Interests</label>
                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center"><textarea name="her_hobby" id ="her_hobby"><?php echo $her_hobby; ?></textarea></label>
                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center"><textarea name="his_hobby" id ="his_hobby"><?php echo $his_hobby; ?></textarea></label>
                            </div>

                            <div class="form-group border-bottom border0 row">
                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Choose your basic info about you</label>
                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center"><textarea name="about_her" id ="about_her"><?php echo $get_user_info['about_her']['0']; ?></textarea></label>
                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center"><textarea name="about_his" id ="about_his"><?php echo $get_user_info['about_his']['0']; ?></textarea></label>
                            </div>

                           



                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
                <!--------------------------------------------------------------------EndBasic info---------------------------------------------------->

                <div class="modal-footer">
                    <button type="button" id="hide_msgbody" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id ="update_basic_info">Save changes</button>
                </div>
            </div>

        </div>
    </div>
    </div>
    <script>
        jQuery("#update_basic_info").click(function () {
            $('.parent').show();
            var she_dob = $("#her_dob").val();
            var he_dob = $("#his_dob").val();
            var her_hight = $("#height1").val();
            var his_hight = $("#heighthe1").val();
            var her_figure = $("#her_figure").val();
            var his_figure = $("#his_figure").val();
            var logged_in_user = $("#logged_in_user").val();
            var her_smoke = $("#non_smokershe").val();
            var his_smoke = $("#non_smokerhe").val();
            var her_shave = $("#her_shave").val();
            var his_shave = $("#his_shave").val();
            var her_tattoos = $("#her_tattoos").val();
            var his_tattoos = $("#his_tattoos").val();

            var her_language = $("#langshe1").val();
            var her_language1 = $("#langshe2").val();
            var her_language2 = $("#langshe3").val();
            var her_language3 = $("#langshe4").val();

            var his_language = $("#langhe1").val();
            var his_language1 = $("#langhe2").val();
            var his_language2 = $("#langhe3").val();
            var his_language3 = $("#langhe4").val();

            var her_graduation = $("#graduationshe1").val();
            var his_graduation = $("#graduationhe1").val();
            var her_profession = $("#prffessionshe1").val();
            var his_profession = $("#prffessionhe1").val();
            var her_haircolor = $("#her_haircolor").val();
            var his_haircolor = $("#his_haircolor").val();
            var about_her = $("#about_her").val();
            var about_his = $("#about_his").val();
            var relationship = new Array();
            $('input[name="married"]:checked').each(function () {
                relationship.push(this.value);
            });
            //alert(she_dob);
            //alert(he_dob);
			$('#hideuserInfo').hide();
            var her_hobby = $("#her_hobby").val();
            var his_hobby = $("#his_hobby").val();

            intro_data = '&she_dob=' + she_dob + '&logged_in_user=' +
logged_in_user + '&he_dob=' + he_dob + '&her_hight=' + her_hight +
'&his_hight=' + his_hight + '&her_figure=' + her_figure + '&his_figure=' +
his_figure + '&her_smoke=' + her_smoke + '&his_smoke=' + his_smoke +
'&her_shave=' + her_shave + '&his_shave=' + his_shave + '&her_tattoos=' +
her_tattoos + '&his_tattoos=' + his_tattoos + '&her_language=' + her_language
+ '&her_language1=' + her_language1 + '&her_language2=' + her_language2 +
'&her_language3=' + her_language3 + '&his_language=' + his_language +
'&his_language1=' + his_language1 + '&his_language2=' + his_language2 +
'&his_language3=' + his_language3 + '&her_graduation=' + her_graduation +
'&his_graduation=' + his_graduation + '&her_profession=' + her_profession +
'&his_profession=' + his_profession + '&relationship=' + relationship +
'&her_hobby=' + her_hobby + '&his_hobby=' + his_hobby + '&her_haircolor=' +
her_haircolor + '&his_haircolor=' + his_haircolor + '&about_her=' + about_her
+ '&about_his=' + about_his + '&type=couple';             $.ajax({
type: 'POST', data: intro_data,                url: "<?php echo
bloginfo('template_url'); ?>/update_basic_info.php",                 success:
function (result) { $('.parent').hide();
setTimeout(function(){jQuery('#basicmsg').fadeOut(2000);}, 3000);
$('#hideuserInfo').show(); $('#basicmsg').show();
$('#hide_msg').show(); $('#hideuserInfo').html(result);
$('#basicmsg').html("Information update successfully");
                    
                }
            });
        });

    </script>





    <?php /*     * ***************************************************End Edit basic info*************************************************** */ ?>




    <?php /*     * *************************************************** Edit edit_main_residence*************************************************** */ ?>


    <div class="modal fade" id="edit_main_residence" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="parent" style="display:none;">
            <span class="loader"></span>
        </div>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" id="close_residence_msg" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Main Residence</h4>
                </div>
                <div class="modal-body">
                    <div id ="country_suc" class="successMsg"></div>
                    <div class="Profile-Part-BI cus_scroll3">
                        <div id ="country_suc" class="msg"></div>
                        <div class="padding-basic-info">						
                        <div class="form-group border-bottom row">
                            <label class="col-sm-4 BasicInfo-label" >COUNTRY:</label>
							<label class="col-sm-4 BasicInfo-label para-center bold"><select id="country" name ="country[]" class="required form-control"></select></label>
                        </div>
                        <div class="form-group border-bottom row">
                            <label class="col-sm-4 BasicInfo-label" for="inputEmail3">REGIO:</label>
							<label class="col-sm-4 BasicInfo-label para-center bold">
							<select name ="state[]" id ="state" class="required form-control"></select>
							</label>
                        </div>
                        <div class="form-group border-bottom row">
                            <script language="javascript">                                                                                                                                                    populateCountries("country", "state");
                            </script>                                                                                                                           <label class="col-sm-4 BasicInfo-label"> NEXT CITY:</label>
							 <label class="col-sm-4 BasicInfo-label para-center bold">
							<input type ="text" name="city_name" id="city_name" class="form-control"  required="" value="<?php echo $city1;  ?>" >
							</label>
							</div>
							<div class="form-group border-bottom row" style="border-bottom:0 none!important;">
                            <label class="col-sm-4 BasicInfo-label">Frequently in:</label>
                            <label class="col-sm-4 BasicInfo-label para-center bold">							
							<textarea name="frequently_in" id="frequently_in"><?php echo $get_user_info['frequently_in']['0']; ?></textarea>
                            <input type="hidden" name="selected_country" id="selected_country" value="<?php echo $main_country; ?>">
                            <input type="hidden" name="selected_region" id="selected_region" value="<?php echo $main_district; ?>">
                            <input type="hidden" name="selected_city" id="selected_city" value="<?php echo $main_city; ?>">
							</label>
                        </div>
						</div>
                    </div>
                    

                <div class="clear"></div>
                </div>

                <div class="modal-footer">
                    <button type="button" id="hide_content" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id ="update_main_address">Save changes</button>
                </div>
            </div>

        </div>
    </div>
    </div>

    <script>
        jQuery("#update_main_address").click(function () {
            $('.parent').show();
            var logged_in_user = $("#logged_in_user").val();
            var country = $("#country").val();
            var state = $("#state").val();
            var city = $("#city_name").val();
            var selected_country = $("#selected_country").val();
            var selected_region = $("#selected_region").val();
            var selected_city = $("#selected_city").val();
            var first_resi = $("#first_add").val();
            var frequently_in = $("#frequently_in").val();
            intro_data = '&country=' + country + '&logged_in_user=' + logged_in_user + '&state=' + state + '&city=' + city + '&selected_country=' + selected_country + '&selected_region=' + selected_region + '&selected_city=' + selected_city + '&first_resi=' + first_resi + '&frequently_in=' + frequently_in;
            $.ajax({
                type: 'POST',
                data: intro_data,
                url: "<?php echo bloginfo('template_url'); ?>/update_user_country.php",
                success: function (result) {
                	setTimeout(function(){jQuery('#country_suc').fadeOut(2000);}, 3000);
                    $('.parent').hide();
                    $('#country_suc').show();
                    jQuery("#country_suc").html(result);
                }
            });
        });

        
    </script>



    <?php /*     * *************************************************** End Edit edit_main_residence*************************************************** */ ?>



    <?php /*     * *************************************************** Edit edit_second_residence*************************************************** */ ?>


    <div class="modal fade" id="edit_second_residence" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="parent" style="display:none;">
            <span class="loader"></span>
        </div>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" id="close_secondResi_msg" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">SECOND RESIDENCE</h4>
                </div>
                <div class="modal-body">
                <div id="second_residence" class="successMsg" ></div>
                    <div class="Profile-Part-BI cus_scroll3">
                    <div class="padding-basic-info">
                        <div class="form-group border-bottom row">
                            <label class="col-sm-4 BasicInfo-label">COUNTRY:</label>
                            <label class="col-sm-4 BasicInfo-label para-center bold">
                            <select id="country_second" name ="country_second[]" class="required form-control"></select>
                            </label>
                        </div>
                        <div class="form-group border-bottom row">
                            <label class="col-sm-4 BasicInfo-label">REGIO:</label>
                            <label class="col-sm-4 BasicInfo-label para-center bold">
                            <select name ="state_second[]" id ="state_second" class="required form-control"></select>
                            </label>
                        </div>
                        <div class="form-group border-bottom row" style="border-bottom: 0 none!important">
                            <script language="javascript">                                                                                                                                                    populateCountries("country_second", "state_second");
                            </script> 
                            <label class="col-sm-4 BasicInfo-label">NEXT CITY:</label> 
                            <label class="col-sm-4 BasicInfo-label para-center bold">
                            <input type ="text" name="second_city_name" id="second_city_name" class="form-control"  required="" >
                            <input type="hidden" name ="sec_resi" value="<?php echo $second_country; ?>" id="sec_resi">
                            <input type="hidden" name ="sec_dist" value="<?php echo $second_district; ?>" id="sec_dist">
                            <input type="hidden" name ="sec_city" value="<?php echo $second_city; ?>" id="sec_city">
                            </label>
                        </div>
                    </div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id ="update_second_address">Save changes</button>
                </div>
            </div>

        </div>
    </div>
    </div>

    <script>
    jQuery("#update_second_address").click(function () {
            jQuery('.parent').show();
            var logged_in_user = $("#logged_in_user").val();
            var second_city_name = $("#second_city_name").val();
            var second_country = $("#country_second").val();
            var state_second = $("#state_second").val();

            intro_data = '&second_city_name=' + second_city_name + '&logged_in_user=' + logged_in_user + '&second_country=' + second_country + '&state_second=' + state_second;
            $.ajax({
                type: 'POST',
                data: intro_data,
                url: "<?php echo bloginfo('template_url'); ?>/update_second_residence.php",
                success: function (result) {
                	setTimeout(function(){jQuery('#second_residence').fadeOut(2000);}, 3000);
                    jQuery('.parent').hide();
                    jQuery('#second_residence').show();
                    jQuery('#second_residence').html("Update Successfully")
                }
            });
        });

    </script>



    <?php /*     * *************************************************** End Edit edit_second_residence*************************************************** */ ?>



    <?php /*     * ***************************************************Edit female Profile Pick*************************************************** */ ?>



    <div class="modal fade" id="edit_female_pick" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Form Preview</h4>
                </div>
                <div class="modal-body">
                    <div id="msg_female" class="successMsg"></div>
                    <form id="uploadimage" action="" method="post" enctype="multipart/form-data">
                        <div id="image_preview" class="imgFrame"><img id="previewing" src="noimage.png" /></div>	

                        <div id="selectImage">
                            <label>Select Your Image</label><br/>
                            <input type="file" name="file" id="file" alt="file" onchange ="showimagepreview2(this)" />                            
                        </div>                   
                    </form>		
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id ="update_female_pick">Save changes</button>
                </div>
                <h4 id='loading' style="display:none;position:absolute;top:50px;left:850px;font-size:25px;">loading...</h4>
                <div id="message"> 			
                </div>


            </div>
        </div>
    </div>

    <script>
        jQuery("#update_female_pick").click(function () {
            var logged_in_user = $("#logged_in_user").val();
            var srcimg = $('#previewing').attr('src');
            //alert(logged_in_user);

            //alert(srcimg);
            intro_data = '&srcimg=' + srcimg + '&logged_in_user=' + logged_in_user;
            $.ajax({
                type: 'POST',
                data: intro_data,
                url: "<?php echo bloginfo('template_url'); ?>/update_feamle_profilepick.php",
                success: function (result) {
                    jQuery("#msg_female").html(result);
                }
            });
        });

    </script>











    <?php /*     * *************************************************** End Edit female pick*************************************************** */ ?>









    <?php /*     * ***************************************************Edit male Profile Pick*************************************************** */ ?>



    <div class="modal fade" id="edit_male_pick" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Form Preview</h4>
                </div>
                <div class="modal-body">
                    <div id="message12" class="successMsg"> 			
                    </div>
                    <form id="uploadimage_male" action="" method="post" enctype="multipart/form-data">
                        <div id="image_preview2"><img id="previewing_male" src="noimage.png" /></div>	

                        <div id="selectImage">
                            <label>Select Your Image</label><br/>
                            <input type="file" name="file" id="file_male" alt="file" onchange="showimagepreview1(this)"/>

                        </div>                   
                    </form>		
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id ="update_male_pick">Save changes</button>
                </div>
                <h4 id='loading' style="display:none;position:absolute;top:50px;left:850px;font-size:25px;">loading...</h4>

            </div>

        </div>
    </div>
    </div>

    <script>
        jQuery("#update_male_pick").click(function () {
            var logged_in_user = $("#logged_in_user").val();
            var srcimg = $('#previewing_male').attr('src');
            alert(logged_in_user);

            //alert(srcimg);
            intro_data = '&srcimg=' + srcimg + '&logged_in_user=' + logged_in_user;
            $.ajax({
                type: 'POST',
                data: intro_data,
                url: "<?php echo bloginfo('template_url'); ?>/update_male_profilepick.php",
                success: function (result) {
                    jQuery("#message12").html(result);
                }
            });
        });

    </script>

    <?php /*     * *************************************************** End Edit male pick*************************************************** */ ?>
    <script type="text/javascript">
        function showimagepreview1(input) {
            if (input.files && input.files[0]) {
                var filerdr = new FileReader();
                filerdr.onload = function (e) {
                    $("#malepick").remove();
                    $('#previewing_male').attr('src', e.target.result);
                }
                filerdr.readAsDataURL(input.files[0]);
            }
        }
    </script>


    <script type="text/javascript">
        function showimagepreview2(input) {
            if (input.files && input.files[0]) {
                var filerdr = new FileReader();
                filerdr.onload = function (e) {
                    $("#malepick").remove();
                    $('#previewing').attr('src', e.target.result);
                }
                filerdr.readAsDataURL(input.files[0]);
            }
        }
    </script>



    <?php
    $results = $wpdb->get_results("SELECT * FROM bg_images");
    $upload_dir = wp_upload_dir();
    $imgPath = $upload_dir['baseurl'] . '/background_images' . '/';
    ?>


    <div class="modal fade" id="change_background" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="parent" style="display:none;">
            <span class="loader"></span>
        </div>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Form Preview</h4>
                </div>
                <div class="modal-body">
                    <div id ="country_suc1" class="successMsg"></div>
                    <div class="col-sm-12">
                        <div id ="country_suc" class="msg"></div>                           
                        <div class="form-group">
                            <?php foreach ($results as $value) { ?> 
                                <a href="javascript:void(0)" class="bg_image" img_id = "<?php echo $value->id; ?>">
                                    <img style="width:150px;" src= "<?php echo $imgPath . $value->img_url; ?>"> </a>
                            <?php } ?> 
                        </div>                        
                    </div>
                    <div class="clear"></div>

                </div>

                <div class="modal-footer">
                    <button type="button" id="hide_image_msg" class="btn btn-default" data-dismiss="modal">Close</button>
                    
                </div>
            </div>

        </div>
    </div>
    </div>


    <?php
} else {
    echo 'You must be loggedin to access this page';
}
?>

<script>
    $(document).ready(function () {

        $('#hide_image_msg').click(function(){
            $('#country_suc1').hide();

        });
        $('#hide_content').click(function(){
            $('#country_suc').hide();

        });

        $('a.bg_image').on('click', function () {
            jQuery('.parent').show();
            var img = $(this).attr('img_id');           

            $.ajax({
                type: 'POST',
                data: 'img=' + img,
                url: '<?php bloginfo("template_url"); ?>/change_bg_image.php',
                success: function (data) {
                    jQuery('.parent').hide();
                    data = data.replace(/\s+/g, '');
                    var img = '<?php echo $imgPath; ?>' + data;
                    $('#country_suc1').html('Background image changed!');
                    $('#introduction_part').css('background-image', 'url("' + img + '")');
                    setTimeout(function(){jQuery('#country_suc1').fadeOut(2000);}, 3000);

                }

            });

        });

    });

</script>
<script>

    (function ($) {
        $.noConflict();
            $(window).load(function () {
				
				
                $(".cus_scroll").mCustomScrollbar({
                    theme: "light-2"
                });
                $(".cus_scrolln").mCustomScrollbar({
                    theme: "light-2"
                });

                $("#content-1").mCustomScrollbar({   
                    theme: "light-2"
                });
            });
        })(jQuery);
    </script>
    


<?php get_footer(); ?>


