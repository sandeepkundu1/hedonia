<?php
/*
 * Template Name: User HomePage 
 * Description: A Page Template show the user profile homepage outside.
 * Author Name : Sanjay Shinghania
 */

get_header();
$user_type = get_user_meta($current_user->ID, 'user_type', true);

?>
<?php
if (is_user_logged_in() ) {
    ?>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            //Album ON homepage
            jQuery('#ok').click(function () {
                var userID = jQuery("#userid").val();
                var final = [];
                jQuery('.alb_check:checked').each(function () {
                    var values = jQuery(this).val();
                    final.push(values);
                });
                var erotic = jQuery('#erotic:checked').val();
                if (final.length > 0 || erotic !== 'undefined')
                {
                    jQuery(".al_img").css("display", "block");
                    jQuery.ajax({
                        url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
                        data: 'action=my_action_homepage&user_id=' + userID + '&album=' + final + '&erotic_pre=' + erotic,
                        type: 'POST',
                        dataType: 'html',
                        success: function (response) {
                            jQuery("#update").html(response);
                            jQuery("#update").addClass('alert alert-success');
                            jQuery("#update").show();
                            setTimeout(function () {
                                jQuery('#update').fadeOut(2000);
                            }, 3000);
                        },
                        complete: function ()
                        {
                            jQuery(".al_img").css("display", "none");
                        }
                    });
                } else
                {
                    jQuery("#update").html('Please select at least one ..!');
                    jQuery("#update").addClass('alert alert-danger');
                    jQuery("#update").show();
                    setTimeout(function () {
                        jQuery('#update').fadeOut(2000);
                    }, 3000);
                }
            });
            // Activate Link
            jQuery('#act-link').click(function () {
                var userID = jQuery("#userid").val();
                jQuery(".al_img").css("display", "block");
                jQuery.ajax({
                    url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
                    data: 'action=my_action_activate_link&user_id=' + userID,
                    type: 'POST',
                    dataType: 'html',
                    success: function (response) {
                        jQuery("#link-pass").html(response);
                    },
                    complete: function ()
                    {
                        jQuery(".al_img").css("display", "none");
                    }
                });
            });
        });
    </script>
    <script type="text/javascript">
        function select_url(el)
        {
            if (window.getSelection && document.createRange) {
                // IE 9 and non-IE
                var range = document.createRange();
                range.selectNodeContents(el);
                var sel = window.getSelection();
                sel.removeAllRanges();
                sel.addRange(range);
            } else if (document.body.createTextRange) {
                // IE < 9
                var textRange = document.body.createTextRange();
                textRange.moveToElementText(el);
                textRange.select();
            }
        }
    </script>

    <div id="main-content" class="main-content">    
        <div id="primary" class="content-area">
            <div id="content" class="contHome" role="main">
                <?php
                // Start the Loop.
                while (have_posts()) : the_post();
                    ?>
                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                        <?php
                        the_title('<h1 class="home-title">', '</h1><!-- .entry-header -->');
                        ?>
                        <div class="entry-content profiel-staus">
                            <?php echo ($operational_setting[11]->$user_type == 0) ? '<span class="grayTrans"></span>' : ''; ?>
                            <div class="top_part">
                                <?php
                                global $wpdb;
                                $user_id = get_current_user_id();
                                $data = get_user_meta($user_id);
                                $album = unserialize(get_user_meta($user_ID, 'album_shows_case', true));
                                $erotic = get_user_meta($user_ID, 'erotic_shows_case', true);
                                ?>
                                <?php the_content(); ?>
                                <form method="POST" action="" onsubmit="return false;">
                                    <div class="panel panel-default">
                                        <div class="row  form-group">
                                            <div class="col-lg-12">
                                                <?php
                                                $results = $wpdb->get_results("SELECT * FROM user_create_album WHERE user_id = '$user_id'");
                                                if ($results) {
                                                    foreach ($results as $value) {
                                                        ?>
                                                        <div class="col-lg-2 text-center">
                                                            <div class="input-group">
                                                                <span class=""> 
                                                                    <?php echo $value->album_name; ?><br/>
                                                                    <input  type="checkbox" id="al-<?php echo $value->id; ?>" style="display: none;" value="<?php echo $value->id; ?>" class="form-control alb_check checkbox_cus" <?php
                                                                    if (in_array($value->id, $album)) {
                                                                        echo 'checked';
                                                                    }
                                                                    ?>>                                                                            
                                                                    <label class="checkbox_label" for="al-<?php echo $value->id; ?>"></label>
                                                                </span>
                                                            </div>
                                                        </div><!-- /.col-lg-2 -->
                                                        <?php
                                                    }
                                                } else {
                                                    echo 'No Album';
                                                }
                                                ?>
                                            </div>
                                        </div><!-- /.row -->
                                        <div class="row">                                        
                                            <div class="col-lg-8">
                                                Show your erotic preferences externally
                                            </div>
                                            <div class="col-lg-2">
                                                <div class="input-group">
                                                    <input style="display: none;"  type="checkbox" <?php
                                                    if ($erotic == 'erotic preferences') {
                                                        echo 'checked';
                                                    }
                                                    ?> id="erotic" value="  erotic preferences" class="form-control checkbox_cus">
                                                    <label class="checkbox_label" for="erotic"></label>
                                                </div>
                                            </div><!-- /.col-lg-2 -->
                                            <div class="col-lg-2">
                                                <div class="input-group">
                                                    <button type="submit" class="btn label-success" id="ok">OK</button>
                                                </div>
                                            </div><!-- /.col-lg-2 -->
                                            <input type="hidden" name="userID" id="userid" value="<?php echo $user_id; ?>">
                                        </div>
                                       
                                    </div>
                                </form>
                                <div class="row">
                                    <div class="col-lg-5 col-sm-offset-5">
                                        <div class="input-group">
                                            <button type="submit" class="btn label-danger" id="act-link">Activate link</button>
                                        </div>
                                    </div><!-- /.col-lg-3 -->
                                    <div style="display:none;" class="al_img text-center">
                                        <img width="60px" height="12px" src="<?php echo get_template_directory_uri(); ?>/images/ajax-status.gif">
                                    </div>
                                </div>
                            </div>
                            <div class="bot-part">
                                <?php
                                    $idendcode = urlencode('/?member='.$user_id);
                                    $url = get_page_link(529).$idendcode;
                                ?>
                                <div id="link-pass">
                                    <div class="row form-group">                                                                 
                                        <label class="col-sm-3">Link to your homepage</label>
                                        <span class="label-warning col-sm-6" id="share_link"><?php echo $url; ?></span>
                                        <a href="javascript:void(0);" class="col-sm-3" onclick="select_url(document.getElementById('share_link'))">copy link</a>                                                                                                   
                                    </div>
                                    <div class="row form-group">                                                                 
                                        <label class="col-sm-3">Password</label>
                                        <span class="label-warning col-sm-6" id="share_pass"><?php echo get_user_meta($user_id ,'activate_password' ,true); ?></span>
                                        <a href="javascript:void(0);" class="col-sm-3" onclick="select_url(document.getElementById('share_pass'))">copy password</a>                                                                                                    
                                    </div> 
                                    <div class="clear"></div>
                                </div>
                                <div id="update"></div>
                            </div>
                            <div class="clear"></div>
                        </div><!-- .entry-content -->
                    </article><!-- #post-## -->
                    <?php
                endwhile;
                ?>
            </div><!-- #content -->
        </div><!-- #primary -->
    </div><!-- #main-content -->

    <?php
} else {
    wp_redirect(home_url());
}
?>


<?php get_footer(); ?>