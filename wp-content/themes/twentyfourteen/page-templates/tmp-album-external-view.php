<?php
/*
 * Template Name: Album External View 
 * Description: A Page Template show the Member profile homepage.
 * Author Name : Sanjay Shinghania
 */

//get_header(); 
global $wpdb;
$upload_dir = wp_upload_dir();

?>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width">
        <title><?php wp_title('|', true, 'right'); ?></title>
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
        <!--[if lt IE 9]>
        <script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
        <![endif]-->
        <link rel="shortcut icon" href="<?php echo bloginfo('template_url'); ?>/images/favicon.ico" type="image/x-icon" />
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.css">
        <link href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo get_template_directory_uri(); ?>/css/fonts.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo get_template_directory_uri(); ?>/css/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/style.css">
        <link href="<?php echo get_template_directory_uri(); ?>/css/autoCompleteStyle.css" rel="stylesheet" type="text/css"/>	
        <script src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.11.1.min.js" type="text/javascript"></script>			
        <script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.js" type="text/javascript"></script>

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
        <script type="text/javascript" src="http://ajax.microsoft.com/ajax/jQuery.Validate/1.6/jQuery.Validate.min.js"></script>
        <script type="text/javascript" src="<?php echo bloginfo('template_url'); ?>/js/browser_and_os_classes.js"></script>

<!--script src="<?php //echo content_url() . '/themes/ckeditor/ckeditor.js';  ?>" type="text/javascript"></script-->       
        <script type= "text/javascript" src = "<?php echo bloginfo('template_url'); ?>/js/countries_list.js"></script>
        <script src="<?php echo bloginfo("template_url"); ?>/js/sample-registration-form-validation.js"></script>
 <!--       <script src="https://cdn.ckeditor.com/4.4.5.1/standard/ckeditor.js"></script> -->
        <script src="<?php echo get_site_url(); ?>/wp-content/themes/ckeditor/ckeditor.js"></script>
        <script type="text/javascript" src="<?php echo bloginfo('template_url'); ?>/js/autoload.js"></script>

        <!--- scroll script and style -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.mCustomScrollbar.concat.min.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.mCustomScrollbar.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.mousewheel.min.js" type="text/javascript"></script>
        <script>
       (function ($) {
           $(window).load(function () {
               $(".mCustomScrollbar,.cus_scrolln,.cus_scroll,.eropic-profile-Part,.Favourite-Part2,#content-1,.Favourite-Part,.text-box-round").mCustomScrollbar({
                   mouseWheel: {scrollAmount: 300}
               });
           });
       })(jQuery);
        </script>
                        <div class="">
                            <a href="<?php echo home_url(); ?>"><?php echo do_shortcode('[site_logo]'); ?></a>
                        </div>
<div class="albumWrapEx">
    <div class="container">
        <div class="">
            <div class="col-sm-3 albumPic">
            <?php $album_id=$_GET['album'];
                  $user_id=$_GET['user_id'];
                  $imgPath = $upload_dir['baseurl'] . '/users_album/album_' . $user_id . '/'; 
                  $album_images=$wpdb->get_results("SELECT img_url FROM user_album_images WHERE album_id='" . $album_id. "' AND status=1 order by img_order ASC");
            ?>

                <div class="ablbumCum">
                    Picture Album / 
                </div>
                <div class="row form-group">
                <?php foreach ($album_images as $value) 
                { ?>
                    

                    <div class="col-sm-6">
                        <a href="javascript:void(0)" id="select_img" class="select"><img src="<?php echo $imgPath.$value->img_url; ?>" alt="" class="img-responsive"/></a>
                    </div> 
                    <?php } ?>            
                </div>
            </div>
            <div class="col-sm-9">
                <div class="imgViewPoint">
                    <div class="row">
                        <div class="col-sm-8 img_frame">
                            <img src="<?php echo $imgPath.$album_images[0]->img_url; ?><?php /* echo get_template_directory_uri(); ?>/images/validate-process-right-pic.jpg<?php */ ?>" alt="" class="img-responsive"/>
                        </div>
                        <div class="col-sm-4">
                            <div class="pictureText">
                                There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php // get_footer(); ?>
<script>
jQuery(document).ready(function(){
    jQuery('.select').click(function(){
       var img=jQuery(this).html();
       jQuery('.img_frame').html(img);
    })
})
</script>
