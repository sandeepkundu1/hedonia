<?php
/*
 * Template Name: Profile Status
 * Description: A Page Template with a Change Profile Staus.
 * Author Name : Sanjay Shinghania
 */

get_header();
?>

<script type="text/javascript">
    jQuery(document).ready(function () {
        //Deactive Membership
        jQuery('button.deactive-member').click(function () {
            var userID = jQuery(this).attr('id');
            jQuery(".parent").css("display", "block");
            jQuery.ajax({
                url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
                data: 'action=my_action_deactive_member&user_id=' + userID,
                type: 'POST',
                dataType: 'html',
                success: function (response) {
                    jQuery("#update").html(response);
                    jQuery("#update").addClass('alert alert-success');
                    jQuery("#update").show();
                    setTimeout(function () {
                        jQuery('#update').fadeOut(2000);
                    }, 3000);
                    jQuery(".parent").css("display", "none");
                }
            });
        });
        //Reactive Membership
        jQuery('button.reactive-member').click(function () {
            var userID = jQuery(this).attr('id');
            jQuery(".parent").css("display", "block");
            jQuery.ajax({
                url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
                data: 'action=my_action_reactive_member&user_id=' + userID,
                type: 'POST',
                dataType: 'html',
                success: function (response) {
                    jQuery("#update").html(response);
                    jQuery("#update").addClass('alert alert-success');
                    jQuery("#update").show();
                    setTimeout(function () {
                        jQuery('#update').fadeOut(2000);
                    }, 3000);
                    jQuery(".parent").css("display", "none");
                }
            });
        });
        //Delete Membership
        jQuery('button.delete-member').click(function () {
            var userID = jQuery(this).attr('id');
            jQuery(".parent").css("display", "block");
            jQuery.ajax({
                url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
                data: 'action=my_action_delete_member&user_id=' + userID,
                type: 'POST',
                dataType: 'html',
                success: function (response) {
                    jQuery("#update").html(response);
                    jQuery("#update").addClass('alert alert-success');
                    jQuery("#update").show();
                    setTimeout(function () {
                        jQuery('#update').fadeOut(2000);
                    }, 3000);
                    jQuery(".parent").css("display", "none");
                }
            });
        });
    });
</script>
<?php
if (is_user_logged_in()) {
    ?>
    <div class="container">
        <div id="main-content" class="main-content">
            <div id="primary" class="content-area">
                <div id="content" class="contHome def" role="main">
                    <?php
                    // Start the Loop.
                    while (have_posts()) : the_post();
                        ?>
                        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                            <?php
                            the_title('<h1 class="home-title">', '</h1>');
                            ?>
                            <div class="entry-content profiel-staus">
                                <div class="top_part">
                                <?php
                                $user_id = get_current_user_id();
                                $user_type = get_user_meta($user_id, 'user_type', true);
                                $user_date = get_user_meta($user_id, 'user_date', true);
                                ?>
                                     <div class="form-group">
                                    <label class="col-xs-6">
                                        Status of your membership
                                    </label>
                                    <div class="col-xs-6">
                                        <input value="<?php echo ucfirst($user_type) . ' Member'; ?>" class="form-control mem_status"  disabled>
                                    </div>                                
                                </div>
                                    
                                <div class=" form-group">
                                    <label class="col-xs-6">
                                        Membership fee paid untill
                                    </label>
                                    <div class="col-xs-6">
                                        <input value="--/--/20--" class="form-control mem_validity"  disabled>
                                    </div>                                
                                </div>     
                                    <div class="form-group">
                                    <label class="col-xs-5 deactive">
                                        deactivate your membership
                                    </label>
                                    <div class="col-xs-7">
                                        <button id="<?php echo $user_id; ?>" class="deactive-member btn btn-primary pull-right orange-bg">OK</button>
                                    </div>                                
                                </div> 
                                <p>without losing your pictures, validation,erotic profile, contacts, favorites and more.This means </p><p>that your profile becomes inactive and thus completely invisible,although it can be reactivated anytime.</p>
                                <div class="form-group">
                                    <label class="col-xs-5 reactive">
                                        reactivate your membership
                                    </label>
                                    <div class="col-xs-7">
                                        <button id="<?php echo $user_id; ?>" class="reactive-member btn btn-primary pull-right green-bg">OK</button>
                                    </div>                                
                                </div> 
                                 <div class="form-group">
                                    <label class="col-xs-5 delete-lab del">
                                        delete your membership
                                    </label>
                                    <div class="col-xs-7">
                                        <button id="<?php echo $user_id; ?>" class="delete-member btn btn-primary pull-right red-bg">OK</button>
                                    </div>                                
                                </div>
                                <div class="parent" style="display:none;"><span class="loader">loading..</span></div>
                                <div id="update"></div>
                            </div><!-- .entry-content -->
                            <div class="clear"></div>
                        </article><!-- #post-## -->
                        <?php
                    endwhile;
                    ?>
                </div><!-- #content -->
            </div><!-- #primary -->
        </div><!-- #main-content -->
    </div>
    <?php
}else {
    wp_redirect(home_url());
}
?>
<?php get_footer(); ?>