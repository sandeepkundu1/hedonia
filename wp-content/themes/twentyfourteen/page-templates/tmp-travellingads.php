<?php
/* 
* 
* Template Name:Travel dating
* 
*/
get_header();
if ( is_user_logged_in() ) {
?>

<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
<!--<link href="<?php echo get_template_directory_uri(); ?>/js/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css"/>-->
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
<!--<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.form.min.js"></script>-->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.pajinate.js"></script>
<!--<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/fancybox/jquery.fancybox.js"></script>-->


<?php  
$current_user  = wp_get_current_user(); 
$log_user      = $current_user->ID;
?>
<?php   $user_type = get_user_meta($user_ID ,'user_type', true);
if($operational_setting[10]->$user_type != 1) 
{?>
	<div class="container" style="position:relative;" >
	<span class="grayTrans"></span>
<?php } else{ ?>
	<div class="container">
<?php } ?>			
		
						
    <div class="parent_div">
        <div class="row">
             <div class="col-sm-11 dating-margin-main top_part">
            <div class="col-sm-6 no-padding-left">
                <div class="dating-part-main form-horizontal">
                
                    <div class="dating-part">
                        <div class="dating-bg-blue">
                            <h4>SEARCH YOUR TRAVEL  AGENDA</h4>
                            <span class="error_adds" style="display:none;">All Fileds are Required</span>
                        </div>
                        <div class="dating-part-inner1">
                            <div class="dating-inner-head">
                                <div class="dating-bg-gray3">
                                    <h4>WHERE</h4> 
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Country</label>
                                <div class="col-sm-4">
                                    <select id="country_search" name ="countrysearch[]" class="required form-control"></select>
                                    <div id="errMsg" class="successMsg"></div>
                                </div>
                                <label for="inputEmail3" class="col-sm-2 control-label">Region</label>
                                <div class="col-sm-4">
                                    <select name ="statesearch[]" id ="state_search" class="required form-control"></select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="dating-part">
                        <div class="dating-part-inner1">
                            <div class="dating-inner-head">
                                <div class="dating-bg-gray3">
                                    <h4>WHEN</h4> 
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">from</label>
                                <div class="col-sm-3">
                                    <div>
                                        <input id="datepicker-fromsearch" class="form-control"  name="from" value="" type="text">

                                    </div>
                                </div>
                                <label for="inputEmail3" class="col-sm-1 control-label">to</label>
                                <div class="col-sm-3">
                                    <input id="datepicker-tosearch" value="" name="to" class="form-control" type="text">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-5">
                        </div>
                        <div class="col-sm-3 no-padding-left">
                            <input type="hidden" name="logged_user_search" value="<?php echo $log_user; ?>" id="logged_user">
                            <a href="javascript:void(0)"  id="search" class="dating-btn-maroon"><i class="fa fa-search"> Search</i></a>
                        </div>
                    </div>               
            </div>
            </div>                
             <div class="col-sm-6 no-padding-left">
                 <div class="dating-part-main">
                     <div class="dating-part">
                         <div class="dating-bg-red">
                             <h4>TRAVEL AGENDA search results</h4>
                         </div>
                     </div>
                     <div class="serach_result">
                     </div>
                 </div>
             </div>                   
            </div>
        </div>
        <div class="row">
            <div class="col-sm-11 dating-margin-main bot_part">
                <div class="col-sm-6 no-padding-left">
                    <div class="dating-part-main">
                        <div class="dating-part">
                            <div  class="dating-bg-red">
                                <h4>TRAVEL AGENDA in your preferred region (s)</h4>
                            </div>				
                        </div>

                        <div id="tab_a" class="entry-content just_gallery">	
                            <div class="gallaryList">
                                <?php
                                global $wpdb;
                                $master = $wpdb->get_results("SELECT * FROM wp_travel_dating where tarvel_logged_user=$log_user order by travel_currentdate DESC");
                                $i = 0;
                                foreach ($master as $rec) {
                                    $travel_id = $rec->id;
                                    $traveldatedb = $rec->travel_from;
                                    $strattime = date("d/m/Y", strtotime($traveldatedb));
                                    $stratdate = date("d", strtotime($traveldatedb));
                                    $stratmoth = date("M", strtotime($traveldatedb));
                                    $travelstartdate = $stratdate . ' ' . $stratmoth;
                                    $enddatedb = $rec->travel_to;
                                    $endtime = date("d/m/Y", strtotime($enddatedb));
                                    $enddate = date("d", strtotime($enddatedb));
                                    $endmonth = date("M", strtotime($enddatedb));
                                    $travelenddate = $enddate . ' ' . $endmonth;
                                    $travelcountry = $rec->travel_country;
                                    $travestate = $rec->travel_region;
                                    $travecity = $rec->travel_city;
                                    $trave_user = $rec->tarvel_logged_user;
                                    if ($log_user == $trave_user) {
                                        ?>

                                        <div class="dating-part">
                                            <div class="dating-part-inner">
                                                <div class="dating-inner-head">
                                                    <div class="dating-bg-gray">
                                                        <h4>From: <span class="country"><?php echo $travelstartdate; ?></span></h4> 
                                                    </div>
                                                    <div class="dating-bg-gray1">
                                                        <h4>to: <span class="region"><?php echo $travelenddate; ?><span></h4> 
                                                        </div>
                                                                    <input type ="hidden" id="startdate_<?php echo $travel_id; ?>" value="<?php echo date("m/d/Y", strtotime($traveldatedb)); ?>">
                                                                    <input type ="hidden" id="enddate_<?php echo $travel_id; ?>" value="<?php echo date("m/d/Y", strtotime($enddatedb)); ?>">
                                                                    <input type ="hidden" id="country_<?php echo $travel_id; ?>" value="<?php echo $travelcountry; ?>">
                                                                    <input type ="hidden" id="city_<?php echo $travel_id; ?>" value="<?php echo $travecity; ?>">
                                                                    <input type ="hidden" id="region_<?php echo $travel_id; ?>" value="<?php echo $travestate; ?>">
                                                                    </div>
                                                                    <div class=" dating-img-header">
                                                                        <a class="edit_travel pull-right" href="javascript:void(0)" id="<?php echo $travel_id; ?>" onclick="edit_travel(this.id)">Edit</a>
                                                                        <a class="delet_travel pull-right" href="javascript:void(0)" id="<?php echo $travel_id; ?>" onclick="delet_travel(this.id)">Delete</a>
                                                                        <h4><?php echo $travelcountry; echo ($travestate != '') ? " / " : ""; echo $travestate; ?></h4>
                                                                    </div>
                                                    </div>
                                                </div>
                                                      <?php $i++; } } ?>
                                                            </div>
                                                            <?php if (count($master) >= 1) { //echo "yes"; ?>
                                                                <div class="page_navigation" style="padding-top:10px;"></div>
                                                            <?php } ?>
                                                            <?php
                                                            if (count($master) == 0) {
                                                                echo "There is no ads currently! Please Uploads your ads here";
                                                            }
                                                            ?>
                                                            </div>
                                                            </div>
                                                            </div>
                                                            <div class="col-sm-6 no-padding-left">
                                                                <div class="alert alert-success adds_succ" style="display:none;">Your travel ad has been published</div>
                                                                <div class="alert alert-danger adds_error" style="display:none;">Please fill all the required fields</div>
                                                                <div class="alert alert-success adds_succ_upadated" style="display:none;">Your Travel Ads has been Updated</div>
                                                                <div class="dating-part-main">
                                                                    <form action="" method="post" enctype="multipart/form-data" id="MyUploadForm" class="form-horizontal">
                                                                        <div class="dating-part">
                                                                            <div class="dating-bg-blue">
                                                                                <h4>PLACE YOUR TRAVEL AGENDA</h4>
                                                                                <span class="error_adds" style="display:none;">All Fileds are Required</span>
                                                                            </div>
                                                                            <div class="dating-part-inner1">
                                                                                <div class="dating-inner-head">
                                                                                    <div class="dating-bg-gray3">
                                                                                        <h4>WHERE</h4> 
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label for="inputEmail3" class="col-sm-2 control-label">Country</label>
                                                                                    <div class="col-sm-4">
                                                                                        <select id="country" name ="country[]" class="required form-control"></select>
                                                                                    </div>
                                                                                    <label for="inputEmail3" class="col-sm-2 control-label">Region</label>
                                                                                    <div class="col-sm-4">
                                                                                        <select name ="state[]" id ="state" class="form-control"></select>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label for="inputEmail3" class="col-sm-2 control-label">City</label>
                                                                                    <div class="col-sm-4">
                                                                                        <input type ="text" name="city_name" id="city_name" class="form-control">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="dating-part">
                                                                            <div class="dating-part-inner1">
                                                                                <div class="dating-inner-head">
                                                                                    <div class="dating-bg-gray3">
                                                                                        <h4>WHEN</h4> 
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label for="inputEmail3" class="col-sm-2 control-label">from</label>
                                                                                    <div class="col-sm-3">
                                                                                        <div>
                                                                                            <input id="datepicker-example1" class="form-control"  name="from" value="" type="text">
                                                                                        </div>
                                                                                    </div>
                                                                                    <label for="inputEmail3" class="col-sm-1 control-label">to</label>
                                                                                    <div class="col-sm-3">
                                                                                        <input id="datepicker-example5" value="" name="to" class="form-control" type="text">

                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-sm-5">
                                                                            </div>
                                                                            <input type="hidden" name="update_travellingadds" value="" id="update_travellingadds">
                                                                            <div class="col-sm-3 no-padding-left">
                                                                                <input type="hidden" name="logged_user" value="<?php echo $log_user; ?>" id="logged_user">

                                                                                <input type="submit"  id="submit-btn" value="PUBLISH"  class="dating-btn-maroon"/>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                    <div class="clear"></div>
                                                                </div>
                                                            </div>
                                                            </div>
                                                            </div>
                                                            </div>
                                                            </div>
<script>
document.getElementById('MyUploadForm').reset();
jQuery(document).ready(function(){

jQuery("#datepicker-example5").datepicker({
    numberOfMonths: 1,
    minDate       : 0,
    onSelect: function(dateText){

        var startDate = new Date(dateText);
        var endday    = startDate.getDate();
        var endmonthnu= startDate.getMonth();
        var getmonth  = getmonthname(endmonthnu);
        jQuery('#end_dpreview').val(endday);
        jQuery('#end_mpreview').val(getmonth);
        startDate.setDate(startDate.getDate() - 1);
        jQuery("#datepicker-example1").datepicker("option", "maxDate", startDate);
    }
});

jQuery("#datepicker-example1").datepicker({ 
    numberOfMonths: 1,
    minDate       : 0,
	onSelect: function(dateText) {
    	var startDate = new Date(dateText);
        var selDay    = startDate.getDate();
        jQuery('#stard_dpreview').val(selDay);
        startDate.setDate(startDate.getDate() + 1);
        jQuery("#datepicker-example5").datepicker("option", "minDate", startDate);  
    }
});

jQuery("#datepicker-fromsearch").datepicker({
    minDate       : 0,
});
jQuery("#datepicker-tosearch").datepicker({ 
     minDate       : 0,
});

populateCountries("country", "state","","");
populateCountries("country_search", "state_search","","");
jQuery('#tab_a').pajinate();

function getmonthname(endmonth) {
    var month = new Array();
    month[0] = "January";
    month[1] = "February";
    month[2] = "March";
    month[3] = "April";
    month[4] = "May";
    month[5] = "June";
    month[6] = "July";
    month[7] = "August";
    month[8] = "September";
    month[9] = "October";
    month[10] = "November";
    month[11] = "December";

    var d = endmonth;
    var n = month[d];
    return n;

}

//jQuery('#tab_a').pajinate();
jQuery('#submit-btn').click(function(){

    var country   = jQuery('#country').val();
    var state     = jQuery('#state').val();
    var cityname  = jQuery('#city_name').val();
    var datestart = jQuery('#datepicker-example1').val();
    var dateend   = jQuery('#datepicker-example5').val();
    var logged_us = jQuery('#logged_user').val();
    var upadte    = jQuery('#update_travellingadds').val();
    var home      = '<?php echo bloginfo("template_url"); ?>';    
    var url       = home+'/page-templates/ajax_taveldating.php';
    var data      = 'action=add&country=' +country+'&state='+state+'&cityname='+cityname+'&datestart='+datestart+'&dateend='+dateend+'&logged_user='+logged_us+'&upadte='+upadte;
    if((country==''||country==null) ||(datestart==''|| datestart==null) ||(dateend==''|| dateend==null))
    {
          jQuery('.adds_error').show();
          setTimeout(function(){
			  $('.adds_error').remove();
		   }, 3000);
          return false;
    }
    else
    {
        jQuery.ajax({
            url     : url,
            type    : 'POST',
            data    : data,
            success : function(data) { 
            	 var res =$.trim(data);
            	 if(res=='suceesed')
            	 {
	                 jQuery('.adds_succ').show();
	                 location.reload();
                 }
                 else
                 {
                 	 jQuery('.adds_succ_upadated').show();
	                 location.reload();
                 }
            }          	 
        });
        return false;
    } 
});					
});


function edit_travel(id){  
    var edit_travel   =  id;
    var start         =  jQuery('#startdate_'+edit_travel).val();
    var enddate       =  jQuery('#enddate_'+edit_travel).val();
    var country       =  jQuery('#country_'+edit_travel).val();
    var region        =  jQuery('#region_'+edit_travel).val();
    var city          =  jQuery('#city_'+edit_travel).val();
  	populateCountries("country", "state",country,"");
    $('#country option[value="'+country+'"]').prop('selected', true);
    $('#state option[value="'+region+'"]').prop('selected', true);
    $('#datepicker-example1').val(start);
    $('#datepicker-example5').val(enddate);
    $('#city_name').val(city);
    $('#update_travellingadds').val(edit_travel);

} 
// End Of edit adds function 

//delete function
function delet_travel(id){
	    var delete_addsid = id;
	    var data          = 'id=' + delete_addsid+'&action=delete_adds';
	    var home          = '<?php echo bloginfo("template_url"); ?>';    
        var url           = home+'/page-templates/ajax_taveldating.php';
	
    	$.ajax({
    		type     : "POST",
    		url      : url,
    		data     : data,
    		datatype : "html",
    		success  : function(data){
    			var res = $.trim(data);
    	        if(res=='delete')
    	        {
    	        	$('#del_succ').show();
    	        	setTimeout(function(){
    			       $('.adds_error').remove();
    		        }, 3000);
    		        location.reload();
    	        }
    	    }
    	});    
}

//End of Delete function

//Search function
jQuery('a#search').click(function(){
    var hint = 0;
    var country       = jQuery('#country_search').val();
    var state         = jQuery('#state_search').val();
    var datefrom      = jQuery('#datepicker-fromsearch').val();    
    var dateend       = jQuery('#datepicker-tosearch').val();
    var home          = '<?php echo bloginfo("template_url"); ?>';    
    var url           = home+'/page-templates/ajax_taveldating.php';
    var data          = 'action=search&country=' +country+'&state='+state+'&datefrom='+datefrom+'&dateend='+dateend;
    if(country == '')
    {
        hint = 1;
        jQuery('#errMsg').html('Please select country');
        setTimeout(function () {
                        jQuery('#errMsg').fadeOut(7000);
                    }, 7000);
    }
    if(hint == 0)
    {
       jQuery.ajax({
        type     : "POST",
        url      : url,
        data     : data,
        datatype : "html",
        success  : function(data){
            $('.serach_result').html(data); 
            jQuery('.tab_b').pajinate({items_per_page:"2"});  

        }
    }); 
    return false; 
    }
        
    
	   
});

//End of search function

</script>


<?php } else { wp_redirect( home_url() ); }get_footer();?>

