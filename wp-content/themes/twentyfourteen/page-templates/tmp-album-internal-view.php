<?php
/*
 * Template Name: Album Internal View 
 * Description: A Page Template show the Member profile homepage.
 * Author Name : Sanjay Shinghania
 */

get_header(); 
global $wpdb;
$upload_dir = wp_upload_dir();
?>
<div class="albumWrapEx">
    <div class="container">
        <div class="">
            <div class="col-sm-3 albumPic">
            <?php $album_id=$_GET['album'];
                  $user_id=$_GET['user_id'];
                  $imgPath = $upload_dir['baseurl'] . '/users_album/album_' . $user_id . '/'; 
                  $album_images=$wpdb->get_results("SELECT img_url FROM user_album_images WHERE album_id='" . $album_id. "' AND status=1 order by img_order ASC");
            ?>

                <div class="ablbumCum">
                    Picture / Album 
                </div>
                <div class="row form-group">
                <?php foreach ($album_images as $value) 
                { ?>
                    

                    <div class="col-sm-6">
                        <a href="javascript:void(0)" id="select_img" class="select"><img src="<?php echo $imgPath.$value->img_url; ?>" alt="" class="img-responsive"/></a>
                    </div> 
                    <?php } ?>            
                </div>
            </div>
            <div class="col-sm-9">
                <div class="imgViewPoint">
                    <div class="row">
                        <div class="col-sm-8 img_frame">
                            <img src="<?php echo $imgPath.$album_images[0]->img_url; ?> <?php /* echo get_template_directory_uri(); ?>/images/validate-process-right-pic.jpg<?php */ ?>" alt="" class="img-responsive"/>
                        </div>
                        <div class="col-sm-4">
                            <div class="pictureText">
                                There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
<script>
jQuery(document).ready(function(){
    jQuery('.select').click(function(){
       var img=jQuery(this).html();
       jQuery('.img_frame').html(img);
    })
})
</script>
