<?php
session_start();
/*
 * Template Name: Welcome Basic Member
 * Description: A Page Template with display some messageing for a basic member.
 * Author Name : Sanjay Shinghania
 */

get_header();

$id         = get_the_ID();
$page_title = get_post($id);
$registered_user_id = $_GET['user_regis_key'];
$user_id = get_current_user_id();
$member_type = get_user_meta($user_id,'member_type',true);
?>

<div class="container-fluid">
    <div class="well-happyWrap">
        <div class="container">
            <form class="form-horizontal" role="form" name="form" method="post">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="row">		
                            <div class="col-md-12">
                                <h4 class="lightGry happy-h4"><?php echo $current_user->user_login; ?></h4>
                                <h4 class="white wellT">Welcome to Hedonia.ch</h4>
                            </div>
                        </div>
                        <div class="row">		                                                    
                            <?php  
                                $page_title->post_content; 
                                if($member_type=="couples")
                                {
								   echo $page_title->post_content; 
								}
								elseif($member_type=="ladies")
								{
									$content = $page_title->post_content;
									$string_content = str_replace('?page_id=203','?page_id=433',$content);
									echo $final_content = str_replace('?page_id=205','?page_id=435',$string_content);									   
								}	   
								elseif($member_type=="gentleman")
								{
									$content = $page_title->post_content;
									$string_content = str_replace('?page_id=203','?page_id=468',$content);
									echo $final_content = str_replace('?page_id=205','?page_id=470',$string_content);
								}
                                else
                                {
								    echo $page_title->post_content; 
								}                            
                             ?>
                            <?php $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail'); ?>
                            <div class="col-sm-5 text-right happyRight"><a href="<?php the_permalink(); ?>"><img src="<?php echo $image[0]; ?>" class="happy-couple-img"></a></div>
                        </div>
                    </div>		
                </div>		
            </form>
        </div>
    </div>
</div>

<?php get_footer(); ?>