<?php
/* Template Name:Dating Ads
 * 
 * 
 * 
 */
get_header();
if ( is_user_logged_in() ) {
?>

<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo get_template_directory_uri(); ?>/js/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.form.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.pajinate.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/fancybox/jquery.fancybox.js"></script>


<script>
jQuery(document).ready(function(){
    jQuery("#datepicker-example5").datepicker({
        numberOfMonths: 1,
        minDate       : 0,
        onSelect: function(dateText){
	        var startDate = new Date(dateText);
	        var endday    = startDate.getDate();
	        var endmonthnu= startDate.getMonth();
	        var getmonth  = getmonthname(endmonthnu);
	        jQuery('#end_dpreview').val(endday);
	        jQuery('#end_mpreview').val(getmonth);
	        startDate.setDate(startDate.getDate() - 1);
	        jQuery("#datepicker-example1").datepicker("option", "maxDate", startDate);
	      

        }
    });

    jQuery("#datepicker-example1").datepicker({ 
        numberOfMonths: 1,
        minDate       : 0,
    	onSelect: function(dateText) {
	    	var startDate = new Date(dateText);
	        var selDay    = startDate.getDate();
	        jQuery('#stard_dpreview').val(selDay);
	        startDate.setDate(startDate.getDate() + 1);
	        jQuery("#datepicker-example5").datepicker("option", "minDate", startDate);
        

        }
    });

    function getmonthname(endmonth) {
	    var month = new Array();
	    month[0] = "January";
	    month[1] = "February";
	    month[2] = "March";
	    month[3] = "April";
	    month[4] = "May";
	    month[5] = "June";
	    month[6] = "July";
	    month[7] = "August";
	    month[8] = "September";
	    month[9] = "October";
	    month[10] = "November";
	    month[11] = "December";

	    var d = endmonth;
	    var n = month[d];
	    return n;

    }   

    jQuery('#tab_a').pajinate();
    var editor = CKEDITOR.replace( 'editor1' );
	editor.on( 'change', function( evt ) {
	    var editorval=evt.editor.getData(); 
	   // var setval=evt.editor.getData('ranjiteee');
	     $('#add_content').val(editorval);
	   
	});
  
    var options = { 
			target        :   '#output',   // target element(s) to be updated with server response 
			beforeSubmit  :   beforeSubmit,  // pre-submit callback 
			success       :   afterSuccess,  // post-submit callback 
			uploadProgress:   OnProgress, //upload progress callback 
			resetForm     :   true        // reset the form after successful submit 
	}; 
		
	jQuery('#MyUploadForm').submit(function(){ 
			jQuery(this).ajaxSubmit(options);  			
			// always return false to prevent standard browser submit and page navigation 
			return false; 
	}); 

	function afterSuccess()
	{       
		jQuery('#submit-btn').show(); //hide submit button
		jQuery('#loading-img').hide(); //hide submit button
		jQuery('#progressbox').delay( 1000 ).fadeOut(); //hide progress bar
	}

	//function to check file size before uploading.
	function beforeSubmit(){
	    //check whether browser fully supports all File API
	   if (window.File && window.FileReader && window.FileList && window.Blob)
		{             
            var update_adds  = $("#update_adds").val();
			$('#blah').each(function () {
				if (this.src.length < 0) {
                  jQuery("#output").html("Are you kidding me?");
				  return false

				}
		    })
			
			/*
			if( !jQuery('#FileInput').val()) //check empty input filed
			{
				jQuery("#output").html("Are you kidding me?");
				return false
			}
			*/
			if(update_adds=='' || update_adds==null)
			{
				//alert("why");
					   var fsize = jQuery('#FileInput')[0].files[0].size; //get file size
					   var ftype = jQuery('#FileInput')[0].files[0].type; // get file type
					   /*
					   var country  = jQuery("#country").val();
				       var state    = jQuery("#state").val();
				       var city     = jQuery("#city_name").val();
				       var from_date= jQuery("#datepicker-example1").val();
				       var to_date  = jQuery("#datepicker-example5").val();
				       var ad_title = jQuery("#add_title").val();
				       var ad_cont  = jQuery("#cke_1_contents").val();
				       var file     = jQuery("#fileUploader").val();
		               */
					//allow file types 
					switch(ftype)
			        {
			            case 'image/png': 
						case 'image/gif': 
						case 'image/jpeg': 
						case 'image/pjpeg':

			            break;
			            default:
			               jQuery("#output").html("<b>"+ftype+"</b> Unsupported file type!");
						   return false
			        }
					
					//Allowed file size is less than 5 MB (1048576)
					if(fsize>5242880) 
					{
						jQuery("#output").html("<b>"+bytesToSize(fsize) +"</b> Too big file! <br />File is too big, it should be less than 5 MB.");
						return false
					}
							
					//jQuery('#submit-btn').hide(); //hide submit button
					jQuery('#loading-img').show(); //hide submit button
					jQuery("#output").html(""); 
					//alert(jQuery('.adds_succ').show()); 
            }
		}
		else
		{
			//Output error to older unsupported browsers that doesn't support HTML5 File API
			jQuery("#output").html("Please upgrade your browser, because your current browser lacks some new features we need!");
			return false;
		}
	}

	//preview bowse images

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
      
            reader.onload = function (e) {
            	//alert(target);
                $('#blah').attr('src', e.target.result);
                $('#addsimg__preview').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#FileInput").change(function(){
        readURL(this);
    });




	//end of preview images

	//progress bar function
	function OnProgress(event, position, total, percentComplete)
	{
	    //Progress bar
		jQuery('#progressbox').show();
	    jQuery('#progressbar').width(percentComplete + '%') //update progressbar percent complete
	    jQuery('#statustxt').html(percentComplete + '%'); //update status text
	    if(percentComplete>50)
	        {
	            jQuery('#statustxt').css('color','#000'); //change status text to white after 50%
	        }
	    if(percentComplete==100)
	    {
	    	jQuery('.adds_succ').show();
	    	jQuery('html, body').animate({ scrollTop: $('.adds_succ').offset().top }, 'slow');
	    	location.reload();
	    }
	}

	//function to format bites bit.ly/19yoIPO
	function bytesToSize(bytes) {
	   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
	   if (bytes == 0) return '0 Bytes';
	   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
	   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
	}


   //end of dating ads images uplaod script
    

						
  });

</script>
<?php  
    $current_user  = wp_get_current_user(); 
    $log_user      = $current_user->ID;
 ?>
<div class="container">
<div class="parent_div">

<?php   
$user_type = get_user_meta($current_user->ID ,'user_type', true);
if($operational_setting[7]->$user_type != 1) 
    {?>
<div class="col-sm-11 dating-margin-main"  style="position:relative;">
	<span class="grayTrans"></span>
<?php } else{ ?>
<div class="col-sm-11 dating-margin-main">	
<?php } ?>	
		
	
	<div class="col-sm-6 no-padding-left">
		<div class="dating-part-main">
			<div class="dating-part">
				<div class="dating-bg-red">
				<h4>DATING ADS in your preferred region (s)</h4>
				</div>				
            </div>

			<div id="tab_a" class="entry-content just_gallery">	
			    <div class="gallaryList">
					<?php 
					global $wpdb;
					$master = $wpdb->get_results( "SELECT * FROM wp_dating_ads where user_id=$log_user order by createdon DESC");
					$i=0;
					foreach ($master as $rec){ 
						$startdate = $rec->from;
						$editstart = date("m/d/Y", strtotime($startdate));
						$firstd    = date("d", strtotime($startdate));						
						$enddate   = $rec->to;
						$editend   = date("m/d/Y", strtotime($enddate));
						$when_date = date("d", strtotime($startdate)) . date("M", strtotime($startdate)) . ' - ' .  date("d", strtotime($enddate)) . date("M", strtotime($enddate));
						$secondd   = date("d", strtotime($enddate));
						$seconm    = date("F", strtotime($enddate));
						$imgdir    = wp_upload_dir();
						$img       = $imgdir[baseurl];
						$imgname   = $rec->ads_pick;
						$conadd    = strip_tags($rec->ads_content);
						$adds_id   = $rec->id; 
						$city_name = $rec->city; 
						$adds_title= $rec->ads_title; 
						$adds_user = $rec->user_id;   
						if($log_user == $adds_user){
					?>							  
						<div id="showDiv<?php echo $adds_id; ?>" class="dating-part">
						<div class="dating-part-inner">
						<div class="dating-inner-head">
						<div class="dating-bg-gray">
						<h4>Where: <span class="country_<?php echo $adds_id; ?>"><?php echo $rec->country; ?></span> <span class="region_<?php echo $adds_id; ?>"><?php  echo ($rec->region != '') ? " / " : ""; echo $rec->region; ?><span></h4> 
						</div>
						<div class="dating-bg-gray1">
						<h4>When: <span><?php echo $when_date;?><span></h4> 
						</div>
						<input type ="hidden" id="city_name_<?php echo $adds_id; ?>" value="<?php echo $city_name; ?>">
						<input type ="hidden" id="datepicker-example1_<?php echo $adds_id; ?>" value="<?php echo $editstart; ?>">
						<input type ="hidden" id="datepicker-example5_<?php echo $adds_id; ?>" value="<?php echo $editend; ?>">
						<input type ="hidden" id="add_title_<?php echo $adds_id; ?>" value="<?php echo $adds_title; ?>">
						<input type ="hidden" id="add_con_<?php echo $adds_id; ?>" value="<?php echo $conadd; ?>">
						<input type ="hidden" id="add_img_<?php echo $adds_id; ?>" value="<?php echo $imgname; ?>">
						</div>
						<div class=" dating-img-header">
						<h4><?php echo $rec->ads_title;?></h4>
						</div>
						<div class="dating-mail-main">
						<div class="col-sm-2 no-padding-left adds_img_<?php echo $adds_id; ?>">
						<img src="<?php echo $img."/dating_ads/".$imgname;?>" class="img-responsive" alt="logo" />
						</div>
						<div class="col-sm-8 no-padding">
						<div class="dating-text-box"><p><?php echo $conadd; ?></p></div>
						</div>
						<div class="col-sm-2 ">
						<img class="mail-img" src="<?php echo bloginfo('template_url'); ?>/images/datingImg4.png" alt="logo" />
						<?php if($log_user = $adds_user) { ?>
						<a href="javascript:void(0)" id="edit_ads_<?php echo $adds_id; ?>" class="edit_ads" onClick="editadds('<?php echo $adds_id; ?>');">Edit</a>| 
						<a href="javascript:void(0)" id="delete_ads_<?php echo $adds_id; ?>" class="delete_ads" onClick="deleteadds('<?php echo $adds_id; ?>');">Delete</a>
						<?php } ?>
						</div>
						</div>
						</div>
						</div>
						<?php $i++;
						}
					}
					?>
				<div class="successMsg" id="showmsg"></div>
				</div>
					<?php  if(count($master)>=1) { //echo "yes"; ?>
				<div class="page_navigation" style="padding-top:10px;"></div>
					<?php } ?>
				<?php if(count($master)==0){ echo "There is no ads Currently! Please Uploads your ads here"; } ?>
			</div>
		</div>
</div>





<div class="col-sm-6 no-padding-left">
<div class="alert alert-success showimg" style="display:<?php echo ($_GET['err'] == 'failed') ? "block" : "none"; ?>">Please upload image</div>
	<div class="alert alert-success adds_succ" style="display:none;">Your ad has been published</div>
	<div class="alert alert-success adds_succ_upadated" style="display:none;">Your Ads has been Updated</div>
	<div class="dating-part-main">
	<form action="<?php echo get_bloginfo('template_url')?>/page-templates/datingaddsimages_uplaod.php" method="post" enctype="multipart/form-data" id="MyUploadForm" class="form-horizontal">
		<div class="dating-part">
			<div class="dating-bg-blue">
				<h4>PLACE YOUR DATING AD</h4>
				<span class="error_adds" style="display:none;">All Fileds are Required</span>
			</div>
			<div class="dating-part-inner1">
				<div class="dating-inner-head">
					<div class="dating-bg-gray3">
						<h4>WHERE</h4> 
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Country</label>
					<div class="col-sm-4">
						<select required id="country" name ="country[]" class="required form-control"></select>
					</div>
					<label for="inputEmail3" class="col-sm-2 control-label">Region</label>
					<div class="col-sm-4">
						<select  name ="state[]" id ="state" class="required form-control"></select>
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">City</label>
					<div class="col-sm-4">
						<input type ="text" name="city_name" id="city_name" class="form-control">
					</div>
				</div>
			</div>
		</div>
		<div class="dating-part">
			<div class="dating-part-inner1">
				<div class="dating-inner-head">
					<div class="dating-bg-gray3">
						<h4>WHEN</h4> 
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">from</label>
					<div class="col-sm-3">
						<div>
							<input id="datepicker-example1" class="form-control"  name="from" value="" type="text">
						</div>
					</div>
					<label for="inputEmail3" class="col-sm-1 control-label">to</label>
					<div class="col-sm-3">
						<input id="datepicker-example5" value="" name="to" class="form-control" type="text">
					</div>
				</div>

			</div>
		</div>         
        <div class="dating-part">
			<div class="dating-part-inner1">
				<div class="dating-inner-head">
					<div class="dating-bg-gray3">
						<h4>Your Ad Title</h4> 
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Name</label>
					<div class="col-sm-6">
						<div>
							<input id="add_title" class="form-control"  name="your_ad_title" value="" type="text">
							
						</div>
					</div>
				
				</div>

			</div>
		</div>


		<div class="dating-part">
			<div class="dating-part-inner1">
				<div class="dating-inner-head">
					<div class="dating-bg-gray3">
						<h4>YOUR AD</h4> 
					</div>
				</div>
<!--                                                                                                                                                                                    <input type="text" name="add_title"> -->
				<div class="form-group no-margin" style="padding:0px 8px;">
					<div class="dating-text-box cust ">
					 <textarea required id='editor1' name ='mail_content' onkeyup="submition();"></textarea>
					 <input type="hidden" name="add_content" id="add_content">
					 <input type="hidden" name="adds_userid" id="adds_userid" value="<?php echo get_current_user_id(); ?>">					

					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
		<div class="row">
		    
				<div class="col-sm-4 no-padding-right photo-section">
					<div class="photo-box adds_thumbimg"><img id="blah" src="#" alt="your image" class="img-responsive"/></div><br/>
                                        <input name="FileInput" id="FileInput" type="file" style="display: none;"/>
					<label class="dating-choose-pt" for="FileInput">Choice one from your profile pictures</label>
					
					
<!--					 <span class="adds_thumbimg"><img id="blah" src="#" alt="your image" /></span>						-->
				</div>
		
			<div class="col-sm-5">
			</div>
			   <input type="hidden" name="update_img" value="" id="update_img">
               <input type="hidden" name="update_adds_ids" value="" id="update_adds">
			<div class="col-sm-3 no-padding-left">
				<a href="#inlinepreview_adds" class="dating-btn-gray fancybox" id="prev_add">PREVIEW</a><br/><br/>
				<input type="submit" id="submit-btn" value="PUBLISH" class="dating-btn-maroon"/>
			</div>                    
		</div>
                            </form>
            <div class="clear"></div>
               <!-- lightbox div-->

			       <div class="dating-part preview_addspopup" id="inlinepreview_adds" style="display: none;">
								<div class="dating-part-inner">
								<div class="dating-inner-head">
								<div class="dating-bg-gray">
								<h4>Where: <span class="country_preview">Belgium</span>/<span class="region_preview"><span></span></span></h4> 
								</div>
								<div class="dating-bg-gray1">
								<h4>When: <span class="when_preview"><span></span></span></h4> 
								</div>
								</div>
								<div class=" dating-img-header">
								<h4 class="addsname_preview">Hedonisten</h4>
								</div>
								<div class="dating-mail-main">
								<div class="col-sm-2 no-padding-left adds_img_8">
								<img id="addsimg__preview" alt="logo" class="img-responsive" src="">
								</div>
								<div class="col-sm-8 no-padding">
								<div class="dating-text-box_preview"><p></p></div>
								</div>
								<div class="col-sm-2 ">
									<img alt="logo" src="http://projects.udaantechnologies.com/hedon/wp-content/themes/twentyfourteen/images/datingImg4.png" class="mail-img">
									<input type="hidden" id="stard_dpreview" value="">	
									<input type="hidden" id="end_dpreview" value="">	
									<input type="hidden" id="end_mpreview" value="">	 				
								</div>
							</div>
							</div>
					</div>			  
		    </div>
		    </div>
</div>
</div>

</div>

</div>	


<script>
setTimeout('$(".showimg").hide()',6000);
document.getElementById('MyUploadForm').reset();

//edit adds function 
    jQuery(document).ready(function(){

        populateCountries("country", "state","","");
        jQuery('#prev_add').click(function(){
		   var country  = jQuery("#country").val();
	       var state    = jQuery("#state").val();
	       var city     = jQuery("#city_name").val();
	       var from_date= jQuery("#stard_dpreview").val();
	       var to_date  = jQuery("#end_dpreview").val();
	       var to_moth  = jQuery("#end_mpreview").val();
	       var ad_title = jQuery("#add_title").val();
	       var ad_cont  = jQuery("#add_content").val();
           jQuery('.country_preview').append(country);
           jQuery('.region_preview').append(state);
           jQuery('.when_preview').append(from_date+"-"+to_date+""+to_moth);
           jQuery('.addsname_preview').append(ad_title);
           jQuery('.dating-text-box_preview').append(ad_cont);
           jQuery('.fancybox').fancybox();
        });
    })

   function editadds(id){      
	    var edit_addsid   =  id;
	    var edit_country  =  jQuery('.country_'+edit_addsid).text();
	    var edit_region   =  jQuery('.region_'+edit_addsid).text();
	    var edit_city     =  jQuery('#city_name_'+edit_addsid).val();
	    var edit_from     =  jQuery('#datepicker-example1_'+id).val();
	    var edit_to       =  jQuery('#datepicker-example5_'+edit_addsid).val();
	    var edit_name     =  jQuery('#add_title_'+edit_addsid).val();
	    var edit_desc     =  jQuery('#add_con_'+edit_addsid).val();
	    var edit_imgpath  =  '<?php echo $img; ?>';
	    var edit_pic      =  jQuery('#add_img_'+edit_addsid).val();
	    var edit_img      =  edit_imgpath+'/dating_ads/'+edit_pic;
	    singleCountries("country", "state",edit_country,edit_region);
	    $('#city_name').val(edit_city);
	    $('#datepicker-example1').val(edit_from);
	    $('#datepicker-example5').val(edit_to);
	    $('#add_title').val(edit_name);
	    $('#update_adds').val(edit_addsid);
	    $('#update_img').val(edit_pic);
	    $('.adds_thumbimg').html('<img alt="your image" src="'+edit_img+'" id="blah" class="img-responsive">');
        
		CKEDITOR.instances.editor1.setData( edit_desc, function()
		{
		    this.checkDirty();  // true
    	});
     
    }
   
   // End Of edit adds function 
   
   //delete function


   function deleteadds(id){

   	    var delete_addsid = id;
   	    var site          =  '<?php echo bloginfo("template_url"); ?>';
   	    var url           = site+'/page-templates/datingaddsimages_uplaod.php';
   	    var data          = 'id=' + delete_addsid+'&action=delete_adds';
   	
		$.ajax({
			type    : "POST",
			url     : url,
			data    : data,
			datatype: "html",
			success : function(response){
				var result = $.parseJSON(response);
				$('#showDiv'+id).hide();
				$('#showmsg').show();
				$('#showmsg').html(result.msg);
		        setTimeout(function () {
                                jQuery('#showmsg').fadeOut(5000);
                            }, 5000);
		        window.location = self.location;
		}
		});
   }

   //End of Delete function
</script>


<?php } else { wp_redirect( home_url() ); }get_footer();?>

