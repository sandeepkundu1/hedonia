<?php
/**
 * Template Name: lost password
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Twenty Twelve consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * Author Name : umesh saini
 * @package WordPress
 * @subpackage twentyfourteen
 * @since Twenty Twelve 1.0
 */
get_header();
require_once($_SERVER['DOCUMENT_ROOT'] . '/smtp/newsmtp/classes/class.phpmailer.php');

?>
<div class="wrapper">

    <?php
    global $wpdb;
    $error = '';
    $success = '';
    

    if (isset($_POST['action']) && 'reset' == $_POST['action']) {
        $email = trim($_POST['user_login']);
        $userName = trim($_POST['display_name']);
        $result = $wpdb->get_results("select * from wp_users where user_login =  '$userName'");
        
        $userEmail = array();
        $userid = array();
        foreach ($result as $key => $value) {
            $userEmail[] = $value->user_email; 
            $userid[] = $value->ID; 
        }        
        
        $user = get_user_by('email', $email);
        $user_login     = get_the_author_meta('user_login', $userid[0] );            
        if (empty($email)) {
            $error = 'Enter a username and e-mail address..';
        } else if ($userEmail[0] != $email) {
            $error = 'There is no user1 registered with that email address.';
        } else if($user_login != $userName){
                $error = 'User name does not exist!';
        } else if(!(username_exists($userName))){
            $error = 'There is no user registered with that email address.';  
            die;            
        } else {
            $random_password = rand();                        
            wp_set_password($random_password, $userid[0]);
            
            if ($user->ID) {
                $mail = new PHPMailer; // call the class
                $mail->IsSMTP();
                $mail->SMTPSecure = "";
                $mail->SMTPDebug = 0;
                $mail->Host = 'ssl://smtp.gmail.com';
                $mail->Port = '465'; //Port of the SMTP like to be 25, 80, 465 or 587
                $mail->SMTPAuth = true; //Whether to use SMTP authentication
                $mail->Username = "theclub.hedon@gmail.com";
                $mail->Password = "testing@theclub";
                $mail->SetFrom('theclub.hedon@gmail.com'); //From address of the mail
                //$mail->SMTPSecure = "tls";
                $mail->Subject = "Reset password"; //Subject od your mail
                $mail->AddAddress($email, $user_login); //To address who will receive this email
                $mail->IsHTML(true);
                $message = 'Your new password is: ' . $random_password;
                $patient_message = '<!DOCTYPE html>
                <html>
                    <head>
                        <title>Hendonia</title>
                    </head>
                    <body style="margin: 0;padding: 0;" bgcolor="#000000">
                        <table width="100%" border="0" cellpadding="0" cellsacing="0" align="center" bgcolor="#000000" style="padding-left:20px;">
                            <tr>
                                <td align="left">
                                    <table width="600" border="0" cellpadding="0" cellsacing="0" align="center" style="padding-bottom: 30px;">
                                        <tr>
                                            <td align="left" height="70" valign="middle">
                                                <a href="'.home_url().'" > <img src="'.get_template_directory_uri().'/images/logo.jpg" alt=""/></a>                   
                                            </td>
                                        </tr>
                                        <tr align="left">
                                            <td style="padding-left: 15px; padding-top: 15px; padding-right:15px; padding-bottom: 10px; background-color: #333;">
                                                <table  border="0" cellpadding="0" cellsacing="0" width="100%">
                                                    <tr>
                                                        <td  style="font-family: arial;font-size: 22px; line-height: 20px; color: #fff;">Password change Success!</td>                                        
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr align="left">
                                            <td  style="font-family: arial;font-size: 24px; color: #FE0000; padding-bottom: 10px; padding-top: 30px;">Welcome to Hedonia</td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: arial;font-size: 14px;line-height:20px; color: #fff; ">Hi  '.$user_login.'</td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: arial;font-size: 14px;line-height:20px; color: #fff; ">You have successfully changed password at <a href="'.home_url().'" style="color: #FE0000">Hedonia.ch</a></td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                <table width="100%" border="0" cellpadding="0" cellsacing="0" >
                                                    <tr>
                                                        <td width="25%" style="font-family: arial;font-size: 14px;line-height:20px; color: #fff; font-weight: bold; ">Your user name :</td>
                                                        <td style="font-family: arial;font-size: 14px;line-height:20px; color: #fff; ">'.$user_login.'</td>
                                                    </tr> 
                                                    <tr>
                                                        <td width="25%" style="font-family: arial;font-size: 14px;line-height:20px; color: #fff; font-weight: bold;">Your password :</td>
                                                        <td style="font-family: arial;font-size: 14px;line-height:20px; color: #fff; ">'.$random_password.'</td>
                                                    </tr> 
                                                </table>
                                            </td>
                                        </tr>                                        
                                        <tr>
                                            <td style="font-family: arial;font-size: 14px;line-height:20px; color: #fff; padding-top: 40px; ">
                                                Regards
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: arial;font-size: 16px;line-height:20px; color: #FE0000; padding-top: 0px; ">
                                                Hedonia
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </body>
                </html>';
                $mail->MsgHTML($patient_message);

                if ($mail->send() == true)
                    $success = 'Check your email address for you new password.';
            } else {
                $error = 'Oops something went wrong updaing your account.';
            }
        }

        if (!empty($error))
            echo '<div class="message"><p class="error"> ' . $error . '</p></div>';

        if (!empty($success))
            echo '<div class="error_login"><p class="success">' . $success . '</p></div>';
    }
    ?>

    <!--html code-->
    <div class="lostPwd">
        <form method="post">
            <fieldset>
                <p>Please enter your username and email id. You will receive a new password via email.</p>
                <div class="form-group">
                    <label for="user_login">Username</label>
<?php $user_login = isset($_POST['display_name']) ? $_POST['display_name'] : ''; ?>
                    <input type="text" name="display_name" class="form-control" id="display_name" value="<?php echo $display_name; ?>" />
                </div>
                <div class="form-group">
                    <label for="user_login">E-mail:</label>
<?php $user_login = isset($_POST['user_login']) ? $_POST['user_login'] : ''; ?>
                    <input type="text" name="user_login" class="form-control" id="user_login" value="<?php echo $user_login; ?>" />
                </div>
                <div class="form-group">
                    <input type="hidden" name="action" value="reset" />
                    <input type="submit" value="Get New Password" class="btn btn-success btn-sm signupBtn" id="submit" />
                </div>

            </fieldset>
        </form>
    </div>
</div>

<?php get_footer(); ?>