<?php
/**
 * Template Name: Control Visibility
 *
 */
get_header();
$userid = get_current_user_id();
$result = $wpdb->get_var("SELECT `visibility_for` FROM `wp_profile_visibility` WHERE `userid` = '$userid'");
$unserializeData = unserialize($result);
$explode = explode(",", $unserializeData);
$europeCountries = array('Austria', 'Belgium', 'France', 'Germany', 'Italy','Netherlands', 'Spain', 'Switzerland');
$user_type = get_user_meta( $userid, 'user_type', true ); 
$setting = $wpdb->get_results("select * from `wp_admin_portal_setting` where portal_setting_type = 'visibilty setting' ");

?>
<script src="<?php echo bloginfo('template_url'); ?>/js/jquery.magnific-popup.min.js" type="text/javascript"></script>   
<script src="<?php echo bloginfo('template_url'); ?>/js/jquery.magnific-popup.js" type="text/javascript"></script>   
<link href="<?php echo bloginfo('template_url'); ?>/css/magnific-popup.css" rel="stylesheet" type="text/css"/>
<div class="userProfile">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
            <?php echo ($setting[1]->$user_type == 'no') ? "<span class='grayTrans'></span>" : ""; ?>
                <div class="Reg2-Part-First controlVisible edit-visibilty">
                    <h3 class="search-bg-header visibilty-profile">Edit visibility of Your Profile</h3>	
                    <div class="editVis">
                        <p class="myPro">Your profile will be only visible for following selected members groups</p>
                        <div class="border-box1 box-content">                                                   
                            <div class="search_input">                            
                                <div class="col-md-4 col-sm-4 col xs-6">                                
                                    <div class="search-padding">
                                        <span>Couples</span>
                                        <input type="checkbox" for="search" <?php echo (in_array('couples', $explode)) ? "checked" : ""; ?> id="couples" value="couples" name="couples" class="checkbox_cus checked">
                                        <label for="couples" class="checkbox_label"></label>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col xs-6">                                
                                    <div class="search-padding">
                                        <span>Couples married</span>
                                        <input type="checkbox" for="search" id="married" value="couples married" <?php echo (in_array('couples married', $explode)) ? "checked" : ""; ?> name="reg3_onlyverifiedcouple" class="checkbox_cus checked">
                                        <label for="married" class="checkbox_label"></label>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col xs-6">                                
                                    <div class="search-padding">
                                        <span>Validated couples</span>
                                        <input type="checkbox" for="search" id="validated" value="validated couples" <?php echo (in_array('validated couples', $explode)) ? "checked" : ""; ?> name="validate couples" class="checkbox_cus checked">
                                        <label for="validated" class="checkbox_label"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="search_input">
                                <div class="col-md-4 col-sm-4 col xs-6">                                
                                    <div class="search-padding">
                                        <span>Ladies</span>
                                        <input type="checkbox" for="search" id="ladies" value="ladies" <?php echo (in_array('ladies', $explode)) ? "checked" : ""; ?> name="ladies" class="checkbox_cus checked">
                                        <label for="ladies" class="checkbox_label"></label>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col xs-6">                                
                                    <div class="search-padding">
                                        <span>Single ladies</span>
                                        <input type="checkbox" for="search" id="single_ladies" value="single ladies" <?php echo (in_array('single ladies', $explode)) ? "checked" : ""; ?> name="reg3_singleladies" class="checkbox_cus checked">
                                        <label for="single_ladies" class="checkbox_label"></label>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col xs-6">                                
                                    <div class="search-padding">
                                        <span>Validated ladies</span>
                                        <input type="checkbox" for="search" id="validate_ladies" value="validate ladies" <?php echo (in_array('validate ladies', $explode)) ? "checked" : ""; ?> name="reg3_validateladies" class="checkbox_cus checked">
                                        <label for="validate_ladies" class="checkbox_label"></label>
                                    </div>
                                </div>
                            </div>	
                            <div class="search_input">
                                <div class="col-md-4 col-sm-4 col xs-6">                                
                                    <div class="search-padding">
                                        <span>Men</span>
                                        <input type="checkbox" for="search" id="gentleman" value="gentleman" <?php echo (in_array('gentleman', $explode)) ? "checked" : ""; ?> name="reg3_mena" class="checkbox_cus checked">
                                        <label for="gentleman" class="checkbox_label"></label>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col xs-6">                                
                                    <div class="search-padding">
                                        <span>Single men</span>
                                        <input type="checkbox" for="search" id="single_man" value="single gentleman" <?php echo (in_array('single gentleman', $explode)) ? "checked" : ""; ?> name="reg3_singleman" class="checkbox_cus checked">
                                        <label for="single_man" class="checkbox_label"></label>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col xs-6">                                
                                    <div class="search-padding">
                                        <span>Validated men</span>
                                        <input type="checkbox" for="search" id="validate_gentleman" value="validate gentleman" <?php echo (in_array('validate gentleman', $explode)) ? "checked" : ""; ?> name="reg3_singlmen" class="checkbox_cus checked">
                                        <label for="validate_gentleman" class="checkbox_label"></label>
                                    </div>
                                </div>			    
                            </div>
                            <div class="search_input">
                                <div class="col-md-8 col-sm-8 col xs-6">                                
                                    <div class="search-padding">
                                        <span style="width:86%;">Make my profile visible for all members</span>
                                        <input type="checkbox" for="search" id="for_all" name="for all" class="checkbox_cus">
                                        <label for="for_all" class="checkbox_label"></label>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col xs-6">
                                    <div class="search-padding text-cenetr">
                                        <button type="button" id="chkVisibility" case="forall" class="btn okBtn okbtn-2">OK</button>
                                    </div>
                                    <div style="display:none;margin-left:-250px;" class="successMsg" id="edit_visibility">sdsdsdsd</div>
                                    <span style="display:none; text-align: left" class="visible_loding">
                                        <img width="60px" height="12px" src="<?php echo bloginfo('template_url'); ?>/images/ajax-status.gif">
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Part two  -->
                <?php
                $currentuserid = get_current_user_id();
                $userlist = $wpdb->get_results("select `ID`,`display_name` from `wp_users` where ID != '$currentuserid' AND ID IN (select `reciver_id` from wp_user_profile where sender_id = '$currentuserid' AND view_profile = '1') ORDER BY ID ASC limit 5 ");
                ?>
                <div class="part-two Reg2-Part-First controlVisible edit-visibilty">
                    <div class="sec-heading">
                        <h3 class="search-bg-header visibilty-profile">SHOW PROFILE FOR SPECIFIC MEMBERS</h3>
                    </div>
                    <p class="makeyour"><strong>make your profile visible for</strong> a specific member outside of your choice above</p>
                    <div class="all-cont">
                        <div class="all-cont-inner">
                            <div class="label-style label-green">
                                <span class="memb-name">Members name</span>
                                <input type="text" id="autocomplete" value="" class="input-box" />
                                <button type="button" case="members" id="send" class="btn okBtn okbtn-2">OK</button>
                                <div style="display: none;" class="loding">
                                    <img width="60px" height="12px" src="<?php echo get_template_directory_uri(); ?>/images/ajax-status.gif">
                                </div>
                                 <div class="successMsg" style="display:none" id="viewmsg"></div>
                            </div>
                            <div class="table-section">
                                <a href="#" data-toggle="modal" data-target="#viewusers" class="list-block">List of members with view of your profile</a>
                                <div class="table-responsive " id="append_td">				    
                                    <table class="table table-blocked">
                                        <thead>
                                            <tr>
                                                <th class="left-cont">Members</th>
                                                <th class="center-cont">delete view</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($userlist as $val) { ?>
                                                <tr>
                                                    <td class="left-cont"><?php echo $val->display_name; ?></td>
                                                    <td class="center-cont">
                                                        <input type="checkbox" id="search<?php echo $val->ID; ?>" for="search" value="<?php echo $val->ID; ?>" name="" class="checkbox_cus checkname">
                                                        <label for="search<?php echo $val->ID; ?>" class="checkbox_label"></label>
                                                    </td>
                                                </tr>
                                            <?php } ?>					    
                                        </tbody>
                                    </table>				    
                                </div>
                                <div class="button-sec">
                                    <button type="button" id="checked" class="btn okBtn okbtn-red pull-right">OK</button>
                                    <div style="display: none;" class="show_img">
                                        <img width="60px" height="12px" src="<?php echo get_template_directory_uri(); ?>/images/ajax-status.gif">
                                    </div>
                                    <a href="javascript:void(0)" id="userlist" class="arrow">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/down-arrow.png" alt="arrow down" />
                                    </a>
                                    <div id="setting_msg" style="display:none;" class="successMsg"></div>
                                </div>				
                            </div>
                        </div>
                    </div>
                </div>

                <!--- End -->

<!-- Popup for view profile user list-->
<?php
$viewuserlist = $wpdb->get_results("select `ID`,`display_name` from `wp_users` where ID != '$currentuserid' AND ID IN (select `reciver_id` from wp_user_profile where sender_id = '$currentuserid' AND view_profile = '1') ORDER BY ID ASC ");?>
    <div class="modal fade" id="viewusers" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Member Name</h4>
            </div>
            <div class="modal-body">
                <div id ="speed_msg" class="successMsg"></div>
                <div class="editBoth" style="">
                <ul>
                <?php foreach($viewuserlist as $value){ ?>
                    <li style="color:black"><?php echo $value->display_name; ?></li>
                <?php } ?>                
                </ul>                
             </div>

            </div>            
        </div>
    </div>
</div>

<!-- End -->


<!-- Hide Erotics section display for individual user--> 
<?php
$eroticsMemberList = $wpdb->get_results("select `ID`,`display_name` from `wp_users` where ID != '$userid' AND ID IN (select `display_to_user_id` from `wp_hided_erotics_members_list` where `erotics_user_id` = '$userid' AND `display_or_not` = '1') ORDER BY ID ASC limit 5 ");
?>
    <div class="part-two Reg2-Part-First controlVisible edit-visibilty">
    <?php echo ($setting[3]->$user_type != '1') ? "<span class='grayTrans'></span>" : ""; ?>
                    <div class="sec-heading">
                        <h3 class="search-bg-header visibilty-profile red-bg" >Edit hided erotics preference</h3>
                    </div>
                    <p class="makeyour"><strong>Make all your hided erotic preferences </strong> visible for specific members *</p>
                    <div class="all-cont">
                        <div class="all-cont-inner">
                            <div class="label-style label-green">
                                <span class="memb-name">Members name</span>
                                <input type="text" class="input-box" value="" id="erotics_user">
                                <button class="btn okBtn okbtn-2" id="save_erotics_member" case="erotics" type="button">OK</button>
                                <div class="loding_erotics" style="display: none;">
                                    <img width="60px" height="12px" src="<?php echo bloginfo('template_url'); ?>/images/ajax-status.gif">
                                </div>
                                 <div id="viewEroticsMsg" style="display:none" class="successMsg"></div>
                            </div>
                            <div class="table-section">
                                <a class="list-block" data-toggle="modal" data-target="#vieweeroticsuserlist" href="#">
                                List of members with view to your hided erotic preferences</a>
                                <div id="append_ertics_td" class="table-responsive ">                  
                                    <table class="table table-blocked">
                                        <thead>
                                            <tr>
                                                <th class="left-cont">Members</th>
                                                <th class="center-cont">delete view</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($eroticsMemberList as $value) { ?>
                                                <tr>
                                                    <td class="left-cont"><?php echo $value->display_name; ?></td>
                                                    <td class="center-cont">
                                                        <input type="checkbox" id="search<?php echo $value->ID; ?>" for="search" value="<?php echo $value->ID; ?>" name="" class="checkbox_cus eroticsuser">
                                                        <label for="search<?php echo $value->ID; ?>" class="checkbox_label"></label>
                                                    </td>
                                                </tr>
                                            <?php } ?>                      
                                        </tbody>
                                    </table>                    
                                </div>

<!-- for erotics user list-->
<?php
$eroticsUserList = $wpdb->get_results("select `ID`,`display_name` from `wp_users` where ID != '$userid' AND ID IN (select `display_to_user_id` from `wp_hided_erotics_members_list` where `erotics_user_id` = '$userid' AND `display_or_not` = '1') ORDER BY ID ASC ");
?>
    <div class="modal fade" id="vieweeroticsuserlist" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Member Name</h4>
            </div>
            <div class="modal-body">
                <div id ="speed_msg" class="successMsg"></div>
                <div class="editBoth" style="">
                <ul>
                <?php foreach($eroticsUserList as $value){ ?>
                    <li style="color:black"><?php echo $value->display_name; ?></li>
                <?php } ?>                
                </ul>                
             </div>

            </div>            
        </div>
    </div>
</div>

<!-- End -->
                                <div class="button-sec">
                                    <button class="btn okBtn okbtn-red pull-right" id="deletesuser" type="button">OK</button>
                                    <div class="show_erotics" style="display: none;">
                                        <img width="60px" height="12px" src="<?php echo bloginfo('template_url'); ?>/images/ajax-status.gif">
                                    </div>
                                    <a class="arrow" id="eroticsuserlist" href="javascript:void(0)">
                                        <img alt="arrow down" src="<?php echo bloginfo('template_url'); ?>/images/down-arrow.png">
                                    </a>
                                    <div class="successMsg" style="display:none;" id="erotics_msg"></div>
                                </div>              
                            </div>
                        </div>
                    </div>
                </div>
<!-- End -->

                <!-- Block profile -->
                <?php
                $blockuserlist = $wpdb->get_results("select `ID`,`display_name` from `wp_users` where ID != '$currentuserid' AND ID IN (select `reciver_id` from wp_user_profile where sender_id = '$currentuserid' AND block_profile = '1') ORDER BY ID ASC limit 5 ");
                ?>
                <div class="part-three Reg2-Part-First controlVisible edit-visibilty">
                    <div class="sec-heading ">
                        <h3 class="search-bg-header  visibilty-profile">BLOCK PROFILE FOR SPECIFIC MEMBERS</h3>
                        <p class="blocking_para">Blocking members option also blocks mail function</p>
                    </div>
                    <p class="makeyour makeyour-loock">For blocking individual members within your permitted groups above</p>
                    <div class="all-cont">
                        <div class="all-cont-inner">
                            <div class="label-style label-red">
                                <span class="memb-name">Members name</span>
                                <input type="text" id="blockuser" class="input-box" />
                                <button type="button" id="send_data" class="btn okBtn okbtn-red">OK</button>
                                <div style="display: none;" class="loding1">
                                    <img width="60px" height="12px" src="<?php echo get_template_directory_uri(); ?>/images/ajax-status.gif">
                                </div> 
                            </div>
                            <div class="table-section">
                                <div id="setting_msg1" style="display:none;" class="successMsg newtext"></div>
                                <a data-toggle="modal" data-target="#blockuserslist" href="#" class="list-block">List of blocked members</a>
                                <div class="table-responsive">
                                    <div id="append_data" class="">				    
                                        <table class="table table-blocked">
                                            <thead>
                                                <tr>
                                                    <th class="left-cont">Members</th>
                                                    <th class="center-cont">delete blocking</th>
                                                </tr>
                                            </thead>
                                            <tbody>					    
                                                <?php foreach ($blockuserlist as $val) { ?>
                                                    <tr>
                                                        <td class="left-cont"><?php echo $val->display_name; ?></td>
                                                        <td class="center-cont">
                                                            <input type="checkbox" id="blockusersearch<?php echo $val->ID; ?>" value="<?php echo $val->ID; ?>" name="" class="checkbox_cus checkusername">
                                                            <label for="blockusersearch<?php echo $val->ID; ?>" class="checkbox_label"></label>
                                                        </td>
                                                    </tr>
                                                <?php } ?>					    
                                            </tbody>
                                        </table>				    
                                    </div>
                                </div>
                                <div class="button-sec">
                                    <button type="button" id="checked_users" class="btn okBtn pull-right">OK</button>
                                    <a href="javascript:void(0)" id="blockuserlist" class="arrow"><img src="<?php echo get_template_directory_uri(); ?>/images/down-arrow.png" alt="arrow down" /></a>
                                    <div style="display: none;" class="show_img1">
                                        <img width="60px" height="12px" src="<?php echo get_template_directory_uri(); ?>/images/ajax-status.gif">
                                    </div>
                                </div>				
                            </div>
                        </div>
                    </div>
                </div>
            </div>

<!-- Popup for block profile user list-->
<?php
$blocklist = $wpdb->get_results("select `ID`,`display_name` from `wp_users` where ID != '$currentuserid' AND ID IN (select `reciver_id` from wp_user_profile where sender_id = '$currentuserid' AND block_profile = '1') ORDER BY ID ASC");
?>
    <div class="modal fade" id="blockuserslist" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Member Name</h4>
            </div>
            <div class="modal-body">
                <div id ="speed_msg" class="successMsg"></div>
                <div class="editBoth" style="">
                <ul>
                <?php foreach($blocklist as $value){ ?>
                    <li style="color:black"><?php echo $value->display_name; ?></li>
                <?php } ?>                
                </ul>                
             </div>

            </div>            
        </div>
    </div>
</div>

<!-- End -->

            <!-- profile visible for countries -->
            <?php
            if($setting[1]->$user_type == "free definable")
            {
                $country = $wpdb->get_results("select * from `wp_user_profile_visible_country` where userid = '$userid' ");
                if($country[0]->regions != "")
                {
                    $country1 = $country[0]->country;
                    $country2 = $country[1]->country;
                    $country3 = $country[2]->country;
                    $country4 = $country[3]->country;
                    $countryoneregion = (isset($country[0]->regions)) ? unserialize($country[0]->regions) : "";
                    $rowid = (isset($country[0]->id)) ? $country[0]->id : "";
                    $countryoneregion2 = (isset($country[1]->regions)) ? unserialize($country[1]->regions) : "";
                    $rowid1 = (isset($country[1]->id)) ? $country[1]->id : "";
                    $countryoneregion3 = (isset($country[2]->regions)) ? unserialize($country[2]->regions) : "";
                    $rowid2 = (isset($country[2]->id)) ? $country[2]->id : "";
                    $countryoneregion4 = (isset($country[3]->regions)) ? unserialize($country[3]->regions) : "";
                    $rowid3 = (isset($country[3]->id)) ? $country[3]->id : "";
                    }
                    else
                    {
                        $europeCountry = $country[0]->country;
                    }
                }
            else if($setting[1]->$user_type == "reduced function"){                    
                    $result = $wpdb->get_results("select * from `wp_user_profile_visible_country` where userid = '$userid' ");
                    if(count($result) > 0){
                        $country = $result[0]->country;
                        $district = (isset($result[0]->regions)) ? unserialize($result[0]->regions) : "";
                        $rowid = (isset($result[0]->id)) ? $result[0]->id : "";
                    }else{
                        $country = get_user_meta($userid,'country',true);
                        $district = get_user_meta($userid, 'district1', true ); 
                    }                
            }                        
            ?>
            <div class="col-sm-8 no-padding-left"> 
             <?php echo ($setting[1]->$user_type == 'no') ? "<span class='grayTrans'></span>" : ""; ?>           
                <div class="Reg2-Part-First controlVisible">                
                    <h3 class="visibleTitle">GEOGRAFICAL VISIBILITY</h3>
                    <?php if($setting[1]->$user_type == 'free definable'){ ?>
                    <label class="visibleEup" >
                        <input id="allEup" <?php echo ($europeCountry != "") ? "checked" : ""; ?> type="checkbox" class="checkbox_cus" />
                        <label class="checkbox_label1" for="allEup"></label>
                        make my profile visible for all Europe
                    </label>
                    <?php } ?>
                    <div class="padding-country-section">
                        <div id="imgLoding" style="display: none;">
                            <img width="60px" height="12px" src="<?php echo bloginfo('template_url') ?>/images/ajax-status.gif">
                        </div>
                        <div id="forAllcountry" style="display:none" class="successMsg"></div>
                        <p>*)If you do not custom the visibility of your profile it can be seen by all basic members of your region, by all validated members within your country and premium members in Europe.</p>
                        <p class="myPro">Your profile will be only visible in following selected combinations of countries & regions</p>
                        <div class="row" id="hideallCountry"> 
                            <div class=" col-sm-4 col-md-3 col-xs-4">
                            <?php echo ($europeCountry != "") ? "<span class='grayTrans'></span>" : ""; ?>                                
                                <label for="exampleInputEmail1">1<span class="top-text1">st</span> &nbsp;&nbsp; country</label>
                                <select <?php echo ($setting[1]->$user_type == 'reduced function') ? "disabled" : ""; ?> id="country_id" class="country_name form-control country-select" name ="country_id[]"></select>
                                <div class="map-part" style="display:none">
                                    <div class=" col-sm-8 col-md-8 padding-left-10">
                                        <div class=" col-sm-7 col-md-7 no-padding">
                                            <h3 class="map-part-h3"></h3>
                                            <img class="selectImage" src="" contryid="">
                                        </div>
                                        <div class="map-zoom col-sm-5 col-md-5">
                                            <div class="margin-top1 map">
                                                <a href="" class="zoomMap" href="javascript:void(0)">+map
                                                    <img src="<?php echo get_template_directory_uri(); ?>/images/Zoom.png"  class="zoom">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-md-4 padding-right-10">
                                        <div class="margin-top3">
                                            <img src="" countryid="" class="showMap germany-map">
                                        </div>
                                    </div>
                                </div>
                                <div class="">
                                    <div class="margin-top2">
                                        <div class="checkbox">
                                            <input type="checkbox" id="all_regions1" value="married" name="regions" class="checkbox_cus asas">
                                            <label for="all_regions1" class="checkbox_label2"></label> all regions
                                        </div>
                                    </div>
                                    <div class="region-part margin-top3">
                                        <div class="">
                                            <div class="region-part">
                                                <div class="part">
                                                    <div id="state-check" class="statecheck-box" ></div>
                                                    <div class="showImage" id="show"></div>
                                                    <select name ="state_id" id ="state_id" style="display:none;"></select>														
                                                </div>
                                                 <div class="clear"></div>
                                            </div>
                                        </div>
                                    </div>  
                                    <?php if($setting[1]->$user_type == 'free definable'){ ?>
                                        <script  type="text/javascript">
                                            populateCountries("country_id", "state_id", "<?php echo $country1; ?>", "<?php echo $countryoneregion; ?>");
                                        </script>
                                    <?php }else if($setting[1]->$user_type == 'reduced function'){ ?>
                                        <script  type="text/javascript">                                        
                                            populateCountries("country_id", "state_id", "<?php echo $country; ?>", "<?php echo $district; ?>");
                                        </script>
                                    <?php  } ?>
                                    <?php if($setting[0]->$user_type == 'from home region'){ ?>
                                    <script type="text/javascript">
                                    window.onload = function () {
                                        $(".asas").each(function(){
                                            $(this).attr('disabled','disabled');
                                        });
                                    };
                                    </script>
                                    <?php } ?>
                                    
                                </div>                                
                                <div class="other-country-add">
                                <?php if($setting[1]->$user_type == "free definable"){ ?>
                                    <div class="add-country">
                                        <a href="javascript:void(0)" id ="other_country">+ add country</a>
                                    </div>                                    
                                    <button rowid='<?php echo $rowid; ?>' class="btn deleteCount">Delete Country</button>                                 
                                    <button rowid='<?php echo $rowid; ?>' class="btn okBtn sendData" id="send_first_country">OK</button>
                                <?php }else if($setting[1]->$user_type == "reduced function"){ ?>
                                        <button style="display:<?php echo ($rowid == "") ? "none" : "block"; ?>" rowid='<?php echo $rowid; ?>' class="btn deleteCount">Delete Country</button>
                                        <button rowid = '<?php echo $rowid ?>' class="btn okBtn sendData" id="<?php echo ($rowid != "") ? "update_visibilty_details" : "send_first_country" ?>">OK</button>
                                <?php } ?>   
                                </div>
                                <span class="bar_img" style="display:none; text-align: center">
                                    <img width="60px" height="12px" src="<?php echo bloginfo('template_url') ?>/images/ajax-status.gif">
                                </span>
                                <div id="control_vis_success" class="successMsg"></div>
                            </div>
                            <!--- End -->
                            <!-- Start second country functionality -->
                            <div class="col-sm-4 col-md-3" style="display:<?php echo ($country2 != '' ) ? "block" : "none"; ?>;" id="second_country">
                                <label for="exampleInputEmail1">2<span class="top-text1">nd</span> &nbsp;&nbsp; country</label>
                                <select id="country_id1" class="country_name form-control country-select" name ="country_id1[]"></select>
                                <div class="map-part" style="display:none">
                                    <div class=" col-sm-8 col-md-8 col-xs-4 padding-left-10">
                                        <div class=" col-sm-7 col-md-7 col-xs-4 no-padding">
                                            <h3 class="map-part-h3"></h3>
                                            <img class="selectImage" src="" contryid="">
                                        </div>
                                        <div class="map-zoom col-sm-5 col-md-5 col-xs-4">
                                            <div class="margin-top1 map">
                                                <a class="zoomMap" href="">+map
                                                    <img src="<?php echo get_template_directory_uri(); ?>/images/Zoom.png" class="zoom"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class=" col-sm-4 col-md-4 col-xs-4 padding-right-10">
                                        <div class="margin-top3">
                                            <img src="" countryid="" class="showMap germany-map">
                                        </div>
                                    </div>
                                </div>
                                <div class="">
                                    <div class="margin-top2">
                                        <div class="checkbox">
                                            <input type="checkbox" required="" id="all_regions2" value="married" name="regions" class="checkbox_cus">
                                            <label for="all_regions2" class="checkbox_label2"></label> all regions
                                        </div>
                                    </div>
                                    <div class="region-part margin-top3">
                                        <div class="">
                                            <div class="region-part">
                                                <div class="part">
                                                    <div id="state-check2" class="statecheck-box"></div>
                                                    <div class="showImage" id="show"></div>
                                                    <select name ="state_id1" id ="state_id1" style="display:none;"></select>														
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                        </div>
                                        <div class=""></div>
                                    </div>
                                    <script language="javascript" type="text/javascript">
                                        populateCountries("country_id1", "state_id1", "<?php echo $country2; ?>", "<?php echo $countryoneregion2; ?>");
                                    </script>
                                </div>
                                <div class="other-country-add">
                                    <div class="add-country">
                                        <a href="javascript:void(0)" id="show_nextcountry">+ add country</a>
                                    </div>                                    
                                    <button rowid='<?php echo $rowid1; ?>' class="btn deleteCount">Delete Country</button>
                                    <button rowid='<?php echo $rowid1; ?>' class="btn okBtn" id="send_first_country2">OK</button>
                                </div>
                                <span class="bar_img1" style="display:none; text-align: center">
                                    <img width="60px" height="12px" src="<?php echo bloginfo('template_url') ?>/images/ajax-status.gif">
                                </span>
                                <div id="control_vis_success2" class="successMsg"></div>
                            </div>
                            <!-- End -->                       
                            <!--- Start third country functionlaity -->     
                            <div class="col-sm-4 col-md-3 col-xs-4" id="third_country" style="display:<?php echo ($country3 != '') ? "block" : "none"; ?>;">
                                <label for="exampleInputEmail1">3<span class="top-text1">rd</span> &nbsp;&nbsp; country</label>
                                <select id="country_id2" class="country_name form-control country-select" name ="country_id2[]"></select>
                                <div class="map-part" style="display:none">
                                    <div class=" col-sm-8 col-md-8 col-xs-4 padding-left-10">
                                        <div class=" col-sm-7 col-md-7 col-xs-4 no-padding">
                                            <h3 class="map-part-h3"></h3>
                                            <img class="selectImage" src="" contryid="">
                                        </div>
                                        <div class="map-zoom col-sm-5 col-md-5 col-xs-4">
                                            <div class="margin-top1 map">
                                                <a class="zoomMap" href="" > +map
                                                    <img src="<?php echo get_template_directory_uri(); ?>/images/Zoom.png" class="zoom"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class=" col-sm-4 col-md-4 col-xs-4 padding-right-10">
                                        <div class="margin-top3">
                                            <img src="" countryid="" class="showMap germany-map">
                                        </div>
                                    </div>
                                </div>
                                <div class="">
                                    <div class="margin-top2">
                                        <div class="checkbox">
                                            <input type="checkbox" id="all_regions3" value="married" name="regions" class="checkbox_cus">
                                            <label for="all_regions3" class="checkbox_label2"></label> all regions
                                        </div>
                                    </div>
                                    <div class="region-part margin-top3">
                                        <div class="">
                                            <div class="">                                                                                                
                                                <div class="part">
                                                    <div id="state-check3" class="statecheck-box"></div>
                                                    <div class="showImage"></div>
                                                    <select name ="state_id2" id ="state_id2" style="display:none;"></select>														
                                                </div>
                                                 <div class="clear"></div>
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <script language="javascript" type="text/javascript">
                                        populateCountries("country_id2", "state_id2", "<?php echo $country3; ?>", "<?php echo $countryoneregion3; ?>");
                                    </script>
                                </div>
                                <div class="other-country-add"> 
                                     <div class="add-country">
                                        <a href="javascript:void(0)" id="show_nextcountry1">+ add country</a>
                                    </div>                                                                       
                                    <button rowid='<?php echo $rowid2; ?>' class="btn deleteCount">Delete Country</button>
                                    <button rowid='<?php echo $rowid2; ?>' class="btn okBtn" id="send_first_country3">OK</button>
                                </div>
                                <span class="bar_img2" style="display:none; text-align: center">
                                    <img width="60px" height="12px" src="<?php echo bloginfo('template_url') ?>/images/ajax-status.gif">
                                </span>
                                <div id="control_vis_success3" class="successMsg"></div>
                            </div>

                    <!-- Start fourth country functionality -->

                            <div class="col-sm-4 col-md-3 col-xs-4" style="display:<?php echo ($country4 != '') ? "block" : "none"; ?>;" id="fourth_country" style="display:none">
                                <label for="exampleInputEmail1">4<span class="top-text1">th</span> &nbsp;&nbsp; country</label>
                                <select id="country_id3" class="country_name form-control country-select" name ="country_id3[]"></select>
                                <div class="map-part" style="display:none">
                                    <div class=" col-sm-8 col-md-8 col-xs-4 padding-left-10">
                                        <div class=" col-sm-7 col-md-7 col-xs-4 no-padding">
                                            <h3 class="map-part-h3"></h3>
                                            <img class="selectImage" src="" contryid="">
                                        </div>
                                        <div class="map-zoom col-sm-5 col-md-5 col-xs-4">
                                            <div class="margin-top1 map">
                                                <a class="zoomMap" href=""> +map
                                                    <img src="<?php echo get_template_directory_uri(); ?>/images/Zoom.png" class="zoom"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class=" col-sm-4 col-md-4 col-xs-4 padding-right-10">
                                        <div class="margin-top3">
                                            <img src="" countryid="" class="showMap germany-map">
                                        </div>
                                    </div>
                                </div>
                                <div class="">
                                    <div class="margin-top2">
                                        <div class="checkbox">
                                            <input type="checkbox" id="all_regions4" value="married" name="regions" class="checkbox_cus">
                                            <label for="all_regions4" class="checkbox_label2"></label> all regions
                                        </div>
                                    </div>
                                    <div class="region-part margin-top3">
                                        <div class="">
                                            <div class="">                                                                                                
                                                <div class="part">
                                                    <div id="state-check4" class="statecheck-box"></div>
                                                    <div class="showImage"></div>
                                                    <select name ="state_id3" id ="state_id3" style="display:none;"></select>                                                       
                                                </div>  
                                                 <div class="clear"></div>
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <script  type="text/javascript">
                                        populateCountries("country_id3", "state_id3", "<?php echo $country4; ?>", "<?php echo $countryoneregion4; ?>");
                                    </script>
                                </div>
                                <div class="other-country-add">                                                                                                      
                                <button class="btn deleteCount" rowid='<?php echo $rowid3; ?>'>Delete Country</button>
                                <button class="btn okBtn" rowid='<?php echo $rowid3; ?>'  id="send_first_country4">OK</button>
                                </div>
                                <span class="bar_img3" style="display:none; text-align: center">
                                    <img width="60px" height="12px" src="<?php echo bloginfo('template_url') ?>/images/ajax-status.gif">
                                </span>
                                <div id="control_vis_success4" class="successMsg"></div>
                            </div>
                        </div>
                    </div>
                </div>           
            </div>
        </div>
    </div>
</div>
<script>
    var count = 2;
    jQuery('a#userlist').click(function () {
        jQuery(".show_img").show();
        var fromData = 'action=get_user_list&page=' + count + '&type=viewuser';
        jQuery.ajax
                ({
                    url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
                    data: fromData,
                    type: 'POST',
                    success: function (response)
                    {
                        jQuery('div#append_td').html(response);
                        jQuery(".show_img").css("display", "none");
                    }
                });
        count++;

    });
</script>
<!-- autocomplete box -->
<style type="text/css">
    .ui-autocomplete { position: absolute; background:#CCC }
</style>
<script type="text/javascript" src="<?php echo bloginfo('template_url') ?>/js/jquery.autocomplete1.min.js"></script>
<script type="text/javascript">
    var userid = "";
    jQuery("input#autocomplete").on("focus", function (event) {
        var mainmenuSearch_a;
        var options = {
            serviceUrl: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
            onSelect: userlist_search,
            deferRequestBy: 0, //miliseconds
            params: {
                type: 'userlist',
                limit: '10',
                action: 'autocomplete_user_list'
            },
            noCache: true //set to true, to disable caching
        };
        mainmenuSearch_a = jQuery(this).autocomplete(options);
    });
    var userlist_search = function (mainmenuSearch_value, mainmenuSearch_data) {
        userid = mainmenuSearch_data;
        jQuery('input#autocomplete').attr('userid', userid);
    }
</script>

<!-- script for erotics user list -->
<script type="text/javascript">
    var userid = "";
    jQuery("input#erotics_user").on("focus", function (event) {
        var mainmenuSearch_a;
        var options = {
            serviceUrl: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
            onSelect: Eroticsuserlist,
            deferRequestBy: 0, //miliseconds
            params: {
                type: 'Eroticsuserlist',
                limit: '10',
                action: 'autocomplete_user_list'
            },
            noCache: true //set to true, to disable caching
        };
        mainmenuSearch_a = jQuery(this).autocomplete(options);        
    });
    var Eroticsuserlist = function (mainmenuSearch_value, mainmenuSearch_data) {
        userid = mainmenuSearch_data;
        jQuery('input#erotics_user').attr('userid', userid);
    }
</script>
<!-- load more erotics user list -->
<script>
    var count = 2;
    jQuery('a#eroticsuserlist').click(function () {
        jQuery(".show_erotics").show();
        var fromData = 'action=load_erotcis_user_list&page=' + count;
        jQuery.ajax
                ({
                    url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
                    data: fromData,
                    type: 'POST',
                    success: function (response)
                    {
                        jQuery('div#append_ertics_td').html(response);
                        jQuery(".show_erotics").css("display", "none");
                    }
                });
        count++;

    });
</script>
<!-- End -->
<!-- save erotics user id -->
<script>
    jQuery('button#save_erotics_member').click(function () {
        var userid = jQuery('input#erotics_user').attr('userid');
        if (userid != undefined) {
            var fromData = 'action=save_erotics_display_user&userid=' + userid + '&type=save';
            jQuery(".loding_erotics").show();
            jQuery.ajax
                    ({
                        url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
                        data: fromData,
                        type: 'POST',
                        success: function (response)
                        {
                            var result = jQuery.parseJSON(response);
                            jQuery(".loding_erotics").css("display", "none");
                            jQuery('#viewEroticsMsg').show();
                            jQuery('#viewEroticsMsg').html(result.msg);
                            setTimeout(function () {
                                jQuery('#viewEroticsMsg').fadeOut(5000);
                            }, 5000);
                           window.location = self.location;
                        }
                    });
        } else {
            jQuery('#viewEroticsMsg').show();
            jQuery('#viewEroticsMsg').html("Please enter atlest one user name");
            setTimeout(function () {
                jQuery('#viewEroticsMsg').fadeOut(5000);
            }, 5000);
        }
    });
</script>

<!-- end script -->

<!-- checked checkbox for erotcis list -->
<script>
jQuery(document).on('click','.eroticsuser',function () {
        if (this.checked) {
            jQuery(this).attr('checked', 'checked');            
        } else {
            jQuery(this).removeAttr('checked');
        }
    });
</script>
<script>
jQuery('#deletesuser').click(function(){
    var userlist = "";
    jQuery('.eroticsuser').each(function(){
        if (this.checked) 
        {
            userlist += jQuery(this).val() + ',';
        }
    });
    userlist = userlist.replace(/,\s*$/, '');
    jQuery('.show_erotics').show();
    var fromData = 'action=save_erotics_display_user&userlist=' + userlist + '&type=update';
    jQuery.ajax
                ({
                    url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
                    data: fromData,
                    type: 'POST',
                    success: function (response)
                    {
                        var result = jQuery.parseJSON(response);
                        jQuery('.show_erotics').hide();
                        jQuery(".loding").css("display", "none");
                        jQuery('#erotics_msg').show();
                        jQuery('#erotics_msg').html(result.msg);
                        setTimeout(function () {
                            jQuery('#erotics_msg').fadeOut(5000);
                        }, 5000);
                      window.location = self.location;
                    }
                });

})
</script>
<!-- End script -->

<script>
    jQuery('button#send').click(function () {
        var userid = jQuery('input#autocomplete').attr('userid');
        if (userid != undefined) {
            var fromData = 'action=my_action_on_user_profile&userid=' + userid;
            jQuery(".loding").show();
            jQuery.ajax
                    ({
                        url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
                        data: fromData,
                        type: 'POST',
                        success: function (response)
                        {
                            var result = jQuery.parseJSON(response);
                            if(result.txt == 'success')
                            {
                                jQuery(".loding").css("display", "none");
                                jQuery('#viewmsg').show();
                                jQuery('#viewmsg').html(result.msg);
                                setTimeout(function () {
                                    jQuery('#viewmsg').fadeOut(5000);
                                }, 5000);
                               window.location = self.location;
                            }else
                            {
                                jQuery(".loding").css("display", "none");
                                jQuery('#viewmsg').show();
                                jQuery('#viewmsg').html(result.msg);
                                setTimeout(function () {
                                    jQuery('#viewmsg').fadeOut(5000);
                                }, 5000);                              
                            }
                            
                        }
                    });
        } else {
            jQuery('#viewmsg').show();
            jQuery('#viewmsg').html("Please enter atlest one user name");
            setTimeout(function () {
                jQuery('#viewmsg').fadeOut(5000);
            }, 5000);
        }
    });
</script>

<script>
    jQuery('#checked').click(function () {
        var userid = "";
        jQuery('.checkname').each(function () {
            if (jQuery(this).is(':checked')) {
                userid += $(this).val() + ',';
            }
        });
        userid = userid.replace(/,\s*$/, '');
        if (userid.length != 0 || userid != '') {
            jQuery(".show_img").show();
            var fromData = 'action=my_action_on_users_profile&userid=' + userid;
            jQuery.ajax
                    ({
                        url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
                        data: fromData,
                        type: 'POST',
                        success: function (response)
                        {
                            var result = jQuery.parseJSON(response);
                            jQuery(".show_img").css("display", "none");
                            jQuery('#setting_msg').show();
                            jQuery('#setting_msg').html(result.msg);
                            setTimeout(function () {
                                jQuery('#setting_msg').fadeOut(10000);
                            }, 10000);
                            window.location = self.location;
                        }
                    });
        } else {
            jQuery('#setting_msg').show();
            jQuery('#setting_msg').html("Please select atlest one user");
            setTimeout(function () {
                jQuery('#setting_msg').fadeOut(10000);
            }, 10000);
        }
    });

</script>

<script>
    var userid = "";
    jQuery("input#blockuser").on("focus", function (event) {
        var mainmenuSearch_a1;
        var options = {
            serviceUrl: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
            onSelect: userlist_search1,
            deferRequestBy: 0, //miliseconds
            params: {
                type: 'blockuserlist',
                limit: '10',
                action: 'autocomplete_user_list'
            },
            noCache: true //set to true, to disable caching
        };
        mainmenuSearch_a1 = jQuery(this).autocomplete(options);
    });
    var userlist_search1 = function (mainmenuSearch_value1, mainmenuSearch_data1) {
        userid = mainmenuSearch_data1;
        jQuery('input#blockuser').attr('userid', userid);
    }
</script>

<script>
    var count = 2;
    jQuery('a#blockuserlist').click(function () {
        jQuery(".show_img1").show();
        var fromData = 'action=get_block_user_list&page=' + count;
        jQuery.ajax
                ({
                    url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
                    data: fromData,
                    type: 'POST',
                    success: function (response)
                    {
                        jQuery('div#append_data').html(response);
                        jQuery(".show_img1").css("display", "none");
                    }
                });
        count++;
    });
</script>

<script>
    jQuery('button#send_data').click(function () {
        var userid = jQuery('input#blockuser').attr('userid');
        if (userid != undefined || userid != '') {
            var fromData = 'action=my_action_on_block_user_profile&userid=' + userid;
            jQuery(".loding1").show();
            jQuery.ajax
                    ({
                        url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
                        data: fromData,
                        type: 'POST',
                        success: function (response)
                        {
                            var result = jQuery.parseJSON(response);
                            if(result.txt == 'success')
                            {
                                jQuery(".loding1").css("display", "none");
                                jQuery('#setting_msg1').show();
                                jQuery('#setting_msg1').html(result.msg);
                                setTimeout(function () {
                                    jQuery('#setting_msg1').fadeOut(10000);
                                }, 10000);
                                window.location = self.location;
                            }else
                            {
                                jQuery(".loding1").css("display", "none");
                                jQuery('#setting_msg1').show();
                                jQuery('#setting_msg1').html(result.msg);
                                setTimeout(function () {
                                    jQuery('#setting_msg1').fadeOut(10000);
                                }, 10000);
                            }                            
                        }
                    });
        } else {
            jQuery('#setting_msg1').show();
            jQuery('#setting_msg1').html("Please enter atlest one user name");
            setTimeout(function () {
                jQuery('#setting_msg1').fadeOut(10000);
            }, 10000);
        }
    });
</script>



<script>
    jQuery('#checked_users').click(function () {
        var userid = "";
        jQuery('.checkusername').each(function () {
            if (jQuery(this).is(':checked')) {
                userid += $(this).val() + ',';
            }
        });
        userid = userid.replace(/,\s*$/, '');
        if (userid.length != 0 || userid != '') {
            jQuery(".show_img1").show();
            var fromData = 'action=my_action_on_block_users_profile&userid=' + userid;
            jQuery.ajax
                    ({
                        url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
                        data: fromData,
                        type: 'POST',
                        success: function (response)
                        {
                            var result = jQuery.parseJSON(response);
                            jQuery(".show_img1").css("display", "none");
                            jQuery('#setting_msg1').show();
                            jQuery('#setting_msg1').html(result.msg);
                            setTimeout(function () {
                                jQuery('#setting_msg1').fadeOut(2000);
                            }, 3000);
                            window.location = self.location;
                        }
                    });
        } else {
            jQuery('#setting_msg1').show();
            jQuery('#setting_msg1').html("Please select atlest one user");
            setTimeout(function () {
                jQuery('#setting_msg1').fadeOut(2000);
            }, 3000);
        }
    });

</script>


<script>
    $('#for_all').click(function () {
        if (this.checked) {
            $('.checked').each(function () {
                this.checked = true;
            });
            $('#for_all').checked = true;
        } else {
            $('.checked').each(function () {
                this.checked = false;
            });
        }
    });

    $('#chkVisibility').click(function () {
        var str = "";
        $('.checked').each(function () {
            if ($(this).is(':checked')) {
                str += $(this).val() + ',';
            }
        });
        if (str.length == 0) {
            alert('Please check atlest one');
        } else
        {
            $('.visible_loding').show();
            var newstring = str.replace(/,\s*$/, '');
            var intro_data = 'action=check_profile_visibility&data=' + newstring + '&type=' + $(this).attr('case');
            $.ajax({
                url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
                type: 'POST',
                data: intro_data,
                success: function (data) {
                    var result = $.parseJSON(data);
                    $("#edit_visibility").show();
                    $("#edit_visibility").html(result.msg);
                    setTimeout(function ()
                    {
                        jQuery('#edit_visibility').fadeOut(2000);
                    }, 3000);
                    $('.visible_loding').hide();
                }
            });
        }

    });

    $('#search_member').click(function () {
        var name = $('#member_name').val();
        var intro_data = 'action=check_profile_visibility&data=' + name + '&type=' + $(this).attr('case');
        $.ajax({
            url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
            type: 'POST',
            data: intro_data,
            success: function (data) {
                $('#result').html(data);
            }
        });
    });

    var count = 2;
    $('#loadmore').on('click', function () {
        var name = $('#member_name').val();
        $.ajax({
            type: 'POST',
            data: 'data=' + name + '&type=' + $(this).attr('case') + '&currentpage=' + count++,
            url: '<?php echo bloginfo("template_url"); ?>/profile_visible.php',
            success: function (data) {
                $('#result').append(data);
            }
        });
    });  

    $('#block_member').click(function () {
        var name = $('#block_user').val();
        $.ajax({
            type: 'POST',
            data: 'data=' + name + '&type=' + $(this).attr('case'),
            url: '<?php echo bloginfo("template_url"); ?>/profile_visible.php',
            success: function (data) {
                $('#blockuser').html(data);
            }
        });
    });
    
</script>

<!--   First country -->
<script>
    jQuery('#other_country').click(function () {
        jQuery('#second_country').show();
    });
    jQuery('#show_nextcountry').click(function () {
        jQuery('#third_country').show();
    });
    jQuery('#show_nextcountry1').click(function () {
        jQuery('#fourth_country').show();
    });
</script>


<script>
    jQuery('#all_regions1').click(function () {
        if (this.checked == true)
        {
            jQuery('#state-check input[type="checkbox"]').each(function () {
                jQuery(this).attr('checked', 'checked');
            });
        } else {
            jQuery('#state-check input[type="checkbox"]').each(function () {
                jQuery(this).attr('checked', false);
            });
        }
    });
</script>

<script>
    jQuery('#send_first_country').click(function () {
        jQuery(".bar_img").show();
        var rowid = jQuery(this).attr('rowid');
        if(rowid == "")
        {
            var actiontype = 'insert';
        }
        else
        {
            var actiontype = 'update';
        }
        jQuery(".countryone").css("display", "block");
        var firstcountry = new Array();
        jQuery('#state-check :checked').each(function () {
            firstcountry.push(jQuery(this).val());
        });
        var countryname = jQuery('#country_id').val();
        var fromData = 'action=user_profile_visible_country&country=' + countryname + '&regions=' + firstcountry + '&rowid='+rowid + '&actiontype='+actiontype;
        jQuery.ajax
                ({
                    url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
                    data: fromData,
                    type: 'POST',
                    success: function (response)
                    {
                        setTimeout(function () {
                            jQuery('.bar_img').fadeOut(2000);
                        }, 3000);
                        var result = jQuery.parseJSON(response);
                        jQuery(".countryone").css("display", "none");
                        jQuery('#control_vis_success').show();
                        jQuery('#control_vis_success').html(result.msg);
                        setTimeout(function () {
                            jQuery('#control_vis_success').fadeOut(2000);
                        }, 3000);
                        window.location = self.location;                        
                    }
                });
    });
</script>
<!-- end -->

<!-- update fist country update -->

<script>
    jQuery('#update_visibilty_details').click(function () {
        jQuery(".bar_img").show();
        jQuery(".countryone").css("display", "block");
        var rowid = jQuery(this).attr('rowid');
        var firstcountry = new Array();
        jQuery('#state-check :checked').each(function () {
            firstcountry.push(jQuery(this).val());
        });
        var countryname = jQuery('#country_id').val();
        var fromData = 'action=update_user_profile_visible_country&country=' + countryname + '&regions=' + firstcountry + '&rowid='+rowid;
        jQuery.ajax
                ({
                    url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
                    data: fromData,
                    type: 'POST',
                    success: function (response)
                    {
                        jQuery('.bar_img').hide();                        
                        var result = jQuery.parseJSON(response);
                        jQuery(".countryone").css("display", "none");
                        jQuery('#control_vis_success').show();
                        jQuery('#control_vis_success').html(result.msg);
                        setTimeout(function () {
                            jQuery('#control_vis_success').fadeOut(10000);
                        }, 10000);
                      window.location = self.location;
                    }
                });
    });
</script>
<!-- second country -->
<script>
    jQuery(document).on('click', '#all_regions2', function () {
        if (this.checked == true)
        {
            jQuery('#state-check2 input[type="checkbox"]').each(function () {
                jQuery(this).attr('checked', 'checked');
            });
        } else {
            jQuery('#state-check2 input[type="checkbox"]').each(function () {
                jQuery(this).attr('checked', false);
            });
        }
    });
</script>

<script>
    jQuery('#send_first_country2').click(function () {
        jQuery('.bar_img1').show();
        jQuery(".countryone").css("display", "block");
        var rowid = jQuery(this).attr('rowid');
        if(rowid == "")
        {
            var actiontype = 'insert';
        }
        else
        {
            var actiontype = 'update';
        }
        var secondcountry = new Array();
        jQuery('#state-check2 :checked').each(function () {
            secondcountry.push(jQuery(this).val());
        });
        var countryname = jQuery('#country_id1').val();
        var fromData = 'action=user_profile_visible_country&country=' + countryname + '&regions=' + secondcountry + '&rowid='+rowid + '&actiontype='+actiontype;
        jQuery.ajax
                ({
                    url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
                    data: fromData,
                    type: 'POST',
                    success: function (response)
                    {
                        var result = jQuery.parseJSON(response);
                        jQuery('.bar_img1').hide();
                        jQuery('#control_vis_success2').show();
                        jQuery('#control_vis_success2').html(result.msg);
                        setTimeout(function () {
                            jQuery('#control_vis_success2').fadeOut(2000);
                        }, 3000);
                        window.location = self.location;                        
                    }
                });
    });
</script>
<!-- end -->

<!-- third country -->

<script>
    jQuery(document).on('click', '#all_regions3', function () {
        if (this.checked == true)
        {
            jQuery('#state-check3 input[type="checkbox"]').each(function () {
                jQuery(this).attr('checked', 'checked');
            });
        } else {
            jQuery('#state-check3 input[type="checkbox"]').each(function () {
                jQuery(this).attr('checked', false);
            });
        }
    });
</script>

<script>
    jQuery('#send_first_country3').click(function () {
        jQuery(".bar_img2").show();
        jQuery(".countryone").css("display", "block");
        var rowid = jQuery(this).attr('rowid');
        if(rowid == "")
        {
            var actiontype = 'insert';
        }
        else
        {
            var actiontype = 'update';
        }
        var thirdcountry = new Array();
        jQuery('#state-check3 :checked').each(function () {
            thirdcountry.push(jQuery(this).val());
        });
        var countryname = jQuery('#country_id2').val();
        var fromData = 'action=user_profile_visible_country&country=' + countryname + '&regions=' + thirdcountry + '&rowid='+rowid + '&actiontype='+actiontype;
        jQuery.ajax
                ({
                    url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
                    data: fromData,
                    type: 'POST',
                    success: function (response)
                    {
                        var result = jQuery.parseJSON(response);
                        jQuery('.bar_img2').hide();
                        jQuery('#control_vis_success3').show();
                        jQuery('#control_vis_success3').html(result.msg);
                        setTimeout(function () {
                            jQuery('.control_vis_success3').fadeOut(2000);
                        }, 3000);
                        window.location = self.location;                        
                    }
                });
    });
</script>

<script>
    jQuery('.deleteCount').click(function () {
        var attrClass = jQuery(this).parent('.other-country-add').next('span').attr('class');
        jQuery('.' + attrClass).show();
        var rowid = jQuery(this).attr('rowid');
        var fromData = 'action=user_profile_visible_country_delete&rowid=' + rowid;
        jQuery.ajax
                ({
                    url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
                    data: fromData,
                    type: 'POST',
                    success: function (response)
                    {
                        setTimeout(function () {
                            jQuery('.' + attrClass).fadeOut(2000);
                        }, 3000);
                        window.location = self.location;
                    }
                });
    });

</script>

<!-- end -->
<!-- foourth country -->
<script>
    jQuery('#send_first_country4').click(function () {
        jQuery(".bar_img3").show();
        jQuery(".countryone").css("display", "block");
        var rowid = jQuery(this).attr('rowid');
        if(rowid == "")
        {
            var actiontype = 'insert';
        }
        else
        {
            var actiontype = 'update';
        }
        var fourthcountry = new Array();
        jQuery('#state-check4 :checked').each(function () {
            fourthcountry.push(jQuery(this).val());
        });
        var countryname = jQuery('#country_id3').val();
        var fromData = 'action=user_profile_visible_country&country=' + countryname + '&regions=' + fourthcountry + '&rowid='+rowid + '&actiontype='+actiontype;
        jQuery.ajax
                ({
                    url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
                    data: fromData,
                    type: 'POST',
                    success: function (response)
                    {
                        var result = jQuery.parseJSON(response);
                        jQuery('.bar_img3').hide();
                        jQuery('#control_vis_success4').show();
                        jQuery('#control_vis_success4').html(result.msg);
                        setTimeout(function () {
                            jQuery('.control_vis_success4').fadeOut(2000);
                        }, 3000);
                        window.location = self.location;                    
                    }
                });
    });
</script>
<script>
    jQuery('#all_regions4').click(function () {
        if (this.checked == true)
        {
            jQuery('#state-check4 input[type="checkbox"]').each(function () {
                jQuery(this).attr('checked', 'checked');
            });
        } else {
            jQuery('#state-check4 input[type="checkbox"]').each(function () {
                jQuery(this).attr('checked', false);
            });
        }
    });
</script>
<!--- check all europe country -->
<script>
    jQuery('#allEup').click(function(){     
    var str = 'Austria,'+'Belgium,'+'France,'+'Germany,'+'Italy,'+'Netherlands,'+'Spain,'+'Switzerland';  
       if (this.checked == true){ 
            jQuery('#imgLoding').show();           
            jQuery('#hideallCountry').attr('disabled','disabled');
            var fromData = 'action=profile_visible_allEurope_country&europeCountries=' + str + '&type=insert';
            jQuery.ajax
                ({
                    url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
                    data: fromData,
                    type: 'POST',
                    success: function (response)
                    {
                        var result = jQuery.parseJSON(response);
                        jQuery('#forAllcountry').show();
                        jQuery('#forAllcountry').html(result.msg); 
                        jQuery('#imgLoding').hide(); 
                        setTimeout(function () 
                        {
                           jQuery('#forAllcountry').fadeOut(7000);
                        }, 7000);
                        window.location = self.location;
                    }
                });
        } 
        else 
        {
            jQuery('#imgLoding').show();
            jQuery('#hideallCountry').removeAttr('disabled');
            var fromData = 'action=profile_visible_allEurope_country&type=delete';
            jQuery.ajax
                ({
                    url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
                    data: fromData,
                    type: 'POST',
                    success: function (response)
                    {
                        var result = jQuery.parseJSON(response);
                        jQuery('#imgLoding').hide(); 
                        jQuery('#forAllcountry').show();
                        jQuery('#forAllcountry').html(result.msg);
                        setTimeout(function () 
                        {
                           jQuery('#forAllcountry').fadeOut(7000);
                        }, 7000);
                        window.location = self.location;
                    }
                });
        }        
    });
</script>
<script>
jQuery(document).ready(function() {
  jQuery('.zoomMap').magnificPopup({type:'image'});
});
</script>

<?php get_footer(); ?>
