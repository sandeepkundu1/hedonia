<?php
/*
 * Template Name: Couple My Profile 
 * Description: A Page Template with a Change Profile Staus.
 * Author Name : Sanjay Shinghania
 */
 global $wpdb;
 get_header();

 $textColor = array(
            "182" => "#395723", 
            "183" => "#476D2D", 
            "184" => "#61953D", 
            "185" => "#A9D12A",
            "186" => "#E2F0D9", 
            "187" => "#ECF5E7", 
            "706" => "#F8FBF7", 
            "707" => "#F2F2F2", 
            "708" => "#F2F2F2", 
            "709" => "#F2F2F2", 
            "710" => "#F2F2F2", 
            "711" => "#F2F2F2", 
            "712" => "#F2F2F2", 
            "713" => "#F2F2F2", 
            "714" => "#F2F2F2", 
            "715" => "#FF3F3F",
            "716" => "#C00000"             
        );

    $forColor = array(
            "182" => "#fff", 
            "183" => "#fff", 
            "184" => "#fff", 
            "185" => "#fff",
            "186" => "#000", 
            "187" => "#000", 
            "706" => "#000", 
            "707" => "#000", 
            "708" => "#000", 
            "709" => "#000", 
            "710" => "#000", 
            "711" => "#000", 
            "712" => "#000", 
            "713" => "#000", 
            "714" => "#000", 
            "715" => "#000", 
            "716" => "#000"            
        );

$get_user_info = get_user_meta($current_user->ID);
$userid =  $current_user->ID;
$get_user_info = get_user_meta($current_user->ID);
$user_type_name =  get_user_meta($current_user->ID ,'user_type', true );
$user_alb_count=$album_setting[0]->$user_type_name;
# Profilepick for couples
$upload_dir = wp_upload_dir();
$image_url_female = $upload_dir['baseurl'] . '/profilepick_' . $current_user->ID . '/' . $get_user_info['her_pick']['0'];
$image_url_male = $upload_dir['baseurl'] . '/profilepick_' . $current_user->ID . '/' . $get_user_info['his_pick']['0'];
# End Profilepick for couples
# Age for couples
$age1 = $get_user_info['she_dob']['0'];
$age2 = $get_user_info['he_dob']['0'];
$current_date = date();

$datetime1 = new DateTime($current_date);
$datetime2 = new DateTime($age1);
$her_age = $datetime1->diff($datetime2);
$female_age = $her_age->y;

$datetime3 = new DateTime($current_date);
$datetime4 = new DateTime($age2);
$his_age = $datetime3->diff($datetime4);
$male_age = $his_age->y;

$her_hight = $get_user_info['her_hight']['0'];
$his_hight = $get_user_info['his_hight']['0'];


$her_figure = $get_user_info['her_figure']['0'];
$explodeHerfigure = explode('(', $her_figure);

$his_figure = $get_user_info['his_figure']['0'];
$explodeHisFigure = explode('(', $his_figure);

$fav_quote_content = $get_user_info['faq_quote']['0'];
$her_shave = $get_user_info['her_shave']['0'];
$his_shave = $get_user_info['his_shave']['0'];

$her_tattoos = $get_user_info['her_tattoos']['0'];
$his_tattoos = $get_user_info['his_tattoos']['0'];

$her_smoke = $get_user_info['her_smoke']['0'];
$his_smoke = $get_user_info['his_smoke']['0'];

$her_language = $get_user_info['her_language']['0'];
$her_language1 = $get_user_info['her_language1']['0'];
$her_language2 = $get_user_info['her_language2']['0'];
$her_language3 = $get_user_info['her_language3']['0'];

$selectedLanguages = "";
if ($her_language != "") {
    $selectedLanguages .= $her_language . ",";
}
if ($her_language1 != "") {
    $selectedLanguages .= $her_language1 . ",";
}
if ($her_language2 != "") {
    $selectedLanguages .= $her_language2 . ",";
}
if ($her_language3 != "") {
    $selectedLanguages .= $her_language3 . ",";
}

$language_array = array($her_language, $her_language1, $her_language2, $her_language3);
$his_language = $get_user_info['his_language']['0'];
$his_language1 = get_user_meta($current_user->ID, 'his_language1', true);
$his_language2 = get_user_meta($current_user->ID, 'his_language2', true);
$his_language3 = get_user_meta($current_user->ID, 'his_language3', true);
$selectedLanguages1 = "";

if ($his_language != "") {
    $selectedLanguages1 .= $his_language . ",";
}
if ($his_language1 != "") {
    $selectedLanguages1 .= $his_language1 . ",";
}
if ($his_language2 != "") {
    $selectedLanguages1 .= $his_language2 . ",";
}
if ($his_language3 != "") {
    $selectedLanguages1 .= $his_language3 . ",";
}

$language_array1 = array($his_language, $secondLang, $thrd_lang, $forLang);

$her_weight = $get_user_info['her_weight']['0'];
$his_weight = $get_user_info['his_weight']['0'];

$her_profession = $get_user_info['her_profession']['0'];
$his_profession = $get_user_info['his_profession']['0'];

$her_hobby = $get_user_info['her_hobby']['0'];
$his_hobby = $get_user_info['his_hobby']['0'];

$describe = $get_user_info['describe']['0'];
$des_status = $get_user_info['describe_status']['0'];

$married = $get_user_info['married']['0'];
$notmarried = $get_user_info['notmarried']['0'];
$seprated = $get_user_info['seprated']['0'];

$nickname = $get_user_info['nickname']['0'];
$first_name = $get_user_info['first_name']['0'];
$last_name = $get_user_info['last_name']['0'];

$country = $get_user_info['country']['0'];
$second_country = $get_user_info['country_second']['0'];

$district1 = $get_user_info['district1']['0'];
$second_district = $get_user_info['district_second']['0'];

$city1 = $get_user_info['city1']['0'];
$second_city = $get_user_info['city_second']['0'];

$reg3_txtshe = $get_user_info['reg3_txtshe']['0'];
$reg3_txthe = $get_user_info['reg3_txthe']['0'];
$reg3_txtboth = $get_user_info['reg3_both']['0'];

# End Age for couples
#change user type
$user_type_name = get_user_meta($current_user->ID, 'user_type', true);
$profile_name = get_option($user_type_name);

//perfect match
$profile_name = get_option($user_type_name);
$setting = $wpdb->get_results("select * from wp_admin_portal_setting where portal_setting_type = 'portal setting' ");

$id = get_the_ID();
$page_title = get_page($id);
$page_title->post_title;
?>
<style type="text/css">
    .marker {
        background-color: #ffff00;
    }
</style>

<?php
if (is_user_logged_in()) {
    ?>
    <div class="userProfile">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <form class="form-horizontal" role="form" name="form" method="post">
                        <div class="col-sm-7 no-padding-right">
                            <div class="col-sm-6 no-padding profilePicWrap"> 
                                <?php if ($get_user_info['approved_as_basic']['0'] == 'no') { ?>          
                                    <div class="col-sm-6 no-padding-left">
                                        <div class="posRel">                                           
                                            <h3 class="search_premium_heading red_heading">
                                            <?php echo $get_user_info['nickname']['0']; ?>
                                            </h3>
                                                <?php if ($get_user_info['her_pick']['0'] != 'no_img.jpg') { ?>
                                                <img class="prof-cartoon-img" src="<?php echo $image_url_female; ?>">
                                                <?php } else { ?>
                                                <img class="prof-cartoon-img" src="<?php echo bloginfo('template_url'); ?>/page-templates/images/images.jpeg">
                                                <?php } ?>  
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 no-padding-left">
                                        <div class="posRel">                                                                                
                                            <?php if ($get_user_info['his_pick']['0'] == 'no_img.jpg') { ?> 
                                                <img class="prof-cartoon-img" src="<?php echo bloginfo('template_url'); ?>/page-templates/images/images.jpeg">                         
                                            <?php } else { ?>
                                                <img class="prof-cartoon-img" src="<?php echo $image_url_male; ?>">
                                            <?php } ?>
                                        </div>  
                                    </div>                                  
                                    <?php } else { ?>  
                                    <div class="col-sm-6 no-padding-left">
                                        <h3 class="search_premium_heading red_heading"><?php echo $get_user_info['nickname']['0']; ?></h3>
                                        <?php if ($get_user_info['her_pick']['0'] != 'no_img.jpg') { ?>
                                            <img class="prof-cartoon-img" src="<?php echo $image_url_female; ?>">
                                        <?php } else { ?>
                                            <img class="prof-cartoon-img" src="<?php echo bloginfo('template_url'); ?>/page-templates/images/images.jpeg">
                                        <?php } ?>  
                                    </div>
                                    <div class="col-md-6 col-sm-6 no-padding-left">                              
                                        <?php if ($get_user_info['his_pick']['0'] == 'no_img.jpg') { ?> 
                                            <img class="prof-cartoon-img" src="<?php echo bloginfo('template_url'); ?>/page-templates/images/images.jpeg">                          
                                        <?php } else { ?>
                                            <img class="prof-cartoon-img" src="<?php echo $image_url_male; ?>">
                                        <?php } ?>
                                    </div>  
                                    <?php } ?>  
                            </div>
                            <div class="col-sm-6 no-padding">
                                    <?php if ($user_type_name == 'premium') { ?>
                                    <h5 class="red profileType">
                                    <?php if ($profile_name) {
                                        echo $profile_name;
                                    } else {
                                        echo "Premium Member";
                                    } ?>
                                    </h5>
                                    <?php } elseif ($user_type_name == 'validated') { ?>   
                                    <h5 class="greenText profileType">
                                    <?php echo "Validated Member"; ?>
                                        <img width="12" src="<?php echo bloginfo('template_url'); ?>/images/tick-green.png" alt="">
                                    </h5>
                                    <?php } elseif ($user_type_name == 'basic' && $get_user_info['approved_as_basic']['0'] == 'yes') { ?>
                                    <h5 class="greenText profileType" style="color:#989898;">
                                    <?php if ($profile_name) {
                                        echo $profile_name;
                                    } else {
                                        echo "Basic Member";
                                    } ?>
                                    </h5>
                                    <?php } elseif ($get_user_info['approved_as_basic']['0'] == 'no') { ?>
                                    <h5 class="greenText profileType" style="color:#989898;"><?php echo "Pre-membership stage"; ?></h5>
                                    <?php } ?>

                                <div class="Profile-Part-BI cus_scroll">                                    
                                    <h3 class="Proh-h3"><span>BASIC INFO</span></h3>
                                    <div class="row">
                                        <p class="Pro-p col-sm-4 col-xs-offset-4">she</p>
                                        <p class="Pro-p col-sm-4">he</p> 
                                    </div>  
                                    <div class="padding-basic-info">
                                        <div class="form-group border-bottom">
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Age</label>
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center bold"><?php echo $female_age; ?></label>
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center bold"><?php echo $male_age; ?></label>
                                        </div>
                                        <div class="form-group border-bottom">
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Height (cm)</label>
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center bold"><?php echo $her_hight; ?></label>
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center bold"><?php echo $his_hight; ?></label>
                                        </div>
                                        <div class="form-group border-bottom">
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Figure</label>
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label normal para-center">
                                                <span class="normalText"><?php echo $explodeHerfigure[0]; ?></span>
                                                <span class="normalText"><?php echo ($explodeHerfigure[1] != '') ? str_replace(')', '', $explodeHerfigure[1]) : ''; ?></span>
                                            </label>
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label normal para-center">
                                                <span class="normalText"><?php echo $explodeHisFigure[0]; ?></span>
                                                <span class="normalText"><?php echo ($explodeHisFigure[1] != '') ? str_replace(')', '', $explodeHisFigure[1]) : ''; ?></span>
                                            </label>
                                        </div>
                                        <div class="form-group border-bottom">
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Relationship</label>
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center"><?php echo $get_user_info['relation_ship']['0']; ?></label>
                                        </div>
                                        <div class="form-group border-bottom">
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Smoking</label>
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center"><?php echo $her_smoke; ?></label>
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center"><?php echo $his_smoke; ?></label>
                                        </div>

                                        <div class="form-group border-bottom">
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Hair color</label>
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center"><?php echo $get_user_info['her_haircolor']['0']; ?></label>
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center"><?php echo $get_user_info['his_haircolor']['0']; ?></label>
                                        </div>                                        
                                        <div class="form-group border-bottom">
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Languages</label>
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label">
                                            <?php echo str_replace(',', ', ', rtrim($selectedLanguages, ",")); ?>
                                            </label>
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label">
                                            <?php echo str_replace(',', ', ', rtrim($selectedLanguages1, ",")); ?>
                                            </label>
                                        </div>

                                        <div class="form-group border-bottom">
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Education</label>
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center"><?php echo $get_user_info['her_graduation']['0']; ?></label>
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center"><?php echo $get_user_info['his_graduation']['0']; ?></label>
                                        </div>                                        
                                        <div class="form-group border-bottom">
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Hobbies Interests</label>
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label"><?php echo $her_hobby; ?></label>
                                            <label for="inputEmail3" class="col-sm-4 BasicInfo-label"><?php echo $his_hobby; ?></label>
                                        </div>
    <?php
    if (isset($_GET['user_id'])) {
        if ($current_user->ID == $_GET['user_id']) {
            ?>
                                                <div class="form-group">
                                                    <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Additional info</label>
                                                    <label for="inputEmail3" class="col-sm-4 BasicInfo-label"><?php echo $get_user_info['about_her']['0']; ?></label>
                                                    <label for="inputEmail3" class="col-sm-4 BasicInfo-label"><?php echo $get_user_info['about_his']['0']; ?></label>
                                                </div>
                                            <?php }
                                        } else { ?>
                                            <div class="form-group">
                                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Additional info</label>
                                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label"><?php echo $get_user_info['about_her']['0']; ?></label>
                                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label"><?php echo $get_user_info['about_his']['0']; ?></label>
                                            </div>
    <?php } ?>
                                    </div>
                                </div>
                            </div>

                            <div class="clear">
                                <div class="col-sm-4 col-md-4 col-xs-12 no-padding">
                                    <div class="list_div">
                                        <ul class="side-bar-profile no-padding">
                                            <?php if ($get_user_info['approved_as_basic']['0'] == 'no') { ?>
                                                <li class="invalid"><span class="grayTrans"></span><a href="javascript:void(0)"><i>
                                                            <img src="<?php echo bloginfo("template_url"); ?>/images/hand.jpg">
                                                        </i><span>i like add to favourites</span></a></li>

                                                <li class="invalid"><span class="grayTrans"></span><a href="javascript:void(0)"><i><img src="<?php echo bloginfo("template_url"); ?>/images/lip.jpg"></i><span>send a kiss</span></a></li>

                                                <li class="invalid"><span class="grayTrans"></span><a href="javascript:void(0)"><i><img src="<?php echo bloginfo("template_url"); ?>/images/mes1.jpg"></i><span>send mail</span></a></li>

                                                <li class="invalid"><span class="grayTrans"></span><a href="javascript:void(0)"><i><img src="<?php echo bloginfo("template_url"); ?>/images/mes3.jpg"></i><span>mail history</span></a></li>

                                                <li class="invalid"><span class="grayTrans"></span><a href="javascript:void(0)"><i><img src="<?php echo bloginfo("template_url"); ?>/images/lockband.jpg"></i><span>hide/show profile<br/> open/close albums<br/> erotic preferences</span></a></li>

                                                <li class="invalid"><span class="grayTrans"><a href="javascript:void(0)"></span><i><img src="<?php echo bloginfo("template_url"); ?>/images/note.jpg"></i><span>make a note<br/> for this profile</span></a></li>

                                                <li class="invalid"><span class="grayTrans"></span><a href="javascript:void(0)"><i><img src="<?php echo bloginfo("template_url"); ?>/images/block.jpg"></i><span>block this profile</span></a></li>

                                                <li class="invalid"><span class="grayTrans"></span><a href="javascript:void(0)"><i><img src="<?php echo bloginfo("template_url"); ?>/images/submit.jpg"></i><span>submit complaint about this profile</span></a></li>

                                            <?php  } //elseif ($get_user_info['user_type']['0'] == 'premium') { 
                                           else { $get_user_info['user_type']['0']=$get_user_info['user_type']['0'];
                                            ?>
                                                                         
                                                                                                       
                                       <?php  if($operational_setting[3]->$user_type_name != 1){ ?>
                                <li style="position:relative;"> 
									<span class="grayTrans"></span>
								<?php } else { ?>
									<li>
								<?php } ?>	
                                    <a href = "#"><i>
                                        <img src="<?php echo bloginfo("template_url"); ?>/images/hand.jpg">
                                        </i><span>i like<br/>   add to favourites</span>
                                    </a>
                                </li>
                                 
                               <?php  if($operational_setting[5]->$user_type_name != 1){ ?>
                                <li style="position:relative;"> 
									<span class="grayTrans"></span>
								<?php } else { ?>
									<li>
								<?php } ?>	
                                    <a href = "#">
                                        <i><img src="<?php echo bloginfo("template_url"); ?>/images/lip.jpg"></i>
                                        <span>send a kiss</span>
                                    </a>
                                </li>
                  
									<li>
								
                                    <a href ="#" >
                                        <i><img src="<?php echo bloginfo("template_url"); ?>/images/mes1.jpg"></i>
                                        <span>send mail</span>
                                    </a>
                                </li>
                               <?php  if($operational_setting[2]->$user_type_name < 0 ){ ?>
                                <li style="position:relative;"> 
									<span class="grayTrans"></span>
								<?php } else { ?>
									<li>
								<?php } ?>	
                                    <i><img src="<?php echo bloginfo("template_url"); ?>/images/mes3.jpg"></i>
                                    <span>mail history</span>
                                </li>
       
								<li>
                                <?php echo ($user_type_name == 'premium') ? "" : "<span style='height:50px;' class='grayTrans'></span>"; ?>							
                                    <i><img src="<?php echo bloginfo("template_url"); ?>/images/lockband.jpg"></i>
                                    <span>hide/show profile<br/> open/close albums<br/> erotic preferences</span>
                                </li>
                                <li style="position:relative;"> 
                                 <?php if($operational_setting[6]->$user_type_name != 1){ ?>                                
									<span class="grayTrans"></span>
								<?php } ?>	
                                    <i><img src="<?php echo bloginfo("template_url"); ?>/images/note.jpg"></i>
                                    <span>make a note <br/>for this profile</span>
                                </li>      
							<?php if ($get_user_info['user_type']['0'] == 'basic') {?>     
                                <li style="position:relative;"> 
									<span class="grayTrans"></span>	
							
                                    <i><img src="<?php echo bloginfo("template_url"); ?>/images/block.jpg"></i>
                                    <span>block this profile</span>
                                </li>
                     
                                <li style="position:relative;"> 
									<span class="grayTrans"></span>	
							
                                    <i><img src="<?php echo bloginfo("template_url"); ?>/images/submit.jpg"></i>
                                    <span>submit complaint<br/> about this profile</span>
                                </li>
                                <?php } else { ?>
                                
                                <li> 
                                    <i><img src="<?php echo bloginfo("template_url"); ?>/images/block.jpg"></i>
                                    <span>block this profile</span>
                                </li>
                                <li> 
							     <i><img src="<?php echo bloginfo("template_url"); ?>/images/submit.jpg"></i>
                                    <span>submit complaint<br/> about this profile</span>
                                </li>
                                <?php } } /* elseif ($get_user_info['user_type']['0'] == 'validated') { ?>


                                                <li>
                                                    <a href = "#"><i><img src="<?php echo bloginfo("template_url"); ?>/images/hand.jpg"></i><span>i like<br/>   add to favourites</span></a></li>

                                                <li class="invalid"><span class="grayTrans"></span><a href = "#"><i><img src="<?php echo bloginfo("template_url"); ?>/images/lip.jpg"></i><span>send a kiss</span></a></li>

                                                <li><a href ="#" ><i><img src="<?php echo bloginfo("template_url"); ?>/images/mes1.jpg"></i><span>send mail</span></a></li>

                                                <li><a href ="#" ><i><img src="<?php echo bloginfo("template_url"); ?>/images/mes3.jpg"></i><span>mail history</span></a></li>

                                                <li class="invalid"><div class="clear"></div><span class="grayTrans"></span><a href ="#" ><i><img src="<?php echo bloginfo("template_url"); ?>/images/lockband.jpg"></i><span>hide/show profile<br/> open/close albums<br/> erotic preferences</span></a></li>

                                                <li class="invalid"><div class="clear"></div><span class="grayTrans"></span><a href ="#" ><i><img src="<?php echo bloginfo("template_url"); ?>/images/note.jpg"></i><span>make a note <br/>for this profile</span></a></li>

                                                <li><a href ="#" ><i><img src="<?php echo bloginfo("template_url"); ?>/images/block.jpg"></i><span>block this profile</span></a></li>

                                                <li><a href ="#" ><i><img src="<?php echo bloginfo("template_url"); ?>/images/submit.jpg"></i><span>submit complaint<br/> about this profile</span></a></li>


                                            <?php } elseif ($get_user_info['user_type']['0'] == 'basic') { ?>



                                                <li class="invalid"><span class="grayTrans"></span><a href = "#"><i>
                                                            <img src="<?php echo bloginfo("template_url"); ?>/images/hand.jpg">
                                                        </i><span>i like add to favourites</span></a></li>

                                                <li class="invalid"><span class="grayTrans"></span><a href = "#"><i><img src="<?php echo bloginfo("template_url"); ?>/images/lip.jpg"></i><span>send a kiss</span></a></li>

                                                <li><a href ="#" ><i><img src="<?php echo bloginfo("template_url"); ?>/images/mes1.jpg"></i><span>send mail</span></a></li>

                                                <li><a href ="#" ><i><img src="<?php echo bloginfo("template_url"); ?>/images/mes3.jpg"></i><span>mail history</span></a></li>

                                                <li class="invalid"><span class="grayTrans"></span><a href ="#" ><i><img src="<?php echo bloginfo("template_url"); ?>/images/lockband.jpg"></i><span>hide/show profile<br/> open/close albums<br/> erotic preferences</span></a></li>

                                                <li class="invalid"><span class="grayTrans"><a href ="#" ></span><i><img src="<?php echo bloginfo("template_url"); ?>/images/note.jpg"></i><span>make a note<br/> for this profile</span></a></li>

                                                <li class="invalid"><span class="grayTrans"></span><a href ="#" ><i><img src="<?php echo bloginfo("template_url"); ?>/images/block.jpg"></i><span>block this profile</span></a></li>

                                                <li class="invalid"><span class="grayTrans"></span><a href ="#" ><i><img src="<?php echo bloginfo("template_url"); ?>/images/submit.jpg"></i><span>submit complaint about this profile</span></a></li>

                                            <?php }*/ ?>
                                        </ul>
                                    </div>
                                    <div class="clear"></div>
                                    <hr/>
                                    <span class="border-bottom"></span>

<!---******************* for speed dating **************-->
                                    <?php if($operational_setting[9]->$user_type_name  == 1 && $get_user_info['approved_as_basic']['0'] != 'no'){
                                    $user_type_name=$get_user_info['user_type']['0'];
                                 ?>
                                    <div id="addText" class="text-box-round">                                        
                                        <?php 
                                        $dating_ads=$wpdb->get_results("SELECT * FROM wp_dating_ads WHERE user_id=".$current_user->ID." ORDER BY ID DESC LIMIT 0,2");
                                        $upload_dir = wp_upload_dir();
                                        $img_srcpath = $upload_dir['baseurl'] . '/dating_ads/'; ?>
                                        <h5>OUR SPEED DATING</h5>
                                    <?php foreach ($dating_ads as $value) { 
                                          $from=strtotime($value->from);
                                          $to=strtotime($value->to); 
                                          $from_m=date('m', $from);
                                          $monthName_from = date('F', mktime(0, 0, 0, $from_m, 10));
                                          $to_m=date('m', $to);
                                          $monthName_to = date('F', mktime(0, 0, 0, $to_m, 10));
                                          $from_y=date('Y', $from);
                                          $to_y=date('Y', $to);
                                          $from_d=date('d', $from);
                                          $to_d=date('d',$to);
                                          $country=$value->country;
                                          if($value->region != "")
                                            {
                                                $city= ' - ' . $value->region;
                                            }else
                                            {
                                               $city= ""; 
                                            }
                                     ?>
                                    <div class="form-group"> 
                                       <div class="adds_content no-padding-left col-sm-7">
                                       <p>
                                       <?php echo $country; ?>
                                       <span class="city_white"><?php echo $city; ?></span><br/>
                                       <?php echo $from_d."-".$to_d; ?>
                                       <?php if($from_m==$to_m){ echo $monthName_from; }
                                       else{ echo $monthName_from."-".$monthName_to; }
                                       echo $to_y; ?>
                                       </p>
                                       <a href="<?php echo home_url(); ?>/?page_id=735&user_id=<?php echo $current_user->ID; ?>" class="goAdd pull-right">go to ads</a>
                                       </div>
                                       <div class="col-sm-5">
                                          <img src="<?php echo $img_srcpath.$value->ads_pick; ?>" alt="adds image" class="img-responsive">
                                       </div>
                                     </div>
                                   <?php } ?>
                                    </div>
                                    <?php } else { ?>
                                    <div class="text-box-round invalid">
                                        <span class="grayTrans"></span>
                                        <?php $speed_dating = get_post(362); ?>
                                        <h5><?php echo $speed_dating->post_title; ?></h5>
                                        <p><?php // echo $get_user_info['speed_data']['0']; ?></p>
                                    </div>
                                    <?php } ?> 

<!---******************* for travel agenda **************-->
                                <?php 
                                    if($operational_setting[10]->$user_type_name  == 1 && $get_user_info['approved_as_basic']['0'] != 'no') { ?>
                                    <div class="text-box-round">
                                        <h5>OUR TRAVEL AGENDA</h5>
                                        <?php $travels_ads=$wpdb->get_results("SELECT * FROM wp_travel_dating WHERE tarvel_logged_user=".$current_user->ID." ORDER BY ID DESC"); ?>
                                        <?php 
                                        foreach ($travels_ads as $value) 
                                        { 
                                            $from=strtotime($value->travel_from);
                                            $to=strtotime($value->travel_to); 
                                            $from_m=date('m', $from);
                                            $monthName_from = date('F', mktime(0, 0, 0, $from_m, 10));
                                            $to_m=date('m', $to);
                                            $monthName_to = date('F', mktime(0, 0, 0, $to_m, 10));
                                            $from_y=date('Y', $from);
                                            $to_y=date('Y', $to);
                                            $from_d=date('d', $from);
                                            $to_d=date('d',$to);
                                            $country=$value->travel_country;
                                            if($value->travel_region != "")
                                            {
                                                $city= ' - ' . $value->travel_region;
                                            }else
                                            {
                                             $city= "";   
                                            }
                                        ?>
                                           <div class="form-group"> 
                                                <div class="adds_content no-padding-left col-sm-12">
                                                   <p>
                                                   <?php echo $country; ?>
                                                   <span class="city_white"><?php echo $city; ?></span><br/>
                                                   <?php echo $from_d."-".$to_d; ?>
                                                   <?php if($from_m==$to_m)
                                                   {  echo $monthName_from; }
                                                   else { echo $monthName_from."-".$monthName_to; }
                                                   echo $to_y; ?>
                                                   </p>

                                                </div>
                                           </div>
                                      <?php } ?>                                            
                                    </div>
                                    <?php } else { ?>
                                        <div class="text-box-round invalid">
                                            <span class="grayTrans"></span>
                                            <?php $trave_agenda = get_post(364); ?>
                                            <h5><?php echo $trave_agenda->post_title; ?></h5>
                                            <p><?php // echo $get_user_info['travel_agent']['0']; ?></p>
                                        </div>
                                    <?php } ?>
                            

                                </div>
                                <div class="col-sm-8 col-md-8 col-xs-12 no-padding">
                                    <div class="col-sm-7 col-md-7 col-xs-12 ">
                                        <?php if($get_user_info['approved_as_basic']['0']=='no' ) {  ?> 
                                        <div class="Album-Part">
                                            <span class="grayTrans"></span>
                                            <div class=""> 
                                                <h3 class="Pro-h3"><span>ALBUMS</span></h3>
                                            </div>
                                            <div class="WidthFull">
                                                <?php
                                                global $current_user;
                                                get_currentuserinfo();
                                                $current_user->user_type;
                                                $results = $wpdb->get_results("SELECT * FROM user_create_album WHERE user_id = '$current_user->ID'");
                                                $upload_dir = wp_upload_dir();
                                                $imgPath = $upload_dir['baseurl'] . '/users_album/album_' . $current_user->ID . '/';
                                                $i = 1;
                                                $count =1;
                                                foreach ($results as $value) {
                                                    $select_img = $wpdb->get_results("SELECT * FROM user_album_images WHERE album_id = '$value->id' AND status != 2 order by img_order ASC limit 1");
                                                    if (count($select_img) == 0) {
                                                        ?>
                                                        <figure class="album-img-div text-center">
                                                            <img src="<?php bloginfo('template_url'); ?>/images/NoAlbum.jpeg" alt="">
                                                            <figcaption><?php echo $count++; ?></figcaption>
                                                        </figure>
                                                        <?php
                                                    } else {
                                                        foreach ($select_img as $select) {
                                                            $number = '"' . $value->id . '"';
                                                            ?>                                                            
                                                            <figure class="album-img-div text-center"><a href="<?php echo get_permalink(424); ?>&imgid=<?php echo base64_encode($number); ?>" name='imgid' target='_blank' value="<?php echo $value->id; ?>" >
                                                            <img src="<?php echo $imgPath . $select->img_url; ?>"></a><figcaption><?php echo $count++; ?></figcaption></figure>                                      
                                                            <?php
                                                        }
                                                    }
                                                }
                                                if (count($results) != 0) {
                                                    
                                                    $sum = 1;
                                                    for ($i = 0; $i < (5 - count($results)); $i++) {
                                                        ?>
                                                        <figure class="album-img-div text-center">
                                                            <img src="<?php bloginfo('template_url'); ?>/images/alb_2.jpg" alt="">
                                                            <figcaption><?php echo count($results) + $sum; ?></figcaption>
                                                        </figure>
                                                        <?php
                                                        $sum++;
                                                    }
                                                } else {
                                                    echo "No Album";
                                                }
                                                ?>                                                 

                                            </div> 
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                    <?php } else { ?>
                                        
                                         <div class="Album-Part">
                                            <div class=""> 
                                                <h3 class="Pro-h3"><span>ALBUMS</span></h3>
                                            </div>
                                            <div class="WidthFull">
                                                <?php
                                                global $current_user;
                                                get_currentuserinfo();
                                                $current_user->user_type;
                                                $results = $wpdb->get_results("SELECT * FROM user_create_album WHERE user_id = '$current_user->ID'");
                                                $upload_dir = wp_upload_dir();
                                                $imgPath = $upload_dir['baseurl'] . '/users_album/album_' . $current_user->ID . '/';
                                                $i = 1;
                                                $count =1;
                                                foreach ($results as $value) {
                                                    $select_img = $wpdb->get_results("SELECT * FROM user_album_images WHERE album_id = '$value->id' AND status != 2 order by img_order ASC limit 1");
                                                    if (count($select_img) == 0) {
                                                        ?>
                                                        <figure class="album-img-div text-center">
                                                            <img src="<?php bloginfo('template_url'); ?>/images/no_image_avl.jpeg" alt="">
                                                            <figcaption><?php echo $value->album_name; ?></figcaption>
                                                        </figure>
                                                        <?php
                                                    } else {
                                                        foreach ($select_img as $select) {
                                                            $number = '"' . $value->id . '"';
                                                            ?>                                                            
                                                            <figure class="album-img-div text-center"><a href="<?php echo home_url(); ?>/?page_id=761&album=<?php echo $value->id; ?>&user_id=<?php echo $value->user_id; ?>" >
                                                            <img src="<?php echo $imgPath . $select->img_url; ?>"></a><figcaption><?php echo $value->album_name; ?></figcaption></figure>                                      
                                                            <?php
                                                        }
                                                    }
                                                }
                                              
                                                if (count($results) != 0) {
                                                    
                                                    $sum = 1;
                                                    for ($i = 0; $i < (5 - count($results)); $i++) {
                                                        if((count($results)+$i)<$user_alb_count){?>
                                                <figure class="album-img-div text-center">
                                                    <img src="<?php bloginfo('template_url'); ?>/images/album_dummy.gif" alt="">
                                                    <figcaption>no album</figcaption>
                                                </figure>
                                                <?php } else { ?>
                                                    <figure class="album-img-div text-center">
                                                     <span class="grayTrans"></span>
                                                    <img src="<?php bloginfo('template_url'); ?>/images/album_dummy.gif" alt="">
                                                    <figcaption></figcaption>
                                                </figure>
                                                <?php }
                                                        $sum++;
                                                    }
                                                } else {
                                                    for ($i = 0; $i < (5 - count($results)); $i++) {
                                                if($i<$user_alb_count){?>
                                                <figure class="album-img-div text-center">
                                                    <img src="<?php bloginfo('template_url'); ?>/images/album_dummy.gif" alt="">
                                                    <figcaption>no album</figcaption>
                                                </figure>
                                                <?php } else { ?>
                                                    <figure class="album-img-div text-center">
                                                     <span class="grayTrans"></span>
                                                    <img src="<?php bloginfo('template_url'); ?>/images/album_dummy.gif" alt="">
                                                    <figcaption></figcaption>
                                                </figure>
                                                <?php }
                $sum++;
            }
                                                }
                                                ?>                                                 

                                            </div> 
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                        
                                <?php } ?>  

                                <div class="col-sm-5 col-md-5 col-xs-12 no-padding">
    <?php if ($get_user_info['approved_as_basic']['0'] == 'no') { ?> 
                                        <div class="Favourite-Part">
                                            <span class="grayTrans"></span>
                                            <div class=""> 
                                                <h3 class="Pro-h4"><span>FAVOURITE QUOTE</span></h2>
                                            </div>
                                            <div class="col-sm-12 col-md-12 no-padding"> 
                                                <p class="fav-para"><?php echo $fav_quote_content; ?></p>
                                            </div>
                                        </div>
                                    </div>
                                        <?php } else { ?>

                                    <div class="Favourite-Part">
                                        <div class=""> 
                                            <h3 class="Pro-h4"><span>FAVOURITE QUOTE</span></h2>
                                        </div>
                                        <div class="col-sm-12 col-md-12 no-padding"> 
                                <?php echo $fav_quote_content; ?>
                                        </div>
                                    </div>
                                </div>
    <?php } ?>
                            <?php
                            $results = $wpdb->get_var("SELECT img_url FROM bg_images");
                            //$upload_dir = wp_upload_dir();
                            $imgPath = $results;
                            ?>
                            <div class="col-sm-12 col-md-12 col-xs-12 no-padding-right prelative">

                                <h3 class="Pro-h3 newIntro"><span>INTRODUCTION</span></h3><span></span>                                        
                                <div  id="content-1" > 
                                    <div class="Introduction-Part" style="background-size: 100% 100%; background-image: url('<?php echo $imgPath; ?>');" id ="introduction_part">

                                        <div class="padding-para-div"> 
                                            <p class="Intro-para" id="intro_data">
                                            <?php 
                                                if($des_status == '' && $describe != '')
                                                {
                                                    $message = "Your introduction not approved by admin yet!";
                                                }else if($des_status == 'refused'){
                                                    $message = 'Your introduction has refused by admin!';
                                                }else
                                                {
                                                   $message = $describe;
                                                }                                        
                                                echo $message; 
                                            ?> </p>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <div class="col-sm-5 col-md-5 col-xs-12">
                <div class="col-sm-6 col-md-6 col-xs-12 no-padding">
                    <div class="Favourite-Part2">
                            <div class=""> 
                                <h3 class="Pro-h3 col-sm-12"><span>MAIN RESIDENCE</span></h3>
                            </div>
                            <div class="padding-top-bottom">
                                <div class="form-group"> 
                                    <label for="inputEmail3" class="col-sm-5 BasicInfo-label">COUNTRY :</label>
                                    <label for="inputEmail3" class="col-sm-7 BasicInfo-label para-left"><?php echo $country; ?></label>
                                </div>
                                <div class="form-group"> 
                                    <label for="inputEmail3" class="col-sm-5 BasicInfo-label">REGIO :</label>
                                    <label for="inputEmail3" class="col-sm-7 BasicInfo-label para-left"><?php echo $district1; ?></label>
                                </div>
                                <div class="form-group"> 
                                    <label for="inputEmail3" class="col-sm-5 BasicInfo-label">NEXT CITY :</label>
                                    <label for="inputEmail3" class="col-sm-7 BasicInfo-label para-left"><?php echo $city1; ?></label>
                                </div>
                                <div class="form-group"> 
                                    <label for="inputEmail3" class="col-sm-5 BasicInfo-label">Frequently in :</label>
                                    <label for="inputEmail3" class="col-sm-7 BasicInfo-label para-left"><?php echo $get_user_info['frequently_in']['0']; ?></label>
                                </div>
                        </div>
                    </div>

                    <div class="Favourite-Part3 <?php if($portalsetting[2]->$user_type_name == '0'){echo 'invalid' ;}?>">
                        <?php if($portalsetting[2]->$user_type_name == '0'){echo '<span class="grayTrans-more"></span>';}?>
                        <div class=""> 
                            <h3 class="Pro-h3 col-sm-12"><span>MATCHES</span></h3>
                        </div>
                        <div class="padding-top-bottom">
                            <div class="row"> 
                                <?php
                                $get_sender_data = $wpdb->get_results("SELECT reciver_id FROM wp_user_profile WHERE sender_id ='" . $current_user->ID . "' AND add_fav='1' ORDER BY id DESC");
                                $get_reciver_data = $wpdb->get_results("SELECT sender_id FROM  wp_user_profile WHERE reciver_id ='" . $current_user->ID . "' AND add_fav='1' ");
                                $senderdataid = array();
                                $count = 1;
                                foreach ($get_sender_data as $value) {
                                    $senderdataid[] = $value->reciver_id;
                                }
                                $alldata = array();
                                foreach ($get_reciver_data as $data) {
                                    if (in_array($data->sender_id, $senderdataid)) {
                                        $alldata[] = $data->sender_id;
                                    }
                                }
                                if (!empty($alldata)) {
                                    foreach ($alldata as $mat_value) {
                                        if ($count == 4) {
                                            break;
                                        }
                                        $senderID = $mat_value;
                                        $user_name = get_user_meta($senderID, 'nickname', true);
                                        $userUrl = get_permalink(305) . '&user_id=' . $senderID;
                                        #.. sender user pic data
                                        $his_pick = get_user_meta($senderID, 'his_pick', true);
                                        $her_pick = get_user_meta($senderID, 'her_pick', true);
                                        if (!empty($her_pick)) {
                                            $pick_name = $her_pick;
                                        } else {
                                            $pick_name = $his_pick;
                                        }
                                        $pick_source = $upload_dir['baseurl'] . '/profilepick_' . $senderID . '/' . $pick_name;
                                        ?>
                                        <div class="col-xs-4">
                                            <img src="<?php echo $pick_source; ?>" height="45" width="52">
                                            <label class="matchName"><?php echo $user_name; ?></label>
                                        </div>
                                        <?php
                                        $count++; } } 
                                        else {
                                            echo '<div class="col-xs-8">No Matches</div>';
                                        }
                                        ?>  
                            </div>           
                        </div>
                    </div> 
                    <?php  
                        $userid = get_current_user_id();
                        $arr = array();
                        $subId = array();
                        $status = array();
                        $myrows = $wpdb->get_results("SELECT *  FROM `wp_erotics_preference` where `userid` = $userid AND `type` = 1 order by like_dislike_attr_id" );
                        foreach ($myrows as $result) {
                            $arr[] = $result->like_dislike_attr_id;
                            $subId[] = $result->subject_id;
                            $status[] = unserialize($result->status);
                        }                                                       

                        $myrows1 = $wpdb->get_results("SELECT *  FROM `wp_erotics_preference` where `userid` = $userid AND `type` = 2 order by like_dislike_attr_id" );
                        $arr1 = array();
                        $subId1 = array();
                        $status1 = array();

                        foreach ($myrows1 as $result) {
                            $arr1[] = $result->like_dislike_attr_id;
                            $subId1[] = $result->subject_id;
                            $status1[] = unserialize($result->status);
                        }

                        $arg = array('posts_per_page' => -1, 'offset' => 1, 'category' => 14, 'post_type' => 'eroticoption', 'order' => 'asc');
                        $myposts = get_posts($arg);

                        $myAllposts = array();
                        $ctagoryid = array("3", "4", "5");

                        foreach ($ctagoryid as $values) {
                            $args = array('posts_per_page' => -1, 'offset' => 1, 'category' => $values->ID, 'post_type' => 'erotics', 'order' => 'asc', 'show_count' => 0);
                            $myAllposts = get_posts($args);
                        }

                        $content = array();
                        foreach ($subId as $key => $value) 
                        {                          
                            $content['subject'][] = get_the_title($value); 
                            $content['subjectID'][] =  $value;                   
                        }
                        foreach ($arr as $key => $value1) {
                            foreach ($myposts as $value) {
                                if ($value1 == $value->ID) {
                                    $content['desc'][] = $value->post_title;
                                    if(!(in_array($value->ID, $content['id'])))
                                    {
                                        $content['id'][] = $value->ID;
                                    } 
                                }
                            }
                        }

                        $content1 = array();
                        foreach ($subId1 as $key => $value) 
                        {                          
                            $content1['subject'][] = get_the_title($value);
                            $content1['subjectID'][] =  $value;                    
                        }

                        foreach ($arr1 as $key => $value1) {
                            foreach ($myposts as $value) {
                                if ($value1 == $value->ID) {
                                    $content1['desc'][] = $value->post_title;
                                    if(!(in_array($value->ID, $content1['id'])))
                                    {
                                        $content1['id'][] = $value->ID;
                                    } 
                                }
                            }
                        }
                        $unique_arr_desc=array_unique($content['desc']);
                        foreach ($unique_arr_desc as $value) 
                        {
                           $unique_arr_dist[]=$value;
                        }
                        $unique_arr_desc1=array_unique($content1['desc']);
                        foreach ($unique_arr_desc1 as $value) 
                        {
                           $unique_arr_dist1[]=$value;
                        }
                        ?>
                    <div class="eropic-profile-Part">
						 <?php echo ($get_user_info['approved_as_basic']['0'] == 'no' || $setting[1]->$user_type_name != '1') ? "<span class='grayTrans'></span>"  : "" ?>
                            <h3 class="eropic-h3 no-margin no-padding col-sm-12 col-md-12"><span class="red">Her erotic profile</span> <a class="profile-her-erotic-right-btn" href="#">E </a></h3>
                            <?php
                            $count = 0;
                            for ($i = 0; $i < count($unique_arr_dist); $i++) {
                                $color = $content['id'][$count];
                                $getTileShow = getActiveOrPassviveTitle($wpdb,$userid,$color, '1');
                                ?>              
                                <div class="eropic-box-round " style="<?php echo ($getTileShow[0] != '2') ? 'display:block' : 'display:none'; ?>;background-color:<?php echo ($bgColor != "") ? $bgColor : $textColor[$color]; ?>;color:<?php echo ($bgColor != "") ? $bgColor : $forColor[$color]; ?>;">
                                    <p><strong class="up-case"><?php echo strtoupper(qtrans_use($q_config['language'],$unique_arr_dist[$i], true)); ?> :</strong></p>
                                    <?php for($j = 0; $j < count($content['subject']); $j++)
                                    {
                                        if($content['desc'][$j] == $unique_arr_dist[$i])
                                        { 
                                            $like_dislike_ID = $content['subjectID'][$j];
                                            $text = getActiveOrPassvive($wpdb,$userid,$color,$like_dislike_ID, '1');
                                        ?>
                                    <p style=""><?php echo qtrans_use($q_config['language'],$content['subject'][$j], true) .' '. $text['text']; ?></p>
                                    <?php } } ?>
                                </div>
                                <?php $count++; } ?>
                        </div>                          
            </div>
            <div class="col-sm-6 col-md-6 col-xs-12 no-padding-right">
               
	
                                <?php if ($setting[2]->$user_type_name == '1' && $get_user_info['approved_as_basic']['0'] != 'no') { ?>
                                    <div class="perfect-match-Part cus_scrolln">

                                        <div class=""> 
                                            <h3 class="opm-h3 col-sm-12 col-md-12"><span>PERFECT MATCH</span></h2>
                                        </div>
                                       <?php if($reg3_txtshe) { ?>
                                        <div class="opm-text-box-roundsec">
                                         <p><strong>SHE :</strong><?php echo $reg3_txtshe; ?></p>
                                        </div>
                                        <?php } ?>
                                        <?php if($reg3_txthe) { ?>
                                        <div class="opm-text-box-roundsec">
										<p><strong>HE :</strong><?php echo $reg3_txthe; ?></p> 
                                        </div><?php } ?>
                                         <?php if($reg3_txtboth){ ?>
                                        <div class="opm-text-box-roundsec">
                                           <p><strong>BOTH :</strong><?php echo $reg3_txtboth; ?></p> 
                                        </div>
                                        <?php } ?>
                                    </div>
                                <?php } else { ?>

                                    <div class="perfect-match-Part cus_scrolln invalid">
                                        <span class="greenTrans"></span>

                                        <div class=""> 
                                            <h3 class="opm-h3 col-sm-12 col-md-12"><span>PERFECT MATCH</span></h2>
                                        </div>

                                        <div class="opm-text-box-roundsec">
                                            <p><strong>SHE :</strong><?php echo $reg3_txtshe; ?></p>
                                        </div>
                                        <div class="opm-text-box-roundsec">
                                            <p><strong>HE :</strong><?php echo $reg3_txthe; ?></p> 
                                        </div>
                                        <div class="opm-text-box-roundsec">
                                            <p><strong>BOTH :</strong><?php echo $reg3_txtboth; ?></p> 
                                        </div>
                                    </div>

                                <?php } ?>  
     
                    <div class="eropic-profile-Part">
						 <?php echo ($get_user_info['approved_as_basic']['0'] == 'no' || $setting[1]->$user_type_name != '1') ? "<span class='grayTrans'></span>"  : "" ?>
                            <h3 class="eropic-h3 no-margin no-padding col-sm-12 col-md-12">
                                <span class="red">His erotic profile</span>
                            <?php if ($get_user_info['user_type']['0'] == 'validated') { ?> <a class="profile-her-erotic-right-btn invalid" href="#">E </a> <?php } else { ?>
                                    <a class="profile-her-erotic-right-btn" href="#">E </a> 
                            <?php } ?>
                            </h3>
                            <?php
                        $count1 = 0;
                        for ($i = 0; $i < count($unique_arr_dist1); $i++) 
                        {
                            $color1 = $content1['id'][$count1];
                            $textColor[$color1]; 
                            $getTileShow = getActiveOrPassviveTitle($wpdb,$userid,$color1, '2');                                                                                                                                
                        ?>              
                        <div class="eropic-box-round" style="background-color:<?php echo ($bgColor1 != "") ? $bgColor1 : $textColor[$color1]; ?>;color:<?php echo ($bgColor1 != "") ? $bgColor1 : $forColor[$color1]; ?>;">
                            <p class="child"><strong class="up-case"><?php echo strtoupper(qtrans_use($q_config['language'],$unique_arr_dist1[$i], true)); ?> :</strong></p>
                            <?php for($j = 0; $j < count($content1['subject']); $j++)
                             {
                            if($content1['desc'][$j]==$unique_arr_dist1[$i])
                            { 
                                $like_dislike_ID = $content1['subjectID'][$j];
                                $text1 = getActiveOrPassvive($wpdb,$userid,$color1,$like_dislike_ID, '2');
                            ?>
                            <p style=""><?php echo qtrans_use($q_config['language'],$content1['subject'][$j], true) .' '. $text1['text']; ?></p>
                            <?php }
                        } ?>
                        </div>
                        <?php $count1++; } ?>
                        </div>                         
                </div>
            </div>
            </form>
        </div>
        <div class="active_passive" style="float:right;margin-right:30%"> (a) active (p) passive </div>
    </div>
    </div>
    </div>

    <?php
} else {
    ?>
    <div class="text-center succesPoint">
        <h4>You must be loggedin to access this page</h4>
    </div>
    <?php
}



?>


<?php get_footer(); ?>


