<?php
/*
 * Template Name: Search On User Type
 * Description: A Page Template display different search on user type.
 * Author Name : Sanjay Shinghania
 */

get_header();
$login_user_id = get_current_user_id();
$user_type = get_user_meta($login_user_id, 'user_type', true);

$count = $wpdb->get_results("select search_content from wp_user_standard_search where user_id = '$login_user_id'");
if(isset($_GET['type']))
{
    $result = $wpdb->get_results("select search_content from wp_user_standard_search where user_id = '$login_user_id'");
}
else
{
    $result = array("");
}
$unserializeData = unserialize($result[0]);
foreach ($result as $key => $value) {
    $content = unserialize($value->search_content);    
    if(isset($content->country1))
    {
        $country1 = ($content->country1 != "" ? $content->country1 : "");        
    }
    if(isset($content->state1))
    {
       $state1 = (count($content->state1) > 0 ? $content->state1 : "");
    }
    if(isset($content->country2))
    {
        $country2 = ($content->country2 != "" ? $content->country2 : "");
    }
    if(isset($content->country3))
    {
        $country3 = ($content->country3 != "" ? $content->country3 : "");
    }
}

$hideclass = '';
$html = '';
if ($user_type == 'premium') {
   
} else {
    $hideclass = '';
    $html = '<span class="grayTrans"></span>';
}
?>
<?php
if (is_user_logged_in()) 
{ 
$setting = $wpdb->get_results("select * from wp_admin_portal_setting where portal_setting_type= 'search setting' ");
if($setting[0]->$user_type == '1')
{
  $region = get_user_meta($login_user_id, 'district1', true);   
}else
{
   $region = ''; 
}

if($setting[1]->$user_type == '1')
{
 $country = get_user_meta($login_user_id, 'country', true);  
}else
{
$country = $country1;  
}
?>

<div class="container-fluid">
    <div class="search-page">
        <form class="form-horizontal" id="search_form" role="form" name="form" method="POST" action="" onsubmit="return false;">
            <div class="container">
                <div class="col-sm-12">
                    <div class="search_form row">
                        <?php
                            if ($user_type == 'basic') {
                                echo '<h3 class="blue_heading">Search for basic members</h3>';
                            } elseif ($user_type == 'validated') {
                                echo '<h3 class="blue_heading">Search for validated members</h3>';
                            } else {
                                echo '<h3 class="red_heading">Search for premium members</h3>';
                            }
                        ?>
                        <div class="search_form_body">
                            <div class="col-sm-6 ">
                                <div class="heading_loca">
                                    <div class="col-sm-12 col-xs-12 col-md-12 no-padding">
                                        <div class="col-sm-6 search-padding <?php if($user_type != 'premium'){echo 'invalid';}?>">                                            
                                            <?php if($setting[11]->$user_type != '1'){echo '<span class="grayTrans"></span>';}?>
                                            <?php if(count($count) > 0) { ?>
                                                <h4>USE YOUR STANDARD SEARCH PROFILE</h4>
                                                <input type="checkbox" <?php echo ($_GET['type'] == 'standard') ? "checked":""; ?> for="search" id="standard" value="standard" name="standard" class="checkbox_cus">
                                                <label for="standard" class="checkbox_label"></label>
                                            <?php } ?>
                                        </div>
                                        <div class="col-sm-6 search-padding">                                                
                                        </div>
                                    </div>
                                </div>                              
                                <div id="border_box" class="col-xs-12 border-box">
                                    <h3 class="Search-bg-header blue_heading">Search area</h3>
                                    <div class="row <?php if($setting[2]->$user_type != '1'){echo 'invalid';}?>">
                                        <?php if($setting[2]->$user_type != '1'){echo '<span class="grayTrans"></span>';}?>
                                        <div class="col-md-6 col-sm-6 col xs-6 ">
                                            <p class="italic basic_member_disable">All over Europe</p>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col xs-6">
                                            <div class="col-sm-1 search-padding no-padding_how">
                                                <input type="checkbox" for="search" id="all_over_europe" value="all_over_europe" name="all_over_europe" class="checkbox_cus">
                                                <label for="all_over_europe" class="checkbox_label"></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="bg_blue_light" class="bg-blue-light">
                                        <h3>Define your individual search area</h3>
                                        <div class="row search_input search_input1 <?php if($setting[1]->$user_type != '1'){echo 'invalid';}?>">
                                            <div class="col-md-6 col-sm-6 col xs-6">
                                                <p class="">1<span class="top-text1">st</span>&nbsp; &nbsp; country</p>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col xs-6" id="all_country_all">
                                                <select class="form-control germany-txtbox country_disable" id="country" name ="country[]"></select>
                                            </div>
                                        </div>
                                        <div class="row search_input search_input1">
                                            <div class="col-md-6 col-sm-6 col xs-6">
                                                <p class="italic">all regions</p>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col xs-6">
                                                <div class="col-sm-1 search-padding no-padding_how">
                                                    <input type="checkbox" id="all_first_country_region" value="all_region" name="all_region" class="checkbox_cus">
                                                    <label for="all_first_country_region" class="checkbox_label germany-chkbox"></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row search_input search_input1" id="regions_all">                                            
                                            <div class="col-md-6 col-sm-6 col xs-6"  >
                                                <p class="">1<span class="top-text1">st</span>&nbsp; &nbsp; region</p>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col xs-6" id="all_states_all">
                                                <select class="form-control" name ="state[]" for="state1" id ="state"></select>                                  
                                            </div>
                                        </div>
                                        <div class="row search_input search_input1" id="regions_all">                                            
                                            <div style="display:none" id="hideregion1" class="col-md-6 col-sm-6 col xs-6"  >
                                                <p class="">2<span class="top-text1">nd</span>&nbsp; &nbsp; region</p>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col xs-6" id="appen_regions">                                                                             
                                            </div>
                                        </div>
                                        <div class="row search_input search_input1" id="regions_all">                                            
                                            <div style="display:none" id="hideregion2" class="col-md-6 col-sm-6 col xs-6"  >
                                                <p class="">3<span class="top-text1">rd</span>&nbsp; &nbsp; region</p>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col xs-6" id="appen_regions1">                                                                             
                                            </div>
                                        </div>
                                        <?php if($setting[0]->$user_type != 1){ ?>
                                        <div class="col-md-12 col-sm-6 col xs-6">
                                            <p class="pull-right"><a href="javascript:void(0)" class="country_disable" style="float:right;" class="add_new_one" id="add_new_one">+ add region</a></p>
                                        </div>
                                        <?php } ?>
                                <script>
                                var addnew = 1;
                                jQuery('#add_new_one').click(function(){                                    
                                    var getHtml = jQuery('#all_states_all').html();
                                    if(addnew == 1)
                                    {
                                        jQuery('#hideregion1').show();                                    
                                        jQuery('#appen_regions').html(getHtml);
                                    }
                                    if(addnew == 2)
                                    {
                                        jQuery('#hideregion2').show();                                    
                                        jQuery('#appen_regions1').html(getHtml);
                                    }
                                    addnew++;                                    
                                });
                                </script>                                       


                        <!-- Second country -->
                                        <div id="countrydiv2" style="display:<?php echo ($country2 != "") ? 'block' : 'none' ?>" class="bg-blue-light">
                                            <h3>Define your individual search area</h3>
                                            <div class="row search_input search_input1 <?php if($user_type != 'premium'){echo 'invalid';}?>">
                                                <?php if($user_type != 'premium'){echo '<span class="grayTrans"></span>';}?>
                                                <div class="col-md-6 col-sm-6 col xs-6">
                                                    <p class="">2<span class="top-text1">nd</span>&nbsp; &nbsp; country</p>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col xs-6" id="all_country_all">
                                                    <select class="form-control germany-txtbox country_disable" id="country2" name ="country2[]"></select>
                                                </div>
                                            </div>
                                            <div class="row search_input search_input1">
                                                <div class="col-md-6 col-sm-6 col xs-6">
                                                    <p class="italic">all regions</p>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col xs-6">
                                                    <div class="col-sm-1 search-padding no-padding_how">
                                                        <input type="checkbox" id="all_second_country_region" value="all_region" name="all_region" class="checkbox_cus">
                                                        <label for="all_second_country_region" class="checkbox_label germany-chkbox"></label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row search_input search_input1" id="regions_all">
                                                <div class="col-md-6 col-sm-6 col xs-6"  >
                                                    <p class="">1<span class="top-text1">st</span>&nbsp; &nbsp; region</p>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col xs-6" id="all_states_all1">
                                                    <select class="form-control" for="state2" name ="state2[]" id ="state2"></select>                                  
                                                </div>
                                            </div> 
                                            <div class="row search_input search_input1" >  
                                                <div class="col-md-6 col-sm-6 col xs-6" style="display:none" id="hiddenstate" >
                                                    <p class="">2<span class="top-text1">nd</span>&nbsp; &nbsp; region</p>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col xs-6" id="append_state">                                                                             
                                                </div>
                                            </div>
                                            <div class="row search_input search_input1" >  
                                                <div class="col-md-6 col-sm-6 col xs-6" style="display:none" id="hiddenstate1" >
                                                    <p class="">3<span class="top-text1">rd</span>&nbsp; &nbsp; region</p>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col xs-6" id="append_state1">                                                                             
                                                </div>
                                            </div>    
                                                <div class="col-md-12 col-sm-6 col xs-6">
                                                    <p><a style="float:right;" class="country_disable" href="javascript:void(0)" id="add_new_two">+ add region</a></p>
                                                </div>
                                         
                                         <script>
                                            var addnew1 = 1;
                                            jQuery('#add_new_two').on('click',function(){
                                                var getHtml = jQuery('#all_states_all1').html();
                                                if(addnew1 == 1)
                                                {
                                                    jQuery('#hiddenstate').show();                                    
                                                    jQuery('#append_state').html(getHtml);
                                                }
                                                if(addnew1 == 2)
                                                {
                                                    jQuery('#hiddenstate1').show();                                    
                                                    jQuery('#append_state1').html(getHtml);
                                                }
                                                addnew1++;                                    
                                            });
                                            </script>

                                 <!-- Third country -->
                                         <div id="countrydiv3" style="display:<?php echo ($country3 != "") ? 'block' : 'none' ?>;" class="bg-blue-light">
                                            <h3>Define your individual search area</h3>
                                            <div class="row search_input search_input1 <?php if($user_type != 'premium'){echo 'invalid';}?>">
                                                <?php if($user_type != 'premium'){echo '<span class="grayTrans"></span>';}?>
                                                <div class="col-md-6 col-sm-6 col xs-6">
                                                    <p class="">3<span class="top-text1">rd</span>&nbsp; &nbsp; country</p>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col xs-6" id="all_country_all">
                                                    <select class="form-control germany-txtbox country_disable" newAttr="country" for="countrystate" id="country3" name ="country3[]"></select>
                                                </div>
                                            </div>
                                            <div class="row search_input search_input1">
                                                <div class="col-md-6 col-sm-6 col xs-6">
                                                    <p class="italic">all regions</p>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col xs-6">
                                                    <div class="col-sm-1 search-padding no-padding_how">
                                                        <input type="checkbox" id="all_third_country_region" value="all_region" name="all_region" class="checkbox_cus">
                                                        <label for="all_third_country_region" class="checkbox_label germany-chkbox"></label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row search_input search_input1" id="regions_all2">
                                                <div class="col-md-6 col-sm-6 col xs-6"  >
                                                    <p class="">1<span class="top-text1">st</span>&nbsp; &nbsp; region</p>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col xs-6" id="all_states_all2">
                                                    <select class="form-control" name ="state3[]" for="state3" id ="state3"></select>                                  
                                                </div>
                                            </div>
                                            <div class="row search_input search_input1" id="regions_all2">
                                                <div class="col-md-6 col-sm-6 col xs-6" style="display:none" id="hidden_state" >
                                                    <p class="">2<span class="top-text1">nd</span>&nbsp; &nbsp; region</p>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col xs-6" id="append_new_state">                                                                             
                                                </div>  
                                            </div>
                                            <div class="row search_input search_input1" id="regions_all2">
                                                <div class="col-md-6 col-sm-6 col xs-6" style="display:none" id="hidden_state1" >
                                                    <p class="">3<span class="top-text1">rd</span>&nbsp; &nbsp; region</p>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col xs-6" id="append_new_state1">                                                                             
                                                </div> 
                                            </div>
                                                <div class="col-md-12 col-sm-6 col xs-6">
                                            <p><a href="javascript:void(0)" class="country_disable pull-right" id="add_new_three">+ add region</a></p>
                                        </div>                                       
                                         </div>
                                           
                                        
                                        <div class="row search_input search_input1" id="regions_all1"></div>
                                    </div>
                                        <div class="row search_input search_input1" id="regions_all1"></div>
                                    </div>
                                    <div id="bg_blue_light1" class="bg-blue-light"></div>
                                    <div class="row bg-blue-light">
                                        <div class="col-md-4 col-sm-4 col xs-6 <?php if($user_type != 'premium'){echo 'invalid';}?>">
                                            <?php if($setting[0]->$user_type == '1' || $setting[1]->$user_type == '1'){echo '<span class="grayTrans"></span>';}?>
                                            <p><button class="country_disable" id="add_other_country">+ add other country</button></p>
                                        </div>                                        
                                    </div>
                                </div>
                                <script>
                                            var newstate = 1;
                                            jQuery('#add_new_three').on('click',function(){
                                                var getHtml = jQuery('#all_states_all2').html();
                                                if(newstate == 1)
                                                {
                                                    jQuery('#hidden_state').show();                                    
                                                    jQuery('#append_new_state').html(getHtml);
                                                }
                                                if(newstate == 2)
                                                {
                                                    jQuery('#hidden_state1').show();                                    
                                                    jQuery('#append_new_state1').html(getHtml);
                                                }
                                                newstate++;                                    
                                            });
                                            </script>

                                <!--search area-->
                                <?php // } ?>
                                <!---==============-->
                                <div class="heading_loca">
                                    <div class="col-md-12 col-sm-12 col-xs-12 border-box1">
                                        <h3 class="Search-bg-header blue_heading">Gender / status</h3>
                                        <div class="row">
                                            <div class="col-md-3 col-sm-3 col xs-6"></div>
                                            <div class="col-md-5 col-sm-5 col xs-6 <?php if($user_type != 'premium'){echo 'invalid';}?>">
                                                <?php if($user_type != 'premium'){echo '<span class="grayTrans"></span>';}?>
                                                <p class="basic_member_disable">only premium members</p>
                                                <div class="search-padding">
                                                    <input type="checkbox" for="search" id="premium"  value="premium" name="premium" class="checkbox_cus">
                                                    <label for="premium" class="checkbox_label"></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-4 col xs-6 <?php if($user_type != 'premium'){echo 'invalid';}?>">
                                                <?php //if($user_type != 'premium'){echo '<span class="grayTrans"></span>';}?>
                                                <p class="basic_member_disable">only with erotic profile</p>
                                                <div class="search-padding">
                                                    <input type="checkbox" for="search" id="erotic_profile" value="erotic preferences" name="erotic_profile" class="checkbox_cus">
                                                    <label for="erotic_profile" class="checkbox_label"></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row search_input">
                                            <div class="col-md-3 col-sm-3 col xs-6">
                                                <p>Couples</p>
                                                <div class="search-padding">
                                                    <input type="radio" for="search" id="couples" <?php echo (in_array("couples",$content->status)) ? "checked" : ""; ?> value="couples" usertype="user_type" name="couples" class="checkbox_cus disable">
                                                    <label for="couples" class="checkbox_label"></label>
                                                </div>
                                            </div>
                                            <div class="col-md-5 col-sm-5 col xs-6 ">
                                                <?php if($setting[4]->$user_type != '1'){echo '<span class="grayTrans"></span>';}?>
                                                <p class="basic_member_disable">only validated couples</p>
                                                <div class="search-padding">
                                                    <input type="radio" for="search" id="validated_couples" <?php echo (in_array("validated couples",$content->status)) ? "checked" : ""; ?> value="validated couples" name="couples" usertype="user_type" class="checkbox_cus disable">
                                                    <label for="validated_couples" class="checkbox_label"></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-4 col xs-6 <?php if($user_type != 'premium'){echo 'invalid';}?>">
                                                <?php if($setting[3]->$user_type != '1'){echo '<span class="grayTrans"></span>';}?>
                                                <p class="basic_member_disable">only married couples</p>
                                                <div class="search-padding">
                                                    <input type="radio" for="search" id="married_couples" value="married couples" <?php echo (in_array("married couples",$content->status)) ? "checked" : ""; ?> name="couples" usertype="user_type" class="checkbox_cus disable">
                                                    <label for="married_couples" class="checkbox_label"></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row search_input">
                                            <div class="col-md-3 col-sm-3 col xs-6">
                                                <p>Ladies</p>
                                                <div class="search-padding">
                                                    <input type="radio" for="search" id="ladies" <?php echo (in_array("ladies",$content->status)) ? "checked" : ""; ?> value="ladies" usertype="user_type" name="ladies" class="checkbox_cus disable">
                                                    <label for="ladies" class="checkbox_label"></label>
                                                </div>
                                            </div>
                                            <div class="col-md-5 col-sm-5 col xs-6 <?php if($user_type == 'basic'){echo 'invalid';}?>">
                                                <?php if($setting[4]->$user_type != '1'){echo '<span class="grayTrans"></span>';}?>
                                                <p class="basic_member_disable">only validated ladies</p>
                                                <div class="search-padding">
                                                    <input type="radio" for="search" id="validated_ladies" usertype="user_type" <?php echo (in_array("validated ladies",$content->status)) ? "checked" : ""; ?> value="validated ladies" name="ladies" class="checkbox_cus disable">
                                                    <label for="validated_ladies" class="checkbox_label"></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-4 col xs-6 <?php if($user_type != 'premium'){echo 'invalid';}?>">
                                                <?php if($setting[3]->$user_type != '1'){ echo '<span class="grayTrans"></span>';} ?>
                                                <p class="basic_member_disable">only single ladies</p>
                                                <div class="search-padding">
                                                    <input type="radio" for="search" id="single_ladies" usertype="user_type" <?php echo (in_array("single ladies",$content->status)) ? "checked" : ""; ?> value="single ladies" name="ladies" class="checkbox_cus disable">
                                                    <label for="single_ladies" class="checkbox_label"></label>
                                                </div>
                                            </div>
                                        </div>  
                                        <div class="row search_input">
                                            <div class="col-md-3 col-sm-3 col xs-6">
                                                <p>Gentleman</p>
                                                <div class="search-padding">
                                                    <input type="radio" for="search" fieldname="gentleman" usertype="user_type" id="gentleman" value="gentleman" <?php echo (in_array("gentleman",$content->status)) ? "checked" : ""; ?> name="gentleman" class="checkbox_cus disable">
                                                    <label for="gentleman" class="checkbox_label"></label>
                                                </div>
                                            </div>
                                            <div class="col-md-5 col-sm-5 col xs-6 <?php if($user_type == 'basic'){echo 'invalid';}?>">
                                                <?php if($setting[4]->$user_type != '1'){echo '<span class="grayTrans"></span>';}?>
                                                <p class="basic_member_disable">only validated gentleman</p>
                                                <div class="search-padding">
                                                    <input type="radio" for="search" id="validated_gentleman" fieldname="validated_gentleman" usertype="user_type" <?php echo (in_array("validated gentleman",$content->status)) ? "checked" : ""; ?> value="validated gentleman" name="gentleman" class="checkbox_cus disable">
                                                    <label for="validated_gentleman" class="checkbox_label"></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-4 col xs-6 <?php if($user_type != 'premium'){echo 'invalid';}?>">
                                                <?php if($setting[3]->$user_type != '1'){ echo '<span class="grayTrans"></span>';}?>
                                                <p class="basic_member_disable">only single gentleman</p>
                                                <div class="search-padding">
                                                    <input type="radio" for="search" fieldname="single_gentleman" usertype="user_type" value="single gentleman" id="single_gentleman" <?php echo (in_array("single gentleman",$content->status)) ? "checked" : ""; ?>  name="gentleman" class="checkbox_cus disable">
                                                    <label for="single_gentleman" class="checkbox_label"></label>
                                                </div>
                                            </div>
                                        </div>  
                                    </div>
                                </div><!--gender and status -->     

                                <div class="heading_loca">
                                    <div class="col-md-12 col-sm-12 col-xs-12 border-box1 <?php echo $hideclass; ?>">
                                        <?php if($setting[10]->$user_type != '1'){echo '<span class="grayTrans"></span>';}?>
                                        <h3 class="Search-bg-header blue_heading">Playroom</h3>
                                        <div class="space-div"></div>
                                        <div class="row search_input">
                                            <?php
                                                $args = array(
                                                    'post_type' => 'rooms',
                                                    'tax_query' => array(
                                                        array(
                                                            'taxonomy' => 'int_rooms',
                                                            'field' => 'term_id',
                                                            'terms' => 13)
                                                    ),
                                                    'order' => 'ASC'
                                                );
                                                $my_query = new WP_Query($args);
                                                $i =1;
                                                if ($my_query->have_posts()) {
                                                    while ($my_query->have_posts()) : $my_query->the_post();
                                                   
                                            ?>
                                            <div class="col-sm-4 col xs-6">
                                                <p class="basic_member_disable col-sm-10 no-padding-left"><?php the_title(); ?></p>
                                                <div class="search-padding col-sm-2">
                                                    <input type="checkbox" <?php echo (in_array(get_the_ID(), $content->playroom)) ? "checked" : ""; ?> for="search" id="rooms-<?php echo get_the_ID(); ?>" value="<?php the_ID(); ?>" name="int_data" class="checkbox_cus">
                                                    <label for="rooms-<?php echo get_the_ID(); ?>" class="checkbox_label"></label>
                                                </div>
                                            </div>
                                            <?php
                                            if($i == 3)
                                            {
                                              echo "<div class='clear'></div>";     
                                              $i = 1;                                         
                                            }else
                                            {
                                              $i++;  
                                            }
                                           
                                                endwhile;
                                            }
                                            wp_reset_query();
                                            ?>
                                        </div>
                                    </div>
                                </div><!--play rooms-->

                                <div class="heading_loca">
                                    <div class="col-md-12 col-sm-12 col-xs-12 border-box1 ">
                                        <?php if($setting[5]->$user_type != '1'){echo '<span class="grayTrans"></span>';}?>
                                        <h3 class="Search-bg-header blue_heading">Languages</h3>
                                        <div class="space-div"></div>
                                        <div class="row search_input">
                                            <div class="col-md-4 col-sm-3 col xs-6">
                                                <p class="basic_member_disable">English </p>
                                                <div class="search-padding">
                                                    <input type="checkbox" <?php echo (in_array('English', $content->language)) ? "checked" : ""; ?> for="search" id="eng" value="English" name="language" class="checkbox_cus">
                                                    <label for="eng" class="checkbox_label"></label>
                                                </div>
                                            </div>                                  
                                            <div class="col-md-4 col-sm-5 col xs-6">
                                                <p class="basic_member_disable">German </p>
                                                <div class="search-padding">
                                                    <input type="checkbox" for="search" <?php echo (in_array('German', $content->language)) ? "checked" : ""; ?> id="ger" value="German" name="language" class="checkbox_cus">
                                                    <label for="ger" class="checkbox_label"></label>
                                                </div>
                                            </div>                                  
                                            <div class="col-md-4 col-sm-4 col xs-6">
                                                <p class="basic_member_disable">French</p>
                                                <div class="search-padding">
                                                    <input type="checkbox" for="search" id="fr" <?php echo (in_array('French', $content->language)) ? "checked" : ""; ?> value="French" name="language" class="checkbox_cus">
                                                    <label for="fr" class="checkbox_label"></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row search_input">
                                            <div class="col-md-4 col-sm-3 col xs-6">
                                                <p class="basic_member_disable">Italien</p>
                                                <div class="search-padding">
                                                    <input type="checkbox" for="search" id="it" value="Italien" <?php echo (in_array('Italien', $content->language)) ? "checked" : ""; ?> name="language" class="checkbox_cus">
                                                    <label for="it" class="checkbox_label"></label>
                                                </div>
                                            </div>                                          
                                            <div class="col-sm-4 col xs-6">
                                                <p class="basic_member_disable">Spanish</p>
                                                <div class="search-padding">
                                                    <input type="checkbox" for="search" <?php echo (in_array('Spanish', $content->language)) ? "checked" : ""; ?> id="sp" value="Spanish" name="language" class="checkbox_cus">
                                                    <label for="sp" class="checkbox_label"></label>
                                                </div>
                                            </div>
                                            <div class="col-sm-4 col xs-6">
                                                <p class="basic_member_disable">Netherlands</p>
                                                <div class="search-padding">
                                                    <input type="checkbox" for="search" <?php echo (in_array('Netherlands', $content->language)) ? "checked" : ""; ?> id="nth" value="Netherlands" name="language" class="checkbox_cus">
                                                    <label for="nth" class="checkbox_label"></label>
                                                </div>
                                            </div>                                          
                                        </div>                                      
                                    </div>
                                </div>
                            </div><!--language --> 
                            <div class="col-sm-6"><!--age and smooking -->                                
                                <div class="heading_loca search-padding-top ">
                                    <div class="col-md-12 col-sm-12 col-xs-12 border-box1">                                        
                                        <h3 class="Search-bg-header blue_heading">Age, smoking</h3>
                                        <div class="space-div"></div>
                                        <div class="row search_input smoking-select">
                                            <div class="col-md-4 col-sm-4 col xs-6">
                                                <div class="form-group">
                                                <?php if($setting[6]->$user_type != '1'){echo '<span class="grayTrans"></span>';}?>
                                                    <label for="inputEmail3" class="col-sm-8 control-label no-padding-right text-left basic_member_disable">Her age between</label>
                                                    <div class="col-sm-4 no-padding-left">
                                                        <select type ="select" name="age_her_from" id="age_her_from" class="form-control">
                                                            <option></option>
                                                            <?php for ($i = 18; $i <= 90; $i++) { ?>
                                                                <option <?php echo ($i ==  $content->she_age[0]) ? "selected" : ""; ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                            <?php } ?>                  
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-sm-2 col xs-6">
                                                <div class="form-group">
                                                <?php if($setting[6]->$user_type != '1'){echo '<span class="grayTrans"></span>';}?>
                                                    <label for="inputEmail3" class="col-sm-5 control-label no-padding-right text-left basic_member_disable">and</label>
                                                    <div class="col-sm-7 no-padding-left">
                                                        <select type ="select" for="search" name="age_her_to" id="age_her_to" class="form-control">
                                                            <option></option>
                                                            <?php for ($i = 18; $i <= 90; $i++) { ?>
                                                                <option <?php echo ($i ==  $content->she_age[1]) ? "selected" : ""; ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                            <?php } ?>  
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-4 col xs-6">
                                                <div class="form-group">
                                                <?php if($setting[6]->$user_type != '1'){echo '<span class="grayTrans"></span>';}?>
                                                    <label for="inputEmail3" class="col-sm-8 control-label no-padding-right text-left basic_member_disable">His age between</label>
                                                    <div class="col-sm-4 no-padding-left">
                                                        <select type ="select" for="search" name="age_his_from" id="age_his_from" class="form-control">
                                                            <option></option>
                                                            <?php for ($i = 18; $i <= 90; $i++) { ?>
                                                                <option <?php echo ($i ==  $content->he_age[0]) ? "selected" : ""; ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                            <?php } ?>                                  
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-sm-2 col xs-6">
                                                <div class="form-group">
                                                <?php if($setting[6]->$user_type != '1'){echo '<span class="grayTrans"></span>';}?>
                                                    <label for="inputEmail3" class="col-sm-5 control-label no-padding-right text-left basic_member_disable">and</label>
                                                    <div class="col-sm-7 no-padding-left">
                                                        <select type ="select" for="search" name="age_his_to" id="age_his_to" class="form-control">
                                                            <option></option>
                                                            <?php for ($i = 18; $i <= 90; $i++) { ?>
                                                                <option <?php echo ($i ==  $content->he_age[1]) ? "selected" : ""; ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                            <?php } ?>                                      
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row search_input">
                                            <div class="col-md-4 col-sm-4 col xs-6">
                                            <?php if($setting[7]->$user_type != '1'){echo '<span class="grayTrans"></span>';}?>
                                                <p class="basic_member_disable">non-smoker required</p>
                                                <div class="search-padding">
                                                    <input type="radio" for="search" id="non-smoker" value="non smoker" name="smoker_or_not" class="checkbox_cus">
                                                    <label for="non-smoker" class="checkbox_label"></label>
                                                </div>
                                            </div>
                                            <div class="col-md-1 col-sm-1 col xs-6"></div>
                                            <div class="col-md-3 col-sm-3 col xs-6">
                                            <?php if($setting[7]->$user_type != '1'){echo '<span class="grayTrans"></span>';}?>
                                                <p class="basic_member_disable">smoking OK</p>
                                                <div class="search-padding">
                                                    <input type="radio" for="search" id="smoker" value="smoker" name="smoker_or_not" class="checkbox_cus">
                                                    <label for="smoker" class="checkbox_label"></label>
                                                </div>
                                            </div>                                          
                                        </div>  
                                    </div>
                                </div><!--age and smooking -->

                                <!--intension -->
                                <div class="heading_loca">
                                    <div class="col-md-12 col-sm-12 col-xs-12 border-box1 <?php echo $hideclass; ?>">
                                        <?php if($setting[8]->$user_type != '1'){echo '<span class="grayTrans"></span>';}?>
                                        <h3 class="Search-bg-header blue_heading">Intension</h3>
                                        <div class="space-div"></div>
                                        <div class="row search_input">
                                            <?php
                                                $args = array(
                                                    'post_type' => 'rooms',
                                                    'tax_query' => array(
                                                        array(
                                                            'taxonomy' => 'int_rooms',
                                                            'field' => 'term_id',
                                                            'terms' => 12)
                                                    ),
                                                    'order' => 'ASC',
                                                    'orderby'=>'ID'
                                                );
                                                $my_query = new WP_Query($args);
                                                $i = 1;
                                                if ($my_query->have_posts()) {
                                                    while ($my_query->have_posts()) : $my_query->the_post();
                                            ?>
                                            <div class="col-md-4 col-sm-4 col xs-6">
                                                <p class="col-sm-10 no-padding"><?php the_title(); ?></p>
                                                <div class="search-padding col-sm-2">
                                                    <input type="checkbox" <?php echo (in_array(get_the_ID(), $content->intension)) ? "checked" : ""; ?> for="search" id="intension-<?php echo get_the_ID(); ?>" value="<?php the_ID(); ?>" name="intension" class="checkbox_cus">
                                                    <label for="intension-<?php echo get_the_ID(); ?>" class="checkbox_label"></label>
                                                </div>
                                            </div>
                                            <?php
                                            if($i == 3)
                                            {
                                              echo "<div class='clear'></div>";     
                                              $i = 1;                                         
                                            }else
                                            {
                                              $i++;  
                                            }
                                                endwhile;
                                            }
                                            wp_reset_query();
                                            ?>
                                        </div>  
                                    </div>
                                </div><!--intension -->


                                <div class="heading_loca">
                                    <div class="col-md-12 col-sm-12 col-xs-12 border-box1 <?php echo $hideclass; ?>">
                                        <?php if($setting[9]->$user_type != '1'){echo '<span class="grayTrans"></span>';}?>
                                        <h3 class="Search-bg-header blue_heading">Erotic preferences</h3>
                                        <div class="space-div"></div>
                                        <div class="row">
                                            <div class="selection-box">
                                                <div class="col-md-5 col-sm-5 col xs-6">
                                                    <select id='canselect_code' multiple class="form-control left-select-box" for="search">
                                                        <?php
                                                            $args = array(
                                                                'posts_per_page' => -1,
                                                                'post_type' => 'erotics',
                                                                'tax_query' => array(
                                                                    array(
                                                                        'taxonomy' => 'category',
                                                                        'terms' => 3
                                                                    )
                                                                )
                                                            );
                                                            $posts = get_posts($args);
                                                            $srchposts1 = get_posts($args);
                                                            foreach ($posts as $key => $value) {
                                                        ?>
                                                            <option value="<?php echo $value->ID; ?>">
                                                                <?php echo qtrans_use($q_config['language'],$value->post_title, true); ?>
                                                            </option>
                                                        <?php } 
                                                            $args = array(
                                                                'posts_per_page' => -1,
                                                                'post_type' => 'erotics',
                                                                'tax_query' => array(
                                                                    array(
                                                                        'taxonomy' => 'category',
                                                                        'terms' => 4
                                                                    )
                                                                )
                                                            );
                                                            $posts = get_posts($args);
                                                            $srchposts2 = get_posts($args);
                                                            foreach ($posts as $key => $value) { ?>
                                                            <option value="<?php echo $value->ID; ?>">
                                                                <?php echo qtrans_use($q_config['language'],$value->post_title, true);?>
                                                            </option>
                                                        <?php } 
                                                            $args = array(
                                                                'posts_per_page' => -1,
                                                                'post_type' => 'erotics',
                                                                'tax_query' => array(
                                                                    array(
                                                                        'taxonomy' => 'category',
                                                                        'terms' => 5
                                                                    )
                                                                )
                                                            );
                                                            $posts = get_posts($args);
                                                            $srchposts3 = get_posts($args);
                                                            foreach ($posts as $key => $value){
                                                        ?>
                                                            <option value="<?php echo $value->ID; ?>">
                                                                <?php echo qtrans_use($q_config['language'],$value->post_title, true); ?>
                                                            </option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <?php 
                                                $allPost = array_merge($srchposts1,$srchposts2,$srchposts3);
                                                foreach ($allPost as $key => $value) 
                                                {
                                                    $allPOstID['id'][] = $value->ID;
                                                    $allPOstTitle['title'][] = $value->post_title;
                                                }
                                                $html = "";                                                
                                                if(count($content->erotics) > 0)
                                                {
                                                    $i=0;
                                                    foreach ($content->erotics[0] as $key => $value) 
                                                    {
                                                        $title = qtrans_use($q_config['language'],$allPOstTitle['title'][$i], true);
                                                        if(in_array($value, $allPOstID['id']))
                                                        {
                                                          $html .= "<option selected='' value='$value'>$title</option>";  
                                                        }                                                        
                                                    }                                                    
                                                }
                                                ?>
                                                <div class="col-sm-2 no-padding">
                                                    <div class="s-b-l">
                                                        <span id='btnRight_code'>
                                                            <img src="<?php echo get_template_directory_uri(); ?>/images/select-box-btn-l.jpg" alt="logo" class="img-responsive" />
                                                        </span>
                                                    </div>
                                                    <div class="s-b-r">
                                                        <span id='btnLeft_code'>
                                                            <img src="<?php echo get_template_directory_uri(); ?>/images/select-box-btn-r.jpg" alt="logo" class="img-responsive" />
                                                        </span>                         
                                                    </div>
                                                </div>
                                                <div class="col-md-5 col-sm-5 col xs-6">
                                                    <label class="right-box-label">Your selection (max. 3)</label>
                                                    <select for="search" name="eroticpre" id='isselect_code' multiple class="form-control right-select-box">
                                                    <?php echo $html; ?>
                                                    </select>
                                                </div>
                                            </div>  
                                        </div>          
                                    </div>
                                </div><!--erotic prefrencess-->
                            </div>
                            <div class="row">
                                <div class="col-sm-6 heading_loca">
                                    <div class="col-sm-8 <?php if($user_type != 'premium'){echo '';}?>">
                                        <?php if($setting[11]->$user_type != '1'){echo '<span class="grayTrans"></span>';}?>
                                        <div class=" search-padding">
                                            <h4>Set this selection as your standard selection</h4>
                                            <input type="checkbox" for="search" id="select_standard" value="married" name="married" class="checkbox_cus">
                                            <label for="select_standard" class="checkbox_label"></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="heading_loca intence">
                                            <input type="hidden" name="type" id="user_type" value="<?php echo $user_type; ?>">
                                            <a class="btn_search" id="search_data" href="javascript:void(0)">Search</a>
                                        </div>
                                    </div>
                                </div>
                            </div><!--search button-->
                        </div>
                    </div>
                </div>
            </div>
        </form><!--form close -->
    </div>
</div>
<script language="javascript">
    populateCountries("country", "state" ,"<?php echo $country; ?>", "<?php echo $region; ?>");
</script>
<script language="javascript">
    populateCountries("country2", "state2", "<?php echo $country2; ?>", "");
</script>
<script language="javascript">
    populateCountries("country3", "state3", "<?php echo $country3; ?>", "");
</script>

<script>
jQuery('#premium').click(function(){
    if (this.checked == true)
     {
        jQuery('.disable').each(function()
        {
            jQuery(this).removeAttr('checked');
            jQuery(this).attr('disabled', 'disabled');
        });
    }else
    {
        jQuery('.disable').each(function()
        {
            jQuery(this).removeAttr('disabled');
        });
    }    
});
</script>

<script>
jQuery('#all_over_europe').click(function(){
    if (this.checked == true)
     {
        jQuery(this).attr('checked','checked');
        jQuery('.country_disable').each(function()
        {
            jQuery(this).removeAttr('checked');
            jQuery(this).attr('disabled', 'disabled');
        });
     }else
     {
        jQuery(this).removeAttr('checked');
        jQuery('.country_disable').each(function()
        {
            jQuery(this).removeAttr('disabled');
        });
     }
});
</script>


<script>
jQuery('input#all_first_country_region').click(function(){
    if(jQuery(this).attr('checked') != 'checked'){
        jQuery(this).removeAttr('checked');               
    }else
    {
        jQuery(this).attr('checked','checked');        
    }    
});
jQuery('input#all_second_country_region').click(function(){
    if(jQuery(this).attr('checked') != 'checked'){
        jQuery(this).removeAttr('checked');        
    }else
    {
        jQuery(this).attr('checked','checked');        
    }    
});
jQuery('input#all_region2').click(function(){
    if(jQuery(this).attr('checked') != 'checked'){
        jQuery(this).removeAttr('checked');        
    }else
    {
        jQuery(this).attr('checked','checked');        
    }    
});
</script>
<script>
jQuery('#select_standard').click(function()
{    
    if (this.checked == true)
    {        
        jQuery(this).attr('checked', 'checked');
    }
    else
    {
        jQuery(this).removeAttr('checked');
    }        
 
});
    
</script>

<script language="javascript">
    jQuery(document).ready(function () {
        jQuery('#search_data').click(function () { 
        var user_type = jQuery("#user_type").val();        
        var newArray = {};
        if(jQuery('#all_over_europe').attr('checked') == 'checked')
        {
            newArray['europeCountry'] =  ['Germany','France','Italy', 'Romania', 'Netherlands', 'Belgium', 'Greece', 'Belarus', 'Austria', 'Switzerland', 'Bulgaria', 'Finland'];
        }
        else
        {
            if(jQuery('select#country option:selected').val() != "")
            {
            newArray['country1'] = jQuery('select#country option:selected').val();
                if(jQuery('input#all_first_country_region').attr('checked') == 'checked')
                {
                    var state = [];
                    jQuery('select[for="state1"]:first option').each(function(index,value){
                        if(jQuery(this).val() != "")
                        {
                            state.push(jQuery(this).val());
                        }                    
                    });
                    newArray['state1'] =  state;
                }else
                {                 
                    var state = [];
                    jQuery('select[for="state1"] option:selected').each(function(index,value){
                        state.push(jQuery(this).val());
                    });
                    newArray['state1'] =  state;
                }
            }
// Second country 
            if(jQuery('select#country2 option:selected').val() != "")
            {
                newArray['country2'] = jQuery('select#country2 option:selected').val(); 
                if(jQuery('input#all_second_country_region').attr('checked') == 'checked')
                {
                    var state = [];
                    jQuery('select[for="state2"]:first option').each(function(index,value){
                        if(jQuery(this).val() != "")
                        {
                            state.push(jQuery(this).val());
                        }                    
                    });
                    newArray['state2'] =  state;
                }else
                {
                    var state = [];
                    jQuery('select[for="state2"] option:selected').each(function(index,value){
                        state.push(jQuery(this).val());
                    });
                    newArray['state2'] =  state;
                }            
            }
//3RD country
        if(jQuery('select#country3 option:selected').val() != "")
            {
                newArray['country3'] = jQuery('select#country3 option:selected').val(); 
                if(jQuery('input#all_third_country_region').attr('checked') == 'checked')
                {
                    var state = [];
                    jQuery('select[for="state3"]:first option').each(function(index,value){
                        if(jQuery(this).val() != "")
                        {
                            state.push(jQuery(this).val());
                        }                    
                    });
                    newArray['state3'] =  state;
                }else
                {
                    var state = [];
                    jQuery('select[for="state3"] option:selected').each(function(index,value){
                        state.push(jQuery(this).val());
                    });
                    newArray['state3'] =  state;
                }                
            } 
        }
            
            var status = [];
            var playroom = [];
            var language = [];
            var intension = [];
            var intension = [];
            var smoke = [];
            var premium = [];
            var eroticsProfile = [];                    
            jQuery('input[for="search"]').each(function () {                
                if (jQuery(this).attr('usertype') == 'user_type')
                {
                    if (jQuery(this).is(':checked'))
                    {
                        status.push(jQuery(this).val());                                            
                    }
                }
                if (jQuery(this).attr('name') == 'premium')
                {
                    if (jQuery(this).is(':checked'))
                    {
                        premium.push(jQuery(this).val());                                            
                    }
                }
                if (jQuery(this).attr('name') == 'erotic_profile')
                {
                    if (jQuery(this).is(':checked'))
                    {
                        eroticsProfile.push(jQuery(this).val());                                            
                    }
                }    
                if (jQuery(this).attr('name') == 'int_data')
                {
                    if (jQuery(this).is(':checked'))
                    {
                        playroom.push(jQuery(this).val());                                            
                    }
                }
                if (jQuery(this).attr('name') == 'language')
                {
                    if (jQuery(this).is(':checked'))
                    {
                        language.push(jQuery(this).val());                                            
                    }
                }
                if (jQuery(this).attr('name') == 'intension')
                {
                    if (jQuery(this).is(':checked'))
                    {
                        intension.push(jQuery(this).val());                                            
                    }
                }
                if (jQuery(this).attr('name') == 'smoker_or_not')
                {
                    if (jQuery(this).is(':checked'))
                    {
                        smoke.push(jQuery(this).val());                                            
                    }
                }                
            });            
            newArray['premium'] =  premium;
            newArray['eroticsProfile'] =  eroticsProfile;
            newArray['status'] =  status;            
            newArray['playroom'] =  playroom;
            newArray['language'] =  language;
            newArray['intension'] =  intension;
            newArray['smoke'] =  smoke;

            var erotics = [];
            jQuery('select[for="search"]').each(function () {
                if(jQuery(this).attr('name') == 'eroticpre')
                {
                    erotics.push(jQuery(this).val());                  
                } 
            });            
            newArray['erotics'] =  erotics;
            var she_age = [];                        
            jQuery('select#age_her_from option:selected').each(function ()
            {
                if (jQuery(this).val() != "")
                {
                    she_age.push(jQuery(this).val());                     
                }
            });
            newArray['she_age'] =  she_age;
            jQuery('select#age_her_to option:selected').each(function ()
            {
                if (jQuery(this).val() != "")
                {
                    she_age.push(jQuery(this).val());
                }
            });
            newArray['she_age'] =  she_age;
            var he_age = [];
            jQuery('select#age_his_from option:selected').each(function ()
            {
                if (jQuery(this).val() != "")
                {
                    he_age.push(jQuery(this).val());                    
                }
            });
            jQuery('select#age_his_to option:selected').each(function ()
            {
                if (jQuery(this).val() != "")
                {
                    he_age.push(jQuery(this).val());
                }
            });
            newArray['he_age'] =  he_age;
            var json = JSON.stringify(newArray);    
            //alert(json);       
            if(jQuery('#select_standard').attr('checked'))
            {
                var data = 'action=my_action_search_user&type=member&data=' + json + '&standard=standard';
            }else
            {
                var data = 'action=my_action_search_user&type=member&data=' + json;
            }
            jQuery.ajax({
                url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
                data: data,
                type: 'POST',
                dataType: 'html',
                success: function (response) {
                    jQuery("#update").html(response);
                    jQuery("#update").addClass('alert alert-success');
                    jQuery("#update").show();
                    setTimeout(function(){jQuery('#update').fadeOut(2000);}, 3000);
                    window.location = "<?php echo get_page_link(337); ?>";
                }
            });
        });
    }); 
</script>
<script type="text/javascript">
jQuery('#standard').click(function(){
    var type = '<?php echo $_GET["type"]; ?>';
    if(type != "")
    {
        window.location = '<?php echo site_url(); ?>'+'/?page_id=309';
    }
    else{
         window.location = '<?php echo site_url(); ?>'+'/?page_id=309&type=standard';
    }
    
});
    
</script>
<script>
    var count = 1;
    jQuery('[id^=\"btnRight_code\"]').click(function (e) {
        var option = jQuery('#canselect_code').find("option:selected").text();
        var value = jQuery('#canselect_code').find("option:selected").val();
        if (value != '' && value != undefined)
        {
            var addOption = '<option selected="" value="' + value + '">' + option + '</option>';
            if (jQuery('#isselect_code').find('option').length < 3)
            {
                jQuery('#isselect_code').append(addOption);
                count++;
            }
            else
            {
                alert("You can select maximum 3 erotic preferences");
            }
        }
    });

    jQuery('[id^=\"btnLeft_code\"]').click(function (e) {
        if (count > 1)
        {
            jQuery('#isselect_code').each(function(){
                jQuery(this).find('option:selected').remove();
            })            
        }
    });

    jQuery('#isselect_code').click(function()
    {
        jQuery(this).find('option[value="'+jQuery(this).val()+'"]').attr('selected','selected');
    })
</script>

<script>
    var count = 2;
    jQuery(document).on('click', '#add_other_country', function () {
        if (count <= 3)
        {
            jQuery("#countrydiv"+count).show();
            count++;
        }
    });
</script>

<?php
}else {
    wp_redirect(home_url());
}
?>
<?php get_footer(); ?>
