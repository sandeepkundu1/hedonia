<?php
/*
 * Template Name: View Member Profile Page
 * Description: A Page Template with Display the member profile Information.
 * Author Name : Sanjay Shinghania
 */

get_header();
global $wpdb;
#erotic color display
$textColor = array(
            "182" => "#395723", 
            "183" => "#476D2D", 
            "184" => "#61953D", 
            "185" => "#A9D12A",
            "186" => "#E2F0D9", 
            "187" => "#ECF5E7", 
            "706" => "#F8FBF7", 
            "707" => "#F2F2F2", 
            "708" => "#F2F2F2", 
            "709" => "#F2F2F2", 
            "710" => "#F2F2F2", 
            "711" => "#F2F2F2", 
            "712" => "#F2F2F2", 
            "713" => "#F2F2F2", 
            "714" => "#F2F2F2", 
            "715" => "#FF3F3F",
            "716" => "#C00000"             
        );

$forColor = array(
        "182" => "#fff", 
        "183" => "#fff", 
        "184" => "#fff", 
        "185" => "#fff",
        "186" => "#000", 
        "187" => "#000", 
        "706" => "#000", 
        "707" => "#000", 
        "708" => "#000", 
        "709" => "#000", 
        "710" => "#000", 
        "711" => "#000", 
        "712" => "#000", 
        "713" => "#000", 
        "714" => "#000", 
        "715" => "#000", 
        "716" => "#000"            
    );

#...current user detail
$current_user_ID        = get_current_user_id();
$get_current_user_info  = get_user_meta($current_user_ID);

//get the user id and display user profile by id
$user_id            = $_GET['user_id'];
$get_user_info      = get_user_meta($user_id);
$membership_status  = get_user_meta($user_id, 'membership_status', true);
$user               = get_user_by('id', $user_id);
if(isset($_GET['user_id'])){
    $userid = $_GET['user_id'];
    $url    = get_permalink(262).'&userid='.$userid;
}else{
    $url    = 'javascript:void(0)';  
}

#*== Display the profile pick
$her_pick_name      = get_user_meta($user_id,'her_pick',true);
$his_pick_name      = get_user_meta($user_id,'his_pick',true);
$upload_dir         = wp_upload_dir();
$image_url_female   = $upload_dir['baseurl'] . '/profilepick_' . $user_id . '/' . $her_pick_name;
$image_url_male     = $upload_dir['baseurl'] . '/profilepick_' . $user_id . '/' . $his_pick_name;

#===* Age  for couples
$age1           = get_user_meta($user_id,'she_dob',true);
$age2           = get_user_meta($user_id,'he_dob',true);
if($age1=='undefined')
{
   $age1 = date();
}
if($age2=='undefined')
{
   $age2 = date();
}

$setting = $wpdb->get_results("select * from wp_admin_portal_setting where portal_setting_type = 'portal setting' ");

$current_date   = date();
$datetime1  = new DateTime($current_date);
$datetime2  = new DateTime($age1);
$her_age    = $datetime1->diff($datetime2);
$female_age = $her_age->y;

$datetime3  = new DateTime($current_date);
$datetime4  = new DateTime($age2);
$his_age    = $datetime3->diff($datetime4);
$male_age   = $his_age->y;
#--- height
$her_hight = $get_user_info['her_hight']['0'];
$his_hight = $get_user_info['his_hight']['0'];
#=== Figure
$her_figure = $get_user_info['her_figure']['0'];
$his_figure = $get_user_info['his_figure']['0'];
#=== smoke
$her_smoke = $get_user_info['her_smoke']['0'];
$his_smoke = $get_user_info['his_smoke']['0'];
#=== Language
$her_language = $get_user_info['her_language']['0'];
$her_language1 = $get_user_info['her_language1']['0'];
$her_language2 = $get_user_info['her_language2']['0'];
$her_language3 = $get_user_info['her_language3']['0'];

$selectedLanguages = "";
if($her_language != ""){
$selectedLanguages .= $her_language . ",";
}
if($her_language1 != ""){
$selectedLanguages .= $her_language1 . ",";
}
if($her_language2 != ""){
$selectedLanguages .= $her_language2 . ",";
}
if($her_language3 != ""){
$selectedLanguages .= $her_language3 . ",";
}
$language_array = array($her_language,$her_language1,$her_language2,$her_language3);

$his_language   = $get_user_info['his_language']['0'];
$secondLang = $get_user_info['language_two']['0'];
$thrd_lang = $get_user_info['language_thre']['0'];
$forLang =  $get_user_info['language_for']['0']; 

$selectedLanguages1 = "";
if($his_language != ""){
$selectedLanguages1 .= $his_language . ",";
}
if($secondLang != ""){
$selectedLanguages1 .= $secondLang . ",";
}
if($thrd_lang != ""){
$selectedLanguages1 .= $thrd_lang . ",";
}
if($forLang != ""){
$selectedLanguages1 .= $forLang . ",";
}   

$language_array1 = array($his_language,$secondLang,$thrd_lang,$forLang);
#=== weight
$her_weight = $get_user_info['her_weight']['0'];
$his_weight = $get_user_info['his_weight']['0'];
#=== profession
$her_profession = $get_user_info['her_profession']['0'];
$his_profession = $get_user_info['his_profession']['0'];
#=== hobby
$her_hobby = $get_user_info['her_hobby']['0'];
$his_hobby = $get_user_info['his_hobby']['0'];

$describe = $get_user_info['describe']['0'];
$des_status = $get_user_info['describe_status']['0'];
$fav_quote_content = $get_user_info['faq_quote']['0'];
#=== display countryINTRO
$country = $get_user_info['country']['0'];
$data = unserialize($country);
$main_country = $data['0'];
$second_country = $data['1'];

$district1 = $get_user_info['district1']['0'];
$datadistrict = unserialize($district1);
$main_district = $datadistrict['0'];
$second_district = $datadistrict['1'];

$city1 = $get_user_info['city1']['0'];
$datacity = unserialize($city1);
$main_city = $datacity['0'];
$second_city = $datacity['1'];

/**** perfect match *****/
$reg3_txtshe = $get_user_info['reg3_txtshe']['0'];
$reg3_txthe = $get_user_info['reg3_txthe']['0'];
$reg3_txtboth = $get_user_info['reg3_both']['0'];

// current user type
$current_user_type = get_user_meta($current_user_ID, 'user_type', true);

// member profile member type
$member_user_type = get_user_meta($userid, 'member_type', true);
$user_type_name = get_user_meta($current_user_ID, 'user_type', true);
$profile_name = get_option($user_type_name);
#==== page id and data
$page_id = get_the_ID();
$page_title = get_page($page_id);
$page_title->post_title;

#====show album
$showalbum = '';
if ($current_user_type == 'basic') {
    $showalbum = '1';
} elseif ($current_user_type == 'validated') {
    $showalbum = '2';
} else {
    $showalbum = '5';
}

$appproved = get_user_meta($user_id,'approved_as_basic',true);

/**** Query for add to fav ****/
$addbysender = $wpdb->get_results("select * from `wp_user_profile` where `reciver_id` = '$user_id' AND sender_id = '$current_user_ID' AND `add_fav` = '1'");
$addbyrecivder = $wpdb->get_results("select * from `wp_user_profile` where `reciver_id` = '$current_user_ID' AND sender_id = '$user_id' AND `add_fav` = '1'");

if(count($addbysender) > 0 && count($addbyrecivder) > 0){
    $style = 'style="color:green"';
    $text = 'i like add to unfavourites';
    $image = 'hand.jpg';
}else if(count($addbysender) > 0)
{
    $style = '';
    $text = 'i like add to unfavourites';
    $image = 'hand.jpg';
}else{
    $style = '';
    $text = 'i like add to favourites';
    $image = 'hand.jpg';
}

$blockuser = $wpdb->get_results("select block_profile from wp_user_profile where reciver_id = '$userid' AND sender_id = '$current_user_ID'");
if(count($blockuser) > 0)
{
    if($blockuser[0]->block_profile == 1)
    {
        echo "<div class='text-center succesPoint'>
                <h4>You profile blocked by this user!</h4>
             </div>";
        exit;
    }
}
if($membership_status == 'Deactive')
{
    echo "<div class='text-center succesPoint'>
            <h4>This member profile deactivate!</h4>
         </div>";
    exit;
}

#.......check the user logged in or  not
if (is_user_logged_in() && isset($user_id)) 
{
    if ($current_user_ID != $user_id) 
    {
        switch ($member_user_type) 
        {
            case 'couples': // display couple html
?>
<div class="userProfile">
    <div class="container">
        <div class="parent_div">
            <form class="form-horizontal" role="form" name="form" method="post" action ="">
                <div class="col-sm-7 col-md-7 col-xs-12 no-padding-right">
                    <!--image show-->
                    <div class="col-sm-6 col-md-6 col-xs-12 no-padding" style="margin-top:25px">
                        <div class="clear">
                            <div class="col-md-6 col-sm-6 no-padding-left">
                                <h3 class="search_premium_heading red_heading" >
                                    <?php echo ucfirst(get_user_meta($user_id,'nickname',true)); ?>
                                </h3>
                                <?php if ($her_pick_name != 'no_img.jpg') { ?>
                                    <img class="prof-cartoon-img" src="<?php echo $image_url_female; ?>">
                                <?php } else { ?>
                                    <img class="prof-cartoon-img" src="<?php echo bloginfo('template_url'); ?>/images/images.jpeg">
                                <?php } ?>
                            </div>
                            <div class="col-md-6 col-sm-6 no-padding-left">                         
                                <?php if ($his_pick_name == 'no_img.jpg') { ?> 
                                    <img class="prof-cartoon-img" src="<?php echo bloginfo('template_url'); ?>/images/images.jpeg">                                                        
                                <?php } else { ?>
                                    <img class="prof-cartoon-img" src="<?php echo $image_url_male; ?>">
                                <?php } ?>
                            </div>
                        </div> 
                    </div><!--profile image and name-->

                    <!--member name-->
                    <div class="col-sm-6 no-padding">
                        <?php if ($user_type_name == 'premium') { ?>
                            <h5 class="red profileType">
                                <?php if ($profile_name) {
                                    echo $profile_name;
                                } else {
                                    echo "Premium Member";
                                } ?>
                            </h5>
                        <?php } elseif ($user_type_name == 'validated') { ?>   
                            <h5 class="greenText profileType">
                                <?php if ($profile_name) {
                                    echo $profile_name;
                                } else {
                                    echo "Validated Member";
                                } ?>
                                <img width="12" src="<?php echo bloginfo('template_url'); ?>/images/tick-green.png" alt="">
                            </h5>
                        <?php } elseif ($user_type_name == 'basic' && $appproved == 'yes') { ?>
                            <h5 class="greenText profileType" style="color:#989898;">
                                <?php if ($profile_name) {
                                    echo $profile_name;
                                } else {
                                    echo "Basic Member";
                                } ?>
                            </h5>
                        <?php } elseif ($appproved == 'no') { ?>
                            <h5 class="greenText profileType" style="color:#989898;">
                                <?php echo "Pre-membership stage"; ?>
                            </h5>
                        <?php } ?>
                        
                        <!--basic info -->
                        <div class="Profile-Part-BI cus_scroll">
                            <h3 class="Proh-h3">
                                <span>BASIC INFO</span>
                            </h3>
                            <div class="row">
                                <p class="Pro-p col-sm-4 col-xs-offset-4">she</p>
                                <p class="Pro-p col-sm-4">he</p> 
                            </div>
                            <div class="padding-basic-info">
                                <div class="form-group border-bottom">
                                    <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Age</label>
                                    <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center bold"><?php echo $female_age; ?></label>
                                    <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center bold"><?php echo $male_age; ?></label>
                                </div>
                                <div class="form-group border-bottom">
                                    <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Height (cm)</label>
                                    <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center bold"><?php echo get_user_meta($user_id,'her_hight',true); ?></label>
                                    <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center bold"><?php echo get_user_meta($user_id,'his_hight',true); ?></label>
                                </div>
                                <div class="form-group border-bottom">
                                    <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Figure</label>
                                    <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center"><?php echo get_user_meta($user_id,'her_figure',true); ?></label>
                                    <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center"><?php echo get_user_meta($user_id,'his_figure',true); ?></label>
                                </div>
                                <div class="form-group border-bottom">
                                    <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Relationship</label>
                                    <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center"><?php echo get_user_meta($user_id,'relation_ship',true); ?></label>
                                </div>
                                <div class="form-group border-bottom">
                                    <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Smoking</label>
                                    <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center"><?php echo get_user_meta($user_id,'her_smoke',true); ?></label>
                                    <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center"><?php echo get_user_meta($user_id,'his_smoke',true);; ?></label>
                                </div>

                                <div class="form-group border-bottom row">
                                    <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Hair color</label>
                                    <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center"><?php echo get_user_meta($user_id,'her_haircolor',true); ?></label>
                                    <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center"><?php echo get_user_meta($user_id,'his_haircolor',true); ?></label>
                                </div>

                                <div class="form-group border-bottom">
                                    <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Languages</label>
                                    <label for="inputEmail3" class="col-sm-4 BasicInfo-label"><?php echo str_replace(',', ', ', rtrim($selectedLanguages,",")); ?></label>
                                    <label for="inputEmail3" class="col-sm-4 BasicInfo-label"><?php echo str_replace(',', ', ', rtrim($selectedLanguages1,",")); ?></label>
                                </div>
                                <div class="form-group border-bottom">
                                    <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Education</label>
                                    <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center"><?php echo get_user_meta($user_id,'her_graduation',true); ?></label>
                                    <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center"><?php echo get_user_meta($user_id,'his_graduation',true); ?></label>
                                </div>                                        
                                <div class="form-group border-bottom">
                                    <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Hobbies Interests</label>
                                    <label for="inputEmail3" class="col-sm-4 BasicInfo-label"><?php echo get_user_meta($user_id,'her_hobby',true); ?></label>
                                    <label for="inputEmail3" class="col-sm-4 BasicInfo-label"><?php echo get_user_meta($user_id,'his_hobby',true);; ?></label>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Choose your basic info about you</label>
                                    <label for="inputEmail3" class="col-sm-4 BasicInfo-label"><?php echo get_user_meta($user_id,'about_her',true); ?></label>
                                    <label for="inputEmail3" class="col-sm-4 BasicInfo-label"><?php echo get_user_meta($user_id,'about_his',true); ?></label>
                                </div>
                            </div>
                        </div>
                        <!--basic information -->
                    </div> 
                    <!--col-sm-6-->

                    <div class="clear">
                        <div class="col-sm-4 col-md-4 col-xs-12 no-padding">
                            <div class="list_div">
                                <ul class="side-bar-profile no-padding">                                    
                                    <li class="<?php echo ($operational_setting[3]->$user_type_name != 1) ? 'invalid' : ''; ?>">
                                        <?php  echo ($operational_setting[3]->$user_type_name != 1) ? "<span class='grayTrans'></span>" : ""; ?>
                                        <a href ="javascript:void(0);" id ="add_fav">
                                            <i><img src="<?php echo bloginfo("template_url"); ?>/images/<?php echo $image; ?>"></i>
                                            <span <?php echo $style; ?>><?php echo $text; ?></span>
                                        </a>
                                        <span class="successMsg" id="add_fav_msg" style="display: none;"></span>
                                        <span style="display:none;text-align: center" class="bar_img1">
                                            <img width="60px" height="12px" src="<?php echo site_url(); ?>/wp-content/themes/twentyfourteen/images/ajax-status.gif">
                                        </span>
                                    </li>
                                    <li class="<?php echo ($operational_setting[5]->$user_type_name != 1) ? 'invalid' : ''; ?>">
                                        <?php echo ($operational_setting[5]->$user_type_name != 1) ? "<span class='grayTrans'></span>" : ""; ?>
                                     <span class="successMsg" id="kiss_msg" style="display: none;"></span>
                                        <a href ="javascript:void(0);" id="send_a_kiss">
                                            <i><img src="<?php echo bloginfo("template_url"); ?>/images/lip.jpg"></i> 
                                            <span>send a kiss</span>
                                        </a>
                                    </li>  
                                    <li>
                                        <a href ="javascript:void(0);" id ="send_mail" >
                                            <i><img src="<?php echo bloginfo("template_url"); ?>/images/mes1.jpg"></i>
                                            <span>send mail</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo $url; ?>" target="_blank">
                                            <i><img src="<?php echo bloginfo("template_url"); ?>/images/mes3.jpg"></i>
                                            <span>mail history</span>
                                        </a>
                                    </li>
                                    <li class="<?php echo ($current_user_type != 'premium') ? "invalid" : ""; ?>">
                                        <span class="<?php echo ($operational_setting[9]->$user_type_name  != 1 && $get_user_info['approved_as_basic']['0'] == 'no') ? "grayTrans" : ""; ?>"></span>
                                        <a href="javascript:void(0)" id ="hide_show">
                                            <i><img src="<?php echo bloginfo("template_url"); ?>/images/lockband.jpg"></i> 
                                            <span>hide/show profile open/close albums erotic preferences</span>
                                        </a>
                                    </li>
                                    <li class="<?php echo ($user_type_name != 'premium') ? 'invalid' : ''; ?>">
                                        <span class="<?php echo ($operational_setting[9]->$user_type_name  != 1 && $get_user_info['approved_as_basic']['0'] == 'no') ? 'grayTrans' : ''; ?>"></span>
                                        <a href ="javascript:void(0)" id="makenote" data-toggle="modal" data-target="#make_note">
                                            <i><img src="<?php echo bloginfo("template_url"); ?>/images/note.jpg"></i>
                                            <span>make a note for this profile</span>
                                        </a>
                                    </li>
							<?php if ($user_type_name == 'basic' && $operational_setting[3]->$user_type_name != 1) {?>     
                                <li style="position:relative;"> 
									<span class="grayTrans"></span>	
							
                             
                                            <i><img src="<?php echo bloginfo("template_url"); ?>/images/block.jpg"></i>
                                            <span>block this profile</span>
                                   
                                </li>
                     
                                <li style="position:relative;"> 
									<span class="grayTrans"></span>	
							
                                    <i><img src="<?php echo bloginfo("template_url"); ?>/images/submit.jpg"></i>
                                    <span>submit complaint<br/> about this profile</span>
                                </li>
                                <?php } else { ?>
                                  <li>      <a href="javascript:void(0)" id ="block_profile">
                                            <i><img src="<?php echo bloginfo("template_url"); ?>/images/block.jpg"></i>
                                            <span>block this profile</span>
                                        </a>
                                </li>
                                <li> 
                                        <a href = "javascript:void(0)" data-toggle="modal" data-target="#report_abuse">
                                            <i><img src="<?php echo bloginfo("template_url"); ?>/images/submit.jpg"></i>
                                            <span>submit complaint about this profile</span>
                                        </a>
                                </li>
                                <?php } ?>                                  
                                </ul>
                                 <span class="bar_img" style="display:none;">
                                    <img height="12px" width="60px" src="<?php echo get_template_directory_uri(); ?>/images/ajax-status.gif">
                                </span>
                                <span id="block_user_profile" class="successMsg"></span>
                            </div> 
                            <!--list div -->
                            <hr/><span class="border-bottom"></span>

                                <div class="text-box-round <?php if($operational_setting[9]->$user_type_name  != 1 && $get_user_info['approved_as_basic']['0'] == 'no'){echo 'invalid';} ?>">
                                <?php if($operational_setting[9]->$user_type_name  != 1 && $get_user_info['approved_as_basic']['0'] == 'no'){echo '<span class="grayTrans"></span>';} ?>
                                <?php $dating_ads=$wpdb->get_results("SELECT * FROM wp_dating_ads WHERE user_id=".$user_id." ORDER BY ID DESC LIMIT 0,2");
                                    $upload_dir = wp_upload_dir();
                                    $img_srcpath = $upload_dir['baseurl'] . '/dating_ads/'; 
                                ?>
                                <h5><?php if($member_user_type=='couples'){ echo "OUR SPEED DATING";} else{  echo "MY SPEED DATING"; }?></h5>
                                <?php foreach ($dating_ads as $value) 
                                    { 
                                    $from=strtotime($value->from);
                                    $to=strtotime($value->to); 
                                    $from_m=date('m', $from);
                                    $monthName_from = date('F', mktime(0, 0, 0, $from_m, 10));
                                    $to_m=date('m', $to);
                                    $monthName_to = date('F', mktime(0, 0, 0, $to_m, 10));
                                    $from_y=date('Y', $from);
                                    $to_y=date('Y', $to);
                                    $from_d=date('d', $from);
                                    $to_d=date('d',$to);
                                    $country=$value->country;
                                    $city=$value->region;
                                ?>
                                <div class="form-group"> 
                                   <div class="adds_content no-padding-left col-sm-7">
                                   <p>
                                   <?php echo $country."-"; ?>
                                   <span class="city_white"><?php echo $city; ?></span><br/>
                                   <?php echo $from_d."-".$to_d; ?>
                                   <?php if($from_m==$to_m)
                                   {
                                    echo $monthName_from;
                                   }
                                   else
                                   {
                                    echo $monthName_from."-".$monthName_to;
                                   }
                                   ?>
                                   <?php echo $to_y; ?>
                                   </p>
                                   <a href="<?php echo home_url(); ?>/?page_id=735&user_id=<?php echo $user_id; ?>" class="goAdd pull-right">go to ads</a>
                                   </div>
                                   <div class="col-sm-5">
                                      <img src="<?php echo $img_srcpath.$value->ads_pick; ?>" alt="adds image" class="img-responsive">
                                   </div>
                                 </div>
                               <?php }
                                 ?>
                            </div>
                            <div class="text-box-round">
                                <?php
                                $travels_ads=$wpdb->get_results("SELECT * FROM wp_travel_dating WHERE tarvel_logged_user=".$user_id." ORDER BY ID DESC"); 
                                ?>
                                <h5><?php if($member_user_type=='couples'){ echo "OUR TRAVEL AGENDA";} else{  echo "MY TRAVEL AGENDA"; }?></h5>
                                <?php 
                                    foreach ($travels_ads as $value) 
                                    { 
                                    $from=strtotime($value->travel_from);
                                    $to=strtotime($value->travel_to); 
                                    $from_m=date('m', $from);
                                    $monthName_from = date('F', mktime(0, 0, 0, $from_m, 10));
                                    $to_m=date('m', $to);
                                    $monthName_to = date('F', mktime(0, 0, 0, $to_m, 10));
                                    $from_y=date('Y', $from);
                                    $to_y=date('Y', $to);
                                    $from_d=date('d', $from);
                                    $to_d=date('d',$to);
                                    $country=$value->travel_country;
                                    $city=$value->travel_region;
                                ?>
                                <div class="form-group">
                                    <?php echo ($operational_setting[10]->$current_user_type == 0) ? "<span class='grayTrans'></span>" : ''; ?> 
                                       <div class="adds_content no-padding-left col-sm-12">
                                       <p>
                                       <?php echo $country."-"; ?>
                                       <span class="city_white"><?php echo $city; ?></span><br/>
                                       <?php echo $from_d."-".$to_d; ?>
                                       <?php if($from_m==$to_m)
                                       {
                                        echo $monthName_from;
                                       }
                                       else
                                       {
                                        echo $monthName_from."-".$monthName_to;
                                       }
                                       ?>
                                       <?php echo $to_y; ?>
                                       </p>
                                       </div>
                                       </div>
                                      <?php } ?>
                            </div>
                        </div>  
                        <!--col-sm-4 -->

                        <div class="col-sm-8 col-md-8 col-xs-12 no-padding">
                            <div class="col-sm-7 col-md-7 col-xs-12 ">
                                <div class="Album-Part">
                                    <div class="row"> 
                                        <h3 class="Pro-h3-2 col-sm-12 col-md-12"><span>ALBUMS</span></h3>
                                    </div>
                                    <div class="WidthFull">
                                        <?php
                                            $results    = $wpdb->get_results("SELECT * FROM user_create_album WHERE user_id = '" . $user_id . "'");
                                            $upload_dir = wp_upload_dir();
                                            $imgPath    = $upload_dir['baseurl'] . '/users_album/album_' . $user_id . '/';
                                            $i = 1;
                                            $count = 1;
                                            foreach ($results as $value) 
                                            {
                                                $select_img = $wpdb->get_results("SELECT * FROM user_album_images WHERE album_id = '$value->id' order by img_order ASC limit 1");
                                                if (count($select_img) == 0) 
                                                {
                                        ?>
                                                <figure class="album-img-div text-center <?php if ($count <= $showalbum) {echo '';} else {echo 'invalid';} ?>">
                                                    <?php
                                                        if ($count <= $showalbum) {
                                                            echo '';
                                                        } else {
                                                            echo '<span class="grayTrans"></span>';
                                                        }
                                                    ?>
                                                    <img src="<?php bloginfo('template_url'); ?>/images/NoAlbum.jpeg" alt="">
                                                    <figcaption><?php echo $count++; ?></figcaption>
                                                </figure>
                                                <?php
                                                } else 
                                                {
                                                foreach ($select_img as $select){
                                                $number = '"' . $value->id . '"';
                                                ?>                                          
                                                <figure class="album-img-div text-center">
                                                    <a href="<?php echo get_permalink(761); ?>&album=<?php echo $value->id; ?>&user_id=<?php echo $_GET['user_id'];?>" name='imgid' target='_blank' value="<?php echo $value->id; ?>" >
                                                        <img src="<?php echo $imgPath . $select->img_url; ?>">
                                                    </a>
                                                    <figcaption><?php echo $count++; ?></figcaption>
                                                </figure>                                      
                                                <?php
                                                    }
                                                }
                                            }// end foreach
                                            if (count($results) != 0) 
                                            {
                                                $sum = 1;
                                                for ($i = 0; $i < (5 - count($results)); $i++) 
                                                {
                                        ?>
                                        <figure class="album-img-div text-center <?php if ($count == $showalbum) {echo '';} else {echo 'invalid';} ?>">
                                        <?php
                                        if ($count == $showalbum) 
                                        {
                                            echo '';
                                        } else {
                                            echo '<span class="grayTrans"></span>';
                                        }
                                        ?>
                                            <img src="<?php bloginfo('template_url'); ?>/images/alb_2.jpg" alt="">
                                            <figcaption><?php echo count($results) + $sum; ?></figcaption>
                                        </figure>
                                        <?php $sum++; } 
                                            }else 
                                            {
                                                echo "No Album";
                                            } 
                                        ?>
                                    </div>                                    
                                </div>
                                <!--album part-->
                            </div>

                            <!--fav section -->
                            <div class="col-sm-5 col-md-5 col-xs-12 no-padding">
                                <div class="Favourite-Part">
                                    <div class="clear"> 
                                        <h3 class="Pro-h4 col-sm-12 col-md-12"><span>FAVOURITE QUOTE</span></h3>
                                    </div>
                                    <div class="col-sm-12 col-md-12 no-padding"> 
                                        <p class="fav-para"><?php echo $fav_quote_content; ?></p>
                                    </div>
                                </div>
                            </div><!-- fav section -->

                            <?php
                            $results = $wpdb->get_var("SELECT img_url FROM bg_images");
                            ?>

                            <div class="col-sm-12 col-md-12 col-xs-12 no-padding-right">
                                <div id="content-1">
                                <div class="Introduction-Part" style="background-size: 100% 100%; background-image: url('<?php echo $results; ?>');">
                                    <div class="clear"> 
                                        <h3 class="Pro-h3 col-sm-12 col-md-12"><span>INTRODUCTION</span></h3>
                                    </div>
                                    <div class="padding-para-div"> 
                                        <p class="Intro-para">
                                        <?php 
                                        if($des_status == '' && $describe != '')
                                        {
                                            $message = "Your introduction not approved by admin yet!";
                                        }else if($des_status == 'refused'){
                                            $message = 'Your introduction has refused by admin!';
                                        }else
                                        {
                                           $message = $describe;
                                        }                                        
                                        echo $message; ?> </p>
                                    </div>
                                </div>
                                </div>
                            </div>
                            <!-- introduction -->
                        </div>
                    </div>
                    <!--clear -->
                </div>
                <!--col-sm -7 -->

                <!--send mail code-->
                <div class="col-sm-5 col-md-5 col-xs-12">
                    <div id ="send_mail_viewer" style="display:none" class="profile-mail-part">
                        <div class="mail-btn-back">
                            <a href="javascript:void(0)" id ="back_profile">
                                <img src="<?php bloginfo('template_directory'); ?>/images/arrow-right.png"> &nbsp; &nbsp;Back to profile
                            </a>
                        </div>
                        <div class="col-sm-11 mail-part-main">
                            <p class="mail-text-top">SEND MAIL TO <span> <?php echo ucfirst($get_user_info['nickname']['0']); ?></span></p>
                            <form  enctype="multipart/form-data"> 
                                <div class="form-group">
                                    <input class="form-control" placeholder="We would like to meet you" type ="text" name ="headline" id="headline" value="" maxlength="60">
                                </div>
                                <p class="txtbox-under-text">Headline <span>(less than 60 characters)</span></p>
                                <textarea id='editor1' class="form-control-area"></textarea>
                                <!--<p class="txtbox-under-text">Mail <span>(less than 5,000 characters)</span></p>
                                -->
                                <p><span></span></p>
                                <script>
                                    CKEDITOR.replace('editor1', {
                                        toolbar: [
                                            {name: 'document', items: ['-', 'NewPage', 'Preview', '-', 'Templates']},
                                            {name: 'basicstyles', items: ['Bold', 'Italic', 'Underline']}
                                        ]
                                    });
                                </script>
                                <?php
                                if ($operational_setting[1]->$current_user_type == 1) {
                                ?>
                                    <div class="row">
                                        <div class="col-sm-2">      
                                            <input type="file" name="upload1" size="30" class="attachment" id ="attachment1" onchange="showimagepreview(this)" style="display:none;" >
                                            <label class="file-uploader" for="attachment1">+add</label>
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="txtbox-under-text"><span>Attaché picture 1</span></p>
                                        </div>
                                        <div class="col-sm-4">
                                            <button type="button" class="btn dating-btn-gray" data-toggle="modal" data-target="#myModal" id="preview">Preview</button>
                                        </div>
                                    </div>
                                    <?php } ?>

                                <div class="row attach-2">
                                    <?php if ($operational_setting[1]->$current_user_type == 1) { ?>
                                        <div class="col-sm-2">
                                            <input type="file" name="upload2" size="30" class="attachment" id ="attachment2" onchange="showimagepreview1(this)" style="display:none;">
                                            <label class="file-uploader" for="attachment2">+add</label>
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="txtbox-under-text"><span>Attaché picture 2</span></p>
                                        </div>
                                    <?php } ?>
                                    <div class="col-sm-4">
                                        <input type ="hidden" name="sender_uid" value="<?php echo $current_user_ID; ?>" >
                                        <input type ="hidden" name="recipient_uid" value="<?php echo $user_id; ?>" >
                                        <input type="hidden" name="from" value="<?php echo $current_user->user_email; ?>">
                                        <input type="hidden" name="to" value="<?php echo $user->user_email; ?>">
                                        <button type="button" name="send" class="btn dating-btn-maroon" value="PUBLISH" id="ajax_email">PUBLISH</button>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="col-sm-6"><img id="img_mail1" class="img-responsive" height="150" width="1500"></div>
                                        <div class="col-sm-6"><img id="img_mail2" class="img-responsive" height="150" width="150"></div>
                                    </div>
                            </form>
                            <p class="mail-text-bottom">
                               <strong>Note : </strong>Advertisement and mass mailing is prohibited. If you send
                                server mails with same or always same content this can lead to
                                cancellation of your membership.
                            </p>
                        </div><br/>
                        <div id ="mail_msg"></div>
                        <span class="mail_img" style="display:none;">
                            <img height="12px" width="60px" src="<?php echo get_template_directory_uri(); ?>/images/ajax-status.gif">
                        </span>
                    </div>
                </div> <!--mail code end -->

                <div class="col-sm-12 col-md-12 col-xs-12 no-padding-left" id="left_section">
                    <div class="col-sm-6 col-md-6 col-xs-12 no-padding">
                        <div class="Favourite-Part2">
                            <div class="clear"> 
                                <h3 class="Pro-h3 col-sm-12 col-md-12"><span>MAIN RESIDENCE</span></h3>
                            </div>
                            <div class="padding-top-bottom">
                                <div class="form-group"> 
                                    <label for="inputEmail3" class="col-sm-6 BasicInfo-label">COUNTRY :</label>
                                    <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-left"><?php echo $country; ?></label>
                                </div>
                                <div class="form-group"> 
                                    <label for="inputEmail3" class="col-sm-6 BasicInfo-label">REGIO :</label>
                                    <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-left"><?php echo $district1; ?></label>
                                </div>
                                <div class="form-group"> 
                                    <label for="inputEmail3" class="col-sm-6 BasicInfo-label">NEXT CITY :</label>
                                    <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-left"><?php echo $city1; ?></label>
                                </div>
                                <div class="form-group"> 
                                    <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Frequently in :</label>
                                    <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-left"><?php echo $get_user_info['frequently_in']['0']; ?></label>
                                </div>
                            </div>
                        </div> <!-- main residence -->

                        <div class="Favourite-Part3 <?php if ($current_user_type != 'premium'){echo 'invalid';} ?>">
                            <?php if ($current_user_type != 'premium'){echo '<span class="grayTrans-more"></span>';} ?>
                            <div class=""> 
                                <h3 class="Pro-h3 col-sm-12 col-md-12"><span>MATCHES</span></h3>
                            </div>
                            <div class="padding-top-bottom">
                                <div class="row">
                                    <?php
                                        $get_sender_data = $wpdb->get_results("SELECT sender_id FROM wp_user_profile WHERE reciver_id='".$user_id."' AND add_fav='1'  ORDER BY id DESC LIMIT 3");
                                        $get_reciver_data = $wpdb->get_results("SELECT reciver_id FROM wp_user_profile WHERE sender_id='".$user_id."' AND add_fav='1' ORDER BY id DESC LIMIT 3");
                                        $senderdataid = array();
                                        foreach($get_sender_data as $value)
                                        {
                                            $senderdataid[] =  $value->sender_id;
                                        }
                                        $alldata = array();
                                        foreach($get_reciver_data as $data){
                                            if(in_array($data->reciver_id, $senderdataid)){
                                                $alldata[] = $data->reciver_id ;
                                            }                                                
                                        } 
                                        if (!empty($alldata)) 
                                        {
                                            foreach ($alldata as $mat_value) 
                                            {
                                                $senderID       = $mat_value;
                                                $user_name      = get_user_meta($senderID,'nickname',true);

                                                $userUrl = get_permalink(305) . '&user_id=' . $senderID;
                                                #.. sender user pic data
                                                $his_pick          = get_user_meta($senderID,'his_pick', true);
                                                $her_pick          = get_user_meta($senderID,'her_pick' , true);
                                                if(!empty($her_pick))
                                                {
                                                    $pick_name = $her_pick;
                                                }else{
                                                    $pick_name = $his_pick;
                                                }
                                                $pick_source        = $upload_dir['baseurl'] . '/profilepick_' .$senderID.'/'.$pick_name;
                                    ?>
                                    <div class="col-xs-4">
                                        <img src="<?php echo $pick_source; ?>" height="45" width="52">
                                        <label class="matchName"><?php echo $user_name; ?></label>
                                    </div> 
                                    <?php  }
                                        }else
                                        {
                                            echo 'No Matches';
                                        }
                                     ?>  
                                </div>           
                            </div>
                        </div>
                        <!--second residence -->
           
                        <?php
/******* for ladies ******************/
                            $userid = $_GET["user_id"];
                            $arr = array();
                            $subId = array();
                            $status = array();
                            $myrows = $wpdb->get_results("SELECT *  FROM `wp_erotics_preference` where `userid` = $userid AND `type` = 1 order by like_dislike_attr_id" );
                            foreach ($myrows as $result) {
                                $arr[] = $result->like_dislike_attr_id;
                                $subId[] = $result->subject_id;
                                $status[] = unserialize($result->status);
                            }

                            $arg = array('posts_per_page' => -1, 'offset' => 1, 'category' => 14, 'post_type' => 'eroticoption', 'order' => 'asc');
                            $myposts = get_posts($arg);
                            $myAllposts = array();
                            $ctagoryid = array("3", "4", "5");
                            foreach ($ctagoryid as $values) {
                                $args = array('posts_per_page' => -1, 'offset' => 1, 'category' => $values->ID, 'post_type' => 'erotics', 'order' => 'asc', 'show_count' => 0);
                                $myAllposts = get_posts($args);
                            }
                            $content = array();
                            foreach ($subId as $key => $value) 
                            {                          
                                $content['subject'][] = get_the_title($value); 
                                $content['subjectID'][] =  $value;                   
                            }
                            foreach ($arr as $key => $value1) {
                                foreach ($myposts as $value) {
                                    if ($value1 == $value->ID) {
                                        $content['desc'][] = $value->post_title;
                                        if(!(in_array($value->ID, $content['id'])))
                                        {
                                            $content['id'][] = $value->ID;
                                        } 
                                    }
                                }
                            }
                            $unique_arr_desc=array_unique($content['desc']);
                            foreach ($unique_arr_desc as $value) 
                            {
                               $unique_arr_dist[]=$value;
                            }

/************ for gentleman *****************/
                            $myrows1 = $wpdb->get_results("SELECT *  FROM `wp_erotics_preference` where `userid` = $userid AND `type` = 2 order by like_dislike_attr_id" );
                            $arr1 = array();
                            $subId1 = array();
                            $status1 = array();

                            foreach ($myrows1 as $result) {
                                $arr1[] = $result->like_dislike_attr_id;
                                $subId1[] = $result->subject_id;
                                $status1[] = unserialize($result->status);
                            }
                            $content1 = array();
                            foreach ($subId1 as $key => $value) 
                            {                          
                                $content1['subject'][] = get_the_title($value);
                                $content1['subjectID'][] =  $value;                    
                            }
                            foreach ($arr1 as $key => $value1) {
                                foreach ($myposts as $value) {
                                    if ($value1 == $value->ID) {
                                        $content1['desc'][] = $value->post_title;
                                        if(!(in_array($value->ID, $content1['id'])))
                                        {
                                            $content1['id'][] = $value->ID;
                                        } 
                                    }
                                }
                            }
                            $unique_arr_desc1=array_unique($content1['desc']);
                            foreach ($unique_arr_desc1 as $value) 
                            {
                               $unique_arr_dist1[]=$value;
                            }
                            
                            if(isset($_GET["user_id"])){
                                $userid = $_GET["user_id"];
                                $results = $wpdb->get_results( "SELECT * FROM email_template WHERE sender_uid = '$current_user_ID' AND reciepient_uid = '$userid' AND mail_type= 'hidden_erotics'"  );
                            } 
                        ?>

                        <div class="eropic-profile-Part  <?php  if ($visibilty_setting[2]->$current_user_type !=1 && $current_user_type == 'basic') {echo 'invalid';} ?>">
                            <?php                    
                               if ($visibilty_setting[2]->$current_user_type !=1 && $current_user_type == 'basic') {
                                    echo '<span class="grayTrans"></span>';
                                }
                            ?>
                            <div class="clear"> 
                                <h3 class="eropic-h3 no-margin no-padding col-sm-12 col-md-12">
                                    <span>Her erotic profile </span>
                                    <a href="javascript:void(0)">
                                    <?php if (count($results) > 0){ ?>
                                    <span id="show_hidden" style="<?php echo ($current_user_type == 'validated') ?  'color: rgba(255, 255, 255, 0.3);pointer-events: none;' : ''; ?>" class="profile-her-erotic-right-btn">E</span>
                                    <?php }else{ ?>
                                    <span data-toggle="modal" style="<?php echo ($current_user_type == 'validated') ?  'color: rgba(255, 255, 255, 0.3);pointer-events: none;' : ''; ?>" data-target="#send_request" id="open_box" class="profile-her-erotic-right-btn">E</span>
                                    <?php } ?>                                                        
                                    </a>
                                </h3>
                            </div>              
                            <?php 
                                if ($visibilty_setting[2]->$current_user_type == 1) {
                            ?>
                            <div style="display:block;" id="hide_hidden">
                                <?php
                                    $count = 0;
                                    for ($i = 0; $i < count($unique_arr_dist); $i++) {
                                        $color = $content['id'][$count];
                                        $getTileShow = getActiveOrPassviveTitle($wpdb,$userid,$color, '1');
                                        ?>              
                                        <div class="eropic-box-round " style="<?php echo ($getTileShow[0] != '2') ? 'display:block' : 'display:none'; ?>;background-color:<?php echo ($bgColor != "") ? $bgColor : $textColor[$color]; ?>;color:<?php echo ($bgColor != "") ? $bgColor : $forColor[$color]; ?>;">
                                            <p><strong class="up-case"><?php echo strtoupper(qtrans_use($q_config['language'],$unique_arr_dist[$i], true)); ?> :</strong></p>
                                            <?php for($j = 0; $j < count($content['subject']); $j++){
                                                if($content['desc'][$j]==$unique_arr_dist[$i]){ 
                                                    $like_dislike_ID = $content['subjectID'][$j];
                                                    $text = getActiveOrPassvive($wpdb,$userid,$color,$like_dislike_ID, '1');
                                                ?>
                                            <p style="<?php echo $text['class']; ?>"><?php echo qtrans_use($q_config['language'],$content['subject'][$j], true) .' '.  $text['text']; ?></p>
                                            <?php }
                                            } ?>
                                        </div>
                                        <?php $count++; } ?>
                                </div>
                            <?php  } ?>
                            <!-- hidden erotics --- -->
                            <div style="display:none;" id="hidden_erotics">
                                <?php for ($i = 0; $i < count($hidden_content['subject']); $i++) { ?>             
                                    <div class="eropic-box-round bg-green1">
                                        <p>
                                            <strong class="up-case">
                                                <?php echo $hidden_content['desc'][$i]; ?> :
                                            </strong>
                                            <?php echo $hidden_content['subject'][$i]; ?>
                                        </p>
                                    </div>
                                <?php } ?>
                            </div>
                        </div> <!--erotic part she-->
                    </div> 

                    <div class="col-sm-6 col-md-6 col-xs-12 no-padding-right">
						<?php 
						if ($setting[2]->$user_type_name == '1' && $get_user_info['approved_as_basic']['0'] != 'no') { ?>
                        <div class="cus_scrolln perfect-match-Part">
 
                            <div class="clear"> 
                                <h3 class="opm-h3 col-sm-12 col-md-12"><span>PERFECT MATCH</span></h3>
                            </div>
                            <div style="display:<?php echo ($reg3_txtshe != '') ? "block" : "none"; ?>" class="opm-text-box-roundsec">
                                <p><strong>SHE :</strong><?php echo $reg3_txtshe; ?></p>
                            </div>
                            <div style="display:<?php echo ($reg3_txthe != '') ? "block" : "none"; ?>" class="opm-text-box-roundsec">
                                <p><strong>HE :</strong><?php echo $reg3_txthe; ?></p> 
                            </div>
                            <div style="display:<?php echo ($reg3_txtboth != '') ? "block" : "none"; ?>" class="opm-text-box-roundsec">
                                <p><strong>BOTH :</strong><?php echo $reg3_txtboth; ?></p> 
                            </div>
                        </div> <!-- our prefect match -->
                        <?php }else {?>
						<div class="cus_scrolln perfect-match-Part invalid">
                        <span class="grayTrans-more"></span>
                            <div class="clear"> 
                                <h3 class="opm-h3 col-sm-12 col-md-12"><span>PERFECT MATCH</span></h3>
                            </div>
                            <div style="display:<?php echo ($reg3_txtshe != '') ? "block" : "none"; ?>" class="opm-text-box-roundsec">
                                <p><strong>SHE :</strong><?php echo $reg3_txtshe; ?></p>
                            </div>
                            <div style="display:<?php echo ($reg3_txthe != '') ? "block" : "none"; ?>" class="opm-text-box-roundsec">
                                <p><strong>HE :</strong><?php echo $reg3_txthe; ?></p> 
                            </div>
                            <div style="display:<?php echo ($reg3_txtboth != '') ? "block" : "none"; ?>" class="opm-text-box-roundsec">
                                <p><strong>BOTH :</strong><?php echo $reg3_txtboth; ?></p> 
                            </div>
                        </div> <!-- our prefect match -->
						<?php } ?>	
        
                        <div class="modal fade" id="send_request" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="parent" style="display:none;"><span class="loader"></span></div>
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Request for hidden erotics</h4>
                                        <div id="profile_img" class="successMsg"></div>
                                    </div>
                                    <div class="modal-body">             
                                        <form id="uploadimage_male" action="" method="post" enctype="multipart/form-data">
                                            <textarea id="request_hidden_erotics" rows="8" cols="50" style="color:#000;"></textarea>                  
                                        </form>     
                                    </div> 
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary" id ="request_to_user">Send</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="eropic-profile-Part <?php if ($visibilty_setting[2]->$current_user_type == 0) {echo 'invalid';} ?>">
                            <?php                            
                                if ($visibilty_setting[2]->$current_user_type == 0) 
                                {
                                    echo '<span class="grayTrans"></span>'; 
                                } 
                            ?>
                            <div class="clear"> 
                                <h3 class="eropic-h3 no-margin no-padding col-sm-12 col-md-12">
                                    <span>His erotic profile</span>
                                    <a href="javascript:void(0)">
                                    <?php if (count($results) > 0){ ?>
                                    <span id="show_hidden1" style="<?php echo ($current_user_type == 'validated') ?  'color: rgba(255, 255, 255, 0.3);pointer-events: none;' : ''; ?>" class="profile-her-erotic-right-btn">E</span>
                                    <?php }else{ ?>
                                    <span data-toggle="modal" style="<?php echo ($current_user_type == 'validated') ?  'color: rgba(255, 255, 255, 0.3);pointer-events: none;' : ''; ?>" data-target="#send_request" id="open_box" class="profile-her-erotic-right-btn">E</span>
                                    <?php } ?>
                                    </a>
                                </h3>
                            </div>

                            <?php //if ($current_user_type != 'basic') {?>
                                <div style="display:block;" id="hide_hidden1">
                                         <?php
                                        $count1 = 0;
                                        for ($i = 0; $i < count($unique_arr_dist1); $i++) 
                                        {
                                            $color1 = $content1['id'][$count1];
                                            $textColor[$color1]; 
                                            $getTileShow = getActiveOrPassviveTitle($wpdb,$userid,$color1, '2');                                                                                                                                
                                        ?>              
                                        <div class="eropic-box-round" style="<?php echo ($getTileShow[0] != '2') ? 'display:block' : 'display:none'; ?>;background-color:<?php echo ($bgColor1 != "") ? $bgColor1 : $textColor[$color1]; ?>;color:<?php echo ($bgColor1 != "") ? $bgColor1 : $forColor[$color1]; ?>;">
                                            <p class="child"><strong class="up-case"><?php echo strtoupper(qtrans_use($q_config['language'],$unique_arr_dist1[$i], true)); ?> :</strong></p>
                                            <?php for($j = 0; $j < count($content1['subject']); $j++)
                                             {
                                            if($content1['desc'][$j]==$unique_arr_dist1[$i])
                                            { 
                                                $like_dislike_ID = $content1['subjectID'][$j];
                                                $text1 = getActiveOrPassvive($wpdb,$userid,$color1,$like_dislike_ID, '2');
                                            ?>
                                            <p style="<?php echo $text1['class']; ?>"><?php echo qtrans_use($q_config['language'],$content1['subject'][$j], true) .' '.  $text1['text']; ?></p>
                                            <?php } } ?>
                                        </div>
                                        <?php $count1++; } ?>
                                    </div>
                            <?php //} ?>
                            <div style="display:none;" id="hidden_erotics1">
                                <?php 
                                        $count1 = 0;
                                        for ($i = 0; $i < count($unique_arr_dist1); $i++){
                                            $color1 = $content1['id'][$count1];
                                            $textColor[$color1]; 
                                            $getTileShow = getHideTitle($wpdb,$userid,$color1, '2');
                                            print_r($getTileShow);
                                            for($k=1;$k<count($getTileShow);$k++)
                                            {
                                                echo $getTileShow[$k][0];
                                            }
                                        ?>              
                                        <div class="eropic-box-round" style="<?php echo $style; ?>;background-color:<?php echo ($bgColor1 != "") ? $bgColor1 : $textColor[$color1]; ?>;color:<?php echo ($bgColor1 != "") ? $bgColor1 : $forColor[$color1]; ?>;">
                                            <p class="child"><strong class="up-case"><?php echo strtoupper(qtrans_use($q_config['language'],$unique_arr_dist1[$i], true)); ?> :</strong></p>
                                            <?php for($j = 0; $j < count($content1['subject']); $j++){
                                            if($content1['desc'][$j]==$unique_arr_dist1[$i])
                                            { 
                                                $like_dislike_ID = $content1['subjectID'][$j];
                                                $text1 = getActiveOrPassvive($wpdb, $userid, $color1, $like_dislike_ID, '2');
                                            ?>
                                            <p style="<?php echo $text1['hidetext']; ?>"><?php echo qtrans_use($q_config['language'],$content1['subject'][$j], true); ?></p>
                                            <?php  }  } ?>
                                        </div>
                                        <?php $count1++; } ?> 
                            </div>
                        </div> <!--erotic he -->
                    </div>
                </div>
            </div>
            <input type="hidden" name="logged_in_user" value="<?php echo $current_user_ID; ?>" id ="logged_in_user">
        </form>
    </div>
</div>

<?php
        break; //couple break

        default: // for male and female
?>
<div class="userProfile">
    <div class="container-single-profile">
        <form class="form-horizontal" role="form" name="form" method="post">
            <div class="row">
                <div class="col-sm-3">                                                       
                    <div class="col-sm-10 no-padding profilePicWrap" >                           
                        <h3 class="search_premium_heading <?php if($member_user_type=='ladies'){echo 'perple_heading';}else{echo 'blue_heading';} ?>">
                            <?php echo ucfirst($get_user_info['nickname']['0']); ?>
                        </h3>                         
                        <?php if ($member_user_type == 'gentleman') { ?>
                            <?php if ($get_user_info['his_pick']['0'] == 'no_img.jpg') { ?> 
                                <img class="prof-cartoon-img" src="<?php echo bloginfo('template_url'); ?>/images/images.jpeg">                                   
                            <?php } else { ?>
                                <img class="prof-cartoon-img" src="<?php echo $image_url_male; ?>">
                            <?php } ?>
                        <?php } else { ?>
                            <?php if ($get_user_info['her_pick']['0'] != 'no_img.jpg') { ?>
                                <img class="prof-cartoon-img" src="<?php echo $image_url_female; ?>">
                            <?php } else { ?>
                                <img class="prof-cartoon-img" src="<?php echo bloginfo('template_url'); ?>/images/images.jpeg">
                            <?php } ?>
                        <?php } ?>
                    </div>  
                    <!-- picture wrap-->
                    <div class="clear"></div>
                    <div class="list_div">
                        <ul class="side-bar-profile no-padding">
                               <li class="<?php echo ($operational_setting[3]->$current_user_type != 1) ? 'invalid' : ''; ?>">
                               <?php  echo ($operational_setting[3]->$current_user_type != 1) ? "<span class='grayTrans'></span>" : ""; ?>                                    
                                    <a href ="javascript:void(0);" id ="add_fav">
                                        <i><img src="<?php echo bloginfo("template_url"); ?>/images/<?php echo $image; ?>"></i>
                                        <span <?php echo $style; ?>><?php echo $text; ?></span>
                                    </a>
                                    <span class="successMsg" id="add_fav_msg" style="display: none;"></span>
                                    <span style="display:none; text-align: center" class="bar_img1">
                                            <img width="60px" height="12px" src="<?php echo site_url(); ?>/wp-content/themes/twentyfourteen/images/ajax-status.gif">
                                        </span>
                                </li>
                                <li class="<?php echo ($operational_setting[5]->$current_user_type != 1) ? 'invalid' : ''; ?>">
                                <?php echo ($operational_setting[5]->$current_user_type != 1) ? "<span class='grayTrans'></span>" : ""; ?>
                                         <span class="successMsg" id="kiss_msg" style="display: none;"></span>
                                    <a href ="javascript:void(0);" id="send_a_kiss">
                                        <i><img src="<?php echo bloginfo("template_url"); ?>/images/lip.jpg"></i> 
                                        <span>send a kiss</span>
                                    </a>
                                    <span class="successMsg" id="kiss_msg" style="display: none;"></span>
                                    <span style="display:none;text-align: center" class="bar_img2">
                                        <img width="60px" height="12px" src="<?php echo site_url(); ?>/wp-content/themes/twentyfourteen/images/ajax-status.gif">
                                    </span>
                                </li>  
                                <li>
                                    <a href ="javascript:void(0);" id ="send_mail" >
                                        <i><img src="<?php echo bloginfo("template_url"); ?>/images/mes1.jpg"></i>
                                        <span>send mail</span>
                                    </a>                                    
                                </li>
                                <li>
                                    <a href="<?php echo $url; ?>" target="_blank">
                                        <i><img src="<?php echo bloginfo("template_url"); ?>/images/mes3.jpg"></i>
                                        <span>mail history</span>
                                    </a>
                                </li>
                                <li class='<?php echo ($current_user_type == 'premium') ? "" : "invalid"; ?>'>  
                                <?php echo ($current_user_type == 'premium') ? "" : "<span style='height:50px;' class='grayTrans'></span>"; ?>                                  
                                    <a href="javascript:void(0)" id ="hide_show">
                                        <i><img src="<?php echo bloginfo("template_url"); ?>/images/lockband.jpg"></i> 
                                        <span>hide/show profile open/close albums erotic preferences</span>
                                    </a>
                                </li>
                                <li class="<?php echo ($operational_setting[6]->$current_user_type != 1) ? 'invalid' : ''; ?>">
                                <?php  echo ($operational_setting[6]->$current_user_type != 1) ? "<span class='grayTrans'></span>" : ""; ?>
                                    <a href ="javascript:void(0)" data-toggle="modal" data-target="#make_note" id="makenote">
                                        <i><img src="<?php echo bloginfo("template_url"); ?>/images/note.jpg"></i>
                                        <span>make a note for this profile</span>
                                    </a>
                                </li>
							<?php if ($user_type_name == 'basic' && $operational_setting[3]->$user_type_name != 1) {?>     
                                <li style="position:relative;"> 
									<span class="grayTrans"></span>	
							
                                            <i><img src="<?php echo bloginfo("template_url"); ?>/images/block.jpg"></i>
                                            <span>block this profile</span>
                             
                                </li>
                     
                                <li style="position:relative;"> 
									<span class="grayTrans"></span>	
							
                                    <i><img src="<?php echo bloginfo("template_url"); ?>/images/submit.jpg"></i>
                                    <span>submit complaint<br/> about this profile</span>
                                </li>
                                <?php } else { ?>
                                  <li>      <a href="javascript:void(0)" id ="block_profile">
                                            <i><img src="<?php echo bloginfo("template_url"); ?>/images/block.jpg"></i>
                                            <span>block this profile</span>
                                        </a>
                                </li>
                                <li> 
                                        <a href = "javascript:void(0)" data-toggle="modal" data-target="#report_abuse">
                                            <i><img src="<?php echo bloginfo("template_url"); ?>/images/submit.jpg"></i>
                                            <span>submit complaint about this profile</span>
                                        </a>
                                </li>
                                <?php } ?>                             
                        </ul>
                         <span class="bar_img" style="display:none;">
                            <img height="12px" width="60px" src="<?php echo get_template_directory_uri(); ?>/images/ajax-status.gif">
                        </span> <!--loader bar -->
                        <span id="block_user_profile" class="successMsg"></span>
                    </div> 
                    <!--list div -->

                    <div class="clear"></div><hr/><span class="border-bottom"></span>

                                <div class="text-box-round <?php if ($current_user_type != 'premium'){echo 'invalid';} ?>">
                                <?php if ($current_user_type != 'premium'){echo '<span class="grayTrans"></span>';} ?>
                                <?php $dating_ads=$wpdb->get_results("SELECT * FROM wp_dating_ads WHERE user_id=".$user_id." ORDER BY ID DESC LIMIT 0,2");
                                    $upload_dir = wp_upload_dir();
                                    $img_srcpath = $upload_dir['baseurl'] . '/dating_ads/'; 
                                ?>
                                <h5><?php if($member_user_type=='couples'){ echo "OUR SPEED DATING";} else{  echo "MY SPEED DATING"; }?></h5>
                                <?php foreach ($dating_ads as $value) 
                                    { 
                                    $from=strtotime($value->from);
                                    $to=strtotime($value->to); 
                                    $from_m=date('m', $from);
                                    $monthName_from = date('F', mktime(0, 0, 0, $from_m, 10));
                                    $to_m=date('m', $to);
                                    $monthName_to = date('F', mktime(0, 0, 0, $to_m, 10));
                                    $from_y=date('Y', $from);
                                    $to_y=date('Y', $to);
                                    $from_d=date('d', $from);
                                    $to_d=date('d',$to);
                                    $country=$value->country;
                                    $city=$value->region;
                                ?>
                                <div class="form-group"> 
                                   <div class="adds_content no-padding-left col-sm-7">
                                   <p>
                                   <?php echo $country."-"; ?>
                                   <span class="city_white"><?php echo $city; ?></span><br/>
                                   <?php echo $from_d."-".$to_d; ?>
                                   <?php if($from_m==$to_m)
                                   {
                                    echo $monthName_from;
                                   }
                                   else
                                   {
                                    echo $monthName_from."-".$monthName_to;
                                   }
                                   ?>
                                   <?php echo $to_y; ?>
                                   </p>
                                   <a href="<?php echo home_url(); ?>/?page_id=735&user_id=<?php echo $user_id; ?>" class="goAdd pull-right">go to ads</a>
                                   </div>
                                   <div class="col-sm-5">
                                      <img src="<?php echo $img_srcpath.$value->ads_pick; ?>" alt="adds image" class="img-responsive">
                                   </div>
                                 </div>
                               <?php }
                                 ?>
                            </div>
                            <div class="text-box-round">
                                <?php
                                $travels_ads=$wpdb->get_results("SELECT * FROM wp_travel_dating WHERE tarvel_logged_user=".$user_id." ORDER BY ID DESC"); 
                                ?>
                                <h5><?php if($member_user_type=='couples'){ echo "OUR TRAVEL AGENDA";} else{  echo "MY TRAVEL AGENDA"; }?></h5>
                                <?php 
                                    foreach ($travels_ads as $value) 
                                    { 
                                    $from=strtotime($value->travel_from);
                                    $to=strtotime($value->travel_to); 
                                    $from_m=date('m', $from);
                                    $monthName_from = date('F', mktime(0, 0, 0, $from_m, 10));
                                    $to_m=date('m', $to);
                                    $monthName_to = date('F', mktime(0, 0, 0, $to_m, 10));
                                    $from_y=date('Y', $from);
                                    $to_y=date('Y', $to);
                                    $from_d=date('d', $from);
                                    $to_d=date('d',$to);
                                    $country=$value->travel_country;
                                    $city=$value->travel_region;
                                ?>
                                <div class="form-group">
                                    <?php echo ($operational_setting[10]->$current_user_type == 0) ? "<span class='grayTrans'></span>" : ''; ?> 
                                       <div class="adds_content no-padding-left col-sm-12">
                                       <p>
                                       <?php echo $country."-"; ?>
                                       <span class="city_white"><?php echo $city; ?></span><br/>
                                       <?php echo $from_d."-".$to_d; ?>
                                       <?php if($from_m==$to_m)
                                       {
                                        echo $monthName_from;
                                       }
                                       else
                                       {
                                        echo $monthName_from."-".$monthName_to;
                                       }
                                       ?>
                                       <?php echo $to_y; ?>
                                       </p>
                                       </div>
                                       </div>
                                      <?php } ?>
                            </div>
                </div> 
                <!--col-sm-3 -->

                <div class="col-sm-6 no-padding">
                    <?php if ($user_type_name == 'premium') { ?>
                        <h5 class="red profileType">
                            <?php if ($profile_name) {
                                echo $profile_name;
                            } else {
                                echo "Premium Member";
                            } ?>
                        </h5>
                    <?php } elseif ($user_type_name == 'validated') { ?>   
                        <h5 class="greenText profileType">
                            <?php if ($profile_name) {
                                echo $profile_name;
                            } else {
                                echo "Validated Member";
                            } ?>
                            <img width="12" src="<?php echo bloginfo('template_url'); ?>/images/tick-green.png" alt="">
                        </h5>

                    <?php } elseif ($user_type_name == 'basic' && $appproved == 'yes') { ?>
                        <h5 class="greenText profileType" style="color:#989898;">
                            <?php if ($profile_name) {
                                echo $profile_name;
                            } else {
                                echo "Basic Member";
                            } ?>
                        </h5>
                    <?php } elseif ($appproved == 'no') { ?>
                            <h5 class="greenText profileType" style="color:#989898;">
                                <?php echo "Pre-membership stage"; ?>
                            </h5>
                    <?php } ?>

                    <div class="row">
                        <div class="col-sm-6 no-padding-right">
                            <div class="Profile-Part-BI cus_scroll">                              
                                <h3 class="Proh-h3"><span>BASIC INFO</span></h3>
                                <?php if ($member_user_type == 'ladies') { ?>
                                <div class="padding-basic-info mt30">
                                    <div class="form-group border-bottom">
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Age</label>
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-center bold">
                                            <?php                                                       
                                                echo $female_age;                                                                           
                                            ?>
                                        </label>
                                    </div>
                                    <div class="form-group border-bottom">
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Height (cm)</label>
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-center bold"><?php echo $her_hight; ?></label>
                                    </div>
                                    <div class="form-group border-bottom">
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Figure</label>
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-center"><?php echo $her_figure; ?></label>
                                    </div>
                                    <div class="form-group border-bottom">
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Relationship</label>
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-center"><?php echo $get_user_info['relation_ship']['0']; ?></label>
                                    </div>
                                    <div class="form-group border-bottom">
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Smoking</label>
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-center"><?php echo $her_smoke; ?></label>
                                    </div>
                                    <div class="form-group border-bottom">
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Hair color</label>
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-center"><?php echo $get_user_info['her_haircolor']['0']; ?></label>
                                    </div>                                        
                                    <div class="form-group border-bottom">
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Languages</label>
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label">
                                            <?php                                                        
                                                echo str_replace(',', ', ', rtrim($selectedLanguages, ","));                                                        
                                            ?>
                                        </label>
                                    </div>
                                    <div class="form-group border-bottom">
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Education</label>
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-center"><?php echo $get_user_info['her_graduation']['0']; ?></label>
                                    </div>                                        
                                    <div class="form-group border-bottom">
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Hobbies Interests</label>
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label"><?php echo $her_hobby; ?></label>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Additional info</label>
                                        </label>
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label"><?php echo $get_user_info['about_her']['0']; ?></label>
                                    </div>
                                </div>  
                                <?php } else { ?>
                                <div class="padding-basic-info mt30">
                                    <div class="form-group border-bottom">
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Age</label>
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-center bold">
                                            <?php                                                    
                                                echo $male_age;
                                            ?>
                                        </label>
                                    </div>
                                    <div class="form-group border-bottom">
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Height (cm)</label>
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-center bold"><?php echo $his_hight; ?></label>
                                    </div>
                                    <div class="form-group border-bottom">
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Figure</label>
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-center"><?php echo $his_figure; ?></label>
                                    </div>
                                    <div class="form-group border-bottom">
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Relationship</label>
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-center"><?php echo $get_user_info['relation_ship']['0']; ?></label>
                                    </div>
                                    <div class="form-group border-bottom">
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Smoking</label>
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-center"><?php echo $his_smoke; ?></label>
                                    </div>
                                    <div class="form-group border-bottom">
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Hair color</label>
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-center"><?php echo $get_user_info['his_haircolor']['0']; ?></label>
                                    </div>                                        
                                    <div class="form-group border-bottom">
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Languages</label>
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label">
                                            <?php                                                        
                                                echo str_replace(',', ', ', rtrim($selectedLanguages1, ","));                                                        
                                            ?>
                                        </label>
                                    </div>
                                    <div class="form-group border-bottom">
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Education</label>
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label"><?php echo $get_user_info['his_graduation']['0']; ?></label>
                                    </div>                                        
                                    <div class="form-group border-bottom">
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Hobbies Interests</label>
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label"><?php echo $his_hobby; ?></label>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Additional info</label>
                                        </label>
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label"><?php echo $get_user_info['about_his']['0']; ?></label>
                                    </div>
                                </div>                                                
                                <?php }?>
                            </div> 
                            <!-- profile part -->

                            <div class="Album-Part">
                                <div class=""> 
                                    <h3 class="Pro-h3"><span>ALBUMS</span></h3>
                                </div>
                                <div class="WidthFull">
                                    <?php
                                        $results    = $wpdb->get_results("SELECT * FROM user_create_album WHERE user_id = '" . $user_id . "'");
                                        $upload_dir = wp_upload_dir();
                                        $imgPath    = $upload_dir['baseurl'] . '/users_album/album_' . $user_id . '/';
                                        $i = 1;
                                        $count = 1;
                                        foreach ($results as $value) 
                                        {
                                            $select_img = $wpdb->get_results("SELECT * FROM user_album_images WHERE album_id = '$value->id' order by img_order ASC limit 1");
                                            if (count($select_img) == 0) 
                                            {
                                    ?>
                                            <figure class="album-img-div text-center <?php if ($count <= $showalbum) {echo '';} else {echo 'invalid';} ?>">
                                                <?php
                                                    if ($count <= $showalbum) {
                                                        echo '';
                                                    } else {
                                                        echo '<span class="grayTrans"></span>';
                                                    }
                                                ?>
                                                <img src="<?php bloginfo('template_url'); ?>/images/NoAlbum.jpeg" alt="">
                                                <figcaption><?php echo $count++; ?></figcaption>
                                            </figure>
                                    <?php
                                            } else 
                                            {
                                                foreach ($select_img as $select) 
                                                {
                                                    $number = '"' . $value->id . '"';
                                    ?>                                          
                                                    <figure class="album-img-div text-center">
                                                        <a href="<?php echo get_permalink(761); ?>&album=<?php echo $value->id; ?>&user_id=<?php echo $_GET['user_id'];?>" name='imgid' target='_blank' value="<?php echo $value->id; ?>" >
                                                            <img src="<?php echo $imgPath . $select->img_url; ?>">
                                                        </a>
                                                        <figcaption><?php echo $count++; ?></figcaption>
                                                    </figure>                                      
                                    <?php
                                                }
                                            }
                                        }// end foreach
                                        if (count($results) != 0) 
                                        {
                                            $sum = 1;
                                            for ($i = 0; $i < (5 - count($results)); $i++) 
                                            {
                                    ?>
                                            <figure class="album-img-div text-center <?php if ($count == $showalbum) {echo '';} else {echo 'invalid';} ?>">
                                    <?php
                                            if ($count == $showalbum) 
                                            {
                                                echo '';
                                            } else {
                                                echo '<span class="grayTrans"></span>';
                                            }
                                    ?>
                                                <img src="<?php bloginfo('template_url'); ?>/images/alb_2.jpg" alt="">
                                                <figcaption><?php echo count($results) + $sum; ?></figcaption>
                                            </figure>
                                    <?php
                                            $sum++;
                                            } 
                                        }else 
                                        {
                                            echo "No Album";
                                        } 
                                    ?>
                                </div>
                                <div class="clear"></div>        
                            </div> 
                            <!--album part -->
                        </div>
                        <!--col-sm-6 -->

                        <div class="col-sm-6">
                            <div class="Favourite-Part2">                                    
                                <h3 class="Pro-h3 col-sm-12"><span>MAIN RESIDENCE</span></h3>   
                                <div class="padding-top-bottom">
                                    <div class="form-group"> 
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label">COUNTRY :</label>
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-left"><?php echo $country; ?></label>
                                    </div>
                                    <div class="form-group"> 
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label">REGIO :</label>
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-left"><?php echo $district1; ?></label>
                                    </div>
                                    <div class="form-group"> 
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label">NEXT CITY :</label>
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-left"><?php echo $city1; ?></label>
                                    </div>
                                    <div class="form-group"> 
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Frequently in :</label>
                                        <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-left"><?php echo $get_user_info['frequently_in']['0']; ?></label>
                                    </div>
                                </div>
                            </div>
                            <!--main residence -->

                            <div class="Favourite-Part3 <?php if ($current_user_type != 'premium') { echo 'invalid';} ?>">
                                <?php if ($current_user_type != 'premium') { echo '<span class="grayTrans-more"></span>';} ?>
                                <div class=""> 
                                    <h3 class="Pro-h3 col-sm-12 col-md-12"><span>MATCHES</span></h3>
                                </div>
                                <div class="padding-top-bottom">
                                    <div class="row">
                                        <?php
                                            $get_sender_data = $wpdb->get_results("SELECT sender_id FROM wp_user_profile WHERE reciver_id='".$user_id."' AND add_fav='1'  ORDER BY id DESC LIMIT 3");
                                            $get_reciver_data = $wpdb->get_results("SELECT reciver_id FROM wp_user_profile WHERE sender_id='".$user_id."' AND add_fav='1' ORDER BY id DESC LIMIT 3");
                                            $senderdataid = array();
                                            foreach($get_sender_data as $value)
                                            {
                                                $senderdataid[] =  $value->sender_id;
                                            }
                                            $alldata = array();
                                            foreach($get_reciver_data as $data){
                                                if(in_array($data->reciver_id, $senderdataid)){
                                                    $alldata[] = $data->reciver_id ;
                                                }                                                
                                            }                                            
                                            
                                            if (!empty($alldata)) 
                                            {
                                                foreach ($alldata as $mat_value) 
                                                {
                                                    $senderID       = $mat_value;
                                                    $user_name      = get_user_meta($senderID,'nickname',true);

                                                    $userUrl = get_permalink(305) . '&user_id=' . $senderID;
                                                    #.. sender user pic data
                                                    $his_pick          = get_user_meta($senderID,'his_pick', true);
                                                    $her_pick          = get_user_meta($senderID,'her_pick' , true);
                                                    if(!empty($her_pick))
                                                    {
                                                        $pick_name = $her_pick;
                                                    }else{
                                                        $pick_name = $his_pick;
                                                    }
                                                    $pick_source        = $upload_dir['baseurl'] . '/profilepick_' .$senderID.'/'.$pick_name;
                                        ?>
                                        <div class="col-xs-4">
                                            <img src="<?php echo $pick_source; ?>" height="45" width="52">
                                            <label class="matchName"><?php echo $user_name; ?></label>
                                        </div> 
                                        <?php  }
                                            }else
                                            {
                                                echo 'No Matches';
                                            }
                                         ?>  
                                    </div>
                                </div>
                            </div>
                            <!-- end second residence -->

                            <div class="Favourite-Part">
                                <div class=""> 
                                    <h3 class="Pro-h4"><span>FAVOURITE QUOTE</span></h3>
                                </div>
                                <div class="col-sm-12 col-md-12 no-padding"> 
                                    <?php echo $fav_quote_content; ?>
                                </div>
                            </div>
                            <!--fav -section-->
                        </div>
                        <!--col-sm-6 -->
                    </div>
                    <!--row-->

                    <?php
                        $results = $wpdb->get_var("SELECT img_url FROM bg_images");
                    ?>
                    <div class="row">
                        <div class="col-sm-12 prelative">
                            <h3 class="Pro-h3 newIntro"><span>INTRODUCTION</span></h3>   
                            <div  id="content-1"> 
                                <div class="Introduction-Part" style="background-size: 100% 100%; background-image: url('<?php echo $results; ?>');" id ="introduction_part">
                                    <div class="padding-para-div" id="intro_data"> 
                                        <?php 
                                        if($des_status == '' && $describe != '')
                                        {
                                            $message = "Your introduction not approved by admin yet!";
                                        }else if($des_status == 'refused'){
                                            $message = 'Your introduction has refused by admin!';
                                        }else
                                        {
                                           $message = $describe;
                                        }                                        
                                        echo $message; ?> </p>
                                    </div>                                        
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- row -->
                </div> 
                <!--col-sm-6 -->

                <div class="col-sm-3" id="left_section"> 
					   <?php if ($setting[2]->$user_type_name == '1' && $get_user_info['approved_as_basic']['0'] != 'no') { ?>                                  
                    <div class="perfect-match-Part cus_scrolln">
    
                        <div class=""> 
                            <h3 class="opm-h3 col-sm-12 col-md-12"><span>PERFECT MATCH</span> </h3>
                        </div>
                        <?php if($reg3_txtshe!='') { ?>
                        <div style="display:<?php echo ($reg3_txtshe != '') ? "block" : "none"; ?>" class="opm-text-box-roundsec">
                            <p><strong>SHE :</strong><?php echo $reg3_txtshe; ?></p>
                        </div><?php } ?>
                         <?php if($reg3_txthe!='') { ?>
                        <div style="display:<?php echo ($reg3_txthe != '') ? "block" : "none"; ?>" class="opm-text-box-roundsec">
                            <p><strong>HE :</strong><?php echo $reg3_txthe; ?></p> 
                        </div><?php } ?>
                        <?php if($reg3_txtboth!='') { ?>
                        <div style="display:<?php echo ($reg3_txtboth != '') ? "block" : "none"; ?>" class="opm-text-box-roundsec">
                            <p><strong>BOTH :</strong><?php echo $reg3_txtboth; ?></p> 
                        </div><?php } ?>
                    </div>
                    <?php }else{?>
					   <div class="perfect-match-Part cus_scrolln invalid">
                     <span class="grayTrans-more"></span>
                        <div class=""> 
                            <h3 class="opm-h3 col-sm-12 col-md-12"><span>PERFECT MATCH</span> </h3>
                        </div>
                         <?php if($reg3_txtshe!='') { ?>
                        <div style="display:<?php echo ($reg3_txtshe != '') ? "block" : "none"; ?>" class="opm-text-box-roundsec">
                            <p><strong>SHE :</strong><?php echo $reg3_txtshe; ?></p>
                        </div><?php } ?>
                         <?php if($reg3_txthe!='') { ?>
                        <div style="display:<?php echo ($reg3_txthe != '') ? "block" : "none"; ?>" class="opm-text-box-roundsec">
                            <p><strong>HE :</strong><?php echo $reg3_txthe; ?></p> 
                        </div><?php } ?>
                          <?php if($reg3_txtboth!='') { ?>
                        <div style="display:<?php echo ($reg3_txtboth != '') ? "block" : "none"; ?>" class="opm-text-box-roundsec">
                            <p><strong>BOTH :</strong><?php echo $reg3_txtboth; ?></p> 
                        </div><?php } ?>
                    </div>	
					<?php } ?>	
                    <!--perfect match-->

                    <div class="modal fade" id="send_request" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="parent" style="display:none;"><span class="loader"></span></div>
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Request for hidden erotics</h4>
                                    <div id="profile_img" class="successMsg"></div>
                                </div>
                                <div class="modal-body">             
                                    <form id="uploadimage_male" action="" method="post" enctype="multipart/form-data">
                                        <textarea id="request_hidden_erotics" rows="8" cols="50" style="color:#000;"></textarea>                  
                                    </form>     
                                </div> 
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary" id ="request_to_user">Send</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--hidden request-->
                    <?php
/******* for ladies ******************/
                            $userid = $_GET["user_id"];
                            $arr = array();
                            $subId = array();
                            $status = array();
                            $myrows = $wpdb->get_results("SELECT *  FROM `wp_erotics_preference` where `userid` = $userid AND `type` = 1 order by like_dislike_attr_id" );
                            foreach ($myrows as $result) {
                                $arr[] = $result->like_dislike_attr_id;
                                $subId[] = $result->subject_id;
                                $status[] = unserialize($result->status);
                            }

                            $arg = array('posts_per_page' => -1, 'offset' => 1, 'category' => 14, 'post_type' => 'eroticoption', 'order' => 'asc');
                            $myposts = get_posts($arg);
                            $myAllposts = array();
                            $ctagoryid = array("3", "4", "5");
                            foreach ($ctagoryid as $values) {
                                $args = array('posts_per_page' => -1, 'offset' => 1, 'category' => $values->ID, 'post_type' => 'erotics', 'order' => 'asc', 'show_count' => 0);
                                $myAllposts = get_posts($args);
                            }
                            $content = array();
                            foreach ($subId as $key => $value) 
                            {                          
                                $content['subject'][] = get_the_title($value); 
                                $content['subjectID'][] =  $value;                   
                            }
                            foreach ($arr as $key => $value1) {
                                foreach ($myposts as $value) {
                                    if ($value1 == $value->ID) {
                                        $content['desc'][] = $value->post_title;
                                        if(!(in_array($value->ID, $content['id'])))
                                        {
                                            $content['id'][] = $value->ID;
                                        } 
                                    }
                                }
                            }
                            $unique_arr_desc=array_unique($content['desc']);
                            foreach ($unique_arr_desc as $value) 
                            {
                               $unique_arr_dist[]=$value;
                            }

/************ for gentleman *****************/
                            $myrows1 = $wpdb->get_results("SELECT *  FROM `wp_erotics_preference` where `userid` = $userid AND `type` = 2 order by like_dislike_attr_id" );
                            $arr1 = array();
                            $subId1 = array();
                            $status1 = array();

                            foreach ($myrows1 as $result) {
                                $arr1[] = $result->like_dislike_attr_id;
                                $subId1[] = $result->subject_id;
                                $status1[] = unserialize($result->status);
                            }
                            $content1 = array();
                            foreach ($subId1 as $key => $value) 
                            {                          
                                $content1['subject'][] = get_the_title($value);
                                $content1['subjectID'][] =  $value;                    
                            }
                            foreach ($arr1 as $key => $value1) {
                                foreach ($myposts as $value) {
                                    if ($value1 == $value->ID) {
                                        $content1['desc'][] = $value->post_title;
                                        if(!(in_array($value->ID, $content1['id'])))
                                        {
                                            $content1['id'][] = $value->ID;
                                        } 
                                    }
                                }
                            }
                            $unique_arr_desc1=array_unique($content1['desc']);
                            foreach ($unique_arr_desc1 as $value) 
                            {
                               $unique_arr_dist1[]=$value;
                            }
                            
                            if(isset($_GET["user_id"])){
                                $userid = $_GET["user_id"];
                                $results = $wpdb->get_results( "SELECT * FROM email_template WHERE sender_uid = '$current_user_ID' AND reciepient_uid = '$userid' AND mail_type= 'hidden_erotics'"  );
                            } 
                        if ($member_user_type == 'gentleman') 
                        {
                    ?>      
                        <div class="eropic-profile-Part <?php if ($current_user_type == 'basic') { echo 'invalid';} ?>">
                            <?php
                                if(isset($_GET["user_id"])){
                                    $userid = $_GET["user_id"];
                                    $results = $wpdb->get_results( "SELECT * FROM email_template WHERE sender_uid = '$current_user_ID' AND reciepient_uid = '$userid' AND mail_type= 'hidden_erotics'"  );
                                } 
                                if ($visibilty_setting[2]->$current_user_type == 0) {
                                    echo '<span class="grayTrans"></span>';
                                }
                            ?>
                            <h3 class="eropic-h3 no-margin no-padding col-sm-12 col-md-12">
                                <span class="red">His erotic profile</span>
                                <a href="javascript:void(0)">
                                    <?php if (count($results) > 0){ ?>
                                        <span id="show_hidden1" style="<?php echo ($current_user_type == 'validated') ?  'color: rgba(255, 255, 255, 0.3);pointer-events: none;' : ''; ?>" class="profile-her-erotic-right-btn">E</span>
                                    <?php }else{ ?>
                                        <span data-toggle="modal" style="<?php echo ($current_user_type == 'validated') ?  'color: rgba(255, 255, 255, 0.3);pointer-events: none;' : ''; ?>" data-target="#send_request" id="open_box" class="profile-her-erotic-right-btn">E</span>
                                    <?php } ?>
                                </a>
                            </h3>                                           

                             <div style="display:block;" id="hide_hidden1">
                                    <?php
                                        $count1 = 0;
                                        for ($i = 0; $i < count($unique_arr_dist1); $i++) {
                                            $color1 = $content1['id'][$count1];
                                            $textColor[$color1]; 
                                            $getTileShow = getActiveOrPassviveTitle($wpdb,$userid,$color1, '2'); 
                                        ?>              
                                        <div class="eropic-box-round" style="<?php echo ($getTileShow[0] != '2') ? 'display:block' : 'display:none'; ?>;background-color:<?php echo ($bgColor1 != "") ? $bgColor1 : $textColor[$color1]; ?>;color:<?php echo ($bgColor1 != "") ? $bgColor1 : $forColor[$color1]; ?>;">
                                            <p class="child"><strong class="up-case"><?php echo strtoupper(qtrans_use($q_config['language'],$unique_arr_dist1[$i], true)); ?> :</strong></p>
                                            <?php for($j = 0; $j < count($content1['subject']); $j++)
                                             {
                                            if($content1['desc'][$j]==$unique_arr_dist1[$i])
                                            { 
                                                $like_dislike_ID = $content1['subjectID'][$j];
                                                $text1 = getActiveOrPassvive($wpdb,$userid,$color1,$like_dislike_ID, '2');
                                            ?>
                                            <p style="<?php echo $text1['class']; ?>"><?php echo qtrans_use($q_config['language'],$content1['subject'][$j], true) .' '.  $text1['text']; ?></p>
                                            <?php }
                                        } ?>
                                        </div>
                                        <?php $count1++; } ?>
                                    </div>                            

                            <div style="display:none;" id="hidden_erotics1">
                                    <?php
                                        $count1 = 0;
                                        for ($i = 0; $i < count($unique_arr_dist1); $i++) 
                                        {
                                            $color1 = $content1['id'][$count1];
                                            $textColor[$color1]; 
                                            $getTileShow = getActiveOrPassviveTitle($wpdb,$userid,$color1, '2'); 
                                           // print_r($getTileShow);
                                        ?>              
                                        <div class="eropic-box-round" style="<?php echo (in_array('2', $getTileShow)) ? 'display:block' : 'display:none'; ?>;background-color:<?php echo ($bgColor1 != "") ? $bgColor1 : $textColor[$color1]; ?>;color:<?php echo ($bgColor1 != "") ? $bgColor1 : $forColor[$color1]; ?>;">
                                            <p class="child"><strong class="up-case"><?php echo strtoupper(qtrans_use($q_config['language'],$unique_arr_dist1[$i], true)); ?> :</strong></p>
                                            <?php for($j = 0; $j < count($content1['subject']); $j++)
                                             {
                                            if($content1['desc'][$j]==$unique_arr_dist1[$i])
                                            { 
                                                $like_dislike_ID = $content1['subjectID'][$j];
                                                $text1 = getActiveOrPassvive($wpdb,$userid,$color1,$like_dislike_ID, '2');
                                            ?>
                                            <p style="<?php echo $text1['hidetext']; ?>"><?php echo qtrans_use($q_config['language'],$content1['subject'][$j], true); ?></p>
                                            <?php } } ?>
                                        </div>
                                        <?php $count1++; } ?>  
                            </div>
                        </div>
                    <?php } ?>
                    <!--===end gemtleman ====-->

                    <!--===start ladies ====-->
                    <?php 
                        if ($member_user_type == 'ladies') 
                        {
                    ?>
                        <div class="eropic-profile-Part  <?php if ($current_user_type == 'basic') { echo 'invalid';} ?>">
                            <?php
                                if(isset($_GET["user_id"])){
                                    $userid = $_GET["user_id"];
                                    $results = $wpdb->get_results( "SELECT * FROM wp_user_request_for_hidden_erotics WHERE sender_id = '$current_user_ID' AND reciver_id = '$userid'"  );
                                }                    
                                if ($visibilty_setting[2]->$current_user_type == 0 && $current_user_type == 'basic') {
                                    echo '<span class="grayTrans"></span>';
                                }
                            ?>
                            <h3 class="eropic-h3 no-margin no-padding col-sm-12 col-md-12">
                                <span>Her erotic profile</span>
                                <a href="javascript:void(0)">
                                    <?php if (count($results) > 0){ ?>
                                        <span id="show_hidden" style="<?php echo ($current_user_type == 'validated') ?  'color: rgba(255, 255, 255, 0.3);pointer-events: none;' : ''; ?>" class="profile-her-erotic-right-btn">E</span>
                                    <?php }else{ ?>
                                        <span data-toggle="modal" style="<?php echo ($current_user_type == 'validated') ?  'color: rgba(255, 255, 255, 0.3);pointer-events: none;' : ''; ?>" data-target="#send_request" id="open_box" class="profile-her-erotic-right-btn">E</span>
                                    <?php } ?>                                                        
                                </a>
                            </h3> 
                                <div style="display:block;" id="hide_hidden1">
                                    <?php
                                    $count = 0;
                                    for ($i = 0; $i < count($unique_arr_dist); $i++) 
                                    {
                                        $color = $content['id'][$count];
                                        $getTileShow = getActiveOrPassviveTitle($wpdb,$userid,$color, '1');
                                        //print_r($getTileShow);
                                        ?>              
                                        <div class="eropic-box-round " style="<?php echo ($getTileShow[0] != '2') ? 'display:block' : 'display:none'; ?>;background-color:<?php echo ($bgColor != "") ? $bgColor : $textColor[$color]; ?>;color:<?php echo ($bgColor != "") ? $bgColor : $forColor[$color]; ?>;">
                                            <p><strong class="up-case"><?php echo strtoupper(qtrans_use($q_config['language'],$unique_arr_dist[$i], true)); ?> :</strong></p>
                                            <?php for($j = 0; $j < count($content['subject']); $j++)
                                            {
                                                if($content['desc'][$j]==$unique_arr_dist[$i])
                                                { 
                                                    $like_dislike_ID = $content['subjectID'][$j];
                                                    $text = getActiveOrPassvive($wpdb,$userid,$color,$like_dislike_ID, '1');
                                                ?>
                                            <p style="<?php echo $text['class']; ?>"><?php echo qtrans_use($q_config['language'],$content['subject'][$j], true) .' '.  $text['text']; ?></p>
                                            <?php } } ?>
                                        </div>
                                        <?php $count++; } ?>
                                </div>                            
                            <div style="display:none;" id="hidden_erotics">
                                <?php 
                                for ($i = 0; $i < count($hidden_content['subject']); $i++){ ?>             
                                    <div class="eropic-box-round bg-green1">
                                        <p>
                                            <strong class="up-case">
                                                <?php echo $hidden_content['desc'][$i]; ?> :
                                            </strong>
                                            <?php echo $hidden_content['subject'][$i]; ?>
                                        </p>
                                    </div>
                                <?php } ?>  
                            </div>  
                        </div>  
                        <?php } // end if ?>                                             
                        <!--===end ladies ====-->
                    </div>
                
                    <!--send mail code-->
                    <div class="col-sm-3">
                        <div id ="send_mail_viewer" style="display:none;width:375px" class="profile-mail-part">
                            <?php
                                $user = get_user_by('id', $user_id);
                                $user->user_email;
                            ?>
                            <div class="mail-btn-back">
                                <a href="javascript:void(0)" id ="back_profile">
                                    <img src="<?php bloginfo('template_directory'); ?>/images/arrow-right.png"> &nbsp; &nbsp;Back to profile
                                </a>
                            </div>
                            <div class="col-sm-11 mail-part-main">
                                <p class="mail-text-top">SEND MAIL TO <span> <?php echo ucfirst($get_user_info['nickname']['0']); ?></span></p>
                                <form  method="post" enctype="multipart/form-data"> 
                                    <div class="form-group">
                                        <input class="form-control" placeholder="We would like to meet you" type ="text" name ="headline" id="headline" value="" maxlength="60">
                                    </div>
                                    <p class="txtbox-under-text">Headline <span>(less than 60 characters)</span></p>
                                    <textarea id='editor1' class="form-control-area" ></textarea>
                                    <!--<p class="txtbox-under-text msgNext">Mail <span>(less than 5,000 characters)</span></p>-->
                                    <p><span></span></p>
                                    <script>
                                        CKEDITOR.replace('editor1', {
                                            toolbar: [
                                                {name: 'document', items: ['-', 'NewPage', 'Preview', '-', 'Templates']},
                                                {name: 'basicstyles', items: ['Bold', 'Italic', 'Underline']}
                                            ]
                                        });
                                    </script>
                                    <?php
                                    if ($operational_setting[1]->$current_user_type == 1) {
                                    ?>
                                        <div class="row">
                                            <div class="col-sm-2">      
                                                <input type="file" name="upload1" size="30" class="attachment" id ="attachment1" onchange="showimagepreview(this)" style="display:none;" >
                                                <label class="file-uploader" for="attachment1">+add</label>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="txtbox-under-text"><span>Attaché picture 1</span></p>
                                            </div>
                                            <div class="col-sm-4">
                                                <button type="button" class="btn dating-btn-gray" data-toggle="modal" data-target="#myModal" id="preview">Preview</button>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <div class="row">
                                        <?php if ($operational_setting[1]->$current_user_type == 1) { ?>
                                            <div class="col-sm-2">
                                                <input type="file" name="upload2" size="30" class="attachment" id ="attachment2" onchange="showimagepreview1(this)" style="display:none;">
                                                <label class="file-uploader" for="attachment2">+add</label>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="txtbox-under-text"><span>Attaché picture 2</span></p>
                                            </div>
                                        <?php } ?>
                                        <div class="col-sm-4">
                                            <input type ="hidden" name="sender_uid" value="<?php echo $current_user_ID; ?>" >
                                            <input type ="hidden" name="recipient_uid" value="<?php echo $user_id; ?>" >
                                            <input type="hidden" name="from" value="<?php echo $current_user->user_email; ?>">
                                            <input type="hidden" name="to" value="<?php echo $user->user_email; ?>">
                                            <button type="button" name="send" class="btn dating-btn-maroon" value="PUBLISH" id="ajax_email">PUBLISH</button>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="col-sm-6"><img id="img_mail1" class="img-responsive" height="150" width="150"></div>
                                        <div class="col-sm-6"><img id="img_mail2" class="img-responsive" height="150" width="150"></div>
                                    </div>
                                </form>
                                <p class="mail-text-bottom">
                                    <strong>Note : </strong>Advertisement and mass mailing is prohibited. If you send
                                    server mails with same or always same content this can lead to
                                    cancellation of your membership.
                                </p>
                            </div>
                            <br/>
                            <div id ="mail_msg"></div>
                            <span class="mail_img" style="display:none;">
                                <img height="12px" width="60px" src="<?php echo get_template_directory_uri(); ?>/images/ajax-status.gif">
                            </span>
                        </div>
                    </div> 
                    <!--mail code end -->
                <input type="hidden" name="logged_in_user" value="<?php echo $current_user_ID; ?>" id ="logged_in_user">
            </div>
        </form>
    </div>
</div>
<?php
        break;
    } // switch case
?>

<!--============================= Functional part here ================-->
<script type="text/javascript">
// image preview
function showimagepreview(input) {
    if (input.files && input.files[0])
    {
        var filerdr = new FileReader();
        filerdr.onload = function (e) {
            //$("#femalepick").remove();
            $('#imgprvw1').attr('src', e.target.result);
            $('#img_mail1').attr('src', e.target.result);
        }
        filerdr.readAsDataURL(input.files[0]);
    }
}

function showimagepreview1(input) {
    if (input.files && input.files[0])
    {
        var filerdr = new FileReader();
        filerdr.onload = function (e) {
            //$("#malepick").remove();	
            $('#imgprvw2').attr('src', e.target.result);
            $('#img_mail2').attr('src', e.target.result);
        }
        filerdr.readAsDataURL(input.files[0]);
    }
}

// open send mail part and hide
jQuery(document).ready(function () {
    jQuery("#send_mail").click(function () {
        jQuery("#send_mail_viewer").show();
        jQuery("#left_section").hide();
    });

    jQuery("#back_profile").click(function () {
        jQuery("#send_mail_viewer").hide();
        jQuery("#left_section").show();
    });
});

jQuery("#ajax_email").click(function () {
    var hint = 0;
    var str = $("form").serialize();
    var headline = $("#headline").val();
    if(headline.length  > 60 || headline.length == 0)
    {
        jQuery('#headline').after( "<div id='char_validate' class='successMsg'>Please write less than 60 charachter!</div>" );
        setTimeout(function () {
                    jQuery('#char_validate').fadeOut(6000);}, 6000);
        hint = 1;
    }
    var mail_content = CKEDITOR.instances['editor1'].getData();
    if(mail_content.length  > 5000)
    {
        jQuery('.msgNext').after( "<div id='mail_char' class='successMsg'>Your message content will not exceed to charachter limit!</div>" );
        setTimeout(function () {
                    jQuery('#mail_char').fadeOut(6000);}, 6000);
        hint = 1;
    }
    var logged_in_user = $("#logged_in_user").val();
    var img_mail1 = $("#img_mail1").attr('src');
    var img_mail2 = $("#img_mail2").attr('src');
    var formData = 'action=my_action_mail_send&str=' + str + '&img_mail1=' + img_mail1 + '&img_mail2=' + img_mail2 + '&mail_content=' + mail_content + '&headline=' + headline;
    if(hint == 0)
    {
        jQuery.ajax({
        url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
        data: formData,
        type: 'POST',
        dataType: 'json',
        beforeSend: function () {
            jQuery(".mail_img").css("display", "block");
        },
        success: function (response) {
            if(response.msg == 'true')
            {
                   jQuery("#mail_msg").html(response.err);
                    setTimeout(function () {
                    jQuery('#mail_msg').fadeOut(5000);
                    location.reload();
                }, 5000); 
            }else
            {
                jQuery("#mail_msg").html(response.err);
                    setTimeout(function () {
                    jQuery('#mail_msg').fadeOut(10000);}, 10000);
                    }            
        },
        complete: function () {
            jQuery('#headline').val('');
            jQuery(".mail_img").css("display", "none");
            location.location();
        }
    });

    }
    
});
</script>

<!--========Send request for hidden erotics functionality ==============-->
<script>
jQuery('#request_to_user').click(function(){
    var textarea = $('textarea#request_hidden_erotics').val();
    var reciverid = '<?php echo $_GET["user_id"] ?>';
    var formData = 'action=request_for_hidden_erotics_by_user&message=' + textarea + '&reciverid='+reciverid;
    jQuery.ajax({
        url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
        data: formData,
        type: 'POST',
        dataType: 'json',
        beforeSend: function () {
            jQuery(".parent").css("display", "block");
        },
        success: function (data) {                                
            if (data.txt == 'success') {
                $('#profile_img').show();
                $('#profile_img').html(data.msg);
                setTimeout(function(){jQuery('#profile_img').fadeOut(2000);}, 3000);                                    
            }else if (data.txt == 'failed') {
                $('#profile_img').show();
                $('#profile_img').html(data.msg);
                setTimeout(function(){jQuery('#profile_img').fadeOut(2000);}, 3000); 
            }
        },
        complete: function ()
        {
            jQuery(".parent").css("display", "none");
        }
    });

});

</script>

<!--===================Send mail functionality ==========================-->
<script>
// images preview
$(document).ready(function () {
    $("#preview").click(function () {
        var headline = $("#headline").val();
        var editor1 = CKEDITOR.instances['editor1'].getData();
        var attachment1 = $("#attachment1").val();
        $("#subject").html(headline);
        $("#mail_data").html(editor1);
    });

    $('a').on('click', '#show_hidden', function () {
        var reciver_id = '<?php echo $_GET["user_id"]; ?>';
        var formData = 'action=my_action_show_hidden_erotics&reciverid=' + reciver_id;
        jQuery.ajax({
            url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
            data: formData,
            type: 'POST',
            dataType: 'json',
            beforeSend: function () {
                jQuery(".bar_img").css("display", "block");
            },
            success: function (data) {
                if (data.txt == 'success') {
                    $('span#show_hidden').text('');
                    $('span#show_hidden').html("<img src='<?php bloginfo('template_directory'); ?>/images/profile_back_btn.png'>");
                    $('#hide_hidden').hide();
                    $('#hidden_erotics').show();
                    $('span#show_hidden').attr('id', 'hidden_erotics_fields');
                }
            },
            complete: function ()
            {
                jQuery(".bar_img").css("display", "none");
            }
        });
    });

    $('a').on('click', 'span#hidden_erotics_fields', function () {
        $('span#show_hidden').html("");
        $('#hide_hidden').show();
        $('#hidden_erotics').hide();
        $(this).attr('id', 'show_hidden');
        $(this).text('E');
    });

});
</script>

<!--============script for showing his erotics preference ===============-->
<script>
$('a').on('click', '#show_hidden1', function () {
    var reciver_id = '<?php echo $_GET["user_id"]; ?>';
    var formData = 'action=my_action_show_hidden_erotics&reciverid=' + reciver_id;
    jQuery.ajax({
        url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
        data: formData,
        type: 'POST',
        dataType: 'json',
        beforeSend: function () {
            jQuery(".bar_img").css("display", "block");
        },
        success: function (data) {
            if (data.txt == 'success') {
                $('span#show_hidden1').text('');
                $('span#show_hidden1').html("<img src='<?php bloginfo('template_directory'); ?>/images/profile_back_btn.png'>");
                $('#hide_hidden1').hide();
                $('#hidden_erotics1').show();
                $('span#show_hidden1').attr('id', 'hidden_erotics_fields1');
            }
        },
        complete: function ()
        {
            jQuery(".bar_img").css("display", "none");
        }
    });
});

$('a').on('click', 'span#hidden_erotics_fields1', function () {
    $('#hide_hidden1').show();
    $('#hidden_erotics1').hide();
    $(this).attr('id', 'show_hidden1');
    $('span#show_hidden1').html("E");
});

</script>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Image Preview</h4>
                <div id="hidden_msg" class="successMsg"></div>
            </div>
            <div class="modal-body">
                <p id = "subject"></p>
                <p id ="mail_data"></p>
                <img id="imgprvw2" height="150" width="90">
                <img id="imgprvw1" height="150" width="90">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!--=================== End Send mail functionality ==========================-->

<!--=================== like to add favorate =============================-->
<input type="hidden" name="liked_by" value="<?php echo $current_user->ID; ?>" id="liked_by">
<input type="hidden" name="liked_to" value="<?php echo $user_id; ?>" id="liked_to">
<script>
    jQuery("#add_fav").click(function () {
        var sender = $('#liked_by').val();
        var reciver = $('#liked_to').val();
        formData = 'action=my_action_add_to_fav&sender=' + sender + '&reciver=' + reciver + '&status=favourite';
        jQuery.ajax({
            url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
            data: formData,
            type: 'POST',
            dataType: 'json',
            beforeSend: function () {
                jQuery(".bar_img1").css("display", "block");
            },
            success: function (data) {
                $('span#add_fav_msg').show();
                if (data.msg == 'fail') {
                    $('span#add_fav_msg').text(data.err);
                    setTimeout(function () {
                        jQuery('#add_fav_msg').fadeOut(7000);
                    }, 7000);
                } else if (data.msg == 'success') {
                    $('#add_fav').find('span').css('style','color:white');
                    $('#add_fav').find('span').text(data.txt);
                    $('span#add_fav_msg').text(data.err);
                    setTimeout(function () {
                        jQuery('#add_fav_msg').fadeOut(7000);
                    }, 7000);
                }
            },
            complete: function ()
            {
                jQuery(".bar_img1").css("display", "none");
            }
        });
    });

</script>

<!--=================== End like to add favorate ========================-->

<!--=================== like to add favorate a kiss mail functionality==========================-->
<?php $kiss_image = $upload_dir['baseurl'] . '/lip.jpg'; ?>
<img class="prof-cartoon-img" src="<?php echo $kiss_image = $upload_dir['baseurl'] . '/lip.jpg'; ?>" style="display:none" id ="kiss_img">
<input type="hidden" name="liked_by" value="<?php echo $current_user->ID; ?>" id="sent_kiss_by">
<input type="hidden" name="liked_to" value="<?php echo $user_id; ?>" id="to_sent">

<script>
    jQuery("#send_a_kiss").click(function () {
        var sender = $('#sent_kiss_by').val();
        var reciver = $('#to_sent').val();
        formData = 'action=my_action_add_to_fav&sender=' + sender + '&reciver=' + reciver + '&status=kiss';
        jQuery.ajax({
            url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
            data: formData,
            type: 'POST',
            dataType: 'json',
            beforeSend: function () {
                jQuery(".bar_img2").css("display", "block");
            },
            success: function (data) {
                $('span#kiss_msg').show();
                if (data.msg == "fail") {
                    $('span#kiss_msg').text(data.err);
                    setTimeout(function () {
                        jQuery('#kiss_msg').fadeOut(7000);
                    }, 7000);
                } else if (data.msg == 'success') {
                    $('span#kiss_msg').text(data.err);
                    setTimeout(function () {
                        jQuery('#kiss_msg').fadeOut(7000);
                    }, 7000);
                }
            },
            complete: function ()
            {
                jQuery(".bar_img2").css("display", "none");
            }
        });
    });
</script>

<!--=================== End Send a kiss mail functionality==========================-->

<!--=================== make a note to this profile =======================-->
<div class="modal fade" id="make_note" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="close_quoto_msg" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Make a Note</h4>
                <span id="note_msg" class="successMsg"></span>
            </div>
            <div class="modal-body">
                <div class="editBoth">
                    <textarea id='editor2'></textarea>              
                    <script>CKEDITOR.replace('editor2', {
                            toolbar: [
                                {name: 'document', items: ['-', 'NewPage', 'Preview', '-', 'Templates']},
                                {name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Underline']},
                                '/', ]
                        });
                    </script>
                </div>
            </div>
            <div class="modal-footer">
                <span class="note_img" style="display:none;">
                    <img height="12px" width="60px" src="<?php echo get_template_directory_uri(); ?>/images/ajax-status.gif">
                </span>
                <button type="button" id="close_quoto" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id ="add_make_button">Send</button>
            </div>
        </div>
    </div>
</div>
<script>
jQuery("#add_make_button").click(function () {
    var editor2 = CKEDITOR.instances['editor2'].getData();
    var logged_in_user = $("#logged_in_user").val();
    var note_to = $("#note_to").val();
    formData = 'action=my_action_make_note&editor2=' + editor2 + '&logged_in_user=' + logged_in_user + '&note_to=' + note_to;
    jQuery.ajax({
        url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
        data: formData,
        type: 'POST',
        dataType: 'json',
        beforeSend: function () {
            jQuery(".note_img").css("display", "block");
        },
        success: function (response) {
            jQuery('#note_msg').html(response.err);
            setTimeout(function () {
                jQuery('#note_msg').fadeOut(2000);
            }, 3000);
        },
        complete: function ()
        {
            jQuery(".note_img").css("display", "none");
        }
    });
});

</script>

<!--=================== End make a note ==============================-->

<!--=================== Block this profile ========================-->
<script>
    jQuery("#block_profile").click(function () {
        var logged_in_user = $("#logged_in_user").val();
        var block_user_id = $("#block_user_id").val();
        formData = 'action=my_action_add_to_fav&sender=' + logged_in_user + '&reciver=' + block_user_id + '&status=block';
        jQuery.ajax({
            url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
            data: formData,
            type: 'POST',
            dataType: 'json',
            beforeSend: function ()
            {
                jQuery(".bar_img").css("display", "block");
            },
            success: function (data) {
                $('span#block_user_profile').show();
                if (data.msg == 'fail') {
                    $('span#block_user_profile').text(data.err);
                    setTimeout(function () {
                        jQuery('#block_user_profile').fadeOut(2000);
                    }, 3000);
                } else if (data.msg == 'success') {
                    $('span#block_user_profile').text(data.err);
                    setTimeout(function () {
                        jQuery('#block_user_profile').fadeOut(2000);
                    }, 3000);
                }
            },
            complete: function ()
            {
                jQuery(".bar_img").css("display", "none");
            }
        });
    });
</script>
<!--=================== End Block this profile=======================-->

<!--=================== Submit complaint about profile =================-->
<div class="modal fade" id="report_abuse" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <input type="hidden" name="block_user_id" value="<?php echo $user_id; ?>" id="block_user_id">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="close_quoto_msg" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">complaint</h4>
                <span id="complaint_msg" class="successMsg"></span>
            </div>
            <div class="modal-body">
                <div class="editBoth">
                    <textarea id='editor3'></textarea>              
                    <script>CKEDITOR.replace('editor3', {
                            toolbar: [
                                {name: 'document', items: ['-', 'NewPage', 'Preview', '-', 'Templates']},
                                {name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Underline']},
                                '/', ]
                        });
                    </script>
                </div>
            </div>
            <div class="modal-footer">
                <span class="comp_img" style="display:none;">
                    <img height="12px" width="60px" src="<?php echo get_template_directory_uri(); ?>/images/ajax-status.gif">
                </span>
                <button type="button" id="close_quoto" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id ="add_complain">Send</button>
            </div>
        </div>
    </div>
</div>

<script>
    jQuery("#add_complain").click(function () {
        var complaint_by = $("#complaint_by").val();
        var note_to = $("#block_user_id").val();
        var editor3 = CKEDITOR.instances['editor3'].getData();
        formData = 'action=my_action_add_to_fav&sender=' + complaint_by + '&reciver=' + note_to + '&messagebody=' + editor3 + '&status=complain';
        jQuery.ajax({
            url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
            data: formData,
            type: 'POST',
            dataType: 'json',
            beforeSend: function ()
            {
                jQuery(".comp_img").css("display", "block");
            },
            success: function (response) {
                $('#complaint_msg').show();
                if (response.msg =='fail') {
                    $('#complaint_msg').text(response.err);
                    setTimeout(function () {
                        jQuery('#complaint_msg').fadeOut(2000);
                    }, 3000);
                } else if (response.msg =='success') {
                    $('#complaint_msg').text(response.err);
                    setTimeout(function () {
                        jQuery('#complaint_msg').fadeOut(2000);
                    }, 3000);
                }
            },
            complete: function ()
            {
                jQuery(".comp_img").css("display", "none");
            }
        });
    });
</script>
<!--=================== End Submit complaint about profile =============================-->

<!--=================== hide/show profileopen/close albumserotic preferences ==========================-->
<input type="hidden" name="hidden_by" value="<?php echo $current_user_ID; ?>" id ="hidden_by">
<input type="hidden" name="hide_user_id" value="<?php echo $user_id; ?>" id="hide_user_id">

<script>
    jQuery("#hide_show").click(function () {
        var hidden_by = $("#hidden_by").val();
        var hide_user_id = $("#note_to").val();

        formData = 'action=my_action_hide_profile&hidden_by=' + hidden_by + '&hide_user_id=' + hide_user_id;

        jQuery.ajax({
            url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
            data: formData,
            type: 'POST',
            dataType: 'json',
            complete: function ()
            {
                jQuery(".bar_img").css("display", "block");
            },
            success: function (response) {
                alert(response.err);
            },
            complete: function ()
            {
                jQuery(".bar_img").css("display", "none");
            }
        });
    });

</script>

<!--=================== End hide/show profileopen/close albumserotic preferences=====================-->                     
<?php
    } //membership if
    else {
?>
    <div class="text-center succesPoint">
        <h4>Your viewing own profile ....!</h4>
    </div>
<?php
    }
} else {
?>
    <div class="text-center succesPoint">
        <h4>You must be loggedin to access this page</h4>
    </div>
<?php
}
?>

<?php get_footer(); ?>
