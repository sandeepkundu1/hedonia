<?php
/*
 * Template Name: External Member Homepage 
 * Description: A Page Template show the Member profile homepage.
 * Author Name : Sanjay Shinghania
 */

//get_header(); ?>
<link rel="shortcut icon" href="<?php echo bloginfo('template_url'); ?>/images/favicon.ico" type="image/x-icon" />
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.css">
        <link href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo get_template_directory_uri(); ?>/css/fonts.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo get_template_directory_uri(); ?>/css/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/style.css">    
        <link href="<?php echo get_template_directory_uri(); ?>/css/autoCompleteStyle.css" rel="stylesheet" type="text/css"/>

        <script src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.11.1.min.js" type="text/javascript"></script>          
        <script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.js" type="text/javascript"></script>

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
        <script type="text/javascript" src="http://ajax.microsoft.com/ajax/jQuery.Validate/1.6/jQuery.Validate.min.js"></script>
        <script type="text/javascript" src="<?php echo bloginfo('template_url'); ?>/js/browser_and_os_classes.js"></script>

<!--script src="<?php //echo content_url() . '/themes/ckeditor/ckeditor.js';  ?>" type="text/javascript"></script-->       
        <script type= "text/javascript" src = "<?php echo bloginfo('template_url'); ?>/js/countries12345.js"></script>
        <script src="<?php echo bloginfo("template_url"); ?>/js/sample-registration-form-validation.js"></script>
        <script src="https://cdn.ckeditor.com/4.4.5.1/standard/ckeditor.js"></script>
        <script type="text/javascript" src="<?php echo bloginfo('template_url'); ?>/js/autoload.js"></script>

        <!--- scroll script and style -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.mCustomScrollbar.concat.min.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.mCustomScrollbar.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.mousewheel.min.js" type="text/javascript"></script>
        <script>
            (function ($) {
                $(window).load(function () {
                    $(".mCustomScrollbar,.cus_scrolln,.cus_scroll,.eropic-profile-Part,.Favourite-Part2,#content-1,.Favourite-Part,.text-box-round").mCustomScrollbar({
                        mouseWheel: {scrollAmount: 300}
                    });
                });
            })(jQuery);
        </script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        // Password Match
        jQuery('.no_image_avl').click(function(){
            jQuery('.no_img_avl').html("No image available for this album");
        });
        jQuery('#click').click(function () {
            var userID = jQuery("#userid").val();
            var userPass = jQuery("#match-pass").val();
            jQuery(".al_img").css("display", "block");
            jQuery.ajax({
                url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
                data: 'action=my_action_password_match&user_id=' + userID + '&pass=' + userPass,
                type: 'POST',
                dataType: 'json',
                success: function (response) {
                    var msg = response.msg;
                    if (msg == 'Fail')
                    {
                        jQuery("#update").html(response.err);
                        jQuery("#update").addClass('alert alert-danger');
                        jQuery("#update").show();
                        setTimeout(function () {
                            jQuery('#update').fadeOut(2000);
                        }, 3000);
                    } else
                    {
                        jQuery(".pass-section").css("display", "none");
                        jQuery(".externally-profile").css("display", "block");
                    }
                },
                complete: function ()
                {
                    jQuery(".al_img").css("display", "none");
                }
            });
        });
    });
</script>
<?php
#erotic color display
$textColor = array(
            "182" => "#395723", 
            "183" => "#476D2D", 
            "184" => "#61953D", 
            "185" => "#A9D12A",
            "186" => "#E2F0D9", 
            "187" => "#ECF5E7", 
            "706" => "#F8FBF7", 
            "707" => "#F2F2F2", 
            "708" => "#F2F2F2", 
            "709" => "#F2F2F2", 
            "710" => "#F2F2F2", 
            "711" => "#F2F2F2", 
            "712" => "#F2F2F2", 
            "713" => "#F2F2F2", 
            "714" => "#F2F2F2", 
            "715" => "#FF3F3F",
            "716" => "#C00000"             
        );

    $forColor = array(
            "182" => "#fff", 
            "183" => "#fff", 
            "184" => "#fff", 
            "185" => "#fff",
            "186" => "#000", 
            "187" => "#000", 
            "706" => "#000", 
            "707" => "#000", 
            "708" => "#000", 
            "709" => "#000", 
            "710" => "#000", 
            "711" => "#000", 
            "712" => "#000", 
            "713" => "#000", 
            "714" => "#000", 
            "715" => "#000", 
            "716" => "#000"            
        );

// get the user id
$urlencode = urldecode($_SERVER['QUERY_STRING']);
$memberdata = explode('?member=', $urlencode);
$user_id = $memberdata[1]; // user id

$get_user_info = get_user_meta($user_id);
#=== * Profilepick for couples
$upload_dir = wp_upload_dir();
$her_pick = get_user_meta($user_id, 'her_pick', true);
$his_pick = get_user_meta($user_id, 'his_pick', true);
$image_url_female = $upload_dir['baseurl'] . '/profilepick_' . $user_id . '/' . $her_pick;
$image_url_male = $upload_dir['baseurl'] . '/profilepick_' . $user_id . '/' . $his_pick;
#===* Age  for couples
$age1 = get_user_meta($user_id, 'she_dob', true);
$age2 = get_user_meta($user_id, 'he_dob', true);
if($age1=='undefined')
{
   $age1 = date();
}
if($age2=='undefined')
{
   $age2 = date();
}
$current_date = date();

$datetime1  = new DateTime($current_date);
$datetime2  = new DateTime($age1);
$her_age    = $datetime1->diff($datetime2);
$female_age = $her_age->y;

$datetime3  = new DateTime($current_date);
$datetime4  = new DateTime($age2);
$his_age    = $datetime3->diff($datetime4);
$male_age   = $his_age->y;

$her_hight = $get_user_info['her_hight']['0'];
$his_hight = $get_user_info['his_hight']['0'];

$her_figure = $get_user_info['her_figure']['0'];
$his_figure = $get_user_info['his_figure']['0'];

$her_smoke = $get_user_info['her_smoke']['0'];
$his_smoke = $get_user_info['his_smoke']['0'];

#=== Language
$her_language = $get_user_info['her_language']['0'];
$her_language1 = $get_user_info['her_language1']['0'];
$her_language2 = $get_user_info['her_language2']['0'];
$her_language3 = $get_user_info['her_language3']['0'];

$selectedLanguages = "";
if ($her_language != "") {
    $selectedLanguages .= $her_language . ",";
}
if ($her_language1 != "") {
    $selectedLanguages .= $her_language1 . ",";
}
if ($her_language2 != "") {
    $selectedLanguages .= $her_language2 . ",";
}
if ($her_language3 != "") {
    $selectedLanguages .= $her_language3 . ",";
}

$his_language = $get_user_info['his_language']['0'];
$secondLang = $get_user_info['language_two']['0'];
$thrd_lang = $get_user_info['language_thre']['0'];
$forLang = $get_user_info['language_for']['0'];

$selectedLanguages1 = "";
if ($his_language != "") {
    $selectedLanguages1 .= $his_language . ",";
}
if ($secondLang != "") {
    $selectedLanguages1 .= $secondLang . ",";
}
if ($thrd_lang != "") {
    $selectedLanguages1 .= $thrd_lang . ",";
}
if ($forLang != "") {
    $selectedLanguages1 .= $forLang . ",";
}

$her_weight = $get_user_info['her_weight']['0'];
$his_weight = $get_user_info['his_weight']['0'];

$her_profession = $get_user_info['her_profession']['0'];
$his_profession = $get_user_info['his_profession']['0'];

$her_hobby = $get_user_info['her_hobby']['0'];
$his_hobby = $get_user_info['his_hobby']['0'];

$describe = $get_user_info['describe']['0'];
$fav_quote_content = $get_user_info['faq_quote']['0'];

$nickname = $get_user_info['nickname']['0'];
$first_name = $get_user_info['first_name']['0'];
$last_name = $get_user_info['last_name']['0'];

$country = $get_user_info['country']['0'];
$data = unserialize($country);
$main_country = $data['0'];
$second_country = $data['1'];

$district1 = $get_user_info['district1']['0'];
$datadistrict = unserialize($district1);
$main_district = $datadistrict['0'];
$second_district = $datadistrict['1'];

$city1 = $get_user_info['city1']['0'];
$datacity = unserialize($city1);
$main_city = $datacity['0'];
$second_city = $datacity['1'];

$reg3_txtshe = $get_user_info['reg3_txtshe']['0'];
$reg3_txthe = $get_user_info['reg3_txthe']['0'];
$reg3_txtboth = $get_user_info['reg3_both']['0'];
#..code
$user_type = get_user_meta($user_id, 'member_type', true);
$profile_status      = get_user_meta($user_id,'membership_profile_status',true);
?>

<div class="navbar-center pass-section" id="pass-section">
    <h3 class="home-title">Title</h3>
    <div class="form-group">
        <label class="">Please Enter the password :</label>
        <input type="text" class="form-control" id="match-pass" placeholder="Password" aria-describedby="basic-addon1">
    </div>
    <div class="form-group">
        <input type="hidden" id="userid" value="<?php echo $user_id; ?>">
        <button type="submit" class="btn  premium-red_btn" id="click">Submit</button>
    </div>
    <div style="display:none;" class="al_img text-center" >
        <img width="60px" height="12px" src="<?php echo get_template_directory_uri(); ?>/images/ajax-status.gif">
    </div>
    <div id="update"></div>	
</div>

<!-- show the Externally profile -->
<div class="externally-profile" id="profile-section" style="display:none;">
    <?php
    switch ($user_type) {
        case 'couples': // display cuple html
            ?>
            <div class="navbar-header">
                <a href="<?php echo home_url(); ?>" class="navbar-brand"><?php echo do_shortcode('[site_logo]'); ?></a>
            </div>
            <div class="userProfile">
                <div class="container">
                <?php if($profile_status!='Deactive') 
                { ?>
                    <div class="parent_div">
                        <form class="form-horizontal" role="form" name="form" method="post" action ="">
                            <div class="profileTopBar" style="width: 71%;">
                                <h3>
                                    <span>Hedonia.ch is a Swiss portal for</span> highest level of safety, confidentiality and privacy in the Swinger World
                                </h3>
                                <a href="javascript:void(0)" class="whiteArrow">
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                    Highlights of Hedonia
                                </a>
                            </div>
                            <div class="col-sm-7 col-md-7 col-xs-12 no-padding">
                                <!--image show-->
                                <div class="col-sm-6 col-md-6 col-xs-12 no-padding" style="margin-top:25px">
                                    <div class="clear">
                                        <div class="col-md-6 col-sm-6 no-padding-left">
                                            <h3 class="search_premium_heading red_heading">
                                                <?php echo ucfirst(get_user_meta($user_id, 'nickname', true)); ?>
                                            </h3>
                                            <?php if ($her_pick_name != 'no_img.jpg') { ?>
                                                <img class="prof-cartoon-img" src="<?php echo $image_url_female; ?>">
                                            <?php } else { ?>
                                                <img class="prof-cartoon-img" src="<?php echo bloginfo('template_url'); ?>/images/images.jpeg">
                                            <?php } ?>
                                        </div>
                                        <div class="col-md-6 col-sm-6 no-padding-left">                         
                                            <?php if ($his_pick_name == 'no_img.jpg') { ?> 
                                                <img class="prof-cartoon-img" src="<?php echo bloginfo('template_url'); ?>/images/images.jpeg">                                           
                                            <?php } else { ?>
                                                <img class="prof-cartoon-img" src="<?php echo $image_url_male; ?>">
                                            <?php } ?>
                                        </div>
                                    </div> 
                                    <?php
                                        $src = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'full' );
                                    ?>
                                    <!-- <img src="<?php //echo $src[0]; ?>"> -->
                                </div>
                                <!--profile image and name-->

                                <div class="col-sm-6 no-padding">
                                    <!--basic info -->
                                    <div class="Profile-Part-BI cus_scroll">
                                        <h3 class="Proh-h3">
                                            <span>BASIC INFO</span>
                                        </h3>
                                        <div class="row">
                                            <p class="Pro-p col-sm-4 col-xs-offset-4">she</p>
                                            <p class="Pro-p col-sm-4">he</p> 
                                        </div>
                                        <div class="padding-basic-info">
                                            <div class="form-group border-bottom">
                                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Age</label>
                                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center bold"><?php echo $female_age; ?></label>
                                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center bold"><?php echo $male_age; ?></label>
                                            </div>
                                            <div class="form-group border-bottom">
                                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Height (cm)</label>
                                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center bold"><?php echo get_user_meta($user_id, 'her_hight', true); ?></label>
                                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center bold"><?php echo get_user_meta($user_id, 'his_hight', true); ?></label>
                                            </div>
                                            <div class="form-group border-bottom">
                                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Figure</label>
                                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center"><?php echo get_user_meta($user_id, 'her_figure', true); ?></label>
                                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center"><?php echo get_user_meta($user_id, 'his_figure', true); ?></label>
                                            </div>
                                            <div class="form-group border-bottom">
                                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Relationship</label>
                                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center"><?php echo get_user_meta($user_id, 'relation_ship', true); ?></label>
                                            </div>
                                            <div class="form-group border-bottom">
                                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Smoking</label>
                                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center"><?php echo get_user_meta($user_id, 'her_smoke', true); ?></label>
                                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center"><?php echo get_user_meta($user_id, 'his_smoke', true);
                                            ; ?></label>
                                            </div>

                                            <div class="form-group border-bottom row">
                                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Hair color</label>
                                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center"><?php echo get_user_meta($user_id, 'her_haircolor', true); ?></label>
                                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center"><?php echo get_user_meta($user_id, 'his_haircolor', true); ?></label>
                                            </div>

                                            <div class="form-group border-bottom">
                                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Languages</label>
                                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label "><?php echo str_replace(',', ', ', rtrim($selectedLanguages, ",")); ?></label>
                                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label "><?php echo str_replace(',', ', ', rtrim($selectedLanguages1, ",")); ?></label>
                                            </div>
                                            <div class="form-group border-bottom">
                                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Education</label>
                                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center"><?php echo get_user_meta($user_id, 'her_graduation', true); ?></label>
                                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label para-center"><?php echo get_user_meta($user_id, 'his_graduation', true); ?></label>
                                            </div>                                        
                                            <div class="form-group border-bottom">
                                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Hobbies Interests</label>
                                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label "><?php echo get_user_meta($user_id, 'her_hobby', true); ?></label>
                                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label "><?php echo get_user_meta($user_id, 'his_hobby', true);
                                            ; ?></label>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label">Additional info</label>
                                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label "><?php echo get_user_meta($user_id, 'about_her', true); ?></label>
                                                <label for="inputEmail3" class="col-sm-4 BasicInfo-label "><?php echo get_user_meta($user_id, 'about_his', true); ?></label>
                                            </div>
                                        </div>
                                    </div><!--basic information -->
                                </div> <!--col-sm-6-->

                                <div class="clear">
                                    <div class="col-sm-4 col-md-4 col-xs-12 no-padding"></div>  
                                    <div class="col-sm-8 col-md-8 col-xs-12 no-padding">
                                        <div class="col-sm-7 col-md-7 col-xs-12 ">
                                            <div class="Album-Part">
                                                <div class="row"> 
                                                    <h3 class="Pro-h3-2 col-sm-12 col-md-12"><span>ALBUMS</span></h3>
                                                </div>
                                                <div class="WidthFull">
                                                    <?php
                                                    $albumData = unserialize(get_user_meta($user_id, 'album_shows_case', true));
                                                    $imgPath = $upload_dir['baseurl'] . '/users_album/album_' . $user_id . '/';
                                                    for ($a = 0; $a < count($albumData); $a++) {
                                                        $no_img = $wpdb->get_results("SELECT album_name FROM user_create_album WHERE user_id = '" . $user_id . "' AND id='" . $albumData[$a] . "'");
                                                       foreach ($no_img as $value) 
                                                       {
                                                          $image_no=$value->album_name;
                                                       }
                                                        $results = $wpdb->get_results("SELECT uc.* , ua.img_url FROM user_create_album as uc INNER JOIN user_album_images as ua on uc.id =ua.album_id WHERE uc.user_id = '" . $user_id . "' AND ua.album_id='" . $albumData[$a] . "' ORDER BY ua.img_order ASC limit 1");
                                                        $count = 1;
                                                        if (count($results) > 0) {
                                                            foreach ($results as $value) {
                                                                ?>
                                                                <figure class="album-img-div text-center">
                                                                    <a target="_blank" href="<?php echo get_site_url(); ?>/?page_id=768&album=<?php echo $albumData[$a]; ?>&user_id=<?php echo $user_id; ?>"><img src="<?php echo $imgPath . $value->img_url; ?>"></a>
                                                                    <figcaption>
                                                                        <?php echo $image_no; ?>
                                                                    </figcaption>
                                                                </figure>
                                                                <?php
                                                            }
                                                        } else { ?>
                                                            <figure class="album-img-div text-center">
                                                                    <a href="javascript:void(0)" class="no_image_avl"><img src="<?php bloginfo('template_directory') ?>/images/no_image_avl.jpeg"></a>
                                                                    <figcaption>
                                                                        <?php echo $image_no; ?>
                                                                    </figcaption>
                                                                </figure>
                                         <?php          }
                                                    }
                                                    ?>                                                 
                                                </div> 
                                                <div class="no_img_avl"></div>      </div><!--album part-->
                                        </div>

                                        <!--fav section -->
                                        <div class="col-sm-5 col-md-5 col-xs-12 no-padding">
                                            <div class="Favourite-Part">
                                                <div class="clear"> 
                                                    <h3 class="Pro-h4 col-sm-12 col-md-12"><span>FAVOURITE QUOTE</span></h3>
                                                </div>
                                                <div class="col-sm-12 col-md-12 no-padding"> 
                                                    <p class="fav-para"><?php echo $fav_quote_content; ?></p>
                                                </div>
                                            </div>
                                        </div><!-- fav section -->

                                        <?php
                                            $results = $wpdb->get_results("SELECT * FROM bg_images where active = 1");
                                            $upload_dir = wp_upload_dir();
                                            $img = "";
                                            foreach ($results as $value) {
                                                $img = $value->img_url;
                                            }
                                            $imgPath = $upload_dir['baseurl'] . '/background_images' . '/' . $img;
                                        ?>
                                        <div class="col-sm-12 col-md-12 col-xs-12 no-padding-right">
                                            <div class="Introduction-Part" style="background-size: 100% 100%; background-image: url('<?php echo $imgPath; ?>');">
                                                <div class="clear"> 
                                                    <h3 class="Pro-h3 col-sm-12 col-md-12"><span>INTRODUCTION</span></h3>
                                                </div>
                                                <div class="padding-para-div"> 
                                                    <p class="Intro-para"><?php echo $describe; ?> </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- introduction -->
                                    </div>
                                </div>
                                <!--clear -->
                            </div>
                            <!--col-sm -7 -->

                            <div class="col-md-5 no-padding-right">
                                <div class="col-sm-6 col-md-6 col-xs-12 no-padding-left">
                                    <div class="Favourite-Part2">
                                        <div class="clear"> 
                                            <h3 class="Pro-h3 col-sm-12 col-md-12"><span>MAIN RESIDENCE</span></h3>
                                        </div>
                                        <div class="padding-top-bottom">
                                            <div class="form-group"> 
                                                <label for="inputEmail3" class="col-sm-6 BasicInfo-label">COUNTRY :</label>
                                                <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-left"><?php echo $country; ?></label>
                                            </div>
                                            <div class="form-group"> 
                                                <label for="inputEmail3" class="col-sm-6 BasicInfo-label">REGIO :</label>
                                                <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-left"><?php echo $district1; ?></label>
                                            </div>
                                            <div class="form-group"> 
                                                <label for="inputEmail3" class="col-sm-6 BasicInfo-label">NEXT CITY :</label>
                                                <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-left"><?php echo $city1; ?></label>
                                            </div>
                                            <div class="form-group"> 
                                                <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Frequently in :</label>
                                                <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-left"><?php echo $get_user_info['frequently_in']['0']; ?></label>
                                            </div>
                                        </div>
                                    </div> <!-- main residence -->

                                    <div class="Favourite-Part3 match_sec_img">                      
                                        <!--<div class=""> 
                                            <h3 class="Pro-h3 col-sm-12 col-md-12"><span>MATCHES</span></h3>
                                        </div>
                                        <div class="padding-top-bottom">
                                            <div class="row">
                                            <?php
                                                $alldata = $wpdb->get_results("SELECT * FROM wp_user_profile WHERE reciver_id='" . $user_id . "' AND add_fav='1' AND fav_status='0' OR fav_status='1' ORDER BY id DESC LIMIT 3");
                                                if (!empty($alldata)) 
                                                {
                                                    foreach ($alldata as $mat_value) 
                                                    {
                                                    $senderID = $mat_value->sender_id;
                                                    $user_name = get_user_meta($senderID, 'nickname', true);

                                                    $userUrl = get_permalink(305) . '&user_id=' . $senderID;
                                                    #.. sender user pic data
                                                    $his_pick = get_user_meta($senderID, 'his_pick', true);
                                                    $her_pick = get_user_meta($senderID, 'her_pick', true);
                                                    if (!empty($her_pick)) {
                                                        $pick_name = $her_pick;
                                                    } else {
                                                        $pick_name = $his_pick;
                                                    }
                                                    $pick_source = $upload_dir['baseurl'] . '/profilepick_' . $senderID . '/' . $pick_name;
                                            ?>
                                                        <div class="col-xs-4">
                                                            <img src="<?php echo $pick_source; ?>" height="45" width="52">
                                                            <label class="matchName"><?php echo $user_name; ?></label>
                                                        </div> 
                                                    <?php
                                                    }
                                                } else {
                                                    echo 'No MATCHES';
                                                }
                                                ?>  
                                            </div>           
                                        </div>-->
                                    </div>
                                    <!--second residence -->

                                  <?php  
                                    $userid = $user_id;
                                    $arr = array();
                                    $subId = array();
                                    $status = array();
                                    $myrows = $wpdb->get_results("SELECT *  FROM `wp_erotics_preference` where `userid` = $userid AND `type` = 1 order by like_dislike_attr_id" );
                                    foreach ($myrows as $result) {
                                        $arr[] = $result->like_dislike_attr_id;
                                        $subId[] = $result->subject_id;
                                        $status[] = unserialize($result->status);
                                    }                                                       

                                    $myrows1 = $wpdb->get_results("SELECT *  FROM `wp_erotics_preference` where `userid` = $userid AND `type` = 2 order by like_dislike_attr_id" );
                                    $arr1 = array();
                                    $subId1 = array();
                                    $status1 = array();

                                    foreach ($myrows1 as $result) {
                                        $arr1[] = $result->like_dislike_attr_id;
                                        $subId1[] = $result->subject_id;
                                        $status1[] = unserialize($result->status);
                                    }

                                    $arg = array('posts_per_page' => -1, 'offset' => 1, 'category' => 14, 'post_type' => 'eroticoption', 'order' => 'asc');
                                    $myposts = get_posts($arg);

                                    $myAllposts = array();
                                    $ctagoryid = array("3", "4", "5");

                                    foreach ($ctagoryid as $values) {
                                        $args = array('posts_per_page' => -1, 'offset' => 1, 'category' => $values->ID, 'post_type' => 'erotics', 'order' => 'asc', 'show_count' => 0);
                                        $myAllposts = get_posts($args);
                                    }

                                    $content = array();
                                    foreach ($subId as $key => $value) 
                                    {                          
                                        $content['subject'][] = get_the_title($value); 
                                        $content['subjectID'][] =  $value;                   
                                    }


                                    foreach ($arr as $key => $value1) {
                                        foreach ($myposts as $value) {
                                            if ($value1 == $value->ID) {
                                                $content['desc'][] = $value->post_title;
                                                if(!(in_array($value->ID, $content['id'])))
                                                {
                                                    $content['id'][] = $value->ID;
                                                } 
                                            }
                                        }
                                    }

                                    $content1 = array();
                                    foreach ($subId1 as $key => $value) 
                                    {                          
                                        $content1['subject'][] = get_the_title($value);
                                        $content1['subjectID'][] =  $value;                    
                                    }

                                    foreach ($arr1 as $key => $value1) {
                                        foreach ($myposts as $value) {
                                            if ($value1 == $value->ID) {
                                                $content1['desc'][] = $value->post_title;
                                                if(!(in_array($value->ID, $content1['id'])))
                                                {
                                                    $content1['id'][] = $value->ID;
                                                } 
                                            }
                                        }
                                    }
                                    $unique_arr_desc=array_unique($content['desc']);
                                    foreach ($unique_arr_desc as $value) 
                                    {
                                       $unique_arr_dist[]=$value;
                                    }
                                    $unique_arr_desc1=array_unique($content1['desc']);
                                    foreach ($unique_arr_desc1 as $value) 
                                    {
                                       $unique_arr_dist1[]=$value;
                                    }
                                    
                                   $eroticDatas = trim(get_user_meta($user_id, 'erotic_shows_case', true));  
                                
                                ?>

                                    <div class="eropic-profile-Part <?php if ($eroticDatas != 'erotic preferences') {
                                echo 'invalid';
                            } ?> ">  
                                    <?php if ($eroticDatas != 'erotic preferences') {
                                        echo '<span class="grayTrans"></span>';
                                    } ?>                          
                                        <div class="clear"> 
                                            <h3 class="eropic-h3 no-margin no-padding col-sm-12 col-md-12">
                                                <span>Her erotic profile</span>                                    
                                            </h3>
                                        </div>                                         
                                        <div style="display:block;" id="hide_hidden">
                                        <?php
                                        $count = 0;
                                        for ($i = 0; $i < count($unique_arr_dist); $i++) {
                                        $color = $content['id'][$count];
                                        $getTileShow = getActiveOrPassviveTitle($wpdb,$userid,$color, '1');
                                        ?>              
                                        <div class="eropic-box-round " style="background-color:<?php echo ($bgColor != "") ? $bgColor : $textColor[$color]; ?>;color:<?php echo ($bgColor != "") ? $bgColor : $forColor[$color]; ?>;">
                                            <p><strong class="up-case"><?php echo strtoupper(qtrans_use($q_config['language'],$unique_arr_dist[$i], true)); ?> :</strong></p>
                                            <?php for($j = 0; $j < count($content['subject']); $j++)
                                            {
                                                if($content['desc'][$j]==$unique_arr_dist[$i])
                                                { 
                                                    $like_dislike_ID = $content['subjectID'][$j];
                                                    $text = getActiveOrPassvive($wpdb,$userid,$color,$like_dislike_ID, '1');
                                                ?>
                                            <p style=""><?php echo qtrans_use($q_config['language'],$content['subject'][$j], true) .' '. $text['text']; ?></p>
                                            <?php }
                                            } ?>
                                        </div>
                                        <?php $count++; }?>
                                        </div>                           
                                    </div> <!--erotic part she-->
                                </div> 

                                <div class="col-sm-6 col-md-6 col-xs-12 no-padding">
                                    <div class="perfect-match-Part cus_scrolln">                            
                                        <div class="clear"> 
                                            <h3 class="opm-h3 col-sm-12 col-md-12"><span>PERFECT MATCH</span></h3>
                                        </div>
                                        <div class="opm-text-box-roundsec">
                                            <p><strong>SHE :</strong><?php echo $reg3_txtshe; ?></p>
                                        </div>
                                        <div class="opm-text-box-roundsec">
                                            <p><strong>HE :</strong><?php echo $reg3_txthe; ?></p> 
                                        </div>
                                        <div class="opm-text-box-roundsec">
                                            <p><strong>BOTH :</strong><?php echo $reg3_txtboth; ?></p> 
                                        </div>
                                    </div> <!-- our prefect match -->

                                    <div class="eropic-profile-Part <?php if ($eroticDatas != 'erotic preferences') {
                                                echo 'invalid';
                                            } ?> "> 
                                        <?php if ($eroticDatas != 'erotic preferences') {
                                            echo '<span class="grayTrans"></span>';
                                        } ?>                           
                                        <div class="clear"> 
                                            <h3 class="eropic-h3 no-margin no-padding col-sm-12 col-md-12">
                                                <span>His erotic profile</span>                                   
                                            </h3>
                                        </div>                            
                                        <div style="display:block;" id="hide_hidden1">
                                            <?php
                                        $count1 = 0;
                                        for ($i = 0; $i < count($unique_arr_dist1); $i++) 
                                        {
                                            $color1 = $content1['id'][$count1];
                                            $textColor[$color1]; 
                                            $getTileShow = getActiveOrPassviveTitle($wpdb,$userid,$color1, '2');                                                                                                                                
                                        ?>              
                                        <div class="eropic-box-round" style="background-color:<?php echo ($bgColor1 != "") ? $bgColor1 : $textColor[$color1]; ?>;color:<?php echo ($bgColor1 != "") ? $bgColor1 : $forColor[$color1]; ?>;">
                                            <p class="child"><strong class="up-case"><?php echo strtoupper(qtrans_use($q_config['language'],$unique_arr_dist1[$i], true)); ?> :</strong></p>
                                            <?php for($j = 0; $j < count($content1['subject']); $j++)
                                             {
                                            if($content1['desc'][$j]==$unique_arr_dist1[$i])
                                            { 
                                                $like_dislike_ID = $content1['subjectID'][$j];
                                                $text1 = getActiveOrPassvive($wpdb,$userid,$color1,$like_dislike_ID, '2');
                                            ?>
                                            <p style=""><?php echo qtrans_use($q_config['language'],$content1['subject'][$j], true) .' '. $text1['text']; ?></p>
                                            <?php }
                                        } ?>
                                        </div>
                                        <?php
                                        $count1++;
                                        }
                                        ?>
                                        </div>                            
                                    </div> <!--erotic he -->
                                </div>
                            </div>
                        </form>
                    </div>
                    <?php } else { ?>
                    <h2>User profile is Deactive</h2>
                    <?php } ?>
                </div>
            </div>
        <?php
            break; //cuple break

        default: // for male and female
        ?>
                        <div class="navbar-header">
                            <a href="<?php echo home_url(); ?>" class="navbar-brand"><?php echo do_shortcode('[site_logo]'); ?></a>
                        </div>
            <div class="userProfile">
                <div class="container-single-profile">
                <?php if($profile_status!='Deactive') 
                { ?>
                    <form class="form-horizontal" role="form" name="form" method="post">
                        <div class="profileTopBar">
                            <h3>
                                <span>Hedonia.ch is a Swiss portal for</span> highest level of safety, confidentiality and privacy in the Swinger World
                            </h3>
                            <a href="javascript:void(0)" class="whiteArrow">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                                Highlights of Hedonia
                            </a>
                        </div>
                        <div class="clear"></div>
                        <div class="row">
                            <div class="col-sm-3">                                                       
                                <div class="col-sm-10 no-padding profilePicWrap" >                           
                                    <h3 class="search_premium_heading <?php if($user_type=='ladies'){echo 'perple_heading';}else{echo 'blue_heading';} ?>">
                                        <?php echo ucfirst(get_user_meta($user_id, 'nickname', true)); ?>
                                    </h3>                         
                                    <?php if ($user_type == 'gentleman') { ?>
                                        <?php if ($his_pick == 'no_img.jpg') { ?> 
                                            <img class="prof-cartoon-img" src="<?php echo bloginfo('template_url'); ?>/images/images.jpeg">                                   
                                        <?php } else { ?>
                                            <img class="prof-cartoon-img" src="<?php echo $image_url_male; ?>">
                                        <?php } ?>
                                    <?php } else { ?>
                                        <?php if ($her_pick != 'no_img.jpg') { ?>
                                            <img class="prof-cartoon-img" src="<?php echo $image_url_female; ?>">
                                        <?php } else { ?>
                                            <img class="prof-cartoon-img" src="<?php echo bloginfo('template_url'); ?>/images/images.jpeg">
                                        <?php } ?>
                                    <?php } ?>
                                </div>  
                                <!-- picture wrap-->
                            </div> 
                            <!--col-sm-3 -->

                            <div class="col-sm-6 no-padding">                                
                                <div class="row">
                                    <div class="col-sm-6 no-padding-right">
                                        <div class="Profile-Part-BI cus_scroll">                              
                                            <h3 class="Proh-h3"><span>BASIC INFO</span></h3>
                                            <?php if ($user_type == 'ladies') { ?>
                                            <div class="padding-basic-info mt30">
                                                <div class="form-group border-bottom">
                                                    <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Age</label>
                                                    <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-center bold">
                                                        <?php                                                       
                                                            echo $female_age;                                                                           
                                                        ?>
                                                    </label>
                                                </div>
                                                <div class="form-group border-bottom">
                                                    <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Height (cm)</label>
                                                    <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-center bold"><?php echo $her_hight; ?></label>
                                                </div>
                                                <div class="form-group border-bottom">
                                                    <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Figure</label>
                                                    <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-center"><?php echo $her_figure; ?></label>
                                                </div>
                                                <div class="form-group border-bottom">
                                                    <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Relationship</label>
                                                    <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-center"><?php echo $get_user_info['relation_ship']['0']; ?></label>
                                                </div>
                                                <div class="form-group border-bottom">
                                                    <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Smoking</label>
                                                    <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-center"><?php echo $her_smoke; ?></label>
                                                </div>
                                                <div class="form-group border-bottom">
                                                    <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Hair color</label>
                                                    <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-center"><?php echo $get_user_info['her_haircolor']['0']; ?></label>
                                                </div>                                        
                                                <div class="form-group border-bottom">
                                                    <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Languages</label>
                                                    <label for="inputEmail3" class="col-sm-6 BasicInfo-label ">
                                                        <?php                                                        
                                                            echo str_replace(',', ', ', rtrim($selectedLanguages, ","));                                                        
                                                        ?>
                                                    </label>
                                                </div>
                                                <div class="form-group border-bottom">
                                                    <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Education</label>
                                                    <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-center"><?php echo $get_user_info['her_graduation']['0']; ?></label>
                                                </div>                                        
                                                <div class="form-group border-bottom">
                                                    <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Hobbies Interests</label>
                                                    <label for="inputEmail3" class="col-sm-6 BasicInfo-label "><?php echo $her_hobby; ?></label>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Additional info</label>
                                                    </label>
                                                    <label for="inputEmail3" class="col-sm-6 BasicInfo-label "><?php echo $get_user_info['about_her']['0']; ?></label>
                                                </div>
                                            </div>  
                                            <?php } else { ?>
                                            <div class="padding-basic-info mt30">
                                                <div class="form-group border-bottom">
                                                    <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Age</label>
                                                    <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-center bold">
                                                        <?php                                                    
                                                            echo $male_age;
                                                        ?>
                                                    </label>
                                                </div>
                                                <div class="form-group border-bottom">
                                                    <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Height (cm)</label>
                                                    <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-center bold"><?php echo $his_hight; ?></label>
                                                </div>
                                                <div class="form-group border-bottom">
                                                    <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Figure</label>
                                                    <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-center"><?php echo $his_figure; ?></label>
                                                </div>
                                                <div class="form-group border-bottom">
                                                    <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Relationship</label>
                                                    <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-center"><?php echo $get_user_info['relation_ship']['0']; ?></label>
                                                </div>
                                                <div class="form-group border-bottom">
                                                    <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Smoking</label>
                                                    <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-center"><?php echo $his_smoke; ?></label>
                                                </div>
                                                <div class="form-group border-bottom">
                                                    <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Hair color</label>
                                                    <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-center"><?php echo $get_user_info['his_haircolor']['0']; ?></label>
                                                </div>                                        
                                                <div class="form-group border-bottom">
                                                    <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Languages</label>
                                                    <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-center">
                                                        <?php                                                        
                                                            echo str_replace(',', ', ', rtrim($selectedLanguages1, ","));                                                        
                                                        ?>
                                                    </label>
                                                </div>
                                                <div class="form-group border-bottom">
                                                    <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Education</label>
                                                    <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-center"><?php echo $get_user_info['his_graduation']['0']; ?></label>
                                                </div>                                        
                                                <div class="form-group border-bottom">
                                                    <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Hobbies Interests</label>
                                                    <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-center"><?php echo $his_hobby; ?></label>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputEmail3" class="col-sm-6 BasicInfo-label">Additional info</label>
                                                    </label>
                                                    <label for="inputEmail3" class="col-sm-6 BasicInfo-label para-center"><?php echo $get_user_info['about_his']['0']; ?></label>
                                                </div>
                                            </div>                                                
                                            <?php }?>
                                            
                                        </div> 
                                        <!-- profile part -->

                                        <div class="Album-Part">
                                            <div class=""> 
                                                <h3 class="Pro-h3"><span>ALBUMS</span></h3>
                                                <div class="WidthFull">
                                                    <?php
                                                        $albumData = unserialize(get_user_meta($user_id, 'album_shows_case', true));
                                                        $imgPath = $upload_dir['baseurl'] . '/users_album/album_' . $user_id . '/';
                                                        for ($a = 0; $a < count($albumData); $a++) {
                                                            $no_img = $wpdb->get_results("SELECT album_name FROM user_create_album WHERE user_id = '" . $user_id . "' AND id='" . $albumData[$a] . "'");
                                                       foreach ($no_img as $value) 
                                                       {
                                                          $image_no=$value->album_name;
                                                       }
                                                        $results = $wpdb->get_results("SELECT uc.* , ua.img_url FROM user_create_album as uc INNER JOIN user_album_images as ua on uc.id =ua.album_id WHERE uc.user_id = '" . $user_id . "' AND ua.album_id='" . $albumData[$a] . "' ORDER BY ua.img_order ASC limit 1");
                                                        $count = 1;

                                                        if (count($results) > 0) {
                                                            foreach ($results as $value) {
                                                    ?>
                                                                <figure class="album-img-div text-center">
                                                                    <a target="_blank" href="<?php echo get_site_url(); ?>/?page_id=768&album=<?php echo $albumData[$a]; ?>&user_id=<?php echo $user_id; ?>"><img src="<?php echo $imgPath . $value->img_url; ?>"></a>
                                                                    <figcaption>
                                                                <?php echo $value->album_name; ?>
                                                                    </figcaption>
                                                                </figure>
                                                                <?php
                                                            }
                                                        } else {
                                                            ?>
                                                            <figure class="album-img-div text-center">
                                                                    <a href="javascript:void(0)" class="no_image_avl"><img src="<?php bloginfo('template_directory') ?>/images/no_image_avl.jpeg"></a>
                                                                    <figcaption>
                                                                        <?php echo $image_no; ?>
                                                                    </figcaption>
                                                                </figure>
                                                <?php        }
                                                    }
                                                    ?>                                                 
                                                </div> 
                                            </div>                          
                                            <div class="clear"></div>        
                                        </div> 
                                        <!--album part -->
                                    </div>
                                    <!--col-sm-6 -->

                                    <div class="col-sm-6">
                                        <div class="Favourite-Part2">                                    
                                            <h3 class="Pro-h3 col-sm-12"><span>MAIN RESIDENCE</span></h3>   
                                            <div class="padding-top-bottom">
                                                <div class="form-group"> 
                                                    <label for="inputEmail3" class="col-sm-5 BasicInfo-label">COUNTRY :</label>
                                                    <label for="inputEmail3" class="col-sm-7 BasicInfo-label para-left"><?php echo $country; ?></label>
                                                </div>
                                                <div class="form-group"> 
                                                    <label for="inputEmail3" class="col-sm-5 BasicInfo-label">REGIO :</label>
                                                    <label for="inputEmail3" class="col-sm-7 BasicInfo-label para-left"><?php echo $district1; ?></label>
                                                </div>
                                                <div class="form-group"> 
                                                    <label for="inputEmail3" class="col-sm-5 BasicInfo-label">NEXT CITY :</label>
                                                    <label for="inputEmail3" class="col-sm-7 BasicInfo-label para-left"><?php echo $city1; ?></label>
                                                </div>
                                                <div class="form-group"> 
                                                    <label for="inputEmail3" class="col-sm-5 BasicInfo-label">Frequently in :</label>
                                                    <label for="inputEmail3" class="col-sm-7 BasicInfo-label para-left"><?php echo $get_user_info['frequently_in']['0']; ?></label>
                                                </div>
                                            </div>
                                        </div>
                                        <!--main residence -->

                                        <div class="Favourite-Part3 match_sec_img">                                
                                            <!--<div class=""> 
                                                <h3 class="Pro-h3 col-sm-12 col-md-12"><span>MATCHES</span></h3>
                                            </div>
                                            <div class="padding-top-bottom">
                                                <div class="row">
                                                    <?php
                                                        $alldata = $wpdb->get_results("SELECT * FROM wp_user_profile WHERE reciver_id='" . $user_id . "' AND add_fav='1' AND fav_status='0' OR fav_status='1' ORDER BY id DESC LIMIT 3");
                                                        if (!empty($alldata)) {
                                                            foreach ($alldata as $mat_value) {
                                                                $senderID = $mat_value->sender_id;
                                                                $user_name = get_user_meta($senderID, 'nickname', true);

                                                                $userUrl = get_permalink(305) . '&user_id=' . $senderID;
                                                                #.. sender user pic data
                                                                $his_pick = get_user_meta($senderID, 'his_pick', true);
                                                                $her_pick = get_user_meta($senderID, 'her_pick', true);
                                                                if (!empty($her_pick)) {
                                                                    $pick_name = $her_pick;
                                                                } else {
                                                                    $pick_name = $his_pick;
                                                                }
                                                                $pick_source = $upload_dir['baseurl'] . '/profilepick_' . $senderID . '/' . $pick_name;
                                                    ?>
                                                            <div class="col-xs-4">
                                                                <img src="<?php echo $pick_source; ?>" height="45" width="52">
                                                                <label class="matchName"><?php echo $user_name; ?></label>
                                                            </div> 
                                                        <?php
                                                        }
                                                    } else {
                                                        echo 'No MATCHES';
                                                    }
                                                    ?>  
                                                </div>
                                            </div>-->
                                        </div>
                                        <!-- end second residence -->

                                        <div class="Favourite-Part">
                                            <div class=""> 
                                                <h3 class="Pro-h4"><span>FAVOURITE QUOTE</span></h3>
                                            </div>
                                            <div class="col-sm-12 col-md-12 no-padding"> 
                                                    <?php echo $fav_quote_content; ?>
                                            </div>
                                        </div>
                                        <!--fav -section-->
                                    </div>
                                    <!--col-sm-6 -->
                                </div>
                                <!--row-->

                                <?php
                                    $results = $wpdb->get_var("SELECT img_url FROM bg_images");
                                    $imgPath = $results;
                                ?>
                                <div class="row">
                                    <div class="col-sm-12 prelative">
                                        <h3 class="Pro-h3 newIntro"><span>INTRODUCTION</span></h3>   
                                        <div  id="content-1"> 
                                            <div class="Introduction-Part" style="background-size: 100% 100%; background-image: url('<?php echo $imgPath; ?>');" id ="introduction_part">
                                                <div class="padding-para-div" id="intro_data"> 
                                                    <?php echo $describe; ?>
                                                </div>                                        
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- row -->
                            </div> 
                            <!--col-sm-6 -->

                            <div class="col-sm-3" id="left_section">                                   
                                <div class="perfect-match-Part cus_scrolln">
                                    <div class=""> 
                                        <h3 class="opm-h3 col-sm-12 col-md-12"><span>PERFECT MATCH</span></h3>
                                    </div>
                                    <div style="display:<?php echo ($reg3_txtshe == '') ? 'none' : 'block'; ?>" class="opm-text-box-roundsec">
                                        <p><strong>SHE :</strong><?php echo $reg3_txtshe; ?></p>
                                    </div>
                                    <div style="display:<?php echo ($reg3_txthe == '') ? 'none' : 'block'; ?>" class="opm-text-box-roundsec">
                                        <p><strong>HE :</strong><?php echo $reg3_txthe; ?></p> 
                                    </div>
                                    <div style="display:<?php echo ($reg3_txtboth == '') ? 'none' : 'block'; ?>" class="opm-text-box-roundsec">
                                        <p><strong>BOTH :</strong><?php echo $reg3_txtboth; ?></p> 
                                    </div>
                                </div>
                                <!--perfect match-->

                                <!--hidden request-->
                                <?php
                                    $userid = $user_id;
                                    $arr = array();
                                    $subId = array();
                                    $status = array();
                                    $myrows = $wpdb->get_results("SELECT *  FROM `wp_erotics_preference` where `userid` = $userid AND `type` = 1 order by like_dislike_attr_id" );
                                    foreach ($myrows as $result) {
                                        $arr[] = $result->like_dislike_attr_id;
                                        $subId[] = $result->subject_id;
                                        $status[] = unserialize($result->status);
                                    }                                                       

                                    $myrows1 = $wpdb->get_results("SELECT *  FROM `wp_erotics_preference` where `userid` = $userid AND `type` = 2 order by like_dislike_attr_id" );
                                    $arr1 = array();
                                    $subId1 = array();
                                    $status1 = array();

                                    foreach ($myrows1 as $result) {
                                        $arr1[] = $result->like_dislike_attr_id;
                                        $subId1[] = $result->subject_id;
                                        $status1[] = unserialize($result->status);
                                    }

                                    $arg = array('posts_per_page' => -1, 'offset' => 1, 'category' => 14, 'post_type' => 'eroticoption', 'order' => 'asc');
                                    $myposts = get_posts($arg);

                                    $myAllposts = array();
                                    $ctagoryid = array("3", "4", "5");

                                    foreach ($ctagoryid as $values) {
                                        $args = array('posts_per_page' => -1, 'offset' => 1, 'category' => $values->ID, 'post_type' => 'erotics', 'order' => 'asc', 'show_count' => 0);
                                        $myAllposts = get_posts($args);
                                    }

                                    $content = array();
                                    foreach ($subId as $key => $value) 
                                    {                          
                                        $content['subject'][] = get_the_title($value); 
                                        $content['subjectID'][] =  $value;                   
                                    }


                                    foreach ($arr as $key => $value1) {
                                        foreach ($myposts as $value) {
                                            if ($value1 == $value->ID) {
                                                $content['desc'][] = $value->post_title;
                                                if(!(in_array($value->ID, $content['id'])))
                                                {
                                                    $content['id'][] = $value->ID;
                                                } 
                                            }
                                        }
                                    }

                                    $content1 = array();
                                    foreach ($subId1 as $key => $value) 
                                    {                          
                                        $content1['subject'][] = get_the_title($value);
                                        $content1['subjectID'][] =  $value;                    
                                    }

                                    foreach ($arr1 as $key => $value1) {
                                        foreach ($myposts as $value) {
                                            if ($value1 == $value->ID) {
                                                $content1['desc'][] = $value->post_title;
                                                if(!(in_array($value->ID, $content1['id'])))
                                                {
                                                    $content1['id'][] = $value->ID;
                                                } 
                                            }
                                        }
                                    }
                                    $unique_arr_desc=array_unique($content['desc']);
                                    foreach ($unique_arr_desc as $value) 
                                    {
                                       $unique_arr_dist[]=$value;
                                    }
                                    $unique_arr_desc1=array_unique($content1['desc']);
                                    foreach ($unique_arr_desc1 as $value) 
                                    {
                                       $unique_arr_dist1[]=$value;
                                    }
                                
                                    $eroticDatas = get_user_meta($user_ID, 'erotic_shows_case', true);

                                    // erotic for man
                                    if ($user_type == 'gentleman') {
                                ?>      
                                    <div class="eropic-profile-Part <?php if ($eroticDatas != 'erotic preferences') {
                                        echo 'invalid';
                                    } ?>">     
                                    <?php if ($eroticDatas != 'erotic preferences') {
                                        echo '<span class="grayTrans"></span>';
                                    } ?>          
                                        <h3 class="eropic-h3 no-margin no-padding col-sm-12 col-md-12">
                                            <span class="red">His erotic profile</span>
                                        </h3>                                                                        
                                        <div style="display:block;" id="hide_hidden1">
                                    <?php
                        $count1 = 0;
                        for ($i = 0; $i < count($unique_arr_dist1); $i++) 
                        {
                            $color1 = $content1['id'][$count1];
                            $textColor[$color1]; 
                            $getTileShow = getActiveOrPassviveTitle($wpdb,$userid,$color1, '2');                                                                                                                                
                        ?>              
                        <div class="eropic-box-round" style="background-color:<?php echo ($bgColor1 != "") ? $bgColor1 : $textColor[$color1]; ?>;color:<?php echo ($bgColor1 != "") ? $bgColor1 : $forColor[$color1]; ?>;">
                            <p class="child"><strong class="up-case"><?php echo strtoupper(qtrans_use($q_config['language'],$unique_arr_dist1[$i], true)); ?> :</strong></p>
                            <?php for($j = 0; $j < count($content1['subject']); $j++)
                             {
                            if($content1['desc'][$j]==$unique_arr_dist1[$i])
                            { 
                                $like_dislike_ID = $content1['subjectID'][$j];
                                $text1 = getActiveOrPassvive($wpdb,$userid,$color1,$like_dislike_ID, '2');
                            ?>
                            <p style=""><?php echo qtrans_use($q_config['language'],$content1['subject'][$j], true) .' '. $text1['text']; ?></p>
                            <?php }
                        } ?>
                        </div>
                        <?php
                        $count1++;
                        }
                        ?>
                                        </div>                       
                                    </div>
                                <?php } ?>
                                <!--===end gemtleman ====-->

                                <!--===start ladies ====-->
                                <?php
                                if ($user_type == 'ladies') {                                                    
                                    $userid = $user_id;
                                    $arr = array();
                                    $subId = array();
                                    $status = array();
                                    $myrows = $wpdb->get_results("SELECT *  FROM `wp_erotics_preference` where `userid` = $userid AND `type` = 1 order by like_dislike_attr_id" );
                                    foreach ($myrows as $result) {
                                        $arr[] = $result->like_dislike_attr_id;
                                        $subId[] = $result->subject_id;
                                        $status[] = unserialize($result->status);
                                    }                                                       

                                    $myrows1 = $wpdb->get_results("SELECT *  FROM `wp_erotics_preference` where `userid` = $userid AND `type` = 2 order by like_dislike_attr_id" );
                                    $arr1 = array();
                                    $subId1 = array();
                                    $status1 = array();

                                    foreach ($myrows1 as $result) {
                                        $arr1[] = $result->like_dislike_attr_id;
                                        $subId1[] = $result->subject_id;
                                        $status1[] = unserialize($result->status);
                                    }

                                    $arg = array('posts_per_page' => -1, 'offset' => 1, 'category' => 14, 'post_type' => 'eroticoption', 'order' => 'asc');
                                    $myposts = get_posts($arg);

                                    $myAllposts = array();
                                    $ctagoryid = array("3", "4", "5");

                                    foreach ($ctagoryid as $values) {
                                        $args = array('posts_per_page' => -1, 'offset' => 1, 'category' => $values->ID, 'post_type' => 'erotics', 'order' => 'asc', 'show_count' => 0);
                                        $myAllposts = get_posts($args);
                                    }

                                    $content = array();
                                    foreach ($subId as $key => $value) 
                                    {                          
                                        $content['subject'][] = get_the_title($value); 
                                        $content['subjectID'][] =  $value;                   
                                    }


                                    foreach ($arr as $key => $value1) {
                                        foreach ($myposts as $value) {
                                            if ($value1 == $value->ID) {
                                                $content['desc'][] = $value->post_title;
                                                if(!(in_array($value->ID, $content['id'])))
                                                {
                                                    $content['id'][] = $value->ID;
                                                } 
                                            }
                                        }
                                    }

                                    $content1 = array();
                                    foreach ($subId1 as $key => $value) 
                                    {                          
                                        $content1['subject'][] = get_the_title($value);
                                        $content1['subjectID'][] =  $value;                    
                                    }

                                    foreach ($arr1 as $key => $value1) {
                                        foreach ($myposts as $value) {
                                            if ($value1 == $value->ID) {
                                                $content1['desc'][] = $value->post_title;
                                                if(!(in_array($value->ID, $content1['id'])))
                                                {
                                                    $content1['id'][] = $value->ID;
                                                } 
                                            }
                                        }
                                    }
                                    $unique_arr_desc=array_unique($content['desc']);
                                    foreach ($unique_arr_desc as $value) 
                                    {
                                       $unique_arr_dist[]=$value;
                                    }
                                    $unique_arr_desc1=array_unique($content1['desc']);
                                    foreach ($unique_arr_desc1 as $value) 
                                    {
                                       $unique_arr_dist1[]=$value;
                                    }
                                ?>                                
                                <div class="eropic-profile-Part <?php if ($eroticDatas != 'erotic preferences') {
                                                        echo 'invalid';
                                                    } ?> ">    
                                <?php if ($eroticDatas != 'erotic preferences') {
                                    echo '<span class="grayTrans"></span>';
                                } ?>                        
                                <h3 class="eropic-h3 no-margin no-padding col-sm-12 col-md-12">
                                    <span>Her erotic profile</span>                               
                                </h3>                                                                       
                                <div style="display:block;" id="hide_hidden1">
                                    <?php
                                    $count = 0;
                                    for ($i = 0; $i < count($unique_arr_dist); $i++) {
                                        $color = $content['id'][$count];
                                        $getTileShow = getActiveOrPassviveTitle($wpdb,$userid,$color, '1');
                                        ?>              
                                        <div class="eropic-box-round " style="background-color:<?php echo ($bgColor != "") ? $bgColor : $textColor[$color]; ?>;color:<?php echo ($bgColor != "") ? $bgColor : $forColor[$color]; ?>;">
                                            <p><strong class="up-case"><?php echo strtoupper(qtrans_use($q_config['language'],$unique_arr_dist[$i], true)); ?> :</strong></p>
                                            <?php for($j = 0; $j < count($content['subject']); $j++)
                                            {
                                                if($content['desc'][$j]==$unique_arr_dist[$i])
                                                { 
                                                    $like_dislike_ID = $content['subjectID'][$j];
                                                    $text = getActiveOrPassvive($wpdb,$userid,$color,$like_dislike_ID, '1');
                                                ?>
                                            <p style=""><?php echo qtrans_use($q_config['language'],$content['subject'][$j], true) .' '. $text['text']; ?></p>
                                            <?php }
                                            } ?>
                                        </div>
                                        <?php $count++; }?>
                                        </div>                            
                                    </div>  
                                                    <?php } // end if ?>                                             
                                <!--===end ladies ====-->
                            </div>
                        </div>
                    </form>
                <?php } else { ?>
                    <h2>User profile is Deactive</h2>
                    <?php } ?>
                    </div>
            </div>

        <?php
        break;
} // switch case
?>	
</div><!-- externally-profile -->
<?php //get_footer(); ?>
