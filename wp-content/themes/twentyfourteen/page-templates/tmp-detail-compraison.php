<?php
/*
 * Template Name: Detail compraison
 * Description: A Page Template with a Membership Level Comparison.
 * Author Name : Sanjay Shinghania
 */

get_header(); 
?>
<?php //require_once('user_profile_header.php'); ?>	
<div class="container">
    <div class="signUpWarp">
        <form class="form-horizontal" role="form" name="form" method="post">
            <div class="row">

                <div class="col-sm-5 col-sm-offset-1 no-padding-right">
                    <div class="premium-table-text-top"><h3>Comparison of three types of Hedonia.ch membership</h3></div>
                    <?php 
                    $result = $wpdb->get_results("select * from wp_admin_portal_setting where portal_setting_type = 'portal setting' ");
                    ?>
                    <table class="table new-add">
                        <tr>
                            <td class="font-bold" style="background: #0C0C0C;">&nbsp;</td>
                            <td class="td-green bg-darkGray"><span class="font-bold">Basic members</span></td>
                            <td class="td-green bg-green"><span class="font-bold">Validated members</span></td>
                            <td class="td-dark-yellow bg-red font-bold">Premium members</td>
                        </tr>

                        <tr>
                            <td class="td-red bg-dark">PROFILE</td>
                            <td class="td-green bg-dark-light"></td>
                            <td class="td-green bg-dark-light" ></td>
                            <td class="td-dark-yellow bg-dark-light"></td>
                        </tr> 
                        <?php /* ?><tr>
                            <td class="td-red">Min. character</td>
                            <td class="td-green text-center"><?php echo $result[0]->basic; ?></td>
                            <td class="td-green text-center" ><?php echo $result[0]->validated; ?></td>
                            <td class="td-dark-yellow text-center"><?php echo $result[0]->premium; ?></td>
                        </tr><?php */ ?>
                        <tr>
                            <td class="td-red"><?php echo $result[0]->portal_setting_text; ?></td>
                            <td class="td-green text-center"><?php echo $result[0]->basic; ?></td>
                            <td class="td-green text-center" ><?php echo $result[0]->validated; ?></td>
                            <td class="td-dark-yellow text-center"><?php echo $result[0]->premium; ?></td>
                        </tr>
                        <?php /* ?><tr>
                            <td class="td-red">Second residence</td>
                            <td class="td-green text-center bg-dark-light1">
                            <?php if($result[2]->basic !=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/cross-icon.png"> 
                            <?php } 
                            else { ?>
                               <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/arrow.png" alt="" />
                            <?php    } ?>
                            </td>
                            <td class="td-green text-center bg-dark-light1"> 
                            <?php if($result[2]->validated !=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/cross-icon.png"> 
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/arrow.png" alt="" />
                             <?php   } ?></td>
                            <td class="td-dark-yellow text-center">
                            <?php if($result[2]->premium !=1) 
                            { ?><img src="<?php echo bloginfo('template_url');?>/page-templates/images/cross-icon.png"> 
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/arrow.png" alt="" />
                             <?php   } ?></td>
                        </tr><?php */ ?>                                                      
                            <td class="td-red"><?php echo $result[1]->portal_setting_text; ?></td>
                            <td class="td-green text-center bg-dark-light1">
                            <?php if($result[1]->basic !=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/cross-icon.png"> 
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/arrow.png" alt="" />
                              <?php  } ?></td>
                            <td class="td-green text-center bg-dark-light1">
                            <?php if($result[1]->validated !=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/cross-icon.png"> 
                            <?php } 
                            else { ?>
                               <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/arrow.png" alt="" />
                              <?php  } ?></td>
                            <td class="td-dark-yellow text-center">
                            <?php if($result[1]->premium !=1) 
                            { ?><img src="<?php echo bloginfo('template_url');?>/page-templates/images/cross-icon.png"> 
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/arrow.png" alt="" />
                              <?php  } ?></td>
                        </tr>
                        <tr>
                            <td class="td-red"><?php echo $result[2]->portal_setting_text; ?></td>
                            <td class="td-green text-center bg-dark-light1">
                            <?php if($result[2]->basic !=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/cross-icon.png"> 
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/arrow.png" alt="" />
                              <?php  } ?></td>
                            <td class="td-green text-center bg-dark-light1">
                            <?php if($result[2]->validated !=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/cross-icon.png"> 
                            <?php } 
                            else { ?>
                               <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/arrow.png" alt="" />
                              <?php  } ?></td>
                            <td class="td-dark-yellow text-center">
                            <?php if($result[2]->premium !=1) 
                            { ?><img src="<?php echo bloginfo('template_url');?>/page-templates/images/cross-icon.png"> 
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/arrow.png" alt="" />
                              <?php  } ?></td>
                        </tr>
                        <tr>
                        <?php 
                        $album = $wpdb->get_results("select * from wp_admin_portal_setting where portal_setting_type = 'album setting'");
                        ?>
                        <tr>
                            <td class="td-red bg-dark">ALBUMS</td>
                            <td class="td-green bg-dark-light"></td>
                            <td class="td-green bg-dark-light" ></td>
                            <td class="td-dark-yellow bg-dark-light"></td>
                        </tr>
                        <tr>
                            <td class="td-red"><?php echo $album[0]->portal_setting_text; ?></td>
                            <td class="td-green text-center"><?php echo $album[0]->basic;?></td>
                            <td class="td-green text-center" ><?php echo $album[0]->validated;?></td>
                            <td class="td-dark-yellow text-center"><?php echo $album[0]->premium;?></td>
                        </tr>
                        <tr>
                            <td class="td-red"><?php echo $album[1]->portal_setting_text; ?></td>
                            <td class="td-green text-center"><?php echo $album[1]->basic;?></td>
                            <td class="td-green text-center" ><?php echo $album[1]->validated;?></td>
                            <td class="td-dark-yellow text-center"><?php echo $album[1]->premium;?></td>
                        </tr>
                        <tr> 
                            <td class="td-red"><?php echo $album[2]->portal_setting_text; ?></td>
                            <td class="td-green text-center bg-dark-light1"><?php echo $album[2]->basic;?>  
                            </td>
                            <td class="td-green text-center bg-dark-light1" ><?php echo $album[2]->validated;?>
                            </td>
                            <td class="td-dark-yellow text-center"><?php echo $album[2]->premium;?></td>
                        </tr>
                        <tr> 
                            <td class="td-red"><?php echo $album[3]->portal_setting_text; ?></td>
                            <td class="td-green text-center bg-dark-light1"><?php echo $album[3]->basic;?></td>
                            <td class="td-green text-center bg-dark-light1" ><?php echo $album[3]->validated;?></td>
                            <td class="td-dark-yellow text-center"><?php echo $album[3]->premium;?></td>
                        </tr>
                        <?php 
                        $visibilty = $wpdb->get_results("select * from wp_admin_portal_setting where portal_setting_type = 'visibilty setting' ");
                        ?>
                        <tr>
                            <td class="td-red bg-dark">VISIBILITY</td>
                            <td class="td-green bg-dark-light"></td>
                            <td class="td-green bg-dark-light" ></td>
                            <td class="td-dark-yellow bg-dark-light"></td>
                        </tr>
                        <tr>
                            <td class="td-red"><?php echo $visibilty[0]->portal_setting_text; ?></td>
                            <td class="td-green text-center"><?php echo $visibilty[0]->basic;?> </td>
                            <td class="td-green text-center" ><?php echo $visibilty[0]->validated;?></td>
                            <td class="td-dark-yellow text-center"><?php echo $visibilty[0]->premium;?></td>
                        </tr>
                        <tr>
                            <td class="td-red"><?php echo $visibilty[1]->portal_setting_text; ?></td>
                            <td class="td-green text-center"><?php echo $visibilty[1]->basic;?> </td>
                            <td class="td-green text-center" ><?php echo $visibilty[1]->validated;?></td>
                            <td class="td-dark-yellow text-center"><?php echo $visibilty[1]->premium;?></td>
                        </tr>
                        <tr>
                            <td class="td-red"><?php echo $visibilty[2]->portal_setting_text; ?></td>
                            <td class="td-green text-center bg-dark-light1"> 
                            <?php if($visibilty[2]->basic!=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/cross-icon.png"> 
                            <?php } 
                            else { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/arrow.png"> 
                           <?php     } ?></td>
                            <td class="td-green text-center" >
                            <?php if($visibilty[2]->validated!=1) 
                            { ?><img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/cross-icon.png" alt="" />
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url');?>/page-templates/images/arrow.png">
                              <?php  } ?></td>
                            <td class="td-dark-yellow text-center">
                            <?php if($visibilty[2]->premium!=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/cross-icon.png" alt="" />
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/arrow.png" alt="" />
                           <?php  } ?></td>
                        </tr>
                        <tr>
                            <td class="td-red"><?php echo $visibilty[3]->portal_setting_text; ?></td>
                            <td class="td-green text-center bg-dark-light1"><?php if($visibilty[3]->basic!=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/cross-icon.png"> 
                            <?php } 
                            else { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/arrow.png"> 
                           <?php     } ?></td>
                            <td class="td-green text-center" >
                            <?php if($visibilty[3]->validated!=1) 
                            { ?><img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/cross-icon.png" alt="" />
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url');?>/page-templates/images/arrow.png">
                              <?php  } ?></td>
                            <td class="td-dark-yellow text-center">
                            <?php if($visibilty[3]->premium!=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/cross-icon.png" alt="" />
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/arrow.png" alt="" />
                           <?php  } ?></td>
                        </tr>
                        <tr>
                            <td class="td-red bg-dark">SEARCH OPTIONS</td>
                            <td class="td-green bg-dark-light"></td>
                            <td class="td-green bg-dark-light" ></td>
                            <td class="td-dark-yellow bg-dark-light"></td>
                        </tr>
                        <?php 
                        $search = $wpdb->get_results("select * from wp_admin_portal_setting where portal_setting_type = 'search setting'");
                        ?>
                        <tr>
                            <td class="td-red"><?php echo $search[0]->portal_setting_text; ?></td>
                            <td class="td-green text-center"><?php if($search[0]->basic!=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/cross-icon.png"> 
                            <?php } 
                            else { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/arrow.png"> 
                           <?php     } ?></td>
                            <td class="td-green text-center" >
                            <?php if($search[0]->validated!=1) 
                            { ?><img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/cross-icon.png" alt="" />
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url');?>/page-templates/images/arrow.png">
                              <?php  } ?></td>
                            <td class="td-dark-yellow text-center">
                            <?php if($search[0]->premium!=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/cross-icon.png" alt="" />
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/arrow.png" alt="" />
                           <?php  } ?></td>
                        </tr>
                        <tr>
                            <td class="td-red"><?php echo $search[1]->portal_setting_text; ?></td>
                            <td class="td-green text-center bg-dark-light1"><?php if($search[1]->basic!=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/cross-icon.png"> 
                            <?php } 
                            else { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/arrow.png"> 
                           <?php     } ?></td>
                            <td class="td-green text-center" >
                            <?php if($search[1]->validated!=1) 
                            { ?><img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/cross-icon.png" alt="" />
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url');?>/page-templates/images/arrow.png">
                              <?php  } ?></td>
                            <td class="td-dark-yellow text-center">
                            <?php if($search[1]->premium!=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/cross-icon.png" alt="" />
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/arrow.png" alt="" />
                           <?php  } ?></td>
                        </tr>
                        <tr>
                            <td class="td-red"><?php echo $search[2]->portal_setting_text; ?></td>
                            <td class="td-green text-center bg-dark-light1"><?php if($search[2]->basic!=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/cross-icon.png"> 
                            <?php } 
                            else { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/arrow.png"> 
                           <?php     } ?></td>
                            <td class="td-green text-center" >
                            <?php if($search[2]->validated!=1) 
                            { ?><img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/cross-icon.png" alt="" />
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url');?>/page-templates/images/arrow.png">
                              <?php  } ?></td>
                            <td class="td-dark-yellow text-center">
                            <?php if($search[2]->premium!=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/cross-icon.png" alt="" />
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/arrow.png" alt="" />
                           <?php  } ?></td>
                        </tr>
                        <tr>
                            <td class="td-red"><?php echo $search[3]->portal_setting_text; ?></td>
                            <td class="td-green text-center bg-dark-light1"><?php if($search[3]->basic!=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/cross-icon.png"> 
                            <?php } 
                            else { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/arrow.png"> 
                           <?php     } ?></td>
                            <td class="td-green text-center" >
                            <?php if($search[3]->validated!=1) 
                            { ?><img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/cross-icon.png" alt="" />
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url');?>/page-templates/images/arrow.png">
                              <?php  } ?></td>
                            <td class="td-dark-yellow text-center">
                            <?php if($search[3]->premium!=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/cross-icon.png" alt="" />
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/arrow.png" alt="" />
                           <?php  } ?></td>
                        </tr>
                        <tr>
                            <td class="td-red"><?php echo $search[4]->portal_setting_text; ?></td>
                            <td class="td-green text-center bg-dark-light1"><?php if($search[4]->basic!=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/cross-icon.png"> 
                            <?php } 
                            else { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/arrow.png"> 
                           <?php     } ?></td>
                            <td class="td-green text-center" >
                            <?php if($search[4]->validated!=1) 
                            { ?><img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/cross-icon.png" alt="" />
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url');?>/page-templates/images/arrow.png">
                              <?php  } ?></td>
                            <td class="td-dark-yellow text-center">
                            <?php if($search[4]->premium!=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/cross-icon.png" alt="" />
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/arrow.png" alt="" />
                           <?php  } ?></td>
                        </tr>
                        <tr>
                            <td class="td-red"><?php echo $search[5]->portal_setting_text; ?></td>
                            <td class="td-green text-center bg-dark-light1">    
                            <?php if($search[5]->basic!=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/cross-icon.png"> 
                            <?php } 
                            else { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/arrow.png"> 
                           <?php     } ?></td>
                            <td class="td-green text-center" >
                            <?php if($search[5]->validated!=1) 
                            { ?><img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/cross-icon.png" alt="" />
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url');?>/page-templates/images/arrow.png">
                              <?php  } ?></td>
                            <td class="td-dark-yellow text-center">
                            <?php if($search[5]->premium!=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/cross-icon.png" alt="" />
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/arrow.png" alt="" />
                           <?php  } ?></td>
                        </tr>
                        <tr>
                            <td class="td-red"><?php echo $search[6]->portal_setting_text; ?></td>
                            <td class="td-green text-center bg-dark-light1"><?php if($search[6]->basic!=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/cross-icon.png"> 
                            <?php } 
                            else { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/arrow.png"> 
                           <?php     } ?></td>
                            <td class="td-green text-center" >
                            <?php if($search[6]->validated!=1) 
                            { ?><img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/cross-icon.png" alt="" />
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url');?>/page-templates/images/arrow.png">
                              <?php  } ?></td>
                            <td class="td-dark-yellow text-center">
                            <?php if($search[6]->premium!=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/cross-icon.png" alt="" />
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/arrow.png" alt="" />
                           <?php  } ?></td>
                        </tr>
                        <tr>
                            <td class="td-red"><?php echo $search[7]->portal_setting_text; ?></td>
                            <td class="td-green text-center bg-dark-light1"><?php if($search[7]->basic!=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/cross-icon.png"> 
                            <?php } 
                            else { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/arrow.png"> 
                           <?php     } ?></td>
                            <td class="td-green text-center" >
                            <?php if($search[7]->validated!=1) 
                            { ?><img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/cross-icon.png" alt="" />
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url');?>/page-templates/images/arrow.png">
                              <?php  } ?></td>
                            <td class="td-dark-yellow text-center">
                            <?php if($search[7]->premium!=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/cross-icon.png" alt="" />
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/arrow.png" alt="" />
                           <?php  } ?></td>
                        </tr>
                        <tr>
                            <td class="td-red"><?php echo $search[8]->portal_setting_text; ?></td>
                            <td class="td-green text-center bg-dark-light1"><?php if($search[8]->basic!=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/cross-icon.png"> 
                            <?php } 
                            else { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/arrow.png"> 
                           <?php     } ?></td>
                            <td class="td-green text-center" >
                            <?php if($search[8]->validated!=1) 
                            { ?><img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/cross-icon.png" alt="" />
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url');?>/page-templates/images/arrow.png">
                              <?php  } ?></td>
                            <td class="td-dark-yellow text-center">
                            <?php if($search[8]->premium!=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/cross-icon.png" alt="" />
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/arrow.png" alt="" />
                           <?php  } ?></td>
                        </tr>
                        <tr>
                            <td class="td-red"><?php echo $search[9]->portal_setting_text; ?></td>
                            <td class="td-green text-center bg-dark-light1"><?php if($search[9]->basic!=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/cross-icon.png"> 
                            <?php } 
                            else { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/arrow.png"> 
                           <?php     } ?></td>
                            <td class="td-green text-center" >
                            <?php if($search[9]->validated!=1) 
                            { ?><img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/cross-icon.png" alt="" />
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url');?>/page-templates/images/arrow.png">
                              <?php  } ?></td>
                            <td class="td-dark-yellow text-center">
                            <?php if($search[9]->premium!=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/cross-icon.png" alt="" />
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/arrow.png" alt="" />
                           <?php  } ?></td>
                        </tr>
                        <tr>
                            <td class="td-red"><?php echo $search[10]->portal_setting_text; ?></td>
                            <td class="td-green text-center bg-dark-light1"><?php if($search[10]->basic!=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/cross-icon.png"> 
                            <?php } 
                            else { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/arrow.png"> 
                           <?php     } ?></td>
                            <td class="td-green text-center" >
                            <?php if($search[10]->validated!=1) 
                            { ?><img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/cross-icon.png" alt="" />
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url');?>/page-templates/images/arrow.png">
                              <?php  } ?></td>
                            <td class="td-dark-yellow text-center">
                            <?php if($search[10]->premium!=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/cross-icon.png" alt="" />
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/arrow.png" alt="" />
                           <?php  } ?></td>
                        </tr>
                        <tr>
                            <td class="td-red"><?php echo $search[11]->portal_setting_text; ?></td>
                            <td class="td-green text-center bg-dark-light1"><?php if($search[11]->basic!=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/cross-icon.png"> 
                            <?php } 
                            else { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/arrow.png"> 
                           <?php     } ?></td>
                            <td class="td-green text-center" >
                            <?php if($search[11]->validated!=1) 
                            { ?><img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/cross-icon.png" alt="" />
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url');?>/page-templates/images/arrow.png">
                              <?php  } ?></td>
                            <td class="td-dark-yellow text-center">
                            <?php if($search[11]->premium!=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/cross-icon.png" alt="" />
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/arrow.png" alt="" />
                           <?php  } ?></td>
                        </tr>
                        <?php 
                            $operational = $wpdb->get_results("select * from wp_admin_portal_setting where portal_setting_type = 'operational setting'");
                        ?>
                        <tr>
                            <td class="td-red bg-dark">OPERATIONAL FEATURES</td>
                            <td class="td-green bg-dark-light"></td>
                            <td class="td-green bg-dark-light" ></td>
                            <td class="td-dark-yellow bg-dark-light"></td>
                        </tr>
                        <tr>
                            <td class="td-red"><?php echo $operational[0]->portal_setting_text; ?></td>
                            <td class="td-green text-center"><?php echo $operational[0]->basic;?></td>
                            <td class="td-green text-center"><?php echo $operational[0]->validated;?></td>
                            <td class="td-dark-yellow text-center"><?php echo $operational[0]->premium;?></td></td>
                        </tr>
                        <tr>
                            <td class="td-red"><?php echo $operational[1]->portal_setting_text; ?></td>
                            <td class="td-green text-center bg-dark-light1"><?php if($operational[1]->basic!=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/cross-icon.png"> 
                            <?php } 
                            else { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/arrow.png"> 
                           <?php     } ?></td>
                            <td class="td-green text-center" >
                            <?php if($operational[1]->validated!=1) 
                            { ?><img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/cross-icon.png" alt="" />
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url');?>/page-templates/images/arrow.png">
                              <?php  } ?></td>
                            <td class="td-dark-yellow text-center">
                            <?php if($operational[1]->premium!=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/cross-icon.png" alt="" />
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/arrow.png" alt="" />
                           <?php  } ?></td>
                        </tr>
                        <tr>
                            <td class="td-red"><?php echo $operational[2]->portal_setting_text; ?></td>
                            <td class="td-green text-center"><?php echo $operational[2]->basic;?></td>
                            <td class="td-green text-center"><?php echo $operational[2]->validated;?></td>
                            <td class="td-dark-yellow text-center"><?php echo $operational[2]->premium;?></td>
                        </tr>
                        <tr>
                            <td class="td-red"><?php echo $operational[3]->portal_setting_text; ?> </td>
                            <td class="td-green text-center bg-dark-light1"><?php if($operational[3]->basic!=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/cross-icon.png"> 
                            <?php } 
                            else { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/arrow.png"> 
                           <?php     } ?></td>
                            <td class="td-green text-center" >
                            <?php if($operational[3]->validated!=1) 
                            { ?><img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/cross-icon.png" alt="" />
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url');?>/page-templates/images/arrow.png">
                              <?php  } ?></td>
                            <td class="td-dark-yellow text-center">
                            <?php if($operational[3]->premium!=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/cross-icon.png" alt="" />
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/arrow.png" alt="" />
                           <?php  } ?></td>
                        </tr>
                        <tr>
                            <td class="td-red"><?php echo $operational[4]->portal_setting_text; ?></td>
                            <td class="td-green text-center bg-dark-light1"><?php if($operational[4]->basic!=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/cross-icon.png"> 
                            <?php } 
                            else { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/arrow.png"> 
                           <?php     } ?></td>
                            <td class="td-green text-center" >
                            <?php if($operational[4]->validated!=1) 
                            { ?><img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/cross-icon.png" alt="" />
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url');?>/page-templates/images/arrow.png">
                              <?php  } ?></td>
                            <td class="td-dark-yellow text-center">
                            <?php if($operational[4]->premium!=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/cross-icon.png" alt="" />
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/arrow.png" alt="" />
                           <?php  } ?></td>
                        </tr>
                        <tr>
                            <td class="td-red"><?php echo $operational[5]->portal_setting_text; ?></td>
                            <td class="td-green text-center bg-dark-light1"><?php if($operational[5]->basic!=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/cross-icon.png"> 
                            <?php } 
                            else { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/arrow.png"> 
                           <?php     } ?></td>
                            <td class="td-green text-center" >
                            <?php if($operational[5]->validated!=1) 
                            { ?><img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/cross-icon.png" alt="" />
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url');?>/page-templates/images/arrow.png">
                              <?php  } ?></td>
                            <td class="td-dark-yellow text-center">
                            <?php if($operational[5]->premium!=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/cross-icon.png" alt="" />
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/arrow.png" alt="" />
                           <?php  } ?></td>
                        </tr>
                        <tr>
                            <td class="td-red"><?php echo $operational[6]->portal_setting_text; ?></td>
                            <td class="td-green text-center bg-dark-light1"><?php if($operational[6]->basic!=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/cross-icon.png"> 
                            <?php } 
                            else { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/arrow.png"> 
                           <?php     } ?></td>
                            <td class="td-green text-center" >
                            <?php if($operational[6]->validated!=1) 
                            { ?><img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/cross-icon.png" alt="" />
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url');?>/page-templates/images/arrow.png">
                              <?php  } ?></td>
                            <td class="td-dark-yellow text-center">
                            <?php if($operational[6]->premium!=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/cross-icon.png" alt="" />
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/arrow.png" alt="" />
                           <?php  } ?></td>
                        </tr>
                        <tr>
                            <td class="td-red"><?php echo $operational[7]->portal_setting_text; ?></td>
                            <td class="td-green text-center bg-dark-light1"><?php if($operational[7]->basic!=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/cross-icon.png"> 
                            <?php } 
                            else { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/arrow.png"> 
                           <?php     } ?></td>
                            <td class="td-green text-center" >
                            <?php if($operational[7]->validated!=1) 
                            { ?><img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/cross-icon.png" alt="" />
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url');?>/page-templates/images/arrow.png">
                              <?php  } ?></td>
                            <td class="td-dark-yellow text-center">
                            <?php if($operational[7]->premium!=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/cross-icon.png" alt="" />
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/arrow.png" alt="" />
                           <?php  } ?></td>
                        </tr>
                        <tr>
                            <td class="td-red"><?php echo $operational[8]->portal_setting_text; ?></td>
                            <td class="td-green text-center bg-dark-light1"><?php if($operational[8]->basic!=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/cross-icon.png"> 
                            <?php } 
                            else { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/arrow.png"> 
                           <?php     } ?></td>
                            <td class="td-green text-center" >
                            <?php if($operational[8]->validated!=1) 
                            { ?><img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/cross-icon.png" alt="" />
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url');?>/page-templates/images/arrow.png">
                              <?php  } ?></td>
                            <td class="td-dark-yellow text-center">
                            <?php if($operational[8]->premium!=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/cross-icon.png" alt="" />
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/arrow.png" alt="" />
                           <?php  } ?></td>
                        </tr>

                        <tr>
                            <td class="td-red"><?php echo $operational[9]->portal_setting_text; ?></td>
                            <td class="td-green text-center bg-dark-light1"><?php if($operational[9]->basic!=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/cross-icon.png"> 
                            <?php } 
                            else { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/arrow.png"> 
                           <?php     } ?></td>
                            <td class="td-green text-center" >
                            <?php if($operational[9]->validated!=1) 
                            { ?><img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/cross-icon.png" alt="" />
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url');?>/page-templates/images/arrow.png">
                              <?php  } ?></td>
                            <td class="td-dark-yellow text-center">
                            <?php if($operational[9]->premium!=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/cross-icon.png" alt="" />
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/arrow.png" alt="" />
                           <?php  } ?></td>
                        </tr>
                        <tr>
                            <td class="td-red"><?php echo $operational[10]->portal_setting_text; ?></td>
                            <td class="td-green text-center bg-dark-light1"><?php if($operational[10]->basic!=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/cross-icon.png"> 
                            <?php } 
                            else { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/arrow.png"> 
                           <?php     } ?></td>
                            <td class="td-green text-center" >
                            <?php if($operational[10]->validated!=1) 
                            { ?><img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/cross-icon.png" alt="" />
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url');?>/page-templates/images/arrow.png">
                              <?php  } ?></td>
                            <td class="td-dark-yellow text-center">
                            <?php if($operational[10]->premium!=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/cross-icon.png" alt="" />
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/arrow.png" alt="" />
                           <?php  } ?></td>
                        </tr>
                        <tr>
                            <td class="td-red"><?php echo $operational[11]->portal_setting_text; ?></td>
                            <td class="td-green text-center bg-dark-light1"><?php if($operational[11]->basic!=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/cross-icon.png"> 
                            <?php } 
                            else { ?>
                            <img src="<?php echo bloginfo('template_url');?>/page-templates/images/arrow.png"> 
                           <?php     } ?></td>
                            <td class="td-green text-center" >
                            <?php if($operational[11]->validated!=1) 
                            { ?><img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/cross-icon.png" alt="" />
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url');?>/page-templates/images/arrow.png">
                              <?php  } ?></td>
                            <td class="td-dark-yellow text-center">
                            <?php if($operational[11]->premium!=1) 
                            { ?>
                            <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/cross-icon.png" alt="" />
                            <?php } 
                            else { ?>
                                <img src="<?php echo bloginfo('template_url'); ?>/page-templates/images/arrow.png" alt="" />
                           <?php  } ?></td>
                        </tr>
                        <tr>
                            <td class="td-red"><?php echo $operational[12]->portal_setting_text; ?></td>
                            <td class="td-green text-center"><?php  
                            echo $operational[12]->basic ;?>
                            </td>
                            <td class="td-green text-center"><?php echo $operational[12]->validated;?></td>
                            <td class="td-dark-yellow text-center">
                            <?php echo $operational[12]->premium;?></td>
                        </tr> 
                        <tr class="bottomLine">
                            <td>monthly fee</td>
                            <td>free membership</td>
                            <td>free membership</td>
                            <td class="text-center">25 € <br> 97 €<small> (for 6 months)</small></td>
                        </tr>
                    </table>
                    <div class="text-center">
                        <a id="wp-submit" class="btn btn-success btn-sm signupBtn"  href="<?php echo get_permalink(45); ?>" >Sign Up</a>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="detailComp addSpace">
                        <div class="premium-text-h3">
                            <h3 class="red">Premium membership</h3>
                        </div>
                        <div class="mapAll">           
                            <figure>
                                <img width="63" alt="" src="<?php echo bloginfo('template_url'); ?>/page-templates/images/map.png">
                            </figure>
                            <article>
                                <h5 class="red">MEET HEDONISTIC MEMBERS FROM ALL OVER EUROPE</h5> 
                                <p>Only profiles of premium members can be also seen in other European countries.</p>
                            </article>
                        </div>
                        <div class="premium-plus red"><a href="#">+</a></div>
                        <div class="premium-part-main">
                            <div class="premium-part">                                        
                                <h5 class="red">FULL VISIBILITY CONTROL                                             
                                </h5>   
                            </div>

                            <p>Premium members decide which membership groups can even see their profile at all. For example, if you area couple and do not like that many single men also watching your profile, you can make your profile invisible to this group. Or you could keep your profile invisible to all but your own, selected group consisting of only validated couples. You can even make your profile invisible for everybody but members of your personal selection. Furthermore you also can decide in which countries and their districts your profile should be visible (e.g. some couples do not like to be seen by members from their home district).</p>
                            <p>Independently from the visibility of your profile, you can separately hide some of your erotic preferences. In this case, your profile will be visible according to your settings&mdash;as described above&mdash;but some of your erotic preferences will not initially be visible to any member unless you specifically decide to show your full erotic spectrum to selected, individual members. This ensures you the highest possible confidentiality for such sensitive and personal information and enables premium members to create a <span class="red">fully individual erotic portal</span></p>  
                                                                     
                        </div>
                        <div class="premium-plus red "><a href="#">+</a></div>
                        <div class="premium-part-main ownHome">
                            <div class="premium-part">
                                <h5 class="red">OWN HOMEPAGE</h5>
                            </div>
                           
                            <p>Premium members also have the ability to show their Hedonia.ch profile to people outside Hedonia by sending a special link and a password by mail. Premium members can decide separately which album(s) should be shown to people outside of Hedonia.ch. External visitors following your link have no access to other profiles or to any other parts of Hedonia.ch
                            </p>
                        </div>
                        <div class="premium-plus red"><a href="#">+</a></div>
                        <div class="premium-part-main pullOper">
                            <div class="premium-part">
                                <h5 class="red">FULL OPERATIONAL SPECTRUM</h5>
                            </div> 
                            <p>Premium  members can use all features of Hedonia for interaction with other members: unlimited mail sending, kisses, and adding members to favourites and other operational benefits.<br> Finally only premium members can search based on many countries, many languages, many different erotic preferences and other search criteria.</p>       
                        </div>                     
                    </div>
                </div>

            </div>
        </form>
    </div>

</div>

</div>	


<?php get_footer(); ?>
