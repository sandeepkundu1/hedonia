<?php
/*
 * Template Name: Welcome Premium Member
 * Description: A Page Template display a page for premium member.
 * Author Name : Sanjay Shinghania
 */

get_header(); 
global $wpdb;
?>
<?php 

if(!is_user_logged_in()){
wp_redirect(home_url());
}else{  
$user_type_name =  get_user_meta($current_user->ID ,'member_type', true );
    if($user_type_name == 'couples'){
        $myhomepageid = get_page_link('203');
        $edit_profile_pageid = get_page_link('205');
    }else if($user_type_name == 'ladies'){
        $myhomepageid = get_page_link('433');
        $edit_profile_pageid = get_page_link('435');
    }else if($user_type_name == 'gentleman'){
        $myhomepageid = get_page_link('468');
        $edit_profile_pageid = get_page_link('470');
    }

?>
<div class="welcomeAs">
    <div class="container">                   
        <div class="row">                   
            <div class="col-sm-12">
                <div class="col-sm-3 rightImgSec pr0">
                    <h4 class="red text-center"><?php echo $current_user->user_login; ?></h4>
                    <img class="img-responsive" src="<?php echo bloginfo('template_url'); ?>/images/img4.png" />
                </div>
                <div class="col-sm-6 welcomeMsg pl0 pr0">
                    <h4>Welcome as <span class="red">Premium member</span></h4>
                    <article>
                        <p>We congratulate you being a premium member of Hedonia.ch.</p> 
                        <p>As premium member you can fully utilize our worldwide unique concept, show / hide your profile for different European regions, express your individual erotic preferences in finest graduation and if you like hide them for the majority of members. You have now all tools to create your <strong>fully individual erotic portal</strong>. <br/> Furthermore by use of our powerful search tools including all European countries, many languages, different erotic likes, intension of your contacts you can get in touch with the right hedonistic members from all over Europe.</p>  
                        <p class="mb0">We recommend you first</p>
                        <ul class="listingB">
                            <li><strong>complete your profile</strong> ( perfect match and more..) <img src="<?php echo bloginfo('template_url'); ?>/images/small-arrow.png" alt="" />
                                <i><a href="<?php echo $myhomepageid; ?>">my profile </a> / <a href="<?php echo $edit_profile_pageid; ?>">edit</a></i></li>                                    <li></li>
                            <li><strong>send unlimited mails and attach pictures to your mails</strong></li>
                            <li><strong>define your individual contact / visibility area in all over Europe <img src="<?php echo bloginfo('template_url'); ?>/images/small-arrow.png" alt="" />
                                </strong> <i>control panel / visibility</i></li> 
                            <li><strong>define your perfect match <img src="<?php echo bloginfo('template_url'); ?>/images/small-arrow.png" alt="" />
                                </strong> <i><a href="<?php echo $myhomepageid; ?>">my profile </a> / <a href="<?php echo $edit_profile_pageid; ?>">edit</a></i></li> 
                            <li><strong>use additional albums and lock them individually for member groups <img src="<?php echo bloginfo('template_url'); ?>/images/small-arrow.png" alt="" />
                                </strong> <i>in my control panel</i></li>
                            <li><strong>create your external link <img src="<?php echo bloginfo('template_url'); ?>/images/small-arrow.png" alt="" />
                                </strong> <i>in my control panel</i></li> 
                            <li><strong>hide some of your erotic preferences <img src="<?php echo bloginfo('template_url'); ?>/images/small-arrow.png" alt="" />
                                </strong> <i>in my control panel / <a href="<?php echo get_page_link('245'); ?>">erotic preferences</a></i></li>
                            <li><strong>use our powerful search tools across Europe using many criteria</strong> <img src="<?php echo bloginfo('template_url'); ?>/images/small-arrow.png" alt="" />
                                <i>search</i></li> 
                            <li><strong>use speed dating and travel agenda possibilities</strong> 
                            <img src="<?php echo bloginfo('template_url'); ?>/images/small-arrow.png" alt="" />
                            <i><a href="<?php echo $myhomepageid; ?>">in my profile </a></i></li>
                        </ul>
                        <p>We wish you pleasant contacts with other Hedonia members. <br/> If you need any support we would be pleased to help you. <img src="<?php echo bloginfo('template_url'); ?>/images/small-arrow.png" alt="" />
                            <a href="#" style="text-decoration: underline">mail admin</a></p>  
                    </article>              
                </div>
                <div class="col-sm-3 pl0 rightFixed">
                    <img class="img-responsive" src="<?php echo bloginfo('template_url'); ?>/images/Reg3lrightimg.jpg" />
                </div>                    
            </div>
        </div>
    </div>
</div>
</div>
<?php } ?>
 <?php get_footer(); ?>

