<?php
/*
 * Template Name: Swiss portal Basic Registration
 * Description: A Page Template with Portal Registration.
 * Author Name : Sanjay Shinghania
 */
get_header();
?>
<script type="text/javascript">
    jQuery(document).ready(function(){
        var count =0;
        jQuery('.checkedAll').click(function(){	
			$(this).each(function () {
				if(this.checked){
					count++;
				}else{
					count--;
				}
			    if(count==3){
			    $('#reg_process').show();
			    }else{ $('#reg_process').hide(); }
			});			
        });
    });
</script>
<div id="main-content" class="main-content">
	<div class="signUpWarp">
		<div class="container">
			<div class="row">
				<div class="signup" id ="chedonia" >
					<div class="col-sm-3 text-center col-sm-offset-1">
						<?php
							$srcs = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'full' );
						?>
						<img class="Reg1SideImg" src="<?php echo $srcs[0]; ?>">
					</div>
					<div class="col-sm-8">
						<div class="sign-up-rigth">
							<h3 class="sign-up-h3">SIGN-UP FOR HEDONIA. CH</h3>
							<p>Hedonia is developed as an erotic portal following the traces of Epicurus in a hedonistic way. Consequently we are only interested hosting members in our community who agree with our <a href="<?php echo get_permalink(370);  ?>"><span>philosophy and code of behaviour </span></a>.</p>
							<div class="separate-text-chk">
								<div class="text">
									<p>I (we) have read and accept these</p>
								</div>
								<div class="chk">
									<input type="checkbox" class="checkbox_cus checkedAll " id ="accept1" value ="0" name ="checkme" required="">
									<label class="checkbox_label6" for="accept1"></label>
								</div>
							</div>
							<div class="separate-text-chk">
								<div class="text">
									<p>I (we) am/are over 18 years of age</p>
								</div>
								<div class="chk">
									<input type="checkbox" class="checkbox_cus checkedAll" name="married" id="accept2" value ="0" required="">
									<label class="checkbox_label6" for="accept2"></label>
								</div>
							</div>
							<div class="separate-text-chk">
								<div class="text">
									<p>I (we) accept <a href="<?php echo get_permalink( 335 ); ?>"><span>commercial conditions</span></a> of Hedonia</p>
								</div>
								<div class="chk">
									<input type="checkbox" class="checkbox_cus checkedAll" name="married" id ="accept3" value="0" required="">
									<label class="checkbox_label6" for="accept3"></label>
								</div>
							</div>
							<div id="chekedAll" style="color:red; display:none;">Please select all fields</div>
						</div>
						<div id ="reg_process" style="display:none">
							<div class="sign-couple">
								<a class="sign-couple" href="<?php echo get_permalink(7); ?>&type=couples"><span class="sign-couple-text1">Sign-up for couples</span> 
								<span class="sign-couple-arrow1"></span> </a>
							</div>
							<div class="sign-couple">
								<a class="sign-ladies" href="<?php echo get_permalink(7); ?>&type=ladies"><span class="sign-couple-text2">Sign-up for ladies</span> 
								<span class="sign-ladies-arrow2"></span></a>
							</div>
							<div class="sign-couple">
								<a class="sign-gentlemen" href="<?php echo get_permalink(7); ?>&type=gentleman"><span class="sign-couple-text3">Sign-up for gentlemen</span> <span class="sign-ladies-arrow3"></span></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div><!-- #main-content -->
</div>
    
<?php get_footer(); ?>