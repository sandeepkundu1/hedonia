<?php
/*
 * Template Name: Intension And Rooms 
 * Description: A Page Template show the user Intension And Rooms.
 * Author Name : Sanjay Shinghania
 */

get_header();
global $wpdb;
$user_id = get_current_user_id();
$data = get_user_meta($user_id);
$tableName = $wpdb->prefix . 'int_rooms';
$instData = $wpdb->get_row("SELECT * FROM $tableName WHERE user_id='" . $user_id . "'");

$intArr = unserialize($instData->int_data);
//=======
$roomArr = unserialize($instData->room_data);
?>
<?php
if (is_user_logged_in()) {
    ?>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            //Intension
            jQuery('#instension').click(function () {
                var userID = jQuery("#userid").val();
                var type = jQuery("#int_type").val();
                var intfinal = [];
                jQuery('.int_check:checked').each(function () {
                    var values = jQuery(this).val();
                    intfinal.push(values);
                });
                jQuery(".loding").css("display", "block");
                jQuery.ajax({
                    url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
                    data: 'action=my_action_intension&user_id=' + userID + '&data=' + intfinal + '&type=' + type,
                    type: 'POST',
                    dataType: 'html',
                    success: function (response) {
                        jQuery("#intensionMsg").html(response);
                        jQuery("#intensionMsg").addClass('alert alert-success');
                        jQuery("#intensionMsg").show();
                        setTimeout(function () {
                            jQuery('#intensionMsg').fadeOut(10000);
                        }, 10000);
                        setTimeout(function () {
                            location.reload();
                        }, 3000);
                    },
                    complete: function ()
                    {
                        jQuery(".loding").css("display", "none");
                    }
                });
            });
            // Playrooms
            jQuery('#rooms').click(function () {
                var userID = jQuery("#userid").val();
                var type = jQuery("#room_type").val();
                var roomfinal = [];
                jQuery('.room_check:checked').each(function () {
                    var values = jQuery(this).val();
                    roomfinal.push(values);
                });
                jQuery(".loding1").css("display", "block");
                jQuery.ajax({
                    url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
                    data: 'action=my_action_intension&user_id=' + userID + '&data=' + roomfinal + '&type=' + type,
                    type: 'POST',
                    dataType: 'html',
                    success: function (response) {
                        jQuery("#playroomMsg").html(response);
                        jQuery("#playroomMsg").addClass('alert alert-success');
                        jQuery("#playroomMsg").show();
                        setTimeout(function () {
                            jQuery('#playroomMsg').fadeOut(2000);
                        }, 3000);
                        setTimeout(function () {
                            location.reload();
                        }, 3000);
                    },
                    complete: function ()
                    {
                        jQuery(".loding1").css("display", "none");
                    }
                });
            });
        });
    </script>
    <div div class="intensionCont">
        <article id="post-<?php echo get_the_ID(); ?>" <?php post_class(); ?>>
            <?php
            echo get_the_title('<header class="entry-header"><h1 class="entry-title">', '</h1></header><!-- .entry-header -->');
            ?>
            <div class="entry-content profiel-staus">
                <div class="panel-body">
                    <h1 class="home-title">INTENSION</h1>                   
                        <form method="POST" action="" onsubmit="return false;">
                          <div class="row">                                     
                                <?php
                                $args = array(
                                    'post_type' => 'rooms',
                                    'tax_query' => array(
                                        array(
                                            'taxonomy' => 'int_rooms',
                                            'field' => 'term_id',
                                            'terms' => 12)
                                    ),
                                    'order' => 'ASC'
                                );
                                $my_query = new WP_Query($args);
                                if ($my_query->have_posts()) {
                                    while ($my_query->have_posts()) : $my_query->the_post();
                                        ?>    
                                        <div class="col-lg-4">
                                            <div class="input-group panel panel-default">
                                            <div class="row">
                                            <div class="col-sm-10">
                                                <?php the_title(); ?>
                                            </div>
                                                 <div class="col-sm-2">
                                                    <input type="checkbox" id="int-<?php echo get_the_ID(); ?>" class="int_check checkbox_cus" value="<?php the_ID(); ?>" <?php if (in_array(get_the_ID(), $intArr)) {
                echo 'checked';
            } ?>> 
                                                    <label class="checkbox_label" for="int-<?php echo get_the_ID(); ?>"></label>                                                
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                        <?php
                                    endwhile;
                                }
                                wp_reset_query();
                                ?>
                            </div> <!--first div-->
                             <div class="col-sm-12">
                                <div class="input-group pull-right">    
                                    <input type="hidden" id="int_type" value="intension">                                           
                                    <button type="submit" id="instension" class="btn saveBtn pull-right">Save</button>
                                </div>
                                <div id="intensionMsg" class="successMsg" style="display: none;"></div>
                                <div class="loding" style="display: none;">
                                    <img  width="60px" height="12px" src="<?php echo bloginfo('template_url') ?>/images/ajax-status.gif">
                                </div>
                            </div>
                        </form>                    
                </div> <!-- first panel end -->
                 <p class="text-center">These preferences here will be not shown in your profile.These will be used in search part to find the rights contacts.</p>
                <div class="panel-body">
                     <h1 class="home-title" >PLAYROOM PREFRENCES</h1>
                   
                        <form method="POST" action="" onsubmit="return false;">
                            <div class="row">                                       
                                <?php
                                $args = array(
                                    'post_type' => 'rooms',
                                    'tax_query' => array(
                                        array(
                                            'taxonomy' => 'int_rooms',
                                            'field' => 'term_id',
                                            'terms' => 13)
                                    ),
                                    'order' => 'ASC'
                                );
                                $my_query = new WP_Query($args);
                                if ($my_query->have_posts()) {
                                    while ($my_query->have_posts()) : $my_query->the_post();
                                        ?>    
                                    <div class="col-lg-4">
                                <div class="input-group panel panel-default">

                                    <div class="row">
                                        <div class="col-sm-10">
                                            <?php the_title(); ?>
                                        </div>
                                        <div class="col-sm-2">
                                                    <input type="checkbox" id="room-<?php echo get_the_ID(); ?>" class="room_check checkbox_cus" value="<?php the_ID(); ?>" <?php if (in_array(get_the_ID(), $roomArr)) {
                echo 'checked';
            } ?>> <label class="checkbox_label" for="room-<?php echo get_the_ID(); ?>"></label>   
                                               
                                            </div>
                                        </div>
                                </div>
                                    </div>
                                        <?php
                                    endwhile;
                                }
                                wp_reset_query();
                                ?>
                            </div> <!--first div-->                                     
                            <div class="col-sm-12">
                                <div class="input-group pull-right">
                                    <input type="hidden" id="room_type" value="rooms">                                              
                                    <button type="submit" id="rooms" class="btn saveBtn pull-right">Save</button>
                                </div>
                                <div id="playroomMsg" class="successMsg" style="display: none;"></div>
                                <div class="loding1" style="display: none;">
                                    <img  width="60px" height="12px" src="<?php echo bloginfo('template_url') ?>/images/ajax-status.gif">
                                </div>
                            </div>
                        </form>
                    
                </div>
                <input type="hidden" name="userID" id="userid" value="<?php echo $user_id; ?>">
                <div class="col-md-6 col-md-offset-2">
                    <div class="parent" style="display:none;">
                        <span class="loader">loading..</span>
                    </div>
                    <div id="update"></div>
                </div>
            </div><!-- .entry-content -->
        </article><!-- #post-## -->             
    </div><!-- #content -->

    <?php
} else {
    wp_redirect(home_url());
}
?>

<?php get_footer(); ?>