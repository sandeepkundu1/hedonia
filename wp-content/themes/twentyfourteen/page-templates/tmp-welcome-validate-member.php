<?php
/*
 * Template Name: Welcome Validated Member
 * Description: A Page Template display a page for validated member.
 * Author Name : Sanjay Shinghania
 */

get_header();   
global $wpdb;


if(!is_user_logged_in()){
wp_redirect(home_url());
}else{  
    $user_type_name =  get_user_meta($current_user->ID ,'member_type', true );
    if($user_type_name == 'couples'){
        $myhomepageid = get_page_link('203');
        $edit_profile_pageid = get_page_link('205');
    }else if($user_type_name == 'ladies'){
        $myhomepageid = get_page_link('433');
        $edit_profile_pageid = get_page_link('435');
    }else if($user_type_name == 'gentleman'){
        $myhomepageid = get_page_link('468');
        $edit_profile_pageid = get_page_link('470');
    }
?>
<div class="container-fluid">         
    <div class="happyWrap">                
        <div class="container">                
            <div class="row">                
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="congBox">
                                <h4 class="white"><?php echo $current_user->user_login; ?> <img src="<?php echo bloginfo('template_url'); ?>/images/tick-green.png" alt=""/></h4>
                                <div class="graypanal">
                                    <p>We congratulate you being a validated member.</p>
                                    <p>As validated member please go first to "<em class="red"><a class="red" href="<?php echo $myhomepageid; ?>">my profile</a></em>" <img src="<?php echo bloginfo('template_url'); ?>/images/white-arrow.png" alt="" />
                                        "<em class="red"><b><a class="red" href="<?php echo $edit_profile_pageid; ?>">edit my profile</a></b></em>" and complete your profile.</p>
                                    <p>As validated member we recommend you to create your  erotic profile</p>
                                    <p class="rArrow">go to your <b class="red">erotic profile</b> in "my control panel"</p>

                                    <p>You can also make your profile invisible in some regions of your country</p>
                                    <p class="rArrow">go to your <b class="red">control visibility</b> in "my control panel"</p>

                                    <p>You have now the possibility to use another album and place more pictures in your albums</p>
                                    <p class="rArrow">go to your <b class="red">albums / pictures</b> in "my control panel"</p>

                                    <p>To be reached by the right members via search results, please fill additional information for our search engine about your intensions and playroom wishes</p>
                                    <p class="rArrow">go to your <b class="red">intensions / rooms</b> in "my control panel"</p>

                                    <p>You can also already place dating ads</p>
                                    <p class="rArrow"><b>go to <span class="red">dating ads</span> in the bar</b></p>
                                </div>
                                <div class="graypanal redBorder">
                                    <p>This  exceptional portal with the highest level of safety, confidentiality, and privacy is designed for premium members. Only premium members can enjoy all the unique benefits of our portal.</p>
                                    <p>As validated member you immediately can upgrade your membership to  <strong>Premium member</strong> to enjoy the worldwide unique full functionality of our portal.</p></div>
                            </div>
                        </div>                        
                        <div class="col-sm-6">
                            <div class="detailComp validated-detail">
                                <div class="premium-text-h3"><h3 class="red">Premium membership</h3></div>
                                <div class="mapAll">           
                                    <figure>
                                        <img src="<?php echo bloginfo('template_url'); ?>/images/map.png" alt="" width="63"/>
                                    </figure>
                                    <article>
                                        <h5 class="red">MEET HEDONISTIC MEMBERS FROM ALL OVER EUROPE</h5> 
                                        <p>Only profiles of premium members can be also seen in other European countries.</p>
                                    </article>
                                </div> 
                                <div class="premium-plus red"><a href="#">+</a></div>
                                <div class="premium-part-main">
                                    <div class="premium-part">                                        
                                        <h5 class="red">FULL VISIBILITY CONTROL                                             
                                        </h5>   
                                    </div>
                                    <p>Premium members decide which membership groups can even see their profile at all. For example, if you are a couple and do not like that many single men also watching your profile, you can make your profile invisible to this group. Or you could keep your profile invisible to all but your own, selected group consisting of only validated couples. You can even make your profile invisible for everybody but members of your personal selection. Furthermore you also can decide in which countries and their districts your profile should be visible (e.g. some couples do not like to be seen by members from their home district).</p>

                                    <p>Independently from the visibility of your profile, you can separately hide some of your erotic preferences. In this case, your profile will be visible according to your settings—as described above—but some of your erotic preferences will not initially be visible to any member unless you specifically decide to show your full erotic spectrum to selected, individual members. This ensures you the highest possible confidentiality for such sensitive and personal information and enables premium members to create a
                                        <span class="red">fully individual erotic portal</span></p>                                       
                                </div>
                                <div class="premium-plus red "><a href="#">+</a></div>
                                <div class="premium-part-main ownHome">
                                    <div class="premium-part">
                                        <h5 class="red">OWN HOMEPAGE</h5>
                                    </div>
                                    <p>Premium members also have the ability to show their Hedonia.ch profile to people outside Hedonia by sending a special link and a password by mail. Premium members can decide separately which album(s) should be shown to people outside of Hedonia.ch. External visitors following your link have no access to other profiles or to any other parts of Hedonia.ch
                                    </p>
                                </div>
                                <div class="premium-plus red"><a href="#">+</a></div>
                                <div class="premium-part-main pullOper">
                                    <div class="premium-part">
                                        <h5 class="red">FULL OPERATIONAL SPECTRUM</h5>
                                    </div> 
                                    <p>Premium  members can use all features of Hedonia for interaction with other members: unlimited mail sending, kisses, and adding members to favourites and other operational benefits.<br/> Finally only premium members can search based on many countries, many languages, many different erotic preferences and other search criteria.</p>       
                                </div> 
                                <div class="text-center preMembentWrap">
                                <button class=" premium-red_btn" >Become a premium member</button>
                                </div>                    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>	 
<?php } ?>      

 <?php get_footer(); ?>