<?php
/*
 * Template Name: Search Results Members
 * Description: A Page Template with a Change Profile Staus.
 * Author Name : Sanjay Shinghania
 */
session_start();
get_header();

$user_ids = $_SESSION['user_ids'];

//$user_details   =  $_SESSION['user_details'];    
?>

<div class="container-fluid">
    <div class="parent_div">
        <div class="col-sm-12 col-md-12 col-xs-12" id="search-result">
            <div class="row search-all-main">
                <div class="col-md-10 col-md-offset-2 col-sm-10 col-xs-12">
                    <div class="search_form search_all">
                        <h3>Search result</h3>
                        <h4><a href="<?php echo get_page_link(309); ?>">Re-Search</a></h4>
                    </div>
                    <div class="search_cont" id="user-list">
                        <ul class="search_all_user">
                            <?php
                            if (count($user_ids) == 0) {
                                echo $html = 'No Result Match Please search again.';
                            } else {
                                $members = count($user_ids);
                                foreach ($user_ids as $k => $val) {
                                    $she_dob = get_user_meta($val, 'she_dob', true);
                                    $he_dob = get_user_meta($val, 'he_dob', true);
                                    $her_pick = get_user_meta($val, 'her_pick', true);
                                    $district = get_user_meta($val, 'district1', true);
                                    //$dis = implode(',',$district);
                                    $country = get_user_meta($val, 'country', true);
                                    //$con = implode(',',$country);
                                    $his_pick = get_user_meta($val, 'his_pick', true);
                                    $he_age = calculateAge($he_dob);
                                    $she_age = calculateAge($she_dob);
                                    if (!empty($her_pick)) {
                                        $pick_name = $her_pick;
                                    } else {
                                        $pick_name = $his_pick;
                                    }
                                    $pick_source = home_url() . '/wp-content/uploads/profilepick_' . $val . '/' . $pick_name;

                                    //if($he_age >=$user_details['age_his1'] && $he_age <=$user_details['age_his2'] && $she_age >=$user_details['age_her1'] && $she_age <=$user_details['age_her2'])
                                    //{
                                    ?>
                                    <li>
                                        <figure>
                                            <img class="img-responsive" src="<?php echo $pick_source; ?>" style="max-height:120px">
                                        </figure> 
                                        <div class="user_detail_search">
                                            <h3>
                                                <a href="<?php echo get_page_link(305); ?>&user_id=<?php echo $val; ?>">
        <?php echo get_user_meta($val, 'nickname', true); ?>
                                                </a>
                                            </h3>
                                            <p><?php echo $she_age; ?>/<?php echo $he_age; ?></p>
                                            <p><?php echo $district; ?></p>
                                            <p><?php echo $country; ?></p>
                                        </div>
                                    </li>
        <?php
        //}
    }
}
?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php

function calculateAge($date) {
    list($day, $month, $year) = explode("/", $date);
    $year_diff = date("Y") - $year;
    $month_diff = date("m") - $month;
    $day_diff = date("d") - $day;
    if ($day_diff < 0 || $month_diff < 0)
        $year_diff--;
    return $year_diff;
}
?>

<?php get_footer(); ?>