<?php
/*
 * Template Name: Pics & Albums
 */

get_header();
global $wpdb;
global $current_user;
$user_id = get_current_user_id();
$get_login_type = get_user_meta($user_id, 'user_type', true);
$album_setting = $wpdb->get_results("select * from `wp_admin_portal_setting` where portal_setting_type = 'album setting' ");
$lockable_albums = $album_setting[2]->$get_login_type;
$results = $wpdb->get_results("SELECT * FROM user_create_album WHERE user_id = '$user_id'");
$create_album_limit = $album_setting[0]->$get_login_type;
?>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.js"></script>

<!---  Poop box for create new album -->
<div class="modal fade" id="speed_dating" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Album Name</h4>
            </div>
            <div class="modal-body">
                <div id ="speed_msg" class="successMsg"></div>
                <div class="editBoth" style="padding-top: 50px;">
                    <input type="text" id="album_name" data-provide="typeahead" value="" class="form-control">
                    <input type="hidden" name="speed_dating" id="speed_date" value ="speed_date">
                </div>
            </div>
            <div class="modal-footer">                
                <button type="button" class="btn btn-default" data-dismiss="modal" style="margin-left: 10px;">Close</button>
                <button type="button" class="btn btn-primary" id ="update_speed_dating">Save changes</button>
            </div>
            <span class="bar_img" style="display:none; text-align: center">
                <img width="60px" height="12px" src="<?php echo bloginfo('template_url'); ?>/images/ajax-status.gif">
            </span>
        </div>
    </div>
</div>

<!----------------------------- End ------------------- -->
<div class="picAlbum">
    <div class="col-sm-6">
        <div class="Album-Part1">
            <h3 class="home-title"><span>MY ALBUMS</span></h3>            
            <ul class="album_section">
                <?php
                $totalalbum = 1;
                $sum = 1;
                if (count($results) > 0) {
                    foreach ($results as $value) {
                        $result = $wpdb->get_results("select * from user_album_images where album_id = '$value->id' AND status != 2 order by img_order asc limit 1");
                        $upload_dir = wp_upload_dir();
                        $imgPath = $upload_dir['baseurl'];
                        if (count($result) > 0) {
                            foreach ($result as $url) {
                                ?>
                                <li>
                                    <a href="javascript:void(0)" album_no="<?php echo $sum++; ?>" class="content-img" img-ids = "<?php echo $value->id; ?>">
                                        <img src="<?php echo $imgPath . "/users_album" . "/album_" . $user_id . "/" . $url->img_url; ?>">
                                    </a>
                                    <label ><?php echo $totalalbum++; ?></label>
                                </li> 
                            <?php }}
                            else {
                            ?>
                            <li>
                                <a href="javascript:void(0)" album_no="<?php echo $sum++; ?>" class="content-img" img-ids = "<?php echo $value->id; ?>">
                                    <img src="<?php bloginfo('template_url'); ?>/images/NoAlbum.jpeg">
                                </a>    
                                <label><?php echo $totalalbum++; ?></label>
                            </li>
                            <?php
                        }
                    }
                } else {
                    ?>
                    <li>
                        <span>No album create</span>
                    </li>
                <?php } ?>              
            </ul> 
            <?php if (count($results) < $create_album_limit) { ?>
                <button type="button" class="btn dating-btn-gray edit-profile" data-toggle="modal" data-target="#speed_dating" id="create_album">Create Album</button>
            <?php } ?> 
        </div>

        <!-- script for create Albums -->
        <script>
            jQuery('button#update_speed_dating').click(function () {
                var name = jQuery('#album_name').val();
                if (name != '')
                {
                    jQuery('.bar_img').show();
                    var fromData = 'action=create_album_by_user&albumName=' + name;
                    jQuery.ajax
                            ({
                                url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
                                data: fromData,
                                type: 'POST',
                                success: function (response)
                                {
                                    var result = jQuery.parseJSON(response);
                                    jQuery('.bar_img').hide();
                                    jQuery('#speed_msg').show();
                                    jQuery('#speed_msg').html(response.msg);
                                    setTimeout(function () {
                                        jQuery('#speed_msg').fadeOut(7000);
                                    }, 7000);
                                    window.location = self.location;
                                }
                            });
                } else
                {
                    jQuery('#speed_msg').show();
                    setTimeout(function () {
                        jQuery('#speed_msg').fadeOut(7000);
                    }, 7000);
                }
            });
        </script>

        <script>
            jQuery('a.content-img').on('click', function () {
                jQuery('.displayImage').show();
                var img_id = $(this).attr('img-ids');
                var albumNo = $(this).attr('album_no');
                jQuery('#home-title').text('EDIT ALBUM ' + albumNo);
                jQuery('#deleteAlb').attr('deleteAlbumid', img_id);
                jQuery('#upload-img').attr("img-id", img_id);
                var album_no = jQuery(this).attr('album_no');
                var fromData = 'action=user_image_fetching&img-id=' + img_id + '&imgNo=' + albumNo;
                jQuery.ajax({
                    type: 'POST',
                    data: fromData,
                    url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
                    success: function (data) {
                        jQuery('#deleteAlb').show();
                        jQuery('#enter_img_name').val('');
                        resetForm: true
                        jQuery('.displayImage').hide();
                        jQuery('#delete_album').attr('album_id', albumNo);
                        jQuery('span#spanText').text("EDIT ALBUM " + album_no);
                        jQuery('#showimages').html(data);
                    }
                });
            });
        </script>


        <!---End script -->
        <!--  Upload image section -->
        <div class="modal fade" id="edit_female_pick" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Form Preview</h4>
                    </div>
                    <div class="modal-body">
                        <div id="msg_female" class="successMsg"></div>
                        <form id="uploadimage" action="" method="post" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div id="image_preview" class="imgFrame">
                                        <img id="previewing" src="noimage.png" />
                                        <input type="hidden" value="" name="image" id="img-name">
                                    </div> 
                                    <div id="selectImage">
                                        <label class="control-label">Select Your Image</label><br/>
                                        <input type="file" name="file" id="file" alt="file" onchange ="showimagepreview(this)" />
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div id="select_img_name" class="select_img_name">
                                        <label>Picture name</label>
                                        <input type="text" name="enter_img_name" id="enter_img_name" class="enter_img_name">
                                    </div>  
                                </div>
                            </div>
                        </form>   
                    </div> 
                    <div class="modal-footer">
                    <div id="imgMsg" class="successMsg"></div>
                        <button type="button" class="btn btn-default" data-dismiss="modal" style="margin-left: 10px;">Close</button>
                        <button type="button" class="btn btn-primary" id ="upload-img" data-dismiss="modal">Save changes</button>
                    </div>
                    <h4 id='loading' style="display:none;position:absolute;top:50px;left:850px;font-size:25px;">loading...</h4>
                    <div id="message"></div>
                </div>
            </div>
        </div>
        <!-- script for uploading image -->
        <script>
            var divId = "";
            var imgno = "";
            jQuery(document).on('click', 'a#edit', function () {
                divId = jQuery(this).parent('li').attr('id');
                imgOrder = jQuery(this).parent('li').attr('imgOrder');
                imgno = jQuery(this).children('label').attr('imgno');
            });
        </script>
        <script>
            jQuery("#upload-img").on('click', function () {
                var hint = 0;
                var img_name = jQuery('#enter_img_name').val();
                if (img_name == '')
                {
                    hint = 1;
                    jQuery('#enter_img_name').css("border-color", "red");
                }                
                if(jQuery('#img-name').val() == '')
                {
                  hint = 1;                      
                }
                if (hint == 0)
                {
                    var srcimg = jQuery('#previewing').attr('src');                    
                    var intro_data = 'action=album_image_upload&srcimg=' + srcimg + '&userid=' +<?php echo $user_id; ?> + '&imgid=' + $(this).attr('img-id') + '&img_name=' + img_name + '&imgorder=' + imgOrder + '&imgno=' + imgno;
                    jQuery.ajax({
                        type: 'POST',
                        data: intro_data,
                        url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
                        success: function (result) {
                            jQuery('#enter_img_name').val('');
                            jQuery('#' + divId).children().remove();
                            jQuery('#' + divId).append(result);
                            //jQuery('#previewing').attr('src', '');
                             location.reload(true);
                        }
                    });
                }
            });
        </script>
        <script type="text/javascript">
            function showimagepreview(input)
            {
                var fileTypes = ['jpg', 'jpeg'];
                if (input.files && input.files[0])
                {
                    var filerdr = new FileReader();
                    var extension = input.files[0].name.split('.').pop().toLowerCase(),  //file extension from input file
                    isSuccess = fileTypes.indexOf(extension) > -1;
                    if (isSuccess) {
                        filerdr.onload = function (e) {
                            jQuery("#malepick").remove();
                            jQuery('#previewing').attr('src', e.target.result);
                            jQuery('#previewing').next('input').val(input.files[0]['name']);
                        }
                    }else
                    { 
                        jQuery('#previewing').attr('src', ''); 
                        jQuery('#previewing').next('input').val(''); 
                        jQuery('#imgMsg').show();                     
                        jQuery('#imgMsg').html('Upload only jpg/jpeg format images!');
                        setTimeout(function () {
                                        jQuery('#imgMsg').fadeOut(5000);
                                    }, 5000);
                    }
                    filerdr.readAsDataURL(input.files[0]);
                }
            }
        </script>


        <!-- End Script -->
        <!-- script for deleting picture -->
        <script>
            jQuery(document).on('click', '#deleteBtn', function () {
                var hint = 0;
                jQuery('.bar_img1').show();
                if (jQuery('#rename-caption').val('') == "")
                {
                    hint = 1;
                }

                var imgid = jQuery(this).attr('imgid');
                var deletpic = jQuery(this).attr('rowid');
                var intro_data = 'action=delete_user_pics_or_album&rowid=' + deletpic + '&type=pics';
                if (hint == 0) {
                    jQuery.ajax
                            ({
                                type: 'POST',
                                data: intro_data,
                                url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
                                success: function (data)
                                {
                                    var result = jQuery.parseJSON(data);
                                    jQuery('#shwImg').html(result.msg);
                                    setTimeout(function () {
                                        jQuery('#shwImg').fadeOut(10000);
                                    }, 10000);
                                    jQuery('#' + imgid).children('a').children('img').attr('src', '');
                                    jQuery('#largeImaga').children('img').attr('src', '');
                                    jQuery('#rename-caption').val('');
                                    jQuery('.bar_img1').hide();
                                    window.location = self.location;
                                }
                            });
                }
            });
        </script>
        <!-- End script -->
        <!-- start script for deleting album -->
        <div class="Album-Part1 mt15 album2">
            <h3 id="home-title" class="home-title">EDIT ALBUM </h3>
            <div class="topAction adjust1">
                <div class="row">
                    <div class="col-sm-12">
                        <span class="onclick">Click on field to add a picture</span>
                        <button id="deleteAlb" style="display:none" class="deleteBtn btn pull-right">delete album</button>
                        <a href="#" class="shiftPic" >shift pictures</a>         
                    </div>
                </div>

                <p><em>First picture is album cover</em></p>
            </div>
            <div id="showimages">
                <div class="displayImage" style="display:none;margin-left:200px;margin-bottom:20px">
                    <img src="<?php echo bloginfo('template_url'); ?>/images/ajax-status.gif"> 
                </div>
            </div>
            <!-- End -->
            <!-- Show image in large size -->
            <script>
                jQuery(document).on('click', '.clickOn', function () {
                    jQuery('#editNew').show();
                    var imgid = jQuery(this).parent('li').attr('id');
                    var rowid = jQuery(this).attr('rowid');
                    jQuery('#editNew').attr('rowid', rowid);
                    var imgname = jQuery(this).find('label').attr('img-name');
                    var text = jQuery(this).find('label').text();
                    jQuery('span#caption').text(imgname);
                    jQuery('#rename-caption').val(imgname);
                    jQuery('#rename-caption').attr('rename_id', rowid);
                    jQuery('.pickView').find('h4').html('Edit picture ' + text);
                    jQuery('#largeImaga').find('img').attr('src', jQuery(this).children().attr('src'));
                    jQuery('#deleteBtn').attr('rowid', rowid);
                    jQuery('#deleteBtn').attr('imgid', imgid);
                });
            </script>
            <script>
                jQuery('#deleteAlb').click(function () {
                    var albid = jQuery(this).attr('deleteAlbumid');
                    var formData = 'action=delete_user_pics_or_album&albid=' + albid + '&type=album';
                    jQuery.ajax
                            ({
                                url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
                                data: formData,
                                type: 'POST',
                                success: function (response)
                                {
                                    window.location = self.location;
                                }
                            });

                });
            </script>
            <div class="editNew" id="editNew" style="display:none">
                <div class="row">
                    <div class="col-sm-5 pickView">
                        <h4>Edit picture</h4>
                        <figure id="largeImaga"><img class="prof-cartoon-img" src="<?php bloginfo('template_url'); ?>/images/img_e.jpg"></figure>
                        <label class="text-center"></label>
                    </div>
                    <div class="col-sm-7">
                        <div class="inputComment form-group">
                            <label class="label-control">New Comment</label>
                            <input id="rename-caption" type="text" value="" class="form-control" />
                        </div>

                        <div class="text-right form-group">
                            <span class="bar_img2" style="display: none; text-align: center;">
                                <img width="60px" height="12px" src="<?php echo get_stylesheet_directory_uri(); ?>/images/ajax-status.gif">
                            </span>
                            <button class="renameBtn btn" id="rename_id">rename</button>   
                        </div>
                        <div class="text-right form-group">
                            <button href="javascript:void(0)" id="deleteBtn" class="deleteBtn btn pull-right" >Delete pictures</button>
                            <span class="bar_img1" style="display: none;">
                                <img width="60px" height="12px" src="<?php echo bloginfo('template_url'); ?>/images/ajax-status.gif">
                            </span>   
                        </div>
                        <div id="shwImg"  class="successMsg"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Album visibilty section -->
    <?php
    $result = $wpdb->get_results("select * from wp_album_setting_for_all_member_types where user_id = '$user_id'");
    $album1 = array();
    $album2 = array();
    $album3 = array();
    $album4 = array();
    $album5 = array();
    if (count($result) > 0) {
        foreach ($result as $key => $value) {
            $album1 = explode(',', trim($value->album1, ','));
            $album2 = explode(',', trim($value->album2, ','));
            $album3 = explode(',', trim($value->album3, ','));
            $album4 = explode(',', trim($value->album4, ','));
            $album5 = explode(',', trim($value->album5, ','));
        }
    }
    ?>    
    <div class="col-sm-6">
        <div class="table-responsive table-album">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th rowspan="2"><span>MY ALBUMS</span></th>
                        <th class="count">Album <br/>1</th>
                        <th class="count">Album <br/>2</th>
                        <th class="count">Album <br/>3</th>
                        <th class="count">Album <br/>4</th>
                        <th class="count">Album <br/>5</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>For all members</th>
                        <td  class="count td1">
                            <input type="checkbox" name="album1" class="form-control checkbox_cus checkAll" <?php echo (in_array('all', $album1)) ? 'checked' : ''; ?> value="all" style="display: none;" id="all1">                                                                            
                            <label for="all1" class="checkbox_label"></label>
                        </td>
                        <td class="count td2">
                            <input type="checkbox" name="album2" class="form-control checkbox_cus checkAll" <?php echo (in_array('all', $album2)) ? 'checked' : ''; ?> value="all" style="display: none;" id="all2">                                                                            
                            <label for="all2" class="checkbox_label"></label>
                        </td>
                        <td class="count td3">
                            <input type="checkbox" name="album3" class="form-control checkbox_cus checkAll" <?php echo (in_array('all', $album3)) ? 'checked' : ''; ?> value="all" style="display: none;" id="all3">                                                                            
                            <label for="all3" class="checkbox_label"></label>
                        </td>     
                        <td class="count td4">
                            <input type="checkbox" name="album4" class="form-control checkbox_cus checkAll" <?php echo (in_array('all', $album4)) ? 'checked' : ''; ?> value="all" style="display: none;" id="all4">                                                                            
                            <label for="all4" class="checkbox_label"></label>
                        </td>   
                        <td class="count td5">
                            <input type="checkbox" name="album5" class="form-control checkbox_cus checkAll" <?php echo (in_array('all', $album5)) ? 'checked' : ''; ?> value="all" style="display: none;" id="all5">                                                                            
                            <label for="all5" class="checkbox_label"></label>
                        </td>
                    </tr>
                    <tr>
                        <th>couples</th>
                        <td class="count td1">
                            <input type="checkbox" name="album1" class="form-control checkbox_cus checkAll" <?php echo (in_array('couples', $album1)) ? 'checked' : ''; ?> value="couples" style="display: none;" id="couples-al1">                                                                            
                            <label for="couples-al1" class="checkbox_label"></label>
                        </td>
                        <td class="count td2">
                            <input type="checkbox" name="album2" class="form-control checkbox_cus checkAll" <?php echo (in_array('couples', $album2)) ? 'checked' : ''; ?> value="couples" style="display: none;" id="couples-al2">                                                                            
                            <label for="couples-al2" class="checkbox_label"></label>
                        </td>
                        <td class="count td3">
                            <input type="checkbox" name="album3" class="form-control checkbox_cus checkAll" <?php echo (in_array('couples', $album3)) ? 'checked' : ''; ?> value="couples" style="display: none;" id="couples-al3">                                                                            
                            <label for="couples-al3" class="checkbox_label"></label>
                        </td>
                        <td class="count td4">
                            <input type="checkbox" name="album4" class="form-control checkbox_cus checkAll" <?php echo (in_array('couples', $album4)) ? 'checked' : ''; ?> value="couples" style="display: none;" id="couples-al4">                                                                            
                            <label for="couples-al4" class="checkbox_label"></label>
                        </td>
                        <td class="count td5">
                            <input type="checkbox" name="album5" class="form-control checkbox_cus checkAll" <?php echo (in_array('couples', $album5)) ? 'checked' : ''; ?> value="4" style="display: none;" id="couples-al5">                                                                            
                            <label for="couples-al5" class="checkbox_label"></label>
                        </td>
                    </tr>
                    <tr>
                        <th>couples validated</th>
                        <td class="count td1">
                            <input type="checkbox" name="album1" class="form-control checkbox_cus checkAll" <?php echo (in_array('couples validate', $album1)) ? 'checked' : ''; ?> value="couples validate" style="display: none;" id="Cvalidate1">                                                                            
                            <label for="Cvalidate1" class="checkbox_label"></label>
                        </td>
                        <td class="count td2">
                            <input type="checkbox" name="album2" class="form-control checkbox_cus checkAll" <?php echo (in_array('couples validate', $album2)) ? 'checked' : ''; ?> value="couples validate" style="display: none;" id="Cvalidate2">                                                                            
                            <label for="Cvalidate2" class="checkbox_label"></label>
                        </td>
                        <td class="count td3">
                            <input type="checkbox" name="album3" class="form-control checkbox_cus checkAll" <?php echo (in_array('couples validate', $album3)) ? 'checked' : ''; ?> value="couples validate" style="display: none;" id="Cvalidate3">                                                                            
                            <label for="Cvalidate3" class="checkbox_label"></label>
                        </td>
                        <td class="count td4">
                            <input type="checkbox" name="album4" class="form-control checkbox_cus checkAll" <?php echo (in_array('couples validate', $album4)) ? 'checked' : ''; ?> value="couples validate" style="display: none;" id="Cvalidate4">                                                                            
                            <label for="Cvalidate4" class="checkbox_label"></label>
                        </td>
                        <td class="count td5">
                            <input type="checkbox" name="album5" class="form-control checkbox_cus checkAll" <?php echo (in_array('couples validate', $album5)) ? 'checked' : ''; ?> value="couples validate" style="display: none;" id="Cvalidate5">                                                                            
                            <label for="Cvalidate5" class="checkbox_label"></label>
                        </td>
                    </tr>
                    <tr>
                        <th>ladies</th>
                        <td class="count td1">
                            <input type="checkbox" name="album1" class="form-control checkbox_cus checkAll" <?php echo (in_array('ladies', $album1)) ? 'checked' : ''; ?> value="ladies" style="display: none;" id="ladies1">                                                                            
                            <label for="ladies1" class="checkbox_label"></label>
                        </td>
                        <td class="count td2">
                            <input type="checkbox" name="album2" class="form-control checkbox_cus checkAll" <?php echo (in_array('ladies', $album2)) ? 'checked' : ''; ?> value="ladies" style="display: none;" id="ladies2">                                                                            
                            <label for="ladies2" class="checkbox_label"></label>
                        </td>
                        <td class="count td3">
                            <input type="checkbox" name="album3" class="form-control checkbox_cus checkAll" <?php echo (in_array('ladies', $album3)) ? 'checked' : ''; ?> value="ladies" style="display: none;" id="ladies3">                                                                            
                            <label for="ladies3" class="checkbox_label"></label>
                        </td>
                        <td class="count td4">
                            <input type="checkbox" name="album4" class="form-control checkbox_cus checkAll" <?php echo (in_array('ladies', $album4)) ? 'checked' : ''; ?> value="ladies" style="display: none;" id="ladies4">                                                                            
                            <label for="ladies4" class="checkbox_label"></label>
                        </td>
                        <td class="count td5">
                            <input type="checkbox" name="album5" class="form-control checkbox_cus checkAll" <?php echo (in_array('ladies', $album5)) ? 'checked' : ''; ?> value="ladies" style="display: none;" id="ladies5">                                                                            
                            <label for="ladies5" class="checkbox_label"></label>
                        </td>
                    </tr>
                    <tr>
                        <th>ladies validated</th>
                        <td class="count td1">
                            <input type="checkbox" name="album1" class="form-control checkbox_cus checkAll" <?php echo (in_array('ladies validated', $album1)) ? 'checked' : ''; ?> value="ladies validated" style="display: none;" id="validatedladies1">                                                                            
                            <label for="validatedladies1" class="checkbox_label"></label>
                        </td>
                        <td class="count td2">
                            <input type="checkbox" name="album2" class="form-control checkbox_cus checkAll" <?php echo (in_array('ladies validated', $album2)) ? 'checked' : ''; ?> value="ladies validated" style="display: none;" id="validatedladies2">                                                                            
                            <label for="validatedladies2" class="checkbox_label"></label>
                        </td>
                        <td class="count td3">
                            <input type="checkbox" name="album3" class="form-control checkbox_cus checkAll" <?php echo (in_array('ladies validated', $album3)) ? 'checked' : ''; ?> value="ladies validated" style="display: none;" id="validatedladies3">                                                                            
                            <label for="validatedladies3" class="checkbox_label"></label>
                        </td>
                        <td class="count td4">
                            <input type="checkbox" name="album4" class="form-control checkbox_cus checkAll" <?php echo (in_array('ladies validated', $album4)) ? 'checked' : ''; ?> value="ladies validated" style="display: none;" id="validatedladies4">                                                                            
                            <label for="validatedladies4" class="checkbox_label"></label>
                        </td>
                        <td class="count td5">
                            <input type="checkbox" name="album5" class="form-control checkbox_cus checkAll" <?php echo (in_array('ladies validated', $album5)) ? 'checked' : ''; ?> value="ladies validated" style="display: none;" id="validatedladies5">                                                                            
                            <label for="validatedladies5" class="checkbox_label"></label>
                        </td>
                    </tr>       
                    <tr>
                        <th>single ladies</th>
                        <td class="count td1">
                            <input type="checkbox" name="album1" class="form-control checkbox_cus checkAll" <?php echo (in_array('single ladies', $album1)) ? 'checked' : ''; ?> value="single ladies" style="display: none;" id="Sladies1">                                                                            
                            <label for="Sladies1" class="checkbox_label"></label>
                        </td>
                        <td class="count td2">
                            <input type="checkbox" name="album2" class="form-control checkbox_cus checkAll" <?php echo (in_array('single ladies', $album2)) ? 'checked' : ''; ?>  value="single ladies" style="display: none;" id="Sladies2">                                                                            
                            <label for="Sladies2" class="checkbox_label"></label>
                        </td>
                        <td class="count td3">
                            <input type="checkbox" name="album3" class="form-control checkbox_cus checkAll" <?php echo (in_array('single ladies', $album3)) ? 'checked' : ''; ?> value="single ladies" style="display: none;" id="Sladies3">                                                                            
                            <label for="Sladies3" class="checkbox_label"></label>
                        </td>
                        <td class="count td4">
                            <input type="checkbox" name="album4" class="form-control checkbox_cus checkAll" <?php echo (in_array('single ladies', $album4)) ? 'checked' : ''; ?> value="single ladies" style="display: none;" id="Sladies4">                                                                            
                            <label for="Sladies4" class="checkbox_label"></label>
                        </td>
                        <td class="count td5">
                            <input type="checkbox" name="album5" class="form-control checkbox_cus checkAll" <?php echo (in_array('single ladies', $album5)) ? 'checked' : ''; ?> value="single ladies" style="display: none;" id="Sladies5">                                                                            
                            <label for="Sladies5" class="checkbox_label"></label>
                        </td>
                    </tr>       
                    <tr>
                        <th>men</th>
                        <td class="count td1">
                            <input type="checkbox" name="album1" class="form-control checkbox_cus checkAll" <?php echo (in_array('men', $album1)) ? 'checked' : ''; ?>  value="men" style="display: none;" id="men1">                                                                            
                            <label for="men1" class="checkbox_label"></label>
                        </td>
                        <td class="count td2">
                            <input type="checkbox" name="album2" class="form-control checkbox_cus checkAll" <?php echo (in_array('men', $album2)) ? 'checked' : ''; ?> value="men" style="display: none;" id="men2">                                                                            
                            <label for="men2" class="checkbox_label"></label>
                        </td>
                        <td class="count td3">
                            <input type="checkbox" name="album3" class="form-control checkbox_cus checkAll" <?php echo (in_array('men', $album3)) ? 'checked' : ''; ?> value="men" style="display: none;" id="men3">                                                                            
                            <label for="men3" class="checkbox_label"></label>
                        </td>
                        <td class="count td4">
                            <input type="checkbox" name="album4" class="form-control checkbox_cus checkAll" <?php echo (in_array('men', $album4)) ? 'checked' : ''; ?> value="men" style="display: none;" id="men4">                                                                            
                            <label for="men4" class="checkbox_label"></label>
                        </td>
                        <td class="count td5">
                            <input type="checkbox" name="album5" class="form-control checkbox_cus checkAll" <?php echo (in_array('men', $album5)) ? 'checked' : ''; ?> value="men" style="display: none;" id="men5">                                                                            
                            <label for="men5" class="checkbox_label"></label>
                        </td>
                    </tr>       
                    <tr>
                        <th>men validated</th>
                        <td class="count td1">
                            <input type="checkbox" name="album1" class="form-control checkbox_cus checkAll" <?php echo (in_array('men validated', $album1)) ? 'checked' : ''; ?> value="men validated" style="display: none;" id="Vman1">                                                                            
                            <label for="Vman1" class="checkbox_label"></label>
                        </td>
                        <td class="count td2">
                            <input type="checkbox" name="album2" class="form-control checkbox_cus checkAll" <?php echo (in_array('men validated', $album2)) ? 'checked' : ''; ?> value="men validated" style="display: none;" id="Vman2">                                                                            
                            <label for="Vman2" class="checkbox_label"></label>
                        </td>
                        <td class="count td3">
                            <input type="checkbox" name="album3" class="form-control checkbox_cus checkAll" <?php echo (in_array('men validated', $album3)) ? 'checked' : ''; ?> value="men validated" style="display: none;" id="Vman3">                                                                            
                            <label for="Vman3" class="checkbox_label"></label>
                        </td>
                        <td class="count td4">
                            <input type="checkbox" name="album4" class="form-control checkbox_cus checkAll" <?php echo (in_array('men validated', $album4)) ? 'checked' : ''; ?> value="men validated" style="display: none;" id="Vman4">                                                                            
                            <label for="Vman4" class="checkbox_label"></label>
                        </td>
                        <td class="count td5">
                            <input type="checkbox" name="album5" class="form-control checkbox_cus checkAll" <?php echo (in_array('men validated', $album5)) ? 'checked' : ''; ?> value="men validated" style="display: none;" id="Vman5">                                                                            
                            <label for="Vman5" class="checkbox_label"></label>
                        </td>
                    </tr>       
                    <tr>
                        <th rowspan="2">single men</th>
                        <td class="count td1">
                            <input type="checkbox" name="album1" class="form-control checkbox_cus checkAll" <?php echo (in_array('single men', $album1)) ? 'checked' : ''; ?> value="single men" style="display: none;" id="Sman1">                                                                            
                            <label for="Sman1" class="checkbox_label"></label>
                        </td>
                        <td class="count td2">
                            <input type="checkbox" name="album2" class="form-control checkbox_cus checkAll" <?php echo (in_array('single men', $album2)) ? 'checked' : ''; ?> value="single men" style="display: none;" id="Sman2">                                                                            
                            <label for="Sman2" class="checkbox_label"></label>
                        </td>
                        <td class="count td3">
                            <input type="checkbox" name="album3" class="form-control checkbox_cus checkAll" <?php echo (in_array('single men', $album3)) ? 'checked' : ''; ?> value="single men" style="display: none;" id="Sman3">                                                                            
                            <label for="Sman3" class="checkbox_label"></label>
                        </td>
                        <td class="count td4">
                            <input type="checkbox" name="album4" class="form-control checkbox_cus checkAll" <?php echo (in_array('single men', $album4)) ? 'checked' : ''; ?> value="single men" style="display: none;" id="Sman4">                                                                            
                            <label for="Sman4" class="checkbox_label"></label>
                        </td>
                        <td class="count td5">
                            <input type="checkbox" name="album5" class="form-control checkbox_cus checkAll" <?php echo (in_array('single men', $album5)) ? 'checked' : ''; ?> value="single men" style="display: none;" id="Sman5">                                                                            
                            <label for="Sman5" class="checkbox_label"></label>
                        </td>
                    </tr>       
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="6"><p class="footer-msg col-sm-9">If you do not customize the visibility of your albums here, they can be seen by all profile visitors</p>
                            <span style="display: none;" class="setting_loding">
                                <img width="60px" height="12px" src="<?php echo bloginfo('template_url') ?>/images/ajax-status.gif">
                            </span>
                            <button type="button" id="save" class="renameBtn btn save-btn pull-right">save</button>
                            <div id="setting_msg" class="successMsg"></div>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
        <script>
            jQuery('.checkAll').click(function () {
                if (this.checked == true)
                {
                    jQuery(this).each(function () {
                        jQuery(this).attr('checked', 'checked');
                    });
                } else {
                    jQuery(this).each(function () {
                        jQuery(this).attr('checked', false);
                    });
                }
            });

            jQuery('button#save').click(function () {
                var count = 0;
                var json = '['
                jQuery('.checkAll').each(function () {
                    if (this.checked == true) {
                        json = json + '{"' + jQuery(this).attr('name') + '":"' + jQuery(this).val() + '"';
                        json = json + '},';
                        count++;
                    }
                });
                json = json.replace(/,\s*$/, "");
                json = json + ']';
                if (count > 0) {
                    jQuery('.setting_loding').show();
                    var fromData = 'action=my_action_on_album_setting&json=' + json;
                    jQuery.ajax
                            ({
                                url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
                                data: fromData,
                                type: 'POST',
                                success: function (response)
                                {
                                    var result = jQuery.parseJSON(response);
                                    jQuery('.setting_loding').hide();
                                    jQuery('#setting_msg').html(result.msg);
                                    setTimeout(function () {
                                        jQuery('#setting_msg').fadeOut(7000);
                                    }, 7000);
                                }
                            });
                } else {
                    jQuery('#setting_msg').html("Please checked atlest one album");
                }
            });
        </script>
        <?php if ($lockable_albums > 0) {
            $i=1;
            ?>
            <div class="Album-Part1 closingPoint">
                <h3 class="home-title">
                    <span>INDIVIDUAL OPENING / CLOSING</span>
                </h3>
                <a href="#" data-toggle="modal" data-target="#openmemberlist" class="listOpen">List of opened members</a>
                <div class="closingIn">
                    <p class="col-sm-12">open marked albums for a specific member of Hedonia<br/>
                        (outside of your above selection group)</p>
                    <div class="panel panel-green panel-all">                                                                      
                       <?php foreach ($results as $key => $value) { ?>
                        <div class="input-group">
                            <span class="">                                        
                                <input type="checkbox" name="album<?php echo $i; ?>" id="alb<?php echo $i; ?>" class="form-control openAlb checkbox_cus" value="4" style="display: none;" >                                                                            
                                <label for="alb<?php echo $i; ?>" class="checkbox_label"></label><br/>          
                                <?php echo $value->album_name; ?>
                            </span>
                        </div> 
                        <?php $i++; } ?>                            
                        <div class="clear"></div>                 
                        <div class="input-sec">                        
                            <span class="memb-name">Members name</span>
                            <input type="text" id="autocomplete" value="" class="input-box">
                            <span class="open_loding" style="display:none;">
                                <img width="60px" height="12px" src="<?php echo bloginfo('template_url'); ?>/images/ajax-status.gif">
                            </span>
                            <button id="save_open_marked" class="btn okBtn okbtn-green" type="button">Ok</button>
                            <div style="color:red!important; margin-top: 5px;" id="open_msg" class="successMsg"></div>        
                        </div>
                    </div>
                <style type="text/css">
                    .ui-autocomplete { position: absolute; background:#CCC }
                </style>
                <script type="text/javascript" src="<?php echo bloginfo('template_url') ?>/js/jquery.autocomplete1.min.js"></script>
                <script type="text/javascript">
                var userid = "";
                jQuery("input#autocomplete").on("focus", function (event) {
                    var mainmenuSearch_a;
                    var options = {
                        serviceUrl: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
                        onSelect: userlist_search,
                        deferRequestBy: 0, //miliseconds
                        params: {
                            type: 'open_marked_user',
                            limit: '10',
                            action: 'autocomplete_user_list'
                        },
                        noCache: true //set to true, to disable caching
                    };
                    mainmenuSearch_a = jQuery(this).autocomplete(options);
                });
                var userlist_search = function (mainmenuSearch_value, mainmenuSearch_data)
                {
                    userid = mainmenuSearch_data;
                    jQuery('input#autocomplete').attr('userid', userid);
                }
                    </script>
                    <script>
                        jQuery('.openAlb').click(function () {
                            if (this.checked == true)
                            {
                                jQuery(this).each(function () {
                                    jQuery(this).attr('checked', 'checked');
                                });
                            } else {
                                jQuery(this).each(function () {
                                    jQuery(this).attr('checked', false);
                                });
                            }

                        });

                        jQuery('button#save_open_marked').click(function () {
                            var count = 0;
                            var userid = jQuery('#autocomplete').attr('userid');
                            var json = '['
                            jQuery('.openAlb').each(function () {
                                if (this.checked == true) {
                                    json = json + '{"' + jQuery(this).attr('name') + '":"1"';
                                    json = json + '},';
                                    count++;
                                }
                            });
                            json = json.replace(/,\s*$/, "");
                            json = json + ']';
                            if (count > 0 && userid != "" && userid != undefined) {
                                jQuery('.open_loding').show();
                                var fromData = 'action=my_action_open_album_setting&json=' + json + '&user_id=' + userid;
                                jQuery.ajax
                                        ({
                                            url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
                                            data: fromData,
                                            type: 'POST',
                                            success: function (data)
                                            {
                                                var result = jQuery.parseJSON(data);
                                                jQuery('.open_loding').hide();
                                                if(result.err == "success")
                                                {                                                    
                                                    jQuery('#open_msg').show();
                                                    jQuery('#open_msg').html(result.msg);
                                                    setTimeout(function () {
                                                        jQuery('#open_msg').fadeOut(7000);
                                                    }, 7000);
                                                    window.location = self.location;
                                                }else
                                                {                                                   
                                                    jQuery('#open_msg').show();
                                                    jQuery('#open_msg').html(result.msg);
                                                    setTimeout(function () {
                                                        jQuery('#open_msg').fadeOut(7000);
                                                    }, 7000); 
                                                }
                                                
                                            }
                                        });
                            } else {
                                jQuery('#open_msg').html("Please checked atlest one album");
                                setTimeout(function () {
                                    jQuery('#open_msg').fadeOut(7000);
                                }, 7000);
                            }
                        });
                    </script>

                    <p class="col-sm-12">close marked albums for a specific member of Hedonia<br/>
                        (outside of your above selection group)</p>
                    <div class="panel panel-red panel-all">                   
                     <?php $j=1; foreach ($results as $key => $value) { ?>                                             
                        <div class="input-group">
                            <span class="">                                        
                                <input type="checkbox" id="album<?php echo $j; ?>" class="form-control closeAlb  checkbox_cus" value="4" style="display: none;" id="al-<?php echo $i; ?>">                                                                            
                                <label for="album<?php echo $j; ?>" class="checkbox_label"></label><br/>
                                <?php echo $value->album_name; ?>
                            </span>
                        </div> 
                        <?php $j++; } ?>                                            
                        <div class="clear"></div>                 
                        <div class="input-sec">                        
                            <span class="memb-name">Members name</span>
                            <input type="text" id="autocomplete1" class="input-box">
                            <span class="close_loding" style="display:none;">
                                <img width="60px" height="12px" src="<?php echo bloginfo('template_url'); ?>/images/ajax-status.gif">
                            </span>
                            <button class="btn okBtn okbtn-red" id="save_closed_marked" type="button">Ok</button>         
                        <div style="color:red!important; margin-top: 5px;" id="open_msg1" class="successMsg"></div> 
                        </div>
                    </div>
                </div>
            </div>
        <?php } else {
            ?>
            <div class="Album-Part1 closingPoint">
                <span class="grayTrans"></span>
                <h3 class="home-title">
                    <span>INDIVIDUAL OPENING / CLOSING</span>
                </h3>
                <a href="#" class="listOpen">List of opened members</a>
                <div class="closingIn">
                    <p class="col-sm-12">open marked albums for a specific member of Hedonia<br/>
                        (outside of your above selection group)</p>
                    <div class="panel panel-green panel-all">                                                                       
                        <div class="input-group">
                            <span class="">                                        
                                <input type="checkbox" name="album1" id="alb1" class="form-control openAlb checkbox_cus" value="4" style="display: none;" >                                                                            
                                <label for="alb1" class="checkbox_label"></label><br/>          
                                Album 1
                            </span>
                        </div>                            
                        <div class="input-group">
                            <span class="">
                                <input type="checkbox" name="album2" id="alb2" class="form-control openAlb checkbox_cus" value="5" style="display: none;" >                                                                            
                                <label for="alb2" class="checkbox_label"></label><br/>
                                Album 2
                            </span>
                        </div>
                        <div class="input-group">
                            <span class="">
                                <input type="checkbox" name="album3" id="alb3" class="form-control openAlb checkbox_cus" value="5" style="display: none;" >                                                                            
                                <label for="alb3" class="checkbox_label"></label><br/>
                                Album 3
                            </span>
                        </div>
                        <div class="input-group">
                            <span class="">
                                <input type="checkbox" name="album4" id="alb4" class="form-control openAlb checkbox_cus" value="5" style="display: none;" >                                                                            
                                <label for="alb4" class="checkbox_label"></label><br/>
                                Album 4
                            </span>
                        </div>
                        <div class="input-group">
                            <span class="">
                                <input type="checkbox" name="album5" id="alb5" class="form-control openAlb checkbox_cus" value="5" style="display: none;" >                                                                            
                                <label for="alb5" class="checkbox_label"></label><br/>
                                Album 5
                            </span>
                        </div>                            
                        <div class="clear"></div>                 
                        <div class="input-sec">                        
                            <span class="memb-name">Members name</span>
                            <input type="text" id="autocomplete" value="" class="input-box">
                            <span class="open_loding" style="display:none;">
                                <img width="60px" height="12px" src="<?php echo bloginfo('template_url'); ?>/images/ajax-status.gif">
                            </span>
                            <button id="save_open_marked" class="btn okBtn okbtn-green" type="button">Ok</button>
                            <div style="color:red!important; margin-top: 5px;" id="open_msg" class="successMsg"></div>        
                        </div>
                    </div>
                    <style type="text/css">
                        .ui-autocomplete { position: absolute; background:#CCC }
                    </style>
                    <p class="col-sm-12">close marked albums for a specific member of Hedonia<br/>
                        (outside of your above selection group)</p>
                    <div class="panel panel-red panel-all">                                                                       
                        <div class="input-group">
                            <span class="">                                        
                                <input disabled="disabled" type="checkbox" disabled id="album1" class="form-control closeAlb  checkbox_cus" value="4" style="display: none;" id="al-4">                                                                            
                                <label for="album1" class="checkbox_label"></label><br/>
                                album 1
                            </span>
                        </div>                            
                        <div class="input-group">
                            <span class="">
                                <input disabled="disabled" type="checkbox" disabled id="album2" class="form-control closeAlb checkbox_cus" value="5" style="display: none;" id="al-5">                                                                            
                                <label for="album2" class="checkbox_label"></label><br/>
                                album 2
                            </span>
                        </div>                            
                        <div class="input-group">
                            <span class="">
                                <input disabled="disabled" type="checkbox" id="album3" disabled class="form-control closeAlb checkbox_cus" value="5" style="display: none;" id="al-5">                                                                            
                                <label for="album3" class="checkbox_label"></label><br/>
                                album 3
                            </span>
                        </div>                            
                        <div class="input-group">
                            <span class="">
                                <input disabled="disabled" type="checkbox" disabled id="album4" class="form-control closeAlb checkbox_cus" value="5" style="display: none;" id="al-5">                                                                            
                                <label for="album4" class="checkbox_label"></label><br/>
                                album 4
                            </span>
                        </div>                            
                        <div class="input-group">
                            <span class="">
                                <input disabled="disabled" type="checkbox" disabled id="album5" class="form-control closeAlb checkbox_cus" value="5" style="display: none;" id="al-5">                                                                            
                                <label for="album5" class="checkbox_label"></label><br/>
                                album 5
                            </span>
                        </div>                            
                        <div class="clear"></div>                 
                        <div class="input-sec">                        
                            <span class="memb-name">Members name</span>
                            <input type="text" id="autocomplete1" class="input-box">
                            <span class="close_loding" style="display:none;">
                                <img width="60px" height="12px" src="<?php echo bloginfo('template_url'); ?>/images/ajax-status.gif">
                            </span>
                            <button class="btn okBtn okbtn-red" id="save_closed_marked" type="button">Ok</button>         
                        <div style="color:red!important; margin-top: 5px;" id="open_msg1" class="successMsg"></div>        
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
    <div class="clear"></div>
</div>

<!-- Modal for open member list -->

<?php 
$userlist = $wpdb->get_results("select `display_name` from `wp_users` where ID != '$user_id' AND ID IN (select `marked_for_user_id` from wp_open_closed_marked_user_status where user_id = '$user_id' AND open_closed_status = '1')");

?>
<div class="modal fade" id="openmemberlist" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Member Name</h4>
            </div>
            <div class="modal-body">
                <div id ="speed_msg" class="successMsg"></div>
                <div class="editBoth" style="">
                <ul>
                <?php foreach($userlist as $value){ ?>
                    <li style="color:black"><?php echo $value->display_name; ?></li>
                <?php } ?>                
                </ul>                
             </div>

            </div>            
        </div>
    </div>
</div>

<!-- close -->

<style type="text/css">
    .ui-autocomplete1 { position: absolute; background:#CCC }
</style>
<script type="text/javascript">
    var userid = "";
    jQuery("input#autocomplete1").on("focus", function (event) {
        var mainmenuSearch_a;
        var options = {
            serviceUrl: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
            onSelect: userlist_search1,
            deferRequestBy: 0, //miliseconds
            params: {
                type: 'close_marked_user',
                limit: '10',
                action: 'autocomplete_user_list'
            },
            noCache: true //set to true, to disable caching
        };
        mainmenuSearch_a = jQuery(this).autocomplete(options);
    });
    var userlist_search1 = function (mainmenuSearch_value1, mainmenuSearch_data1) {
        userid = mainmenuSearch_data1;
        jQuery('input#autocomplete1').attr('userid', userid);
        var fromData = 'action=user_close_album_staus&user_id=' + userid;
        jQuery.ajax
                ({
                    url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
                    data: fromData,
                    type: 'POST',
                    success: function (response)
                    {
                        var result = jQuery.parseJSON(response);
                        if (result.album1 != undefined && result.album1 == 1 && result.album1 != null)
                        {
                            jQuery('input#album1').attr('checked', 'checked');
                        } else {
                            jQuery('input#album1').removeAttr('checked');
                        }
                        if (result.album2 != undefined && result.album2 == 1 && result.album2 != null)
                        {
                            jQuery('input#album2').attr('checked', 'checked');
                        } else {
                            jQuery('input#album2').removeAttr('checked');
                        }
                        if (result.album3 != undefined && result.album3 == 1 && result.album3 != null)
                        {
                            jQuery('input#album3').attr('checked', 'checked');
                        } else {
                            jQuery('input#album3').removeAttr('checked');
                        }
                        if (result.album4 != undefined && result.album4 == 1 && result.album4 != null)
                        {
                            jQuery('input#album4').attr('checked', 'checked');
                        } else {
                            jQuery('input#album4').removeAttr('checked');
                        }
                        if (result.album5 != undefined && result.album5 == 1 && result.album5 != null)
                        {
                            jQuery('input#album5').attr('checked', 'checked');
                        } else {
                            jQuery('input#album5').removeAttr('checked');
                        }
                    }
                });
    }
</script>

<script>
    jQuery('button#save_closed_marked').click(function () {
        var count = 0;
        var userid = jQuery('#autocomplete1').attr('userid');
        var json = '['
        jQuery('.closeAlb').each(function () {
            if (this.checked == true) {
                json = json + '{"' + jQuery(this).attr('id') + '":"0"';
                json = json + '},';
                count++;
            }
        });
        json = json.replace(/,\s*$/, "");
        json = json + ']';
        if (count > 0 && userid != "" && userid != undefined) {
            jQuery('.close_loding').show();
            var fromData = 'action=my_action_close_album_setting&json=' + json + '&user_id=' + userid;
            jQuery.ajax
                    ({
                        url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
                        data: fromData,
                        type: 'POST',
                        success: function (response)
                        {
                            jQuery('.close_loding').hide();
                            jQuery('#open_msg1').html(response);
                            setTimeout(function () {
                                jQuery('#open_msg1').fadeOut(7000);
                            }, 7000);
                           window.location = self.location;
                        }
                    });
        } else {
            jQuery('#open_msg1').html("Please checked atlest one album");
            setTimeout(function () {
                jQuery('#open_msg1').fadeOut(7000);
            }, 7000);
        }
    });
</script>

<script>
    jQuery('#rename_id').click(function () {
        var row_id = jQuery('#editNew').attr('rowid');
        var name = jQuery('#rename-caption').val();
        jQuery('.bar_img2').show();
        var fromData = 'action=delete_user_pics_or_album&row_id=' + row_id + '&name=' + name + '&type=rename';
        jQuery.ajax
                ({
                    url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
                    data: fromData,
                    type: 'POST',
                    success: function (response)
                    {
                        jQuery('.bar_img2').hide();
                        jQuery("a[rowid='" + row_id + "']").children('label').attr('img-name', name);
                    }
                });
    });
</script>
<script>
    jQuery(document).ready(function () {
        var user_alb_count = '<?php echo $user_alb_count; ?>';
        var i = parseInt(user_alb_count) + 1;
        for (i; i <= 5; i++)
        {
            jQuery('.td' + i + ' input').attr('disabled', true);
        }
    });
</script>






