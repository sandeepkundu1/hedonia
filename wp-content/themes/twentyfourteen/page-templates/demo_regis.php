<?php
/*Template Name:User Registeration Demo
 * 
 * 
 * 
*/
get_header();

?> 

        <div class="container-fluid">
            <div class="bodySection">		
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-1">
                        <a href="#" class="imgwrap"><img class="img-responsive" src="images/canstockphoto20022895.gif" alt="" /></a>
                    </div>
                    <div class="col-sm-5">
                        <div class="registrationPart">
                            <form class="form-horizontal" role="form" name="form" id="register-form" enctype="multipart/form-data">
                                <div class="firstInfo blue_gred">
                                    <h2>Registration</h2>
                                    <div class="persionalInfo">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-6 control-label">Profile name</label>
                                            
                                            <div class="col-sm-6">
                                                <input type ="text" for="validation" name="profile_name" id="profile_name" class="form-control" required="" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-6 control-label">email</label>
                                            <div class="col-sm-6">
                                                <input type="email" for="validation" name="email" id="email" class="form-control" required="" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-6 control-label">Password <em>(min. 8 characters)</em></label>
                                            <div class="col-sm-6">
                                                <input type="password" for="validation" name="password" id="password" class="form-control" >
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="ageInfo red_gred">                                
                                    <h5>Entries in this section can be changed later only 1x !</h5>   
                                    <div id="dob_result"></div>                             
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-4 control-label">Date of birth (she)</label>
                                        <div class="col-sm-1 paddingsm">
                                            <input type="text" name="shedd" for="validation" id="shedd" enterText="numeric" class="form-control" required="">
                                            
                                        </div>
                                        <div class="col-sm-1 paddingsm">
                                            <input type="text" name="shemm" for="validation" enterText="numeric" id="shemm" class="form-control" required="">
                                           
                                        </div>
                                        <div class="col-sm-2 paddingsm">
                                            <input type="text" name="sheyy" for="validation" enterText="numeric" id="sheyy" class="form-control" >
                                            
                                        </div>
                                        <label for="inputEmail3" class="col-sm-4 control-label">day / month / year</label>
                                        <span id="error" style="color: yellow; display: none; font-size:10px;"></span>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-4 control-label">Date of birth (he)</label>
                                        <div class="col-sm-1 paddingsm">
                                            <input type="text" name="hedd" for="validation" enterText="numeric1" id="hedd" class="form-control" required="">
                                        </div>
                                        <div class="col-sm-1 paddingsm">
                                            <input type="text" name="hemm" for="validation" id="hemm" enterText="numeric1" class="form-control" required="">
                                        </div>
                                        <div class="col-sm-2 paddingsm">
                                            <input type="sheyy" name="heyy" for="validation" id="heyy" enterText="numeric1" class="form-control" required="">
                                        </div>
                                        <label for="inputEmail3" class="col-sm-4 control-label"></label>
                                        <span id="error1" style="color: yellow; display: none; font-size:10px;"></span>
                                    </div>
                                    <div class="form-group margin-top-chk">
                                        <label for="inputEmail3" class="col-sm-3 control-label">married</label>
                                        <div class="col-sm-1 no-padding">
                                            <input type="checkbox" class="checkbox_cus married" for="isChecked" name="married" value="married" id="married" class="form-control" required="">
                                            <label class="checkbox_label" for="married"></label>
                                        </div>
                                        <label for="inputEmail3" class="col-sm-3 control-label">In relation</label>
                                        <div class="col-sm-1 no-padding">
                                            <input type="checkbox" class="checkbox_cus married" for="isChecked" name="married" value="married" id="not_married" class="form-control" required="">
                                            <label class="checkbox_label" for="not_married"></label>
                                        </div>
                                        <label for="inputEmail3" class="col-sm-3 control-label">single</label>
                                        <div class="col-sm-1 no-padding">
                                            <input type="checkbox" class="checkbox_cus married" for="isChecked" name="married" value="married" id="Separeted" class="form-control" required="">
                                            <label class="checkbox_label" for="Separeted"></label>
                                        </div>
                                        <span id="checkedBox" style="display:none;font-size:13px;color:yellow;">Please select atlest one status</span>
                                    </div>
                                </div>
                                <div class="mainResid blue_gred">                                
                                    <h3 class="reg-main-residence">Main residence:</h3>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-6 control-label">Country</label>
                                        <div class="col-sm-6">
                                           <select id="country" name ="country" class="required form-control"></select>
                                           <span id="country-msg" style="display:none;font-size:10px;color:red;">Please Select Atlest one country</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-6 control-label"> Administrative district <p class="ParaS">(country, Bundesland, dapartment, regioni, comunidades, canton.....)</p></label>
                                        <div class="col-sm-6">
                                            <div class="margin-admin">
                                                <select name ="state" id ="state" class="form-control"></select>
                                                <span id="selectstate" style="display:none;font-size:10px;color:red;">Plese select state</span>
												 <script language="javascript">
														populateCountries("country", "state");
												 </script>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-6 control-label"><i>Next city</i> <i class="ParaS">(not mandatory)</i></label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" value=""/>
                                        </div>
                                    </div>                                
                                </div>  
                                <div class="yourLook">
                                    <h4>Your are looking for:</h4>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <span class="label">Couples</span>
                                            <input type="checkbox" class="checkbox_cus checkOne" name="married" value="married" id="CouplesCheck" class="form-control" required="">
                                            <label class="checkbox_label" for="CouplesCheck"></label>
                                        </div>
                                        <div class="col-sm-4">
                                            <span class="label">Ladies</span>
                                            <input type="checkbox" class="checkbox_cus checkOne" name="married" value="married" id="LadiesCheck" class="form-control" required="">
                                            <label class="checkbox_label" for="LadiesCheck"></label>
                                        </div>
                                        <div class="col-sm-4">
                                            <span class="label">Men</span>
                                            <input type="checkbox" class="checkbox_cus checkOne" name="married" value="married " id="MenCheck" class="form-control" required="">
                                            <label class="checkbox_label" for="MenCheck"></label>
                                        </div>
                                        <span id="checkedBox1" style="display:none;font-size:10px;color:yellow;">Please select atlest one status</span>
                                    </div>
                                </div>
                                <div class="termsCond">
                                    <p><a href="#" >We do not accept membership applications without profile photos</a> Please download here your profile picture (s)</p>
                                    <small>You can change your photo (s) later anytime in your “edit my profile” section</small> 
                                    <div class="form-group">
                                        <div class="col-sm-5">
                                            <input type="file" id="addPick1" style="display: none;"/>
                                            <label for="addPick1" class="btn">+ add picture 1</label>
                                        </div>
                                        <div class="col-sm-7 info">
                                            Her profile picture *)
                                        </div>
                                        <span id="pic1" style="display:none;font-size:10px;color:red;">Please upload Pic</span>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-5">
                                            <input type="file" id="addPick2" style="display: none;"/>
                                            <label for="addPick2" class="btn">+ add picture 1</label>
                                        </div>
                                        <div class="col-sm-7 info">
                                            Her profile picture *)
                                        </div>
                                        <span id="pic2" style="display:none;font-size:10px;color:red;">Please upload Pic</span>
                                    </div>
                                    <a href="#" class="text-right gotoTerm" >copyright conditions</a>
                                    <p><small>*)non ‐ pornographic pictures in jpg /maximum ......K</small> </p>
                                    <div class="text-center">
                                        <button type="button" class="btn_look btn" id ="regnext1">Send</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

	 <script>
	 $(document).ready(function(){



$('input,select').focus(function(){
$(this).next('span').remove('span');

});


  
$(".married").click(function() {
  $(".married").attr("checked", false); 
  $(this).attr("checked", true);
});

$(".checkOne").click(function() {
  $(".checkOne").attr("checked", false);
  $(this).attr("checked", true);
});
 
  

var specialKeys = new Array();
specialKeys.push(8); //Backspace
    $(function () {
                    
            $('input[enterText="numeric"]').bind("keypress", function (e) {
            var keyCode = e.which ? e.which : e.keyCode 
            var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
            $("#error").css("display", ret ? "none" : "inline");            
            $("#error").html('Please fill only Numeric value');  
            return ret;
        });
        $(function () {
                
        $('input[enterText="numeric1"]').bind("keypress", function (e) {
        var keyCode = e.which ? e.which : e.keyCode 
        var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
        $("#error1").css("display", ret ? "none" : "inline");            
        $("#error1").html('Please fill only Numeric value');  
        return ret;
    });
        });
});


$('#password').focusout(function(){
    if($(this).val().length > 0 && $(this).val().length < 8){
    $(this).after('<span style="font-size:10px;color:red;">Password must have have 8 charachters</span>');      
      hint=1;
    }
});



$("#regnext1").click(function () {
    var hint = 0;
    var count = 0;
    var count1 = 0;

    if($('#addPick1').val() == ""){
        $('#pic1').show();
        hint =1;
    }else{
        $('#pic1').hide();
    }
    if($('#addPick2').val() == ""){
        $('#pic2').show();
        hint =1;
    }else{
        $('#pic2').hide();
    }
    if(IsEmail($('#email').val()) == false){
        if($('#email').next('span').length == 0){
            $('#email').after('<span style="font-size:10px;">Please fill a valid Id</span>');
             hint = 1; 
       }       
    }

    $('.married').each(function(){        
        if($(this).is(':checked')){
           count++; 
        }
      
    });
    if(count == 0){
       hint = 1;
       $('#checkedBox1').show();
   }else{
    $('#checkedBox').hide();
   }

   $('.checkOne').each(function(){        
        if($(this).is(':checked')){
           count1++; 
        }
      
    });
    if(count1 == 0){
       hint = 1;
       $('#checkedBox1').show();
   }else{
    $('#checkedBox1').hide();
   }
    
	$('input[for="validation"]').each(function(){
       if($(this).val() == ''){
       	hint =1;
        if($(this).next('span').length == 0){
            $(this).after('<span style="font-size:10px;">Please fill required field</span>');
        }
       	
       } 
	});

    

    $('select option:selected').each(function(){

     if($(this).val() == -1 ){
        $('#country-msg').show();
        hint =1;
     }  
     if($(this).val() == '' ){
        $('#selectstate').show();
        hint =1;
     }               
    });

    if(hint == 0){
    alert(hint);
}

});


	 });
        
    </script>


    <script>
    function IsEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}


    </script>
}




<?php get_footer(); ?>			
			
