<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one
 * of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query,
 * e.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress  
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

	get_header(); 

	global $current_user;
	get_currentuserinfo();
	$get_user_meta = get_user_meta($current_user->ID);
	//echo '<pre>';print_r($get_user_meta); echo '</pre>';
	$user_type 		= get_user_meta($current_user->ID, 'user_type', true);
	$member_type 	= get_user_meta($current_user->ID, 'member_type', true);
	$approved 		= get_user_meta($current_user->ID,'approved_as_basic', true);
	$user_roles 	= $current_user->roles['0'];

	// set url to redirect on as user type
	$basicurl 	= get_page_link(247);
	$validaturl = get_page_link(426);
	$premiumurl = get_page_link(431);
	if(is_user_logged_in())
	{
		//===== In case if user is acceptance
		if($approved =='no' && $member_type =="couples")
		{
			$url = get_page_link(203);
			header("location: ".$url);
		}
		elseif($approved =='no' && $member_type =="ladies")
		{
			$url = get_page_link(433);
			header("location: ".$url);			
		}
		elseif($approved =='no' && $member_type =="gentleman")
		{
			$url = get_page_link(468);
			header("location: ".$url);			
		}		
		elseif($user_type =="basic")
		{
			header("location: ".$basicurl);
		}			
		elseif($user_type=="validated")
		{
			header("location: ".$validaturl);
		}
		elseif($user_type=="premium")
		{
			header("location: ".$premiumurl);	
		}
									
	}
?>
		  
<section class="container">
	<div class="homeLogin">			
		<div class="row">
			<div class="col-sm-5 pageContent">
				<?php $page_id ="32";  
              		$page_title = get_page($page_id);
		  		?>
				<h3><?php echo $page_title->post_title;  ?></h3>
				<span class="showContent"><?php echo $page_title->post_content;  ?></span>
			    <figure class="text-left">
					<?php
						$src = wp_get_attachment_image_src( get_post_thumbnail_id($page_title->ID), 'full' );
					?>
					<img style="max-height:350px;" src="<?php echo $src[0]; ?>" alt=""  class="img-responsive" />
				</figure>
			</div>				
												
			<div class="col-sm-7 no-padding-left">	
				<div class="">	
					<div class="col-sm-10">	<!--hedonism page -->
						<?php $pageid = "35"; $page_img = get_page($pageid); ?> 
						<figure class="text-left">
							<?php
								$srcc = wp_get_attachment_image_src( get_post_thumbnail_id($page_img->ID), 'full' );
							?>
							<img src="<?php echo $srcc[0]; ?>" alt="pic4" width="501" height="235" class="img-responsive" />				
						</figure>
						<?php echo $page_img->post_content;  ?>
					</div>
				</div>														
			</div>																	
		</div>
		
        <div class="row">	
			<div class="col-sm-5 loginSec ">
                            <div class="loginSecin">
				<?php $pageidlogin = "73";  $page_title12 = get_page($pageidlogin);
					echo $page_title12->post_content;
				?> 
				<?php 
	                if(isset($_REQUEST['login'])){
						if($_REQUEST['login'] == 'failed'){
						$messge = "User name and password does not match";
						}else if($_REQUEST['login'] == 'disabled'){
							$messge = "User account deactivated";
						}else if($_REQUEST['login'] == 'key'){
							$messge = "Your account is not activated yet";
						}
	                }
                ?>
                	
				<div class="form-horizontal">	
					<div class="account-wall">
						<?php
							if(function_exists('wplb_login')) {
								wplb_login();
							}
	                   
	                        if ( !is_user_logged_in() ) 
	                        { 
						?>
							<span class="textmsg" style="color:red"><?php echo $messge; ?></span>
							<div class="form-group notMember">
								<div class="col-sm-offset-2 col-sm-4">												
									not a member ?								
								</div>
								<div class="col-sm-4 text-left">
									<a href ="<?php echo get_permalink(45); ?>"  class="btn btn-success btn-sm signupBtn">Sign Up</a>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12 col-sm-offset-2 text-left">												
										<a href="<?php echo get_permalink(332); ?>" class="infoMember" >Info membership</a>												
								</div>											
							</div>
						<?php } ?>
					</div>							
				</div>	
                            </div>
			</div>				 
			<div class="col-sm-7 swissPortal newadded">
				<div class="">
					<?php $id12= "38"; $page12 = get_page($id12);  ?>					
					<h3> 
						<div class="float-right">
							<span class="airow-span">
								<img src="<?php echo site_url(); ?>/wp-content/uploads/2014/11/red-airrow.jpg" alt="red-airrow" width="9" height="9" />
							</span>
							<a class="our-highlight" href="<?php echo get_permalink( 330); ?>">
								<?php $highlight= "75"; $page12w = get_page($highlight);  ?>
								<?php  echo $page12w->post_title;  ?>
							</a>
						</div>
					</h3>
				    <div class="">
						<div class="row">
							<div class="col-sm-6">
								<figure class="text-left">
									<?php
										$srcs = wp_get_attachment_image_src( get_post_thumbnail_id($page12->ID), 'full' );
									?>
									<img src="<?php echo $srcs[0]; ?>" alt="img2" class="img-responsive" />
								</figure>
							</div>	
							<?php echo $page12->post_content;  ?>
						</div>
						<spna class="newContent"><?php  echo $page12->post_title;  ?></spna>
					</div>
				</div>
			</div>			
		</div>
	</div>
</section>

<?php
//get_sidebar();
get_footer(); ?>