<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
    <!--<![endif]-->
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width">
        <title><?php wp_title('|', true, 'right'); ?></title>
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
        <!--[if lt IE 9]>
        <script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
        <![endif]-->
        <link rel="shortcut icon" href="<?php echo bloginfo('template_url'); ?>/images/favicon.ico" type="image/x-icon" />
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.css">
        <link href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo get_template_directory_uri(); ?>/css/fonts.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo get_template_directory_uri(); ?>/css/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/style.css">
        <link href="<?php echo get_template_directory_uri(); ?>/css/autoCompleteStyle.css" rel="stylesheet" type="text/css"/>	
        <script src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.11.1.min.js" type="text/javascript"></script>			
        <script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.js" type="text/javascript"></script>

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
        <script type="text/javascript" src="http://ajax.microsoft.com/ajax/jQuery.Validate/1.6/jQuery.Validate.min.js"></script>
        <script type="text/javascript" src="<?php echo bloginfo('template_url'); ?>/js/browser_and_os_classes.js"></script>

<!--script src="<?php //echo content_url() . '/themes/ckeditor/ckeditor.js';  ?>" type="text/javascript"></script-->       
        <script type= "text/javascript" src = "<?php echo bloginfo('template_url'); ?>/js/countries_list.js"></script>
        <script src="<?php echo bloginfo("template_url"); ?>/js/sample-registration-form-validation.js"></script>
 <!--       <script src="https://cdn.ckeditor.com/4.4.5.1/standard/ckeditor.js"></script> -->
        <script src="<?php echo get_site_url(); ?>/wp-content/themes/ckeditor/ckeditor.js"></script>
        <script type="text/javascript" src="<?php echo bloginfo('template_url'); ?>/js/autoload.js"></script>

        <!--- scroll script and style -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.mCustomScrollbar.concat.min.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.mCustomScrollbar.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.mousewheel.min.js" type="text/javascript"></script>
        <script>
       (function ($) {
           $(window).load(function () {
               $(".dating-text-box,.mCustomScrollbar,.Profile-Part-BI,.cus_scrolln,.cus_scroll,.eropic-profile-Part,.Favourite-Part2,#content-1,.Favourite-Part,.text-box-round").mCustomScrollbar({
                   mouseWheel: {scrollAmount: 300}
               });
           });
       })(jQuery);
        </script>
    <?php 
    
    global $current_user;
    global $wpdb;
    get_currentuserinfo();
    $user_type_name =  get_user_meta($current_user->ID ,'user_type', true );
    $result = $wpdb->get_row("select $user_type_name from `wp_admin_portal_setting` where `portal_setting_text` = 'Max. character'");
    $countLimit =  $result->$user_type_name;
    ?>
    <script type="text/javascript">
         // Default Config
        var defaultConfig = {
            showWordCount: true,
            showCharCount: true,
            countSpacesAsChars: false,
            charLimit: '<?php echo $countLimit; ?>',
            wordLimit: 'unlimited',
            countHTML: false
        };
</script>
        

        <?php wp_head(); ?>
    </head>

    <body <?php body_class(); ?>>
        <div class="container-fluid">
            <header class="header">
					<?php // echo __('gireesh','twentyfourteen');?>
                <?php 
                $user_roles = $current_user->roles['0'];
                if ($user_roles == "administrator") {
                    ?>
                    <nav role="navigation" class="navbar"> 
                        <div class="navbar-header">                       
                            <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a href="<?php echo home_url(); ?>" class="navbar-brand">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/logo.jpg" alt="logo" class="img-responsive" />
                            </a>
                        </div>
                        <div class="navbar-collapse collapse" id="navbar">
                            <?php echo do_shortcode('[tp widget="flags/tpw_flags_css.php"]'); ?>
                        </div><!--/.nav-collapse -->                        
                    </nav> 
                    <?php
                } elseif (!is_user_logged_in()) {
                    ?>
                    <nav role="navigation" class="navbar">                        
                        <div class="navbar-header">
                            <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a href="<?php echo home_url(); ?>" class="navbar-brand">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/logo.jpg" alt="logo" class="img-responsive" />
                            </a>
                        </div>
                        <div class="navbar-collapse collapse" id="navbar">
    <?php echo do_shortcode('[tp widget="flags/tpw_flags_css.php"]'); ?>
                        </div><!--/.nav-collapse -->                       
                    </nav>

    <?php
}
?>
            </header>
             <!--   Start Get the array for Comparison         -->
<?php
global $portalsetting,$visibilty_setting,$operational_setting,$search_setting,$album_setting;
$portalsetting=array();
$visibilty_setting=array();
$operational_setting=array();
$search_setting=array();
$album_setting=array();
$current_user_ID = get_current_user_id();
$user_type = get_user_meta($current_user_ID, 'user_type', true);
$get_current_user_info = get_user_meta($current_user_ID);
$operational_setting = $wpdb->get_results("SELECT $user_type FROM wp_admin_portal_setting WHERE portal_setting_type='operational setting'");
$visibilty_setting = $wpdb->get_results("SELECT $user_type FROM wp_admin_portal_setting WHERE portal_setting_type='visibilty setting'");
$search_setting = $wpdb->get_results("SELECT $user_type FROM wp_admin_portal_setting WHERE portal_setting_type='search setting'");
$portalsetting = $wpdb->get_results("SELECT $user_type FROM wp_admin_portal_setting WHERE portal_setting_type='portal setting'");
$album_setting = $wpdb->get_results("SELECT $user_type FROM wp_admin_portal_setting WHERE portal_setting_type='album setting'");
?>
            <!--   End Get the array for Comparison    -->           
                <?php
                if (is_user_logged_in()) {
                    $check_user_type = get_user_meta($current_user->ID, 'member_type', true);
                    $check_member_type = get_user_meta($current_user->ID, 'user_type', true);
                    $user_approved = get_user_meta($current_user->ID, 'approved_as_basic', true);
                    ?>
                <nav role="navigation" class="navbar">  
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <a href="<?php echo home_url(); ?>" class="navbar-brand"><?php echo do_shortcode('[site_logo]'); ?></a>
                        </div>
                        <div class="navbar-collapse collapse" id="navbar">
    <?php
    $user_roles = $current_user->roles['0'];
    if ($check_user_type == "couples" && $user_approved == "no") {
        $defaults = array(
            'theme_location' => '',
            'menu' => 'user_admin_menu',
            'container' => 'div',
            'container_class' => '',
            'container_id' => '',
            'menu_class' => 'menu',
            'menu_id' => '',
            'echo' => true,
            'fallback_cb' => 'wp_page_menu',
            'before' => '',
            'after' => '',
            'link_before' => '',
            'link_after' => '',
            'items_wrap' => '<ul id="%1$s" class="nav navbar-nav profile-nav disable-btn">%3$s</ul>',
            'depth' => 0,
            'walker' => ''
        );
        echo wp_nav_menu($defaults);
    } elseif ($check_user_type == "gentleman" && $user_approved == "no") {
        $defaults = array(
            'theme_location' => '',
            'menu' => 'gentlemen_menu',
            'container' => 'div',
            'container_class' => '',
            'container_id' => '',
            'menu_class' => 'menu',
            'menu_id' => '',
            'echo' => true,
            'fallback_cb' => 'wp_page_menu',
            'before' => '',
            'after' => '',
            'link_before' => '',
            'link_after' => '',
            'items_wrap' => '<ul id="%1$s" class="nav navbar-nav profile-nav disable-btn">%3$s</ul>',
            'depth' => 0,
            'walker' => ''
        );
        echo wp_nav_menu($defaults);
    } elseif ($check_user_type == "ladies" && $user_approved == "no") {
        $defaults = array(
            'theme_location' => '',
            'menu' => 'Ladies_menu',
            'container' => 'div',
            'container_class' => '',
            'container_id' => '',
            'menu_class' => 'menu',
            'menu_id' => '',
            'echo' => true,
            'fallback_cb' => 'wp_page_menu',
            'before' => '',
            'after' => '',
            'link_before' => '',
            'link_after' => '',
            'items_wrap' => '<ul id="%1$s" class="nav navbar-nav profile-nav disable-btn">%3$s</ul>',
            'depth' => 0,
            'walker' => ''
        );
        echo wp_nav_menu($defaults);
    } elseif ($check_user_type == "couples") {
        $defaults = array(
            'theme_location' => '',
            'menu' => 'user_admin_menu',
            'container' => 'div',
            'container_class' => '',
            'container_id' => '',
            'menu_class' => 'menu',
            'menu_id' => '',
            'echo' => true,
            'fallback_cb' => 'wp_page_menu',
            'before' => '',
            'after' => '',
            'link_before' => '',
            'link_after' => '',
            'items_wrap' => '<ul id="%1$s" class="nav navbar-nav profile-nav">%3$s</ul>',
            'depth' => 0,
            'walker' => ''
        );
        echo wp_nav_menu($defaults);
    } elseif ($check_user_type == "ladies") {
        $defaults = array(
            'theme_location' => '',
            'menu' => 'Ladies_menu',
            'container' => 'div',
            'container_class' => '',
            'container_id' => '',
            'menu_class' => 'menu',
            'menu_id' => '',
            'echo' => true,
            'fallback_cb' => 'wp_page_menu',
            'before' => '',
            'after' => '',
            'link_before' => '',
            'link_after' => '',
            'items_wrap' => '<ul id="%1$s" class="nav navbar-nav profile-nav">%3$s</ul>',
            'depth' => 0,
            'walker' => ''
        );
        echo wp_nav_menu($defaults);
    } elseif ($check_user_type == "gentleman") {
        $defaults = array(
            'theme_location' => '',
            'menu' => 'gentlemen_menu',
            'container' => 'div',
            'container_class' => '',
            'container_id' => '',
            'menu_class' => 'menu',
            'menu_id' => '',
            'echo' => true,
            'fallback_cb' => 'wp_page_menu',
            'before' => '',
            'after' => '',
            'link_before' => '',
            'link_after' => '',
            'items_wrap' => '<ul id="%1$s" class="nav navbar-nav profile-nav">%3$s</ul>',
            'depth' => 0,
            'walker' => ''
        );
        echo wp_nav_menu($defaults);
    } else {
        wp_redirect(home_url());
    }
    ?>
                    </div>
                </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
    </nav>
            <?php if ($check_user_type == 'ladies' && $portalsetting[1]->$check_member_type == '0') { ?>
            <script type="text/javascript">
                window.onload = function () {
                    $("li#menu-item-571").parent().addClass('disable-btn');
                    $("#menu-item-571").addClass('acceptanceuser');
                };
            </script>
            <?php } ?>
            <?php if ($check_user_type == 'couples' && $portalsetting[1]->$check_member_type == '0') { ?>
            <script type="text/javascript">
                window.onload = function () {
                    $("li#menu-item-570").parent().addClass('disable-btn');
                    $("#menu-item-570").addClass('acceptanceuser');
                };
            </script>
            <?php } ?>
            <?php if ($check_user_type == 'gentleman' && $portalsetting[1]->$check_member_type == '0') { ?>
            <script type="text/javascript">
                window.onload = function () {
                    $("li#menu-item-572").parent().addClass('disable-btn');
                    $("#menu-item-572").addClass('acceptanceuser');
                };
            </script>
            <?php } ?>
        <div class="container-fluid">
            <div class="selectLanguages">
                <span>Change languages</span>
                <div  class="languageList">
                <?php echo do_shortcode('[tp widget="flags/tpw_flags_css.php"]'); ?>
                </div>
            </div>
        </div>         
<?php }
?>
