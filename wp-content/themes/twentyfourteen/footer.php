<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>
</div>

	<?php wp_footer(); ?>
	<footer class="footer-bottom">
			<div class="container">
				<?php

$defaults = array(
	'theme_location'  => '',
	'menu'            => 'footer_menu',
	'container'       => 'div',
	'container_class' => 'col-md-12 text-right',
	'container_id'    => '',
	'menu_class'      => 'col-md-12 text-right',
	'menu_id'         => '',
	'echo'            => true,
	'fallback_cb'     => 'wp_page_menu',
	'before'          => '',
	'after'           => '',
	'link_before'     => '',
	'link_after'      => '',
	'items_wrap'      => '<ul id="%1$s" class="list-inline footer">%3$s</ul>',
	'depth'           => 0,
	'walker'          => ''
);

wp_nav_menu( $defaults );

?>

			</div>
		
		</footer>
		
</body>
</html>
