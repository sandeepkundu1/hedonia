/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For complete reference see:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	// The toolbar groups arrangement, optimized for two toolbar rows.	

	config.toolbarGroups = [
    { name: 'document',    groups: [ 'mode', 'document', 'doctools' ] },
    { name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
    { name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
    { name: 'forms' },
    '/',
    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
    { name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
    { name: 'links' },
    { name: 'insert' },
    '/',
    { name: 'styles' },
    { name: 'colors' },
    { name: 'tools' },
    { name: 'others' },
    { name: 'about' }
];

	/**
	 * Whether the replaced element (usually a `<textarea>`)
	 * is to be updated automatically when posting the form containing the editor.
	 *
	 * @cfg
	 */
	autoUpdateElement= true;

	/**
	 * The user interface language localization to use. If left empty, the editor
	 * will automatically be localized to the user language. If the user language is not supported,
	 * the language specified in the {@link CKEDITOR.config#defaultLanguage}
	 * configuration setting is used.
	 *
	 *		// Load the German interface.
	 *		config.language = 'de';
	 *
	 * @cfg
	 */
	config.language = 'en';

	/**
	 * The language to be used if the {@link CKEDITOR.config#language}
	 * setting is left empty and it is not possible to localize the editor to the user language.
	 *
	 *		config.defaultLanguage = 'it';
	 *
	 * @cfg
	 */
	config.defaultLanguage = '';


	// Remove some buttons provided by the standard plugins, which are
	// not needed in the Standard(s) toolbar.
	//config.removeButtons = 'Underline,Subscript,Superscript';

	// Set the most common block elements.
	//config.format_tags = 'p;h1;h2;h3;h4;h5;h6;div';

	config.enterMode = CKEDITOR.ENTER_P;
	config.forceEnterMode= false;
	config.shiftEnterMode = CKEDITOR.ENTER_BR;
	
	config.entities = false;
	config.entities_latin = false;
	config.ForceSimpleAmpersand = true;
	config.templates_replaceContent = false;
	config.forcePasteAsPlainText = true;

	config.htmlEncodeOutput = false;
	config.enterMode = CKEDITOR.ENTER_P;
	config.enterMode = CKEDITOR.ENTER_DIV;
	config.enterMode = CKEDITOR.ENTER_BR;
	config.extraPlugins = 'wordcount';
	config.wordcount = {

    // Whether or not you want to show the Word Count
    showWordCount: false,

    // Whether or not you want to show the Char Count
    showCharCount: true,

     // Whether or not to include Html chars in the Char Count
     countHTML: false
};
	config.specialChars = config.specialChars.concat( [ '&alpha;', 'Alpha' ],
      [ '&beta;', 'Beta' ],
      [ '&gamma;', 'Gamma' ],
      [ '&delta;', 'Delta' ],
      [ '&epsilon;', 'Epsilon' ],
      [ '&zeta;', 'Zeta' ],
      [ '&eta;', 'Eta' ],
      [ '&theta;', 'Theta' ],
      [ '&iota;', 'Iota' ],
      [ '&kappa;', 'Kappa' ],
      [ '&lambda;', 'Lambda' ],
      [ '&mu;', 'Mu' ],
      [ '&nu;', 'Nu' ],
      [ '&xi;', 'Xi' ],
      [ '&omicron;', 'Omicron' ],
      [ '&pi;', 'Pi' ],
      [ '&rho;', 'Rho' ],
      [ '&sigma;', 'Sigma' ],
      [ '&sigma;', 'Sigma' ],
      [ '&tau;', 'Tau' ],
      [ '&upsilon;', 'Upsilon' ],
      [ '&phi;', 'Phi' ],
      [ '&chi;', 'Chi' ],
      [ '&psi;', 'Psi' ],
      [ '&omega;', 'Omega' ]);
	
	// Simplify the dialog windows.
	config.removeDialogTabs = 'image:advanced;link:advanced';
};
