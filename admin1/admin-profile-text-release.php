<link rel="shortcut icon" href="http://hedon.trendhosting.ch/wp-content/themes/twentyfourteen/images/favicon.ico" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="http://hedon.trendhosting.ch/wp-content/themes/twentyfourteen/css/bootstrap.css">
<link href="http://hedon.trendhosting.ch/wp-content/themes/twentyfourteen/css/font-awesome.css" rel="stylesheet" type="text/css"/>
<link href="http://hedon.trendhosting.ch/wp-content/themes/twentyfourteen/css/fonts.css" rel="stylesheet" type="text/css"/>
<link href="http://hedon.trendhosting.ch/wp-content/themes/twentyfourteen/css/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="http://hedon.trendhosting.ch/wp-content/themes/twentyfourteen/css/style.css">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" src="http://hedon.trendhosting.ch/wp-content/themes/twentyfourteen/js/bootstrap.js"></script>
<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/wp-config.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/wp-includes/general-template.php');
global $wpdb;
?>
<header class="header">
    <nav class="navbar" role="navigation">                        
        <div class="navbar-header" style="width: 100%;">            
            <a class="navbar-brand" href="http://hedon.trendhosting.ch">
                <img class="img-responsive" alt="logo" src="http://hedon.trendhosting.ch/wp-content/themes/twentyfourteen/images/logo.jpg">
            </a>
            <p class="profileTextInfo"><em>All profiles and members pictures must be checked BEFORE be placed in portal *)</em></p>
        </div>        
    </nav>
</header>
<div class="container">
    <div class="row">
        <div class="col-sm-6">
            <div class="panel-admin">
                <div class="panel-admin-up"><h3 class="col-sm-12">ADMIN Profile Text Release</h3></div>
                <div class="panel-admin-down textrwrapper">
                    <div class="mast-pass row form-group">
                        <div class="col-sm-7">                            
                            <label>Master password</label>
                            <input type="text" class="form-control">                            
                        </div>
                        <div class="col-sm-4"><a class="arrow-right-btn" href="#"><img src="http://hedon.trendhosting.ch/wp-content/themes/twentyfourteen/images/icon-right.png"></a></div>
                    </div>
                    <p class="edit-text-line">New or edited <strong>texts must be checked according <a href="#" >Swiss law</a></strong> before being public</p>
                    <div class="mast-pass row form-group">
                        <div class="col-sm-7">                            
                            <label>Text of member</label>
                            <select name="text_of_member" id="text_of_member" class="text_of_member form-control">
                                <?php
                                $list_users = get_users(array(
                                    "meta_key" => "approved_as_basic",
                                    "meta_value" => "yes"
                                ));
                                foreach ($list_users as $value) {
                                    $describe_status = get_user_meta($value->ID, 'describe_status', true);
                                    if ($describe_status == '') {
                                        ?>
                                        <option value="<?php echo $value->ID; ?>"><?php echo get_user_meta($value->ID, 'nickname', true); ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>              
                        </div>                        
                    </div>
                    <div class="col-sm-offset-2 col-sm-10"><textarea id="member_introduction" class="form-control" rows="4" cols="50" style="min-height: 350px;"></textarea></div>                    
                    <div class="buttonBar col-sm-offset-2 col-sm-10">
                        <a href="javascript:void(0)" id="accept_content" class="accept_content btn pull-left acceptBtn">accept</a>
                        <a href="javascript:void(0)" id="refused_content" class="refused_content btn pull-right refuseBtn">refused</a> 
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="panel-admin">
                <div class="panel-admin-up"><h3 class="col-sm-12">ADMIN Photo Release</h3></div>            

                <div class="panel-admin-down textrwrapper">
                    <div class="mast-pass row form-group">
                        <div class="col-sm-7">                            
                            <label>Master password</label>
                            <input type="text" class="form-control">                            
                        </div>
                        <div class="col-sm-4"><a class="arrow-right-btn" href="#"><img src="http://hedon.trendhosting.ch/wp-content/themes/twentyfourteen/images/icon-right.png"></a></div>
                    </div>
                    <p class="edit-text-line">Each new members picture has to be checked according <strong><a href="#" >Swiss law</a></strong></p>
                    <div class="mast-pass row form-group">
                        <div class="col-sm-7">                            
                            <label>Picture of member</label>
                            <select name="album_of_member" id="album_of_member" class="album_of_member form-control">
                                <?php
                                foreach ($list_users as $value) {
                                    $albumb_id = $wpdb->get_results("SELECT id FROM user_create_album WHERE user_id=" . $value->ID . "");
                                    if (!empty($albumb_id)) {
                                        ?>
                                        <option value="<?php echo $value->ID; ?>"><?php echo get_user_meta($value->ID, 'nickname', true); ?>
                                        </option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>            
                        </div>                        
                    </div>
                    <div class="col-sm-offset-2 col-sm-10">
                        <div class="slider-main">
                            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner" role="listbox">
                                </div>
                                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                    <span class="left-arrow" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                    <span class="right-arrow" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div> 
                    </div>
                    <div class="buttonBar pickside">
                        <a href="javascript:void(0)" id="accept_image" class="accept_image btn pull-left acceptBtn">accept</a>
                        <a href="javascript:void(0)" id="refused_image" class="refused_image pull-right refuseBtn">refused</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <p class="termsText"><em>*) This job can be done by third parties. For these moderator we need a special password for access only to this area (not the Master Password !)</em></p>
</div>



<script>
    $(document).ready(function () {
        var id = $('select#text_of_member option:selected').val();
        $.ajax({
            type: "POST",
            url: "http://hedon.trendhosting.ch/wp-admin/admin-ajax.php",
            data: ({action: "get_text_of_member_profile", id: id}),
            success: function (data)
            {
                var intro = data.trim()
                if (intro == '')
                {
                    $('#member_introduction').val("No Introduction");
                }
                else
                {
                    $('#member_introduction').val(intro);
                }
            }
        });
    })
</script>
<script>
    $(document).ready(function () {
        $('select#text_of_member').on('change', function () {
            var id = this.value;
            $.ajax({
                type: "POST",
                url: "http://hedon.trendhosting.ch/wp-admin/admin-ajax.php",
                data: ({action: "get_text_of_member_profile", id: id}),
                success: function (data)
                {
                    var intro = data.trim()
                    if (intro == '')
                    {
                        $('#member_introduction').val("No Introduction");
                    }
                    else
                    {
                        $('#member_introduction').val(intro);
                    }
                }
            });
        });
        $('#accept_content').click(function () {
            var id = $('select#text_of_member option:selected').val();
            $.ajax({
                type: "POST",
                url: "http://hedon.trendhosting.ch/wp-admin/admin-ajax.php",
                data: ({action: "get_text_of_member_accept_or_refused", id: id, status: 'accept'}),
                success: function (data)
                {
                    location.reload();
                }
            });
        });
        $('#refused_content').click(function () {
            var id = $('select#text_of_member option:selected').val();
            $.ajax({
                type: "POST",
                url: "http://hedon.trendhosting.ch/wp-admin/admin-ajax.php",
                data: ({action: "get_text_of_member_accept_or_refused", id: id, status: 'refused'}),
                success: function (data)
                {
                    location.reload();
                }
            });
        });
    });
</script>
<script>
    $(document).ready(function () {
        var id = $('select#album_of_member option:selected').val();
        $.ajax({
            type: "POST",
            url: "http://hedon.trendhosting.ch/wp-admin/admin-ajax.php",
            data: ({action: "get_album_of_member", id: id}),
            success: function (data)
            {
                $('.carousel-inner').html(data);

            }
        });
    });
</script>
<script>
    $('select#album_of_member').on('change', function () {
        var id = this.value;
        $.ajax({
            type: "POST",
            url: "http://hedon.trendhosting.ch/wp-admin/admin-ajax.php",
            data: ({action: "get_album_of_member", id: id}),
            success: function (data)
            {
                $('.carousel-inner').html(data);

            }
        });
    });
</script>
<script>
    $(document).ready(function () {
        $('#accept_image').click(function () {
            var img_id = $('.active').attr('id');
            var status = 1;
            $.ajax({
                type: "POST",
                url: "http://hedon.trendhosting.ch/wp-admin/admin-ajax.php",
                data: ({action: "get_album_accept_refused", img_id: img_id, status: status}),
                success: function (data)
                {
                    location.reload();
                }
            });
        });
        $('#refused_image').click(function () {
            var img_id = $('.active').attr('id');
            var status = 2;
            $.ajax({
                type: "POST",
                url: "http://hedon.trendhosting.ch/wp-admin/admin-ajax.php",
                data: ({action: "get_album_accept_refused", img_id: img_id, status: status}),
                success: function (data)
                {
                    location.reload();
                }
            });
        });
    });
</script>