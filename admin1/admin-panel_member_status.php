<link rel="shortcut icon" href="http://hedon.trendhosting.ch/wp-content/themes/twentyfourteen/images/favicon.ico" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="http://hedon.trendhosting.ch/wp-content/themes/twentyfourteen/css/bootstrap.css">
<link href="http://hedon.trendhosting.ch/wp-content/themes/twentyfourteen/css/font-awesome.css" rel="stylesheet" type="text/css"/>
<link href="http://hedon.trendhosting.ch/wp-content/themes/twentyfourteen/css/fonts.css" rel="stylesheet" type="text/css"/>
<link href="http://hedon.trendhosting.ch/wp-content/themes/twentyfourteen/css/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="http://hedon.trendhosting.ch/wp-content/themes/twentyfourteen/css/style.css">
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/wp-config.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/wp-includes/general-template.php');
global $wpdb;
?>
<header class="header">
    <nav role="navigation" class="navbar">                        
        <div style="width: 100%;" class="navbar-header">            
            <a href="http://hedon.trendhosting.ch" class="navbar-brand">
                <img src="http://hedon.trendhosting.ch/wp-content/themes/twentyfourteen/images/logo.jpg" alt="logo" class="img-responsive">
            </a>            
        </div>        
    </nav>
</header>
<div class="member-admin-page admin-panal">
    <div class="col-sm-2">
        <img class="user-icon" alt="user" src="http://hedon.trendhosting.ch/wp-content/themes/twentyfourteen/images/user-img.png">
    </div>
    <div class="col-sm-10">
        <div class="panel-admin">
            <div class="panel-admin-up">
                <div class="col-sm-4">
                    <h3 class="heading-title">ADMIN PANEL for members status</h3>
                </div>
                <div class="col-sm-8 mast-pass">
                    <div class="col-sm-9">
                        <div class="form-group">
                            <label>Operation officer password</label>
                            <input type="text" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <a class="arrow-right-btn" href="#"><img src="http://hedon.trendhosting.ch/wp-content/themes/twentyfourteen/images/icon-right.png">
                        </a>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="panel-title">Panel for new members</div>
            <div class="clear"></div>
            <div class="panel panel-admin-down">
                <div class="row form-group">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3">MEMBER</label>
                            <div class="col-md-7">
                                <?php
                                $args = array(
                                    'meta_key' => 'approved_as_basic',
                                    'meta_value' => 'no'
                                );
                                $blogusers = get_users($args);
                                ?>
                                <select class="form-control member-select blue-control" name="username" id="username">
                                    <option>Select Member</option>
                                    <?php
                                    foreach ($blogusers as $user) {
                                        ?>
                                        <option value="<?php echo $user->ID; ?>"><?php echo '<span>' . esc_html($user->user_login) . '</span>'; ?></option>
                                    <?php } ?>
                                </select>
                                <span class="bar_img" style="display: none;float:right">
                                <img width="60px" height="12px" src="http://hedon.trendhosting.ch/wp-content/themes/twentyfourteen/images/ajax-status.gif">
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-4">Type of Membership</label>
                            <div class="col-md-8">
                                <input type="text" name="typeofmember" id="typeofmember" value="" class="form-control gray-control"/>
                            </div>
                            <div class="validate_pickds" id ="validate_pick">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-12 activate-profile">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="row">
                                    <label class="col-md-7 ckeckLabel">Activate profile 1)</label>
                                    <div class="col-md-4">                                        
                                        <input id="change_act" type="checkbox" for="change_act_type" class="checkbox-profile-1 checkbox_cus" name="activate" value="basic_member"/>
                                        <label class="checkbox_label check_pro" for="change_act"></label>
                                        <span id="msg1"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="row">
                                    <label class="col-md-6 ckeckLabel">Activate as ve basic member:</label>
                                    <div class="col-md-6">
                                        <input id="act_basic" type="checkbox" for="change_act_type" class="checkbox-profile-1 checkbox_cus" name="activate" value="validated" />
                                        <label class="checkbox_label check_pro_val" for="act_basic"></label>
                                        <span id="msg2"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="row">
                                    <label class="col-md-6 ckeckLabel">Activate as premium:</label>
                                    <div class="col-md-6">
                                        <input id="act_pre" type="checkbox" for="change_act_type" class="checkbox-profile-1 checkbox_cus" name="activate" value="premium"/>
                                        <label class="checkbox_label check_pro_pre" for="act_pre"></label>
                                        <span id="msg2"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row form-group veri-code">                   
                    <div class="col-md-4">
                        <div class="row">
                            <label class="col-md-5">Validation code:</label>
                            <div class="col-md-6">
                                <input type="text" name="validationcode" id="validationcode" value="" required class="form-control"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="row form-group">
                            <label class="col-md-4">User Type:</label>
                            <div class="col-md-7"><input type="text" value="" name="transfer" id="transfer" class="form-control"/></div>
                        </div>
                        <div class="row">
                            <label class="col-md-4">Profile Name</label>
                            <div class="col-md-7"><input type="text"  name="profilename" id="profilename" value="" class="form-control"/></div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <label>
                            Verified
                        </label>
                        <a href="javascript:void(0)" id="ok_validated" class="ok_validated btn btn-default">OK</a>
                    </div>                   
                </div>
                <div class="col-md-12 verfiled-bar">

                    <span id="msg3"></span>
                </div>
            </div><!--End Panel-->
            <div class="clearfix"></div>            
            <div class="panel-title">Panel for members
            </div>
            <div class="panel panel-admin-down">
                <div class="row form-group">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3">MEMBER</label>
                            <div class="col-md-7">
                                <?php
                                $args = array(
                                    'meta_key' => 'approved_as_basic',
                                    'meta_value' => 'yes'
                                );
                                $blogusers = get_users($args);
                                ?>
                                <select class="form-control member-select blue-control" name="username" id="panel_username">
                                    <option>Select Member</option>
                                    <?php
                                    foreach ($blogusers as $user) {
                                        ?>
                                        <option value="<?php echo $user->ID; ?>"><?php echo '<span>' . esc_html($user->user_login) . '</span>'; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-4 gold-control">Type of Membership</label>
                            <div class="col-md-8">
                                <input type="text" name="panel_member_type" id="panel_member_type" class="panel_member_type form-control gold-control">
                            </div>
                        </div>
                    </div>
                </div>
                <!--First Row-->
                <!--second Row-->

                <div class="row form-group panel-for-member-second-row">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3">Member Since:</label>
                            <div class="col-md-4">
                                <input class="form-control text-center" type="text" value="25/12/2014"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-4">Premium expires at</label>
                            <div class="col-md-4">
                                <input class="premium-expires-input form-control text-center" type="text" value="25/03/2015"/>
                            </div>
                        </div>
                    </div>
                </div>

                <!--second Row-->
                <!--Third Row-->
                <div class="panel-for-member-second-row row form-group">
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-6 block-pro">                                
                                <label class="ckeckLabel" >Block Profile </label>
                                    <input id="blockPro" type="checkbox" class="checkbox-profile-1 checkbox_cus"/>
                                    <label class="checkbox_label check_block_pro" for="blockPro"></label>
                            </div>
                            <div class="col-md-6 delete-pro">
                                <label class="ckeckLabel" >Delete Profile </label>
                                <input id="dlt_pro" type="checkbox" class="checkbox-profile-1 checkbox_cus"/>
                                <label class="checkbox_label check_del_pro" for="dlt_pro"></label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 free-member">
                        <div class="row">
                            <div class="col-md-4">
                                <label class="ckeckLabel">
                                    Free premium membership
                                </label>
                            </div>
                            <div class="col-md-4">
                                <label>
                                    <label class="ckeckLabel">For 6 Months</label> 
                                    <input id="month6" type="checkbox" class="checkbox-profile-1 checkbox_cus"/>
                                    <label class="checkbox_label check_pro_val" for="month6"></label>
                                </label>
                            </div>
                            <div class="col-md-4">
                                <label>
                                    <label class="ckeckLabel">For 12 Months </label>
                                    <input id="month12" type="checkbox" class="checkbox-profile-1 checkbox_cus"/>
                                    <label class="checkbox_label check_pro_val" for="month12"></label>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Third Row-->                
                <label class="notestext">Block profile because of some disputes between members/suspicion of stolen pictures e.g. until the situation has been clarified
                </label>                
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#username').on('change', function () {
            var user = '';
            var user_info = '';
            var userID = this.value;
            $('.bar_img').show();
            jQuery.ajax({
                type: "POST",
                url: "http://hedon.trendhosting.ch/wp-admin/admin-ajax.php",
                data: ({action: 'get_admin_panel_for_status_member', userID: userID}),
                success: function (data)
                {
                    $('.bar_img').hide();
                    user = data.trim();
                    console.log(user);
                    user_info = user.split(',');
                    $.each(user_info, function (i)
                    {
                        if (i == 0)
                        {
                            $('#typeofmember').val(this);
                        }
                        else if (i == 1)
                        {
                            $('#profilename').val(this);
                        }
                    });
                }
            });

        });
        $("input:checkbox").on('click', function () {
            var $box = $(this);
            if ($box.is(":checked"))
            {
                var group = "input:checkbox[name='" + $box.attr("name") + "']";
                $(group).prop("checked", false);
                $box.prop("checked", true);
            }
            else
            {
                $box.prop("checked", false);
            }
        });
        $('#ok_validated').click(function () {
            var id = $('select#username option:selected').val();
            var user_type = '';
            $('input[for="change_act_type"]').each(function ()
            {
                if ($(this).attr('type') == 'checkbox')
                {
                    if ($(this).is(':checked'))
                    {
                        user_type = this.value;
                    }

                }

            });
            if (id != 'Select Member')
            {
                jQuery.ajax({
                    type: "POST",
                    url: "http://hedon.trendhosting.ch/wp-admin/admin-ajax.php",
                    data: ({action: 'get_admin_panel_change_status', id: id, user_type: user_type}),
                    success: function (data)
                    {
                        $('#msg3').html(data);
                    }
                });
            }
        });
        $('#panel_username').on('change', function () {
            var id = this.value;
            $.ajax({
                type: "POST",
                url: "http://hedon.trendhosting.ch/wp-admin/admin-ajax.php",
                data: ({action: 'get_admin_status_old_member', id: id}),
                success: function (data)
                {
                    alert(data);
                }
            });
        })
    });
</script>