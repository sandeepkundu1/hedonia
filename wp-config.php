<?php
/** 
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information by
 * visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'hedon_new');

/** MySQL database username */
define('DB_USER', 'hedon_new');

/** MySQL database password */
define('DB_PASSWORD', 'hedon_new');

/** MySQL hostname */
define('DB_HOST', '127.0.0.1');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('COOKIE_DOMAIN', false);

/**#@+
 * Authentication Unique Keys.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link http://api.wordpress.org/secret-key/1.1/ WordPress.org secret-key service}
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '!qz2@A5Dn%2!8IpNz8q(&6Mn7LNpu7RiTFdLP3jMO)7LYsoyXWzwp*(eKp5uP$18');
define('SECURE_AUTH_KEY',  'Mhvw(AT(CTK92Xmjgsx*DEKbu(MYdGIxefo5r1%)tC2hx4MYg@xihFq2uIZGR#M1');
define('LOGGED_IN_KEY',    '*NWVHy@bw9cnkd*6(XBevkDKX7gIgE$n3npmv*Df4O@Ds@XuW$$ZJfbZCEDwEyey');
define('NONCE_KEY',        'DlU0LA3!PVlrqV$(lvgqYrF11j(NhYRP@gv$&3eqWM)dveoeb4&!ubtxg4VR$aeC');
define('AUTH_SALT',        'RHhTwlRIxW*WDERe0Xs1ZVXkynfyCY@CeRJ!&edHmsa$)8UmsVUkerI%n&yU)^ro');
define('SECURE_AUTH_SALT', 'c%Ls1vfpaHD*i%)#fKlJXmU3CMJ7jWaUXvL)4pbQBMvT)Ui57Ah#OV#!4i6G95%T');
define('LOGGED_IN_SALT',   'u5$QvwVRpOIOLnSVDZvN3645Vc&EpoE$J))*^vF^!PD(jc%UuPfDYaL1cVt2qpcK');
define('NONCE_SALT',       'u#$YAsl5QfYUzk(keHE(PtwSJQ%F8vVOIKEioi%ly#BYW&cAJb8Uj9mjvCGUiLU)');
/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress.  A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de.mo to wp-content/languages and set WPLANG to 'de' to enable German
 * language support.
 */
//define ('WPLANG', 'en_US');

define ('FS_METHOD', 'direct');

define('WP_DEBUG', false);

define('WP_CACHE',true);

//error_reporting(E_ALL);
/* That's all, stop editing! Happy blogging. */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
if ( !( defined('BLOCK_LOAD') && BLOCK_LOAD ) ) require_once(ABSPATH . 'wp-settings.php');

//--- disable auto upgrade
define( 'AUTOMATIC_UPDATER_DISABLED', true );?>
